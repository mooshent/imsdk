package opendemo.activity;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.LoginStatus;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnConnectionChangedListener;
import imsdk.data.IMMyself.OnLoginStatusChangedListener;
import imsdk.data.IMSDK;
import opendemo.activity.base.BaseActivity;
import am.dtlib.model.a.base.DTAppEnv;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

// 登录界面
// 主要演示功能：
// 1. 注册
// 2. 登录
// 3. 一键登录
// 4. 自动登录
public final class LoginActivity extends BaseActivity {
	// demo
//	private static final String sAppKey = "c7fc5083cc9632d49456389b";

	// 爱萌开发者
	// private static final String sAppKey = "d3fecc6841c022fc7b7021dd";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBase(R.layout.activity_login);

		mCustomUserIDEditText = (EditText) findViewById(R.id.login_custom_userid_edittext);
		mPasswordEditText = (EditText) findViewById(R.id.login_password_edittext);

		mPasswordEditText.setTypeface(Typeface.DEFAULT);
		mPasswordEditText.setTransformationMethod(new PasswordTransformationMethod());

		mRegisterBtn = (Button) findViewById(R.id.login_register_btn);
		mLoginBtn = (Button) findViewById(R.id.login_login_btn);
		mOneKeyLoginBtn = (Button) findViewById(R.id.login_one_key_login_btn);
		mAutoLoginStatusHintTextView = (TextView) findViewById(R.id.login_autologin_hint_textview);
		mVersionTextView = (TextView) findViewById(R.id.login_version_textview);
		mCountDownTextView = (TextView) findViewById(R.id.login_count_down_textview);
		mCountDownBackgroundView = findViewById(R.id.login_count_down_bg_view);

		// 加载动画，与IMSDK无关
		mShowAnimation = AnimationUtils.loadAnimation(this, R.anim.show);
		mHideAnimation = AnimationUtils.loadAnimation(this, R.anim.hide);

		// 显示IMSDK版本号与版本名
		mVersionTextView.setText("版本号： " + IMSDK.getVersionNumber() + " " + IMSDK.getVersionName());

		// 监听连接状态
		IMMyself.setOnConnectionChangedListener(new OnConnectionChangedListener() {
			@Override
			public void onReconnected() {
				// 掉线后自动重连
				Toast.makeText(LoginActivity.this, "重连成功", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onDisconnected(boolean loginConflict) {
				if (loginConflict) {
					// 登录冲突
					Toast.makeText(LoginActivity.this, "登录冲突", Toast.LENGTH_SHORT).show();
					Log.e("Debug", "登录冲突1");
				} else {
					// 网络掉线
					Toast.makeText(LoginActivity.this, "网络掉线", Toast.LENGTH_SHORT).show();
					Log.e("Debug", "网络掉线");
				}

				// Intent intent = new Intent(LoginActivity.this,
				// LoginActivity.class);
				//
				// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				// LoginActivity.this.startActivity(intent);
			}
		});

		// 刷新登录状态显示
		reupdateLoginStatus();

		IMMyself.setOnLoginStatusChangedListener(new OnLoginStatusChangedListener() {
			@Override
			public void onLoginStatusChanged(LoginStatus oldLoginStatus, LoginStatus newLoginStatus) {
				// 刷新登录状态显示
				reupdateLoginStatus();
			}
		});

		// 动画展示 与IMSDK无关
		mHideAnimation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				mCountDownBackgroundView.setVisibility(View.INVISIBLE);
			}
		});

		// 注册按钮
		mRegisterBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// 注册调用共3步

				// 步骤1.
				// 重设customUserID
				boolean result = IMMyself.setCustomUserID(mCustomUserIDEditText.getText().toString());

				if (!result) {
					Toast.makeText(LoginActivity.this, IMSDK.getLastError(), Toast.LENGTH_SHORT).show();
					return;
				}

				// 步骤2.
				// 设置IMMyself用户登录密码
				result = IMMyself.setPassword(mPasswordEditText.getText().toString());

				if (!result) {
					Toast.makeText(LoginActivity.this, IMSDK.getLastError(), Toast.LENGTH_SHORT).show();
					return;
				}

				// 步骤3.
				// 调用注册接口
				IMMyself.register(3, new OnActionListener() {
					@Override
					public void onSuccess() {
						LoginActivity.this.stopCountDownAnimation();
						Toast.makeText(LoginActivity.this, "注册成功", Toast.LENGTH_SHORT).show();
						startActivity(new Intent(LoginActivity.this, MainActivity.class));
					}

					@Override
					public void onFailure(String error) {
						if (error.equals("Timeout")) {
							error = "注册超时";
						}

						LoginActivity.this.stopCountDownAnimation();
						Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
					}
				});

				// 界面动画展示（与IMSDK无关）
				LoginActivity.this.startCountDownAnimation(3);
			}
		});

		// 登录按钮
		mLoginBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				LoginActivity.this.login(false);
			}
		});

		// 一键登录按钮
		mOneKeyLoginBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				LoginActivity.this.login(true);
			}
		});

		getLoginInfo();
	}

	// 刷新登录状态显示
	private void reupdateLoginStatus() {
		if (IMMyself.getLoginStatus() == IMMyself.LoginStatus.Reconnecting) {
			mAutoLoginStatusHintTextView.setVisibility(View.VISIBLE);
			mAutoLoginStatusHintTextView.setText("掉线重连中……");
		} else if (IMMyself.getLoginStatus() == IMMyself.LoginStatus.AutoLogining) {
			mAutoLoginStatusHintTextView.setVisibility(View.VISIBLE);
			mAutoLoginStatusHintTextView.setText("自动登录中……");
		} else {
			mAutoLoginStatusHintTextView.setVisibility(View.GONE);
		}
	}

	private void saveLoginInfo(String uid, String pwd) {
		SharedPreferences sp = getSharedPreferences("LoginInfo", Activity.MODE_PRIVATE);

		SharedPreferences.Editor editor = sp.edit();

		editor.putString("uid", uid);
		editor.putString("pwd", pwd);
		editor.commit();
	}

	private void getLoginInfo() {
		SharedPreferences sp = getSharedPreferences("LoginInfo", Activity.MODE_PRIVATE);

		String uid = sp.getString("uid", "");
		String pwd = sp.getString("pwd", "");

		if (mCustomUserIDEditText != null) {
			mCustomUserIDEditText.setText(uid);
		}

		if (mPasswordEditText != null) {
			mPasswordEditText.setText(pwd);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		// 刷新登录状态显示
		reupdateLoginStatus();
	}

	// autoRegister为true表示免注册一键登录
	private void login(final boolean autoRegister) {
		// 登录共4步

		// 步骤3.
		IMMyself.checkServiceLoginStatus(new IMMyself.OnCheckServiceLoginStateListener() {

			@Override
			public void onNoConnection() {
				DTAppEnv.getMainHandler().post(new Runnable() {

					@Override
					public void run() {
						// 步骤1.
						// 重设customUserID
						// boolean result = IMMyself.setCustomUserID("3000040");
//						 boolean result = IMMyself.setCustomUserID("3000008");

						// lyc
//						boolean result = IMMyself.setCustomUserID("3000003");

						 boolean result = IMMyself.setCustomUserID("3000078");
//						 boolean result = IMMyself.setCustomUserID("3000079");

//						boolean result = IMMyself.setCustomUserID(mCustomUserIDEditText.getText().toString());

						if (!result) {
							Toast.makeText(LoginActivity.this, IMSDK.getLastError(), Toast.LENGTH_SHORT).show();
							return;
						}

						// 步骤2.
						// 设置IMMyself登录密码
//						result = IMMyself.setPassword("4zia9b49");
//						result = IMMyself.setPassword("0ux5r2y6");

						// lyc
//						result = IMMyself.setPassword("e0tql4ek");

						result = IMMyself.setPassword("1pswx4ic");
//						 result = IMMyself.setPassword("384mbvjj");
//						result = IMMyself.setPassword(mPasswordEditText.getText().toString());

						if (!result) {
							Toast.makeText(LoginActivity.this, IMSDK.getLastError(), Toast.LENGTH_SHORT).show();
							return;
						}

						// 主线程运行
						IMMyself.login(autoRegister, 5, new OnActionListener() {
							@Override
							public void onSuccess() {
								saveLoginInfo(IMMyself.getCustomUserID(), IMMyself.getPassword());

								LoginActivity.this.stopCountDownAnimation();
								IMMyself.setNickName("我是昵称啊魂淡");
								Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
								Log.e("Debug", "登录成功");
								startActivity(new Intent(LoginActivity.this, MainActivity.class));
							}

							@Override
							public void onFailure(String error) {
								if (error.equals("Timeout")) {
									error = "登录超时";
								} else if (error.equals("Wrong Password")) {
									error = "密码错误";
								}

								LoginActivity.this.stopCountDownAnimation();
								Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
								Log.e("Debug", error);
							}
						});
					}
				});
			}

			@Override
			public void onConnected() {
				Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
				Log.e("Debug", "登录成功");
				startActivity(new Intent(LoginActivity.this, MainActivity.class));
			}
		});

		// 展示倒计时动画（与IMSDK无关）
		startCountDownAnimation(5);
	}

	private EditText mCustomUserIDEditText;
	private EditText mPasswordEditText;
	private Button mRegisterBtn;
	private Button mLoginBtn;
	private Button mOneKeyLoginBtn;
	private TextView mAutoLoginStatusHintTextView;
	private TextView mVersionTextView;

	private TextView mCountDownTextView;
	private View mCountDownBackgroundView;

	private Animation mShowAnimation;
	private Animation mHideAnimation;
	private boolean mCountDownAnimating;

	// 开始倒计时动画
	private void startCountDownAnimation(final int timeInterval) {
		InputMethodManager inputMethodMgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

		inputMethodMgr.hideSoftInputFromWindow(mCustomUserIDEditText.getWindowToken(), 0);
		inputMethodMgr.hideSoftInputFromWindow(mPasswordEditText.getWindowToken(), 0);

		mCustomUserIDEditText.clearFocus();
		mPasswordEditText.clearFocus();

		mRegisterBtn.setClickable(false);
		mLoginBtn.setClickable(false);
		mOneKeyLoginBtn.setClickable(false);
		mCustomUserIDEditText.setFocusable(false);
		mPasswordEditText.setFocusable(false);
		mCountDownBackgroundView.setVisibility(View.VISIBLE);
		mCountDownTextView.setVisibility(View.VISIBLE);
		mCountDownAnimating = true;
		mCountDownBackgroundView.startAnimation(mShowAnimation);

		this.countDown(timeInterval);
	}

	// 停止倒计时动画，纯界面逻辑，与IMSDK无关
	private void stopCountDownAnimation() {
		mRegisterBtn.setClickable(true);
		mLoginBtn.setClickable(true);
		mOneKeyLoginBtn.setClickable(true);
		mCustomUserIDEditText.setFocusable(true);
		mCustomUserIDEditText.setFocusableInTouchMode(true);
		mPasswordEditText.setFocusable(true);
		mPasswordEditText.setFocusableInTouchMode(true);
		mCountDownAnimating = false;
		mCountDownTextView.clearAnimation();
		mCountDownBackgroundView.startAnimation(mHideAnimation);
		mCountDownTextView.setVisibility(View.INVISIBLE);
	}

	// 开始倒计时
	private void countDown(final int timeInterval) {
		if (!mCountDownAnimating || timeInterval < 0) {
			this.stopCountDownAnimation();
			return;
		}

		if (timeInterval == 0) {
			mCountDownTextView.setText("IMSDK");
		} else {
			mCountDownTextView.setText("" + timeInterval);
		}

		mCountDownTextView.clearAnimation();

		AnimationSet countDownAnimationSet = (AnimationSet) AnimationUtils.loadAnimation(this, R.anim.count_down);

		countDownAnimationSet.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				if (!mCountDownAnimating) {
					return;
				}

				LoginActivity.this.countDown(timeInterval - 1);
			}
		});

		mCountDownTextView.startAnimation(countDownAnimationSet);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();

		System.exit(0);
	}
}
