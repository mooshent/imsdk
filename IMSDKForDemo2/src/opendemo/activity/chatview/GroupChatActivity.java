package opendemo.activity.chatview;

import imsdk.views.IMChatView;
import imsdk.views.IMGroupChatView;
import opendemo.activity.base.BaseActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

public class GroupChatActivity extends BaseActivity {
	// data
	private String mGroupID;

	// ui
	private IMGroupChatView mGroupChatView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBase(0);

		mGroupID = getIntent().getStringExtra("groupID");

		// 创建一个IMGroupChatView实例
		mGroupChatView = new IMGroupChatView(this, mGroupID);

		// 添加到当前activity
		setContentView(mGroupChatView);
		
		// 配置IMGroupChatView实例
		mGroupChatView.setMaxGifCountInMessage(10);
		mGroupChatView.setUserMainPhotoVisible(true);
		mGroupChatView.setTitleBarVisible(true);
		mGroupChatView.setUserMainPhotoCornerRadius(10);
		
		//添加头像点击事件监听
		mGroupChatView.setOnHeadPhotoClickListener(new IMChatView.OnHeadPhotoClickListener() {
			
			@Override
			public void onClick(View v, String customUserID) {
				Toast.makeText(GroupChatActivity.this, "您点击了"+customUserID, Toast.LENGTH_SHORT).show();
			}
			
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// 为了实现捕获用户选择的图片
		mGroupChatView.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// 为了实现点击返回键隐藏表情栏
		mGroupChatView.onKeyDown(keyCode, event);
		return super.onKeyDown(keyCode, event);
	}
}
