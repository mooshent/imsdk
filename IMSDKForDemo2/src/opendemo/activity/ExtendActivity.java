package opendemo.activity;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.customuserinfo.IMMyselfCustomUserInfo;
import opendemo.activity.base.BaseActivity;
import opendemo.activity.photo.MyPhotoActivity;
import opendemo.activity.social.BlacklistActivity;
import opendemo.activity.social.ContactsActivity;
import opendemo.activity.social.RecentActivity;
import opendemo.activity.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

// 扩展页面
// 主要演示功能：
// 1. 修改自定义个人资料
public final class ExtendActivity extends BaseActivity implements View.OnClickListener {
	// ui
	private EditText mCustomUserInfoEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBase(R.layout.activity_extend);

		setTitleWithCustomUserID(IMMyself.getCustomUserID());

		TextView rightTextView = (TextView) findViewById(R.id.right);

		rightTextView.setText("头像");
		rightTextView.setVisibility(View.VISIBLE);

		((Button) findViewById(R.id.extend_submit)).setOnClickListener(this);
		((Button) findViewById(R.id.extend_friends_list_btn)).setOnClickListener(this);
		((Button) findViewById(R.id.extend_blacklist_btn)).setOnClickListener(this);
		((Button) findViewById(R.id.extend_recent_contacts_btn))
				.setOnClickListener(this);

		mCustomUserInfoEditText = (EditText) this
				.findViewById(R.id.extend_custom_userinfo_edittext);
		mCustomUserInfoEditText.setText(IMMyselfCustomUserInfo.get());
		mCustomUserInfoEditText
				.setSelection(mCustomUserInfoEditText.getText().length());
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.right:
			startActivity(new Intent(ExtendActivity.this, MyPhotoActivity.class));
			break;
		case R.id.extend_submit:
			final String customUserInfo = mCustomUserInfoEditText.getText().toString();

			mCustomUserInfoEditText.setEnabled(false);

			// 修改自定义个人资料
			IMMyselfCustomUserInfo.commit(customUserInfo, new OnActionListener() {
				@Override
				public void onSuccess() {
					// 修改成功的回调
					Toast.makeText(ExtendActivity.this, "修改成功", Toast.LENGTH_SHORT)
							.show();
					mCustomUserInfoEditText.setEnabled(true);
				}

				@Override
				public void onFailure(String error) {
					// 修改失败的回调
					Toast.makeText(ExtendActivity.this, "修改失败：" + error,
							Toast.LENGTH_SHORT).show();
					mCustomUserInfoEditText.setEnabled(true);
				}
			});
			break;
		case R.id.extend_friends_list_btn:
			startActivity(new Intent(ExtendActivity.this, ContactsActivity.class));
			break;
		case R.id.extend_blacklist_btn:
			startActivity(new Intent(ExtendActivity.this, BlacklistActivity.class));
			break;
		case R.id.extend_recent_contacts_btn:
			startActivity(new Intent(ExtendActivity.this, RecentActivity.class));
			break;
		default:
			break;
		}
	}
}
