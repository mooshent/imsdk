package opendemo.activity;

import opendemo.activity.chatview.ChatXMLActivity;
import imsdk.data.IMMyself;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

public class WaitingIMActivity extends Activity implements AnimationListener {
	
	private String mCustomUserID;
	
	private RelativeLayout mBaseLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_waiting);
		
		mCustomUserID = this.getIntent().getStringExtra("CustomUserID");
		
		//图片的淡入淡出效果
		mBaseLayout = (RelativeLayout) findViewById(R.id.welcomeLayout);
		
		Animation shadeAnim = AnimationUtils.loadAnimation(WaitingIMActivity.this, R.anim.waiting_shade_anim);
		shadeAnim.setStartTime(Animation.START_ON_FIRST_FRAME);
		shadeAnim.setFillAfter(true);
		shadeAnim.setAnimationListener(this);
		shadeAnim.setRepeatCount(0);
		
		mBaseLayout.startAnimation(shadeAnim);
	}
	
	private Runnable mIMLoginRunnable = new Runnable() {
		
		@Override
		public void run() {
			IMMyself.checkServiceLoginStatus(new IMMyself.OnCheckServiceLoginStateListener() {
				
				@Override
				public void onNoConnection() {
					Log.e("Debug", "数据有误");
					
					WaitingIMActivity.this.finish();
				}
				
				@Override
				public void onConnected() {
					//打开主界面
					Intent intent1 = new Intent(WaitingIMActivity.this, MainActivity.class);
					WaitingIMActivity.this.startActivity(intent1);
					
					//打开聊天界面
					Intent intent2 = new Intent(WaitingIMActivity.this, ChatXMLActivity.class);
					intent2.putExtra("CustomUserID", mCustomUserID);
					WaitingIMActivity.this.startActivity(intent2);
					
					WaitingIMActivity.this.finish();
				}
			});
		}
	};
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		//等待动画完成
		//必须等待一定时间，等待IMSDK初始化完成，再执行此方法
		mIMLoginRunnable.run();
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		
	}

	@Override
	public void onAnimationStart(Animation animation) {
		
	}
}
