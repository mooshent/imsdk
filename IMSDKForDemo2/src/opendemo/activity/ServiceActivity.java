package opendemo.activity;

import imsdk.data.IMMyself.OnInitializedListener;
import imsdk.data.customerservice.IMSDKCustomerService;
import imsdk.data.group.IMMyselfGroup;
import opendemo.activity.base.BaseListActivity;
import opendemo.activity.chatview.ChatActivity;
import opendemo.activity.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

public class ServiceActivity extends BaseListActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (IMSDKCustomerService.isInitialized()) {
			Toast.makeText(ServiceActivity.this, "客服模块已初始化", Toast.LENGTH_SHORT).show();
		}

		initBaseList(R.layout.activity_service, IMSDKCustomerService.getList());

		mListView.setPullEnable(false);
		mListView.setLoadEnable(false);

		setTitle("客服列表");

		// 判断群组模块是否已经初始化
		if (IMMyselfGroup.isInitialized()) {
			mInitProgressBar.setVisibility(View.GONE);
		} else {
			mInitProgressBar.setVisibility(View.VISIBLE);

			IMMyselfGroup.setOnInitializedListener(new OnInitializedListener() {
				@Override
				public void onInitialized() {
					mInitProgressBar.setVisibility(View.GONE);
				}
			});
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Intent intent = new Intent(ServiceActivity.this, ChatActivity.class);

		intent.putExtra("CustomUserID", IMSDKCustomerService.getList()
				.get(position - 1));
		startActivity(intent);
	}
}
