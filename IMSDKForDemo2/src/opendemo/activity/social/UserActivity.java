package opendemo.activity.social;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.customuserinfo.IMSDKCustomUserInfo;
import opendemo.activity.base.BaseActivity;
import opendemo.activity.chatview.ChatXMLActivity;
import opendemo.activity.photo.PhotoActivity;
import opendemo.activity.photo.ThumbnailActivity;
import opendemo.activity.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public final class UserActivity extends BaseActivity implements View.OnClickListener {
	// data
	private String mCustomUserID;

	// ui
	private EditText mContentEditText;
	private Button mSendBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBase(R.layout.activity_user);

		mCustomUserID = getIntent().getStringExtra("CustomUserID");

		setTitleWithCustomUserID(mCustomUserID);

		mContentEditText = (EditText) findViewById(R.id.user_content);

		mSendBtn = (Button) findViewById(R.id.user_send_btn);
		mSendBtn.setOnClickListener(this);

		((Button) findViewById(R.id.user_userinfo_btn)).setOnClickListener(this);

		Button userRelationBtn = (Button) findViewById(R.id.user_relation_btn);

		if (mCustomUserID.equals(IMMyself.getCustomUserID())) {
			userRelationBtn.setVisibility(View.GONE);
		} else {
			userRelationBtn.setOnClickListener(this);
		}

		((Button) findViewById(R.id.user_chat_ui_demo_btn)).setOnClickListener(this);
		((Button) findViewById(R.id.user_thumbnail_btn)).setOnClickListener(this);
		((Button) findViewById(R.id.user_photo_btn)).setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.user_send_btn:
			String content = mContentEditText.getText().toString();

			if (content != null && content.trim().length() > 0) {
				mSendBtn.setEnabled(false);

				IMMyself.sendText(content, mCustomUserID, 5, new OnActionListener() {
					@Override
					public void onSuccess() {
						Toast.makeText(UserActivity.this, "发送成功", Toast.LENGTH_SHORT)
								.show();
						mContentEditText.setText("");
						mSendBtn.setEnabled(true);
					}

					@Override
					public void onFailure(String error) {
						Toast.makeText(UserActivity.this, "发送失败：" + error,
								Toast.LENGTH_SHORT).show();
						mSendBtn.setEnabled(true);
					}
				});
			}
			break;
		case R.id.user_userinfo_btn:
			IMSDKCustomUserInfo.request(mCustomUserID, new OnActionListener() {
				@Override
				public void onSuccess() {
					showDialog(mCustomUserID, IMSDKCustomUserInfo.get(mCustomUserID));
				}

				@Override
				public void onFailure(String error) {
					showDialog(mCustomUserID, "获取信息失败：" + error);
				}
			});
			break;
		case R.id.user_relation_btn: {
			Intent intent = new Intent(UserActivity.this, RelationActivity.class);

			intent.putExtra("CustomUserID", mCustomUserID);
			startActivity(intent);
		}
			break;
		case R.id.user_chat_ui_demo_btn: {
			Intent intent = new Intent(UserActivity.this, ChatXMLActivity.class);

			intent.putExtra("CustomUserID", mCustomUserID);
			startActivity(intent);
		}
			break;
		case R.id.user_thumbnail_btn: {
			Intent intent = new Intent(UserActivity.this, ThumbnailActivity.class);

			intent.putExtra("CustomUserID", mCustomUserID);
			startActivity(intent);
		}
			break;
		case R.id.user_photo_btn: {
			Intent intent = new Intent(UserActivity.this, PhotoActivity.class);

			intent.putExtra("CustomUserID", mCustomUserID);
			startActivity(intent);
		}
			break;
		default:
			break;
		}
	}
}
