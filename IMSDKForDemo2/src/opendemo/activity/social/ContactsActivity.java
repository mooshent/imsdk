package opendemo.activity.social;

import imsdk.data.relations.IMMyselfRelations;
import opendemo.activity.base.BaseListActivity;
import opendemo.activity.chatview.ChatXMLActivity;
import opendemo.activity.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

public class ContactsActivity extends BaseListActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBaseList(R.layout.activity_contacts, IMMyselfRelations.getFriendsList());
		
		TextView rightTextView = (TextView) findViewById(R.id.right);

		rightTextView.setText("添加好友");
		rightTextView.setVisibility(View.VISIBLE);

		// 设置标题栏显示内容
		setTitle("好友列表");
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (position == 0) {
			return;
		}

		Intent intent = new Intent(ContactsActivity.this, ChatXMLActivity.class);

		intent.putExtra("CustomUserID",
				IMMyselfRelations.getFriendsList().get(position - 1));
		startActivity(intent);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.right:
			startActivity(new Intent(ContactsActivity.this, AddFriendActivity.class));
			break;
		}
	}
}
