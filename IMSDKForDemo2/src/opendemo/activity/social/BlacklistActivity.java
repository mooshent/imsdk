package opendemo.activity.social;

import imsdk.data.relations.IMMyselfRelations;
import opendemo.activity.base.BaseListActivity;
import opendemo.activity.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

public class BlacklistActivity extends BaseListActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBaseList(R.layout.activity_blacklist, IMMyselfRelations.getBlacklist());

		// 设置标题栏显示内容
		setTitle("黑名单列表");
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (position == 0) {
			return;
		}

		Intent intent = new Intent(BlacklistActivity.this, RelationActivity.class);

		intent.putExtra("CustomUserID",
				IMMyselfRelations.getBlacklist().get(position - 1));
		startActivity(intent);
	}
}
