package opendemo.activity.social;

import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.relations.IMMyselfRelations;
import opendemo.activity.base.BaseActivity;
import opendemo.activity.R;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

// 用户关系操作界面
public final class RelationActivity extends BaseActivity implements
		View.OnClickListener {
	// const
	private final static int FRIEND = 1;
	private final static int BLACKLIST = 2;
	private final static int STRANGER = 3;
	private final static int TIMEOUT = 5;

	// data
	private String mCustomUserID;

	// ui
	private EditText mRequestContentEditText;
	private Button mSendFriendRequestBtn;
	private Button mKickToBlacklistBtn;
	private Button mRemoveFromBlacklistBtn;
	private Button mRemoveFromFriendListBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBase(R.layout.activity_relation);

		// 设置标题栏显示内容
		setTitle("用户关系");

		mCustomUserID = getIntent().getStringExtra("CustomUserID");

		if (getIntent().getBooleanExtra("notice", false)) {
			showDealFriendRequestDialog();
		}

		mSendFriendRequestBtn = (Button) findViewById(R.id.relation_send_friend_request);
		mSendFriendRequestBtn.setOnClickListener(this);

		mRemoveFromFriendListBtn = (Button) findViewById(R.id.relation_remove_from_friend_list);
		mRemoveFromFriendListBtn.setOnClickListener(this);

		mKickToBlacklistBtn = (Button) findViewById(R.id.relation_kick_to_blacklist);
		mRemoveFromFriendListBtn.setOnClickListener(this);

		mRemoveFromBlacklistBtn = (Button) findViewById(R.id.relation_remove_from_blacklist);
		mRemoveFromBlacklistBtn.setOnClickListener(this);

		mRequestContentEditText = (EditText) findViewById(R.id.relation_request_content_edittext);
	}

	@Override
	protected void onResume() {
		super.onResume();

		int relation = 0;

		if (IMMyselfRelations.isMyBlacklistUser(mCustomUserID)) {
			relation = BLACKLIST;
		} else if (IMMyselfRelations.isMyFriend(mCustomUserID)) {
			relation = FRIEND;
		} else {
			relation = STRANGER;
		}

		updateUserInterface(relation);
	}

	@Override
	public void onClick(final View v) {
		v.setEnabled(false);

		switch (v.getId()) {
		case R.id.relation_send_friend_request:
			String content = mRequestContentEditText.getText().toString();

			IMMyselfRelations.sendFriendRequest(content, mCustomUserID, TIMEOUT,
					new OnActionListener() {
						@Override
						public void onSuccess() {
							showAddFriendDialog();
							v.setEnabled(true);
						}

						@Override
						public void onFailure(String error) {
							Toast.makeText(RelationActivity.this, "添加好友失败：" + error,
									Toast.LENGTH_SHORT).show();
							v.setEnabled(true);
						}
					});
			break;
		case R.id.relation_remove_from_friend_list:
			IMMyselfRelations.removeUserFromFriendsList(mCustomUserID, TIMEOUT,
					new OnActionListener() {
						@Override
						public void onSuccess() {
							updateUserInterface(STRANGER);
							Toast.makeText(RelationActivity.this, "移除好友成功！",
									Toast.LENGTH_SHORT).show();
							v.setEnabled(true);
						}

						@Override
						public void onFailure(String error) {
							Toast.makeText(RelationActivity.this, "移除好友失败：" + error,
									Toast.LENGTH_SHORT).show();
							v.setEnabled(true);
						}
					});
			break;
		case R.id.relation_kick_to_blacklist:
			IMMyselfRelations.moveUserToBlacklist(mCustomUserID, TIMEOUT,
					new OnActionListener() {
						@Override
						public void onSuccess() {
							updateUserInterface(BLACKLIST);
							Toast.makeText(RelationActivity.this, "拉黑成功！",
									Toast.LENGTH_SHORT).show();
							v.setEnabled(true);
						}

						@Override
						public void onFailure(String error) {
							Toast.makeText(RelationActivity.this, "拉黑失败：" + error,
									Toast.LENGTH_SHORT).show();
							v.setEnabled(true);
						}
					});
			break;
		case R.id.relation_remove_from_blacklist:
			IMMyselfRelations.removeUserFromBlacklist(mCustomUserID, TIMEOUT,
					new OnActionListener() {
						@Override
						public void onSuccess() {
							updateUserInterface(STRANGER);
							Toast.makeText(RelationActivity.this, "移除黑名单成功！",
									Toast.LENGTH_SHORT).show();
							v.setEnabled(true);
						}

						@Override
						public void onFailure(String error) {
							Toast.makeText(RelationActivity.this, "移除黑名单失败：" + error,
									Toast.LENGTH_SHORT).show();
							v.setEnabled(true);
						}
					});
			break;
		default:
			break;
		}
	}

	protected void showAddFriendDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(RelationActivity.this);

		builder.setTitle("www.IMSDK.im");

		String content = "已向 " + mCustomUserID + " 发送好友请求";

		builder.setMessage(content);
		builder.setCancelable(false);

		builder.setPositiveButton("确定", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		builder.create().show();
	}

	protected void showDealFriendRequestDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(RelationActivity.this);

		builder.setTitle("www.IMSDK.im");

		String content = "是否同意" + mCustomUserID + " 好友请求？";

		builder.setMessage(content);
		builder.setCancelable(false);

		builder.setPositiveButton("同意", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				agreeFriendRequest();
			}
		});

		builder.setNegativeButton("拒绝", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				rejectFriendRequest();
			}
		});

		builder.create().show();
	}

	private void updateUserInterface(int relation) {
		switch (relation) {
		case FRIEND:
			mRequestContentEditText.setVisibility(View.GONE);
			mRemoveFromBlacklistBtn.setVisibility(View.GONE);
			mSendFriendRequestBtn.setVisibility(View.GONE);
			mKickToBlacklistBtn.setVisibility(View.VISIBLE);
			mRemoveFromFriendListBtn.setVisibility(View.VISIBLE);
			break;
		case BLACKLIST:
			mRequestContentEditText.setVisibility(View.GONE);
			mRemoveFromBlacklistBtn.setVisibility(View.VISIBLE);
			mSendFriendRequestBtn.setVisibility(View.GONE);
			mSendFriendRequestBtn.setVisibility(View.GONE);
			mRemoveFromFriendListBtn.setVisibility(View.GONE);
			break;
		case STRANGER:
			mRequestContentEditText.setVisibility(View.VISIBLE);
			mRemoveFromBlacklistBtn.setVisibility(View.GONE);
			mSendFriendRequestBtn.setVisibility(View.VISIBLE);
			mSendFriendRequestBtn.setVisibility(View.VISIBLE);
			mRemoveFromFriendListBtn.setVisibility(View.GONE);
			break;
		default:
			break;
		}
	}

	private void agreeFriendRequest() {
		IMMyselfRelations.agreeToFriendRequest(mCustomUserID, 10,
				new OnActionListener() {
					@Override
					public void onSuccess() {
						Toast.makeText(RelationActivity.this,
								"现在你和" + mCustomUserID + "已经是好友了", Toast.LENGTH_LONG)
								.show();
						updateUserInterface(FRIEND);
					}

					@Override
					public void onFailure(String error) {
						Toast.makeText(RelationActivity.this,
								"处理" + mCustomUserID + "好友请求失败：" + error,
								Toast.LENGTH_LONG).show();
						updateUserInterface(STRANGER);
					}
				});
	}

	private void rejectFriendRequest() {
		IMMyselfRelations.rejectToFriendRequest("", mCustomUserID, 5,
				new OnActionListener() {
					@Override
					public void onSuccess() {
						Toast.makeText(RelationActivity.this,
								"你拒绝了" + mCustomUserID + "的好友请求！", Toast.LENGTH_LONG)
								.show();
						updateUserInterface(STRANGER);
					}

					@Override
					public void onFailure(String error) {
						Toast.makeText(RelationActivity.this,
								"你拒绝了" + mCustomUserID + "的好友请求！" + error,
								Toast.LENGTH_LONG).show();
						updateUserInterface(STRANGER);
					}
				});
	}
}
