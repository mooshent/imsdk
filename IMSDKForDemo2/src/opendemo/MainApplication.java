package opendemo;

import imsdk.data.IMMyself;
import opendemo.tool.ProcessTool;
import android.app.Application;

public class MainApplication extends Application {
	public static final String REAL_PACKAGE_NAME = "opendemo.activity";

	@Override
	public void onCreate() {
		super.onCreate();

		String processName = ProcessTool.getProcessName(this, android.os.Process.myPid());

		System.out.println("---------Application onCreate ---------：" + processName);

		if (processName != null) {
			boolean defaultProcess = processName.equals(REAL_PACKAGE_NAME);
			if (defaultProcess) {
				initAppForMainProcess();
			} else if (processName.contains(":im")) {
				initAppForIMSocketProcess();
			}
		}
	}

	private void initAppForMainProcess() {
		IMMyself.init(getApplicationContext());
	}

	private void initAppForIMSocketProcess() {

	}

}
