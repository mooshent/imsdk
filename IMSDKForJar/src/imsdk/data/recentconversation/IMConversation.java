//package imsdk.data.recentconversation;
//
//import imsdk.data.IMMessage;
//
//public class IMConversation implements Comparable<IMConversation> {
//	private static final String TAG = "conversation";
//
//	private long unreadMsgCount; //未读消息条数
//	private long msgCount;  //总消息条数
//	
//	private String conversation_id; //此次会话id---群id或customUserID
//	
//	private String userName; //用户名称或群名称
//	
//	private boolean isGroup;  //此次会话是否属于群消息
//	
//	private long lastMessageTime;          //收到最后一次信息时间
//	private String lastMessageContent;  //收到最后一次信息内容
//	
//	private IMMessage lastMessage;
//	
//	public long getUnreadMsgCount() {
//		return unreadMsgCount;
//	}
//
//	public void setUnreadMsgCount(long unreadMsgCount) {
//		this.unreadMsgCount = unreadMsgCount;
//	}
//
//	public long getMsgCount() {
//		return msgCount;
//	}
//
//	public void setMsgCount(long msgCount) {
//		this.msgCount = msgCount;
//	}
//
//	public String getConversationId() {
//		return conversation_id;
//	}
//
//	public void setConversationId(String conversation_id) {
//		this.conversation_id = conversation_id;
//	}
//
//	public String getUserName() {
//		return userName;
//	}
//
//	public void setUserName(String userName) {
//		this.userName = userName;
//	}
//
//	public boolean isGroup() {
//		return isGroup;
//	}
//
//	public void setGroup(boolean isGroup) {
//		this.isGroup = isGroup;
//	}
//
//	public long getLastMessageTime() {
//		return lastMessageTime;
//	}
//
//	public void setLastMessageTime(long lastMessageTime) {
//		this.lastMessageTime = lastMessageTime;
//	}
//
//	public String getLastMessageContent() {
//		return lastMessageContent;
//	}
//
//	public void setLastMessageContent(String lastMessageContent) {
//		this.lastMessageContent = lastMessageContent;
//	}
//
//	public IMMessage getLastMessage() {
//		return lastMessage;
//	}
//
//	public void setLastMessage(IMMessage lastMessage) {
//		this.lastMessage = lastMessage;
//	}
//
//	@Override
//	public int compareTo(IMConversation another) {
//		long time = this.lastMessageTime - another.lastMessageTime;
//		
//		return (time > 0) ? 1 : ((time < 0) ? -1 : 0);
//	}
//	
//
//}
