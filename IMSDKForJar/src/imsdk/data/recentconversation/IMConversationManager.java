//package imsdk.data.recentconversation;
//
//import imsdk.data.group.IMGroupInfo;
//import imsdk.data.group.IMSDKGroup;
//import imsdk.data.localchatmessagehistory.IMChatMessage;
//import imsdk.data.localchatmessagehistory.IMGroupChatMessage;
//import imsdk.data.localchatmessagehistory.IMMyselfLocalChatMessageHistory;
//import imsdk.data.recentcontacts.IMMyselfRecentContacts;
//import imsdk.data.recentgroups.IMMyselfRecentGroups;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//
//public class IMConversationManager {
//
//	public static List<IMConversation> loadAllConversations() {
//		List<IMConversation> conversations = new ArrayList<IMConversation>();
//		conversations.addAll(loadRecentContacts());
//		conversations.addAll(loadRecentGroups());
//
//		Collections.sort(conversations);
//
//		return conversations;
//	}
//
//	/**
//	 * 获取最近单聊列表
//	 * @return
//	 */
//	private static List<IMConversation> loadRecentContacts() {
//		List<IMConversation> contactMessages = new ArrayList<IMConversation>();
//		List<String> userLists = IMMyselfRecentContacts.getUsersList();
//		for (int i = 0; i < userLists.size(); i++) {
//			String cid = String.valueOf(userLists.get(i));
//			IMConversation userMessage = new IMConversation();
//			userMessage.setConversationId(cid);
//			userMessage.setGroup(false);
//
//			IMChatMessage chatMessage = IMMyselfLocalChatMessageHistory
//					.getLastChatMessage(cid);
//			if (chatMessage != null) {
//				userMessage.setLastMessageContent(chatMessage.getText());
//				userMessage
//						.setLastMessageTime(chatMessage.getServerSendTime() == 0 ? chatMessage
//								.getClientSendTime() : chatMessage.getServerSendTime());
//				userMessage.setUnreadMsgCount(IMMyselfRecentContacts
//						.getUnreadChatMessageCount(cid));
//				userMessage.setUserName(cid);
//				userMessage.setLastMessage(chatMessage);
//				userMessage.setMsgCount(IMMyselfLocalChatMessageHistory
//						.getChatMessageCount(cid));
//
//				contactMessages.add(userMessage);
//			}
//		}
//
//		return contactMessages;
//	}
//
//	/**
//	 * 获取最近群聊列表
//	 * @return
//	 */
//	private static List<IMConversation> loadRecentGroups() {
//		List<IMConversation> contactMessages = new ArrayList<IMConversation>();
//		List<String> groupList = IMMyselfRecentGroups.getGroupsList();
//
//		for (int i = 0; i < groupList.size(); i++) {
//			String gid = String.valueOf(groupList.get(i));
//
//			IMConversation userMessage = new IMConversation();
//			userMessage.setConversationId(gid);
//			userMessage.setGroup(true);
//
//			IMGroupChatMessage groupChatMessage = IMMyselfRecentGroups
//					.getLastGroupChatMessage(gid);
//			if (groupChatMessage != null) {
//				userMessage.setLastMessageContent(groupChatMessage.getText());
//				userMessage
//						.setLastMessageTime(groupChatMessage.getServerSendTime() == 0 ? groupChatMessage
//								.getClientSendTime() : groupChatMessage
//								.getServerSendTime());
//				userMessage.setUnreadMsgCount(IMMyselfRecentGroups
//						.getUnreadChatMessageCount(gid));
//				userMessage.setLastMessage(groupChatMessage);
//				userMessage.setMsgCount(IMMyselfRecentGroups.getChatMessageCount(gid));
//
//				IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(gid);
//				if (groupInfo != null) {
//					userMessage.setUserName(groupInfo.getGroupName());
//				}
//
//				contactMessages.add(userMessage);
//			}
//		}
//
//		return contactMessages;
//	}
//
//}
