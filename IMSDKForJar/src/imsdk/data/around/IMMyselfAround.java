package imsdk.data.around;

import imsdk.data.IMUserLocation;

import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.model.IMPrivateAroundUsers;
import am.imsdk.model.a2.IMMyselfAround2;
import am.imsdk.model.userinfo.IMPrivateUserInfo;
import am.imsdk.model.userinfo.IMUsersMgr;

public final class IMMyselfAround {
	public enum State {
		Normal(0), Updating(1), Paging(2), ReachEnd(3);

		private final int value;

		private State(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	};

	public interface OnAroundActionListener {
		public void onSuccess(ArrayList<String> customUserIDsListInCurrentPage);

		public void onFailure(String error);
	}

	public static State getState() {
		return IMMyselfAround2.getState();
	}

	public static void update(final OnAroundActionListener l) {
		IMMyselfAround2.update(l);
	}

	public static void nextPage(final OnAroundActionListener l) {
		IMMyselfAround2.nextPage(l);
	}

	public static ArrayList<String> getAllUsers() {
		return IMPrivateAroundUsers.getInstance().getCustomUserIDsList();
	}

	public static int getUsersCount() {
		return IMPrivateAroundUsers.getInstance().getCustomUserIDsList().size();
	}

	public static String getUser(int index) {
		return IMMyselfAround2.getUser(index);
	}

	public static ArrayList<IMUserLocation> getAllUserLocations() {
		ArrayList<String> aryCustomUserIDs = IMPrivateAroundUsers.getInstance()
				.getCustomUserIDsList();
		ArrayList<IMUserLocation> result = new ArrayList<IMUserLocation>();

		for (String customUserID : aryCustomUserIDs) {
			IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(
					customUserID);

			if (userInfo == null) {
				DTLog.logError();
				continue;
			}

			result.add(userInfo.getUserLocation());
		}

		return result;
	}
}
