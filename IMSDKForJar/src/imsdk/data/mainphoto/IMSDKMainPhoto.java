package imsdk.data.mainphoto;

import imsdk.data.IMSDK.OnDataChangedListener;
import am.imsdk.model.a2.IMSDKMainPhoto2;
import android.graphics.Bitmap;
import android.net.Uri;

public final class IMSDKMainPhoto {
	public static interface OnBitmapRequestProgressListener {
		public void onSuccess(Bitmap mainPhoto, byte[] buffer);

		public void onProgress(double progress);

		public void onFailure(String error);
	}

	public static void addOnDataChangedListener(final String customUserID,
			final OnDataChangedListener l) {
		IMSDKMainPhoto2.addOnDataChangedListener(customUserID, l);
	}

	public static void removeOnDataChangedListener(final OnDataChangedListener l) {
		IMSDKMainPhoto2.removeOnDataChangedListener(l);
	}

	public static long request(final String customUserID, long timeoutInterval,
			final OnBitmapRequestProgressListener l) {
		return IMSDKMainPhoto2.request(customUserID, timeoutInterval, l);
	}

	public static long request(final String customUserID, final int width,
			final int height, final OnBitmapRequestProgressListener l) {
		return IMSDKMainPhoto2.request(customUserID, width, height, l);
	}

	public static Uri getLocalUri(String customUserID) {

		return IMSDKMainPhoto2.getLocalUri(customUserID);
	}

	public static byte[] getLocalBuffer(String customUserID) {

		return IMSDKMainPhoto2.getLocalBuffer(customUserID);
	}

	public static Uri getLocalUri(String customUserID, int width, int height) {

		return IMSDKMainPhoto2.getLocalUri(customUserID, width, height);
	}

	public static byte[] getLocalBuffer(String customUserID, int width, int height) {

		return IMSDKMainPhoto2.getLocalBuffer(customUserID, width, height);
	}

	public static Bitmap get(String customUserID) {

		return IMSDKMainPhoto2.get(customUserID);
	}

	public static Bitmap get(String customUserID, int width, int height) {

		return IMSDKMainPhoto2.get(customUserID, width, height);
	}

	public static String getLastError() {

		return IMSDKMainPhoto2.getLastError();
	}

}
