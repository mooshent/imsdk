package imsdk.data.recentgroups;

import imsdk.data.IMSDK.OnDataChangedListener;
import imsdk.data.localchatmessagehistory.IMGroupChatMessage;

import java.util.ArrayList;

import am.dtlib.model.c.tool.DTLocalModel;
import am.imsdk.model.a2.IMMyselfRecentGroups2;

public final class IMMyselfRecentGroups extends DTLocalModel {
	public static ArrayList<String> getGroupsList() {
		return IMMyselfRecentGroups2.getGroupsList();
	}

	public static boolean removeGroup(String groupID) {
		return IMMyselfRecentGroups2.removeGroup(groupID);
	}

	public static void setOnDataChangedListener(OnDataChangedListener l) {
		IMMyselfRecentGroups2.setOnDataChangedListener(l);
	}

	public static int getChatMessageCount(String groupID) {
		return IMMyselfRecentGroups2.getChatMessageCount(groupID);
	}
	
	public static IMGroupChatMessage getLastGroupChatMessage(String groupID ) {
		return IMMyselfRecentGroups2.getLastGroupChatMessage(groupID);
	}

	public static IMGroupChatMessage getGroupChatMessage(String groupID, int index) {
		return IMMyselfRecentGroups2.getGroupChatMessage(groupID, index);
	}
	
	public static long getUnreadChatMessageCount(String groupID) {
		return IMMyselfRecentGroups2.getUnreadChatMessageCount(groupID);
	}

	public static long getUnreadChatMessageCount() {
		return IMMyselfRecentGroups2.getUnreadChatMessageCount();
	}

	public static boolean clearUnreadChatMessage() {
		return IMMyselfRecentGroups2.clearUnreadChatMessage();
	}

	public static boolean clearUnreadChatMessage(String groupID) {
		return IMMyselfRecentGroups2.clearUnreadChatMessage(groupID);
	}

	public static String getLastError() {
		return IMMyselfRecentGroups2.getLastError();
	}

}
