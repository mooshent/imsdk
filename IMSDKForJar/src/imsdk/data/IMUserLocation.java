package imsdk.data;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.model.userinfo.IMPrivateUserInfo;
import am.imsdk.model.userinfo.IMUsersMgr;

/*
 * 用户位置模型（经纬度 + UserID）
 */
public final class IMUserLocation {
	public IMUserLocation() {
		if (IMPrivateUserInfo.sCreatingLocationUID == 0) {
			DTLog.logError();
			return;
		}

		mUID = IMPrivateUserInfo.sCreatingLocationUID;
	}

	public final String getCustomUserID() {
		IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(mUID);

		if (userInfo == null) {
			DTLog.logError();
			return "";
		}

		return userInfo.getCustomUserID();
	}

	public final double getLatitude() {
		IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(mUID);

		if (userInfo == null) {
			DTLog.logError();
			return 0;
		}

		return userInfo.mLatitude;
	}

	public double getLongitude() {
		IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(mUID);

		if (userInfo == null) {
			DTLog.logError();
			return 0;
		}

		return userInfo.mLongitude;
	}

	private long mUID;
}
