package imsdk.data.customerservice;

import imsdk.data.IMMyself.OnActionResultListener;
import imsdk.data.IMMyself.OnInitializedListener;

import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.model.a2.IMSDKCustomerService2;
import am.imsdk.model.kefu.IMPrivateCSInfo;

public final class IMSDKCustomerService {
	public static boolean isInitialized() {
		return IMSDKCustomerService2.isInitialized();
	}

	public static void setOnInitializedListener(final OnInitializedListener l) {
		IMSDKCustomerService2.setOnInitializedListener(l);
	}

	public final static class IMCustomerServiceInfo {
		public IMCustomerServiceInfo() {
			mPrivateCSInfo = IMPrivateCSInfo.getCreatingCSInfo();

			if (mPrivateCSInfo == null) {
				DTLog.logError();
				return;
			}
		}

		public String getID() {
			if (mPrivateCSInfo == null) {
				DTLog.logError();
				return "";
			}

			if (mPrivateCSInfo.getCustomUserID().length() == 0) {
				DTLog.logError();
				return "";
			}

			return mPrivateCSInfo.getCustomUserID();
		}

		public String getNickName() {
			if (mPrivateCSInfo == null) {
				DTLog.logError();
				return "";
			}

			if (mPrivateCSInfo.getNickName().length() == 0) {
				DTLog.logError();
				return "";
			}

			return mPrivateCSInfo.getNickName();
		}

		public String getEmail() {
			if (mPrivateCSInfo == null) {
				DTLog.logError();
				return "";
			}

			if (mPrivateCSInfo.getEmail().length() == 0) {
				DTLog.logError();
				return "";
			}

			return mPrivateCSInfo.getEmail();
		}

		private IMPrivateCSInfo mPrivateCSInfo;
	}

	public static ArrayList<String> getList() {
		return IMSDKCustomerService2.getList();
	}

	public static IMCustomerServiceInfo getCustomerServiceInfo(
			final String customerServiceID) {
		return IMSDKCustomerService2.getCustomerServiceInfo(customerServiceID);
	}

	public static long request(final String customerServiceID,
			final long timeoutInterval, final OnActionResultListener l) {
		return IMSDKCustomerService2.request(customerServiceID, timeoutInterval, l);
	}
}
