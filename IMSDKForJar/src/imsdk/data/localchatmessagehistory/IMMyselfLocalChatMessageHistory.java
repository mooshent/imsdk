package imsdk.data.localchatmessagehistory;

import am.imsdk.model.a2.IMMyselfLocalChatMessageHistory2;

public final class IMMyselfLocalChatMessageHistory {
	public static IMChatMessage getChatMessage(String customUserID, int index) {
		return IMMyselfLocalChatMessageHistory2.getChatMessage(customUserID, index);
	}

	public static IMChatMessage getLastChatMessage(String customUserID) {
		return IMMyselfLocalChatMessageHistory2.getLastChatMessage(customUserID);
	}

	public static long getChatMessageCount(String customUserID) {
		return IMMyselfLocalChatMessageHistory2.getChatMessageCount(customUserID);
	}

	public static void removeAllChatMessage(String customUserID) {
		IMMyselfLocalChatMessageHistory2.removeAllChatMessage(customUserID);
	}

	public static String getLastError() {
		return IMMyselfLocalChatMessageHistory2.getLastError();
	}
}
