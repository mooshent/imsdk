package imsdk.data.recentcontacts;

import imsdk.data.IMSDK.OnDataChangedListener;
import java.util.ArrayList;
import am.imsdk.model.a2.IMMyselfRecentContacts2;

public final class IMMyselfRecentContacts {
	public static ArrayList<String> getUsersList() {
		return IMMyselfRecentContacts2.getUsersList();
	}

	public static String getUser(int index) {
		return IMMyselfRecentContacts2.getUser(index);
	}

	public static boolean removeUser(String customUserID) {
		return IMMyselfRecentContacts2.removeUser(customUserID);
	}

	public static void setOnDataChangedListener(OnDataChangedListener l) {
		IMMyselfRecentContacts2.setOnDataChangedListener(l);
	}

	public static long getUnreadChatMessageCount() {
		return IMMyselfRecentContacts2.getUnreadChatMessageCount();
	}

	public static boolean clearUnreadChatMessage() {
		return IMMyselfRecentContacts2.clearUnreadChatMessage();
	}

	public static long getUnreadChatMessageCount(String customUserID) {
		return IMMyselfRecentContacts2.getUnreadChatMessageCount(customUserID);
	}

	public static boolean clearUnreadChatMessage(String customUserID) {
		return IMMyselfRecentContacts2.clearUnreadChatMessage(customUserID);
	}

	public static String getLastError() {
		return IMMyselfRecentContacts2.getLastError();
	}
}
