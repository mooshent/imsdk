package imsdk.data.group;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.teaminfo.IMTeamsMgr;

public final class IMSDKGroup {
	public static IMGroupInfo getGroupInfo(String groupID) {
		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			sLastError = IMParamJudge.getLastError();
			return null;
		}
		
		long teamID = DTTool.getUnsecretLongValue(groupID);
		
		if (teamID == 0) {
			sLastError = "IMSDK Error";
			DTLog.logError();
			return null;
		}
		
		IMGroupInfo groupInfo = IMTeamsMgr.getInstance().getGroupInfo(teamID);
		
		if (groupInfo == null) {
			sLastError = "IMSDK Error";
		}
		
		return groupInfo;
	}
	
	public static String getLastError() {
		return sLastError;
	}
	
	private static String sLastError = "";
}
