package am.dtlib.model.a.base;

import java.io.File;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;

// 本类不能调用DTLog
public final class DTAppEnv {
	public static void setContext(Context context) {
		if (null == context) {
			return;
		}

		sContext = context;
		sMainHandler = new Handler(context.getMainLooper());
	}

	public static Context getContext() {
		return sContext;
	}

	public static Handler getMainHandler() {
		if (sMainHandler == null) {
			return null;
		}

		return sMainHandler;
	}

	public static void setRootDirectory(String rootDirectory) {
		sRootDirectory = rootDirectory;
	}

	public static String getRootDirectory() {
		return sRootDirectory == null ? "IMSDK.im" : sRootDirectory;
	}

	public static String getRootDirectoryFullPath() {
		boolean sdCardExist = Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED);

		if (sdCardExist) {
			String externalStorageDirectory = Environment.getExternalStorageDirectory()
					.toString();

			return new File(externalStorageDirectory, DTAppEnv.getRootDirectory())
					.toString();
		} else {
			return new File(getDocumentDirectoryFullPath(), DTAppEnv.getRootDirectory())
					.toString();
		}
	}

	public static String getPackageName() {
		if (getContext() != null) {
			return getContext().getPackageName();
		} else {
			return "am.imsdk.demo";
		}
	}

	public static String getDocumentDirectoryFullPath() {
		if (sDocumentDirectoryFullPath != null) {
			return sDocumentDirectoryFullPath;
		}

		boolean sdCardExist = Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED);

		if (sdCardExist) {
			sDocumentDirectoryFullPath = new File(Environment
					.getExternalStorageDirectory().toString(), getPackageName())
					.toString();
		} else if (DTAppEnv.getContext() != null) {
			sDocumentDirectoryFullPath = DTAppEnv.getContext().getFilesDir().toString();
		} else {
			sDocumentDirectoryFullPath = "";
		}

		return sDocumentDirectoryFullPath;
	}

	public static String getIMSDKDirectoryFullPath() {
		if (sIMSDKDirectoryFullPath != null) {
			return sIMSDKDirectoryFullPath;
		}

		sIMSDKDirectoryFullPath = new File(getRootDirectoryFullPath(), getPackageName())
				.toString();
		return sIMSDKDirectoryFullPath;
	}

	public static boolean isSDKDebugable() {
		if (DTAppEnv.sContext == null) {
			return false;
		}

		// 发布时用false
		return true;
	}

	public static void cancelPreviousPerformRequest(Runnable runnable) {
		if (runnable == null) {
			return;
		}

		if (sMainHandler == null) {
			return;
		}

		sMainHandler.removeCallbacks(runnable);
	}

	public static void cancelAllPreviousPerformRequests() {
		if (sMainHandler == null) {
			return;
		}

		sMainHandler.removeCallbacksAndMessages(null);
	}

	public static void performAfterDelayOnUIThread(long timeInterval, Runnable runnable) {
		if (null == sMainHandler) {
			return;
		}

		sMainHandler.postDelayed(runnable, timeInterval * 1000);
	}

	public static boolean isUIThread() {
		if (DTAppEnv.getContext() == null) {
			return false;
		}

		return Looper.myLooper() == DTAppEnv.getContext().getMainLooper();
	}

	public static boolean isNetworkConnected() {
		if (sContext == null) {
			return false;
		}

		ConnectivityManager connectivityManager = (ConnectivityManager) sContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (null == connectivityManager) {
			return false;
		}

		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

		if (null == networkInfo) {
			return false;
		}

		return networkInfo.isAvailable();
	}

	private static Handler sMainHandler = null;
	private static Context sContext = null;
	private static String sRootDirectory = "";
	private static String sDocumentDirectoryFullPath = null;
	private static String sIMSDKDirectoryFullPath = null;
}
