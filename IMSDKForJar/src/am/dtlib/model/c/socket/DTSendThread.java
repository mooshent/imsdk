package am.dtlib.model.c.socket;

import am.core.jni.IMProtocol;
import am.dtlib.model.b.log.DTLog;

public final class DTSendThread extends DTThread {
	public DTSendThread(Runnable runnable) {
		super(runnable);
	}
	
	public void setRecvThread(DTThread recvThread) {
		mRecvThread = recvThread;
	}
	
	public Thread getRecvThread() {
		return mRecvThread;
	}
	
	@Override
	public void cancel() {
		super.cancel();
		
		if (mRecvThread != null) {
			mRecvThread.cancel();
			mRecvThread = null;
		}

		DTLog.sign("IMProtocol.Close begin");
		IMProtocol.Close();
		DTLog.sign("IMProtocol.Close done");
	}

	private DTThread mRecvThread;
}
