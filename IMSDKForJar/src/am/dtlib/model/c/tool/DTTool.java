package am.dtlib.model.c.tool;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import android.util.Base64;

public final class DTTool {
	public static String getGroupIDFromTeamID(long teamID) {
		return getSecretString(teamID);
	}
	
	public static long getTeamIDFromGroupID(String groupID) {
		return getUnsecretLongValue(groupID);
	}
	
	public static String getSecretString(long value) {
		return Long.toHexString(value);
	}
	
	public static String getSecretString(int value) {
		return Integer.toHexString(value);
	}
	
	public static long getUnsecretLongValue(String string) {
		long result = Long.parseLong(string, 16);
		
		if (!getSecretString(result).equals(string)) {
			DTLog.logError();
		}

		return result;
	}
	
	public static Long getUnsecretIntValue(String string) {
		Long result = Long.parseLong(string, 16);
		
		if (!getSecretString(result).equals(string)) {
			DTLog.logError();
		}

		return result;
	}
	
	public static String getMD5String(byte[] buffer) {
		if (buffer == null) {
			DTLog.logError();
			return "";
		}
		
		if (!(buffer instanceof byte[])) {
			DTLog.logError();
			return "";
		}

		if (buffer.length == 0) {
			DTLog.logError();
			return "";
		}
		
	    byte[] hash;

        try {
			hash = MessageDigest.getInstance("MD5").digest(buffer);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
	        DTLog.logError();
	        DTLog.log(e.toString());
	        return "";
		}
        
	    StringBuilder hex = new StringBuilder(hash.length * 2);
	    
	    for (byte b : hash) {  
	        if ((b & 0xFF) < 0x10) {
	            hex.append("0");
	        }
	        
	        hex.append(Integer.toHexString(b & 0xFF));
	    }  
	    
	    return hex.toString();
	}
	
	public static String getMD5String(String string) {
		if (string == null) {
			DTLog.logError();
			return "";
		}
		
		if (!(string instanceof String)) {
			DTLog.logError();
			return "";
		}

		if (string.length() == 0) {
			DTLog.logError();
			return "";
		}
		
	    byte[] hash;
	    
	    try {
	        hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));  
	    } catch (NoSuchAlgorithmException e) {  
	        e.printStackTrace();
	        DTLog.logError();
	        DTLog.log(e.toString());
	        return "";  
	    } catch (UnsupportedEncodingException e) {  
	        e.printStackTrace();
	        DTLog.logError();
	        DTLog.log(e.toString());
	        return "";
	    }
	  
	    StringBuilder hex = new StringBuilder(hash.length * 2);
	    
	    for (byte b : hash) {  
	        if ((b & 0xFF) < 0x10) {
	            hex.append("0");
	        }
	        
	        hex.append(Integer.toHexString(b & 0xFF));
	    }  
	    
	    return hex.toString();
	}
	
	public static String getBase64EncodedString(String string) {
		if (string == null) {
			DTLog.logError();
			return "";
		}
		
		byte[] buffer = null;
		
		try {
			buffer = string.getBytes("UTF8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			DTLog.logError();
			return "";
		}
		
		return Base64.encodeToString(buffer, Base64.NO_WRAP);
	}

	public static String getBase64DecodedString(String string) {
		if (string == null) {
			DTLog.logError();
			return "";
		}
		
		byte[] buffer = null;
		
		try {
			buffer = Base64.decode(string, Base64.NO_WRAP);
		} catch (IllegalArgumentException e) {
			DTLog.logError();
			DTLog.log(string);
			DTLog.sign("string:" + string);
			return "";
		}
		
		String result = "";
		
		try {
			result = new String(buffer, "UTF8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			DTLog.logError();
			return "";
		}
		
		return result;
	}
	
	public static JSONObject getJsonObject(String jsonText) {
		try {
			return new JSONObject(jsonText);
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
}
