package am.dtlib.model.c.tool;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.http.util.EncodingUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;

public class DTLocalModel {
	public String mLocalFileName = "";
	public String mDecryptedLocalFileName = "";

	public void addIgnoreField(String field) {
		mIgnoreFieldsList.add(field);
	}

	public void addForceField(String field) {
		mForceFieldsList.add(field);
	}

	// dt dictionary
	public HashMap<String, Object> getModelHashMap() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		Field[] fields = this.getClass().getFields();

		for (Field field : fields) {
			try {
				result.put(field.getName(), field.get(this));
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	public DTLocalModel() {
		super();

		if (!DTAppEnv.isSDKDebugable()) {
			mEncrypted = true;
		} else {
			mEncrypted = false;
		}

		mLevel = 1;
	}

	public void addDirectory(String directory) {
		mDirectoriesList.add(directory);
	}

	public void addDecryptedDirectory(String directory) {
		mDecryptedDirectoriesList.add(directory);
	}

	public void setDirectory(String directory, int index) {
		while (mDirectoriesList.size() > index) {
			mDirectoriesList.remove(mDirectoriesList.size() - 1);
		}

		mDirectoriesList.add(directory);
	}

	public void setDecryptedDirectory(String directory, int index) {
		while (mDecryptedDirectoriesList.size() > index) {
			mDecryptedDirectoriesList.remove(mDecryptedDirectoriesList.size() - 1);
		}

		mDecryptedDirectoriesList.add(directory);
	}

	public boolean generateLocalFullPath() {
		return true;
	}

	public String getLocalFullPath() {
		String fileName = mEncrypted ? mLocalFileName : mDecryptedLocalFileName;

		fileName = (fileName != null && fileName.length() > 0) ? fileName : this
				.getClass().getSimpleName();

		String localPath = "";

		if (!mEncrypted && !mIsStreamData) {
			if (!fileName.endsWith(".txt")) {
				fileName = fileName + ".txt";
			}
		}

		if (DTAppEnv.getIMSDKDirectoryFullPath().length() > 0) {
			localPath = DTAppEnv.getIMSDKDirectoryFullPath();
		}

		ArrayList<String> arrayList = mEncrypted ? mDirectoriesList
				: mDecryptedDirectoriesList;

		for (String string : arrayList) {
			localPath = new File(localPath, string).toString();
		}

		DTLog.checkCreateDirectory(localPath);

		return new File(localPath, fileName).toString();
	}

	public boolean isLocalFileExist() {
		if (!this.generateLocalFullPath()) {
			return false;
		}

		String localPath = this.getLocalFullPath();
		File file = new File(localPath);

		if (file.exists() && file.isFile()) {
			return true;
		}

		return false;
	}

	public boolean readFromFile() {
		if (!this.isLocalFileExist()) {
			return false;
		}

		// 第一版-无加密
		File file = new File(this.getLocalFullPath());
		FileInputStream fileInputStream;

		try {
			fileInputStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			DTLog.logError();
			return false;
		}

		byte[] buffer;

		try {
			buffer = new byte[fileInputStream.available()];
			fileInputStream.read(buffer);
			fileInputStream.close();
		} catch (IOException e) {
			return false;
		}

		String content = EncodingUtils.getString(buffer, "UTF-8");

		if (content == null) {
			return false;
		}

		if (content.length() == 0) {
			return false;
		}

		JSONObject jsonObject;

		try {
			jsonObject = new JSONObject(content);
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			DTLog.log(e.toString());
			return false;
		}

		return setObjectWithJSONObject(this, jsonObject, mLevel, mForceFieldsList,
				mIgnoreFieldsList);
	}

	private boolean setObjectWithJSONObject(Object object, JSONObject jsonObject,
			int level, ArrayList<String> forceFieldsList,
			ArrayList<String> ignoreFieldsList) {
		if (object == null) {
			object = this;
		}

		Iterator<?> it = jsonObject.keys();

		while (it.hasNext()) {
			String key = (String) it.next();

			if (ignoreFieldsList != null && ignoreFieldsList.contains(key)) {
				continue;
			}

			Object value;

			try {
				value = jsonObject.get(key);
			} catch (JSONException e) {
				e.printStackTrace();
				DTLog.logError();
				continue;
			}

			Field field = getDeclaredField(object, key);

			if (field == null) {
				// rename
				if (mOldNewFieldsMap.containsKey(key)) {
					field = getDeclaredField(object, mOldNewFieldsMap.get(key));
				}
			}

			if (field == null) {
				continue;
			}

			// 兼容ArrayList<String>类型
			if (value != null
					&& field.getGenericType().toString()
							.equals("java.util.ArrayList<java.lang.String>")) {
				if (!(value instanceof JSONArray)) {
					DTLog.logError();
					return false;
				}

				JSONArray jsonArray = (JSONArray) value;
				ArrayList<String> array = new ArrayList<String>();

				for (int i = 0; i < jsonArray.length(); i++) {
					try {
						array.add(jsonArray.getString(i));
					} catch (JSONException e) {
						e.printStackTrace();
						DTLog.logError();
						DTLog.log(e.toString());
						return false;
					}
				}

				value = array;
			}

			// 兼容ArrayList<Long>类型
			if (value != null
					&& field.getGenericType().toString()
							.equals("java.util.ArrayList<java.lang.Long>")) {
				if (!(value instanceof JSONArray)) {
					DTLog.logError();
					return false;
				}

				JSONArray jsonArray = (JSONArray) value;
				ArrayList<Long> array = new ArrayList<Long>();

				for (int i = 0; i < jsonArray.length(); i++) {
					try {
						array.add(jsonArray.getLong(i));
					} catch (JSONException e) {
						e.printStackTrace();
						DTLog.logError();
						DTLog.log(e.toString());
						return false;
					}
				}

				value = array;
			}
			
			// 兼容HashMap<String, Long>类型
			if (value != null
					&& field.getGenericType()
							.toString()
							.startsWith(
									"java.util.HashMap<java.lang.String, java.lang.Long")) {
				if (!(value instanceof JSONObject)) {
					DTLog.logError();
					return false;
				}

				JSONObject jsonObjectForHashMap = (JSONObject) value;
				HashMap<String, Long> map = new HashMap<String, Long>();

				@SuppressWarnings("unchecked")
				Iterator<String> keysIterator = jsonObjectForHashMap.keys();

				while (keysIterator.hasNext()) {
					String keyString = keysIterator.next();
					Long valueObject;

					try {
						valueObject = jsonObjectForHashMap.getLong(keyString);
					} catch (JSONException e) {
						e.printStackTrace();
						DTLog.logError();
						DTLog.log(keyString);
						return false;
					}

					map.put(keyString, valueObject);
				}

				value = map;
			} else if (value != null
					&& field.getGenericType()
							.toString()
							.startsWith(
									"java.util.HashMap<java.lang.String, java.lang.")) {
				// 兼容HashMap<String, Object>类型
				
				if (!(value instanceof JSONObject)) {
					DTLog.logError();
					return false;
				}

				JSONObject jsonObjectForHashMap = (JSONObject) value;
				HashMap<String, Object> map = new HashMap<String, Object>();

				@SuppressWarnings("unchecked")
				Iterator<String> keysIterator = jsonObjectForHashMap.keys();

				while (keysIterator.hasNext()) {
					String keyString = keysIterator.next();
					Object valueObject;

					try {
						valueObject = jsonObjectForHashMap.get(keyString);
					} catch (JSONException e) {
						e.printStackTrace();
						DTLog.logError();
						DTLog.log(keyString);
						return false;
					}

					map.put(keyString, valueObject);
				}

				value = map;
			}

			// 兼容enum
			if (field.getType().isEnum()) {
				Class<?> c = field.getType();

				Method method = null;

				try {
					method = c.getDeclaredMethod("valueOf", String.class);
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
					DTLog.logError();
					DTLog.log(e.toString());
					continue;
				}

				try {
					value = method.invoke(c, value);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					DTLog.logError();
					DTLog.log(e.toString());
					continue;
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					DTLog.logError();
					DTLog.log(e.toString());
					continue;
				} catch (InvocationTargetException e) {
					e.printStackTrace();
					DTLog.logError();
					DTLog.log(e.toString());
					continue;
				}
			}

			// force
			if (mForceFieldsList.contains(field.getName())) {
				if (!(value instanceof JSONObject)) {
					DTLog.logError();
					continue;
				}

				JSONObject forceJsonObject = (JSONObject) value;

				try {
					value = field.getType().newInstance();
				} catch (InstantiationException e) {
					e.printStackTrace();
					DTLog.logError();
					continue;
				} catch (IllegalAccessException e) {
					e.printStackTrace();
					DTLog.logError();
					continue;
				}

				if (!setObjectWithJSONObject(value, forceJsonObject, 1, null, null)) {
					DTLog.logError();
					continue;
				}
			}

			field.setAccessible(true);

			try {
				field.set(object, value);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				DTLog.logError();
				DTLog.log(e.toString());
				DTLog.log(field.getName());
				DTLog.log(field.getGenericType().toString());
				DTLog.log(field.getType().toString());
				DTLog.log(field.getType().isEnum() + "");
				DTLog.log(field.getDeclaringClass().toString());
				DTLog.log(field.getDeclaringClass().isEnum() + "");
			} catch (IllegalAccessException e) {
				e.printStackTrace();
				DTLog.logError();
				DTLog.log(e.toString());
				DTLog.log(field.getName());
				DTLog.log(field.getGenericType().toString());
				DTLog.log(field.getType().toString());
			}
		}

		return true;
	}

	private JSONObject getJsonObject(Object object, int level,
			ArrayList<String> forceFieldsList, ArrayList<String> ignoreFieldsList) {
		if (object == null) {
			object = this;
		}

		Class<?> c = object.getClass();
		ArrayList<Field> fieldsList = new ArrayList<Field>();

		for (int i = 0; i < level; i++) {
			Field[] fields = c.getDeclaredFields();

			for (Field field : fields) {
				if (field.getGenericType().toString().equals("java.lang.Runnable")) {
					continue;
				}

				fieldsList.add(field);
			}

			c = c.getSuperclass();
		}

		JSONObject result = new JSONObject();

		for (Field field : fieldsList) {
			if (ignoreFieldsList != null && ignoreFieldsList.contains(field.getName())) {
				continue;
			}

			if (Modifier.isStatic(field.getModifiers())) {
				continue;
			}

			field.setAccessible(true);

			Object value = null;

			try {
				value = field.get(object);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				DTLog.logError();
				DTLog.log(e.toString());
				DTLog.log(field.getName());
				DTLog.log(field.getGenericType().toString());
			} catch (IllegalAccessException e) {
				e.printStackTrace();
				DTLog.logError();
				DTLog.log(e.toString());
				DTLog.log(field.getName());
				DTLog.log(field.getGenericType().toString());
			}

			// 兼容ArrayList<String>类型
			if (value != null
					&& field.getGenericType().toString()
							.equals("java.util.ArrayList<java.lang.String>")) {
				if (!(value instanceof ArrayList)) {
					DTLog.logError();
					return result;
				}

				@SuppressWarnings("unchecked")
				ArrayList<String> array = (ArrayList<String>) value;
				value = new JSONArray(array);
			}

			// 兼容ArrayList<Long>类型
			if (value != null
					&& field.getGenericType().toString()
							.equals("java.util.ArrayList<java.lang.Long>")) {
				if (!(value instanceof ArrayList)) {
					DTLog.logError();
					return result;
				}

				@SuppressWarnings("unchecked")
				ArrayList<Long> array = (ArrayList<Long>) value;
				value = new JSONArray(array);
			}

			// 兼容HashMap<String, Object>类型
			if (value != null
					&& field.getGenericType()
							.toString()
							.startsWith(
									"java.util.HashMap<java.lang.String, java.lang.")) {
				if (!(value instanceof HashMap)) {
					DTLog.logError();
					return result;
				}

				@SuppressWarnings("unchecked")
				HashMap<String, Object> map = (HashMap<String, Object>) value;
				value = new JSONObject(map);
			}

			if (forceFieldsList != null && forceFieldsList.contains(field.getName())) {
				value = getJsonObject(value, 1, null, null);
			}

			try {
				result.put(field.getName(), value);
			} catch (JSONException e) {
				e.printStackTrace();
				DTLog.logError();
				DTLog.log(e.toString());
				DTLog.log(field.getName());
				DTLog.log(field.getGenericType().toString());
			}
		}

		return result;
	}

	public boolean saveFile() {
		if (!this.generateLocalFullPath()) {
			return false;
		}

		JSONObject jsonObject = getJsonObject(null, mLevel, mForceFieldsList,
				mIgnoreFieldsList);

		if (!mIgnoreFieldsList.isEmpty()) {
			for (String key : mIgnoreFieldsList) {
				jsonObject.remove(key);
			}
		}

		try {
			protectedWriteToFile(jsonObject.toString().getBytes("UTF8"),
					getLocalFullPath());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			DTLog.logError();
			return false;
		}

		return true;
	}

	public void removeFile() {
		if (!this.generateLocalFullPath()) {
			return;
		}

		String localPath = this.getLocalFullPath();
		File file = new File(localPath);

		if (file.exists()) {
			file.delete();
		}
	}

	private Field getDeclaredField(Object object, String key) {
		if (key == null) {
			DTLog.logError();
			return null;
		}

		if (key.length() == 0) {
			DTLog.logError();
			return null;
		}

		if (object == null) {
			object = this;
		}

		Field result = null;
		Class<?> c = object.getClass();

		for (int i = 0; i < mLevel; i++) {
			try {
				result = c.getDeclaredField(key);
			} catch (NoSuchFieldException e) {
				c = c.getSuperclass();
				continue;
			}

			break;
		}

		return result;
	}

	protected byte[] protectedReadFromFile(String localFullPath) {
		File file = new File(localFullPath);

		if (!file.exists()) {
			return null;
		}

		FileInputStream fileInputStream;

		try {
			fileInputStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			return null;
		}

		byte[] buffer;

		try {
			buffer = new byte[fileInputStream.available()];
			fileInputStream.read(buffer);
			fileInputStream.close();
		} catch (IOException e) {
			return null;
		}

		return buffer;
	}

	protected void protectedWriteToFile(byte[] buffer, String localFullPath) {
		File file = new File(localFullPath);

		if (!file.exists()) {
			try {
				if (!file.createNewFile()) {
					DTLog.logError();
					return;
				}
			} catch (IOException e) {
				e.printStackTrace();
				DTLog.logError();
				DTLog.log(e.toString());
				DTLog.log("localPath:" + localFullPath);
				return;
			}
		}

		FileOutputStream fileOutputStream = null;

		try {
			fileOutputStream = new FileOutputStream(localFullPath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			DTLog.logError();
			DTLog.log(e.toString());
			return;
		}

		try {
			fileOutputStream.write(buffer);
			fileOutputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
			DTLog.logError();
			DTLog.log(e.toString());
			return;
		} finally {
			try {
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				DTLog.logError();
				DTLog.log(e.toString());
				return;
			}
		}
	}

	protected void addRenameField(String oldName, String newName) {
		mOldNewFieldsMap.put(oldName, newName);
	}

	protected int mLevel = 0;
	protected boolean mIsStreamData = false;
	private ArrayList<String> mIgnoreFieldsList = new ArrayList<String>();
	private ArrayList<String> mForceFieldsList = new ArrayList<String>();

	// 是否加密
	private boolean mEncrypted = false;
	private ArrayList<String> mDirectoriesList = new ArrayList<String>();
	private ArrayList<String> mDecryptedDirectoriesList = new ArrayList<String>();
	private HashMap<String, String> mOldNewFieldsMap = new HashMap<String, String>();
}
