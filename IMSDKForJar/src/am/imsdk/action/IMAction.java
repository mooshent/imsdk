package am.imsdk.action;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.imsdk.aacmd.user.IMCmdUserGetCustomerServiceInfo;
import am.imsdk.aacmd.user.IMCmdUserGetInfo;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.kefu.IMCustomerServiceMgr;
import am.imsdk.model.userinfo.IMUsersMgr;

public abstract class IMAction {
	public boolean isOver() {
		return mIsOver;
	}

	public long getOwnerUID() {
		return mOwnerUID;
	}

	private long mOwnerUID;
	private boolean mIsOver;
	public long mTimeoutInterval = 10;
	public long mBeginTime;
	private Runnable mTimeoutTask;
	protected int mStepCount = 2;
	private int mCurrentStep = 1;

	protected int getCurrentStep() {
		return mCurrentStep;
	}
	
	protected void resetStep() {
		mCurrentStep = 1;
	}

	// unfinished. by lyc.
	public void begin() {
		DTLog.sign("begin " + this.getClass().getSimpleName());

		if (!DTAppEnv.isUIThread()) {
			
			return;
		}

		mIsOver = false;
		mOwnerUID = IMPrivateMyself.getInstance().getUID();
		IMActionsMgr.getInstance().beginAction(this);

		if (mBeginTime == 0) {
			mBeginTime = System.currentTimeMillis() / 1000;
		}

		DTAppEnv.getMainHandler().post(new Runnable() {
			@Override
			public void run() {
				IMAction.this.onActionBegan();
			}
		});

		this.beginStep(1);

		if (mTimeoutInterval <= 0) {
			mTimeoutInterval = 60;
		}

		if (mTimeoutTask == null) {
			mTimeoutTask = new Runnable() {
				@Override
				public void run() {
					if (!DTAppEnv.isUIThread()) {
						DTLog.logError();
						return;
					}

//					IMAction.this.done("Timeout");
				}
			};
		}

		DTAppEnv.cancelPreviousPerformRequest(mTimeoutTask);
		DTAppEnv.performAfterDelayOnUIThread(mTimeoutInterval, mTimeoutTask);
	}

	protected void beginStep(final int stepNumber) {
		if (!DTAppEnv.isUIThread()) {
			DTLog.logError();
			return;
		}

		if (mIsOver) {
			return;
		}

		if (mCurrentStep != stepNumber) {
			mCurrentStep = stepNumber;
		}

		if (stepNumber > mStepCount) {
			this.done();
		} else {
			DTAppEnv.getMainHandler().post(new Runnable() {
				@Override
				public void run() {
					
					if (isOver()) {
						return;
					}
					IMAction.this.onActionStepBegan(stepNumber);
				}
			});
		}
	}

	public void nextStep() {
		beginStep(++mCurrentStep);
	}

	public void done() {
		DTLog.sign("done " + this.getClass().getSimpleName());
		DTAppEnv.cancelPreviousPerformRequest(mTimeoutTask);

		if (!DTAppEnv.isUIThread()) {
			DTLog.logError();
			return;
		}

		if (mIsOver) {
			DTLog.sign("already isOver " + this.getClass().getSimpleName());
			return;
		}

		if (this.mOnActionPartiallyDoneListener != null) {
			this.mOnActionPartiallyDoneListener.onActionPartiallyDone(100.0);
		} else {
			this.onActionPartiallyDone(100.0);
		}

		mIsOver = true;

		if (this.mOnActionDoneListener != null) {
			DTLog.sign("Listener onActionDone " + this.getClass().getSimpleName());
			this.mOnActionDoneListener.onActionDone();
		} else {
			DTLog.sign("onActionDone " + this.getClass().getSimpleName());
			this.onActionDone();
		}

		if (this.mOnActionDoneEndListener != null) {
			this.mOnActionDoneEndListener.onActionDoneEnd();
		}

		IMActionsMgr.getInstance().endAction(this);
	}

	public void done(String error) {
		DTLog.sign("done " + this.getClass().getSimpleName()  + " error:" + error);
		
		DTAppEnv.cancelPreviousPerformRequest(mTimeoutTask);

		if (!DTAppEnv.isUIThread()) {
			DTLog.logError();
			return;
		}

		if (mIsOver) {
			return;
		}

		mIsOver = true;

		if (this.mOnActionFailedListener != null) {
			this.mOnActionFailedListener.onActionFailed(error);
		} else {
			this.onActionFailed(error);
		}

		if (this.mOnActionFailedEndListener != null) {
			this.mOnActionFailedEndListener.onActionFailedEnd(error);
		}

		IMActionsMgr.getInstance().endAction(this);
	}

	public void doneWithIMSDKError() {
		DTLog.logError();
		this.done("IMSDK Error");
	}

	public void doneWithServerError() {
		DTLog.logError();
		this.done("IMSDK Server Error");
	}

	public void doneWithNetworkError() {
		this.done("Network Error");
	}

	public void done(final double percentage) {
		if (percentage > 100) {
			DTLog.logError();
			return;
		}

		if (DTAppEnv.isUIThread()) {
			if (mIsOver) {
				return;
			}

			if (mOnActionPartiallyDoneListener != null) {
				mOnActionPartiallyDoneListener.onActionPartiallyDone(percentage);
			} else {
				onActionPartiallyDone(percentage);
			}
		} else {
			DTAppEnv.getMainHandler().post(new Runnable() {
				@Override
				public void run() {
					if (mIsOver) {
						return;
					}

					if (mOnActionPartiallyDoneListener != null) {
						mOnActionPartiallyDoneListener
								.onActionPartiallyDone(percentage);
					} else {
						onActionPartiallyDone(percentage);
					}
				}
			});
		}
	}

	protected void checkBeginGetCustomUserID(long uid) {
		if (uid == 0) {
			doneWithIMSDKError();
			return;
		}

		String customUserID = IMCustomerServiceMgr.getInstance().getCustomUserID(uid);

		if (customUserID.length() == 0) {
			customUserID = IMUsersMgr.getInstance().getCustomUserID(uid);
		}

		if (customUserID.length() != 0) {
			nextStep();
		} else {
			beginGetCustomUserID(uid);
		}
	}

	protected void beginGetCustomUserID(long uid) {
		if (uid == 0) {
			this.doneWithIMSDKError();
			return;
		}

		if ((uid & 0x000f) == 6) {
			IMCmdUserGetCustomerServiceInfo cmd = new IMCmdUserGetCustomerServiceInfo();

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					if (IMAction.this.isOver()) {
						return;
					}

					nextStep();
				}
			};

			cmd.send();
		} else {
			IMCmdUserGetInfo cmd = new IMCmdUserGetInfo();

			cmd.addUID(uid);
			cmd.addProperty("phonenum");

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) {
					if (IMAction.this.isOver()) {
						return;
					}

					nextStep();
				}
			};

			cmd.send();
		}
	}

	protected void checkBeginGetUID(String customUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			this.doneWithIMSDKError();
			return;
		}

		long uid = IMCustomerServiceMgr.getInstance().getUID(customUserID);

		if (uid == 0) {
			uid = IMUsersMgr.getInstance().getUID(customUserID);
		}

		if (uid != 0) {
			nextStep();
		} else {
			beginGetUID(customUserID);
		}
	}

	protected void beginGetUID(String customUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			this.doneWithIMSDKError();
			return;
		}

//		IMCmdUserGetUID cmd = new IMCmdUserGetUID();
//
//		cmd.mPhoneNum = customUserID;
//
//		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
//			@Override
//			public void onCommonFailed(FailedType type, long errorCode,
//					JSONObject erroJsonObject) throws JSONException {
//				commonFailedDealWithJudge(type);
//			}
//		};
//
//		cmd.mOnRecvEndListener = new OnRecvEndListener() {
//			@Override
//			public void onRecvEnd(JSONObject jsonObject) throws JSONException {
//				nextStep();
//			}
//		};
//
//		cmd.send();
		
		IMUsersMgr.getInstance().setCustomUserIDForUID(customUserID, Long.parseLong(customUserID));
		IMUsersMgr.getInstance().saveFile();
		
		nextStep();
	}

	protected void commonFailedDealWithJudge(FailedType type) {
		if (IMAction.this.isOver()) {
			return;
		}

		if (type == FailedType.RecvError) {
			IMAction.this.doneWithIMSDKError();
		} else {
			IMAction.this.doneWithNetworkError();
		}
	}

	protected void commonFailedDealWithJudge(FailedType type, long errorCode,
			JSONObject errorJsonObject) {
		if (IMAction.this.isOver()) {
			return;
		}

		if (type == FailedType.RecvError) {
//			if (errorCode == 0 || errorJsonObject == null
//					|| errorJsonObject.toString().length() == 0) {
			if (errorCode == 0) {
				IMAction.this.doneWithIMSDKError();
			} else {
				IMAction.this
						.done("errorCode:" + errorCode + ";errorJsonObject:"
								+ (errorJsonObject == null ? "null" : errorJsonObject
								.toString()));
			}
		} else {
			IMAction.this.doneWithNetworkError();
		}
	}

	public void performStep(final int step) {
		if (mIsOver) {
			DTLog.logError();
			return;
		}

		if (step > mStepCount) {
			DTLog.logError();
			return;
		}

		if (mOwnerUID != IMPrivateMyself.getInstance().getUID()) {
			IMActionsMgr.getInstance().endAction(this);
			return;
		}

		mCurrentStep = step;
		this.onActionStepBegan(step);
	}

	protected void beginGetBaseInfo(long uid) {
		IMCmdUserGetInfo cmd = new IMCmdUserGetInfo();

		cmd.addUID(uid);
		cmd.addProperty("baseinfo");

		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
			@Override
			public void onCommonFailed(FailedType type, long errorCode,
					JSONObject errorJsonObject) throws JSONException {
				commonFailedDealWithJudge(type);
			}
		};

		cmd.mOnRecvEndListener = new OnRecvEndListener() {
			@Override
			public void onRecvEnd(JSONObject jsonObject) {
				if (IMAction.this.isOver()) {
					return;
				}

				nextStep();
			}
		};

		cmd.send();
	}

	public abstract void onActionBegan();

	public abstract void onActionStepBegan(final int stepNumber);

	public void onActionPartiallyDone(double percentage) {
	}

	public void onActionDone() {
	}

	public void onActionFailed(String error) {
	}

	public interface OnActionDoneListener {
		public void onActionDone();
	}

	public OnActionDoneListener mOnActionDoneListener;

	public interface OnActionPartiallyDoneListener {
		public void onActionPartiallyDone(double percentage);
	}

	public OnActionPartiallyDoneListener mOnActionPartiallyDoneListener;

	public interface OnActionDoneEndListener {
		public void onActionDoneEnd();
	}

	public OnActionDoneEndListener mOnActionDoneEndListener;

	public interface OnActionFailedListener {
		public void onActionFailed(String error);
	}

	public OnActionFailedListener mOnActionFailedListener;

	public interface OnActionFailedEndListener {
		public void onActionFailedEnd(String error);
	}

	public OnActionFailedEndListener mOnActionFailedEndListener;
}
