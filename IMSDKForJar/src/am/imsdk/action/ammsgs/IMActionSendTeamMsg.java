package am.imsdk.action.ammsgs;

import imsdk.data.localchatmessagehistory.IMGroupChatMessage;

import java.io.UnsupportedEncodingException;

import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.action.IMAction;
import am.imsdk.action.fileserver.IMActionUploadFile;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amimteam.IMTeamMsg;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;
import am.imsdk.model.amimteam.IMTeamMsgsMgr;
import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.audio.IMAudiosMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;

// 发送各种类型的TeamMsg
// 只负责大容量消息转文件
// 维护内存IMTeamMsgsMgr
// 1. 上传文件
// 2. 发送文本消息
public final class IMActionSendTeamMsg extends IMAction {
	public static final int IMTextMaxLength = 400;
	public IMTeamMsg mTeamMsg;
	private IMTeamMsg mRealTeamMsg;
	private byte[] mBuffer;

	public IMActionSendTeamMsg() {
		mStepCount = 2;
	}

	@Override
	public void begin() {
		if (mTeamMsg == null) {
			doneWithIMSDKError();
			return;
		}

		super.begin();

		mTeamMsg.mStatus = IMGroupChatMessage.SENDING_OR_RECVING;
		mTeamMsg.saveFile();
	}

	@Override
	public void onActionDone() {
		if (mTeamMsg.mContent.length() > IMTextMaxLength) {
			mRealTeamMsg.mStatus = IMGroupChatMessage.SUCCESS;
			mRealTeamMsg.saveFile();
			mTeamMsg.mMsgID = mRealTeamMsg.mMsgID;
			mTeamMsg.mServerSendTime = mRealTeamMsg.mServerSendTime;
		}

		IMTeamMsgsMgr.getInstance().moveUnsentTeamMsgToBeSent(mTeamMsg);
		mTeamMsg.mStatus = IMGroupChatMessage.SUCCESS;
		mTeamMsg.saveFile();

		DTNotificationCenter.getInstance().postNotification(
				mTeamMsg.getSendStatusChangedNotificationKey());
	}

	@Override
	public void onActionFailed(String error) {
		mTeamMsg.mStatus = IMGroupChatMessage.FAILURE;
		mTeamMsg.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				mTeamMsg.getSendStatusChangedNotificationKey());
		IMPrivateMyself.getInstance().checkIfIPAddressChanged();
	}

	@Override
	public void onActionBegan() {
		if (mTeamMsg == null) {
			doneWithIMSDKError();
			return;
		}

		if (mTeamMsg.mTeamMsgType == null) {
			doneWithIMSDKError();
			return;
		}

		if (mTeamMsg.mTeamMsgType == TeamMsgType.Unit) {
			doneWithIMSDKError();
			return;
		}

		if (mTeamMsg.mIsRecv) {
			doneWithIMSDKError();
			return;
		}

		if (!IMParamJudge.isTeamIDLegal(mTeamMsg.mTeamID)) {
			doneWithIMSDKError();
			return;
		}

		if (mTeamMsg.mContent == null) {
			mTeamMsg.mContent = "";
		}

		if (mTeamMsg.mClientSendTime == 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			switch (mTeamMsg.mTeamMsgType) {
			case Normal:
			case Custom: {
				if (mTeamMsg.mContent.length() <= IMTextMaxLength) {
					nextStep();
					return;
				}

				// 构造FileText
				IMFileText fileText = IMFileTextsMgr.getInstance().getFileTextWithText(
						mTeamMsg.mContent);

				fileText.saveFile();

				try {
					mBuffer = DTTool.getBase64EncodedString(fileText.mText).getBytes(
							"UTF8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					doneWithIMSDKError();
					return;
				}

				mRealTeamMsg = new IMTeamMsg();

				mRealTeamMsg.mFromCustomUserID = IMPrivateMyself.getInstance()
						.getCustomUserID();
				mRealTeamMsg.mTeamID = mTeamMsg.mTeamID;
				mRealTeamMsg.mClientSendTime = mTeamMsg.mClientSendTime;

				if (mTeamMsg.mTeamMsgType == TeamMsgType.Normal) {
					mRealTeamMsg.mTeamMsgType = TeamMsgType.NormalFileText;
				} else {
					mRealTeamMsg.mTeamMsgType = TeamMsgType.CustomFileText;
				}

//				mRealTeamMsg.mContent = fileText.mFileID;
				mRealTeamMsg.setFileID(fileText.mFileID);
			}
				break;
			case Audio: {
				IMAudio audio = IMAudiosMgr.getInstance()
						.getAudio(mTeamMsg.getFileID());

				if (audio.mBuffer == null || audio.mBuffer.length == 0) {
					if (!audio.readFromFile()) {
						doneWithIMSDKError();
						return;
					}
				}

				if (!audio.isLocalFileExist()) {
					audio.saveFile();
				}

				mBuffer = audio.mBuffer;
			}
				break;
			case Photo: {
				IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
						mTeamMsg.getFileID());

				if (photo.getBuffer() == null || photo.getBufferLength() == 0) {
					if (!photo.readFromFile()) {
						doneWithIMSDKError();
						return;
					}
				}

				if (!photo.isLocalFileExist()) {
					photo.saveFile();
				}

				mBuffer = photo.getBuffer();
			}
				break;
			default:
				nextStep();
				return;
			}

			final IMActionUploadFile action = new IMActionUploadFile();

			action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
				@Override
				public void onActionFailedEnd(String error) {
					done(error);
				}
			};

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					switch (mTeamMsg.mTeamMsgType) {
					case Normal:
					case Custom: {
						if (mRealTeamMsg == null) {
							doneWithIMSDKError();
							return;
						}

						IMFileText fileText = IMFileTextsMgr.getInstance().getFileText(
								mRealTeamMsg.getFileID());

						if (fileText == null) {
							doneWithIMSDKError();
							return;
						}

						if (!fileText.isLocalFileExist()) {
							doneWithIMSDKError();
							return;
						}

						fileText.readFromFile();
						IMFileTextsMgr.getInstance().replace(fileText.mFileID,
								action.mFileID);
						fileText.mFileID = action.mFileID;
						fileText.saveFile();
					}
						break;
					case Audio: {
						mRealTeamMsg = mTeamMsg;

						IMAudio audio = IMAudiosMgr.getInstance().getAudio(
								mTeamMsg.getFileID());

						audio.removeFile();
						IMAudiosMgr.getInstance().replaceClientFileID(audio.mFileID,
								action.mFileID);
						audio.mFileID = action.mFileID;
						audio.saveFile();
					}
						break;
					case Photo: {
						mRealTeamMsg = mTeamMsg;

						IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
								mTeamMsg.getFileID());

						photo.removeFile();
						IMAudiosMgr.getInstance().replaceClientFileID(photo.mFileID,
								action.mFileID);
						photo.mFileID = action.mFileID;
						photo.saveFile();
					}
						break;
					default:
						doneWithIMSDKError();
						return;
					}

					if (mRealTeamMsg == null) {
						doneWithIMSDKError();
						return;
					}

					mRealTeamMsg.setFileID(action.mFileID);
					nextStep();
				}
			};

			action.mBuffer = mBuffer;
			action.begin();
		}
			break;
		case 2: {
			IMActionSendBaseTeamMsg action = new IMActionSendBaseTeamMsg();

			if (mRealTeamMsg != null) {
				action.mTeamMsg = mRealTeamMsg;
			} else {
				action.mTeamMsg = mTeamMsg;
			}

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					done();
				}
			};

			action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
				@Override
				public void onActionFailedEnd(String error) {
					done(error);
				}
			};

			action.begin();
		}
			break;
		default:
			break;
		}
	}
}
