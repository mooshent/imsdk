package am.imsdk.action;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.LoginStatus;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.socket.DTSocket.DTSocketStatus;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.aacmd.IMSocket;
import am.imsdk.aacmd.user.IMCmdUserGetSetupID;
import am.imsdk.aacmd.user.IMCmdUserLogin;
import am.imsdk.aacmd.user.IMCmdUserRegister;
import am.imsdk.aacmd.user.IMCmdUserSetSetupID;
import am.imsdk.model.IMAppSettings;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.kefu.IMCustomerServiceMgr;
import am.imsdk.model.userinfo.IMUsersMgr;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;

// 注册或登录，用户行为或非用户行为
// 1. 判断网络，重新连接
// 2. 注册、登录
// 完成
public final class IMActionLogin extends IMAction {
	public long mUID;
	public String mCustomUserID;
	private String mPassword;
	public String mAppKey;
	private IMActionLoginType mType = IMActionLoginType.None;

	private JSONObject mCmdJsonObjectResult = null;

	private Observer mSocketUpdatedObserver;
	private IMPrivateMyself mPrivateMyself;

	private IMActionLogin() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (mType == null) {
			doneWithIMSDKError();
			return;
		}

		if (mType == IMActionLoginType.None) {
			doneWithIMSDKError();
			return;
		}

		if (mPassword == null || mPassword.length() == 0) {
			doneWithIMSDKError();
			return;
		}

		if (IMCustomerServiceMgr.getInstance().getUID(mCustomUserID) != 0) {
			mPrivateMyself = IMPrivateMyself.getInstance();
			done("Client can't login customer service account");
			return;
		}

		if (mType == IMActionLoginType.OneKeyLogin) {
			if (!(mCustomUserID instanceof String)) {
				doneWithIMSDKError();
				return;
			}

			if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)
					&& mCustomUserID != null && mCustomUserID.length() != 0) {
				doneWithIMSDKError();
				return;
			}
		} else {
			if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)) {
				doneWithIMSDKError();
				return;
			}
		}

		if (mType == IMActionLoginType.OneKeyLogin) {
			if (!(mPassword instanceof String)) {
				doneWithIMSDKError();
				return;
			}

			if (!IMParamJudge.isPasswordLegal(mPassword) && mPassword != null
					&& mPassword.length() != 0) {
				doneWithIMSDKError();
				return;
			}

			if (mCustomUserID == null || mCustomUserID.length() == 0) {
				mUID = 0;
			}
		} else {
			if (!IMParamJudge.isPasswordLegal(mPassword)) {
				doneWithIMSDKError();
				return;
			}
		}

		if (!IMParamJudge.isAppKeyLegal(mAppKey)) {
			doneWithIMSDKError();
			return;
		}

		if (mType == IMActionLoginType.OneKeyLogin
				|| mType == IMActionLoginType.OneKeyRegister) {
			IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.Logining);
			IMPrivateMyself.getInstance().setCustomUserInfoInitedInFact(false);
			IMPrivateMyself.getInstance().setGroupInfoInitedInFact(false);
			IMPrivateMyself.getInstance().setRelationsInitedInFact(false);
//			IMPrivateMyself.getInstance().setAppInfoInitedInFact(false);
			IMPrivateMyself.getInstance().saveFile();
		} else if (mType == IMActionLoginType.Reconnect) {
			IMPrivateMyself.getInstance().setLoginStatus(
					IMMyself.LoginStatus.Reconnecting);
			IMPrivateMyself.getInstance().setCustomUserInfoInited(true);
			IMPrivateMyself.getInstance().setGroupInfoInited(true);
			IMPrivateMyself.getInstance().setRelationsInited(true);
//			IMPrivateMyself.getInstance().setAppInfoInited(true);
			IMPrivateMyself.getInstance().saveFile();
		} else if (mType == IMActionLoginType.AutoLogin) {
			IMPrivateMyself.getInstance().setLoginStatus(
					IMMyself.LoginStatus.AutoLogining);
			IMPrivateMyself.getInstance().setCustomUserInfoInited(true);
			IMPrivateMyself.getInstance().setGroupInfoInited(true);
			IMPrivateMyself.getInstance().setRelationsInited(true);
//			IMPrivateMyself.getInstance().setAppInfoInited(true);
			IMPrivateMyself.getInstance().saveFile();
		} else {
			DTLog.logError();
		}

		if (mSocketUpdatedObserver != null) {
			DTNotificationCenter.getInstance().removeObserver("SOCKET_UPDATED", mSocketUpdatedObserver);
		}
		
		Log.e("Debug", "IMActionLogin--创建--"+ IMActionLogin.this);

		mSocketUpdatedObserver = new Observer() {
			@Override
			public void update(Observable observable, Object data) {
				Log.e("Debug", "IMActionLogin--mSocketUpdatedObserver:"
						+ IMActionLogin.this + "---"
						+ IMSocket.getInstance().getStatusWithoutSync());

				if (isOver() || IMActionLogin.getInstance() != IMActionLogin.this) {
					DTNotificationCenter.getInstance().removeObserver("SOCKET_UPDATED", this);
					return;
				}

				if (IMSocket.getInstance().getStatusWithoutSync() == DTSocketStatus.Connected) {

					Log.e("Debug", "IMActionLogin--"
							+ IMSocket.getInstance().getStatusWithoutSync());

					if (getCurrentStep() != 1) {
						Log.e("Debug", "IMActionLogin--"
								+ IMSocket.getInstance().getStatusWithoutSync()
								+ "--but currStep = 2");
//						doneWithNetworkError();
						return;
					}

					IMActionLogin.this.nextStep();
				} else if (IMSocket.getInstance().getStatusWithoutSync() == DTSocketStatus.None) {
					resetStep();
					
					IMSocket.getInstance().reconnect();
				}
			}
		};

		DTNotificationCenter.getInstance().addObserver("SOCKET_UPDATED",
				mSocketUpdatedObserver);
		mPrivateMyself = IMPrivateMyself.getInstance();
	}

	private void registerIMSDK() {
		IMCmdUserRegister cmd = new IMCmdUserRegister();

		if (IMActionLogin.getInstance() != this || isOver()) {
			return;
		}

		if (TextUtils.isEmpty(mCustomUserID)) {
			return;
		}

		if (TextUtils.isEmpty(mPassword)) {
			return;
		}

		cmd.mCustomUserID = mCustomUserID;
		cmd.mPassword = mPassword;

		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
			@Override
			public void onCommonFailed(FailedType type, long errorCode,
					JSONObject errorJsonObject) throws JSONException {
				if (IMActionLogin.getInstance() != IMActionLogin.this || isOver()) {
					return;
				}

				if (type == FailedType.RecvError) {
					if (100 == errorCode) {
						done("Wrong Password");
					} else {
						done(errorJsonObject != null ? errorJsonObject.toString()
								: "Error code:" + errorCode);
					}
				} else {
					IMSocket.getInstance().reconnect();
				}
			}
		};

		cmd.mOnRecvEndListener = new OnRecvEndListener() {
			@Override
			public void onRecvEnd(JSONObject jsonObject) throws JSONException {
				if (IMActionLogin.getInstance() != IMActionLogin.this || isOver()) {
					return;
				}

				mCmdJsonObjectResult = jsonObject;
				done();
			}
		};

		cmd.send();
	}

	private void loginIMSDK() {
		IMCmdUserLogin cmd = new IMCmdUserLogin();

		if (IMActionLogin.getInstance() != this || isOver()) {
			return;
		}

		if (TextUtils.isEmpty(mCustomUserID)) {
			return;
		}

		if (TextUtils.isEmpty(mPassword)) {
			return;
		}

		cmd.mPhoneNum = mCustomUserID;
		cmd.mPassword = mPassword;

		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
			@Override
			public void onCommonFailed(FailedType type, long errorCode,
					JSONObject errorJsonObject) throws JSONException {
				if (IMActionLogin.getInstance() != IMActionLogin.this || isOver()) {
					return;
				}

				if (type == FailedType.RecvError) {
					if (100 == errorCode) {
						done("Wrong Password");
					} else {
						done(errorJsonObject != null ? errorJsonObject.toString()
								: "Error code:" + errorCode);
					}
				} else {
					IMSocket.getInstance().reconnect();
				}
			}
		};

		cmd.mOnRecvEndListener = new OnRecvEndListener() {
			@Override
			public void onRecvEnd(JSONObject jsonObject) throws JSONException {
				if (IMActionLogin.getInstance() != IMActionLogin.this || isOver()) {
					return;
				}

				mCmdJsonObjectResult = jsonObject;
				done();
			}
		};

		cmd.send();
	}

	private void autoLoginOrReconnect() {
//		if (IMAppSettings.getInstance().mSetupID == 0) {
//			doneWithIMSDKError();
//			return;
//		}

		IMCmdUserLogin cmd = new IMCmdUserLogin();

		if (IMActionLogin.getInstance() != this || isOver()) {
			return;
		}

		cmd.mPhoneNum = mCustomUserID;
		cmd.mPassword = mPassword;
		cmd.mAutoLogin = true;
		cmd.mSetupID = IMAppSettings.getInstance().mSetupID;

		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
			@Override
			public void onCommonFailed(FailedType type, long errorCode,
					JSONObject errorJsonObject) throws JSONException {
				if (IMActionLogin.getInstance() != IMActionLogin.this || isOver()) {
					return;
				}

				if (type == FailedType.RecvError) {
					if (100 == errorCode) {
						done("Wrong Password");
					} else if (14 == errorCode) {
						done("Login Conflict");
					} else {
						done(errorJsonObject != null ? errorJsonObject.toString()
								: "Error code:" + errorCode);
					}
				} else {
					IMSocket.getInstance().reconnect();
				}
			}
		};

		cmd.mOnRecvEndListener = new OnRecvEndListener() {
			@Override
			public void onRecvEnd(JSONObject jsonObject) throws JSONException {
				if (IMActionLogin.getInstance() != IMActionLogin.this || isOver()) {
					return;
				}

				mCmdJsonObjectResult = jsonObject;
				done();
			}
		};

		cmd.send();
	}

	// private void pureLoginOrRegister(boolean login) {
	// IMCmdUserPure cmd = new IMCmdUserPure();
	//
	// if (IMActionLogin.getInstance() != this || isOver()) {
	// return;
	// }
	//
	// if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)) {
	// doneWithIMSDKError();
	// }
	//
	// cmd.mLogin = login;
	// cmd.mCustomUserID = mCustomUserID;
	// cmd.mPassword = mPassword;
	//
	// cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
	// @Override
	// public void onCommonFailed(FailedType type, long errorCode,
	// JSONObject errorJsonObject) throws JSONException {
	// if (IMActionLogin.getInstance() != IMActionLogin.this || isOver()) {
	// return;
	// }
	//
	// if (type == FailedType.RecvError) {
	// if (100 == errorCode) {
	// done("Wrong Password");
	// } else {
	// done(errorJsonObject != null ? errorJsonObject.toString()
	// : "Error code:" + errorCode);
	// }
	// } else {
	// IMSocket.getInstance().reconnect();
	// }
	// }
	// };
	//
	// cmd.mOnRecvEndListener = new OnRecvEndListener() {
	// @Override
	// public void onRecvEnd(JSONObject jsonObject) throws JSONException {
	// if (IMActionLogin.getInstance() != IMActionLogin.this || isOver()) {
	// return;
	// }
	//
	// mCmdJsonObjectResult = jsonObject;
	// done();
	// }
	// };
	//
	// cmd.send();
	// }

	@Override
	public void onActionStepBegan(final int stepNumber) {
		Log.e("Debug", "onActionStepBegan:" + stepNumber + "---" + IMActionLogin.this);
		switch (stepNumber) {
		case 1: {
			if (IMSocket.getInstance().getStatus() == DTSocketStatus.Connected) {
				Log.e("Debug", "start step 2");
				this.nextStep();
				return;
			}

			Log.e("Debug", "onActionStepBegan step = "
					+ IMSocket.getInstance().getStatus() + "--reconnect");
			IMSocket.getInstance().reconnect();
		}
			break;
		case 2: {
			if (IMSocket.getInstance().getStatus() != DTSocketStatus.Connected) {
				Log.e("Debug", "onActionStepBegan step = " + stepNumber + "---"
						+ IMSocket.getInstance().getStatus() + "--error");
				doneWithNetworkError();
				return;
			}

			Log.e("Debug", mType + "");

			switch (mType) {
			case OneKeyLogin: {
				if (mCustomUserID == null) {
					mCustomUserID = "";
				}

				if (mPassword == null) {
					mPassword = "";
				}

				// 登录
				loginIMSDK();
			}
				break;
			case OneKeyRegister: {
				if (mCustomUserID == null) {
					mCustomUserID = "";
				}

				if (mPassword == null) {
					mPassword = "";
				}

				// 注册
				registerIMSDK();
			}
				break;
			// case PureLogin: {
			// // 登录
			// pureLoginOrRegister(true);
			// }
			// break;
			// case PureRegister: {
			// // 注册
			// if (mUID != 0) {
			// done("CustomUserID Already Exist");
			// return;
			// }
			//
			// pureLoginOrRegister(false);
			// }
			// break;
			case AutoLogin:
			case Reconnect: {
				if (mUID == 0) {
					doneWithIMSDKError();
					return;
				}

				autoLoginOrReconnect();
			}
				break;
			default:
				DTLog.logError();
				break;
			}
		}
			break;
		default:
			DTLog.logError();
			break;
		}
	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {
		if (mCustomUserID != null && !mCustomUserID.equals(IMMyself.getCustomUserID())) {
			return;
		}

		if (IMActionLogin.getInstance() != this) {
			return;
		}

		if (mPrivateMyself != IMPrivateMyself.getInstance()) {
			return;
		}

		sSingleton = null;

		if (mType != IMActionLoginType.Reconnect
				&& mType != IMActionLoginType.AutoLogin) {
			IMPrivateMyself.getInstance().setTeamIDs(new ArrayList<Long>());
			IMPrivateMyself.getInstance().saveFile();
		}

		switch (mType) {
		case OneKeyLogin:
		case OneKeyRegister:
			if (IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.Logining) {
				DTLog.logError();
				return;
			}

			break;
		case AutoLogin:
			if (IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.AutoLogining) {
				DTLog.logError();
				return;
			}

			break;
		case Reconnect:
			if (IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.Reconnecting) {
				DTLog.logError();
				return;
			}

			break;
		default:
			DTLog.logError();
			return;
		}

		if (mCmdJsonObjectResult == null) {
			doneWithIMSDKError();
			return;
		}

		long uid = 0;
		long sid = 0;
		boolean autoCreateCustomUserID = false;

		try {
			uid = mCmdJsonObjectResult.getLong("uid");
			sid = mCmdJsonObjectResult.getLong("sid");

			if (uid == 0) {
				doneWithServerError();
				return;
			}

			if (sid == 0) {
				doneWithServerError();
				return;
			}

			IMSocket.getInstance().setUID(uid);
			IMSocket.getInstance().setSessionID(sid);

//			if (mCustomUserID == null || mCustomUserID.length() == 0) {
//				mCustomUserID = mCmdJsonObjectResult.getString("phonenum");
//
//				if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)) {
//					doneWithIMSDKError();
//					return;
//				}
//
//				IMMyself2.privateSetCustomUserID(mCustomUserID);
//				autoCreateCustomUserID = true;
//			}

			IMUsersMgr.getInstance().setCustomUserIDForUID(mCustomUserID, uid);
			IMUsersMgr.getInstance().saveFile();
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			DTLog.log(e.toString());
			this.done("IMSDK Server error");
			return;
		}
		
//		long uid = 0;
//		
//		try {
//			int state = mCmdJsonObjectResult.getInt("state");
//			
//			// 0-正常   400- 请求参数错误 500- 服务器端错误
//			if(state > 0) {
//				DTLog.logError();
//				return;
//			}
//			
//			uid = Long.parseLong(mCustomUserID);
//			
//			IMSocket.getInstance().setUID(uid);
//			IMSocket.getInstance().setSessionID(0);
//			
//			IMMyself2.privateSetCustomUserID(mCustomUserID);
//			
//			IMUsersMgr.getInstance().setCustomUserIDForUID(mCustomUserID, uid);
//			IMUsersMgr.getInstance().saveFile();
//		} catch (JSONException e) {
//			e.printStackTrace();
//			DTLog.logError();
//			DTLog.log(e.toString());
//			this.done("IMSDK Server error");
//			return;
//		}

		if (IMPrivateMyself.getInstance().getPassword().length() == 0) {
			IMPrivateMyself.getInstance().setPassword(mPassword);
		}

		if (mUID == 0) {
			if (uid == 0) {
				DTLog.logError();
				return;
			}

			IMUsersMgr.getInstance().setCustomUserIDForUID(mCustomUserID, uid);
			IMUsersMgr.getInstance().saveFile();

			if (mType != IMActionLoginType.AutoLogin
					&& mType != IMActionLoginType.Reconnect) {
				IMPrivateMyself.getInstance().reinit();
			}

			IMPrivateMyself.getInstance().setPassword(mPassword);
			IMPrivateMyself.getInstance().saveFile();

			if (IMPrivateMyself.getInstance().getUID() == 0) {
				DTLog.logError();
				return;
			}
		}

		// save login history
		// unfinished by lyc
		IMPrivateMyself.getInstance().setLoginStatus(IMMyself.LoginStatus.Logined);

		// init modules
		if (mType == IMActionLoginType.OneKeyLogin) {
			IMPrivateMyself.getInstance().onUserLoginInit();
		} else if (mType == IMActionLoginType.OneKeyRegister) {
			IMPrivateMyself.getInstance().onUserRegisterInit();
		} else {
			IMPrivateMyself.getInstance().onMachineLoginInit();
		}

		// setup id
		if (IMAppSettings.getInstance().mSetupID == 0) {
			IMCmdUserGetSetupID cmd = new IMCmdUserGetSetupID();

			cmd.send();
		} else {
			IMCmdUserSetSetupID cmd = new IMCmdUserSetSetupID();

			cmd.mSetupID = IMAppSettings.getInstance().mSetupID;
			cmd.send();
		}

		if (!mCustomUserID.equals(IMPrivateMyself.getInstance().getCustomUserID())) {
			DTLog.logError();
			return;
		}

		IMAppSettings.getInstance().mLastLoginCustomUserID = IMPrivateMyself
				.getInstance().getCustomUserID();
		IMAppSettings.getInstance().mLastLoginPassword = IMPrivateMyself.getInstance()
				.getPassword();

		if (mType == IMActionLoginType.OneKeyLogin) {
			if (IMAppSettings.getInstance().mOneKeyLoginCustomUserID != null
					&& IMAppSettings.getInstance().mOneKeyLoginCustomUserID.length() != 0
					&& IMAppSettings.getInstance().mOneKeyLoginCustomUserID
							.equals(mCustomUserID)) {
				if (IMAppSettings.getInstance().mOneKeyLoginPassword != null
						&& IMAppSettings.getInstance().mOneKeyLoginPassword.length() != 0
						&& !IMAppSettings.getInstance().mOneKeyLoginPassword
								.equals(mPassword)) {
					DTLog.logError();
				}
			}
//			else if (autoCreateCustomUserID) {
//				if (IMAppSettings.getInstance().mOneKeyLoginCustomUserID != null
//						&& IMAppSettings.getInstance().mOneKeyLoginCustomUserID
//								.length() > 0) {
//					DTLog.logError();
//				}
//
//				IMAppSettings.getInstance().mOneKeyLoginCustomUserID = IMPrivateMyself
//						.getInstance().getCustomUserID();
//				IMAppSettings.getInstance().mOneKeyLoginPassword = IMPrivateMyself
//						.getInstance().getPassword();
//			}
		}

		IMAppSettings.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification("IMActionLogin_done");

		if (IMAppSettings.getInstance().mLastLoginCustomUserID == null
				|| IMAppSettings.getInstance().mLastLoginCustomUserID.length() == 0) {
			DTLog.logError();
			return;
		}
	}

	@Override
	public void onActionFailed(String error) {
		if (IMActionLogin.getInstance() != this) {
			return;
		}

		sSingleton = null;

		switch (mType) {
		case OneKeyLogin:
		case OneKeyRegister:
		// 用户行为
		{
			IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.None);
			IMSocket.getInstance().checkCloseSocket();
			IMAppSettings.getInstance().mLastLoginCustomUserID = "";
			IMAppSettings.getInstance().saveFile();
			DTNotificationCenter.getInstance().postNotification(
					"IMActionUserLogin_failed", error);
		}
			break;
		case Reconnect: {
			if (error.equals("Wrong Password") || error.equals("Login Conflict")) {
				IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.None);
				IMSocket.getInstance().checkCloseSocket();
				IMAppSettings.getInstance().mLastLoginCustomUserID = "";
				IMAppSettings.getInstance().saveFile();
				DTNotificationCenter.getInstance().postNotification("LoginConflict",
						error);
			} else {
				IMSocket.getInstance().reconnect();
			}
		}
			break;
		case AutoLogin: {
			DTLog.sign("AutoLogin done");

			if (error.equals("Wrong Password") || error.equals("Login Conflict")) {
				IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.None);
				IMSocket.getInstance().checkCloseSocket();
				IMAppSettings.getInstance().mLastLoginCustomUserID = "";
				IMAppSettings.getInstance().saveFile();
				DTNotificationCenter.getInstance().postNotification(
						"IMActionLogin_failed",
						error.equals("Login Conflict") ? true : false);
				DTNotificationCenter.getInstance().postNotification("LoginConflict",
						error);
			} else {
				IMSocket.getInstance().reconnect();
				DTNotificationCenter.getInstance().postNotification(
						"IMActionLogin_failed", false);
			}

			IMActionLogin.newInstance();
		}
			break;
		default:
			break;
		}
	}

	public void setType(IMActionLoginType type) {
		mType = type;
		DTLog.sign("IMActionLoginType:" + type.toString());
	}

	public IMActionLoginType getType() {
		return mType;
	}

	public void setPassword(String password) {
		if (password == null || password.length() == 0) {
			DTLog.logError();
			return;
		}

		mPassword = password;
	}

	public enum IMActionLoginType {
		None(0),

		// 用户行为
		// PureLogin(1), PureRegister(2),

		// 用户行为
		OneKeyLogin(3), OneKeyRegister(4),

		// 非用户行为
		Reconnect(10), AutoLogin(11);

		private final int value;

		private IMActionLoginType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		private static final SparseArray<IMActionLoginType> sMapValues = new SparseArray<IMActionLoginType>();

		static {
			for (IMActionLoginType type : IMActionLoginType.values()) {
				sMapValues.put(type.value, type);
			}
		}

		public static IMActionLoginType fromInt(int i) {
			IMActionLoginType type = sMapValues.get(Integer.valueOf(i));

			if (type == null) {
				DTLog.logError();
				return null;
			}

			return type;
		}
	}

	// singleton
	private volatile static IMActionLogin sSingleton;

	public static IMActionLogin getInstance() {
		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
//		printCallStatck();
		synchronized (IMActionLogin.class) {
			if (sSingleton != null && sSingleton.mSocketUpdatedObserver != null) {
				DTNotificationCenter.getInstance().removeObserver("SOCKET_UPDATED",
						sSingleton.mSocketUpdatedObserver);
			}

			sSingleton = new IMActionLogin();
		}
	}
	
	public static void printCallStatck() {
        Throwable ex = new Throwable();
        StackTraceElement[] stackElements = ex.getStackTrace();
        if (stackElements != null) {
            for (int i = 0; i < stackElements.length; i++) {
                System.out.print(stackElements[i].getClassName()+"--");
                System.out.print(stackElements[i].getFileName()+"--");
                System.out.print(stackElements[i].getLineNumber()+"--");
                System.out.println(stackElements[i].getMethodName());
            }
        }
        System.out.println("**********************************");
    }
}
