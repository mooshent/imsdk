package am.imsdk.action.group;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.team.IMCmdTeamGetMembers;
import am.imsdk.aacmd.user.IMCmdUserGetInfo;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import am.imsdk.model.userinfo.IMUsersMgr;

// 1. 请求用户列表
// 2. 请求customUserID
public final class IMActionUserRequestUpdateMemberList extends IMAction {
	public String mGroupID;
	private long mTeamID;
	private ArrayList<Long> mAryUIDs = new ArrayList<Long>();

	public IMActionUserRequestUpdateMemberList() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (!IMParamJudge.isGroupIDLegal(mGroupID)) {
			doneWithIMSDKError();
			return;
		}

		mTeamID = DTTool.getUnsecretLongValue(mGroupID);

		if (mTeamID == 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			final IMCmdTeamGetMembers cmd = new IMCmdTeamGetMembers();

			cmd.mTeamID = mTeamID;

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					JSONArray jsonArray = jsonObject.getJSONArray("members");
					mAryUIDs.clear();

					for (int i = 0; i < jsonArray.length(); i++) {
						long uid = jsonArray.getLong(i);

						if (uid == 0) {
							DTLog.logError();
							return;
						}

						mAryUIDs.add(uid);
					}

					nextStep();
				}
			};

			cmd.send();
		}
			break;
		case 2: {
			IMCmdUserGetInfo cmd = new IMCmdUserGetInfo();
			boolean needSend = false;

			for (long uid : mAryUIDs) {
				String customUserID = IMUsersMgr.getInstance().getCustomUserID(uid);

				if (customUserID.length() > 0) {
					continue;
				}

				cmd.addUID(uid);
				needSend = true;
			}

			if (!needSend) {
				nextStep();
				return;
			}

			cmd.addProperty("phonenum");

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					nextStep();
				}
			};

			cmd.send();
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {
		IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(mTeamID);

		teamInfo.clear();
		teamInfo.setUIDList(mAryUIDs);
		teamInfo.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				"GroupMemberUpdated:" + mTeamID);
	}

	@Override
	public void onActionFailed(String error) {
	}
}
