package am.imsdk.action.group;

import imsdk.data.group.IMGroupInfo;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.aacmd.team.IMCmdTeamCreate;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo.TeamType;
import am.imsdk.model.teaminfo.IMTeamsMgr;

// 1. 创建Group
public final class IMActionGroupCreate extends IMAction {
	public String mGroupName = "";
	public long mTeamID;

	private IMActionGroupCreate() {
		mStepCount = 1;
	}

	@Override
	public void onActionBegan() {
		if (!IMParamJudge.isGroupNameLegal(mGroupName)) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			IMCmdTeamCreate cmd = new IMCmdTeamCreate();

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					if (type == FailedType.RecvError) {
						if (errorCode == 5) {
							DTLog.logError();
						}
					}

					commonFailedDealWithJudge(type, errorCode, errorJsonObject);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					mTeamID = jsonObject.getLong("teamid");

					if (!IMParamJudge.isTeamIDLegal(mTeamID)) {
						doneWithServerError();
						return;
					}

					IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(
							mTeamID);

					if (teamInfo.mTeamType != TeamType.Group) {
						DTLog.logError();
						return;
					}

					if (teamInfo.getUIDList().size() == 0) {
						DTLog.logError();
						return;
					}

					if (teamInfo.getUID() == 0) {
						DTLog.logError();
						return;
					}

					DTNotificationCenter.getInstance().postNotification(
							"TeamUpdated:" + teamInfo.mTeamID);

					IMGroupInfo groupInfo = IMTeamsMgr.getInstance().getGroupInfo(
							mTeamID);

					if (groupInfo == null) {
						DTLog.logError();
						return;
					}

					if (groupInfo.getMemberList().size() == 0) {
						DTLog.logError();
						return;
					}

					done();
				}
			};

			cmd.mTeamType = TeamType.Group;
			cmd.mTeamName = mGroupName;
			cmd.send();
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActionDone() {
		sSingleton = null;
		super.onActionDone();
	}

	@Override
	public void onActionFailed(String error) {
		sSingleton = null;
		super.onActionFailed(error);
	}

	// singleton
	private static IMActionGroupCreate sSingleton;

	public static IMActionGroupCreate getInstance() {
		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
		if (sSingleton != null) {
			DTLog.logError();
			return;
		}

		synchronized (IMActionGroupCreate.class) {
			sSingleton = new IMActionGroupCreate();
		}
	}
}
