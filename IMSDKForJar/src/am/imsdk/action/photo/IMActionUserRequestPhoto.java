package am.imsdk.action.photo;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.action.IMAction;
import am.imsdk.action.fileserver.IMActionDownloadImage;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.userinfo.IMPrivateUserInfo;
import am.imsdk.model.userinfo.IMUsersMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;
import android.graphics.Bitmap;

// 1. 获取uid
// 2. 更新baseInfo
// 3. 判断本地图片 -> 下载图片
public final class IMActionUserRequestPhoto extends IMAction {
	public String mCustomUserID = "";
	public long mUID = 0;
	public Bitmap mBitmap = null;
	public byte[] mBuffer = null;

	public IMActionUserRequestPhoto() {
		mStepCount = 3;
	}

	@Override
	public void onActionBegan() {
		if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			checkBeginGetUID(mCustomUserID);
		}
			break;
		case 2: {
			if (mUID == 0) {
				mUID = IMUsersMgr.getInstance().getUID(mCustomUserID);
			}

			if (mUID == 0) {
				doneWithIMSDKError();
				return;
			}

			beginGetBaseInfo(mUID);
		}
			break;
		case 3: {
			if (mUID == 0) {
				doneWithIMSDKError();
				return;
			}

			IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(mUID);
			String mainPhotoFileID = userInfo.getMainPhotoFileID();

			DTLog.sign("mainPhotoFileID:" + mainPhotoFileID);

			if (mainPhotoFileID.length() == 0) {
				done();
				return;
			}

			// 获取本地数据
			IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(mainPhotoFileID);

			// 存在的话就不下载了
			if (photo.getBitmap() != null) {
				mBitmap = photo.getBitmap();
				mBuffer = photo.getBuffer();
				done();
				return;
			}

			final IMActionDownloadImage action = new IMActionDownloadImage();

			action.mOnActionFailedListener = new OnActionFailedListener() {
				@Override
				public void onActionFailed(String error) {
					IMActionUserRequestPhoto.this.done(error);
				}
			};

			action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
				@Override
				public void onActionPartiallyDone(double percentage) {
					IMActionUserRequestPhoto.this.done(percentage);
				}
			};

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					mBitmap = action.mBitmap;
					mBuffer = action.mBuffer;
					done();
				}
			};

			action.mFileID = mainPhotoFileID;
			action.begin();
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {
	}

	@Override
	public void onActionFailed(String error) {
	}
}
