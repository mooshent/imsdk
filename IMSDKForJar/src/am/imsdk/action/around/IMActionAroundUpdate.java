package am.imsdk.action.around;

import imsdk.data.IMMyself.LoginStatus;
import imsdk.data.around.IMMyselfAround;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.imsdk.aacmd.around.IMCmdAroundGet;
import am.imsdk.aacmd.user.IMCmdUserGetInfo;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMPrivateAroundUsers;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.userinfo.IMPrivateUserInfo;
import am.imsdk.model.userinfo.IMUsersMgr;
import android.content.ContentResolver;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;

// 1. 获取经纬度
// 2. 获取周边用户
// 3. 获取cid
public final class IMActionAroundUpdate extends IMAction {
	private LocationManager mLocationManager;
	private Location mLocation;
	private ArrayList<Long> mAryUIDs = new ArrayList<Long>();
	private int mStep3TryTimes;

	public IMActionAroundUpdate() {
		mStepCount = 3;
	}

	@Override
	public void onActionBegan() {
		if (IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.Logined) {
			doneWithIMSDKError();
			return;
		}

		if (DTAppEnv.getContext() == null) {
			doneWithIMSDKError();
			return;
		}

		mLocationManager = (LocationManager) DTAppEnv.getContext().getSystemService(
				Context.LOCATION_SERVICE);

		if (mLocationManager == null) {
			doneWithIMSDKError();
			return;
		}

		IMPrivateAroundUsers.getInstance().mState = IMMyselfAround.State.Updating;
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			Criteria criteria = new Criteria();

			criteria.setAccuracy(Criteria.ACCURACY_FINE);
			criteria.setAltitudeRequired(false);
			criteria.setCostAllowed(true);
			criteria.setPowerRequirement(Criteria.POWER_HIGH);
			criteria.setBearingRequired(false);
			criteria.setSpeedRequired(false);

			String provider = mLocationManager.getBestProvider(criteria, true);

			if (provider == null) {
				//有部分低端手机不存在gps_provider、network_provider的情况
				List<String> providers = mLocationManager.getAllProviders();

				if (isGPSOpen(DTAppEnv.getContext()) && providers.contains(LocationManager.GPS_PROVIDER)) {
					provider = LocationManager.GPS_PROVIDER;
					
				}else if(providers.contains(LocationManager.NETWORK_PROVIDER)){
					provider = LocationManager.NETWORK_PROVIDER;
					
				}else if(providers != null && providers.size() > 0){
					provider = providers.get(0);
					
				}
			}
			DTLog.sign("provider="+provider);
			
			mLocation = mLocationManager.getLastKnownLocation(provider);

			final LocationListener locationListener = new LocationListener() {
				@Override
				public void onStatusChanged(String provider, int status, Bundle extras) {
					DTLog.sign("onStatusChanged");
				}

				@Override
				public void onProviderEnabled(String provider) {
					DTLog.sign("onProviderEnabled");
				}

				@Override
				public void onProviderDisabled(String provider) {
					DTLog.sign("onProviderDisabled");
				}

				@Override
				public void onLocationChanged(Location location) {
					DTLog.sign("onLocationChanged");
					mLocationManager.removeUpdates(this);
					mLocation = location;

					if (getCurrentStep() == 1) {
						nextStep();
					}
				}
			};

			// mLocationManager.requestLocationUpdates(
			// LocationManager.GPS_PROVIDER, 1000 * 60,
			// 10, locationListener);
			
			if (!provider.equals(LocationManager.NETWORK_PROVIDER)) {
				mLocationManager.requestLocationUpdates(provider, 2000, 0, locationListener);
			}else{
				mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
						3000, 0, locationListener);
			}

			DTAppEnv.performAfterDelayOnUIThread(10, new Runnable() {
				@Override
				public void run() {
					if (getCurrentStep() == 1) {
						mLocationManager.removeUpdates(locationListener);
						nextStep();
					}
				}
			});
		}
			break;
		case 2: {
			DTLog.sign("Step2");

			if (mLocation == null) {
				mLocation = mLocationManager
						.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			}

			if (mLocation == null) {
				// mLocationManager.setTestProviderEnabled(LocationManager.GPS_PROVIDER,
				// true);
				mLocation = mLocationManager
						.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				// mLocationManager.setTestProviderEnabled(LocationManager.GPS_PROVIDER,
				// false);
			}

			if (mLocation == null) {
				// mLocationManager.setTestProviderEnabled("gps", true);
				mLocation = mLocationManager
						.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				// mLocationManager.setTestProviderEnabled("gps", false);
			}

			if (mLocation == null) {
				mLocation = mLocationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			}

			if (mLocation == null) {
				List<String> providers = mLocationManager.getProviders(true);

				for (String provider : providers) {
					Location l = mLocationManager.getLastKnownLocation(provider);

					if (l == null) {
						continue;
					}

					if (mLocation == null || l.getAccuracy() < mLocation.getAccuracy()) {
						mLocation = l;
						break;
					}
				}
			}

			if (mLocation == null) {
				done("无法获取经纬度信息");
				return;
			}

			IMCmdAroundGet cmd = new IMCmdAroundGet();

			cmd.mLongitude = mLocation.getLongitude();
			cmd.mLatitude = mLocation.getLatitude();
			cmd.mPage = 0;

			cmd.mOnCommonFailedListener = new DTCmd.OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new DTCmd.OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					parseCmdResult(jsonObject);
					nextStep();
				}
			};

			cmd.send();
		}
			break;
		case 3: {
			mStep3TryTimes++;

			if (mStep3TryTimes > 3) {
				doneWithIMSDKError();
				return;
			}

			IMCmdUserGetInfo cmd = new IMCmdUserGetInfo();

			cmd.addProperty("phonenum");

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					if (isOver()) {
						return;
					}

					beginStep(3);
				}
			};

			boolean needSend = false;

			for (long uid : mAryUIDs) {
				if (uid == 0) {
					DTLog.logError();
					continue;
				}

				String customUserID = IMUsersMgr.getInstance().getCustomUserID(uid);

				if (customUserID.length() == 0) {
					cmd.addUID(uid);
					needSend = true;
				}
			}

			if (needSend) {
				cmd.send();
			} else {
				done();
			}
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {
		IMPrivateAroundUsers.getInstance().clear();

		for (long uid : mAryUIDs) {
			IMPrivateAroundUsers.getInstance().add(uid);
		}

		IMPrivateAroundUsers.getInstance().mState = IMMyselfAround.State.Normal;
		IMPrivateAroundUsers.getInstance().saveFile();
	}

	@Override
	public void onActionFailed(String error) {
	}

	private void parseCmdResult(JSONObject jsonObject) throws JSONException {
		if (mAryUIDs.size() != 0) {
			doneWithIMSDKError();
			return;
		}

		JSONArray jsonArray = jsonObject.getJSONArray("userlocations");

		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject userLocationJsonObject = (JSONObject) jsonArray.get(i);

			long uid = userLocationJsonObject.getLong("uid");

			if (uid == 0) {
				doneWithServerError();
				return;
			}

			IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(uid);

			userInfo.mLongitude = userLocationJsonObject.getDouble("xpos");
			userInfo.mLatitude = userLocationJsonObject.getDouble("ypos");

			userInfo.saveFile();

			if (uid == IMPrivateMyself.getInstance().getUID()) {
				mAryUIDs.add(0, uid);
			} else {
				mAryUIDs.add(uid);
			}
			
			mStep3TryTimes = 0;
		}
	}

	/*
	 * 获取GPS开关状态״̬
	 */
	private boolean isGPSOpen(Context context) {
		ContentResolver resolver = context.getContentResolver();
		boolean open = Settings.Secure.isLocationProviderEnabled(resolver,
				LocationManager.GPS_PROVIDER);

		return open;
	}



}
