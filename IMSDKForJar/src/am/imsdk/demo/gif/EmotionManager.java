package am.imsdk.demo.gif;

import imsdk.views.IMChatView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import am.imsdk.demo.util.CPResourceUtil;
import am.imsdk.demo.util.FacesXml;
import am.imsdk.demo.util.FileUtils;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class EmotionManager implements View.OnClickListener {
	private WeakReference<Activity> mActivityReference;
	private ViewPager mViewPager;
	private LinearLayout mPagerIndexPanel;
	private NativePagerAdapter mAdapter;
	private SparseArray<ViewHolder> mClassicalPages;
	private NativeOnPageChangeListener mPageChangeListener;
	private ArrayList<ImageView> mClassicalPointerImageViews; // 底部圆点指示器

	private Handler mHandler;
	private int mCurrrentClassicalEmotionPage = 0;

	public EmotionManager(Activity activity, ViewPager viewPager, View pagerIndex,
			Handler handler) {
		mActivityReference = new WeakReference<Activity>(activity);
		mViewPager = viewPager;
		mPagerIndexPanel = (LinearLayout) pagerIndex;
		mClassicalPages = new SparseArray<EmotionManager.ViewHolder>();
		mAdapter = new NativePagerAdapter();
		mPageChangeListener = new NativeOnPageChangeListener();
		mHandler = handler;

		// 初始化 表情的键值对
		if (!initializeEmoji()) {
			(mActivityReference.get()).finish();
			return;
		}

		// 初始化底部圆点指示器
		initializeClassicalEmoji();
	}

	private static LinkedHashMap<String, String> sMapFaceShortNames;
	private static LinkedHashMap<Integer, String> sMapFaceNames;

	public boolean initializeEmoji() {
		if (FacesXml.hasParse()) {
			sMapFaceShortNames = FacesXml.getFacesMap();
			sMapFaceNames = FacesXml.getFacesDes();
			return true;
		}

		if (sMapFaceShortNames != null && sMapFaceShortNames.size() > 0) {
			return true;
		}

		File file = new File(FileUtils.getGifStorePath(), "face.xml");

		if (!file.exists()) {
			mActivityReference.get()
					.getSharedPreferences(FacesXml.key, Context.MODE_PRIVATE).edit()
					.putBoolean(FacesXml.key, false).commit();
			FacesXml.downLoadGif(mActivityReference.get());

			return false;
		}

		FileInputStream fis = null;

		try {
			fis = new FileInputStream(file);

			if (FacesXml.startParse(new FileInputStream(file))) {
				sMapFaceShortNames = FacesXml.getFacesMap();
				sMapFaceNames = FacesXml.getFacesDes();
			}

			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void initializeClassicalEmoji() {
		mClassicalPointerImageViews = new ArrayList<ImageView>();

		int totalpages = (int) Math.ceil(sMapFaceShortNames.size() / 21.0F);

		for (int i = 0; i < totalpages; i++) {
			ImageView imageView = new ImageView(mActivityReference.get());

			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.leftMargin = 5;
			imageView.setLayoutParams(params);
			mClassicalPointerImageViews.add(imageView);
		}
	}

	/**
	 * 初始化底部ViewPage
	 */
	public void initialize() {
		mViewPager.setAdapter(mAdapter);
		ArrayList<ImageView> imageViews = null;

		imageViews = mClassicalPointerImageViews;

		for (ImageView imageView : imageViews) {
			imageView.setImageResource(CPResourceUtil.getDrawableId(
					mActivityReference.get(), "im_dot_unselect"));
			// imageView.setImageDrawable(mContext.getResources().getDrawable(
			// R.drawable.dot_unselect));
			mPagerIndexPanel.addView(imageView);
		}

		mViewPager.setOnPageChangeListener(mPageChangeListener);

		int page = 0;

		page = mCurrrentClassicalEmotionPage;

		ImageView imageView = imageViews.get(page);

		imageView.setImageResource(CPResourceUtil.getDrawableId(
				mActivityReference.get(), "im_dot_select"));

		// imageView.setImageDrawable(mContext.getResources().getDrawable(
		// R.drawable.dot_select));
		mViewPager.setCurrentItem(page);
	}

	public void destory() {
		mViewPager.setAdapter(null);
		mViewPager.removeAllViews();
		mPagerIndexPanel.removeAllViews();
		mViewPager.setOnPageChangeListener(null);
	}

	private ViewHolder instanceView(int position) {
		ViewHolder viewHolder = new ViewHolder();

		View rootView = null;
		int base_index = 0;
		int count = 21;

		base_index = position * count;

		rootView = LayoutInflater.from(mActivityReference.get()).inflate(
				CPResourceUtil.getLayoutId(mActivityReference.get(),
						"im_emotion_pager_classical_item"), null);

		viewHolder.emotionViews = new ArrayList<ImageButton>();

		for (int i = 0; i < count; i++) {
			ImageButton view = (ImageButton) rootView.findViewById(mActivityReference
					.get()
					.getResources()
					.getIdentifier("emo_" + (i + 1), "id",
							mActivityReference.get().getPackageName()));

			view.setOnClickListener(this);
			view.setTag(String.valueOf(base_index + i));
			viewHolder.emotionViews.add(view);
		}

		viewHolder.rootView = rootView;
		return viewHolder;
	}

	private class NativePagerAdapter extends PagerAdapter {
		@Override
		public int getCount() {
			if (sMapFaceShortNames.size() % 21 > 0) {
				return sMapFaceShortNames.size() / 21 + 1;
			}

			return sMapFaceShortNames.size() / 21;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public void destroyItem(View container, int position, Object object) {
			SparseArray<ViewHolder> pages = mClassicalPages;
			ViewHolder viewHolder = pages.get(Integer.valueOf(position));

			if (viewHolder != null) {
				((ViewPager) container).removeView(viewHolder.rootView);
			}
		}

		@Override
		public Object instantiateItem(View container, int position) {
			ViewHolder viewHolder = null;

			viewHolder = mClassicalPages.get(Integer.valueOf(position));

			if (viewHolder == null) {
				viewHolder = instanceView(position);
				mClassicalPages.put(Integer.valueOf(position), viewHolder);
			}

			int offset = position * 21;

			if ((FacesXml.face_StaticBitmaps.size() < FacesXml.REAL_IMAGENUM)) {
				parseGif(offset, (position + 1) * 21, viewHolder);
			} else {
				// L.e("======= parsed  start ="+offset+ "  end = "+ ((position
				// + 1) * 21));
				for (int i = offset; i < sMapFaceShortNames.size()
						&& i < (position + 1) * 21; i++) {
					ImageButton button = viewHolder.emotionViews.get(i - offset);
					String gifName = sMapFaceShortNames.get(sMapFaceNames.get(i));

					button.setImageBitmap(FacesXml.face_StaticBitmaps.get(gifName));
				}
			}

			((ViewPager) container).addView(viewHolder.rootView);
			return viewHolder.rootView;
		}
	}

	private synchronized void parseGif(int start, int end, ViewHolder viewHolder) {
		if (FacesXml.face_StaticBitmaps.size() >= FacesXml.REAL_IMAGENUM) {
			if (viewHolder != null) {
				for (int i = start; i < end; i++) {
					ImageButton button = viewHolder.emotionViews.get(i - start);
					String gifName = sMapFaceShortNames.get(sMapFaceNames.get(i));
					Bitmap bd = FacesXml.face_StaticBitmaps.get(gifName);

					button.setImageBitmap(bd);
				}
			}

			// L.e("======= return  start ="+start+ "  end = "+ end);
			return;
		}

		for (int i = start; i < end; i++) {
			String gifName = sMapFaceShortNames.get(sMapFaceNames.get(i));
			Bitmap bitmap = FacesXml.face_StaticBitmaps.get(gifName);

			if (bitmap != null) {
				if (viewHolder != null) {
					ImageButton button = viewHolder.emotionViews.get(i - start);
					button.setImageBitmap(bitmap);
				}

				continue;
			}

			FileInputStream fis = null;

			try {
				fis = new FileInputStream(
						new File(FileUtils.getGifStorePath(), gifName));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			GifOpenHelper gifOpenHelper = new GifOpenHelper();
			gifOpenHelper.setIsOnlyGetFirstFrame(true);
			int status = gifOpenHelper.read(fis);

			if (status == GifOpenHelper.STATUS_OK) {

				bitmap = gifOpenHelper.getImage();
				if (viewHolder != null) {
					ImageButton button = viewHolder.emotionViews.get(i - start);
					button.setImageBitmap(bitmap);
				}
				if (FacesXml.faceH == 0) {
					FacesXml.faceH = bitmap.getHeight();
					FacesXml.faceW = bitmap.getWidth();
				}
				FacesXml.face_StaticBitmaps.put(gifName, bitmap);

			} else {
				try {
					bitmap = FacesXml.getBitmap(gifName);
					if (viewHolder != null) {
						ImageButton button = viewHolder.emotionViews.get(i - start);
						button.setImageBitmap(bitmap);
					}
					FacesXml.face_StaticBitmaps.put(gifName, bitmap);
				} catch (Exception e) {
				}
			}

		}

	}

	public class NativeOnPageChangeListener implements OnPageChangeListener {
		@Override
		public void onPageScrollStateChanged(int arg0) {
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageSelected(int arg0) {
			ImageView nowImageView = null;
			ImageView beforeImageView = null;
			ImageView afterImageView = null;

			if (arg0 < 0 || arg0 > mClassicalPointerImageViews.size() - 1) {
				return;
			}

			mCurrrentClassicalEmotionPage = arg0;
			nowImageView = mClassicalPointerImageViews.get(arg0);
			beforeImageView = arg0 > 0 ? mClassicalPointerImageViews.get(arg0 - 1)
					: null;
			afterImageView = arg0 < (mClassicalPointerImageViews.size() - 1) ? mClassicalPointerImageViews
					.get(arg0 + 1) : null;

			nowImageView.setImageResource(CPResourceUtil.getDrawableId(
					mActivityReference.get(), "im_dot_select"));

			if (beforeImageView != null) {
				beforeImageView.setImageResource(CPResourceUtil.getDrawableId(
						mActivityReference.get(), "im_dot_unselect"));
			}

			if (afterImageView != null) {
				afterImageView.setImageResource(CPResourceUtil.getDrawableId(
						mActivityReference.get(), "im_dot_unselect"));
			}
		}
	}

	private class ViewHolder {
		View rootView;
		ArrayList<ImageButton> emotionViews;
	}

	@Override
	public void onClick(View v) {
		String tag = (String) v.getTag();
		if (tag != null) {
			try {
				int index = Integer.parseInt(tag);

				// 发送经典头像
				Message message = mHandler
						.obtainMessage(IMChatView.MSG_SEND_CLASSICAL_EMOTION,
								sMapFaceNames.get(index));

				message.sendToTarget();
			} catch (Exception e) {
			}
		}
	}
}
