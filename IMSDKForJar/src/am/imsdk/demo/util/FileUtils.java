package am.imsdk.demo.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;

public class FileUtils {
	private static FileUtils sFileUtils = null;
	private String mRootDir = null;

	public static FileUtils getInstance() {
		if (sFileUtils == null) {
			sFileUtils = new FileUtils();
		}

		return sFileUtils;
	}

	private FileUtils() {
		mRootDir = FileUtils.getStorePath() + "im/";
	}

	public String getStoreRootPath() {
		return mRootDir;
	}

	public static void choosePhoto(Activity activty, int requestCode) {
//		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//
//		intent.addCategory(Intent.CATEGORY_OPENABLE);
//		intent.setType("image/*");
//		intent.putExtra("return-data", true);
//		activty.startActivityForResult(intent, requestCode);
		
		Intent intent = new Intent(
		                    Intent.ACTION_PICK,
		                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		activty.startActivityForResult(intent, requestCode);
	}

	public static void takePhoto(Activity activity, Uri imageUri, int requestCode) {
		if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			try {
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); // 调用系统相机
				intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
				// 直接使用，没有缩小
				activity.startActivityForResult(intent, requestCode); // 用户点击了从相机获取
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Toast.makeText(activity, "请确认已经插入SD卡", Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * 生成拍照缓存文件
	 */
	public static Uri generateImageUri() {
		File dir = new File(FileUtils.getStorePath() + "im/dcim");

		if (!dir.exists()) {
			dir.mkdirs();
		}

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS",
				Locale.getDefault());
		File file = new File(dir.getAbsolutePath() + "/"
				+ format.format(Calendar.getInstance().getTime()) + ".jpg");

		return Uri.fromFile(file);
	}

	public static String getPath(Context context, Uri uri) {
		ContentResolver cr = context.getContentResolver();
		String strPath = "";
		Cursor c = null;

		try {
			if ("content".equalsIgnoreCase(uri.getScheme())) {
				c = cr.query(uri, null, null,
						null, null);
				if (c != null && c.getCount() > 0) {
					if (c.moveToFirst()) {
						strPath = c.getString((c
								.getColumnIndex(MediaStore.Images.Media.DATA)));
					}
				}
			} else {
				strPath = uri.toString();
			}

			if (strPath.length() != 0) {
				if (strPath.startsWith("file:")) {
					strPath = strPath.replaceFirst("file:", "");
				}
			}
		} catch (Exception ex) {

		} finally {
			if (c != null) {
				c.close();
				c = null;
			}
		}

		return strPath;
	}

	public static final String FILE_SUFFIX = ".ik";

	private static String mStorePath = null;
	private static String mGifStorePath = null;
	private static Context mContext = null;

	public static void register(Context context) {
		mContext = context;
	}

	public static void unRegister() {
		mContext = null;
	}

	public static String getStorePath() {
		if (mStorePath == null) {
			String path = Environment.getExternalStorageDirectory().getAbsolutePath();

			if (path == null
					|| !Environment.getExternalStorageState().equals(
							Environment.MEDIA_MOUNTED)) {
				path = mContext.getFilesDir().getPath();
			}

			if (!path.endsWith(File.separator)) {
				path = path + File.separator;
			}

			mStorePath = path;
		}

		return mStorePath;
	}

	public static String getGifStorePath() {
		if (mGifStorePath == null) {
			if (mStorePath == null) {
				getStorePath();
			}

			mGifStorePath = mStorePath + "imGif" + File.separator;
		}

		return mGifStorePath;
	}

	public static void deleteFile(File file) {
		if (file.exists()) {
			if (file.isDirectory()) {
				File[] files = file.listFiles();
				for (File file2 : files) {
					if (file2.isDirectory()) {
						deleteFile(file2);
					} else {
						file2.deleteOnExit();
					}
				}
			}

			file.deleteOnExit();
		}
	}
	
	/**  
     * 复制单个文件  
     * @param oldPath String 原文件路径 如：c:/fqf.txt  
     * @param newPath String 复制后路径 如：f:/fqf.txt  
	 * @return 
     * @return boolean  
     */   
   public  static boolean copyFile(String oldPath, String newPath) {
	   boolean result = false;
       try {   
           int bytesum = 0;   
           int byteread = 0;   
           File oldfile = new File(oldPath);   
           if (oldfile.exists()) { //文件存在时   
               InputStream inStream = new FileInputStream(oldPath); //读入原文件   
               File file = new File(newPath);
               (new File(file.getParent())).mkdirs(); //如果文件夹不存在 则建立新文件夹 
               file.createNewFile();
               FileOutputStream fs = new FileOutputStream(file);   
               byte[] buffer = new byte[1444];   
               while ( (byteread = inStream.read(buffer)) != -1) {   
                   bytesum += byteread; //字节数 文件大小   
                   System.out.println(bytesum);   
                   fs.write(buffer, 0, byteread);   
               }   
               inStream.close();   
               fs.flush();
               fs.close();
               result = true;
           }   
       }   
       catch (Exception e) {   
           System.out.println("复制单个文件操作出错");   
           e.printStackTrace();   
  
       }   
       return result;
   }   
}
