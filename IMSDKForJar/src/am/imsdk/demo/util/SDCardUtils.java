package am.imsdk.demo.util;

import java.io.File;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.StatFs;

public class SDCardUtils
{
	private SDCardUtils()
	{
		/* cannot be instantiated */
		throw new UnsupportedOperationException("cannot be instantiated");
	}

	/**
	 * 判断SDCard是否可用
	 * 
	 * @return
	 */
	public static boolean isSDCardEnable()
	{
		return Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED);

	}

	/**
	 * 获取SD卡路径
	 * 
	 * @return
	 */
	public static String getSDCardPath()
	{
		if (isSDCardEnable()) {
			if (Environment.isExternalStorageRemovable()) {
				return Environment.getExternalStorageDirectory().getAbsolutePath();// 文件保存目录
			} else {
				return getExternalMemoryPath();
			}
		}else {
			return null;
		}
	}
	
	public static String getSavePath(Context context)
	{
		if (isSDCardEnable()) {
			if (Environment.isExternalStorageRemovable()) {
				return Environment.getExternalStorageDirectory().getAbsolutePath();// 文件保存目录
			} else {
				return getExternalMemoryPath();
			}
		}else {
			return context.getFilesDir().getAbsolutePath();
		}
	}
	

	/**
	 * 获取SD卡的剩余容量 单位byte
	 * 
	 * @return
	 */
	public static long getSDCardAllSize()
	{
		if (isSDCardEnable())
		{
			StatFs stat = new StatFs(getSDCardPath());
			// 获取空闲的数据块的数量
			long availableBlocks = (long) stat.getAvailableBlocks() - 4;
			// 获取单个数据块的大小（byte）
			long freeBlocks = stat.getAvailableBlocks();
			return freeBlocks * availableBlocks;
		}
		return 0;
	}

	/**
	 * 获取指定路径所在空间的剩余可用容量字节数，单位byte
	 * 
	 * @param filePath
	 * @return 容量字节 SDCard可用空间，内部存储可用空间
	 */
	public static long getFreeBytes(String filePath)
	{
		// 如果是sd卡的下的路径，则获取sd卡可用容量
		if (filePath.startsWith(getSDCardPath()))
		{
			filePath = getSDCardPath();
		} else
		{// 如果是内部存储的路径，则获取内存存储的可用容量
			filePath = Environment.getDataDirectory().getAbsolutePath();
		}
		StatFs stat = new StatFs(filePath);
		long availableBlocks = (long) stat.getAvailableBlocks() - 4;
		return stat.getBlockSize() * availableBlocks;
	}

	/**
	 * 获取系统存储路径
	 * 
	 * @return
	 */
	public static String getRootDirectoryPath()
	{
		return Environment.getRootDirectory().getAbsolutePath();
	}

	/**
	 * 
	 * @return 内置sd卡路径
	 */
	private static String getExternalMemoryPath() {
		if (VERSION.SDK_INT > 16) {
			return File.separator + "storage" + File.separator + "sdcard0";
		} else {
			// ROM路径为/data
			// 内置sd卡或sd卡1的路径为/mnt/sdcard,在4.1以后也可用路径/storage/sdcard0
			// 外置sd卡或sd卡2的路径为/mnt/sdcard,在4.1以后也可用路径/storage/sdcard1
			return File.separator + "mnt" + File.separator + "sdcard";
		}
	}
	
	

}
