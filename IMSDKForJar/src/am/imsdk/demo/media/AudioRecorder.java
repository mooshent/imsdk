package am.imsdk.demo.media;

import java.io.IOException;

import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.audio.IMAudiosMgr;
import android.media.MediaRecorder;
import android.os.Build;

public class AudioRecorder {
	public static String mStorePath = "imVoice/";

	// 录制后的文件名
	public MediaRecorder mMediaRecorder;
	private String localPath;

	@SuppressWarnings("deprecation")
	public synchronized void startRecorder(String fileName) throws Exception {
		byte[] buffer = null;
		IMAudio audio = IMAudiosMgr.getInstance().getAudio(buffer);
		localPath = audio.getLocalFullPath();

		// RECORDER_FILE = fileName + ".ik";

		if (mMediaRecorder == null) {
			try {
				mMediaRecorder = new MediaRecorder();

				// 设置音频录入源
				mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);

				// 设置录制音频的输出格式
				if (Build.VERSION.SDK_INT > 16) {
					mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS);
				} else {
					mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.RAW_AMR);
				}

				mMediaRecorder.setOutputFile(localPath);
				mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception("err_unknown_startRecorder");
			}
		}

		try {
			mMediaRecorder.prepare();
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception("err_unknown_prepare");
		}

		mMediaRecorder.start();
	}

	public synchronized void release() throws Exception {
		stopRecording();
	}

	public synchronized MediaRecorder getMediaRecorder() {
		return mMediaRecorder;
	}

	public String getAudioFilePath() {
		return localPath;
	}

	public synchronized void stopRecording() throws Exception {
		if (mMediaRecorder != null) {
			try {
				mMediaRecorder.stop();
				mMediaRecorder.release();
				mMediaRecorder = null;
			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception("err_unknown_stopRecording");
			}
		}
	}
}
