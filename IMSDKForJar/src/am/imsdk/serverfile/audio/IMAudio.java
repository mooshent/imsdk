package am.imsdk.serverfile.audio;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTFileTool;
import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTTool;

public final class IMAudio extends DTLocalModel {
	public String mFileID;
	public byte[] mBuffer;
	
	public IMAudio() {
		mIsStreamData = true;
		setDirectory("IA", 0);
		setDecryptedDirectory("IMAudio", 0);
	}
	
	@Override
	public boolean generateLocalFullPath() {
		if (mFileID.length() == 0) {
			return false;
		}
		
		mLocalFileName = DTTool.getMD5String(mFileID);
		mDecryptedLocalFileName = mFileID;
		return true;
	}
	
	@Override
	public boolean readFromFile() {
		if (!generateLocalFullPath()) {
			return false;
		}
		
		String localPath = getLocalFullPath();
		
		if (!DTFileTool.fileExistsAtPath(localPath)) {
			return false;
		}
		
		mBuffer = DTFileTool.getContentOfFile(localPath);
		
		if (mBuffer.length == 0) {
			DTLog.logError();
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean saveFile() {
		if (!generateLocalFullPath()) {
			DTLog.logError();
			return false;
		}
		
		String localPath = getLocalFullPath();
		
		if (DTFileTool.fileExistsAtPath(localPath)) {
			return false;
		}
		
		if (mBuffer != null && mBuffer.length > 0) {
			DTFileTool.writeToFile(mBuffer, localPath);
		}
		
		return true;
	}
}
