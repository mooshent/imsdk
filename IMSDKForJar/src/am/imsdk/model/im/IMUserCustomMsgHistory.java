package am.imsdk.model.im;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;

public final class IMUserCustomMsgHistory extends IMUserMsgHistory {
	@Override
	public boolean generateLocalFullPath() {
		if (mUID == 0) {
			return false;
		}

		if (mOppositeCustomUserID.length() == 0) {
			DTLog.logError();
			return false;
		}

		setDirectory(DTTool.getSecretString(mUID), 1);
		setDecryptedDirectory(mUID + "", 1);
		setDirectory(DTTool.getMD5String(mOppositeCustomUserID), 2);
		setDecryptedDirectory(mOppositeCustomUserID, 2);
		mLocalFileName = "IUCUMH";
		mDecryptedLocalFileName = "IMUserCustomMsgHistory";

		return true;
	}
}
