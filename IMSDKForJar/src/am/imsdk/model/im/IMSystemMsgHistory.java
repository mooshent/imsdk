package am.imsdk.model.im;

import java.util.ArrayList;

import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTTool;

public class IMSystemMsgHistory extends DTLocalModel {
	
	public long mUID;
	
	public ArrayList<Long> mArrAllSytemMsgKeys = new ArrayList<Long>();
	public ArrayList<Long> mArrUnReadSytemMsgKeys = new ArrayList<Long>();

	public IMSystemMsgHistory() {
		setDirectory(DTTool.getSecretString(mUID), 1);
		setDecryptedDirectory(mUID + "", 1);
		
		addDirectory("ISMH");
		addDecryptedDirectory("IMSystemMsgHistory");
	}
	
	public void insertSystemMsg(IMPrivateSystemMsg msg) {
		mArrAllSytemMsgKeys.add(0, msg.mMsgID);
		mArrUnReadSytemMsgKeys.add(0, msg.mMsgID);
	}
	
	public void removeUnsendSystemMsg(long systemMsgID) {
		mArrUnReadSytemMsgKeys.remove(systemMsgID);
		
		for (int i = mArrUnReadSytemMsgKeys.size() - 1; i >= 0; i--) {
			long msgID = mArrUnReadSytemMsgKeys.get(i);
			
			if(msgID == systemMsgID) {
				mArrUnReadSytemMsgKeys.remove(i);
				return;
			}
		}
	}
	
	public ArrayList<IMPrivateSystemMsg> getUnReadSystemMsg() {
		ArrayList<IMPrivateSystemMsg> msgArr = new ArrayList<IMPrivateSystemMsg>();
		
		for (int i = 0; i < mArrUnReadSytemMsgKeys.size(); i++) {
			IMPrivateSystemMsg msg = new IMPrivateSystemMsg();
			msg.mMsgID = mArrUnReadSytemMsgKeys.get(i);
			msg.readFromFile();
			
			msgArr.add(msg);
		}
		
		return msgArr;
	}
	
	public ArrayList<IMPrivateSystemMsg> getSystemMsg() {
		ArrayList<IMPrivateSystemMsg> msgArr = new ArrayList<IMPrivateSystemMsg>();
		
		for (int i = 0; i < mArrAllSytemMsgKeys.size(); i++) {
			IMPrivateSystemMsg msg = new IMPrivateSystemMsg();
			msg.mMsgID = mArrAllSytemMsgKeys.get(i);
			msg.readFromFile();
			
			msgArr.add(msg);
		}
		
		return msgArr;
	}

}
