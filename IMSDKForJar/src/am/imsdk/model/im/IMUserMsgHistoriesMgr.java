package am.imsdk.model.im;

import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.model.IMPrivateMyself;

public final class IMUserMsgHistoriesMgr {
	public IMUserChatMsgHistory getUserChatMsgHistory(String customUserID) {
		for (IMUserChatMsgHistory history : mAryUserChatMsgHistories) {
			if (history.mUID != IMPrivateMyself.getInstance().getUID()) {
				DTLog.logError();
				return null;
			}
			
			if (history.mOppositeCustomUserID.equals(customUserID)) {
				return history;
			}
		}
		
		IMUserChatMsgHistory result = new IMUserChatMsgHistory();
		
		result.mOppositeCustomUserID = customUserID;
		result.readFromFile();
		
		mAryUserChatMsgHistories.add(result);
		return result;
	}
	
	public IMUserChatMsgHistoryWithTag getUserChatMsgHistory(String customUserID, String tag) {
		for (IMUserChatMsgHistoryWithTag history : mAryUserChatMsgHistoryWithTags) {
			if (history.mUID != IMPrivateMyself.getInstance().getUID()) {
				DTLog.logError();
				return null;
			}
			
			if (history.mOppositeCustomUserID.equals(customUserID) && history.tag.equals(tag)) {
				return history;
			}
		}
		
		IMUserChatMsgHistoryWithTag result = new IMUserChatMsgHistoryWithTag();
		
		result.tag = tag;
		result.mOppositeCustomUserID = customUserID;
		result.readFromFile();
		
		mAryUserChatMsgHistoryWithTags.add(result);
		return result;
	}

	public IMUserCustomMsgHistory getUserCustomMsgHistory(String customUserID) {
		for (IMUserCustomMsgHistory history : mAryUserCustomMsgHistories) {
			if (history.mUID != IMPrivateMyself.getInstance().getUID()) {
				DTLog.logError();
				return null;
			}
			
			if (history.mOppositeCustomUserID.equals(customUserID)) {
				return history;
			}
		}
		
		IMUserCustomMsgHistory result = new IMUserCustomMsgHistory();
		
		result.mOppositeCustomUserID = customUserID;
		result.readFromFile();
		
		mAryUserCustomMsgHistories.add(result);
		return result;
	}

	private ArrayList<IMUserChatMsgHistoryWithTag> mAryUserChatMsgHistoryWithTags = new ArrayList<IMUserChatMsgHistoryWithTag>();
	private ArrayList<IMUserChatMsgHistory> mAryUserChatMsgHistories = new ArrayList<IMUserChatMsgHistory>();
	private ArrayList<IMUserCustomMsgHistory> mAryUserCustomMsgHistories = new ArrayList<IMUserCustomMsgHistory>();

	// singleton
	private volatile static IMUserMsgHistoriesMgr sSingleton;

	private IMUserMsgHistoriesMgr() {
	}

	public static IMUserMsgHistoriesMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMUserMsgHistoriesMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMUserMsgHistoriesMgr();
				}
			}
		}

		return sSingleton;
	}

	// singleton end
	
	// newInstance
	public static void newInstance() {
		synchronized (IMUserMsgHistoriesMgr.class) {
			sSingleton = new IMUserMsgHistoriesMgr();
		}
	}
	// newInstance end
}
