package am.imsdk.model.im;

import java.util.ArrayList;

import am.imsdk.model.IMPrivateMyself;


public final class IMSytemMsgHistoriesMgr {
	
	public IMSystemMsgHistory getSystemMsgHistory() {
		long uid = IMPrivateMyself.getInstance().getUID();
		
		for(IMSystemMsgHistory history : mArrSystemMsgHistories) {
			if(history.mUID == uid) {
				return history;
			}
		}
		
		IMSystemMsgHistory history = new IMSystemMsgHistory();
		history.mUID = uid;
		history.readFromFile();
		
		mArrSystemMsgHistories.add(history);
		
		return history;
	}
	
	private ArrayList<IMSystemMsgHistory> mArrSystemMsgHistories = new ArrayList<IMSystemMsgHistory>();
	

	// singleton
	private volatile static IMSytemMsgHistoriesMgr sSingleton;

	private IMSytemMsgHistoriesMgr() {
	}

	public static IMSytemMsgHistoriesMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMSytemMsgHistoriesMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMSytemMsgHistoriesMgr();
				}
			}
		}

		return sSingleton;
	}

	// singleton end
	
	// newInstance
	public static void newInstance() {
		synchronized (IMSytemMsgHistoriesMgr.class) {
			sSingleton = new IMSytemMsgHistoriesMgr();
		}
	}
	// newInstance end
}
