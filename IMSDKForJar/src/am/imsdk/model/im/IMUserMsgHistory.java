package am.imsdk.model.im;

import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsgsMgr;

public class IMUserMsgHistory extends DTLocalModel {
	public IMUserMsgHistory() {
		mLevel = 2;
		mUID = IMPrivateMyself.getInstance().getUID();
		addDirectory("IUM");
		addDecryptedDirectory("IMUserMsg");
	}

	public long mUID;
	public String mOppositeCustomUserID = "";
	public ArrayList<String> mAryUserMsgKeys = new ArrayList<String>();
	public long mUnreadMessageCount;

	public void insertRecvUserMsg(long msgID) {
		mAryUserMsgKeys.add(0, "R_" + msgID);
	}

	public void insertUnsentUserMsg(long clientSendTime) {
		mAryUserMsgKeys.add(0, clientSendTime + "");
	}
	
	public void removeUnsentUserMsgWithClientSendTime(long clientSendTime) {
		if (clientSendTime == 0) {
			DTLog.logError();
			return;
		}

		for (int i = mAryUserMsgKeys.size() - 1; i >= 0; i--) {
			String string = mAryUserMsgKeys.get(i);

			if (!(string instanceof String)) {
				DTLog.logError();
				return;
			}

			if (string.startsWith("R_")) {
				continue;
			}
			
			if (string.startsWith("S_")) {
				continue;
			}

			if (string.equals("" + clientSendTime)) {
				mAryUserMsgKeys.remove(i);
				return;
			}
		}
	}

	public void replaceUnsentUserMsgToSent(IMUserMsg userMsg) {
		if (userMsg.mClientSendTime == 0) {
			DTLog.logError();
			return;
		}

		if (userMsg.mMsgID == 0) {
			DTLog.logError();
			return;
		}

		for (int i = mAryUserMsgKeys.size() - 1; i >= 0; i--) {
			String key = mAryUserMsgKeys.get(i);

			if (key.startsWith("R_")) {
				continue;
			}

			if (key.startsWith("S_")) {
				continue;
			}

			if (Long.parseLong(key) == userMsg.mClientSendTime) {
				mAryUserMsgKeys.remove(i);
				mAryUserMsgKeys.add(i, "S_" + userMsg.mMsgID);
				return;
			}
		}

		// 有可能发送过程中，用户把这条消息给删了，删了之后又发送成功
		mAryUserMsgKeys.add(0, "S_" + userMsg.mMsgID);
	}

	public void replaceUnsentUserMsgToSent(long clientSendTime, long msgID) {
		for (int i = mAryUserMsgKeys.size() - 1; i >= 0; i--) {
			String key = mAryUserMsgKeys.get(i);

			if (key.startsWith("R_")) {
				continue;
			}

			if (key.startsWith("S_")) {
				continue;
			}

			if (Long.parseLong(key) == clientSendTime) {
				mAryUserMsgKeys.remove(i);
				mAryUserMsgKeys.add(i, "S_" + msgID);
				return;
			}
		}

		// 有可能发送过程中，用户把这条消息给删了，删了之后又发送成功
		mAryUserMsgKeys.add(0, "S_" + msgID);
	}

	public String getNewMsgNotificationKey() {
		if (mOppositeCustomUserID.length() == 0) {
			DTLog.logError();
			return "";
		}

		return "newMsgNotificationKey:" + mOppositeCustomUserID;
	}
	
	public boolean removeUserMsg(int index) {
		if(index >= 0 && index < mAryUserMsgKeys.size()) {
			mAryUserMsgKeys.remove(index);
			
			return true;
		}
		return false;
	}

	public IMUserMsg getUserMsg(int index) {
		if (index >= mAryUserMsgKeys.size()) {
			DTLog.logError();
			return null;
		}

		String key = mAryUserMsgKeys.get(index);

		if (key.startsWith("R_")) {
			key = key.substring(2);

			long msgID = Long.parseLong(key);

			if (msgID == 0) {
				DTLog.logError();
				return null;
			}

			return IMUserMsgsMgr.getInstance().getRecvUserMsg(mOppositeCustomUserID,
					msgID);
		} else if (key.startsWith("S_")) {
			key = key.substring(2);

			long msgID = Long.parseLong(key);

			if (msgID == 0) {
				DTLog.logError();
				return null;
			}

			return IMUserMsgsMgr.getInstance().getSentUserMsg(mOppositeCustomUserID,
					msgID);
		} else {
			long clientSendTime = Long.parseLong(key);

			if (clientSendTime == 0) {
				DTLog.logError();
				return null;
			}

			return IMUserMsgsMgr.getInstance().getUnsentUserMsg(mOppositeCustomUserID,
					clientSendTime);
		}
	}

	public int getCount() {
		return mAryUserMsgKeys.size();
	}

	public int getIndex(IMUserMsg userMsg) {
		if (userMsg.mIsRecv) {
			return mAryUserMsgKeys.indexOf("R_" + userMsg.mMsgID);
		} else if (userMsg.mMsgID != 0) {
			return mAryUserMsgKeys.indexOf("S_" + userMsg.mMsgID);
		} else {
			return mAryUserMsgKeys.indexOf(userMsg.mClientSendTime + "");
		}
	}
}
