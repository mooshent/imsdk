package am.imsdk.model.a2;

import imsdk.data.IMSDK.OnDataChangedListener;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.im.IMUserChatMsgHistory;
import am.imsdk.model.im.IMUserChatMsgHistoryWithTag;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import am.imsdk.model.im.IMUserMsgHistory;
import android.text.TextUtils;

public final class IMMyselfRecentContacts2 {
	public static ArrayList<String> getUsersList() {
		return (ArrayList<String>) IMPrivateRecentContacts.getInstance()
				.getCustomUserIDsList();
	}

	public static String getUser(int index) {
		if (index < 0
				|| index >= IMPrivateRecentContacts.getInstance()
						.getCustomUserIDsList().size()) {
			return "";
		}

		return IMPrivateRecentContacts.getInstance().getCustomUserID(index);
	}

	public static boolean removeUser(String customUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return false;
		}

		return IMPrivateRecentContacts.getInstance().remove(customUserID);
	}

	public static void setOnDataChangedListener(OnDataChangedListener l) {
		sOnDataChangedListener = l;
	}

	static {
		DTNotificationCenter.getInstance().addObserver(
				IMPrivateRecentContacts.getInstance().notificationKey(),
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (sOnDataChangedListener != null) {
							sOnDataChangedListener.onDataChanged();
						}
					}
				});
	}

	private static OnDataChangedListener sOnDataChangedListener;

	public static long getUnreadChatMessageCount() {
		long result = 0;
		ArrayList<String> aryCustomUserIDs = getUsersList();

		for (String customUserID : aryCustomUserIDs) {
			if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
				DTLog.logError();
				sLastError = "IMSDK Error.";
				continue;
			}

			IMUserChatMsgHistory history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(customUserID);

			result += history.mUnreadMessageCount;
		}

		return result;
	}

	public static long getUnreadChatMessageCount(String customUserID) {
		IMUserChatMsgHistory history = IMUserMsgHistoriesMgr.getInstance()
				.getUserChatMsgHistory(customUserID);

		return history.mUnreadMessageCount;
	}
	
	public static long getUnreadChatMessageCount(String customUserID, String tag) {
		IMUserChatMsgHistoryWithTag history = IMUserMsgHistoriesMgr.getInstance()
				.getUserChatMsgHistory(customUserID, tag);

		return history.mUnreadMessageCount;
	}

	public static boolean clearUnreadChatMessage() {
		if (getUnreadChatMessageCount() == 0) {
			return false;
		}

		boolean result = true;
		ArrayList<String> aryCustomUserIDs = getUsersList();

		for (String customUserID : aryCustomUserIDs) {
			if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
				DTLog.logError();
				sLastError = "IMSDK Error.";
				result = false;
				continue;
			}

			IMUserMsgHistory history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(customUserID);

			history.mUnreadMessageCount = 0;
			history.saveFile();
		}

		return result;
	}
	
	public static boolean clearUnreadChatMessage(String customUserID) {
		return clearUnreadChatMessage(customUserID, "");
	}

	public static boolean clearUnreadChatMessage(String customUserID, String tag) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return false;
		}
		
		IMUserMsgHistory history;
		if(TextUtils.isEmpty(tag)) {
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(customUserID);
		} else {
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(customUserID, tag);
		}

		history.mUnreadMessageCount = 0;
		history.saveFile();
		return true;
	}
	
	public static String getLastError() {
		return sLastError;
	}

	private static String sLastError = "";
}
