package am.imsdk.model.a2;

import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMSDK;
import imsdk.data.IMSDK.OnDataChangedListener;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedListener;
import am.imsdk.action.IMActionUserRequestCustomUserInfo;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.userinfo.IMPrivateUserInfo;
import am.imsdk.model.userinfo.IMUsersMgr;

public final class IMSDKCustomUserInfo2 {
	private static HashMap<String, ArrayList<WeakReference<OnDataChangedListener>>> sOnDataChangedListenersHashMap = new HashMap<String, ArrayList<WeakReference<OnDataChangedListener>>>();
	private static String sLastError;

	static {
		DTNotificationCenter.getInstance().addObserver("CustomUserInfoUpdated",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof String)) {
							DTLog.logError();
							return;
						}

						String customUserID = (String) data;

						if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
							DTLog.logError();
							return;
						}

						ArrayList<WeakReference<OnDataChangedListener>> onDataChangedListenersList = sOnDataChangedListenersHashMap
								.get(customUserID);

						if (onDataChangedListenersList == null) {
							return;
						}

						if (onDataChangedListenersList.size() == 0) {
							return;
						}

						for (WeakReference<OnDataChangedListener> weakReference : onDataChangedListenersList) {
							if (weakReference.get() != null) {
								weakReference.get().onDataChanged();
							}
						}
					}
				});
	}

	public static void addOnDataChangedListener(final String customUserID,
			final OnDataChangedListener l) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return;
		}

		ArrayList<WeakReference<OnDataChangedListener>> onDataChangedListenersList = sOnDataChangedListenersHashMap
				.get(customUserID);

		if (onDataChangedListenersList == null) {
			onDataChangedListenersList = new ArrayList<WeakReference<OnDataChangedListener>>();
			sOnDataChangedListenersHashMap
					.put(customUserID, onDataChangedListenersList);
		}

		for (WeakReference<OnDataChangedListener> weakReference : onDataChangedListenersList) {
			if (weakReference.get() == l) {
				return;
			}
		}

		onDataChangedListenersList
				.add(new WeakReference<IMSDK.OnDataChangedListener>(l));
	}

	public static void removeOnDataChangedListener(final OnDataChangedListener l) {
		for (Map.Entry<String, ArrayList<WeakReference<OnDataChangedListener>>> entry : sOnDataChangedListenersHashMap
				.entrySet()) {
			ArrayList<WeakReference<OnDataChangedListener>> onDataChangedListenersList = entry
					.getValue();

			for (WeakReference<OnDataChangedListener> weakReference : onDataChangedListenersList) {
				if (weakReference.get() == l) {
					onDataChangedListenersList.remove(weakReference);
					break;
				}
			}
		}
	}

	public static String get(final String customUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			return "";
		}

		long uid = IMUsersMgr.getInstance().getUID(customUserID);

		if (uid == 0) {
			return "";
		}

		IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(uid);

		return userInfo.getCustomUserInfo();
	}

	public static long request(final String customUserID, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTAppEnv.performAfterDelayOnUIThread(0, new Runnable() {
				@Override
				public void run() {
					if (l != null) {
						l.onFailure(IMParamJudge.getLastError());
					}
				}
			});

			return actionTime;
		}

		IMActionUserRequestCustomUserInfo action = new IMActionUserRequestCustomUserInfo();

		action.mCustomUserID = customUserID;

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		action.begin();
		return actionTime;
	}

	public static String getLastError() {
		return sLastError;
	}
}
