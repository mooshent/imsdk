package am.imsdk.model.a2;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnActionProgressListener;
import imsdk.data.IMMyself.OnActionResultListener;
import imsdk.data.IMMyself.OnInitializedListener;
import imsdk.data.group.IMGroupInfo;
import imsdk.data.group.IMMyselfGroup.OnGroupActionsListener;
import imsdk.data.group.IMMyselfGroup.OnGroupEventsListener;
import imsdk.data.group.IMMyselfGroup.OnGroupMessageListener;
import imsdk.data.group.IMSDKGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedEndListener;
import am.imsdk.action.IMAction.OnActionFailedListener;
import am.imsdk.action.IMAction.OnActionPartiallyDoneListener;
import am.imsdk.action.ammsgs.IMActionSendTeamMsg;
import am.imsdk.action.group.IMActionGroupCreate;
import am.imsdk.action.group.IMActionGroupDelete;
import am.imsdk.action.group.IMActionOperateGroupMember;
import am.imsdk.model.IMAudioSender;
import am.imsdk.model.IMAudioSender.OnAudioSenderListener;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.amimteam.IMTeamMsg;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;
import am.imsdk.model.amimteam.IMTeamMsgsMgr;
import am.imsdk.model.imgroup.IMPrivateRecentGroups;
import am.imsdk.model.imteam.IMTeamChatMsgHistory;
import am.imsdk.model.imteam.IMTeamMsgHistoriesMgr;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo.TeamType;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;
import android.graphics.Bitmap;

public final class IMMyselfGroup2 {
	static {
		DTNotificationCenter.getInstance().addObserver("GroupInitialized",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (sOnGroupEventsListener != null) {
							sOnGroupEventsListener.onInitialized();
						}

						if (sOnInitializedListener != null) {
							sOnInitializedListener.onInitialized();
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("GroupDeletedByUser",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMTeamMsg)) {
							DTLog.logError();
							return;
						}

						if (sOnGroupEventsListener != null) {
							IMTeamMsg teamMsg = (IMTeamMsg) data;

							if (teamMsg.mTeamMsgType != TeamMsgType.IMSDKGroupDeleted) {
								DTLog.logError();
								return;
							}

							if (!IMParamJudge
									.isCustomUserIDLegal(teamMsg.mFromCustomUserID)) {
								DTLog.logError();
								return;
							}

							if (teamMsg.mTeamID == 0) {
								DTLog.logError();
								return;
							}

							String groupID = DTTool.getSecretString(teamMsg.mTeamID);

							if (!IMParamJudge.isGroupIDLegal(groupID)) {
								DTLog.logError();
								return;
							}

							if (teamMsg.mServerSendTime == 0) {
								DTLog.logError();
								return;
							}

							sOnGroupEventsListener.onGroupDeletedByUser(groupID,
									teamMsg.mFromCustomUserID, teamMsg.mServerSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("GroupNameUpdated",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMTeamMsg)) {
							DTLog.logError();
							return;
						}

						if (sOnGroupEventsListener != null) {
							IMTeamMsg teamMsg = (IMTeamMsg) data;

							if (teamMsg.mTeamMsgType != TeamMsgType.IMSDKGroupInfoUpdate) {
								DTLog.logError();
								return;
							}

							if (!IMParamJudge
									.isCustomUserIDLegal(teamMsg.mFromCustomUserID)) {
								DTLog.logError();
								return;
							}

							if (teamMsg.mTeamID == 0) {
								DTLog.logError();
								return;
							}

							String groupID = DTTool.getSecretString(teamMsg.mTeamID);

							if (!IMParamJudge.isGroupIDLegal(groupID)) {
								DTLog.logError();
								return;
							}

							if (teamMsg.mServerSendTime == 0) {
								DTLog.logError();
								return;
							}

							IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(groupID);

							sOnGroupEventsListener.onCustomGroupInfoUpdated(
									groupInfo.getGroupName(), groupID,
									teamMsg.mServerSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("CustomGroupInfoUpdated",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMTeamMsg)) {
							DTLog.logError();
							return;
						}

						if (sOnGroupEventsListener != null) {
							IMTeamMsg teamMsg = (IMTeamMsg) data;

							if (teamMsg.mTeamMsgType != TeamMsgType.IMSDKGroupInfoUpdate) {
								DTLog.logError();
								return;
							}

							if (!IMParamJudge
									.isCustomUserIDLegal(teamMsg.mFromCustomUserID)) {
								DTLog.logError();
								return;
							}

							if (teamMsg.mTeamID == 0) {
								DTLog.logError();
								return;
							}

							String groupID = DTTool.getSecretString(teamMsg.mTeamID);

							if (!IMParamJudge.isGroupIDLegal(groupID)) {
								DTLog.logError();
								return;
							}

							if (teamMsg.mServerSendTime == 0) {
								DTLog.logError();
								return;
							}

							IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(groupID);

							sOnGroupEventsListener.onCustomGroupInfoUpdated(
									groupInfo.getCustomGroupInfo(), groupID,
									teamMsg.mServerSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("GroupMemberUpdated",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMTeamMsg)) {
							DTLog.logError();
							return;
						}

						if (sOnGroupEventsListener != null) {
							IMTeamMsg teamMsg = (IMTeamMsg) data;

							if (teamMsg.mTeamMsgType != TeamMsgType.IMSDKGroupNewUser
									&& teamMsg.mTeamMsgType != TeamMsgType.IMSDKGroupUserRemoved
									&& teamMsg.mTeamMsgType != TeamMsgType.IMSDKGroupQuit) {
								DTLog.logError();
								return;
							}

							if (!IMParamJudge
									.isCustomUserIDLegal(teamMsg.mFromCustomUserID)) {
								DTLog.logError();
								return;
							}

							if (teamMsg.mTeamID == 0) {
								DTLog.logError();
								return;
							}

							String groupID = DTTool.getSecretString(teamMsg.mTeamID);

							if (!IMParamJudge.isGroupIDLegal(groupID)) {
								DTLog.logError();
								return;
							}

							if (teamMsg.mServerSendTime == 0) {
								DTLog.logError();
								return;
							}

							IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(groupID);

							sOnGroupEventsListener.onGroupMemberUpdated(
									groupInfo.getMemberList(), groupID,
									teamMsg.mServerSendTime);
						}
					}
				});

		// IMTeamMsg
		DTNotificationCenter.getInstance().addObserver("AddedToGroup", new Observer() {
			@Override
			public void update(Observable observable, Object data) {
//				if (!(data instanceof IMUserMsg)) {
//					DTLog.logError();
//					return;
//				}

				if (sOnGroupEventsListener != null) {
//					IMUserMsg userMsg = (IMUserMsg) data;
//
//					if (userMsg.mUserMsgType != UserMsgType.IMSDKGroupNoticeBeAdded) {
//						DTLog.logError();
//						return;
//					}
//
//					if (!IMParamJudge.isCustomUserIDLegal(userMsg.mFromCustomUserID)) {
//						DTLog.logError();
//						return;
//					}
//
//					if (userMsg.mContent.length() == 0) {
//						DTLog.logError();
//						return;
//					}
//
//					String groupID = userMsg.mContent;
//
//					if (!IMParamJudge.isGroupIDLegal(groupID)) {
//						DTLog.logError();
//						return;
//					}
//
//					if (userMsg.mServerSendTime == 0) {
//						DTLog.logError();
//						return;
//					}

//					sOnGroupEventsListener.onAddedToGroup(groupID,
//							userMsg.mServerSendTime);
					
					sOnGroupEventsListener.onAddedToGroup("", 0);
				}
			}
		});

		// IMUserMsg
		DTNotificationCenter.getInstance().addObserver("RemovedFromGroup",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						if (sOnGroupEventsListener != null) {
							IMUserMsg userMsg = (IMUserMsg) data;

							if (userMsg.mUserMsgType != UserMsgType.IMSDKGroupNoticeBeRemoved) {
								DTLog.logError();
								return;
							}

							if (!IMParamJudge
									.isCustomUserIDLegal(userMsg.mFromCustomUserID)) {
								DTLog.logError();
								return;
							}

							if (userMsg.mContent.length() == 0) {
								DTLog.logError();
								return;
							}

							String groupID = userMsg.mContent;

							if (!IMParamJudge.isGroupIDLegal(groupID)) {
								DTLog.logError();
								return;
							}

							if (userMsg.mServerSendTime == 0) {
								DTLog.logError();
								return;
							}

							sOnGroupEventsListener.onRemovedFromGroup(groupID,
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}
					}
				});

		// IMTeamMsg
		DTNotificationCenter.getInstance().addObserver("IMReceiveTextFromGroup",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMTeamMsg)) {
							DTLog.logError();
							return;
						}

						if (sOnGroupEventsListener != null) {
							IMTeamMsg teamMsg = (IMTeamMsg) data;

							if (teamMsg.mTeamMsgType != TeamMsgType.Normal) {
								DTLog.logError();
								return;
							}

							if (!IMParamJudge
									.isCustomUserIDLegal(teamMsg.mFromCustomUserID)) {
								DTLog.logError();
								return;
							}

							if (teamMsg.mTeamID == 0) {
								DTLog.logError();
								return;
							}

							String groupID = DTTool.getSecretString(teamMsg.mTeamID);

							if (!IMParamJudge.isGroupIDLegal(groupID)) {
								DTLog.logError();
								return;
							}

							if (teamMsg.mServerSendTime == 0) {
								DTLog.logError();
								return;
							}

							sOnGroupMessageListener.onReceiveText(teamMsg.mContent,
									groupID, teamMsg.mFromCustomUserID,
									teamMsg.mServerSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver(
				"IMReceiveCustomMessageFromGroup", new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMTeamMsg)) {
							DTLog.logError();
							return;
						}

						if (sOnGroupMessageListener != null) {
							IMTeamMsg teamMsg = (IMTeamMsg) data;

							if (teamMsg.mTeamMsgType != TeamMsgType.Custom) {
								DTLog.logError();
								return;
							}

							if (!IMParamJudge
									.isCustomUserIDLegal(teamMsg.mFromCustomUserID)) {
								DTLog.logError();
								return;
							}

							if (teamMsg.mTeamID == 0) {
								DTLog.logError();
								return;
							}

							String groupID = DTTool.getSecretString(teamMsg.mTeamID);

							if (!IMParamJudge.isGroupIDLegal(groupID)) {
								DTLog.logError();
								return;
							}

							if (teamMsg.mServerSendTime == 0) {
								DTLog.logError();
								return;
							}

							sOnGroupMessageListener.onReceiveCustomMessage(
									teamMsg.mContent, groupID,
									teamMsg.mFromCustomUserID, teamMsg.mServerSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("IMReceiveBitmapFromGroup",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMTeamMsg)) {
							DTLog.logError();
							return;
						}

						if (sOnGroupMessageListener != null) {
							IMTeamMsg teamMsg = (IMTeamMsg) data;

							if (teamMsg.mTeamMsgType != TeamMsgType.Photo) {
								DTLog.logError();
								return;
							}

							if (!IMParamJudge
									.isCustomUserIDLegal(teamMsg.mFromCustomUserID)) {
								DTLog.logError();
								return;
							}

							if (teamMsg.mTeamID == 0) {
								DTLog.logError();
								return;
							}

							String groupID = DTTool.getSecretString(teamMsg.mTeamID);

							if (!IMParamJudge.isGroupIDLegal(groupID)) {
								DTLog.logError();
								return;
							}

							if (teamMsg.mServerSendTime == 0) {
								DTLog.logError();
								return;
							}

							if (teamMsg.mTeamMsgType != TeamMsgType.Photo) {
								DTLog.logError();
								return;
							}

							if (!IMParamJudge.isFileIDLegal(teamMsg.getFileID())) {
								DTLog.logError();
								return;
							}

							IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
									teamMsg.getFileID());

							sOnGroupMessageListener.onReceiveBitmap(photo.getBitmap(),
									teamMsg.getGroupID(), teamMsg.mFromCustomUserID,
									teamMsg.mServerSendTime);
						}
					}
				});
	}

	private static OnGroupEventsListener sOnGroupEventsListener;
	private static OnInitializedListener sOnInitializedListener;
	private static OnGroupMessageListener sOnGroupMessageListener;
	private static OnGroupActionsListener sOnGroupActionsListener;
	private static long sLastSendTextTimeMillis = 0;

	public static long getLastSendTextTimeMillis() {
		return sLastSendTextTimeMillis;
	}

	public static void setOnGroupEventsListener(final OnGroupEventsListener l) {
		sOnGroupEventsListener = l;
	}

	public static void setOnGroupActionsListener(final OnGroupActionsListener l) {
		sOnGroupActionsListener = l;
	}

	public static void setOnGroupMessageListener(final OnGroupMessageListener l) {
		sOnGroupMessageListener = l;
	}

	public static void setOnInitializedListener(final OnInitializedListener l) {
		sOnInitializedListener = l;
	}

	public static boolean isInitialized() {
		return IMPrivateMyself.getInstance().isLogined()
				&& IMPrivateMyself.getInstance().getGroupInfoInitedInFact();
	}

	public static ArrayList<String> getMyGroupsList() {
		if (!isInitialized()) {
			sLastError = "group module not initialized yet.";
			return null;
		}

		ArrayList<String> aryGroupIDs = new ArrayList<String>();

		for (long teamID : IMPrivateMyself.getInstance().getGroupIDList()) {
			if (teamID == 0) {
				DTLog.logError();
				sLastError = "IMSDK Error";
				return null;
			}

			String groupID = DTTool.getSecretString(teamID);

			aryGroupIDs.add(groupID);
		}

		return aryGroupIDs;
	}

	public static ArrayList<String> getMyOwnGroupIDList() {
		if (!isInitialized()) {
			sLastError = "group module not initialized yet.";
			return null;
		}

		ArrayList<String> aryGroupIDs = new ArrayList<String>();

		DTLog.sign("IMPrivateMyself.getInstance().getGroupIDList():"
				+ IMPrivateMyself.getInstance().getGroupIDList());

		for (long teamID : IMPrivateMyself.getInstance().getGroupIDList()) {
			if (teamID == 0) {
				DTLog.logError();
				sLastError = "IMSDK Error";
				return null;
			}

			IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);

			if (teamInfo.mTeamType != TeamType.Group) {
				DTLog.logError();
				sLastError = "IMSDK Error";
				return null;
			}

			if (teamInfo.mOwnerUID != IMPrivateMyself.getInstance().getUID()) {
				continue;
			}

			IMGroupInfo groupInfo = IMTeamsMgr.getInstance().getGroupInfo(teamID);

			if (!groupInfo.getOwnerCustomUserID().equals(IMMyself.getCustomUserID())) {
				DTLog.logError();
				continue;
			}

			String groupID = DTTool.getSecretString(teamID);

			aryGroupIDs.add(groupID);
		}

		DTLog.sign("IMPrivateMyself.getInstance().getOwnGroupIDList():" + aryGroupIDs);
		return aryGroupIDs;
	}

	public static boolean isGroupInMyList(String groupID) {
		if (!isInitialized()) {
			sLastError = "group module not initialized yet.";
			return false;
		}

		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			sLastError = IMParamJudge.getLastError();
			return false;
		}

		long teamID = DTTool.getUnsecretLongValue(groupID);

		return IMPrivateMyself.getInstance().getGroupIDList().contains(teamID);
	}

	public static boolean isMyOwnGroup(String groupID) {
		if (!isInitialized()) {
			sLastError = "group module not initialized yet.";
			return false;
		}

		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			sLastError = IMParamJudge.getLastError();
			return false;
		}

		long teamID = DTTool.getUnsecretLongValue(groupID);

		IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);

		DTLog.sign("teamInfo.mOwnerUID:" + teamInfo.mOwnerUID);

		if (teamInfo.mOwnerUID != IMPrivateMyself.getInstance().getUID()) {
			return false;
		}

		DTLog.sign("IMPrivateMyself.getInstance().getGroupIDList():"
				+ IMPrivateMyself.getInstance().getGroupIDList());

		return IMPrivateMyself.getInstance().getGroupIDList().contains(teamID);
	}

	public static long createGroup(final String groupName,
			final OnActionResultListener l) {
		long actionTime = System.currentTimeMillis() / 1000;

		if (!isInitialized()) {
			sLastError = "group module not initialized yet.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (!IMParamJudge.isGroupNameLegal(groupName)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (IMActionGroupCreate.getInstance() != null) {
			sLastError = "正在创建群";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure("正在创建群");
					}
				});
			}

			return actionTime;
		}

		IMActionGroupCreate.newInstance();

		final IMActionGroupCreate action = IMActionGroupCreate.getInstance();

		action.mGroupName = groupName;

		if (l != null) {
			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					if (!IMParamJudge.isTeamIDLegal(action.mTeamID)) {
						DTLog.logError();
						return;
					}

					l.onSuccess(DTTool.getGroupIDFromTeamID(action.mTeamID));
				}
			};
		}

		if (l != null) {
			action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
				@Override
				public void onActionFailedEnd(String error) {
					if(error != null 
							&& error.contains("errorCode:5")){
						error = "Has reached Limit"; //群组数达到上限
					}
					
					l.onFailure(error);
				}
			};
		}

		action.mBeginTime = actionTime;
		action.begin();

		return actionTime;
	}

	public static long deleteGroup(final String groupID, final OnActionListener l) {
		long actionTime = System.currentTimeMillis() / 1000;

		// 环境检查
		if (!isInitialized()) {
			sLastError = "group module not initialized yet.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 参数检查
		// 1. groupID合法
		// 2. 必须是我的群

		// 1. groupID合法
		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 2. 必须是我创建的群
		if (!IMMyselfGroup2.isMyOwnGroup(groupID)) {
			sLastError = "It is not your own group";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		long teamID = DTTool.getUnsecretLongValue(groupID);

		if (teamID == 0) {
			DTLog.logError();
			return actionTime;
		}

		IMActionGroupDelete action = new IMActionGroupDelete();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		action.mTeamID = teamID;
		action.begin();
		return actionTime;
	}

	public static long addMemeber(String customUserID, String toGroupID,
			final OnActionListener l) {
		long actionTime = System.currentTimeMillis() / 1000;

		// 环境检查
		if (!IMPrivateMyself.getInstance().getGroupInfoInited()) {
			sLastError = "group module not initialized yet.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 参数检查
		// 1. customUserID合法
		// 2. groupID合法
		// 3. 必须是我的群
		// 4. 不能是群成员

		// 1. customUserID合法
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 2. groupID合法
		if (!IMParamJudge.isGroupIDLegal(toGroupID)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 3. 必须是我的群
		if (!IMMyselfGroup2.getMyOwnGroupIDList().contains(toGroupID)) {
			sLastError = "It is not a group in my list";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 4. 不能是群成员
		long teamID = DTTool.getUnsecretLongValue(toGroupID);

		if (teamID <= 0) {
			DTLog.logError();
			return actionTime;
		}

		IMGroupInfo groupInfo = IMTeamsMgr.getInstance().getGroupInfo(teamID);

		if (groupInfo.getMemberList().size() <= 0) {
			DTLog.logError();
			return actionTime;
		}

		if (groupInfo.getMemberList().contains(customUserID)) {
			sLastError = "Group already contain this member.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		IMActionOperateGroupMember action = new IMActionOperateGroupMember();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		action.mCustomUserID = customUserID;
		action.mTeamID = teamID;
		action.begin();
		return actionTime;
	}

	public static long removeMember(String customUserID, String fromGroupID,
			final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		// 环境检查
		if (!IMPrivateMyself.getInstance().getGroupInfoInited()) {
			sLastError = "group module not initialized yet.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 参数检查
		// 1. customUserID合法
		// 2. groupID合法
		// 3. groupID必须是我的群
		// 4. groupID必须是我创建的群
		// 5. customUserID必须是groupID群成员
		// 6. customUserID不能是自己

		// 1. customUserID合法
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 2. groupID合法
		if (!IMParamJudge.isGroupIDLegal(fromGroupID)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 3. 必须是我的群
		if (!IMMyselfGroup2.getMyGroupsList().contains(fromGroupID)) {
			sLastError = "It is not a group in my list";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 4. 必须是我创建的群
		if (!IMMyselfGroup2.getMyOwnGroupIDList().contains(fromGroupID)) {
			sLastError = "It is not my own Group.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 5. customUserID必须是groupID群成员
		long fromTeamID = DTTool.getUnsecretLongValue(fromGroupID);

		if (fromTeamID <= 0) {
			DTLog.logError();
			return actionTime;
		}

		IMGroupInfo groupInfo = IMTeamsMgr.getInstance().getGroupInfo(fromTeamID);

		if (groupInfo.getMemberList().size() == 0) {
			DTLog.logError();
			return actionTime;
		}

		if (!groupInfo.getMemberList().contains(customUserID)) {
			sLastError = "The user is not a member of Group.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 6. customUserID不能是自己
		if (customUserID.equals(IMMyself.getCustomUserID())) {
			sLastError = "Couldn't remove yourself.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		IMActionOperateGroupMember action = new IMActionOperateGroupMember();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		action.mTeamID = fromTeamID;
		action.mCustomUserID = customUserID;
		action.mRemoveAction = true;
		action.begin();
		return actionTime;
	}

	public static long sendText(String text, String toGroupID, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		// 环境检查
		if (!IMPrivateMyself.getInstance().getGroupInfoInited()) {
			sLastError = "group module not initialized yet.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 参数检查
		// 1. text没有超出长度
		// 2. groupID合法
		// 3. groupID必须是我的群
		// 4. 我必须是groupID群成员

		// 1. text没有超出长度
		if (!IMParamJudge.isIMTextLegal(text)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 2. groupID合法
		if (!IMParamJudge.isGroupIDLegal(toGroupID)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 3. groupID必须是我的群
		if (!IMMyselfGroup2.isGroupInMyList(toGroupID)) {
			DTLog.sign(IMMyselfGroup2.getMyGroupsList().toString());

			sLastError = "It is not a group in my list.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 4. 我必须是groupID群成员
		long teamID = DTTool.getUnsecretLongValue(toGroupID);

		if (teamID == 0) {
			DTLog.logError();
			return actionTime;
		}

		IMGroupInfo groupInfo = IMTeamsMgr.getInstance().getGroupInfo(teamID);

		if (!groupInfo.getMemberList().contains(IMMyself.getCustomUserID())) {
			IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);

			DTLog.sign(teamInfo.getUIDList().toString());
			DTLog.sign(groupInfo.getMemberList().toString());

			sLastError = "It is not a group in my list.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (sLastSendTextTimeMillis != 0
				&& System.currentTimeMillis() - sLastSendTextTimeMillis < 1000) {
			if (l != null) {
				l.onFailure("发送频率太快");
			}

			if (sOnGroupActionsListener != null) {
				sOnGroupActionsListener.onActionFailure("sendText", "发送频率太快",
						actionTime);
			}

			return actionTime;
		}

		// 创建IMUserMsg
		final IMTeamMsg teamMsg = IMTeamMsgsMgr.getInstance().getUnsentTeamMsg(teamID,
				actionTime);

		teamMsg.mTeamMsgType = TeamMsgType.Normal;
		teamMsg.mTeamID = teamID;
		teamMsg.mContent = text;
		teamMsg.saveFile();

		// 维护聊天记录
		final IMTeamChatMsgHistory history = IMTeamMsgHistoriesMgr.getInstance()
				.getTeamChatMsgHistory(teamID);

		history.insertUnsentTeamMsg(actionTime);
		history.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				history.getNewMsgNotificationKey(), teamMsg);

		// 维护最近联系Group
		IMPrivateRecentGroups.getInstance().insert(toGroupID);
		IMPrivateRecentGroups.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification("recentGroupsChanged");

		// 发送消息
		IMActionSendTeamMsg action = new IMActionSendTeamMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				history.replaceUnsentTeamMsgToSent(teamMsg);
				history.saveFile();

				if (l != null) {
					l.onSuccess();
				}
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mTeamMsg = teamMsg;
		action.begin();

		return actionTime;
	}

	public static boolean startRecording(String toGroupID) {
		if (!IMParamJudge.isGroupIDLegal(toGroupID)) {
			return false;
		}

		return IMAudioSender.getInstance().startRecordingToGroup(toGroupID);
	}
	
	public static long stopRecording(boolean needSend, final long timeoutInterval,
			final OnActionListener l) {
		return stopRecording(needSend, null, timeoutInterval, l);
	}

	public static long stopRecording(boolean needSend, HashMap<String, Object> mTags, final long timeoutInterval,
			final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		boolean result = IMAudioSender.getInstance().stopRecording(needSend, mTags,
				timeoutInterval, new OnAudioSenderListener() {
					@Override
					public void onSendSuccess() {
						commonActionSuccess(l, "SendAudioToGroup", actionTime);
					}

					@Override
					public void onSendFailure(String error) {
						commonActionFailure(l, "SendAudioToGroup", actionTime);
					}

					@Override
					public void onRecordSuccess() {
					}

					@Override
					public void onRecordFailure(String error) {
						commonActionFailure(l, "SendAudioToGroup", actionTime);
					}
				}, actionTime);

		if (!result) {
			if (l != null) {
				l.onFailure("IMSDK Error");
			}
		}

		return actionTime;
	}

	public static long sendBitmap(final Bitmap bitmap, final String toGroupID,
			final long timeoutInterval, final OnActionProgressListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMParamJudge.isGroupIDLegal(toGroupID)) {
			if (l != null) {
				l.onFailure(IMParamJudge.getLastError());
			}

			if (sOnGroupActionsListener != null) {
				sOnGroupActionsListener.onActionFailure("sendBitmap",
						IMParamJudge.getLastError(), actionTime);
			}

			return actionTime;
		}

		if (sLastSendTextTimeMillis != 0
				&& System.currentTimeMillis() - sLastSendTextTimeMillis < 1000) {
			if (l != null) {
				l.onFailure("发送频率太快");
			}

			if (sOnGroupActionsListener != null) {
				sOnGroupActionsListener.onActionFailure("sendBitmap", "发送频率太快",
						actionTime);
			}

			return actionTime;
		}

		sLastSendTextTimeMillis = actionTime;

		// 生成IMImagePhoto
		final IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(bitmap);

		photo.saveFile();

		// 创建content
		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put("fileID", photo.mFileID);
			jsonObject.put("width", photo.getBitmap().getWidth());
			jsonObject.put("height", photo.getBitmap().getHeight());
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();

			if (l != null) {
				l.onFailure("IMSDK Error");
			}

			if (sOnGroupActionsListener != null) {
				sOnGroupActionsListener.onActionFailure("sendBitmap", "IMSDK Error",
						actionTime);
			}

			return actionTime;
		}

		long teamID = DTTool.getTeamIDFromGroupID(toGroupID);

		// 创建IMTeamMsg
		final IMTeamMsg teamMsg = IMTeamMsgsMgr.getInstance().getUnsentTeamMsg(teamID,
				actionTime);

		teamMsg.mTeamID = teamID;
		teamMsg.mContent = jsonObject.toString();
		teamMsg.mTeamMsgType = TeamMsgType.Photo;
		teamMsg.saveFile();

		// 维护聊天记录
		final IMTeamChatMsgHistory history = IMTeamMsgHistoriesMgr.getInstance()
				.getTeamChatMsgHistory(teamMsg.mTeamID);

		history.insertUnsentTeamMsg(teamMsg.mClientSendTime);
		history.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				history.getNewMsgNotificationKey());

		// 维护最近联系人
		IMPrivateRecentGroups.getInstance().insert(toGroupID);
		IMPrivateRecentGroups.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification(
				"IMMyRecentGroupsDataChanged");

		// 发送消息
		IMActionSendTeamMsg action = new IMActionSendTeamMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				// 维护聊天记录
				history.replaceUnsentTeamMsgToSent(teamMsg);
				history.saveFile();

				if (l != null) {
					l.onSuccess();
				}

				if (sOnGroupActionsListener != null) {
					sOnGroupActionsListener.onActionSuccess("SendBitmap", actionTime);
				}
			}
		};

		action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
			@Override
			public void onActionPartiallyDone(double percentage) {
				if (l != null) {
					l.onProgress(percentage / 100);
				}
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				if (l != null) {
					l.onFailure(error);
				}

				if (sOnGroupActionsListener != null) {
					sOnGroupActionsListener.onActionFailure("SendBitmap", error,
							actionTime);
				}
			}
		};

		action.mTeamMsg = teamMsg;

		if (timeoutInterval > 0) {
			action.mTimeoutInterval = timeoutInterval;
		}

		action.mBeginTime = actionTime;
		action.begin();
		return actionTime;
	}

	public static long quitGroup(String fromGroupID, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		// 环境检查
		if (!IMPrivateMyself.getInstance().getGroupInfoInited()) {
			sLastError = "group module not initialized yet.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		// 参数检查
		// 1. groupID合法
		// 2. 不能是我拥有的群
		// 3. groupID必须是我的群
		// 4. 这个群必须包含我

		// 1. groupID合法
		if (!IMParamJudge.isGroupIDLegal(fromGroupID)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				l.onFailure(sLastError);
			}

			return actionTime;
		}

		// 2. 不能是我拥有的群
		if (IMMyselfGroup2.getMyOwnGroupIDList().contains(fromGroupID)) {
			sLastError = "Couldn't quit your owner Group.";

			if (l != null) {
				l.onFailure(sLastError);
			}

			return actionTime;
		}

		// 3. groupID必须是我的群
		if (!IMMyselfGroup2.getMyGroupsList().contains(fromGroupID)) {
			sLastError = "It is not a group in my list.";
			
			if (l != null) {
				l.onFailure(sLastError);
			}

			return actionTime;
		}

		// 4. 这个群必须包含我
		IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(fromGroupID);

		if (groupInfo == null) {
			DTLog.logError();
			return actionTime;
		}

		if (!groupInfo.getMemberList().contains(IMMyself.getCustomUserID())) {
			sLastError = "It is not a group in my list.";
			if (l != null) {
				l.onFailure(sLastError);
			}

			return actionTime;
		}

		long teamID = DTTool.getUnsecretLongValue(fromGroupID);

		if (teamID <= 0) {
			DTLog.logError();
			return actionTime;
		}

		IMActionOperateGroupMember action = new IMActionOperateGroupMember();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		action.mTeamID = teamID;
		action.mCustomUserID = IMMyself.getCustomUserID();
		action.mRemoveAction = true;
		action.begin();
		return actionTime;
	}

	public static String getLastError() {
		return sLastError;
	}

	private static String sLastError = "";

	protected static void commonActionSuccess(OnActionListener l, String actionType,
			long actionTime) {
		if (!DTAppEnv.isUIThread()) {
			DTLog.logError();
			return;
		}

		if (l != null) {
			l.onSuccess();
		}

		if (sOnGroupActionsListener != null) {
			sOnGroupActionsListener.onActionSuccess(actionType, actionTime);
		}
	}

	protected static void commonActionFailure(OnActionListener l, String actionType,
			long actionTime) {
		commonActionFailure(l, actionType, actionTime, IMParamJudge.getLastError());
	}

	protected static void commonActionFailure(OnActionListener l, String actionType,
			long actionTime, String error) {
		if (!DTAppEnv.isUIThread()) {
			DTLog.logError();
			return;
		}

		if (l != null) {
			l.onFailure(error);
		}

		if (sOnGroupActionsListener != null) {
			sOnGroupActionsListener.onActionFailure(actionType, error, actionTime);
		}
	}
}
