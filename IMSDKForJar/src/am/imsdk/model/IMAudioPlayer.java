package am.imsdk.model;

import java.io.IOException;

import org.json.JSONException;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.model.ambase.IMBaseMsg;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.amimteam.IMTeamMsg;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;
import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.audio.IMAudiosMgr;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;

public final class IMAudioPlayer {
	private MediaPlayer mMediaPlayer;
	private OnCompletionListener mCompletionListener;
	private IMAudio mAudio;

	public boolean play(IMBaseMsg baseMsg, OnCompletionListener l) {
		if (baseMsg instanceof IMUserMsg) {
			IMUserMsg userMsg = (IMUserMsg) baseMsg;

			if (userMsg.mUserMsgType != UserMsgType.Audio) {
				DTLog.logError();
				return false;
			}
		} else {
			if (!(baseMsg instanceof IMTeamMsg)) {
				DTLog.logError();
				return false;
			}

			IMTeamMsg teamMsg = (IMTeamMsg) baseMsg;

			if (teamMsg.mTeamMsgType != TeamMsgType.Audio) {
				DTLog.logError();
				return false;
			}
		}

		String fileID = baseMsg.getFileID();

		if (!IMParamJudge.isFileIDLegal(fileID)) {
			DTLog.logError();
			return false;
		}

		mAudio = IMAudiosMgr.getInstance().getAudio(fileID);

		if (mAudio == null) {
			DTLog.logError();
			return false;
		}

		if (mAudio.mBuffer == null) {
			DTLog.logError();
			return false;
		}

		if (!mAudio.isLocalFileExist()) {
			DTLog.logError();
			return false;
		}

		if (mAudio.mBuffer.length == 0) {
			DTLog.logError();
			return false;
		}

		if (mMediaPlayer != null) {
			try {
				mMediaPlayer.stop();
			} catch (IllegalStateException e) {
			}

			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		
		mMediaPlayer = new MediaPlayer();
		mCompletionListener = l;

		mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				if (mMediaPlayer != mp) {
					return;
				}
				if (mCompletionListener != null) {
					mCompletionListener.onCompletion(mMediaPlayer);
				}

				mMediaPlayer.release();
				mMediaPlayer = null;
				mCompletionListener = null;
			}
		});

		mMediaPlayer.setOnPreparedListener(new OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mp) {
				if (mMediaPlayer != mp) {
					DTLog.logError();
					return;
				}

				mMediaPlayer.start();
			}
		});

		try {
			mMediaPlayer.setDataSource(mAudio.getLocalFullPath());
			mMediaPlayer.prepareAsync();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			DTLog.logError();
			mMediaPlayer.release();
			return false;
		} catch (SecurityException e) {
			e.printStackTrace();
			DTLog.logError();
			mMediaPlayer.release();
			return false;
		} catch (IllegalStateException e) {
			e.printStackTrace();
			DTLog.logError();
			mMediaPlayer.release();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			DTLog.logError();
			mMediaPlayer.release();
			return false;
		}

		return true;
	}

	public void stop() {
		if (mMediaPlayer != null) {
			try {
				mMediaPlayer.stop();
			} catch (IllegalStateException e) {
			}

			mMediaPlayer.release();
			mMediaPlayer = null;
			mCompletionListener = null;
		}
	}

	public long getDurationInSeconds(IMBaseMsg baseMsg) {
		if (baseMsg instanceof IMUserMsg) {
			IMUserMsg userMsg = (IMUserMsg) baseMsg;

			if (userMsg.mUserMsgType != UserMsgType.Audio) {
				DTLog.logError();
				return 0;
			}
		} else {
			if (!(baseMsg instanceof IMTeamMsg)) {
				DTLog.logError();
				return 0;
			}

			IMTeamMsg teamMsg = (IMTeamMsg) baseMsg;

			if (teamMsg.mTeamMsgType != TeamMsgType.Audio) {
				DTLog.logError();
				return 0;
			}
		}

		if (baseMsg.mContent == null || baseMsg.mContent.length() == 0) {
			DTLog.logError();
			return 0;
		}

		long durationInMilliseconds = 0;

		try {
			durationInMilliseconds = baseMsg.getExtraData().getLong(
					"durationInMilliseconds");
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
		}

		long durationInSeconds = durationInMilliseconds / 1000;

		if (durationInSeconds != 0) {
			return durationInSeconds;
		}

		return 1;
	}

	// singleton
	private volatile static IMAudioPlayer sSingleton;

	private IMAudioPlayer() {
	}

	public static IMAudioPlayer getInstance() {
		if (sSingleton == null) {
			synchronized (IMAudioPlayer.class) {
				if (sSingleton == null) {
					sSingleton = new IMAudioPlayer();
				}
			}
		}

		return sSingleton;
	}

	// singleton end
}
