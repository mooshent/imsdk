package am.imsdk.model.amim;

import imsdk.data.localchatmessagehistory.IMChatMessage;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.ambase.IMBaseMsg;
import am.imsdk.model.kefu.IMCustomerServiceMgr;
import am.imsdk.model.userinfo.IMUsersMgr;
import android.util.SparseArray;

public class IMUserMsg extends IMBaseMsg {
	public IMUserMsg() {
		super();

		addIgnoreField("mStatus");
		addIgnoreField("mState");
		addIgnoreField("mUserChatMessage");
		addIgnoreField("mExtraData");
	}

	public IMUserMsg(JSONObject recvJsonObject) {
		this();

		if (recvJsonObject == null) {
			DTLog.logError();
			return;
		}

		mIsRecv = true;

		try {
			mServerSendTime = recvJsonObject.getLong("sendtime");
			mContent = recvJsonObject.getString("msgcontent");
			mMsgID = recvJsonObject.getLong("msgid");
			mUserMsgType = UserMsgType.fromInt(recvJsonObject.getInt("msgtype"));

			long uid = recvJsonObject.getLong("touid");

			if (uid == 0) {
				DTLog.logError();
				return;
			}

			mToCustomUserID = IMCustomerServiceMgr.getInstance().getCustomUserID(uid);

			if (mToCustomUserID.length() == 0) {
				mToCustomUserID = IMUsersMgr.getInstance().getCustomUserID(uid);
			}

			if (mToCustomUserID.length() == 0) {
				DTLog.logError();
				return;
			}

			uid = recvJsonObject.getLong("fromuid");

			if (uid == 0) {
				DTLog.logError();
				return;
			}

			mFromCustomUserID = IMCustomerServiceMgr.getInstance().getCustomUserID(uid);

			if (mFromCustomUserID.length() == 0) {
				mFromCustomUserID = IMUsersMgr.getInstance().getCustomUserID(uid);
			}
			
			try {
				String extra = recvJsonObject.getString("extraData");
				
				mExtraData2 = new JSONObject(extra);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			return;
		}

		mStatus = IMChatMessage.SUCCESS;
	}

	public String mToUserNickName = "";
	public String mToCustomUserID = "";
	public UserMsgType mUserMsgType = UserMsgType.Normal;
	public int mStatus = IMChatMessage.SUCCESS; // 消息发送或接收状态
	private IMChatMessage mUserChatMessage;
	public static IMUserMsg sCreatingUserMsg;
	private boolean mIsListened = false;
	
	public boolean isListened() {
		return mIsListened;
	}
	
	public void setVoiceListen() {
		mIsListened = true;
	}

	public IMChatMessage getUserChatMessage() {
		if (mUserChatMessage != null) {
			if (mUserMsgType != UserMsgType.Normal && mUserMsgType != UserMsgType.Audio
					&& mUserMsgType != UserMsgType.Photo) {
				DTLog.logError();
			}

			return mUserChatMessage;
		}

		// if (mUserMsgType != UserMsgType.Normal) {
		// DTLog.logError();
		// return null;
		// }

		sCreatingUserMsg = this;
		mUserChatMessage = new IMChatMessage();
		sCreatingUserMsg = null;

		return mUserChatMessage;
	}

	@Override
	public boolean generateLocalFullPath() {
		if (IMPrivateMyself.getInstance().getUID() == 0) {
			DTLog.logError();
			return false;
		}

		if (mUserMsgType == UserMsgType.Unit) {
			DTLog.logError();
			return false;
		}

		this.setDirectory("IUM", 0);
		this.setDecryptedDirectory("IMUserMsg", 0);

		this.setDirectory(
				DTTool.getSecretString(IMPrivateMyself.getInstance().getUID()), 1);
		this.setDecryptedDirectory("" + IMPrivateMyself.getInstance().getUID(), 1);

		String oppositeCustomUserID = getOppositeCustomUserID();

		if (oppositeCustomUserID.length() == 0) {
			return false;
		}

		this.setDirectory(DTTool.getMD5String(oppositeCustomUserID), 2);
		this.setDecryptedDirectory(oppositeCustomUserID, 2);

		if (mIsRecv) {
			if (mMsgID == 0) {
				DTLog.logError();
				return false;
			}

			this.mLocalFileName = "R_" + DTTool.getSecretString(mMsgID);
			this.mDecryptedLocalFileName = "R_" + mMsgID;
		} else {
			if (mMsgID != 0) {
				// 已发送成功
				this.mLocalFileName = "S_" + DTTool.getSecretString(mMsgID);
				this.mDecryptedLocalFileName = "S_" + mMsgID;
			} else {
				// 未发送成功
				this.mLocalFileName = DTTool.getSecretString(mClientSendTime);
				this.mDecryptedLocalFileName = "" + mClientSendTime;
			}
		}

		return true;
	}

	@Override
	public boolean readFromFile() {
		if (mToCustomUserID == null || mToCustomUserID.length() == 0) {
			return false;
		}

		boolean result = super.readFromFile();

		if (result) {
			if (mMsgID != 0) {
				mStatus = IMChatMessage.SUCCESS;
			} else {
				mStatus = IMChatMessage.FAILURE;
			}
		}

		return result;
	}

	public String getOppositeCustomUserID() {
		if (IMPrivateMyself.getInstance().getUID() == 0) {
			DTLog.logError();
			return "";
		}

		return mIsRecv ? mFromCustomUserID : mToCustomUserID;
	}

	public long getOppositeUID() {
		if (IMPrivateMyself.getInstance().getUID() == 0) {
			DTLog.logError();
			return 0;
		}

		return mIsRecv ? getFromUID() : getToUID();
	}

	public long getFromUID() {
		if (mFromCustomUserID.length() == 0) {
			DTLog.logError();
			return 0;
		}

		long uid = IMUsersMgr.getInstance().getUID(mFromCustomUserID);

		if (uid != 0) {
			return uid;
		}

		return IMCustomerServiceMgr.getInstance().getUID(mFromCustomUserID);
	}

	public long getToUID() {
		if (mToCustomUserID.length() == 0) {
			DTLog.logError();
			return 0;
		}

		return IMUsersMgr.getInstance().getUID(mToCustomUserID);
	}

	public String getSendStatusChangedNotificationKey() {
		if (mIsRecv) {
			DTLog.logError();
			return "";
		}

		return mToCustomUserID + mClientSendTime;
	}

	public enum UserMsgType {
		Normal(0), Unit(1), Audio(2), Photo(3), NormalFileText(4),
		
		Order(5),
		
		Video(200), File(201), System(202),

		IMSDKFriendRequest(10), IMSDKAgreeToFriendRequest(11), IMSDKRejectToFriendRequest(
				12), IMSDKBlacklist(13), IMSDKDeleteFriend(14),

		IMSDKNotice(20), IMSDKNoticeFileText(24),

		IMSDKGroupNoticeBeAdded(31), IMSDKGroupNoticeBeRemoved(32),

		Custom(100), CustomFileText(104);

		private final int value;

		private UserMsgType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		private static final SparseArray<UserMsgType> sValuesArray = new SparseArray<UserMsgType>();

		static {
			for (UserMsgType type : UserMsgType.values()) {
				sValuesArray.put(type.value, type);
			}
		}

		public static UserMsgType fromInt(int i) {
			UserMsgType type = sValuesArray.get(Integer.valueOf(i));

			if (type == null) {
				DTLog.logError();
				return null;
			}

			return type;
		}
	}
}
