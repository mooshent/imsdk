package am.imsdk.model.ambase;

import java.util.HashMap;

import am.imsdk.model.IMPrivateMyself;

public class IMBaseMsgsMgr {
	// 未发送成功
	protected HashMap<String, IMBaseMsg> mMapBaseMsgsUnsent = new HashMap<String, IMBaseMsg>();
	// 已发送成功
	protected HashMap<String, IMBaseMsg> mMapBaseMsgsSent = new HashMap<String, IMBaseMsg>();
	// 已接收成功
	protected HashMap<String, IMBaseMsg> mMapBaseMsgsRecv = new HashMap<String, IMBaseMsg>();

	public long mUID;

	public IMBaseMsgsMgr() {
		mUID = IMPrivateMyself.getInstance().getUID();
	}
}
