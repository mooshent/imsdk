package am.imsdk.model;

import am.dtlib.model.c.tool.DTLocalModel;

public class IMAppSettings extends DTLocalModel {
	public String mAppKey = "";
	public int mClientDataVersion;
	public long mCustomerServiceUID;
	public long mSetupID;
	public String mLastLoginCustomUserID;
	public String mLastLoginPassword;
	public boolean mAutoLogin = true;
	public String mOneKeyLoginCustomUserID;
	public String mOneKeyLoginPassword;
	public long mCustomerServiceVersion;

	public boolean getAutoLogin() {
		return mAutoLogin;
	}

	// singleton
	private volatile static IMAppSettings sSingleton;

	private IMAppSettings() {
		mLocalFileName = "IAS";
		this.readFromFile();
	}

	public static IMAppSettings getInstance() {
		if (sSingleton == null) {
			synchronized (IMAppSettings.class) {
				if (sSingleton == null) {
					sSingleton = new IMAppSettings();
				}
			}
		}

		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
		synchronized (IMAppSettings.class) {
			sSingleton = new IMAppSettings();
		}
	}
}
