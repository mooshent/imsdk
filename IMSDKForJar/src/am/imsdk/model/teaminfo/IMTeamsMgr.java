package am.imsdk.model.teaminfo;

import imsdk.data.group.IMGroupInfo;

import java.util.HashMap;

import am.dtlib.model.b.log.DTLog;

public final class IMTeamsMgr {
	private IMTeamsMgr() {
	}

	private HashMap<Long, IMPrivateTeamInfo> mMapTeamInfos = new HashMap<Long, IMPrivateTeamInfo>();
	private HashMap<Long, IMGroupInfo> mMapGroupInfos = new HashMap<Long, IMGroupInfo>();
	private long mCreatingGroupInfoTeamID;

	public IMPrivateTeamInfo getTeamInfo(long teamID) {
		IMPrivateTeamInfo result = mMapTeamInfos.get(teamID);

		if (result != null) {
			return result;
		}

		result = new IMPrivateTeamInfo();

		result.mTeamID = teamID;
		result.readFromFile();
		mMapTeamInfos.put(teamID, result);
		return result;
	}

	public IMGroupInfo getGroupInfo(long teamID) {
		if (teamID == 0) {
			DTLog.logError();
			return null;
		}

		IMGroupInfo groupInfo = mMapGroupInfos.get(teamID);

		if (groupInfo != null) {
			return groupInfo;
		}

		mCreatingGroupInfoTeamID = teamID;
		groupInfo = new IMGroupInfo();
		mCreatingGroupInfoTeamID = 0;

		mMapGroupInfos.put(teamID, groupInfo);

		return groupInfo;
	}

	public long getCreatingGroupInfoTeamID() {
		return mCreatingGroupInfoTeamID;
	}

	// singleton
	private volatile static IMTeamsMgr sSingleton;

	public static IMTeamsMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMTeamsMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMTeamsMgr();
				}
			}
		}

		return sSingleton;
	}
	// singleton end
}
