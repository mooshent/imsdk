package am.imsdk.model.kefu;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;

public class IMCustomerServiceMgr extends DTLocalModel {
	private HashMap<String, String> mMapCustomUserIDs = new HashMap<String, String>();
	private HashMap<String, Long> mMapUIDs = new HashMap<String, Long>();
	private ArrayList<IMPrivateCSInfo> mCSInfosList = new ArrayList<IMPrivateCSInfo>();

	public IMCustomerServiceMgr() {
		addIgnoreField("mCSInfosList");

		addDirectory("IPCSI");
		addDecryptedDirectory("IMPrivateCustomerServiceInfo");
		mLocalFileName = "ICSM";
		mDecryptedLocalFileName = "IMCustomerServiceMgr";

		readFromFile();

		if (mMapCustomUserIDs == null) {
			mMapCustomUserIDs = new HashMap<String, String>();
		}

		if (mMapUIDs == null) {
			mMapUIDs = new HashMap<String, Long>();
		}
	}

	public ArrayList<String> getCustomerServiceList() {
		ArrayList<String> result = new ArrayList<String>();

		for (String string : mMapUIDs.keySet()) {
			result.add(string);
		}

		return result;
	}

	public IMPrivateCSInfo getCSInfo(JSONObject jsonObject) {
		if (jsonObject == null) {
			DTLog.logError();
			return null;
		}

		IMPrivateCSInfo result = null;

		try {
			long uid = jsonObject.getLong("uid");

			result = getCSInfo(uid);

			if (result != null) {
				result.setCustomUserID(jsonObject.getString("phonenum"));
				result.setEmail(jsonObject.getString("email"));
				result.setNickName(jsonObject.getString("nickname"));
				result.setTelephone(jsonObject.getString("tel"));
				result.setVersion(jsonObject.getLong("v"));
				result.setMainPhotoID(jsonObject.getString("fid"));
				result.saveFile();
				return result;
			}

			result = new IMPrivateCSInfo();

			result.setUID(jsonObject.getLong("uid"));
			result.setCustomUserID(jsonObject.getString("phonenum"));
			result.setEmail(jsonObject.getString("email"));
			result.setNickName(jsonObject.getString("nickname"));
			result.setTelephone(jsonObject.getString("tel"));
			result.setVersion(jsonObject.getLong("v"));
			result.setMainPhotoID(jsonObject.getString("fid"));
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
		}

		if (result.getCustomUserID().length() == 0) {
			DTLog.logError();
			return null;
		}

		if (result.getUID() == 0) {
			DTLog.logError();
			return null;
		}

		result.saveFile();

		mCSInfosList.add(result);
		mMapCustomUserIDs.put(result.getUID() + "", result.getCustomUserID());
		mMapUIDs.put(result.getCustomUserID(), result.getUID());

		saveFile();

		return result;
	}

	public IMPrivateCSInfo getCSInfo(long uid) {
		if (uid <= 0) {
			DTLog.logError();
			return null;
		}

		for (IMPrivateCSInfo info : mCSInfosList) {
			if (info.getUID() == uid) {
				return info;
			}
		}

		IMPrivateCSInfo info = new IMPrivateCSInfo();

		info.setUID(uid);

		if (!info.readFromFile()) {
			return null;
		}

		mCSInfosList.add(info);

		return info;
	}

	public long getUID(String customerServiceID) {
		Long value = mMapUIDs.get(customerServiceID);

		if (value == null) {
			return 0;
		}

		return value.longValue();
	}

	public String getCustomUserID(long uid) {
		String key = uid + "";
		String customUserID = mMapCustomUserIDs.get(key);

		if (customUserID != null && customUserID.length() != 0) {
			IMPrivateCSInfo info = getCSInfo(uid);

			if (info == null || info.getCustomUserID().length() == 0) {
				return "";
			}

			return customUserID;
		}

		IMPrivateCSInfo info = getCSInfo(uid);

		if (info != null && info.getCustomUserID().length() != 0) {
			return customUserID;
		}

		return "";
	}

	public void set(String customUserID, long uid) {
		if (customUserID == null || customUserID.length() == 0) {
			DTLog.logError();
			return;
		}

		if (uid == 0) {
			DTLog.logError();
			return;
		}

		String key = uid + "";
		String value = mMapCustomUserIDs.get(key);

		if (value != null) {
			if (!value.equals(key)) {
				DTLog.logError();
				return;
			}
		}

		mMapCustomUserIDs.put(key, value);

		long tempUID = mMapUIDs.get(value);

		if (tempUID != 0) {
			if (tempUID != uid) {
				DTLog.logError();
				return;
			}
		}

		mMapUIDs.put(value, Long.valueOf(uid));
	}

	public void removeCSInfo(long uid) {
		if (uid == 0) {
			DTLog.logError();
			return;
		}

		IMPrivateCSInfo info = getCSInfo(uid);

		if (info != null && mCSInfosList.contains(info)) {
			mCSInfosList.remove(info);
			info.removeFile();
		}

		String key = uid + "";
		String value = mMapCustomUserIDs.get(key);

		if (mMapUIDs.containsKey(value)) {
			mMapUIDs.remove(value);
		}

		if (mMapCustomUserIDs.containsKey(key)) {
			mMapCustomUserIDs.remove(key);
		}
	}

	// singleton
	private volatile static IMCustomerServiceMgr sSingleton;

	public static IMCustomerServiceMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMCustomerServiceMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMCustomerServiceMgr();
				}
			}
		}

		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
		synchronized (IMCustomerServiceMgr.class) {
			sSingleton = new IMCustomerServiceMgr();
		}
	}
	// newInstance end
}
