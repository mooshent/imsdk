package am.imsdk.model;

import imsdk.data.group.IMGroupInfo;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMTeamsMgr;

public final class IMDataJudge {
	private static String sLastError;

	public static String getLastError() {
		return IMDataJudge.sLastError;
	}

	public static void setLastError(String lastError) {
		IMDataJudge.sLastError = lastError;
	}

	// 不判断Group Module是否初始化
	public static boolean isMyGroupLegal(long teamID) {
		if (teamID == 0) {
			sLastError = "teamID不能为0";
			return false;
		}

		IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);

		if (teamInfo.mTeamType != IMPrivateTeamInfo.TeamType.Group) {
			sLastError = "不是Group类型";
			return false;
		}

		IMGroupInfo groupInfo = IMTeamsMgr.getInstance().getGroupInfo(teamID);

		if (groupInfo == null) {
			sLastError = "IMSDK Error";
			DTLog.logError();
			return false;
		}

		String ownerCustomUserID = groupInfo.getOwnerCustomUserID();

		if (!IMParamJudge.isCustomUserIDLegal(ownerCustomUserID)) {
			sLastError = "ownerCustomUserID不合法";
			return false;
		}

		if (groupInfo.getMemberList().size() == 0) {
			sLastError = "成员列表为空";
			return false;
		}

		for (String customUserID : groupInfo.getMemberList()) {
			if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
				sLastError = "成员CustomUserID不合法";
				return false;
			}
		}

		if (!IMPrivateMyself.getInstance().getTeamIDList().contains(teamID)) {
			sLastError = "不是我的群";
			return false;
		}

		String groupID = DTTool.getGroupIDFromTeamID(teamID);

		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			sLastError = IMParamJudge.getLastError();
			return false;
		}

		if (!IMPrivateMyself.getInstance().getGroupIDList().contains(teamID)) {
			sLastError = "IMSDK Error";
			DTLog.logError();
			return false;
		}

		return true;
	}
}
