package am.imsdk.model.userinfo;

import imsdk.data.IMUserLocation;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.model.IMPrivateMyself;

public class IMPrivateUserInfo extends DTLocalModel {
	public IMPrivateUserInfo() {
		this.addIgnoreField("mUserLocation");
		this.addIgnoreField("mCustomUserID");
		this.addDirectory("IPUI");
		this.addDecryptedDirectory("IMPrivateUserInfo");

		if (IMUsersMgr.getCreatingUID() == 0 && !(this instanceof IMPrivateMyself)) {
			DTLog.logError();
		}

		mUID = IMUsersMgr.getCreatingUID();
	}

	public String getCustomUserID() {
		if (mUID == 0) {
			return "";
		}

		if (mCustomUserID.length() == 0) {
			mCustomUserID = IMUsersMgr.getInstance().getCustomUserID(mUID);
		}

		return mCustomUserID;
	}

	public String getCustomUserInfo() {
		return mExInfo;
	}

	public String getBaseInfo() {
		return mBaseInfo;
	}

	public void setBaseInfo(String baseInfo) {
		if (baseInfo == null) {
			baseInfo = "";
		}

		DTLog.sign(baseInfo);

		if (mBaseInfo.equals(baseInfo)) {
			return;
		}

		mBaseInfo = baseInfo;

		mBaseInfoJsonObject = new JSONObject();
		mMainPhotoFileID = "";

		try {
			mBaseInfoJsonObject = new JSONObject(mBaseInfo);
			mMainPhotoFileID = mBaseInfoJsonObject.getString("p");
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			return;
		}
		
		if (mMainPhotoFileID == null) {
			mMainPhotoFileID = "";
		}
	}

	public String getMainPhotoFileID() {
		if (mMainPhotoFileID == null) {
			mMainPhotoFileID = "";
		}

		return mMainPhotoFileID;
	}

	public IMUserLocation getUserLocation() {
		if (mUserLocation == null) {
			sCreatingLocationUID = mUID;
			mUserLocation = new IMUserLocation();
			sCreatingLocationUID = 0;
		}

		return mUserLocation;
	}

	public long getUID() {
		return mUID;
	}

	@Override
	public boolean generateLocalFullPath() {
		if (mUID == 0) {
			return false;
		}

		mLocalFileName = DTTool.getSecretString(mUID);
		mDecryptedLocalFileName = "" + mUID;
		return true;
	}

	protected long mUID;
	private String mCustomUserID = "";
	public double mLatitude;
	public double mLongitude;
	public String mAppKeyLogin = "";
	public String mPlatformLogin = "";
	protected String mBaseInfo = "";
	public String mExInfo = "";
	public long mLastUpdateTime;

	protected JSONObject mBaseInfoJsonObject = new JSONObject();
	protected String mMainPhotoFileID = "";
	private IMUserLocation mUserLocation;

	public static long sCreatingLocationUID;
}
