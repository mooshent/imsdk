package am.imsdk.model.imteam;

import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amimteam.IMTeamMsg;
import am.imsdk.model.amimteam.IMTeamMsgsMgr;

public class IMTeamMsgHistory extends DTLocalModel {
	public IMTeamMsgHistory() {
		mLevel = 2;
		mUID = IMPrivateMyself.getInstance().getUID();
		addDirectory("ITM");
		addDecryptedDirectory("IMTeamMsg");
	}

	public long mUID;
	public long mTeamID;
	public ArrayList<String> mAryTeamMsgKeys = new ArrayList<String>();
	public long mUnreadMessageCount;

	public void insertRecvTeamMsg(long msgID) {
		mAryTeamMsgKeys.add(0, "R_" + msgID);
	}

	public void insertUnsentTeamMsg(long clientSendTime) {
		mAryTeamMsgKeys.add(0, clientSendTime + "");
	}

	public void replaceUnsentTeamMsgToSent(IMTeamMsg teamMsg) {
		if (teamMsg.mClientSendTime == 0) {
			DTLog.logError();
			return;
		}

		if (teamMsg.mMsgID == 0) {
			DTLog.logError();
			return;
		}

		for (int i = mAryTeamMsgKeys.size() - 1; i >= 0; i--) {
			String key = mAryTeamMsgKeys.get(i);

			if (key.startsWith("R_")) {
				continue;
			}

			if (key.startsWith("S_")) {
				continue;
			}

			if (Long.parseLong(key) == teamMsg.mClientSendTime) {
				mAryTeamMsgKeys.remove(i);
				mAryTeamMsgKeys.add(i, "S_" + teamMsg.mMsgID);
				return;
			}
		}

		// 有可能发送过程中，用户把这条消息给删了，删了之后又发送成功
		mAryTeamMsgKeys.add(0, "S_" + teamMsg.mMsgID);
	}

	public void replaceUnsentTeamMsgToSent(long clientSendTime, long msgID) {
		for (int i = mAryTeamMsgKeys.size() - 1; i >= 0; i--) {
			String key = mAryTeamMsgKeys.get(i);

			if (key.startsWith("R_")) {
				continue;
			}

			if (key.startsWith("S_")) {
				continue;
			}

			if (Long.parseLong(key) == clientSendTime) {
				mAryTeamMsgKeys.remove(i);
				mAryTeamMsgKeys.add(i, "S_" + msgID);
				return;
			}
		}

		// 有可能发送过程中，用户把这条消息给删了，删了之后又发送成功
		mAryTeamMsgKeys.add(0, "S_" + msgID);
	}
	
	public boolean removeTeamMsg(int index) {
		if(index >= 0 && index < mAryTeamMsgKeys.size()) {
			mAryTeamMsgKeys.remove(index);
			return true;
		}
		return false;
	}

	public IMTeamMsg getTeamMsg(int index) {
		if (index >= mAryTeamMsgKeys.size()) {
			DTLog.logError();
			return null;
		}

		DTLog.sign(mAryTeamMsgKeys.toString());

		String key = mAryTeamMsgKeys.get(index);

		if (key.startsWith("R_")) {
			key = key.substring(2);

			long msgID = Long.parseLong(key);

			if (msgID == 0) {
				DTLog.logError();
				return null;
			}

			return IMTeamMsgsMgr.getInstance().getRecvTeamMsg(mTeamID, msgID);
		} else if (key.startsWith("S_")) {
			key = key.substring(2);

			long msgID = Long.parseLong(key);

			if (msgID == 0) {
				DTLog.logError();
				return null;
			}

			return IMTeamMsgsMgr.getInstance().getSentTeamMsg(mTeamID, msgID);
		} else {
			long clientSendTime = Long.parseLong(key);

			if (clientSendTime == 0) {
				DTLog.logError();
				return null;
			}

			return IMTeamMsgsMgr.getInstance().getUnsentTeamMsg(mTeamID, clientSendTime);
		}
	}

	public String getNewMsgNotificationKey() {
		if (mTeamID == 0) {
			DTLog.logError();
			return "";
		}

		return "newTeamMsgNotificationKey:" + mTeamID;
	}
	
	public int getCount() {
		return mAryTeamMsgKeys.size();
	}

	public int getIndex(IMTeamMsg teamMsg) {
		if (teamMsg.mIsRecv) {
			return mAryTeamMsgKeys.indexOf("R_" + teamMsg.mMsgID);
		} else if (teamMsg.mMsgID != 0) {
			return mAryTeamMsgKeys.indexOf("S_" + teamMsg.mMsgID);
		} else {
			return mAryTeamMsgKeys.indexOf(teamMsg.mClientSendTime + "");
		}
	}
}
