package am.imsdk.aacmd;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTPushCmd;
import am.dtlib.model.c.socket.DTSocket;
import am.imsdk.aacmd.im.IMPushCmdIMSystemMsg;
import am.imsdk.aacmd.im.IMPushCmdIMTeamMsg;
import am.imsdk.aacmd.im.IMPushCmdIMUserMsg;
import am.imsdk.aacmd.user.IMPushCmdUserLoginConflict;

public final class IMSocket extends DTSocket {
	@Override
	public DTPushCmd getPushCmdObject(int cmdTypeValue) {
		if (cmdTypeValue == 0) {
			DTLog.logError();
			return null;
		}
		
		IMCmdType cmdType = IMCmdType.fromInt(cmdTypeValue);
		
		if (cmdType == null) {
			DTLog.logError();
			return null;
		}
		
		switch (cmdType) {
		case IM_PUSH_CMD_IM_USER_MSG:
			return new IMPushCmdIMUserMsg();
		case IM_PUSH_CMD_IM_SYSTEM_MSG:
			return new IMPushCmdIMSystemMsg();
		case IM_PUSH_CMD_IM_TEAM_MSG:
			return new IMPushCmdIMTeamMsg();
		case IM_PUSH_CMD_USER_LOGIN_CONFLICT:
			return new IMPushCmdUserLoginConflict();
		default:
			break;
		}
		
		DTLog.logError();
		DTLog.log(cmdType.toString());
		
		return null;
	}
	
	// singleton
	private IMSocket() {
		super();
	}
	
	public static DTSocket getInstance() {
		if (sSingleton == null) {
			synchronized (IMSocket.class) {
				if (sSingleton == null) {
					sSingleton = new IMSocket();
				}
			}
		}
		
		return sSingleton;
	}
	// singleton end
}
