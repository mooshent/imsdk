package am.imsdk.aacmd.user;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.aacmd.IMSocket;

public final class IMCmdUserHeartbeat extends DTCmd {
	public IMCmdUserHeartbeat() {
		mCmdTypeValue = IMCmdType.IM_CMD_USER_HELLO.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
	}
	
	private static int sFailedTime = 0;

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		sFailedTime = 0;
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	@Override
	public void onSendFailed() {
		sFailedTime++;
		this.tryReconnect();
	}

	@Override
	public void onNoRecv() {
		sFailedTime++;
		this.tryReconnect();
	}

	private void tryReconnect() {
		if (sFailedTime > 3) {
			DTAppEnv.getMainHandler().post(new Runnable() {
				@Override
				public void run() {
					IMSocket.getInstance().reconnect();
				}
			});
		}
	}
}
