 package am.imsdk.ui.utils;

import java.util.HashMap;

import am.imsdk.demo.util.CPResourceUtil;
import am.imsdk.model.IMChatSetting;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Vibrator;

public class IMNoticeUtils {

	/**
	 * 
	 * @Description: 是否响铃
	 * @return 
	 * @throws 
	 * @author 方子君
	 */
	private static boolean isSound(Context context) {
		AudioManager audioService = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {
			return false;
		} else {
			if(IMChatSetting.getInstance().isMsgSound()) {
				return true;
			} else {
				return false;
			}
		}
		
	}
	
	/**
	 * 
	 * @Description: 是否振动
	 * @return 
	 * @throws 
	 * @author 方子君
	 */
	private static boolean isVibrate(Context context) {
		AudioManager audioService = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		if (audioService.getRingerMode() == AudioManager.RINGER_MODE_SILENT) {
			return false;
		} else {
			if(IMChatSetting.getInstance().isMsgVibrate()) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	private static SoundPool sSoundPool;
	private static Vibrator sVibrator;
	
	//R source ID, soundPool source ID
	private static HashMap<Integer, Integer> soundMap = new HashMap<Integer, Integer>();
	
	/**
	 * 
	 * @Description: 播放响铃
	 * @throws 
	 * @author 方子君
	 */
	private static void playMsgSound(Context context) {
		if(!isSound(context)) {
			return;
		}
		
		if(sSoundPool == null) {
			//指定声音池的最大音频流数目为10，声音品质为5
			sSoundPool = new SoundPool(10, AudioManager.STREAM_SYSTEM, 5);  
		}
		
		int sourceID = CPResourceUtil.getRawId(context, "notify");
		
		if (!soundMap.containsKey(sourceID)) {
			final int sourceid = sSoundPool.load(context, sourceID, 0);
			soundMap.put(sourceID, sourceid);

			sSoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {

				public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
					if(status == 0) {
						soundPool.play(sourceid, 1, 1, 0, 0, 1);
					}
				}
			});

			return;
		}
		
		//播放音频，
		//第二个参数为左声道音量;
		//第三个参数为右声道音量;
		//第四个参数为优先级;
		//第五个参数为循环次数，0不循环，-1循环;
		//第六个参数为速率，速率最低0.5最高为2，1代表正常速度  
		sSoundPool.play(soundMap.get(sourceID), 1, 1, 0, 0, 1); 
	}
	
	/**
	 * 
	 * @Description: 播放振动
	 * @throws 
	 * @author 方子君
	 */
	private static void playMsgVibrate(Context context) {
		if(!isVibrate(context)) {
			return;
		}
		
		if(sVibrator == null) {
			sVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE); 
		}
		
		sVibrator.vibrate(300);
	}
	
	public static void playMsgNotice(Context context) {
		if(isSound(context)) {
			playMsgSound(context);
		}
		
		if(isVibrate(context)) {
			playMsgVibrate(context);
		}
	}

}
