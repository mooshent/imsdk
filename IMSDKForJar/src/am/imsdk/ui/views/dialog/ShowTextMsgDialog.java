package am.imsdk.ui.views.dialog;

import am.imsdk.demo.util.CPResourceUtil;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * 文本消息框
 */
public class ShowTextMsgDialog extends Dialog {
	private boolean isPhoto;
	
	private TextView tv_nickname;
	private TextView tv_copy;
	private TextView tv_forward;
	private TextView tv_delete;
	private TextView tv_at_he;
	
	public ShowTextMsgDialog(Context context, boolean isPhoto) {
		super(context, CPResourceUtil.getStyleId(context, "myDialogTheme"));
		this.isPhoto = isPhoto;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(CPResourceUtil.getLayoutId(getContext(), "im_text_msg_dialog"));
		
		tv_nickname = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_nickname"));
		tv_copy = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_copy"));
		tv_forward = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_forward"));
		tv_delete = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_delete"));
		tv_at_he = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_at_he"));
		
		if (isPhoto) {
			tv_copy.setText("保存到手机");
		}else{
			
		}
	}
	
	public void setCopyClickListener(View.OnClickListener l) {
		if(tv_copy != null) {
			tv_copy.setOnClickListener(l);
		}
	}
	
	public void setForwardClickListener(View.OnClickListener l) {
		if(tv_forward != null) {
			tv_forward.setOnClickListener(l);
		}
	}
	
	public void setDeleteClickListener(View.OnClickListener l) {
		if(tv_delete != null) {
			tv_delete.setOnClickListener(l);
		}
	}
	
	public void setAtHeClickListener(View.OnClickListener l) {
		if(tv_at_he != null) {
			tv_at_he.setOnClickListener(l);
		}
	}
}
