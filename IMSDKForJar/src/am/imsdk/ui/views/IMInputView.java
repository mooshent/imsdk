package am.imsdk.ui.views;

import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.image.IMImagePhoto;
import android.content.Context;
import android.view.View;

public final class IMInputView extends View {
	public IMInputView(Context context) {
		super(context);
	}

	public static interface OnInputDone {
		public void onInputText(String text);

		public void onInputAudio(IMAudio audio);

		public void onInputImage(IMImagePhoto image);
	}
}
