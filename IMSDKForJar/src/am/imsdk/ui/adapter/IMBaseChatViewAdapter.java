package am.imsdk.ui.adapter;

import imsdk.views.IMChatView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.demo.gif.GifEmotionUtils;
import am.imsdk.ui.views.IMBaseChatView;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.PopupWindow;

public abstract class IMBaseChatViewAdapter extends BaseAdapter {
	// init
	protected Context mContext;
	protected LayoutInflater mInflater;
	protected GifEmotionUtils mGifEmotionUtils;
	protected IMBaseChatView.OnChatViewTouchListener mOnChatViewTouchListener;

	// config
	protected boolean mUserNameVisible;
	protected boolean mUserMainPhotoVisible;
	protected int mUserMainPhotoCornerRadius;

	// temp data
	protected AnimationDrawable mLastAnimationDrawable;
	protected static int PHOTO_WIDTH = 200;
	protected static int PHOTO_HEIGHT = 200;
	protected int mAudioPlayingPosition = -1;
//	public HashMap<Long, Integer> mPhotoMsgIDPositionMap;

	protected IMChatView.OnHeadPhotoClickListener mHeadPhotoClickListener;
	protected View.OnLongClickListener mContentLongClickListener;
	// protected View.OnClickListener mOnImageClickListener;
	protected ArrayList<Integer> mIDs;
	protected Handler mHandler = new Handler();
	
	protected IMChatView mImchatView;
	protected PopupWindow popupWindow;
	protected View chatImageView;

	public IMBaseChatViewAdapter(Context context, GifEmotionUtils gifEmotionUtils,
			ArrayList<Integer> ids, boolean userNameVisible,
			boolean userMainPhotoVisible, int userMainPhotoCornerRadius,
			IMBaseChatView.OnChatViewTouchListener onChatViewTouchListener) {
		super();

		if (context == null) {
			DTLog.logError();
			return;
		}

		if (context instanceof Activity) {
			WindowManager windowManager = ((Activity) context).getWindowManager();
			Point outSize = new Point(PHOTO_WIDTH, PHOTO_HEIGHT);

			overrideGetSize(windowManager.getDefaultDisplay(), outSize);

			int width = outSize.x;

			width = (int) (width / 2);
			PHOTO_WIDTH = width > 500 ? 500 : width;
			PHOTO_HEIGHT = PHOTO_WIDTH;
		}

		mContext = context;
		mInflater = LayoutInflater.from(context);
		mGifEmotionUtils = gifEmotionUtils;
		mUserNameVisible = userNameVisible;
		mUserMainPhotoVisible = userMainPhotoVisible;
		mUserMainPhotoCornerRadius = userMainPhotoCornerRadius;
		mIDs = ids;
		mOnChatViewTouchListener = onChatViewTouchListener;
//		mPhotoMsgIDPositionMap = new HashMap<Long, Integer>();
	}

	public void setUserNameVisible(boolean visible) {
		this.mUserNameVisible = visible;
	}

	public void setUserMainPhotoVisible(boolean visible) {
		this.mUserMainPhotoVisible = visible;
	}

	public void setUserMainPhotoCornerRadius(int cornerRadius) {
		this.mUserMainPhotoCornerRadius = cornerRadius;
	}


	public void setOnHeadPhotoClickListener(IMChatView.OnHeadPhotoClickListener listener) {
		this.mHeadPhotoClickListener = listener;
	}
	
	public void setOnContentLongClickListener(OnLongClickListener l) {
		mContentLongClickListener = l;
	}
	
	protected Runnable mOrderBtn1ClickRunnable;
	protected Runnable mOrderBtn2ClickRunnable;

	public void setOrderContinueListener(Runnable action) {
		mOrderBtn1ClickRunnable = action;
	}
	
	public void setOrderConfirmListener(Runnable action) {
		mOrderBtn2ClickRunnable = action;
	}
	
	protected OnTouchListener mOnTouchListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
				if (mOnChatViewTouchListener != null) {
					mOnChatViewTouchListener.onTouch(v);
				}
			}

			return false;
		}
	};

	public void setOnImageClickListener(View.OnClickListener listener) {
		// mOnImageClickListener = listener;
	}
	

	@Override
	public long getItemId(int position) {
		return position;
	}

	protected float calculateImageScaleRatio(int width, int height) {
		float scale = 0;
		int suitableWidth = PHOTO_WIDTH;
		int suitableHeight = PHOTO_HEIGHT;
		float scaleW = 1.0F;
		float scaleH = 1.0F;

		if (suitableWidth > 0 && width > suitableWidth) {
			scaleW = suitableWidth * 1.0F / width;
		}

		if (suitableHeight > 0 && height > suitableHeight) {
			scaleH = suitableHeight * 1.0F / height;
		}

		// L.e("图片预定 scaleW=" + scaleW + " scaleH" + scaleH);
		scale = Math.min(scaleH, scaleW);
		return scale;
	}

	@SuppressWarnings("deprecation")
	private void overrideGetSize(Display display, Point outSize) {
		try {
			Class<?> pointClass = Class.forName("android.graphics.Point");
			Method newGetSize = Display.class.getMethod("getSize",
					new Class[] { pointClass });

			newGetSize.invoke(display, outSize);
		} catch (NoSuchMethodException ex) {
			outSize.x = display.getWidth();
			outSize.y = display.getHeight();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
}
