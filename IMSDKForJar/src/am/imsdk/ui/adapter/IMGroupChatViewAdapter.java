package am.imsdk.ui.adapter;

import imsdk.data.localchatmessagehistory.IMChatMessage;
import imsdk.views.RoundedImageView;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.demo.gif.GifEmotionUtils;
import am.imsdk.demo.util.CPResourceUtil;
import am.imsdk.demo.util.ViewHolder;
import am.imsdk.model.IMAudioPlayer;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amimteam.IMTeamMsg;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;
import am.imsdk.model.imteam.IMTeamChatMsgHistory;
import am.imsdk.model.imteam.IMTeamMsgHistoriesMgr;
import am.imsdk.model.userinfo.IMPrivateUserInfo;
import am.imsdk.model.userinfo.IMUsersMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImageThumbnail;
import am.imsdk.serverfile.image.IMImagesMgr;
import am.imsdk.ui.views.IMBaseChatView;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class IMGroupChatViewAdapter extends IMBaseChatViewAdapter {
	// init
	private String mGroupID;
	private long mTeamID;
	private IMTeamChatMsgHistory mTeamChatMsgHistory;

	public IMGroupChatViewAdapter(Context context, GifEmotionUtils gifEmotionUtils,
			ArrayList<Integer> mIDs, boolean userNameVisible,
			boolean userMainPhotoVisible, int userMainPhotoCornerRadius,
			IMBaseChatView.OnChatViewTouchListener onChatViewTouchListener,
			String groupID) {
		super(context, gifEmotionUtils, mIDs, userNameVisible, userMainPhotoVisible,
				userMainPhotoCornerRadius, onChatViewTouchListener);
		setGroupID(groupID);
	}

	public void setGroupID(String groupID) {
		mGroupID = groupID;
		mTeamID = DTTool.getTeamIDFromGroupID(mGroupID);

		mTeamChatMsgHistory = IMTeamMsgHistoriesMgr.getInstance()
				.getTeamChatMsgHistory(mTeamID);
	}

	@Override
	public Object getItem(int position) {
		return mTeamChatMsgHistory.getTeamMsg(mTeamChatMsgHistory.getCount() - 1
				- position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getCount() {
		return mTeamChatMsgHistory.getCount();
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(mIDs.get(0), null);
		}

		if (convertView == null) {
			DTLog.logError();
			return null;
		}

		final IMTeamMsg teamMsg = mTeamChatMsgHistory.getTeamMsg(mTeamChatMsgHistory
				.getCount() - 1 - position);
		final View finalConvertView = convertView;

		if (!teamMsg.mIsRecv) {
			DTNotificationCenter.getInstance().removeObservers(
					teamMsg.getSendStatusChangedNotificationKey());
			DTNotificationCenter.getInstance().addObserver(
					teamMsg.getSendStatusChangedNotificationKey(), new Observer() {
						@Override
						public void update(Observable observable, Object data) {
							updateView(position, finalConvertView, teamMsg);
						}
					});
		}

		updateView(position, convertView, teamMsg);
		return convertView;
	}

	private void updateView(int position, View convertView, IMTeamMsg teamMsg) {
		TextView systemTip = ViewHolder.get(convertView, mIDs.get(1));
		TextView systemContent = ViewHolder.get(convertView, mIDs.get(2));

		ImageView leftHead = ViewHolder.get(convertView, mIDs.get(3));
		ImageView leftAddon = ViewHolder.get(convertView, mIDs.get(4));
		TextView leftUserName = ViewHolder.get(convertView, mIDs.get(5));
		TextView leftContent = ViewHolder.get(convertView, mIDs.get(6));
		View leftImageRoot = ViewHolder.get(convertView, mIDs.get(7));
		TextView leftImageProgress = ViewHolder.get(convertView, mIDs.get(8));
		View leftImageMask = ViewHolder.get(convertView, mIDs.get(9));
		ImageView leftImage = ViewHolder.get(convertView, mIDs.get(10));

		ImageView rightHead = ViewHolder.get(convertView, mIDs.get(11));
		ImageView rightAddon = ViewHolder.get(convertView, mIDs.get(12));
		TextView rightUserName = ViewHolder.get(convertView, mIDs.get(13));
		TextView rightContent = ViewHolder.get(convertView, mIDs.get(14));
		View rightImageRoot = ViewHolder.get(convertView, mIDs.get(15));
		TextView rightImageProgress = ViewHolder.get(convertView, mIDs.get(16));

		View rightImageMask = ViewHolder.get(convertView, mIDs.get(17));

		ImageView rightImage = ViewHolder.get(convertView, mIDs.get(18));
		// View rightProgress = ViewHolder.get(convertView, mIDs.get(19));

		View leftView = ViewHolder.get(convertView, mIDs.get(20));
		View rightView = ViewHolder.get(convertView, mIDs.get(21));

		// Object message = mAryUserChatMessages.get(position);
		// Message messageEntity = getInfo(message);

		systemContent.setVisibility(View.GONE);
		systemTip.setVisibility(View.GONE);
		// if (mAryMessageCreateTimes.get(position)) {
		// if (userMsg.mIsRecv) {
		// systemTip.setText(DateUtils
		// .getTimeBylong(userMsg.mServerSendTime * 1000));
		// } else {
		// systemTip.setText(DateUtils
		// .getTimeBylong(userMsg.mClientSendTime * 1000));
		// }
		//
		// systemTip.setVisibility(View.VISIBLE);
		// } else {
		// systemTip.setVisibility(View.GONE);
		// }

		TextView userTextView = null;
		ImageView userImageView = null;

		if (teamMsg.mTeamMsgType == TeamMsgType.System) {
			leftView.setVisibility(View.GONE);
			rightView.setVisibility(View.GONE);
			systemContent.setText(teamMsg.mContent);
			systemContent.setVisibility(View.VISIBLE);
		} else {
			int messageSate = teamMsg.mStatus;

			// 如果消息是对方发给我的
			if (teamMsg.mIsRecv) {
				rightView.setVisibility(View.GONE);
				leftView.setVisibility(View.VISIBLE);

				userTextView = leftUserName;
				userImageView = leftHead;
				dealMsg(position, leftContent, leftImageRoot, leftImage,
						leftImageProgress, leftImageMask, leftAddon, teamMsg,
						messageSate, convertView);
			} else {
				leftView.setVisibility(View.GONE);
				rightView.setVisibility(View.VISIBLE);

				userTextView = rightUserName;
				userImageView = rightHead;

				dealMsg(position, rightContent, rightImageRoot, rightImage,
						rightImageProgress, rightImageMask, rightAddon, teamMsg,
						messageSate, convertView);
			}

			if (mUserNameVisible) {
				leftUserName.setVisibility(View.VISIBLE);
				rightUserName.setVisibility(View.VISIBLE);
				userTextView.setText(teamMsg.mFromCustomUserID);
			} else {
				leftUserName.setVisibility(View.GONE);
				rightUserName.setVisibility(View.GONE);
			}

			if (mUserMainPhotoVisible) {
				userImageView.setVisibility(View.VISIBLE);

				if (userImageView instanceof RoundedImageView) {
					RoundedImageView roundedImageView = (RoundedImageView) userImageView;

					roundedImageView.setCornerRadius(mUserMainPhotoCornerRadius);

					if (teamMsg.mIsRecv) {
						IMPrivateUserInfo userInfo = IMUsersMgr.getInstance()
								.getUserInfo(teamMsg.getFromUID());

						roundedImageView.setFileID(userInfo.getMainPhotoFileID());
					} else {
						roundedImageView.setFileID(IMPrivateMyself.getInstance()
								.getMainPhotoFileID());
					}
					//头像点击事件
					dealHeadPhotoClick(roundedImageView, teamMsg);
				}
			} else {
				userImageView.setVisibility(View.GONE);
			}
		}
	}
	
	/**
	 * 头像点击事件
	 * @param roundedImageView
	 * @param userMsg
	 */
	private void dealHeadPhotoClick(RoundedImageView roundedImageView, 
			final IMTeamMsg userMsg) {
		
		roundedImageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mHeadPhotoClickListener != null){
					mHeadPhotoClickListener.onClick(v,userMsg.mFromCustomUserID);
				}
			}
		});
		
	}

	/**
	 * 
	 * @param id
	 * @param contentView
	 *            文本消息---textview
	 * @param imageRoot
	 *            图片消息--- （内含 图片 和 加载进度 以及 背景）;
	 * @param imageView
	 *            图片消息--- （ 图片 ）;
	 * @param imageProgress
	 *            图片消息--- （ 加载进度 ）;
	 * @param imageMask
	 *            图片消息--- （ 背景）;
	 * @param addon
	 *            重发 图片控件
	 * @param message
	 *            消息封装体
	 * @param isComMsg
	 *            消息的方向
	 * @param isReceipted
	 * @param holder
	 */
	private void dealMsg(final int id, TextView contentView, View imageRoot,
			final ImageView imageView, final TextView imageProgress,
			final View imageMask, final ImageView addon, final IMTeamMsg teamMsg,
			int messageState, View holder) {
		String content = teamMsg.mContent;

		contentView.setOnTouchListener(null);
		contentView.setTag(null);
		contentView.setVisibility(View.GONE);
		contentView.setOnClickListener(null);
		imageRoot.setVisibility(View.GONE);

		imageView.setTag(null);
		addon.setTag(null);
		addon.setVisibility(View.INVISIBLE);
		addon.setOnClickListener(null);

		final View rightProgress = ViewHolder.get(holder, mIDs.get(19));

		rightProgress.setVisibility(View.INVISIBLE);

		if (messageState == IMChatMessage.FAILURE) { // 发送失败
			addon.setVisibility(View.VISIBLE);
			addon.setTag(id);
			addon.setImageResource(CPResourceUtil.getDrawableId(mContext,
					"im_btn_style_resend"));

			// if (mResendClickListener != null) {
			// addon.setOnClickListener(mResendClickListener);
			// }

			rightProgress.setVisibility(View.INVISIBLE);
		} else if ((messageState == IMChatMessage.SENDING_OR_RECVING)
				&& !teamMsg.mIsRecv) {
			rightProgress.setVisibility(View.VISIBLE);
		}

		/**
		 * 实例化语音控件 隐藏 语音播放动画控件，去除语音点击响应事件 < start>
		 */
		View audioAnimationView;
		View audioContentView;

		if (teamMsg.mIsRecv) {
			audioContentView = ViewHolder.get(holder,
					CPResourceUtil.getId(mContext, "left_content_p"));
			audioAnimationView = ViewHolder.get(holder,
					CPResourceUtil.getId(mContext, "chatting_item_left_audio"));
		} else {
			audioContentView = ViewHolder.get(holder,
					CPResourceUtil.getId(mContext, "right_content_p"));
			audioAnimationView = ViewHolder.get(holder,
					CPResourceUtil.getId(mContext, "chatting_item_right_audio"));
		}

		audioContentView.setOnClickListener(null);
		audioAnimationView.setVisibility(View.GONE);

		/**
		 * 实例化语音控件 隐藏 语音播放动画控件，去除语音点击响应事件 < end>
		 */

		switch (teamMsg.mTeamMsgType) {
		case Normal:
			if (content != null) {
				mGifEmotionUtils.setSpannableText(contentView, content, mHandler);
				contentView.setVisibility(View.VISIBLE);
				contentView.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
						null);
				contentView.setOnTouchListener(mOnTouchListener);
			} else {
				contentView.setText("");
				contentView.setVisibility(View.VISIBLE);
			}
			break;
		case Audio:
			long duration = teamMsg.getAudioDuration();

			contentView.setVisibility(View.VISIBLE);
			contentView.setText("");
			contentView.setText(duration + "\"");
			audioAnimationView.setVisibility(View.VISIBLE);

			final AnimationDrawable animationDrawable = (AnimationDrawable) audioAnimationView
					.getBackground();

			if (-1 != mAudioPlayingPosition && id == mAudioPlayingPosition) {
				animationDrawable.start();
			} else {
				if (animationDrawable.isRunning()) {
					animationDrawable.stop();
					animationDrawable.selectDrawable(0);
				}
			}

			OnClickListener audioClickListener = new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (id == mAudioPlayingPosition) {
						IMAudioPlayer.getInstance().stop();

						if (animationDrawable != null && animationDrawable.isRunning()) {
							animationDrawable.stop();
							animationDrawable.selectDrawable(0);
							mLastAnimationDrawable = null;
						}

						mAudioPlayingPosition = -1;
					} else {
						mAudioPlayingPosition = id;
						animationDrawable.start();

						if (mLastAnimationDrawable != null
								&& mLastAnimationDrawable.isRunning()) {
							mLastAnimationDrawable.stop();
							mLastAnimationDrawable.selectDrawable(0);
						}

						mLastAnimationDrawable = null;
						mLastAnimationDrawable = animationDrawable;

						IMAudioPlayer.getInstance().play(teamMsg,
								new OnCompletionListener() {
									@Override
									public void onCompletion(MediaPlayer mp) {
										animationDrawable.stop();
										animationDrawable.selectDrawable(0);
										mLastAnimationDrawable = null;
										mAudioPlayingPosition = -1;
									}
								});
					}
				}
			};

			audioContentView.setOnClickListener(audioClickListener);
			contentView.setOnClickListener(audioClickListener);
			break;
		case Photo:
			int width = 0;
			int height = 0;

			// if (userMsg.mExtraData != null
			// && userMsg.mExtraData.getObject("width") != null
			// && userMsg.mExtraData.getObject("height") != null) {
			// width = (Integer) userMsg.mExtraData.get("width");
			// height = (Integer) userMsg.mExtraData.get("height");
			// float scale = calculateImageScaleRatio(width, height);
			// width = (int) (width * scale);
			// height = (int) (height * scale);
			// }

			IMImagePhoto photo = IMImagesMgr.getInstance()
					.getPhoto(teamMsg.getFileID());

			imageRoot.setVisibility(View.VISIBLE);

			if (photo.getBitmap() == null) {
//				mPhotoMsgIDPositionMap.put(teamMsg.mMsgID, id);
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imageView
						.getLayoutParams();
				params.width = width == 0 ? PHOTO_WIDTH : width;
				params.height = height == 0 ? PHOTO_HEIGHT : height;
				imageView.setLayoutParams(params);
				imageView.setImageResource(CPResourceUtil.getDrawableId(mContext,
						"im_imsdk_default"));
			} else {
				final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imageView
						.getLayoutParams();

				if (width == 0 || height == 0) {
					width = photo.getWidth();
					height = photo.getHeight();

					float scale = calculateImageScaleRatio(width, height);

					width = (int) (width * scale);
					height = (int) (height * scale);
				}

				params.width = width;
				params.height = height;

				imageView.setLayoutParams(params);

				IMImageThumbnail thumbnail = IMImagesMgr.getInstance().getThumbnail(
						teamMsg.getFileID(), width, height);

				imageView.setImageBitmap(thumbnail.getBitmap());
				updatePhotoUI(id, imageProgress, 100, imageMask, addon, messageState,
						rightProgress);
			}

			break;
		default:
			DTLog.logError();
			break;
		}
	}

	private void updatePhotoUI(int id, TextView imageProgress, int pr, View imageMask,
			ImageView addon, int messageState, View rightProgress) {
		if (messageState == IMChatMessage.SENDING_OR_RECVING) {
			imageProgress.setVisibility(View.VISIBLE);
			imageMask.setVisibility(View.VISIBLE);
			imageProgress.setText(pr + "%");
		} else if (messageState == IMChatMessage.FAILURE) {
			imageProgress.setVisibility(View.GONE);
			imageMask.setVisibility(View.GONE);
			addon.setVisibility(View.VISIBLE);
			addon.setTag(id);
			addon.setImageResource(CPResourceUtil.getDrawableId(mContext,
					"im_btn_style_resend"));
			// if (mResendClickListener != null) {
			// addon.setOnClickListener(mResendClickListener);
			// }
			rightProgress.setVisibility(View.INVISIBLE);
		} else if (messageState == IMChatMessage.SUCCESS) {
			imageProgress.setVisibility(View.GONE);
			imageMask.setVisibility(View.GONE);
		}
	}
}
