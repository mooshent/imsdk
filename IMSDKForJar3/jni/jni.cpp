#ifdef __cplusplus
extern "C" {
#endif

#include <stdafx.h>
#include <string.h>
#include <jni.h>
#include "mytcp.h"

#include <math.h>

#ifdef BUILD_FROM_SOURCE
#include <utils/Log.h>
#else
#include <android/log.h>
#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , LOG_TAG, __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO   , LOG_TAG, __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN   , LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR  , LOG_TAG, __VA_ARGS__)
#endif

#define JNI_COPY    0

/*************************************************************************************************/
/* 动态库版本号 */
const int SO_VERSION_CODE = 4;
/* 包名 + 类名 */
const char *pClassPathName = "am/core/jni/IMProtocol";
/* 销毁内存 */
#define JNI_DELETE(ptr)	{if(ptr != NULL){delete ptr;ptr = NULL;}}

/*************************************************************************************************/

/* Test Flags */
//#define JNI_DEBUG_JAVA_LOGCAT

void JNI_DEBUG_LOGCAT(const char* pMessage) {
#ifdef JNI_DEBUG_JAVA_LOGCAT
	LOGI("%s", pMessage);
#endif
}

/*************************************************************************************************/
static CMyTcp* s_pTcpObject;

JNIEXPORT jint JNICALL InitIMSDK(JNIEnv *env, jclass cls, jstring ip, jint port) {
	JNI_DEBUG_LOGCAT("InitIMSDK");

	if (s_pTcpObject) {
		delete s_pTcpObject;
		s_pTcpObject = NULL;
	}

	JNI_DEBUG_LOGCAT("InitIMSDK2");
	s_pTcpObject = new CMyTcp();

	JNI_DEBUG_LOGCAT("InitIMSDK3");
	jint ret = -1;
	char *pTmp = NULL;
	const char *nativeIp = env->GetStringUTFChars(ip, 0);
	int len = env->GetStringUTFLength(ip);

	JNI_DEBUG_LOGCAT(nativeIp);

	JNI_DEBUG_LOGCAT("InitIMSDK4");
	if (nativeIp == NULL || len <= 0) {
		goto Exit;
	}

	pTmp = new char[len + 2];

	if (pTmp == NULL) {
		goto Exit;
	}

	JNI_DEBUG_LOGCAT("InitIMSDK5");
	memset(pTmp, 0x00, len + 2);
	memcpy(pTmp, nativeIp, len);

	JNI_DEBUG_LOGCAT("InitIMSDK6");
	ret = (jint)((int)s_pTcpObject->Init(pTmp, (int)port));

	JNI_DEBUG_LOGCAT("InitIMSDK7");

	Exit: if (nativeIp != NULL) {
		env->ReleaseStringUTFChars(ip, nativeIp);
		nativeIp = NULL;
	}

	JNI_DEBUG_LOGCAT("InitIMSDK8");
	JNI_DELETE(pTmp);
	return ret;
}

JNIEXPORT jstring JNICALL GetErrorMessage(JNIEnv *env, jclass cls) {
	JNI_DEBUG_LOGCAT("GetErrorMessage");

	if (s_pTcpObject == NULL) {
		return env->NewStringUTF("mConnection is null");
	}

	char* errmsg = s_pTcpObject->GetErrMsg();
	return env->NewStringUTF((const char*)errmsg);
}

JNIEXPORT jint JNICALL GetSdkVersion(JNIEnv *env, jclass cls) {
	return SO_VERSION_CODE;
}

JNIEXPORT jint JNICALL SendIMSDK(JNIEnv *env, jclass cls, jbyteArray buf, jint offset, jint size) {
	if (s_pTcpObject == NULL) {
		return -1;
	}

	jint ret = -1;
	jbyte* pBuffer = env->GetByteArrayElements(buf, 0);

	if (NULL == pBuffer) {
		return -1;
	}

	ret = (jint)s_pTcpObject->Send((char*)pBuffer + offset, size);

	Exit: if (pBuffer != NULL) {
		env->ReleaseByteArrayElements(buf, (jbyte *)pBuffer, JNI_ABORT);
		pBuffer = NULL;
	}

	return ret;
}

JNIEXPORT jint JNICALL RecvIMSDK(JNIEnv *env, jclass cls, jbyteArray buf, jint offset, jint size) {
	if (s_pTcpObject == NULL) {
		return -1;
	}

	jint ret = -1;
	unsigned char isCopy = 1;
	jbyte* pTempBuffer = new jbyte[size + 2];

	ret = (jint)s_pTcpObject->Recv((char *)pTempBuffer, size);

	if (ret > 0) {
		env->SetByteArrayRegion(buf, offset, ret, pTempBuffer);
	}

	Exit: if (pTempBuffer != NULL) {
		delete pTempBuffer;
	}

	return ret;
}

JNIEXPORT jint JNICALL Close(JNIEnv *env, jclass cls) {
	JNI_DEBUG_LOGCAT("Close");

	if (s_pTcpObject == NULL) {
		return -1;
	}

	JNI_DEBUG_LOGCAT("begin delete");
	delete s_pTcpObject;
	JNI_DEBUG_LOGCAT("end delete");
	s_pTcpObject = NULL;

	return 0;
}

/************************************************************************************************
 JNI 注册
 ************************************************************************************************/

JNINativeMethod methods[] = {

{ "RecvIMSDK", "([BII)I", (void*)RecvIMSDK },

{ "SendIMSDK", "([BII)I", (void*)SendIMSDK },

{ "Close", "()I", (void*)Close },

{ "InitIMSDK", "(Ljava/lang/String;I)I", (void*)InitIMSDK },

{ "GetErrorMessage", "()Ljava/lang/String;", (void*)GetErrorMessage },

{ "GetSdkVersion", "()I", (void*)GetSdkVersion } };

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *jvm, void *reserved) {
	JNIEnv* env = NULL;

	if (jvm->GetEnv((void**)&env, JNI_VERSION_1_4) != JNI_OK) {
		return JNI_ERR;
	}

	jclass clazz = env->FindClass(pClassPathName);

	if (clazz == NULL) {
		return JNI_ERR;
	}

	int count = sizeof(methods) / sizeof(methods[0]);

	if (env->RegisterNatives(clazz, methods, count) < 0) {
		return JNI_ERR;
	}

	return JNI_VERSION_1_4;
}

#ifdef __cplusplus
}
#endif
