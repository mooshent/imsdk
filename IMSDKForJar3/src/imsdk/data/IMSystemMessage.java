package imsdk.data;

public final class IMSystemMessage {
	public long mServerSendTime;
	public String mContent = "";
	public String mCustonUserId = "";
}
