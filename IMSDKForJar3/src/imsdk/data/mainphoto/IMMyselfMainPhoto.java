package imsdk.data.mainphoto;

import imsdk.data.IMMyself.OnInitializedListener;
import imsdk.data.IMSDK.OnActionProgressListener;
import am.imsdk.model.a2.IMMyselfMainPhoto2;
import android.graphics.Bitmap;
import android.net.Uri;

public final class IMMyselfMainPhoto {
	public static boolean isInitialized() {
		return IMMyselfMainPhoto2.isInitialized();
	}

	public static void setOnInitializedListener(OnInitializedListener l) {
		IMMyselfMainPhoto2.setOnInitializedListener(l);
	}

	public static void upload(Bitmap bitmap, final OnActionProgressListener l) {
		IMMyselfMainPhoto2.upload(bitmap, l);
	}

	public static Bitmap get() {
		return IMMyselfMainPhoto2.get();
	}

	public static Uri getLocalUri() {
		return IMMyselfMainPhoto2.getLocalUri();
	}

	public static byte[] getLocalBuffer() {
		return IMMyselfMainPhoto2.getLocalBuffer();
	}

	public static Bitmap get(int width, int height) {
		return IMMyselfMainPhoto2.get(width, height);
	}

	public static Uri getLocalUri(int width, int height) {
		return IMMyselfMainPhoto2.getLocalUri(width, height);
	}

	public static byte[] getLocalBuffer(int width, int height) {
		return IMMyselfMainPhoto2.getLocalBuffer(width, height);
	}

	public static String getLastError() {
		return IMMyselfMainPhoto2.getLastError();
	}
}
