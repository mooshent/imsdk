package imsdk.data;

import java.util.HashMap;

import am.imsdk.model.a2.IMMyself2;
import android.content.Context;
import android.graphics.Bitmap;

public class IMMyself {
	public interface OnReceiveTextListener {
		public void onReceiveText(String text, String fromCustomUserID, String nickName,
				long serverActionTime, String extraData);

		public void onReceiveSystemText(String fromCustomUserID, String text, long serverActionTime);
		
		public void onReceiveAudio(String messageID, String fromCustomUserID, String nickName,
				long serverActionTime, String extraData);
	}

	public interface OnReceiveBitmapListener {
		public void onReceiveBitmapMessage(String messageID, String fromCustomUserID, String nickName,
				long serverActionTime, String extraData);

		public void onReceiveBitmap(Bitmap bitmap, String messageID,
				String fromCustomUserID, long serverActionTime);

		public void onReceiveBitmapProgress(double progress, String messageID,
				String fromCustomUserID, long serverActionTime);
	}
	
	public interface OnReceiveOrderListener {
		public void onReceiveOrder(String text, String fromCustomUserID, String nickName, 
				long serverActionTime, String extraData);
	}

	public interface OnConnectionChangedListener {
		public void onDisconnected(boolean loginConflict);

		public void onReconnected();
	}

	public interface OnAutoLoginListener {
		public void onAutoLoginBegan();

		public void onAutoLoginSuccess();

		public void onAutoLoginFailure(boolean loginConflict);
	}

	public interface OnActionListener {
		public void onSuccess();

		public void onFailure(String error);
	}

	public interface OnActionResultListener {
		public void onSuccess(Object result);

		public void onFailure(String error);
	}

	public interface OnActionProgressListener {
		public void onSuccess();

		public void onProgress(double progress);

		public void onFailure(String error);
	}

	public interface OnLoginStatusChangedListener {
		public void onLoginStatusChanged(LoginStatus oldLoginStatus,
				LoginStatus newLoginStatus);
	}

	public interface OnInitializedListener {
		public void onInitialized();
	}
	
	public interface OnCheckServiceLoginStateListener {
		public void onConnected();
		
		public void onNoConnection();
	}

	/**
	 * @method
	 * @brief 初始化登录信息，可能触发自动登录
	 * @param applicationContext
	 *            Android应用上下文环境
	 * @param appKey
	 *            应用标识，不能为空，开发者需要填写从www.IMSDK.im官网注册时获取的appkey
	 */
	public static boolean init(Context applicationContext) {
		return IMMyself2.init(applicationContext);
	}

	// 可能触发退出登录
	public static boolean setCustomUserID(String customUserID) {
		return IMMyself2.setCustomUserID(customUserID);
	}

	// 可能触发退出登录
	public static boolean setPassword(String password) {
		return IMMyself2.setPassword(password);
	}

	public static String getPassword() {
		return IMMyself2.getPassword();
	}

	public static boolean isLogined() {
		return IMMyself2.isLogined();
	}

	public static long register(long timeoutInterval, final OnActionListener l) {
		return IMMyself2.register(timeoutInterval, l);
	}

	public static void login() {
		IMMyself2.login();
	}

	public static void login(boolean autoRegister, long timeoutInterval,
			final OnActionListener l) {
		IMMyself2.login(autoRegister, timeoutInterval, l);
	}
	
	public static void checkServiceLoginStatus(final OnCheckServiceLoginStateListener l) {
		IMMyself2.checkServiceLoginStatus(l);
	}

	public static void logout() {
		IMMyself2.logout();
	}
	
	public static void stopPushService() {
		IMMyself2.stopSocketService();
	}

	public static long sendText(String text, String toCustomUserID) {
		return IMMyself2.sendText(text, toCustomUserID);
	}

	public static long sendText(String text, String toCustomUserID,
			long timeoutInterval, final OnActionListener l) {
		return IMMyself2.sendText(text, toCustomUserID, timeoutInterval, l);
	}
	
	public static long sendText(String text, String toCustomUserID, HashMap<String, Object> tags,
			long timeoutInterval, final OnActionListener l) {
		return IMMyself2.sendText(text, toCustomUserID, tags, timeoutInterval, l);
	}
	
	public static long sendNoticeMsg(String text, String toCustomUserID, HashMap<String, Object> tags,
			long timeoutInterval, final OnActionListener l) {
		return IMMyself2.sendNoticeMsg(text, toCustomUserID, tags, timeoutInterval, l);
	}
	
	/**
	 * 
	 * @Title: sendOrderState 
	 * @Description: 发送结束订单请求
	 * @param orderId 订单ID
	 * @return long    返回类型 
	 * @author 方子君
	 */
	public static long sendRequestForOrderFinish(String toCustomUserID, long orderId,
			long timeoutInterval, final OnActionListener l) {
		HashMap<String, Object> tag = new HashMap<String, Object>();
		tag.put("orderId", orderId);
		
		return IMMyself2.sendOrderState(0, toCustomUserID, tag, timeoutInterval, l);
	}
	
	public static boolean startRecording(String toCustomUserID) {
		return IMMyself2.startRecording(toCustomUserID);
	}

	public static long stopRecording(boolean needSend, HashMap<String, Object> tags, final long timeoutInterval,
			final OnActionListener l) {
		return IMMyself2.stopRecording(needSend, tags, timeoutInterval, l);
	}

	public static long sendBitmap(final Bitmap bitmap, final String toCustomUserID, 
			final long timeoutInterval, final OnActionProgressListener l) {
		return IMMyself2.sendBitmap(bitmap, toCustomUserID, timeoutInterval, l);
	}
	
	public static long sendBitmap(final Bitmap bitmap, final String toCustomUserID, HashMap<String, Object> tags, 
			final long timeoutInterval, final OnActionProgressListener l) {
		return IMMyself2.sendBitmap(bitmap, toCustomUserID, tags, timeoutInterval, l);
	}

	public static void setListener(IMMyself.IMMyselfListener listener) {
		IMMyself2.setListener(listener);
	}

	public static String getCustomUserID() {
		return IMMyself2.getCustomUserID();
	}

	public static String getAppKey() {
		return IMMyself2.getAppKey();
	}
	
	public static void setNickName(String nickName) {
		IMMyself2.setNickName(nickName);
	}

	public enum LoginStatus {
		// 未登录
		None(0), Logining(1), Reconnecting(2), AutoLogining(4),

		// 已登录
		Logined(11);

		private final int value;

		private LoginStatus(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
		
		public static LoginStatus getIndex(int index) {
			switch (index) {
			case 1:
				return Logining;
			case 2:
				return Reconnecting;
			case 4:
				return AutoLogining;
			case 11:
				return Logined;
			case 0:
			default:
				return None;
			}
		}
	}

	public static LoginStatus getLoginStatus() {
		return IMMyself2.getLoginStatus();
	}

	public interface IMMyselfListener {
		// actions
		public void onActionSuccess(String actionType, long clientActionTime);

		public void onActionFailure(String actionType, String error,
				long clientActiontime);

		// login
		public void onDisconnected(boolean loginConflict);

		public void onReconnected();

		// text
		public void onReceiveText(String text, String fromCustomUserID,
				long serverActionTime);
		
		// audio
		public void onReceiveAudio(String messageID, String fromCustomUserID,
				long serverActionTime);

		// system text
		public void onReceiveSystemText(String fromCustomUserId, String text, long serverActionTime);

		// bitmap
		public void onReceiveBitmapMessage(String messageID, String fromCustomUserID,
				long serverActionTime);

		public void onReceiveBitmap(Bitmap bitmap, String messageID,
				String fromCustomUserID, long serverActionTime);

		public void onReceiveBitmapProgress(double progress, String messageID,
				String fromCustomUserID, long serverActionTime);
	}

	public static void setOnReceiveTextListener(OnReceiveTextListener l) {
		IMMyself2.setOnReceiveTextListener(l);
	}

	public static void setOnReceiveBitmapListener(OnReceiveBitmapListener l) {
		IMMyself2.setOnReceiveBitmapListener(l);
	}
	
	public static void setOnReceiveOrderListener(OnReceiveOrderListener l) {
		IMMyself2.setOnReceiveOrderListener(l);
	}

	public static void setOnConnectionChangedListener(OnConnectionChangedListener l) {
		IMMyself2.setOnConnectionChangedListener(l);
	}

	public static void setOnLoginStatusChangedListener(OnLoginStatusChangedListener l) {
		IMMyself2.setOnLoginStatusChangedListener(l);
	}
}
