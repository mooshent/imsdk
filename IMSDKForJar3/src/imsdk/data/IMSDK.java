package imsdk.data;

import am.imsdk.model.IMParamJudge;

public final class IMSDK {
	public static int getVersionID() {
		return 132;
	}

	public static String getVersionNumber() {
		return "1.3.3";
	}

	public static String getVersionName() {
		return "通知消息";
	}

	public static String getLastError() {
		return IMParamJudge.getLastError();
	}

	public interface OnDataChangedListener {
		public void onDataChanged();
	}

	public interface OnActionProgressListener {
		public void onSuccess();

		public void onProgress(double progress);

		public void onFailure(String error);
	}
}
