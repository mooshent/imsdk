package imsdk.data.customuserinfo;

import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMSDK.OnDataChangedListener;
import am.imsdk.model.a2.IMSDKCustomUserInfo2;

public final class IMSDKCustomUserInfo {
	public static void addOnDataChangedListener(final String customUserID,
			final OnDataChangedListener l) {
		IMSDKCustomUserInfo2.addOnDataChangedListener(customUserID, l);
	}

	public static void removeOnDataChangedListener(final OnDataChangedListener l) {
		IMSDKCustomUserInfo2.removeOnDataChangedListener(l);
	}

	public static String get(final String customUserID) {
		return IMSDKCustomUserInfo2.get(customUserID);
	}

	public static long request(final String customUserID, final OnActionListener l) {
		return IMSDKCustomUserInfo2.request(customUserID, l);
	}
	
	public static String getLastError() {
		return IMSDKCustomUserInfo2.getLastError();
	}
}
