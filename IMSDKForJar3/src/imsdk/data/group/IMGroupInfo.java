package imsdk.data.group;

import imsdk.data.IMMyself.OnActionListener;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.team.IMCmdTeamGetInfo;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedListener;
import am.imsdk.action.group.IMActionGroupSetInfo;
import am.imsdk.action.group.IMActionUserRequestUpdateMemberList;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import am.imsdk.model.userinfo.IMUsersMgr;

public final class IMGroupInfo {
	private long mTeamID;
	private IMPrivateTeamInfo mTeamInfo;
	private String mGroupName = "";
	private String mCustomGroupInfo = "";

	public IMGroupInfo() {
		mTeamID = IMTeamsMgr.getInstance().getCreatingGroupInfoTeamID();

		if (mTeamID == 0) {
			return;
		}

		mTeamInfo = IMTeamsMgr.getInstance().getTeamInfo(mTeamID);

		mGroupName = mTeamInfo.mTeamName;
		mCustomGroupInfo = mTeamInfo.mExInfo;

		update();

		DTNotificationCenter.getInstance().addObserver("TeamUpdated:" + mTeamID,
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						IMGroupInfo.this.update();
					}
				});
	}

	private void update() {
		DTLog.sign("GroupInfo update");
		DTLog.sign("mTeamID:" + mTeamID);

		if (mTeamID == 0) {
			DTLog.logError();
			return;
		}

		IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(mTeamID);

		mGroupName = teamInfo.mTeamName;
		mCustomGroupInfo = teamInfo.mExInfo;
		DTLog.sign("teamInfo.getUIDList():" + teamInfo.getUIDList());
	}

	public String getGroupID() {
		if (mTeamInfo == null) {
			DTLog.logError();
			return "";
		}

		return DTTool.getSecretString(mTeamInfo.mTeamID);
	}

	public int getMaxUserCount() {
		if (mTeamInfo == null) {
			DTLog.logError();
			return 0;
		}

		return (int) mTeamInfo.mMaxCount;
	}

	public String getOwnerCustomUserID() {
		if (mTeamInfo == null) {
			DTLog.logError();
			return "";
		}

		String result = IMUsersMgr.getInstance().getCustomUserID(mTeamInfo.mOwnerUID);

		if (!IMParamJudge.isCustomUserIDLegal(result)) {
			DTLog.logError();
			return "";
		}

		return result;
	}

	public String getGroupName() {
		return mGroupName;
	}

	public void setGroupName(String groupName) {
		mGroupName = groupName;
	}

	public String getCustomGroupInfo() {
		return mCustomGroupInfo;
	}

	public void setCustomGroupInfo(String customGroupInfo) {
		mCustomGroupInfo = customGroupInfo;
	}

	public ArrayList<String> getMemberList() {
		if (mTeamInfo == null) {
			DTLog.logError();
			return new ArrayList<String>();
		}

		ArrayList<String> result = mTeamInfo.getCustomUserIDsList();
		
		if (IMMyselfGroup.isInitialized() && result.size() == 0) {
			DTLog.logError();
		}

		return result;
	}

	public long requestUpdateGroupInfo(final OnActionListener l) {
		long actionTime = System.currentTimeMillis() / 1000;

		if (!IMPrivateMyself.getInstance().getGroupInfoInited()) {
			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure("group module not initialized yet.");
					}
				});
			}

			return actionTime;
		}

		if (mTeamID == 0) {
			DTLog.logError();
			return actionTime;
		}

		final IMCmdTeamGetInfo cmd = new IMCmdTeamGetInfo();

		cmd.addTeamID(mTeamID);

		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
			@Override
			public void onCommonFailed(FailedType type, long errorCode,
					JSONObject errorJsonObject) throws JSONException {
				if (l != null) {
					switch (type) {
					case SendFailed:
					case NoRecv:
						l.onFailure(type.toString());
						break;
					case RecvError:
						l.onFailure(errorJsonObject != null ? errorJsonObject
								.toString() : "Error code:" + errorCode);
						break;
					default:
						DTLog.logError();
						break;
					}
				}
			}
		};

		cmd.mOnRecvEndListener = new OnRecvEndListener() {
			@Override
			public void onRecvEnd(JSONObject jsonObject) throws JSONException {
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		cmd.send();
		return actionTime;
	}

	public long requestUpdateMemberList(final OnActionListener l) {
		long actionTime = System.currentTimeMillis() / 1000;

		if (!IMPrivateMyself.getInstance().getGroupInfoInited()) {
			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure("group module not initialized yet.");
					}
				});
			}

			return actionTime;
		}

		if (mTeamID == 0) {
			DTAppEnv.getMainHandler().post(new Runnable() {
				@Override
				public void run() {
					l.onFailure("IMSDK Error");
				}
			});

			DTLog.logError();
			return actionTime;
		}

		IMActionUserRequestUpdateMemberList action = new IMActionUserRequestUpdateMemberList();

		action.mGroupID = getGroupID();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		action.begin();
		return actionTime;
	}

	public long commitGroupInfo(final OnActionListener l) {
		long actionTime = System.currentTimeMillis() / 1000;

		if (!IMPrivateMyself.getInstance().getGroupInfoInited()) {
			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure("group module not initialized yet.");
					}
				});
			}

			return actionTime;
		}

		if (mTeamID == 0) {
			DTAppEnv.getMainHandler().post(new Runnable() {
				@Override
				public void run() {
					l.onFailure("IMSDK Error");
				}
			});

			DTLog.logError();
			return actionTime;
		}

		IMActionGroupSetInfo action = new IMActionGroupSetInfo();

		action.mGroupID = getGroupID();
		action.mGroupName = mGroupName;
		action.mExInfo = mCustomGroupInfo;

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		action.begin();
		return actionTime;
	}
}
