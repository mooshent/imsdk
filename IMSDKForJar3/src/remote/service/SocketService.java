package remote.service;

import remote.service.data.IMRemoteHeartBeat;
import remote.service.data.IMRemoteMyself;
import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.ProcessTool;
import am.dtlib.model.d.DTDevice;
import am.imsdk.aacmd.IMSocket;
import am.imsdk.action.IMActionRemoteLogin;
import am.imsdk.action.IMActionRemoteLogin.IMActionLoginType;
import am.imsdk.action.fileserver.IMActionFile;
import am.imsdk.model.IMAppSettings;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.text.TextUtils;

public class SocketService extends Service {
	public static final String ACTION = "imsdk.service.remote.SocketService";

	@Override
	public IBinder onBind(Intent arg0) {
		return MainToSocketProcessStub.getInstance();
	}

	@Override
	public boolean onUnbind(Intent intent) {
		return super.onUnbind(intent);
	}

	public void onCreate() {
		super.onCreate();

		// 环境配置
		DTAppEnv.setContext(getApplicationContext());
		DTAppEnv.setRootDirectory("IMSDK.im");
		DTLog.checkCreateDirectory(DTAppEnv.getIMSDKDirectoryFullPath());
		
		NetConnectedObserver.getInstance().run();

		IMRemoteHeartBeat.getInstance().heartbeat();

		String address = "192.168.20.51";
//		String address = "112.74.66.141";

		DTDevice.getInstance();
		DTDevice.getInstance().mLastSISAddress = address + ":9100";
		DTDevice.getInstance().mLastIMAddress = address + ":9100";
		IMActionFile.IM_FILE_URL_DOMAIN_NAME = address;
		IMActionFile.IM_FILE_URL_REAL_IP = address;
		IMSocket.getInstance().setSISDefaultAddress(address + ":9100");
		IMSocket.getInstance().setDefaultIMAddress(address + ":9100");

		IMRemoteMyself.getInstance();
		
		IMAppSettings.newInstance();
		IMAppSettings.getInstance().readFromFile();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		DTAppEnv.cancelPreviousPerformRequest(mCheckRunnable);
		DTAppEnv.performAfterDelayOnUIThread(30, mCheckRunnable);
		
		return super.onStartCommand(intent, flags, startId);
	}
	
	private Runnable mCheckRunnable = new Runnable() {
		
		@Override
		public void run() {
			checkAutoConnect();
		}
	};

	private void checkAutoConnect() {
		DTAppEnv.cancelPreviousPerformRequest(mCheckRunnable);
		
		if (!ProcessTool.isClientProcessExist(getApplicationContext())) {
			String customUserId = IMAppSettings.getInstance().mLastLoginCustomUserID;
			String pwd = IMAppSettings.getInstance().mLastLoginPassword;

			if (!TextUtils.isEmpty(customUserId) && !TextUtils.isEmpty(pwd)) {
				IMActionRemoteLogin.newInstance();
				IMActionRemoteLogin.getInstance().setType(IMActionLoginType.AutoLogin);
				IMActionRemoteLogin.getInstance().mTimeoutInterval = 5;
				IMActionRemoteLogin.getInstance().mUID = 0;
				IMActionRemoteLogin.getInstance().mCustomUserID = customUserId;
				IMActionRemoteLogin.getInstance().mAppKey = "";
				IMActionRemoteLogin.getInstance().setPassword(pwd);
				IMActionRemoteLogin.getInstance().begin();
			}
		}
	}
}
