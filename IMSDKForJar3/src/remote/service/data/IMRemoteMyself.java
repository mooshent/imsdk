package remote.service.data;

import imsdk.data.IMMyself.LoginStatus;
import imsdk.data.IMSystemMessage;

import java.util.Observable;
import java.util.Observer;

import remote.service.MainToSocketProcessStub;
import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTSocket;
import am.dtlib.model.c.socket.DTSocket.DTSocketStatus;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.aacmd.IMSocket;
import am.imsdk.action.IMActionRemoteLogin;
import am.imsdk.action.IMActionRemoteLogin.IMActionLoginType;
import am.imsdk.action.ammsgs.IMActionRecvOffLineMsg;
import am.imsdk.demo.util.CPResourceUtil;
import am.imsdk.model.IMAppSettings;
import am.imsdk.model.IMChatSetting;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

public class IMRemoteMyself {
	private LoginStatus mRemoteLoginStatus = LoginStatus.None;
	
//	private long mSessionID;
	private long mUID;
	
//	public long getSessionID() {
//		return mSessionID;
//	}

//	public void setSessionID(long mSession) {
//		this.mSessionID = mSession;
//	}
	
	public long getUID() {
		return mUID;
	}

	public void setUID(long mUID) {
		this.mUID = mUID;
	}

	public LoginStatus getRemoteLoginStatus() {
		return mRemoteLoginStatus;
	}
	
	public void setRemoteLoginStatus(LoginStatus newStatus) {
		mRemoteLoginStatus = newStatus;
		
		if(MainToSocketProcessStub.getInstance().getLoginStatusChangedListener() != null) {
			try {
				MainToSocketProcessStub.getInstance().getLoginStatusChangedListener().onLoginStatusChanged(mRemoteLoginStatus.getValue());
			} catch (RemoteException e) {
				e.printStackTrace();
				MainToSocketProcessStub.getInstance().onDestory();
			}
		}
	}
	
	private static void tryAutoLogin() {
		String uid = IMAppSettings.getInstance().mLastLoginCustomUserID;
		String pwd = IMAppSettings.getInstance().mLastLoginPassword;
		
		if(TextUtils.isEmpty(uid) || TextUtils.isEmpty(pwd)) {
			return;
		}
		
		IMActionRemoteLogin.newInstance();
		IMActionRemoteLogin.getInstance().setType(IMActionLoginType.AutoLogin);
		IMActionRemoteLogin.getInstance().mTimeoutInterval = 5;
		IMActionRemoteLogin.getInstance().mUID = 0;
		IMActionRemoteLogin.getInstance().mCustomUserID = uid;
		IMActionRemoteLogin.getInstance().mAppKey = "";
		IMActionRemoteLogin.getInstance().setPassword(pwd);
		IMActionRemoteLogin.getInstance().begin();
	}

	private static void tryReconnect() {
		DTLog.sign("tryReconnect");
		
		if (IMRemoteMyself.getInstance().getRemoteLoginStatus() == LoginStatus.Logined) {
			if (IMSocket.getInstance().getStatus() == DTSocketStatus.Connected) {
				DTLog.logError();
				return;
			}

			IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.Reconnecting);
		}
		
		if (IMRemoteMyself.getInstance().getRemoteLoginStatus() != LoginStatus.Reconnecting) {
			DTLog.logError();
			return;
		}
		
		String uid = IMAppSettings.getInstance().mLastLoginCustomUserID;
		String pwd = IMAppSettings.getInstance().mLastLoginPassword;
		
		if(TextUtils.isEmpty(uid) || TextUtils.isEmpty(pwd)) {
			return;
		}
		
		IMActionRemoteLogin.newInstance();
		IMActionRemoteLogin.getInstance().setType(IMActionLoginType.Reconnect);
		IMActionRemoteLogin.getInstance().mTimeoutInterval = 5;
		IMActionRemoteLogin.getInstance().mUID = IMRemoteMyself.getInstance().getUID();
		IMActionRemoteLogin.getInstance().mCustomUserID = uid;
		IMActionRemoteLogin.getInstance().mAppKey = "";
		IMActionRemoteLogin.getInstance().setPassword(pwd);
		IMActionRemoteLogin.getInstance().begin();
	}

	private static Observer sSocketUpdatedObserver = new Observer() {
		@Override
		public void update(Observable observable, Object data) {
			DTSocketStatus status = DTSocket.getInstance().getStatus();
			
			Log.e("Debug", "IMRemoteMyself---sSocketUpdatedObserver");
			DTLog.sign("IMRemoteMyself.getInstance().getRemoteLoginStatus():"
					+ IMRemoteMyself.getInstance().getRemoteLoginStatus());

			if (IMActionRemoteLogin.getInstance() != null) {
				DTLog.sign("IMActionRemoteLogin.getInstance().getType():"
						+ IMActionRemoteLogin.getInstance().getType());
			}

			DTLog.sign("IMSocket.getInstance().getStatusWithoutSync():"
					+ status);

			switch (IMRemoteMyself.getInstance().getRemoteLoginStatus()) {
			case None:
				break;
			case Logining: {
				if (IMActionRemoteLogin.getInstance() == null) {
					DTLog.logError();
					return;
				}

				if (IMActionRemoteLogin.getInstance().getType() != IMActionLoginType.OneKeyLogin
						&& IMActionRemoteLogin.getInstance().getType() != IMActionLoginType.OneKeyRegister) {
					DTLog.logError();
					return;
				}

				if (IMActionRemoteLogin.getInstance().isOver()) {
					DTLog.logError();
					return;
				}
			}
				break;
			case Reconnecting:
				if (IMActionRemoteLogin.getInstance() == null) {
					tryReconnect();
				}

				if (IMActionRemoteLogin.getInstance() == null) {
					DTLog.logError();
					return;
				}

				if (IMActionRemoteLogin.getInstance().getType() != IMActionLoginType.Reconnect) {
					DTLog.logError();
					return;
				}

				if (IMActionRemoteLogin.getInstance().isOver()) {
					DTLog.logError();
					return;
				}
				break;
			case AutoLogining: {
				if (IMActionRemoteLogin.getInstance() != null) {
					DTLog.sign("LoginType:" + IMActionRemoteLogin.getInstance().getType());
				}

				// reinit的时候被清理掉
				if (IMActionRemoteLogin.getInstance() == null
						|| IMActionRemoteLogin.getInstance().getType() == IMActionLoginType.None) {
					if (status != DTSocketStatus.Connected) {
						tryAutoLogin();
						return;
					}

					tryAutoLogin();
				}

				if (IMActionRemoteLogin.getInstance() == null) {
					DTLog.logError();
					return;
				}

				if (IMActionRemoteLogin.getInstance().getType() != IMActionLoginType.AutoLogin) {
					DTLog.logError();
					DTLog.log(IMActionRemoteLogin.getInstance().getType().toString());
					return;
				}

				if (IMActionRemoteLogin.getInstance().isOver()) {
					DTLog.logError();
					return;
				}
			}
				break;
			case Logined:
				switch (status) {
				case None:
				case Connecting: {
					// 断线重连
					if (IMActionRemoteLogin.getInstance() != null) {
//						DTLog.logError();
//						return;
						
						IMActionRemoteLogin.newInstance();
					}
					
					//网络不通，不用重试
					if(!DTAppEnv.isNetworkConnected()) {
						IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
						return;
					}

					tryReconnect();
				}
					break;
				case Connected: {
					DTLog.logError();
				}
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
		}

	};
	
	
	public static NotificationManager manager=null;
	
	private static void showNotification(String toCustomUserID, String nickName, String content, long serverSendTime) {
		if (manager == null) {
			manager = (NotificationManager) DTAppEnv.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
		}
		
		manager.cancel(Integer.valueOf(toCustomUserID));

		Intent intent = new Intent();
		intent.setAction("imsdk.ui.waitingActivity");
		intent.putExtra("CustomUserID", toCustomUserID);

		// 创建一个PendingIntent，和Intent类似，不同的是由于不是马上调用，需要在下拉状态条出发的activity，所以采用的是PendingIntent,即点击Notification跳转启动到哪个Activity
		PendingIntent pendingIntent = PendingIntent.getActivity(DTAppEnv.getContext(), 0, intent, 0);
		// 下面需兼容Android 2.x版本是的处理方式
		// Notification notify1 = new Notification(R.drawable.message,
		// "TickerText:" + "您有新短消息，请注意查收！", System.currentTimeMillis());
		Notification notify1 = new Notification();
		notify1.icon = CPResourceUtil.getDrawableId(DTAppEnv.getContext(), "ic_launcher");
		
		if(IMChatSetting.getInstance().isMsgSound()) {
			notify1.sound=Uri.parse("android.resource://" + DTAppEnv.getContext().getPackageName() + "/" + CPResourceUtil.getRawId(DTAppEnv.getContext(), "notify")); 
		}
		 
		if(IMChatSetting.getInstance().isMsgVibrate()) {
			notify1.vibrate = new long[] {0,100,200,300};
		}
		
		notify1.tickerText = "您有新消息，请注意查收！";
		notify1.when = serverSendTime;
		notify1.setLatestEventInfo(DTAppEnv.getContext(), nickName, content, pendingIntent);
		notify1.number = 1;
		notify1.flags |= Notification.FLAG_AUTO_CANCEL;
		// FLAG_AUTO_CANCEL表明当通知被用户点击时，通知将被清除。
		// 通过通知管理器来发起通知。如果id不同，则每click，在statu那里增加一个提示
		manager.notify(Integer.valueOf(toCustomUserID), notify1);
	}
	
	static {
		DTNotificationCenter.getInstance().addObserver("SOCKET_UPDATED",
				sSocketUpdatedObserver);
		
		DTNotificationCenter.getInstance().addObserver("Remote_Logined", new Observer() {
			
			@Override
			public void update(Observable observable, Object data) {
				IMActionRecvOffLineMsg action = new IMActionRecvOffLineMsg();
				action.begin();
			}
		});
		
		DTNotificationCenter.getInstance().addObserver("DTNetConnected", new Observer() {
			
			@Override
			public void update(Observable observable, Object data) {
				IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.Reconnecting);
				
				if (IMActionRemoteLogin.getInstance() == null) {
					tryReconnect();
				}

				if (IMActionRemoteLogin.getInstance() == null) {
					DTLog.logError();
					return;
				}

				if (IMActionRemoteLogin.getInstance().getType() != IMActionLoginType.Reconnect) {
					DTLog.logError();
					return;
				}

				if (IMActionRemoteLogin.getInstance().isOver()) {
					DTLog.logError();
					return;
				}
			}
		});
		
		DTNotificationCenter.getInstance().addObserver("IMReceiveText", new Observer() {
			@Override
			public void update(Observable observable, Object data) {
				if (!(data instanceof IMUserMsg)) {
					DTLog.logError();
					return;
				}

				IMUserMsg userMsg = (IMUserMsg) data;

				if (!userMsg.mIsRecv) {
					DTLog.logError();
					return;
				}

				if (userMsg.mUserMsgType != UserMsgType.Normal) {
					DTLog.logError();
					return;
				}

				if (getInstance().getRemoteLoginStatus() != LoginStatus.Logined) {
					return;
				}

				if (IMRemoteMyself.getInstance().getUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() == 0) {
					DTLog.logError();
					return;
				}
				
				if (userMsg.getToUID() != IMRemoteMyself.getInstance().getUID()) {
					DTLog.sign("userMsg.getToUID():" + userMsg.getToUID());
					DTLog.sign("IMRemoteMyself.getInstance().getUID():"
							+ IMRemoteMyself.getInstance().getUID());
					DTLog.logError();
					return;
				}
				
				showNotification(userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mContent, userMsg.mServerSendTime * 1000);
			}
		});
		
		DTNotificationCenter.getInstance().addObserver("IMReceiveOrder", new Observer() {
			@Override
			public void update(Observable observable, Object data) {
				if (!(data instanceof IMUserMsg)) {
					DTLog.logError();
					return;
				}

				IMUserMsg userMsg = (IMUserMsg) data;

				if (!userMsg.mIsRecv) {
					DTLog.logError();
					return;
				}

				if (userMsg.mUserMsgType != UserMsgType.Order) {
					DTLog.logError();
					return;
				}

				if (getInstance().getRemoteLoginStatus() != LoginStatus.Logined) {
					return;
				}

				if (IMRemoteMyself.getInstance().getUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() != IMRemoteMyself.getInstance().getUID()) {
					DTLog.sign("userMsg.getToUID():" + userMsg.getToUID());
					DTLog.sign("IMRemoteMyself.getInstance().getUID():"
							+ IMRemoteMyself.getInstance().getUID());
					DTLog.logError();
					return;
				}

				showNotification(userMsg.mFromCustomUserID, userMsg.getNickName(), "[订单消息]", userMsg.mServerSendTime * 1000);

			}
		});
		
		DTNotificationCenter.getInstance().addObserver("IMReceiveAudio", new Observer() {
			@Override
			public void update(Observable observable, Object data) {
				if (!(data instanceof IMUserMsg)) {
					DTLog.logError();
					return;
				}

				IMUserMsg userMsg = (IMUserMsg) data;

				if (!userMsg.mIsRecv) {
					DTLog.logError();
					return;
				}

				if (userMsg.mUserMsgType != UserMsgType.Audio) {
					DTLog.logError();
					return;
				}

				if (getInstance().getRemoteLoginStatus() != LoginStatus.Logined) {
					return;
				}

				if (IMRemoteMyself.getInstance().getUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() != IMRemoteMyself.getInstance().getUID()) {
					DTLog.sign("userMsg.getToUID():" + userMsg.getToUID());
					DTLog.sign("IMRemoteMyself.getInstance().getUID():"
							+ IMRemoteMyself.getInstance().getUID());
					DTLog.logError();
					return;
				}

				showNotification(userMsg.mFromCustomUserID, userMsg.getNickName(), "[语音]", userMsg.mServerSendTime * 1000);
			}
		});

		DTNotificationCenter.getInstance().addObserver("IMReceiveSystemText",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMSystemMessage)) {
							DTLog.logError();
							return;
						}

						IMSystemMessage systemMessage = (IMSystemMessage) data;

						if (getInstance().getRemoteLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMRemoteMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}
						
						showNotification(systemMessage.mCustonUserId, "[系统消息]", systemMessage.mContent, systemMessage.mServerSendTime);
					}
				});

		DTNotificationCenter.getInstance().addObserver("LoginConflict", new Observer() {
			@Override
			public void update(Observable observable, Object data) {
				if (IMRemoteMyself.getInstance().getRemoteLoginStatus() == LoginStatus.Logined) {
					return;
				}

				IMAppSettings.getInstance().mLastLoginCustomUserID = "";
				IMAppSettings.getInstance().saveFile();
				
				IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
				DTSocket.getInstance().checkCloseSocket();
			}
		});

		DTNotificationCenter.getInstance().addObserver("IMReceiveBitmapMessage",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (!userMsg.mIsRecv) {
							DTLog.logError();
							return;
						}

						if (userMsg.mUserMsgType != UserMsgType.Photo) {
							DTLog.logError();
							return;
						}

						if (getInstance().getRemoteLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMRemoteMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() != IMRemoteMyself.getInstance()
								.getUID()) {
							DTLog.logError();
							return;
						}

						showNotification(userMsg.mFromCustomUserID, userMsg.getNickName(), "[图片]", userMsg.mServerSendTime * 1000);
					}
				});

	}
	
	private IMRemoteMyself() {}

	private volatile static IMRemoteMyself instance;

	public static synchronized IMRemoteMyself getInstance() {
		if (instance == null) {
			instance = new IMRemoteMyself();
		}

		return instance;
	}

}
