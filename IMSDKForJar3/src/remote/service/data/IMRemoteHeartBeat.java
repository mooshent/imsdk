package remote.service.data;

import imsdk.data.IMMyself.LoginStatus;

import java.util.Observable;
import java.util.Observer;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.aacmd.user.IMCmdUserHeartbeat;
import android.util.Log;

public class IMRemoteHeartBeat {
	public void heartbeat() {
		DTLog.sign("heartbeat");

		if (IMRemoteMyself.getInstance().getRemoteLoginStatus() == LoginStatus.Logined) {
			IMCmdUserHeartbeat cmd = new IMCmdUserHeartbeat();

			cmd.send();
		}

		DTAppEnv.cancelPreviousPerformRequest(mHeartbeatRunnable);
		DTAppEnv.performAfterDelayOnUIThread(80, mHeartbeatRunnable);
	}

	private Runnable mHeartbeatRunnable = new Runnable() {
		@Override
		public void run() {
			Log.e("Debug", "IMPrivateMyself Runnable:" + IMRemoteHeartBeat.this);
			heartbeat();
		}
	};

	private IMRemoteHeartBeat() {
		DTNotificationCenter.getInstance().addObserver("heartbeat", new Observer() {
			@Override
			public void update(Observable observable, Object data) {
				Log.e("Debug", "IMPrivateMyself Observer");
				heartbeat();
			}
		});
	}

	private volatile static IMRemoteHeartBeat instance;

	public static synchronized IMRemoteHeartBeat getInstance() {
		if (instance == null) {
			instance = new IMRemoteHeartBeat();
		}

		return instance;
	}
}
