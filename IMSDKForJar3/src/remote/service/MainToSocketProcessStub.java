package remote.service;

import imsdk.data.IMMyself.LoginStatus;

import org.json.JSONException;
import org.json.JSONObject;

import remote.service.aidl.IMainToSocketService;
import remote.service.aidl.OnLoginRecvListener;
import remote.service.aidl.OnRemoteCmdRecvListener;
import remote.service.aidl.OnRemoteLoginStatusChangedListener;
import remote.service.aidl.OnRemoteSocketStatusChangedListener;
import remote.service.data.IMRemoteMyself;
import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.socket.DTSocket;
import am.dtlib.model.c.socket.DTSocket.DTSocketStatus;
import am.imsdk.action.IMActionRemoteLogin;
import am.imsdk.model.IMAppSettings;
import am.imsdk.model.IMChatSetting;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import android.os.RemoteException;

public class MainToSocketProcessStub extends IMainToSocketService.Stub {
	OnRemoteCmdRecvListener mOnCmdRecvListener;
	OnRemoteSocketStatusChangedListener mOnSocketStatusChangedListener;
	OnRemoteLoginStatusChangedListener mOnLoginStatusChangedListener;
	OnLoginRecvListener mOnLoginRecvListener;

	/**
	 * 
	 * @Description: 清除监听
	 * @throws 
	 * @author 方子君
	 */
	public void onDestory() {
		mOnCmdRecvListener = null;
		mOnSocketStatusChangedListener = null;
		mOnLoginStatusChangedListener = null;
		mOnLoginRecvListener = null;
		
		//client下线，service重新获取缓存数据
		IMChatSetting.newInstance();
		IMUserMsgHistoriesMgr.newInstance();
		IMPrivateRecentContacts.newInstance();
	}
	
	public OnRemoteLoginStatusChangedListener getLoginStatusChangedListener() {
		return mOnLoginStatusChangedListener;
	}

	public OnRemoteCmdRecvListener getCmdRecvListener() {
		return mOnCmdRecvListener;
	}

	public OnRemoteSocketStatusChangedListener getSocketStatusChangedListener() {
		return mOnSocketStatusChangedListener;
	}
	
	public OnLoginRecvListener getLoginRecvListener() {
		return mOnLoginRecvListener;
	}
	
	public void setLoginRecvListener() {
		mOnLoginRecvListener = null;
	}
	
	@Override
	public void setOnRemoteSocketStatusChangedListener(
			OnRemoteSocketStatusChangedListener listener) throws RemoteException {
		mOnSocketStatusChangedListener = listener;
		
		if (mOnSocketStatusChangedListener != null) {
			new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						mOnSocketStatusChangedListener.onSocketStatusChanged(DTSocket.getInstance().getStatus()
								.getValue());
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			}).start();
		}
	}

	@Override
	public void setOnRemoteCmdRecvListener(OnRemoteCmdRecvListener listener)
			throws RemoteException {
		mOnCmdRecvListener = listener;
	}

	@Override
	public void setOnRemoteLoginStatusChangedListener(
			OnRemoteLoginStatusChangedListener listener) throws RemoteException {
		mOnLoginStatusChangedListener = listener;
		
		if (mOnLoginStatusChangedListener != null) {
			new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						mOnLoginStatusChangedListener.onLoginStatusChanged(IMRemoteMyself.getInstance()
								.getRemoteLoginStatus().getValue());
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			}).start();
		}
	}

//	@Override
//	public int getLoginState() throws RemoteException {
//		return 0;
//	}
	
	@Override
	public void login(final int loginType, final String customUserID, final String pwd, OnLoginRecvListener l)
			throws RemoteException {
		mOnLoginRecvListener = l;
		
		DTAppEnv.getMainHandler().post(new Runnable() {
			
			@Override
			public void run() {
				IMActionRemoteLogin.newInstance();
				IMActionRemoteLogin.getInstance().setType(loginType);
				IMActionRemoteLogin.getInstance().mTimeoutInterval = 5;
				IMActionRemoteLogin.getInstance().mUID = 0;
				IMActionRemoteLogin.getInstance().mCustomUserID = customUserID;
				IMActionRemoteLogin.getInstance().mAppKey = "";
				IMActionRemoteLogin.getInstance().setPassword(pwd);
				IMActionRemoteLogin.getInstance().begin();
			}
		});
	}
	
	
	
	@Override
	public void reconnect() throws RemoteException {
		DTAppEnv.getMainHandler().post(new Runnable() {
			@Override
			public void run() {
				DTSocket.getInstance().reconnect();
			}
		});
	}
	
//	@Override
//	public int getSocketState() throws RemoteException {
//		return DTSocket.getInstance().getStatus().getValue();
//	}

	@Override
	public boolean sendCmd(final DTCmd cmd) throws RemoteException {
		if (DTSocket.getInstance().getStatus() == DTSocketStatus.Connected) {
			DTAppEnv.getMainHandler().post(new Runnable() {
				@Override
				public void run() {
					DTSocket.getInstance().sendCmd(cmd);
				}
			});
			
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void setOffline() throws RemoteException {
		onDestory();
	}
	
	@Override
	public String getLoginInfo() throws RemoteException {
		if(IMRemoteMyself.getInstance().getRemoteLoginStatus() == LoginStatus.Logined) {
			if(IMRemoteMyself.getInstance().getUID() <= 0) {
				return "";
			}
			
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("uid", IMRemoteMyself.getInstance().getUID());
				
				return jsonObject.toString();
			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		return "";
	}
	
	@Override
	public void logout() throws RemoteException {
		DTAppEnv.getMainHandler().post(new Runnable() {
			
			@Override
			public void run() {
				IMAppSettings.getInstance().mLastLoginCustomUserID = "";
				IMAppSettings.getInstance().mLastLoginPassword = "";
				IMAppSettings.getInstance().saveFile();
				
				IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
				DTSocket.getInstance().checkCloseSocket();
			}
		});
	}
	
	private MainToSocketProcessStub() {}

	public static MainToSocketProcessStub instance;

	public static synchronized MainToSocketProcessStub getInstance() {
		if (instance == null) {
			instance = new MainToSocketProcessStub();
		}

		return instance;
	}

}
