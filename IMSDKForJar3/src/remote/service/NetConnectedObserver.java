package remote.service;

import java.util.Timer;
import java.util.TimerTask;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.ProcessTool;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public final class NetConnectedObserver {
	public boolean mWikiConnected = false;
	public boolean mMobileConnected = false;

	private Timer mTimer = new Timer();
	private TimerTask mTask = new TimerTask() {
		@Override
		public void run() {
			detect();
		}
	};

	public void run() {
		if (!ProcessTool.isSocketProcess()) {
			DTLog.logError();
			return;
		}

		if (DTAppEnv.getContext() == null) {
			DTLog.logError();
			return;
		}

		ConnectivityManager manager = (ConnectivityManager) DTAppEnv.getContext().getSystemService(
				Context.CONNECTIVITY_SERVICE);
		NetworkInfo mobileInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		NetworkInfo wifiInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo activeInfo = manager.getActiveNetworkInfo();

		mWikiConnected = wifiInfo.isConnected();
		mMobileConnected = mobileInfo.isConnected();

		if (activeInfo == null || !activeInfo.getTypeName().equals("WIFI")) {
			mWikiConnected = false;
		}
		
		// 5秒检测一次
		mTimer.schedule(mTask, 1000, 5000);
	}

	public void detect() {
		if (!ProcessTool.isSocketProcess()) {
			DTLog.logError();
			return;
		}

		if (DTAppEnv.getContext() == null) {
			DTLog.logError();
			return;
		}

		ConnectivityManager manager = (ConnectivityManager) DTAppEnv.getContext().getSystemService(
				Context.CONNECTIVITY_SERVICE);
		NetworkInfo mobileInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		NetworkInfo wifiInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo activeInfo = manager.getActiveNetworkInfo();

		boolean oldWikiConnected = mWikiConnected;
		boolean oldMobileConnected = mMobileConnected;

		mWikiConnected = wifiInfo.isConnected();
		mMobileConnected = mobileInfo.isConnected();

		if (activeInfo == null || !activeInfo.getTypeName().equals("WIFI")) {
			mWikiConnected = false;
		}

		if (oldWikiConnected || oldMobileConnected) {
			return;
		}

		if (mWikiConnected || mMobileConnected) {
			DTNotificationCenter.getInstance().postNotification("DTNetConnected");
		}
	}

	protected volatile static NetConnectedObserver sSingleton;

	public static NetConnectedObserver getInstance() {
		if (sSingleton == null) {
			sSingleton = new NetConnectedObserver();
		}

		return sSingleton;
	}
}
