package remote.service.aidl;

import remote.service.aidl.OnRemoteSocketStatusChangedListener;
import remote.service.aidl.OnRemoteLoginStatusChangedListener;
import remote.service.aidl.OnRemoteCmdRecvListener;
import remote.service.aidl.OnLoginRecvListener;
import am.dtlib.model.c.socket.DTCmd;

interface IMainToSocketService {

	//Main Process -> Socket Process
	
    /** 获取登录状态 */
	//int getLoginState();
	
	/** 重连Socket */
	void reconnect();
	
	/** 获取当前socket状态 */
	//int getSocketState();
	
	/** 设置为离线状态 */
	void setOffline();
	
	/** 登录 */
	void login(int loginType, String customUserID, String pwd, OnLoginRecvListener listener);
	
	/** 发送数据 */
	boolean sendCmd(in DTCmd cmd);
	
	/** 获取service端保存的uid和sid */
	String getLoginInfo();
	
	void logout();
	
	//Socket Process -> Main Process
	
	/** 
	 * 1. 应答包
	 * 2. DTPushCmd
	 */
	void setOnRemoteCmdRecvListener(OnRemoteCmdRecvListener listener);
	
	/** socket状态改变监听 */
	void setOnRemoteSocketStatusChangedListener(OnRemoteSocketStatusChangedListener listener);
	
	/** login状态改变监听 */
	void setOnRemoteLoginStatusChangedListener(OnRemoteLoginStatusChangedListener listener);
}