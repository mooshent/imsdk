package remote.service.aidl;

interface OnRemoteLoginStatusChangedListener {
	/** socket状态改变监听 */
	void onLoginStatusChanged(int status);
}
