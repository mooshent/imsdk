package remote.service.aidl;

interface OnRemoteSocketStatusChangedListener {
	/** socket状态改变监听 */
	void onSocketStatusChanged(int status);
}
