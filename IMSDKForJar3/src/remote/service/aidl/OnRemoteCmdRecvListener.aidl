package remote.service.aidl;

interface OnRemoteCmdRecvListener {
	// 服务器返回包
	void onRecv(int cmdType, int sequenceID, long errorCode, String json);
}