package remote.service.aidl;

interface OnLoginRecvListener {
	void onRecvSuccess(String jsonObj);
	
	void onRecvFailure(String err);
}