package am.imsdk.ui.views;

import java.lang.ref.WeakReference;
import java.util.Observable;
import java.util.Observer;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.demo.util.CPResourceUtil;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class AudioRecorderDialog extends Dialog {
	private static final int MSG_RELOAD_DB = 0x01;
	private static final int MSG_START = 0x02;
	private static final int MSG_STOP = 0x03;
	private static final int MSG_DISMISS = 0x04;

	private Context mContext;
	private OnErrorListener mErrorListener;

	private ImageView mVoiceLevelImageView;
	private View mRecordingAnimRootView;
	private View mCancelRecordRootView;

	private View mRecordingRootView;
	private View mLoadingRootView;
	private View mTooShortRootView;

	private long mStartTime;
	private long mEndTime;
	private int[] mIDs;

	private static class AudioRecorderDialogHandler extends Handler {
		private final WeakReference<AudioRecorderDialog> mDialogReference;

		public AudioRecorderDialogHandler(AudioRecorderDialog dialog) {
			mDialogReference = new WeakReference<AudioRecorderDialog>(dialog);
		}

		@Override
		public void handleMessage(Message msg) {
			if (mDialogReference.get() == null) {
				return;
			}

			switch (msg.what) {
			case MSG_RELOAD_DB:
				int level = mDialogReference.get().getLevel(
						mDialogReference.get().volume);
				int resourceID = mDialogReference.get().mIDs[level - 1];

				Log.i("test", "level : " + level + " id: " + resourceID);

				if (resourceID > 0) {
					mDialogReference.get().mVoiceLevelImageView
							.setBackground(mDialogReference.get().mContext
									.getResources().getDrawable(resourceID));
				}

				mDialogReference.get().mHandler.sendEmptyMessageDelayed(
						MSG_RELOAD_DB, 150L);
				break;
			case MSG_START:
				try {
					mDialogReference.get().showRecordingView();
					mDialogReference.get().mHandler.sendEmptyMessage(MSG_RELOAD_DB);
				} catch (Exception e) {
					if (mDialogReference.get().mErrorListener != null) {
						mDialogReference.get().mErrorListener.onError(e.getMessage());
					}
				}
				break;
			case MSG_STOP:
				mDialogReference.get().mHandler.removeMessages(MSG_RELOAD_DB);
				break;
			case MSG_DISMISS:
				mDialogReference.get().dismiss();
				break;
			default:
				break;
			}
		}
	}

	private Handler mHandler = new AudioRecorderDialogHandler(this);

	public AudioRecorderDialog(Context context, int theme) {
		super(context, theme);

		mContext = context;
		mIDs = new int[7];

		for (int i = 0; i < mIDs.length; i++) {
			mIDs[i] = CPResourceUtil.getDrawableId(mContext, "im_amp" + (i + 1));
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(CPResourceUtil.getLayoutId(mContext, "im_voice_rcd_dialog"));

		mVoiceLevelImageView = (ImageView) findViewById(CPResourceUtil.getId(mContext,
				"voice_rcd_hint_anim"));
		mRecordingAnimRootView = findViewById(CPResourceUtil.getId(mContext,
				"voice_rcd_hint_anim_area"));
		mCancelRecordRootView = findViewById(CPResourceUtil.getId(mContext,
				"voice_rcd_hint_cancel_area"));

		mRecordingRootView = findViewById(CPResourceUtil.getId(mContext,
				"voice_rcd_hint_rcding"));
		mLoadingRootView = findViewById(CPResourceUtil.getId(mContext,
				"voice_rcd_hint_loading"));
		mTooShortRootView = findViewById(CPResourceUtil.getId(mContext,
				"voice_rcd_hint_tooshort"));
	}

	private int volume = 10;

	private Observer observer = new Observer() {

		@Override
		public void update(Observable observable, Object data) {
			// TODO Auto-generated method stub
			if (!(data instanceof Integer)) {
				DTLog.logError();
				return;
			}
			volume = (Integer) data;
		}
	};

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		mCancelRecordRootView.setVisibility(View.GONE);
		mRecordingAnimRootView.setVisibility(View.VISIBLE);
		mRecordingRootView.setVisibility(View.GONE);
		mTooShortRootView.setVisibility(View.GONE);
		mLoadingRootView.setVisibility(View.VISIBLE);

		mStartTime = System.currentTimeMillis();
		mHandler.sendEmptyMessage(MSG_START);
		mHandler.removeMessages(MSG_RELOAD_DB);

		DTNotificationCenter.getInstance().addObserver("volumeUpdated", observer);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		mHandler.sendEmptyMessage(MSG_STOP);
		mHandler.removeMessages(MSG_RELOAD_DB);
		DTNotificationCenter.getInstance().removeObserver("volumeUpdated", observer);
	}

	public void setOnErrorListener(OnErrorListener listener) {
		mErrorListener = listener;
	}

	public void requestDismiss() {
		mEndTime = System.currentTimeMillis();
		if (mEndTime - mStartTime < 1000) {
			mRecordingRootView.setVisibility(View.GONE);
			mLoadingRootView.setVisibility(View.GONE);
			mTooShortRootView.setVisibility(View.VISIBLE);
			mHandler.sendEmptyMessageDelayed(MSG_DISMISS, 200);
		} else {
			dismiss();
		}
	}

	public boolean isTooShort() {
		return mEndTime - mStartTime < 1000;
	}

	public void showRecordingView() {
		mTooShortRootView.setVisibility(View.GONE);
		mLoadingRootView.setVisibility(View.GONE);
		mRecordingRootView.setVisibility(View.VISIBLE);

		mCancelRecordRootView.setVisibility(View.GONE);
		mRecordingAnimRootView.setVisibility(View.VISIBLE);
	}

	public void showCancelRecordView() {
		mTooShortRootView.setVisibility(View.GONE);
		mLoadingRootView.setVisibility(View.GONE);
		mRecordingRootView.setVisibility(View.VISIBLE);

		mRecordingAnimRootView.setVisibility(View.GONE);
		mCancelRecordRootView.setVisibility(View.VISIBLE);
	}

	public int getLevel(int splValue) {
		if (splValue >= 50.0 && splValue < 60.0) {
			return 2;
		} else if (splValue >= 60.0 && splValue < 70) {
			return 3;
		} else if (splValue >= 70.0 && splValue < 80) {
			return 4;
		} else if (splValue >= 80.0 && splValue < 85) {
			return 5;
		} else if (splValue >= 85.0 && splValue < 95) {
			return 6;
		} else if (splValue >= 95.0) {
			return 7;
		} else {
			return 1;
		}
	}

	public interface OnErrorListener {
		void onError(String msg);
	}
}
