package am.imsdk.ui.views.dialog;

import am.imsdk.demo.util.CPResourceUtil;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class OperationChatDialog extends Dialog {
	private android.view.View.OnClickListener mOnClickListener;
	private TextView tv_nickname;
	private String mNickname;
	private TextView tv_delete;
	private TextView tv_playing_mode;
	
	public OperationChatDialog(Context context) {
		super(context, CPResourceUtil.getStyleId(context, "myDialogTheme"));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(CPResourceUtil.getLayoutId(getContext(), "im_chat_operation_dialog"));
		
		tv_nickname = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_nickname"));
		tv_playing_mode = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_playing_mode"));
		tv_nickname.setText(mNickname);
		tv_delete = (TextView) findViewById(CPResourceUtil.getId(getContext(), "tv_delete"));
		
//		if (EMChatManager.getInstance().getChatOptions().getUseSpeaker()) {
//			tv_playing_mode.setText(R.string.use_receiver_mode);//使用听筒模式
//		}else{
//			tv_playing_mode.setText(R.string.use_speaker_mode);//扬声器模式
//		}
		
	}
	
	public void setDeleteClickListener(View.OnClickListener l) {
		if(tv_delete != null) {
			tv_delete.setOnClickListener(mOnClickListener);
		}
	}
	
	public void setPlayModeClickListener(View.OnClickListener l) {
		if(tv_playing_mode != null) {
			tv_playing_mode.setOnClickListener(mOnClickListener);
		}
	}
}
