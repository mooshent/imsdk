package am.imsdk.ui.adapter;

import imsdk.data.IMMessage;
import imsdk.data.IMMyself;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.mainphoto.IMChatImageView;
import imsdk.views.IMChatView;
import imsdk.views.RoundedImageView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedEndListener;
import am.imsdk.action.IMAction.OnActionPartiallyDoneListener;
import am.imsdk.action.ammsgs.IMActionSendUserMsg;
import am.imsdk.demo.gif.GifEmotionUtils;
import am.imsdk.demo.util.CPResourceUtil;
import am.imsdk.demo.util.DateUtils;
import am.imsdk.model.IMAudioPlayer;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import am.imsdk.model.im.IMUserMsgHistory;
import am.imsdk.model.userinfo.IMPrivateUserInfo;
import am.imsdk.model.userinfo.IMUsersMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImageThumbnail;
import am.imsdk.serverfile.image.IMImagesMgr;
import am.imsdk.ui.views.IMBaseChatView;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class IMChatViewAdapter extends IMBaseChatViewAdapter {
	// init
	private String mCustomUserID;
	private IMUserMsgHistory mUserChatMsgHistory;
	
	private LayoutInflater inflater;
	protected WeakReference<Activity> mActivityReference;
	
	private static final int MESSAGE_TYPE_RECV_TXT = 0;
	private static final int MESSAGE_TYPE_SENT_TXT = 1;
	private static final int MESSAGE_TYPE_SENT_IMAGE = 2;
	private static final int MESSAGE_TYPE_SENT_LOCATION = 3;
	private static final int MESSAGE_TYPE_RECV_LOCATION = 4;
	private static final int MESSAGE_TYPE_RECV_IMAGE = 5;
	private static final int MESSAGE_TYPE_SENT_VOICE = 6;
	private static final int MESSAGE_TYPE_RECV_VOICE = 7;
	private static final int MESSAGE_TYPE_SENT_VIDEO = 8;
	private static final int MESSAGE_TYPE_RECV_VIDEO = 9;
	private static final int MESSAGE_TYPE_SENT_FILE = 10;
	private static final int MESSAGE_TYPE_RECV_FILE = 11;
	private static final int MESSAGE_TYPE_SENT_VOICE_CALL = 12;
	private static final int MESSAGE_TYPE_RECV_VOICE_CALL = 13;
	private static final int MESSAGE_TYPE_RECV_ORDER = 14;
	private static final int MESSAGE_TYPE_RECV_Notice = 15;

	public IMChatViewAdapter(IMChatView imchatView, Context context,
			GifEmotionUtils gifEmotionUtils, ArrayList<Integer> ids,
			boolean userNameVisible, boolean userMainPhotoVisible,
			int userMainPhotoCornerRadius,
			IMBaseChatView.OnChatViewTouchListener onChatViewTouchListener,
			String customUserID, HashMap<String, Object> tag) {
		super(context, gifEmotionUtils, ids, userNameVisible, userMainPhotoVisible,
				userMainPhotoCornerRadius, onChatViewTouchListener);

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTLog.logError();
			return;
		}
		
		inflater = LayoutInflater.from(context);
		mActivityReference = new WeakReference<Activity>((Activity) context);
		
		mImchatView = imchatView;
		mCustomUserID = customUserID;
		
		if(tag != null && tag.size() > 0 && tag.containsKey("orderId")) {
			mUserChatMsgHistory = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(mCustomUserID, tag.get("orderId").toString());
		} else {
			mUserChatMsgHistory = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(mCustomUserID);
		}

		if (mUserChatMsgHistory == null) {
			DTLog.logError();
			return;
		}
	}

	public void setCustomUserID(String customUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTLog.logError();
			return;
		}

		mCustomUserID = customUserID;
		mUserChatMsgHistory = IMUserMsgHistoriesMgr.getInstance()
				.getUserChatMsgHistory(mCustomUserID);

		if (mUserChatMsgHistory == null) {
			DTLog.logError();
			return;
		}
	}

	@Override
	public IMUserMsg getItem(int position) {
		return mUserChatMsgHistory.getUserMsg(mUserChatMsgHistory.getCount() - 1
				- position);
	}

	@Override
	public int getCount() {
		return mUserChatMsgHistory.getCount();
	}
	
	/**
	 * 获取item的View类型
	 */
	public int getItemViewType(int position) {
		IMUserMsg userMsg = getItem(position);
		
		switch (userMsg.mUserMsgType) {
		case Normal:
		case Custom: //文字
			return userMsg.mIsRecv ? MESSAGE_TYPE_RECV_TXT : MESSAGE_TYPE_SENT_TXT; 
		case Audio:  //语音
			return userMsg.mIsRecv ? MESSAGE_TYPE_RECV_VOICE : MESSAGE_TYPE_SENT_VOICE;
		case Photo:  //图片
			return userMsg.mIsRecv ? MESSAGE_TYPE_RECV_IMAGE : MESSAGE_TYPE_SENT_IMAGE;
		case Video:  //视频
			return userMsg.mIsRecv ? MESSAGE_TYPE_RECV_VIDEO : MESSAGE_TYPE_SENT_VIDEO;
		case File:   //文件
			return userMsg.mIsRecv ? MESSAGE_TYPE_RECV_FILE : MESSAGE_TYPE_SENT_FILE;
		case Order:
			return MESSAGE_TYPE_RECV_ORDER;
		case IMSDKNotice:
			return MESSAGE_TYPE_RECV_Notice;
		default:
			return -1;
		}
	}
	
	/**
	 * 得到总的视图的个数
	 */
	public int getViewTypeCount() {
		return 16;
	}
	
	/**
	 * 通过messageType获取View
	 * @param message
	 * @param position
	 * @return
	 */
	private View createViewByMessage(IMUserMsg message) {
		switch (message.mUserMsgType) {
		case Audio:  //语音
			return message.mIsRecv ? 
					inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(), 
							"im_chatview_item_received_voice"), null) : 
					inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(), 
							"im_chatview_item_sent_voice"), null);
		case Photo:  //图片
			return message.mIsRecv ? 
					inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(), 
							"im_chatview_item_received_picture"), null) : 
					inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(), 
							"im_chatview_item_sent_picture"), null);
		case Video:  //视频
			return message.mIsRecv ? 
					inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(), 
							"im_chatview_item_received_video"), null) : 
					inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(), 
							"im_chatview_item_sent_video"), null);
		case File:   //文件
			return message.mIsRecv ? 
					inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(), 
							"im_chatview_item_received_file"), null) : 
					inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(), 
							"im_chatview_item_sent_file"), null);
		case Order:
			return inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(), 
					"im_chatview_item_received_order"), null);
		case IMSDKNotice:
			return inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(), 
					"im_chatview_item_received_notice"), null);
		case Normal:
		case Custom: //文字
		default:
			return message.mIsRecv ? 
					inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(), 
							"im_chatview_item_received_message"), null) : 
					inflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(), 
							"im_chatview_item_sent_message"), null);
		}
	}
	
	private void createItemByMessage(IMUserMsg message, View convertView, ViewHolder holder) {
		switch (message.mUserMsgType) {
		case Audio:  //语音
			try {
				holder.iv = ((ImageView) convertView.findViewById(CPResourceUtil.getId(
						mActivityReference.get(), "iv_voice")));
				holder.head_iv = (ImageView) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "iv_userhead"));
				holder.tv = (TextView) convertView.findViewById(CPResourceUtil.getId(
						mActivityReference.get(), "tv_length"));
				holder.pb = (ProgressBar) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "pb_sending"));
				holder.staus_iv = (ImageView) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "msg_status"));
				holder.tv_userId = (TextView) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "tv_userid"));
				holder.iv_read_status = (ImageView) convertView
						.findViewById(CPResourceUtil.getId(mActivityReference.get(),
								"iv_unread_voice"));
			} catch (Exception e) {
			}
			break;
		case Photo:  //图片
			try {
				holder.iv = ((ImageView) convertView.findViewById(CPResourceUtil.getId(
						mActivityReference.get(), "iv_sendPicture")));
				holder.iv_sendPicture_2 = ((ImageView) convertView
						.findViewById(CPResourceUtil.getId(mActivityReference.get(),
								"iv_sendPicture_2")));
				holder.head_iv = (ImageView) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "iv_userhead"));
				holder.tv = (TextView) convertView.findViewById(CPResourceUtil.getId(
						mActivityReference.get(), "percentage"));
				holder.pb = (ProgressBar) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "progressBar"));
				holder.staus_iv = (ImageView) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "msg_status"));
				holder.tv_userId = (TextView) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "tv_userid"));
			} catch (Exception e) {
			}
			break;
		case Video:  //视频
			try {
				holder.iv = ((ImageView) convertView.findViewById(CPResourceUtil.getId(
						mActivityReference.get(), "chatting_content_iv")));
				holder.head_iv = (ImageView) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "iv_userhead"));
				holder.tv = (TextView) convertView.findViewById(CPResourceUtil.getId(
						mActivityReference.get(), "percentage"));
				holder.pb = (ProgressBar) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "progressBar"));
				holder.staus_iv = (ImageView) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "msg_status"));
				holder.size = (TextView) convertView.findViewById(CPResourceUtil.getId(
						mActivityReference.get(), "chatting_size_iv"));
				holder.timeLength = (TextView) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "chatting_length_iv"));
				holder.playBtn = (ImageView) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "chatting_status_btn"));
				holder.container_status_btn = (LinearLayout) convertView
						.findViewById(CPResourceUtil.getId(mActivityReference.get(),
								"container_status_btn"));
				holder.tv_userId = (TextView) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "tv_userid"));

			} catch (Exception e) {
			}
			break;
		case File:   //文件
			try {
				holder.head_iv = (ImageView) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "iv_userhead"));
				holder.tv_file_name = (TextView) convertView
						.findViewById(CPResourceUtil.getId(mActivityReference.get(),
								"tv_file_name"));
				holder.tv_file_size = (TextView) convertView
						.findViewById(CPResourceUtil.getId(mActivityReference.get(),
								"tv_file_size"));
				holder.pb = (ProgressBar) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "pb_sending"));
				holder.staus_iv = (ImageView) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "msg_status"));
				holder.tv_file_download_state = (TextView) convertView
						.findViewById(CPResourceUtil.getId(mActivityReference.get(),
								"tv_file_state"));
				holder.ll_container = (LinearLayout) convertView
						.findViewById(CPResourceUtil.getId(mActivityReference.get(),
								"ll_file_container"));
				// 这里是进度值
				holder.tv = (TextView) convertView.findViewById(CPResourceUtil.getId(
						mActivityReference.get(), "percentage"));

				holder.tv_userId = (TextView) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "tv_userid"));
			} catch (Exception e) {
			}
			break;
		case Order:
			try {
				holder.head_iv = (ImageView) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "iv_userhead"));
				holder.tv_userId = (TextView) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "tv_userid"));
				
				holder.order_view_1 = (RelativeLayout) convertView.findViewById(CPResourceUtil.getId(
						mActivityReference.get(), "order_view_1"));
				holder.order_view_2 = (RelativeLayout) convertView.findViewById(CPResourceUtil.getId(
						mActivityReference.get(), "order_view_2"));
				holder.order_view_3 = (RelativeLayout) convertView.findViewById(CPResourceUtil.getId(
						mActivityReference.get(), "order_view_3"));
				holder.order_clickview_2 = convertView.findViewById(CPResourceUtil.getId(
						mActivityReference.get(), "order_clickview_2"));
				holder.order_clickview_3 = convertView.findViewById(CPResourceUtil.getId(
						mActivityReference.get(), "order_clickview_3"));
			} catch (Exception e) {
			}
			break;
		case IMSDKNotice:
			try {
				holder.tv_chatnotice = (TextView) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "tv_chatnotice"));
			} catch (Exception e) {
			}
			break;
		case Normal:
		case Custom: //文字
		default:
			try {
				holder.pb = (ProgressBar) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "pb_sending"));
				holder.staus_iv = (ImageView) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "msg_status"));
				holder.head_iv = (ImageView) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "iv_userhead"));
				// 这里是文字内容
				holder.tv = (TextView) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "tv_chatcontent"));
				holder.tv_userId = (TextView) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "tv_userid"));
				holder.rl_chat_content = (RelativeLayout) convertView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "rl_chat_content"));						
			} catch (Exception e) {
			}
			break;
		}
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		final IMUserMsg message = getItem(position);
		
		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = createViewByMessage(message);
			
			createItemByMessage(message, convertView, holder);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		if (!message.mIsRecv) {
			//注册发送状态监听事件
			DTNotificationCenter.getInstance().removeObservers(
					message.getSendStatusChangedNotificationKey());

			DTNotificationCenter.getInstance().addObserver(
					message.getSendStatusChangedNotificationKey(), new Observer() {
						@Override
						public void update(Observable observable, Object data) {
							switch (message.mUserMsgType) {
							case Normal:
							case Custom:
							case Audio:
								updateTextUI(message.mStatus, holder);
								break;
							case Photo:
								updatePhotoUI(message.mStatus, 100, holder);
								break;
							default:
								break;
							}
						}
					});
		}
		
		//单聊不需要显示用户名称
		if (message.mIsRecv) {
			if(holder.tv_userId != null) {
				if(message.getNickName() != null && message.getNickName().length() > 0) {
					holder.tv_userId.setVisibility(View.VISIBLE);
					holder.tv_userId.setText(message.getNickName());
				} else {
					holder.tv_userId.setVisibility(View.GONE);
				}
			}
		}
		
		showChatHeardIcon(message, holder.head_iv);
		
		switch (message.mUserMsgType) {
		// 根据消息type显示item
		case Photo: // 图片
			handleImageMessage(message, holder, position, convertView);
			break;
		case Custom:
		case Normal: // 文本
			handleTextMessage(message, holder, position);
			break;
		case Audio: // 语音
			handleVoiceMessage(message, holder, position, convertView);
			break;
		case Video: // 视频
//			handleVideoMessage(message, holder, position, convertView);
			break;
		case File: // 一般文件
//			handleFileMessage(message, holder, position, convertView);
			break;
		case Order:
			handleOrderMessage(message, holder, position, convertView);
			break;
		case IMSDKNotice:
			handleNoticeMessage(message, holder, position, convertView);
			break;
		default:
			// not supported
		}
		
		long currTime = 0;
		long lastTime = 0;
		
		if(message.mIsRecv) {
			currTime = message.mServerSendTime;
		} else {
			currTime = message.mClientSendTime;
		}
		
		TextView timestamp = (TextView) convertView.findViewById(CPResourceUtil
				.getId(mActivityReference.get(), "timestamp"));

		if(timestamp != null) {
			if (position == 0) {
				timestamp.setText(DateUtils.getTimestampString(new Date(currTime * 1000)));
				timestamp.setVisibility(View.VISIBLE);
			} else {
				if(getItem(position - 1).mIsRecv) {
					lastTime = getItem(position - 1).mServerSendTime;
				} else {
					lastTime = getItem(position - 1).mClientSendTime;
				}
				
				// 两条消息时间离得如果稍长，显示时间
				if (Math.abs(currTime - lastTime) < 2000) {
					timestamp.setVisibility(View.GONE);
				} else {
					timestamp.setText(DateUtils.getTimestampString(new Date(currTime * 1000)));
					timestamp.setVisibility(View.VISIBLE);
				}
			}
		}
		

		return convertView;
	}
	
	/**
	 * @Title: showChatHeardIcon 
	 * @Description: 显示头像
	 * @param @param from
	 * @return void    返回类型 
	 * @throws
	 */
	private void showChatHeardIcon(IMUserMsg message, ImageView iv_head) {
		if(iv_head == null) {
			return;
		}
		
		if (mUserMainPhotoVisible) {
			iv_head.setVisibility(View.VISIBLE);

			if (iv_head instanceof RoundedImageView) {
				RoundedImageView roundedImageView = (RoundedImageView) iv_head;

				roundedImageView.setCornerRadius(mUserMainPhotoCornerRadius);

				if (message.mIsRecv) {
					if(mContext.getString(CPResourceUtil.getStringId(mContext, "order_admin")).equals(message.getFromUID())){//系统消息
						iv_head.setImageResource(CPResourceUtil.getDrawableId(mContext, "im_order_admin_msg_warning_icon"));
					}else if(mContext.getString(CPResourceUtil.getStringId(mContext, "system_admin")).equals(message.getFromUID())){
						iv_head.setImageResource(CPResourceUtil.getDrawableId(mContext, "im_system_admin_msg_warning_icon"));
					}else if(mContext.getString(CPResourceUtil.getStringId(mContext, "consume_admin")).equals(message.getFromUID())){
						iv_head.setImageResource(CPResourceUtil.getDrawableId(mContext, "im_consume_admin_secretary_icon"));
					}else if(mContext.getString(CPResourceUtil.getStringId(mContext, "consume_admin")).equals(message.getFromUID())){
						iv_head.setImageResource(CPResourceUtil.getDrawableId(mContext, "im_system_admin_msg_warning_icon"));
					} else {
						IMPrivateUserInfo userInfo = IMUsersMgr.getInstance()
								.getUserInfo(message.getFromUID());

						roundedImageView.setFileID(userInfo.getMainPhotoFileID());
					}
				} else {
					roundedImageView.setFileID(IMPrivateMyself.getInstance()
							.getMainPhotoFileID());
				}
				//头像点击事件
				dealHeadPhotoClick(roundedImageView, message);
			}
		} else {
			iv_head.setVisibility(View.GONE);
		}
	}
	
	/**
	 * 图片消息
	 * 
	 * @param message
	 * @param holder
	 * @param position
	 * @param convertView
	 */
	private void handleImageMessage(final IMUserMsg message, final ViewHolder holder, final int position, View convertView) {
		int width = 0;
		int height = 0;
		
		holder.iv.setImageBitmap(null);
		
		final IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
				message.getFileID());

		if (photo.getBitmap() == null) {
			Log.e("Debug", "contain observer:" + "IMChatImageMessageID:" + message.mMsgID + ":" + DTNotificationCenter.getInstance().containObserver("IMChatImageMessageID:" + message.mMsgID));
			if(!DTNotificationCenter.getInstance().containObserver("IMChatImageMessageID:" + message.mMsgID)) {
				DTNotificationCenter.getInstance().addObserver("IMChatImageMessageID:" + message.mMsgID, new Observer() {
					
					@Override
					public void update(Observable observable, Object data) {
						Log.e("Debug", "get progress:" + data);
						try {
							double progressValue = (Double) data;
							
							updatePhotoUI(message.mStatus, (int) (progressValue * 100), holder);
							
							if(progressValue == 1) {
								DTNotificationCenter.getInstance().removeObservers("IMChatImageMessageID:" + message.mMsgID);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
			
			switch (message.mStatus) {
			case IMMessage.SUCCESS:
				holder.pb.setVisibility(View.GONE);
				holder.tv.setVisibility(View.GONE);
				holder.staus_iv.setVisibility(View.GONE);
				
				holder.iv.setImageResource(CPResourceUtil.getDrawableId(mContext,
						"im_download_image_icon"));
				holder.iv_sendPicture_2.setVisibility(View.GONE);
				break;
			case IMMessage.FAILURE:
				holder.pb.setVisibility(View.GONE);
				holder.tv.setVisibility(View.GONE);
				holder.staus_iv.setVisibility(View.VISIBLE);
				break;
			case IMMessage.SENDING_OR_RECVING:
				holder.staus_iv.setVisibility(View.GONE);
				holder.pb.setVisibility(View.VISIBLE);
				holder.tv.setVisibility(View.VISIBLE);
				
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.iv
						.getLayoutParams();
				params.width = 50;
				params.height = 50;
				holder.iv.setLayoutParams(params);
				
				holder.iv.setImageResource(CPResourceUtil.getDrawableId(mContext,
						"im_btn_gray_normal_shape"));
				break;
			}

			
		} else {
//			final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.iv
//					.getLayoutParams();

			if (width == 0 || height == 0) {
				width = photo.getWidth();
				height = photo.getHeight();

				float scale = calculateImageScaleRatio(width, height);

				width = (int) (width * scale);
				height = (int) (height * scale);
			}

//			params.width = width;
//			params.height = height;
//
//			holder.iv.setLayoutParams(params);
//			holder.iv_sendPicture_2.setLayoutParams(params);

			IMImageThumbnail thumbnail = IMImagesMgr.getInstance().getThumbnail(
					message.getFileID(), width, height);

			holder.iv.setImageBitmap(thumbnail.getBitmap());
			
			switch (message.mStatus) {
			case IMMessage.SUCCESS:
				holder.pb.setVisibility(View.GONE);
				holder.tv.setVisibility(View.GONE);
				holder.staus_iv.setVisibility(View.GONE);
				break;
			case IMMessage.FAILURE:
				holder.pb.setVisibility(View.GONE);
				holder.tv.setVisibility(View.GONE);
				holder.staus_iv.setVisibility(View.VISIBLE);
				break;
			case IMMessage.SENDING_OR_RECVING:
				holder.staus_iv.setVisibility(View.GONE);
				holder.pb.setVisibility(View.VISIBLE);
				holder.tv.setVisibility(View.VISIBLE);
				break;
			}
		}
		
		//聊天图片点击事件
		holder.iv_sendPicture_2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (photo.getBitmap() == null) {
					return;
				}

				// 大图显示初始化
				DisplayMetrics  dm = new DisplayMetrics();
				WindowManager windowManager = (WindowManager) mContext
						.getSystemService(Context.WINDOW_SERVICE);
				windowManager.getDefaultDisplay().getMetrics(dm);
				
				if (popupWindow == null || chatImageView == null) {
					chatImageView = mInflater.inflate(CPResourceUtil.getLayoutId(mActivityReference.get(),
							"im_chatview_image"), null);
					popupWindow = new PopupWindow(chatImageView, dm.widthPixels, dm.heightPixels);
				}

				popupWindow.setFocusable(true);
				popupWindow.setOutsideTouchable(true);// 设置允许在外点击消失
				popupWindow.setBackgroundDrawable(new BitmapDrawable(mContext.getResources(), 
						Bitmap.createBitmap(dm.widthPixels, dm.heightPixels, Bitmap.Config.ALPHA_8)));

				IMChatImageView image = (IMChatImageView) chatImageView.findViewById(CPResourceUtil.getId(mContext, "imimg_image"));

				image.setImageBitmap(photo.getBitmap());

				popupWindow.showAtLocation(mImchatView, Gravity.CENTER, 0, 0);
			}
		});
		
		holder.iv_sendPicture_2.setTag(position);
		holder.iv_sendPicture_2.setOnLongClickListener(mContentLongClickListener);
		holder.iv.setTag(position);
		holder.iv.setOnLongClickListener(mContentLongClickListener);
		
		if(!message.mIsRecv) {
			holder.staus_iv.setTag(message);
			holder.staus_iv.setOnClickListener(mResendClickListener);
		}
	}
	
	/**
	 * 文本消息
	 * 
	 * @param message
	 * @param holder
	 * @param position
	 */
	private void handleTextMessage( final IMUserMsg message, final ViewHolder holder, final int position) {
//		if (message.getFrom().equals(MyApplication.instance.getString(R.string.order_admin))) {//系统消息 显示出来
//			if(holder.rl_chat_content != null)
//				holder.rl_chat_content.setVisibility(View.VISIBLE);
//			if(holder.rl_system_sceretary != null)
//				holder.rl_system_sceretary.setVisibility(View.GONE);
//			Spannable span = SmileUtils.getSmiledText(mContext, txtBody.getMessage());
//			// 设置内容
//			holder.tv.setText(span, BufferType.SPANNABLE);
//			String msgContent = holder.tv.getText().toString();//消息内容
//			String phoneNumber = "";
//			String phoneNumberDot = "";
//			try {
//				phoneNumber = message.getStringAttribute("phoneNumber");
//			} catch (EaseMobException e) {
//				e.printStackTrace();
//			}
//			if (!phoneNumber.equals("0") && phoneNumber.length() == 11) {
//				if (msgContent.contains(phoneNumber)) {
//					msgContent = msgContent.replace(phoneNumber+"。", "");
//					phoneNumberDot = "。";
//				}
//			}else{
//				phoneNumber = "";
//				phoneNumberDot = "";
//			}
//			String beforeType = MyApplication.instance.getString(R.string.order_notify);
//			String htmlContent = "<span>"+  "<font color=\"#04BD0A\">"+beforeType+"</font><br/>"
//					+msgContent+ 
//					"<u><font color=\"#04BD0A\">"+phoneNumber+"</font></u>" 
//					+phoneNumberDot+
//					"</span>";
//			holder.tv.setText(Html.fromHtml(htmlContent));
//			// 设置长按事件监听
//			if (message.direct == IMUserMsg.Direct.RECEIVE) {
//				holder.tv.setOnClickListener(new OnClickListener() {
//					@Override
//					public void onClick(View v) {
//						if (message.getFrom().equals(MyApplication.instance.getString(R.string.order_admin))) {//系统消息点击
//							String phoneNumber = "0";
//							String orderId = "0";
//							String createId = "0";
//							try {
//								phoneNumber = message.getStringAttribute("phoneNumber");
//								orderId = message.getStringAttribute("orderId");
//								createId = message.getStringAttribute("id");//3102697
//							} catch (EaseMobException e) {
//								e.printStackTrace();
//							}
//							if (!phoneNumber.equals("0")) {//有电话号码
//								CallAndSelectDialog callAndSelect = new CallAndSelectDialog(mContext,phoneNumber,orderId,createId);
//								callAndSelect.show();
//							}else{//没有电话号码
//								TAFragmentActivity taa = (TAFragmentActivity) mContext;
//								Bundle bundle = new Bundle();
//								bundle.putLong("orderId", Tools.getLong(orderId));
//								bundle.putLong
//								("createId", Tools.getLong(createId));
//								taa.doActivity(R.string.YouwoOrderDetialActivity, bundle);
//							}
//						}else{
//
//						}
//					}
//				});
//			}
//		}else if(message.getFrom().equals(HxConstant.TASK_ADMIN)){//任务小秘书
//			if(holder.rl_chat_content != null)
//				holder.rl_chat_content.setVisibility(View.VISIBLE);
//			if(holder.rl_system_sceretary != null)
//				holder.rl_system_sceretary.setVisibility(View.GONE);
//			String digest = "";
//			try {
//				JSONObject jsonData = new JSONObject(txtBody.getMessage());
//				JSONArray jsonArray = jsonData.getJSONArray("TaskInfos");
//				int length = jsonData.length();
//				for (int i = 0; i < length; i++) {
//					JSONObject jsonObject = jsonArray.getJSONObject(i);
//					if (i == length - 1) {
//						digest =  "恭喜你：完成任务"+jsonObject.getString("TaskName") + "";
//					}else{
//						digest =  "恭喜你：完成任务"+jsonObject.getString("TaskName") + "/n";
//					}
//				}
//				digest = "<span>"+  "<font color=\"#04BD0A\">"+"【任务通知】"+"</font><br/>"
//						+ digest + 
//						"</span>";
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//			holder.tv.setText(Html.fromHtml(digest));
//			holder.tv.setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View v) {
//					TAFragmentActivity taa = (TAFragmentActivity) mContext;
//					taa.doActivity(R.string.DailyQuestActivity);
//				}
//			});
//		}else if(message.getFrom().equals(MyApplication.instance.getString(R.string.system_admin))){
//			if(holder.rl_chat_content != null)
//				holder.rl_chat_content.setVisibility(View.GONE);
//			if(holder.rl_system_sceretary != null)
//				holder.rl_system_sceretary.setVisibility(View.VISIBLE);
//			JSONObject jsonData;
//			try {
//				jsonData = new JSONObject(txtBody.getMessage());
//				int gratuity = jsonData.getInt("Gratuity");
//				long overTime = (long)jsonData.getInt("OverTime");
//				int tryMoney = jsonData.getInt("TryMoney");
//				int payType = jsonData.getInt("PayType");
//				String posInfo = jsonData.getString("PosInfo");
//				String topic = jsonData.getString("Topic");
//				holder.tv_system_title.setText(topic);
//				holder.tv_system_address.setText(posInfo);
//				Date date = new Date(overTime * 1000);
//				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//				holder.tv_system_time.setText(format.format(date));
//				holder.tv_try_money.setText(tryMoney + "试金石");
//				holder.tv_gratuity.setText(gratuity / 100 + "元");		// gratuity 服务器下发下来的是分需要所以100
//				if(payType == 0){
//					holder.tv_pay_type.setText("线下支付");
//				}else if(payType == 1){
//					holder.tv_pay_type.setText("线上支付");
//				}
//				//				holder.tv_try_money.setText(Html.fromHtml("悬赏：<font color=\"#ff7f05\">" + tryMoney + "试金石</font>"));
//				//				holder.tv_gratuity.setText(Html.fromHtml("服务费：<font color=\"#ff7f05\">" + gratuity + "元</font>"));
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//
//			holder.ll_system_secretary_content.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					String orderId = "";
//					String createId = "";
//					try {
//						orderId = message.getStringAttribute("orderId");
//						createId = message.getStringAttribute("id");//3102697
//					} catch (EaseMobException e) {
//						e.printStackTrace();
//					}
//					TAFragmentActivity taa = (TAFragmentActivity) mContext;
//					Bundle bundle = new Bundle();
//					bundle.putLong("orderId", Tools.getLong(orderId));
//					bundle.putLong("createId", Tools.getLong(createId));
//					taa.doActivity(R.string.YouwoOrderDetialActivity, bundle);
//
//				}
//			});
//		}else if(message.getFrom().equals(MyApplication.instance.getString(R.string.consume_admin))){
//			if(holder.rl_chat_content != null)
//				holder.rl_chat_content.setVisibility(View.VISIBLE);
//			if(holder.rl_system_sceretary != null)
//				holder.rl_system_sceretary.setVisibility(View.GONE);
//			try {
//				JSONObject jsonData = new JSONObject(txtBody.getMessage());
//				final String type = jsonData.getString("type");	// 1 跳转到钱包 2 跳转到商城
//				String title = jsonData.getString("title");
//				String msg = jsonData.getString("msg");
//				final String url = jsonData.getString("url");
//				String htmlContent = "<span>" +  "<font color=\"#04BD0A\">" + title + "</font><br/>"
//						+ msg + "</span>";
//				holder.tv.setText(Html.fromHtml(htmlContent));
//				holder.tv.setOnClickListener(new View.OnClickListener() {
//
//					@Override
//					public void onClick(View v) {
//						Bundle bundle = new Bundle();
//						bundle.putString("url", url);
//						TAFragmentActivity taa = (TAFragmentActivity) mContext;
//						if(!com.jiaoyou.youwo.view.utils.Tools.isNumeric(type))
//							return;
//						if(Integer.parseInt(type) == 1){	// 1 钱包
//							taa.doActivity(R.string.MyWalletActivity, bundle);
//						}else if(Integer.parseInt(type) == 2){ // 2 商城
//							taa.doActivity(R.string.SquareHtmlActivity, bundle);	
//						}
//
//					}
//				});
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//		}else{
			if(holder.rl_chat_content != null){
				holder.rl_chat_content.setVisibility(View.VISIBLE);
			}
			if(holder.rl_system_sceretary != null){
				holder.rl_system_sceretary.setVisibility(View.GONE);
			}
			
			if (message.mContent != null) {
				mGifEmotionUtils.setSpannableText(holder.tv, message.mContent, mHandler);
				holder.tv.setVisibility(View.VISIBLE);
				holder.tv.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
						null);
				holder.tv.setOnTouchListener(mOnTouchListener);
			} else {
				holder.tv.setText("");
				holder.tv.setVisibility(View.VISIBLE);
			}
			
			holder.tv.setTag(position);
			holder.tv.setOnLongClickListener(mContentLongClickListener);
//		}
		if (!message.mIsRecv) {
			updateTextUI(message.mStatus, holder);
			
			holder.staus_iv.setTag(message);
			holder.staus_iv.setOnClickListener(mResendClickListener);
		}
	}
	
	private void handleVoiceMessage(final IMUserMsg message, final ViewHolder holder, final int position, View convertView) {
		final AnimationDrawable animationDrawable = (AnimationDrawable) holder.iv.getDrawable();

		if (-1 != mAudioPlayingPosition && position == mAudioPlayingPosition) {
			animationDrawable.start();
		} else {
			if (animationDrawable.isRunning()) {
				animationDrawable.stop();
				animationDrawable.selectDrawable(0);
			}
		}

		OnClickListener audioClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (position == mAudioPlayingPosition) {
					IMAudioPlayer.getInstance().stop();

					if (animationDrawable != null && animationDrawable.isRunning()) {
						animationDrawable.stop();
						animationDrawable.selectDrawable(0);
						mLastAnimationDrawable = null;
					}

					mAudioPlayingPosition = -1;
				} else {
					mAudioPlayingPosition = position;
					animationDrawable.start();

					if (mLastAnimationDrawable != null
							&& mLastAnimationDrawable.isRunning()) {
						mLastAnimationDrawable.stop();
						mLastAnimationDrawable.selectDrawable(0);
					}

					mLastAnimationDrawable = animationDrawable;

					IMAudioPlayer.getInstance().play(message,
							new OnCompletionListener() {
								@Override
								public void onCompletion(MediaPlayer mp) {
									animationDrawable.stop();
									animationDrawable.selectDrawable(0);
									mLastAnimationDrawable = null;
									mAudioPlayingPosition = -1;
								}
							});
					
					if(message.mIsRecv && !message.isListened()) {
						message.setVoiceListen();
						message.saveFile();
						
						holder.iv_read_status.setVisibility(View.INVISIBLE);
					}
					
				}
			}
		};

		float  dpValue = message.getAudioDuration() * 2 + 54;
		android.widget.RelativeLayout.LayoutParams params = new android.widget.RelativeLayout.LayoutParams(dip2px(mContext, dpValue), android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT);

		if (message.mIsRecv) {
			//			holder.iv.setImageResource(R.drawable.chatfrom_voice_playing);
			params.leftMargin = dip2px(mContext, 5);
			params.rightMargin = dip2px(mContext, 5);
			
			params.addRule(RelativeLayout.RIGHT_OF, CPResourceUtil.getId(
					mActivityReference.get(), "iv_userhead"));
			params.addRule(RelativeLayout.BELOW,CPResourceUtil.getId(
					mActivityReference.get(), "tv_userid"));
			holder.iv.setPadding(dip2px(mContext, 12), dip2px(mContext, 8), dip2px(mContext, 10), dip2px(mContext, 8));
			
			if(message.isListened()) {
				holder.iv_read_status.setVisibility(View.INVISIBLE);
			} else {
				holder.iv_read_status.setVisibility(View.VISIBLE);
			}
		} else {
			//			holder.iv.setImageResource(R.drawable.chatto_voice_playing);
			params.leftMargin = dip2px(mContext, 5);
			params.rightMargin = dip2px(mContext, 5);
			params.addRule(RelativeLayout.LEFT_OF, CPResourceUtil.getId(
					mActivityReference.get(), "iv_userhead"));
			params.addRule(RelativeLayout.CENTER_VERTICAL);
			holder.iv.setPadding(dip2px(mContext, 8), dip2px(mContext, 8), dip2px(mContext, 12), dip2px(mContext, 8));
		}
		holder.iv.setLayoutParams(params);
		holder.tv.setText(message.getAudioDuration() + "\"");
		holder.iv.setOnClickListener(audioClickListener);
		holder.iv.setTag(position);
		holder.iv.setOnLongClickListener(mContentLongClickListener);

		// until here, deal with send voice msg
		if(!message.mIsRecv) {
			updateTextUI(message.mStatus, holder);
			
			holder.staus_iv.setTag(message);
			holder.staus_iv.setOnClickListener(mResendClickListener);
		}
	}

//	protected void updateView(int position, View convertView, IMUserMsg userMsg) {
//		TextView systemTip = ViewHolder.get(convertView, mIDs.get(1));
//		TextView systemContent = ViewHolder.get(convertView, mIDs.get(2));
//
//		ImageView leftHead = ViewHolder.get(convertView, mIDs.get(3));
//		ImageView leftAddon = ViewHolder.get(convertView, mIDs.get(4));
//		TextView leftUserName = ViewHolder.get(convertView, mIDs.get(5));
//		TextView leftContent = ViewHolder.get(convertView, mIDs.get(6));
//		View leftImageRoot = ViewHolder.get(convertView, mIDs.get(7));
//		TextView leftImageProgress = ViewHolder.get(convertView, mIDs.get(8));
//		View leftImageMask = ViewHolder.get(convertView, mIDs.get(9));
//		ImageView leftImage = ViewHolder.get(convertView, mIDs.get(10));
//
//		ImageView rightHead = ViewHolder.get(convertView, mIDs.get(11));
//		ImageView rightAddon = ViewHolder.get(convertView, mIDs.get(12));
//		TextView rightUserName = ViewHolder.get(convertView, mIDs.get(13));
//		TextView rightContent = ViewHolder.get(convertView, mIDs.get(14));
//		View rightImageRoot = ViewHolder.get(convertView, mIDs.get(15));
//		TextView rightImageProgress = ViewHolder.get(convertView, mIDs.get(16));
//
//		View rightImageMask = ViewHolder.get(convertView, mIDs.get(17));
//
//		ImageView rightImage = ViewHolder.get(convertView, mIDs.get(18));
//		// View rightProgress = ViewHolder.get(convertView, mIDs.get(19));
//
//		View leftView = ViewHolder.get(convertView, mIDs.get(20));
//		View rightView = ViewHolder.get(convertView, mIDs.get(21));
//
//		systemContent.setVisibility(View.GONE);
//		systemTip.setVisibility(View.GONE);
//
//		TextView userTextView = null;
//		ImageView userImageView = null;
//
//		if (userMsg.mUserMsgType == UserMsgType.System) {
//			leftView.setVisibility(View.GONE);
//			rightView.setVisibility(View.GONE);
//			systemContent.setText(userMsg.mContent);
//			systemContent.setVisibility(View.VISIBLE);
//		} else {
//			int status = userMsg.mStatus;
//
//			// 如果消息是对方发给我的
//			if (userMsg.mIsRecv) {
//				rightView.setVisibility(View.GONE);
//				leftView.setVisibility(View.VISIBLE);
//
//				userTextView = leftUserName;
//				userImageView = leftHead;
//
//				dealMsg(position, leftContent, leftImageRoot, leftImage,
//						leftImageProgress, leftImageMask, leftAddon, userMsg, status,
//						convertView);
//			} else {
//				leftView.setVisibility(View.GONE);
//				rightView.setVisibility(View.VISIBLE);
//
//				userTextView = rightUserName;
//				userImageView = rightHead;
//
//				dealMsg(position, rightContent, rightImageRoot, rightImage,
//						rightImageProgress, rightImageMask, rightAddon, userMsg,
//						status, convertView);
//			}
//
//			if (mUserNameVisible) {
//				leftUserName.setVisibility(View.VISIBLE);
//				rightUserName.setVisibility(View.VISIBLE);
//				userTextView.setText(userMsg.mFromCustomUserID);
//			} else {
//				leftUserName.setVisibility(View.GONE);
//				rightUserName.setVisibility(View.GONE);
//			}
//
//			if (mUserMainPhotoVisible) {
//				userImageView.setVisibility(View.VISIBLE);
//
//				if (userImageView instanceof RoundedImageView) {
//					RoundedImageView roundedImageView = (RoundedImageView) userImageView;
//
//					roundedImageView.setCornerRadius(mUserMainPhotoCornerRadius);
//
//					if (userMsg.mIsRecv) {
//						IMPrivateUserInfo userInfo = IMUsersMgr.getInstance()
//								.getUserInfo(userMsg.getFromUID());
//
//						roundedImageView.setFileID(userInfo.getMainPhotoFileID());
//					} else {
//						roundedImageView.setFileID(IMPrivateMyself.getInstance()
//								.getMainPhotoFileID());
//					}
//					//头像点击事件
//					dealHeadPhotoClick(roundedImageView, userMsg);
//				}
//			} else {
//				userImageView.setVisibility(View.GONE);
//			}
//		}
//	}
	
	private void handleOrderMessage(final IMUserMsg message,
			final ViewHolder holder, final int position, View convertView) {
		try {
			JSONObject content = new JSONObject(message.mContent);

			int subType = content.getInt("subType");

			if (subType == 0) {
				holder.order_view_1.setVisibility(View.VISIBLE);
				holder.order_view_2.setVisibility(View.GONE);
				holder.order_view_3.setVisibility(View.GONE);

				Button btn1 = (Button) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "order_btn_1"));
				btn1.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						JSONObject content = new JSONObject();
						try {
							content.put("subType", 1);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						message.mContent = content.toString();
						message.saveFile();
						
						holder.order_view_1.setVisibility(View.GONE);
						holder.order_view_2.setVisibility(View.VISIBLE);
						holder.order_view_3.setVisibility(View.GONE);
						
						if(mOrderBtn1ClickRunnable != null) {
							mOrderBtn1ClickRunnable.run();
						}
					}
				});

				Button btn2 = (Button) convertView.findViewById(CPResourceUtil
						.getId(mActivityReference.get(), "order_btn_2"));
				btn2.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						JSONObject content = new JSONObject();
						try {
							content.put("subType", 2);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						message.mContent = content.toString();
						message.saveFile();
						
						holder.order_view_1.setVisibility(View.GONE);
						holder.order_view_2.setVisibility(View.GONE);
						holder.order_view_3.setVisibility(View.VISIBLE);
						
						if(mOrderBtn2ClickRunnable != null) {
							mOrderBtn2ClickRunnable.run();
						}
					}
				});
			} else if (subType == 1) {
				holder.order_view_1.setVisibility(View.GONE);
				holder.order_view_2.setVisibility(View.VISIBLE);
				holder.order_view_3.setVisibility(View.GONE);
			} else if (subType == 2) {
				holder.order_view_1.setVisibility(View.GONE);
				holder.order_view_2.setVisibility(View.GONE);
				holder.order_view_3.setVisibility(View.VISIBLE);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	private void handleNoticeMessage(IMUserMsg message, ViewHolder holder,
			int position, View convertView) {
		if(holder.tv_chatnotice != null) {
			holder.tv_chatnotice.setText(message.mContent);
		}
	}
	
	/**
	 * 头像点击事件
	 * @param roundedImageView
	 * @param userMsg
	 */
	private void dealHeadPhotoClick(RoundedImageView roundedImageView, 
			final IMUserMsg userMsg) {
		
		roundedImageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mHeadPhotoClickListener != null){
					mHeadPhotoClickListener.onClick(v,userMsg.mFromCustomUserID);
				}
			}
		});
		
	}
	

	/**
	 * 
	 * @param id
	 * @param contentView
	 *            文本消息---textview
	 * @param imageRoot
	 *            图片消息--- （内含 图片 和 加载进度 以及 背景）;
	 * @param imageView
	 *            图片消息--- （ 图片 ）;
	 * @param imageProgress
	 *            图片消息--- （ 加载进度 ）;
	 * @param imageMask
	 *            图片消息--- （ 背景）;
	 * @param addon
	 *            重发 图片控件
	 * @param message
	 *            消息封装体
	 * @param isComMsg
	 *            消息的方向
	 * @param isReceipted
	 * @param holder
	 */
//	private void dealMsg(final int position, TextView contentView, View imageRoot,
//			final ImageView imageView, final TextView imageProgress,
//			final View imageMask, final ImageView addon, final IMUserMsg userMsg,
//			int status, final View holder) {
//		String content = userMsg.mContent;
//
//		contentView.setOnTouchListener(null);
//		contentView.setOnLongClickListener(null);
//		contentView.setTag(null);
//		contentView.setVisibility(View.GONE);
//		contentView.setOnClickListener(null);
//
//		imageRoot.setVisibility(View.GONE);
//
//		imageView.setTag(null);
//		imageView.setOnLongClickListener(null);
//		addon.setTag(null);
//		addon.setVisibility(View.INVISIBLE);
//		addon.setOnClickListener(null);
//
//		final View rightProgress = ViewHolder.get(holder, mIDs.get(19));
//
//		rightProgress.setVisibility(View.INVISIBLE);
//
//		if (status == IMChatMessage.FAILURE) { // 发送失败
//			addon.setVisibility(View.VISIBLE);
//			addon.setTag(position);
//			addon.setImageResource(CPResourceUtil.getDrawableId(mContext,
//					"im_btn_style_resend"));
//
//			addon.setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View v) {
//					final IMUserChatMsgHistory history = IMUserMsgHistoriesMgr
//							.getInstance().getUserChatMsgHistory(mCustomUserID);
//
//					history.removeUnsentUserMsgWithClientSendTime(userMsg.mClientSendTime);
//					history.saveFile();
//
//					userMsg.removeFile();
//
//					IMChatViewAdapter.this.notifyDataSetChanged();
//
//					if (userMsg.mUserMsgType == UserMsgType.Normal) {
//						IMMyself.sendText(userMsg.mContent, userMsg.mToCustomUserID,
//								10, new OnActionListener() {
//									@Override
//									public void onSuccess() {
//									}
//
//									@Override
//									public void onFailure(String error) {
//										if (error.equals("102")) {
//											Toast.makeText(mContext,
//													"对方还没有安装最近版本，暂不能收到消息",
//													Toast.LENGTH_LONG).show();
//										}
//									}
//								});
//					} else if (userMsg.mUserMsgType == UserMsgType.Photo) {
//						final long actionTime = System.currentTimeMillis() / 1000;
//
//						userMsg.mClientSendTime = actionTime;
//						userMsg.saveFile();
//
//						// 维护聊天记录
//						history.insertUnsentUserMsg(userMsg.mClientSendTime);
//						history.saveFile();
//						DTNotificationCenter.getInstance().postNotification(
//								history.getNewMsgNotificationKey());
//
//						// 维护最近联系人
//						IMPrivateRecentContacts.getInstance().insert(
//								userMsg.mToCustomUserID);
//						IMPrivateRecentContacts.getInstance().saveFile();
//						DTNotificationCenter.getInstance()
//								.postNotification(
//										IMPrivateRecentContacts.getInstance()
//												.notificationKey());
//
//						// 发送消息
//						IMActionSendUserMsg action = new IMActionSendUserMsg();
//
//						action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
//							@Override
//							public void onActionDoneEnd() {
//								// 维护聊天记录
//								history.replaceUnsentUserMsgToSent(userMsg);
//								history.saveFile();
//							}
//						};
//
//						action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
//							@Override
//							public void onActionPartiallyDone(double percentage) {
//							}
//						};
//
//						action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
//							@Override
//							public void onActionFailedEnd(String error) {
//							}
//						};
//
//						action.mUserMsg = userMsg;
//
//						action.mBeginTime = actionTime;
//						action.begin();
//					} else if (userMsg.mUserMsgType == UserMsgType.Audio) {
//
//					}
//				}
//			});
//
//			rightProgress.setVisibility(View.INVISIBLE);
//		} else if ((status == IMChatMessage.SENDING_OR_RECVING) && !userMsg.mIsRecv) {
//			rightProgress.setVisibility(View.VISIBLE);
//		}
//
//		/**
//		 * 实例化语音控件 隐藏 语音播放动画控件，去除语音点击响应事件 < start>
//		 */
//		View audioAnimationView;
//		View audioContentView;
//
//		if (userMsg.mIsRecv) {
//			audioContentView = ViewHolder.get(holder,
//					CPResourceUtil.getId(mContext, "left_content_p"));
//			audioAnimationView = ViewHolder.get(holder,
//					CPResourceUtil.getId(mContext, "chatting_item_left_audio"));
//		} else {
//			audioContentView = ViewHolder.get(holder,
//					CPResourceUtil.getId(mContext, "right_content_p"));
//			audioAnimationView = ViewHolder.get(holder,
//					CPResourceUtil.getId(mContext, "chatting_item_right_audio"));
//		}
//
//		audioContentView.setOnClickListener(null);
//		audioContentView.setTag(null);
//		audioContentView.setOnLongClickListener(null);
//		audioAnimationView.setVisibility(View.GONE);
//		
//		/**
//		 * 实例化语音控件 隐藏 语音播放动画控件，去除语音点击响应事件 < end>
//		 */
//		switch (userMsg.mUserMsgType) {
//		case Normal:
//			if (content != null) {
//				mGifEmotionUtils.setSpannableText(contentView, content, mHandler);
//				contentView.setVisibility(View.VISIBLE);
//				contentView.setCompoundDrawablesWithIntrinsicBounds(null, null, null,
//						null);
//				contentView.setOnTouchListener(mOnTouchListener);
//			} else {
//				contentView.setText("");
//				contentView.setVisibility(View.VISIBLE);
//			}
//			
//			contentView.setTag(position);
//			contentView.setOnLongClickListener(mContentLongClickListener);
//			break;
//		case Audio: {
//			long duration = userMsg.getAudioDuration();
//
//			contentView.setVisibility(View.VISIBLE);
//			contentView.setText("");
//			contentView.setText(duration + "\"");
//			audioAnimationView.setVisibility(View.VISIBLE);
//
//			final AnimationDrawable animationDrawable = (AnimationDrawable) audioAnimationView
//					.getBackground();
//
//			if (-1 != mAudioPlayingPosition && position == mAudioPlayingPosition) {
//				animationDrawable.start();
//			} else {
//				if (animationDrawable.isRunning()) {
//					animationDrawable.stop();
//					animationDrawable.selectDrawable(0);
//				}
//			}
//
//			OnClickListener audioClickListener = new OnClickListener() {
//				@Override
//				public void onClick(View v) {
//					if (position == mAudioPlayingPosition) {
//						IMAudioPlayer.getInstance().stop();
//
//						if (animationDrawable != null && animationDrawable.isRunning()) {
//							animationDrawable.stop();
//							animationDrawable.selectDrawable(0);
//							mLastAnimationDrawable = null;
//						}
//
//						mAudioPlayingPosition = -1;
//					} else {
//						mAudioPlayingPosition = position;
//						animationDrawable.start();
//
//						if (mLastAnimationDrawable != null
//								&& mLastAnimationDrawable.isRunning()) {
//							mLastAnimationDrawable.stop();
//							mLastAnimationDrawable.selectDrawable(0);
//						}
//
//						mLastAnimationDrawable = animationDrawable;
//
//						IMAudioPlayer.getInstance().play(userMsg,
//								new OnCompletionListener() {
//									@Override
//									public void onCompletion(MediaPlayer mp) {
//										animationDrawable.stop();
//										animationDrawable.selectDrawable(0);
//										mLastAnimationDrawable = null;
//										mAudioPlayingPosition = -1;
//									}
//								});
//					}
//				}
//			};
//
//			audioContentView.setOnClickListener(audioClickListener);
//			contentView.setOnClickListener(audioClickListener);
//			
//			audioContentView.setTag(position);
//			audioContentView.setOnLongClickListener(mContentLongClickListener);
//			contentView.setTag(position);
//			contentView.setOnLongClickListener(mContentLongClickListener);
//		}
//			break;
//		case Photo: {
//			int width = 0;
//			int height = 0;
//
//			final IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
//					userMsg.getFileID());
//
//			imageRoot.setVisibility(View.VISIBLE);
//
//			if (photo.getBitmap() == null) {
//				mPhotoMsgIDPositionMap.put(userMsg.mMsgID, position);
//
//				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imageView
//						.getLayoutParams();
//
//				params.width = width == 0 ? PHOTO_WIDTH : width;
//				params.height = height == 0 ? PHOTO_HEIGHT : height;
//				imageView.setLayoutParams(params);
//				imageView.setImageResource(CPResourceUtil.getDrawableId(mContext,
//						"im_imsdk_default"));
//			} else {
//				final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imageView
//						.getLayoutParams();
//
//				if (width == 0 || height == 0) {
//					width = photo.getWidth();
//					height = photo.getHeight();
//
//					float scale = calculateImageScaleRatio(width, height);
//
//					width = (int) (width * scale);
//					height = (int) (height * scale);
//				}
//
//				params.width = width;
//				params.height = height;
//
//				imageView.setLayoutParams(params);
//
//				IMImageThumbnail thumbnail = IMImagesMgr.getInstance().getThumbnail(
//						userMsg.getFileID(), width, height);
//
//				imageView.setImageBitmap(thumbnail.getBitmap());
//
//				updatePhotoUI(position, imageProgress, 100, imageMask, addon, status,
//						rightProgress);
//			}
//
//			//聊天图片点击事件
//			imageView.setOnClickListener(new View.OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					if (photo.getBitmap() == null) {
//						return;
//					}
//
//					// 大图显示初始化
//					DisplayMetrics  dm = new DisplayMetrics();
//					WindowManager windowManager = (WindowManager) mContext
//							.getSystemService(Context.WINDOW_SERVICE);
//					windowManager.getDefaultDisplay().getMetrics(dm);
//					
//					if (popupWindow == null || chatImageView == null) {
//						chatImageView = mInflater.inflate(mIDs.get(32), null);
//						popupWindow = new PopupWindow(chatImageView, dm.widthPixels, dm.heightPixels);
//					}
//
//					popupWindow.setFocusable(true);
//					popupWindow.setOutsideTouchable(true);// 设置允许在外点击消失
//					popupWindow.setBackgroundDrawable(new BitmapDrawable(mContext.getResources(), 
//							Bitmap.createBitmap(dm.widthPixels, dm.heightPixels, Bitmap.Config.ALPHA_8)));
//
//					IMChatImageView image = ViewHolder.get(chatImageView,
//							CPResourceUtil.getId(mContext, "imimg_image"));
//
//					image.setImageBitmap(photo.getBitmap());
//
//					popupWindow.showAtLocation(mImchatView, Gravity.CENTER, 0, 0);
//				}
//			});
//			
//			imageView.setTag(position);
//			imageView.setOnLongClickListener(mContentLongClickListener);
//		}
//			break;
//		default:
//			DTLog.logError();
//			break;
//		}
//	}

	private void updatePhotoUI(int msgStatus, int progress, ViewHolder holder) {
		switch (msgStatus) {
		case IMMessage.SUCCESS:
			holder.pb.setVisibility(View.GONE);
			holder.tv.setVisibility(View.GONE);
			holder.staus_iv.setVisibility(View.GONE);
			break;
		case IMMessage.FAILURE:
			holder.pb.setVisibility(View.GONE);
			holder.tv.setVisibility(View.GONE);
			holder.staus_iv.setVisibility(View.VISIBLE);
			break;
		case IMMessage.SENDING_OR_RECVING:
			holder.staus_iv.setVisibility(View.GONE);
			holder.pb.setVisibility(View.VISIBLE);
			holder.tv.setVisibility(View.VISIBLE);
			
			holder.tv.setText(progress + "%");
			break;
		}
	}
	
	private void updateTextUI(int msgStatus, ViewHolder holder) {
		switch (msgStatus) {
		case IMMessage.SUCCESS: // 发送成功
			holder.pb.setVisibility(View.GONE);
			holder.staus_iv.setVisibility(View.GONE);
			break;
		case IMMessage.FAILURE: // 发送失败
			holder.pb.setVisibility(View.GONE);
			holder.staus_iv.setVisibility(View.VISIBLE);
			break;
		case IMMessage.SENDING_OR_RECVING: // 发送中
			holder.pb.setVisibility(View.VISIBLE);
			holder.staus_iv.setVisibility(View.GONE);
			break;
		}
	}
	
	private View.OnClickListener mResendClickListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			final IMUserMsg userMsg = (IMUserMsg) v.getTag();
			
			if(userMsg == null) return;
			
			final IMUserMsgHistory history;
			if(userMsg.mTags != null && userMsg.mTags.containsKey("orderId")) {
				String orderId = userMsg.mTags.get("orderId").toString();
				
				history = IMUserMsgHistoriesMgr.getInstance()
						.getUserChatMsgHistory(mCustomUserID, orderId);
			} else {
				history = IMUserMsgHistoriesMgr.getInstance()
						.getUserChatMsgHistory(mCustomUserID);
			}

			history.removeUnsentUserMsgWithClientSendTime(userMsg.mClientSendTime);
			history.saveFile();

			userMsg.removeFile();

			IMChatViewAdapter.this.notifyDataSetChanged();

			if (userMsg.mUserMsgType == UserMsgType.Normal) {
				IMMyself.sendText(userMsg.mContent, userMsg.mToCustomUserID, userMsg.mTags,
						10, new OnActionListener() {
							@Override
							public void onSuccess() {
							}

							@Override
							public void onFailure(String error) {
							}
						});
			} else if (userMsg.mUserMsgType == UserMsgType.Photo) {
				final long actionTime = System.currentTimeMillis() / 1000;

				userMsg.mClientSendTime = actionTime;
				userMsg.saveFile();

				// 维护聊天记录
				history.insertUnsentUserMsg(userMsg.mClientSendTime);
				history.saveFile();
				DTNotificationCenter.getInstance().postNotification(
						history.getNewMsgNotificationKey());

				// 维护最近联系人
				IMPrivateRecentContacts.getInstance().insert(
						userMsg.mToCustomUserID);
				IMPrivateRecentContacts.getInstance().saveFile();
				DTNotificationCenter.getInstance()
						.postNotification(
								IMPrivateRecentContacts.getInstance()
										.notificationKey());

				// 发送消息
				IMActionSendUserMsg action = new IMActionSendUserMsg();

				action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
					@Override
					public void onActionDoneEnd() {
						// 维护聊天记录
						history.replaceUnsentUserMsgToSent(userMsg);
						history.saveFile();
					}
				};

				action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
					@Override
					public void onActionPartiallyDone(double percentage) {
					}
				};

				action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
					@Override
					public void onActionFailedEnd(String error) {
					}
				};

				action.mUserMsg = userMsg;

				action.mBeginTime = actionTime;
				action.begin();
			} else if (userMsg.mUserMsgType == UserMsgType.Audio) {

			}
		}
	};
	
	private int dip2px(Context context, float dpValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dpValue * scale + 0.5f);
	}
	
	public static class ViewHolder {
		ImageView iv_sendPicture_2;
		ImageView iv;
		TextView tv;
		ProgressBar pb;
		ImageView staus_iv;
		ImageView head_iv;
		TextView tv_userId;
		ImageView playBtn;
		TextView timeLength;
		TextView size;
		LinearLayout container_status_btn;
		LinearLayout ll_container;
		ImageView iv_read_status;
		// 显示已读回执状态
		TextView tv_ack;
		// 显示送达回执状态
		TextView tv_delivered;

		TextView tv_file_name;
		TextView tv_file_size;
		TextView tv_file_download_state;

		//消息内容
		RelativeLayout rl_chat_content;
		LinearLayout ll_system_secretary_content;
		// 系统小秘书内容
		RelativeLayout rl_system_sceretary;
		//		ImageView iv_system_head_pic;
		TextView tv_system_title;
		TextView tv_system_address;
		TextView tv_system_time;
		TextView tv_try_money;
		TextView tv_gratuity;
		TextView tv_pay_type; // 支付方式 0 线下 1 线上
		
		ImageView iv_location;
		
		RelativeLayout order_view_1;
		RelativeLayout order_view_2;
		RelativeLayout order_view_3;
		View order_clickview_2;
		View order_clickview_3;
		
		TextView tv_chatnotice;
	}
}
