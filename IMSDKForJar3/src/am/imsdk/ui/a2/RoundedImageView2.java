package am.imsdk.ui.a2;

import am.imsdk.serverfile.image.IMImageThumbnail;
import am.imsdk.serverfile.image.IMImagesMgr;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.ImageView;

public class RoundedImageView2 extends ImageView {
	private final float mDensity = getContext().getResources().getDisplayMetrics().density;
	private float mRoundness;
	private String mFileID;

	public RoundedImageView2(Context context) {
		super(context);

		init();
	}

	public RoundedImageView2(Context context, AttributeSet attrs) {
		super(context, attrs);

		init();
	}

	public RoundedImageView2(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		init();
	}

	@Override
	public void draw(Canvas canvas) {
		final Bitmap composedBitmap;
		final Bitmap originalBitmap;
		final Canvas composedCanvas;
		final Canvas originalCanvas;
		final Paint paint;
		final int height;
		final int width;

		width = getWidth();

		height = getHeight();

		composedBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		originalBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

		composedCanvas = new Canvas(composedBitmap);
		originalCanvas = new Canvas(originalBitmap);

		paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);

		super.draw(originalCanvas);

		composedCanvas.drawARGB(0, 0, 0, 0);

		composedCanvas.drawRoundRect(new RectF(0, 0, width, height), this.mRoundness,
				this.mRoundness, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));

		composedCanvas.drawBitmap(originalBitmap, 0, 0, paint);

		canvas.drawBitmap(composedBitmap, 0, 0, new Paint());
	}

	public float getCornerRadius() {
		return this.mRoundness / this.mDensity;
	}

	public void setCornerRadius(float roundness) {
		if (roundness >= 90) {
			roundness = 90;
		} else if (roundness <= 0) {
			roundness = 0;
		}

		roundness = roundness / 0.9f;
		this.mRoundness = roundness * this.mDensity;
	}

	private void init() {
		// 括号中的数字是调整图片弧度的 调成100为圆形图片 调成15为圆角图片
		setCornerRadius(0);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);

		if (mFileID != null && mFileID.length() > 0) {
			IMImageThumbnail thumbnail = IMImagesMgr.getInstance().getThumbnail(
					mFileID, w, h);
			
			this.setImageBitmap(thumbnail.getBitmap());
		}
	}

	public void setFileID(String fileID) {
		mFileID = fileID;
	}
}
