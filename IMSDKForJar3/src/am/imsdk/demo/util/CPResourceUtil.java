package am.imsdk.demo.util;

import android.content.Context;

public class CPResourceUtil {
	public static int getLayoutId(Context paramContext, String paramString) {
		return getIdByName(paramContext, "layout", paramString);
	}

	public static int getAnimtId(Context paramContext, String paramString) {
		return getIdByName(paramContext, "anim", paramString);
	}
	
	public static int getRawId(Context paramContext, String paramString) {
		return getIdByName(paramContext, "raw", paramString);
	}

	public static int getStringId(Context paramContext, String paramString) {
		return getIdByName(paramContext, "string", paramString);
	}

	public static int getDrawableId(Context paramContext, String paramString) {
		return getIdByName(paramContext, "drawable", paramString);
	}

	public static int getStyleId(Context paramContext, String paramString) {
		return getIdByName(paramContext, "style", paramString);
	}

	public static int getstyleableId(Context paramContext, String paramString) {
		return getIdByName(paramContext, "styleable", paramString);
	}

	public static int[] getstyleableIds(Context paramContext, String paramString) {
		return getIdsByName(paramContext, "styleable", paramString);
	}

	public static int getId(Context paramContext, String paramString) {
		return getIdByName(paramContext, "id", paramString);
		// return paramContext.getResources().getIdentifier(paramString,"id",
		// paramContext.getPackageName());
	}

	public static int getColorId(Context paramContext, String paramString) {
		return getIdByName(paramContext, "color", paramString);
	}

	public static int getArrayId(Context paramContext, String paramString) {
		return getIdByName(paramContext, "array", paramString);
	}

	public static int getDimenId(Context paramContext, String paramString) {
		return getIdByName(paramContext, "dimen", paramString);
	}

	@SuppressWarnings("rawtypes")
	public static int getIdByName(Context context, String className, String name) {
		String packageName = context.getPackageName();
		Class r = null;
		int id = 0;
		
		try {
			r = Class.forName(packageName + ".R");

			Class[] classes = r.getClasses();
			Class desireClass = null;

			for (int i = 0; i < classes.length; ++i) {
				if (classes[i].getName().split("\\$")[1].equals(className)) {
					desireClass = classes[i];
					break;
				}
			}

			if (desireClass != null) {
				id = desireClass.getField(name).getInt(desireClass);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}

		return id;
	}

	@SuppressWarnings("rawtypes")
	public static int[] getIdsByName(Context context, String className, String name) {
		String packageName = context.getPackageName();
		Class r = null;
		int[] ids = null;
		try {
			r = Class.forName(packageName + ".R");

			Class[] classes = r.getClasses();
			Class desireClass = null;

			for (int i = 0; i < classes.length; ++i) {
				if (classes[i].getName().split("\\$")[1].equals(className)) {
					desireClass = classes[i];
					break;
				}
			}

			if ((desireClass != null)
					&& (desireClass.getField(name).get(desireClass) != null)
					&& (desireClass.getField(name).get(desireClass).getClass()
							.isArray())) {
				ids = (int[]) desireClass.getField(name).get(desireClass);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}

		return ids;
	}
}