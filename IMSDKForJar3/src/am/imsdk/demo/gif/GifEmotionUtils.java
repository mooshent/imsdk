package am.imsdk.demo.gif;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import am.imsdk.demo.util.FacesXml;
import am.imsdk.demo.util.FileUtils;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.widget.AbsListView.OnScrollListener;
import android.widget.EditText;
import android.widget.TextView;

public class GifEmotionUtils {
	private int showGifNum = 9;

	private Map<TextView, List<GifTextDrawable>> mGifMap;
	// private int[] mEmotions;
	// private String[] mEmotionDescs;
	private HashMap<String, String> mMapFaces;

	private Callback mCallback;

	// 存储正文中表情符号的位置
	class FacePos {
		int s;// 表情文字起始
		int e;// 表情文字结束点
		String i;// 该表情文字表示的表情的序号

		public FacePos(int s, int e, String i) {
			this.s = s;
			this.e = e;
			this.i = i;
		}
	}

	public void setShowGifNum(int showGifNum) {
		this.showGifNum = showGifNum;
	}

	public GifEmotionUtils(int showGifNum) {
		mMapFaces = FacesXml.getFacesMap();

		mCallback = null;
		mGifMap = new LinkedHashMap<TextView, List<GifTextDrawable>>();

		this.showGifNum = showGifNum;
	}

	private int mScrollState;

	public void setScrollState(int scrollState) {
		mScrollState = scrollState;
	}

	public void setSpannableText(final TextView tv, final String content,
			final Handler handler) {
		synchronized (mGifMap) {
			List<GifTextDrawable> list = mGifMap.get(tv);
			if (list != null) {
				for (GifTextDrawable gifTextDrawable : list) {
					gifTextDrawable.stop();
				}
				mGifMap.remove(tv);
			}
		}

		final int selection = tv.getSelectionStart();

		tv.setText("");

		if (content.indexOf('{') > -1 && content.indexOf('}') > -1) {
			// 用以存储需替换的表情的位置
			final List<FacePos> faceList = new ArrayList<FacePos>();

			// 查找表情的位置
			for (int i = 0; i < content.length(); i++) {
				int start = content.indexOf('{', i);
				if (start != -1) {
					int end = content.indexOf('}', start + 1);
					if (end != -1) {
						String gifName = content.substring(start, end + 1);
						if (mMapFaces.containsKey(gifName)) {
							FacePos fp = new FacePos(start, end, mMapFaces.get(gifName));

							faceList.add(fp);
							i = end;
						} else {
							i = start;
						}
					} else {
						break;
					}
				} else {
					break;
				}
			}

			// 如果无表情
			if (faceList.size() == 0) {
				tv.post(new Runnable() {
					@Override
					public void run() {
						tv.setText(content);

						if (tv instanceof EditText) {
							((EditText) tv).setSelection(selection);
						}
					}
				});
			} else {
				int emotionCount = faceList.size();

				showStaticImage(tv, content, faceList);

				if (emotionCount > showGifNum
						|| mScrollState == OnScrollListener.SCROLL_STATE_TOUCH_SCROLL
						|| mScrollState == OnScrollListener.SCROLL_STATE_FLING
						|| hasStoped) {
					return;
				}

				new Thread(new Runnable() {
					@SuppressWarnings("deprecation")
					@Override
					public void run() {
						// 显示新的已经替换表情的text
						// 如果有表情
						final SpannableString ss = new SpannableString(content);

						final List<GifTextDrawable> gifList = new ArrayList<GifTextDrawable>();
						for (int j = 0; j < faceList.size(); j++) {
							// 开始新动画
							GifTextDrawable textDrawable = new GifTextDrawable(tv);

							GifOpenHelper gifOpenHelper = new GifOpenHelper();

							FileInputStream fis = null;

							try {
								fis = new FileInputStream(new File(FileUtils
										.getGifStorePath(), faceList.get(j).i));
							} catch (FileNotFoundException e1) {
								e1.printStackTrace();
							}

							int status = gifOpenHelper.read(fis);
							String gifName = faceList.get(j).i;
							Bitmap bitmap = FacesXml.face_StaticBitmaps.get(gifName);
							BitmapDrawable bd = null;

							if (status == GifOpenHelper.STATUS_OK) {
								if (bitmap != null) {
									bd = new BitmapDrawable(bitmap);
//									bd = new BitmapDrawable(DTAppEnv.getContext()
//											.getResources(), bitmap);
								} else {
									Bitmap bmp = gifOpenHelper.getFrame(0);
									bd = new BitmapDrawable(bmp);
//									bd = new BitmapDrawable(DTAppEnv.getContext()
//											.getResources(), bmp);
									FacesXml.face_StaticBitmaps.put(gifName, bmp);
								}

								textDrawable.addFrame(bd, gifOpenHelper.getDelay(0));

								for (int i = 1; i < gifOpenHelper.getFrameCount(); i++) {
									textDrawable.addFrame(
											new BitmapDrawable( gifOpenHelper
													.nextBitmap()), gifOpenHelper
													.getDelay(i));
//									textDrawable.addFrame(
//											new BitmapDrawable(DTAppEnv.getContext()
//													.getResources(), gifOpenHelper
//													.nextBitmap()), gifOpenHelper
//													.getDelay(i));
								}
							} else {
								try {
									if (bitmap != null) {
										bd = new BitmapDrawable( bitmap);
//										bd = new BitmapDrawable(DTAppEnv.getContext()
//												.getResources(), bitmap);
									} else {
										Bitmap bmp = FacesXml.getBitmap(gifName);
										bd = new BitmapDrawable(bmp);
//										bd = new BitmapDrawable(DTAppEnv.getContext()
//												.getResources(), bmp);
										FacesXml.face_StaticBitmaps.put(gifName, bmp);
									}
									textDrawable.addFrame(bd, 1000);
								} catch (Exception e) {
								}
							}

							textDrawable
									.setBounds(0, 0, FacesXml.faceH, FacesXml.faceW);

							textDrawable.setOneShot(false);

							ImageSpan span = new ImageSpan(textDrawable,
									ImageSpan.ALIGN_BOTTOM);
							// 替换一个表情
							ss.setSpan(span, faceList.get(j).s, faceList.get(j).e + 1,
									Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
							gifList.add(textDrawable);
							// 显示新的已经替换表情的text
							if (!tv.getText().toString().equals(content)) {
								return;
							}
						}
						
						synchronized (mGifMap) {
							mGifMap.put(tv, gifList);
						}

						final SpannableString ssForPost = ss;

						handler.postDelayed(new Runnable() {
							public void run() {
								if (!tv.getText().toString().equals(content)) {
									return;
								}
								tv.setText(ssForPost);
								if (tv instanceof EditText) {
									((EditText) tv).setSelection(selection);
								}
								for (int i = 0; i < gifList.size(); i++) {
									gifList.get(i).start();
								}
							}
						},50);

						if (mCallback != null) {
							mCallback.onGifEmotionUpdate();
						}
					}
				}).start();
			}
		} else {
			tv.setText(content);
		}
	}

	public void setStaticEmotionText(final TextView tv, final String content) {
		if (content.indexOf('{') > -1 && content.indexOf('}') > -1) {
			// 用以存储需替换的表情的位置
			final List<FacePos> faceList = new ArrayList<FacePos>();

			// 查找表情的位置
			for (int i = 0; i < content.length(); i++) {
				int start = content.indexOf('{', i);
				if (start != -1) {
					int end = content.indexOf('}', start + 1);
					String gifName = content.substring(start, end + 1);
					if (mMapFaces.containsKey(gifName)) {
						FacePos fp = new FacePos(start, end, mMapFaces.get(gifName));

						faceList.add(fp);
						i = end;
					} else {
						i = start;
					}
				} else {
					break;
				}
			}

			// 如果无表情
			if (faceList.size() == 0) {
				tv.setText(content);
			} else {
				showStaticImage(tv, content, faceList);
			}
		} else {
			tv.setText(content);
		}

	}

	private void showStaticImage(final TextView tv, String content,
			final List<FacePos> faceList) {
		tv.setText(content);

		SpannableString ss = new SpannableString(content);
		int emotionCount = faceList.size();
		int selection = tv.getSelectionStart();

		tv.setText(ss);

		if (tv instanceof EditText) {
			((EditText) tv).setSelection(selection);
		}

		for (int i = 0; i < emotionCount; i++) {
			String gifName = faceList.get(i).i;
			Bitmap bitmap = FacesXml.face_StaticBitmaps.get(gifName);

			if (bitmap == null) {
				FileInputStream fis = null;

				try {
					fis = new FileInputStream(new File(FileUtils.getGifStorePath(),
							gifName));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}

				GifOpenHelper gifOpenHelper = new GifOpenHelper();

				gifOpenHelper.setIsOnlyGetFirstFrame(true);

				int status = gifOpenHelper.read(fis);

				if (status == GifOpenHelper.STATUS_OK) {
					bitmap = gifOpenHelper.getImage();
					if (FacesXml.faceH == 0) {
						FacesXml.faceH = bitmap.getHeight();
						FacesXml.faceW = bitmap.getWidth();
					}

					FacesXml.face_StaticBitmaps.put(gifName, bitmap);
				} else {
					try {
						bitmap = FacesXml.getBitmap(gifName);
						if (FacesXml.faceH == 0) {
							FacesXml.faceH = bitmap.getHeight();
							FacesXml.faceW = bitmap.getWidth();
						}
						FacesXml.face_StaticBitmaps.put(gifName, bitmap);
					} catch (Exception e) {
					}
				}

				// bitmap = FacesXml.getBitmap(gifName);
			}

			@SuppressWarnings("deprecation")
			BitmapDrawable statictDrawable = new BitmapDrawable(bitmap);

			statictDrawable.setBounds(0, 0, FacesXml.faceW, FacesXml.faceH);

			ImageSpan span = new ImageSpan(statictDrawable, ImageSpan.ALIGN_BOTTOM);

			// 替换一个表情
			ss.setSpan(span, faceList.get(i).s, faceList.get(i).e + 1,
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		}

		tv.setText(ss);
	}

	public void destory() {
		synchronized (mGifMap) {
			Set<TextView> keys = mGifMap.keySet();

			for (TextView textView : keys) {
				List<GifTextDrawable> list = mGifMap.get(textView);

				if (list != null) {
					for (GifTextDrawable gifTextDrawable : list) {
						gifTextDrawable.stop();
					}
				}
			}

			mGifMap.clear();
			FacesXml.face_StaticBitmaps.clear();
		}
	}

	public void setCallback(Callback mCallback) {
		this.mCallback = mCallback;
	}

	public interface Callback {
		void onGifEmotionUpdate();
	}

	private boolean hasStoped;

	public void stopAllGif() {
		synchronized (mGifMap) {

			if (mGifMap.size() <= 0) {
				return;
			}
			hasStoped = true;
			Set<TextView> keys = mGifMap.keySet();

			for (TextView textView : keys) {
				List<GifTextDrawable> list = mGifMap.get(textView);
				if (list != null) {
					for (GifTextDrawable gifTextDrawable : list) {
						gifTextDrawable.stop();
					}
				}
			}
			mGifMap.clear();
			hasStoped = false;
		}
	}
}
