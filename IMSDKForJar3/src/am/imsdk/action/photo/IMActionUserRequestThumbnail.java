package am.imsdk.action.photo;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.imsdk.aacmd.user.IMCmdUserGetInfo;
import am.imsdk.action.IMAction;
import am.imsdk.action.fileserver.IMActionDownloadImage;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.userinfo.IMPrivateUserInfo;
import am.imsdk.model.userinfo.IMUsersMgr;
import am.imsdk.serverfile.image.IMImageThumbnail;
import am.imsdk.serverfile.image.IMImagesMgr;
import android.graphics.Bitmap;
import android.util.Log;

// 1. 获取uid
// 2. 更新baseInfo
// 3. 查询本地 -> 下载图片
public final class IMActionUserRequestThumbnail extends IMAction {
	public String mCustomUserID = "";
	public int mWidth;
	public int mHeight;
	public Bitmap mBitmap;
	public byte[] mBuffer;
	private long mUID;

	public IMActionUserRequestThumbnail() {
		mStepCount = 3;
	}

	@Override
	public void onActionBegan() {
		if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)) {
			doneWithIMSDKError();
			return;
		}

		if (mWidth <= 0) {
			doneWithIMSDKError();
			return;
		}

		if (mHeight <= 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			checkBeginGetUID(mCustomUserID);
		}
			break;
		case 2: {
			mUID = IMUsersMgr.getInstance().getUID(mCustomUserID);

			if (mUID == 0) {
				doneWithIMSDKError();
				return;
			}

			IMCmdUserGetInfo cmd = new IMCmdUserGetInfo();

			cmd.addUID(mUID);
			cmd.addProperty("baseinfo");

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					done(errorJsonObject != null ? errorJsonObject.toString()
							: "Error code:" + errorCode);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					nextStep();
				}
			};

			cmd.send();
		}
			break;
		case 3: {
			IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(mUID);
			String mainPhotoFileID = userInfo.getMainPhotoFileID();

			Log.e("IMSDK", mainPhotoFileID);
			DTLog.sign("mainPhotoFileID:" + mainPhotoFileID);

			if (mainPhotoFileID.length() == 0) {
				done();
				return;
			}

			// 获取本地数据
			IMImageThumbnail thumbnail = IMImagesMgr.getInstance().getThumbnail(
					mainPhotoFileID, mWidth, mHeight);

			// 存在的话就不下载了
			if (thumbnail.getBitmap() != null) {
				mBitmap = thumbnail.getBitmap();
				mBuffer = thumbnail.getBuffer();
				done();
				DTLog.sign("mainPhotoFileID3:" + mainPhotoFileID);
				return;
			}

			final IMActionDownloadImage action = new IMActionDownloadImage();

			action.mFileID = mainPhotoFileID;
			action.mWidth = mWidth;
			action.mHeight = mHeight;

			action.mOnActionFailedListener = new OnActionFailedListener() {
				@Override
				public void onActionFailed(String error) {
					done(error);
				}
			};

			action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
				@Override
				public void onActionPartiallyDone(double percentage) {
					done(percentage);
				}
			};

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					done();
				}
			};

			action.begin();
		}
			break;
		default:
			break;
		}
	}
}
