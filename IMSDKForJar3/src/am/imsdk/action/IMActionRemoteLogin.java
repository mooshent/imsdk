package am.imsdk.action;

import imsdk.data.IMMyself.LoginStatus;

import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import remote.service.MainToSocketProcessStub;
import remote.service.data.IMRemoteMyself;
import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.socket.DTSocket;
import am.dtlib.model.c.socket.DTSocket.DTSocketStatus;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.aacmd.IMSocket;
import am.imsdk.aacmd.user.IMCmdUserGetSetupID;
import am.imsdk.aacmd.user.IMCmdUserLogin;
import am.imsdk.aacmd.user.IMCmdUserSetSetupID;
import am.imsdk.model.IMAppSettings;
import am.imsdk.model.IMParamJudge;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;

// 注册或登录，用户行为或非用户行为
// 1. 判断网络，重新连接
// 2. 注册、登录
// 完成
public final class IMActionRemoteLogin extends IMAction {
	public long mUID;
	public String mCustomUserID;
	private String mPassword;
	public String mAppKey;
	private IMActionLoginType mType = IMActionLoginType.None;

	private JSONObject mCmdJsonObjectResult = null;

	private Observer mSocketUpdatedObserver;
	
	private int mReTryTime = 0;

	private IMActionRemoteLogin() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (mType == null) {
			doneWithIMSDKError();
			return;
		}

		if (mType == IMActionLoginType.None) {
			doneWithIMSDKError();
			return;
		}

		if (mPassword == null || mPassword.length() == 0) {
			doneWithIMSDKError();
			return;
		}

		if (mType == IMActionLoginType.OneKeyLogin) {
			if (!(mCustomUserID instanceof String)) {
				doneWithIMSDKError();
				return;
			}

			if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)
					&& mCustomUserID != null && mCustomUserID.length() != 0) {
				doneWithIMSDKError();
				return;
			}
		} else {
			if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)) {
				doneWithIMSDKError();
				return;
			}
		}

		if (mType == IMActionLoginType.OneKeyLogin) {
			if (!(mPassword instanceof String)) {
				doneWithIMSDKError();
				return;
			}

			if (!IMParamJudge.isPasswordLegal(mPassword) && mPassword != null
					&& mPassword.length() != 0) {
				doneWithIMSDKError();
				return;
			}

			if (mCustomUserID == null || mCustomUserID.length() == 0) {
				mUID = 0;
			}
		} else {
			if (!IMParamJudge.isPasswordLegal(mPassword)) {
				doneWithIMSDKError();
				return;
			}
		}

		if (!IMParamJudge.isAppKeyLegal(mAppKey)) {
			doneWithIMSDKError();
			return;
		}

		if (mSocketUpdatedObserver != null) {
			DTNotificationCenter.getInstance().removeObserver("SOCKET_UPDATED", mSocketUpdatedObserver);
		}
		
		Log.e("Debug", "IMActionRemoteLogin--创建--"+ IMActionRemoteLogin.this);

		mSocketUpdatedObserver = new Observer() {
			@Override
			public void update(Observable observable, Object data) {
				DTSocketStatus status = DTSocket.getInstance().getStatus();
				
				Log.e("Debug", "IMActionRemoteLogin--mSocketUpdatedObserver:"
						+ IMActionRemoteLogin.this + "---"
						+ status);

				if (isOver() || IMActionRemoteLogin.getInstance() != IMActionRemoteLogin.this) {
					DTNotificationCenter.getInstance().removeObserver("SOCKET_UPDATED", this);
					return;
				}

				if (status == DTSocketStatus.Connected) {

					Log.e("Debug", "IMActionRemoteLogin--"
							+ status);

					if (getCurrentStep() != 1) {
						Log.e("Debug", "IMActionRemoteLogin--"
								+ status
								+ "--but currStep = 2");
//						doneWithNetworkError();
						return;
					}

					IMActionRemoteLogin.this.nextStep();
				} else if (status == DTSocketStatus.None) {
					resetStep();
					
					Log.e("Debug", "----登录尝试次数:" + mReTryTime);
					
//					if(mReTryTime++ >= 3) {
//						done("Login failed");
//						DTSocket.getInstance().checkCloseSocket();
//					}
				}
			}
		};

		DTNotificationCenter.getInstance().addObserver("SOCKET_UPDATED",
				mSocketUpdatedObserver);
	}

//	private void registerIMSDK() {
//		IMCmdUserRegister cmd = new IMCmdUserRegister();
//
//		if (IMActionRemoteLogin.getInstance() != this || isOver()) {
//			return;
//		}
//
//		if (TextUtils.isEmpty(mCustomUserID)) {
//			return;
//		}
//
//		if (TextUtils.isEmpty(mPassword)) {
//			return;
//		}
//
//		cmd.mCustomUserID = mCustomUserID;
//		cmd.mPassword = mPassword;
//
//		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
//			@Override
//			public void onCommonFailed(FailedType type, long errorCode,
//					JSONObject errorJsonObject) throws JSONException {
//				if (IMActionRemoteLogin.getInstance() != IMActionRemoteLogin.this || isOver()) {
//					return;
//				}
//
//				if (type == FailedType.RecvError) {
//					if (100 == errorCode) {
//						done("Wrong Password");
//					} else {
//						done(errorJsonObject != null ? errorJsonObject.toString()
//								: "Error code:" + errorCode);
//					}
//				} else {
//					DTSocket.getInstance().reconnect();
//				}
//			}
//		};
//
//		cmd.mOnRecvEndListener = new OnRecvEndListener() {
//			@Override
//			public void onRecvEnd(JSONObject jsonObject) throws JSONException {
//				if (IMActionRemoteLogin.getInstance() != IMActionRemoteLogin.this || isOver()) {
//					return;
//				}
//
//				mCmdJsonObjectResult = jsonObject;
//				done();
//			}
//		};
//
//		cmd.send();
//	}

	private void loginIMSDK() {
		IMCmdUserLogin cmd = new IMCmdUserLogin();

		if (IMActionRemoteLogin.getInstance() != this || isOver()) {
			return;
		}

		if (TextUtils.isEmpty(mCustomUserID)) {
			done("customUserID is null");
			return;
		}

		if (TextUtils.isEmpty(mPassword)) {
			done("Password is null");
			return;
		}

		cmd.mPhoneNum = mCustomUserID;
		cmd.mPassword = mPassword;

		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
			@Override
			public void onCommonFailed(FailedType type, long errorCode,
					JSONObject errorJsonObject) throws JSONException {
				if (IMActionRemoteLogin.getInstance() != IMActionRemoteLogin.this || isOver()) {
					return;
				}

				if (type == FailedType.RecvError) {
					if (100 == errorCode) {
						done("Wrong Password");
					} else {
						done(errorJsonObject != null ? errorJsonObject.toString()
								: "Error code:" + errorCode);
					}
				} else {
					DTSocket.getInstance().reconnect();
				}
			}
		};

		cmd.mOnRecvEndListener = new OnRecvEndListener() {
			@Override
			public void onRecvEnd(JSONObject jsonObject) throws JSONException {
				if (IMActionRemoteLogin.getInstance() != IMActionRemoteLogin.this || isOver()) {
					return;
				}

				mCmdJsonObjectResult = jsonObject;
				done();
			}
		};

		cmd.send();
	}

	private void autoLoginOrReconnect() {
		IMCmdUserLogin cmd = new IMCmdUserLogin();

		if (IMActionRemoteLogin.getInstance() != this || isOver()) {
			return;
		}
		
		if (TextUtils.isEmpty(mCustomUserID)) {
			done("customUserID is null");
			return;
		}

		if (TextUtils.isEmpty(mPassword)) {
			done("Password is null");
			return;
		}

		cmd.mPhoneNum = mCustomUserID;
		cmd.mPassword = mPassword;
		cmd.mAutoLogin = true;
		cmd.mSetupID = IMAppSettings.getInstance().mSetupID;

		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
			@Override
			public void onCommonFailed(FailedType type, long errorCode,
					JSONObject errorJsonObject) throws JSONException {
				if (IMActionRemoteLogin.getInstance() != IMActionRemoteLogin.this || isOver()) {
					return;
				}

				if (type == FailedType.RecvError) {
					if (100 == errorCode) {
						done("Wrong Password");
					} else if (14 == errorCode) {
						done("Login Conflict");
					} else {
						done(errorJsonObject != null ? errorJsonObject.toString()
								: "Error code:" + errorCode);
					}
				} else {
					DTSocket.getInstance().reconnect();
				}
			}
		};

		cmd.mOnRecvEndListener = new OnRecvEndListener() {
			@Override
			public void onRecvEnd(JSONObject jsonObject) throws JSONException {
				if (IMActionRemoteLogin.getInstance() != IMActionRemoteLogin.this || isOver()) {
					return;
				}

				mCmdJsonObjectResult = jsonObject;
				done();
			}
		};

		cmd.send();
	}

	@Override
	public void onActionStepBegan(final int stepNumber) {
		Log.e("Debug", "onActionStepBegan:" + stepNumber + "---"
				+ IMActionRemoteLogin.this);

		DTSocketStatus status = DTSocket.getInstance().getStatus();

		switch (stepNumber) {
		case 1: {
			if (status == DTSocketStatus.Connected) {
				Log.e("Debug", "start step 2");
				IMActionRemoteLogin.this.nextStep();
				return;
			}

			Log.e("Debug", "onActionStepBegan step = " + status + "--reconnect");
			if(DTAppEnv.isNetworkConnected()) {
				DTSocket.getInstance().reconnect();
			} else {
				done("network closed!");
			}
		}
			break;
		case 2: {
			if (status != DTSocketStatus.Connected) {
				Log.e("Debug", "onActionStepBegan step = " + stepNumber + "---"
						+ status + "--error");
				doneWithNetworkError();
				return;
			}

			 Log.e("Debug", mType + "");

			switch (mType) {
			case OneKeyLogin: {
				if (mCustomUserID == null) {
					mCustomUserID = "";
				}

				if (mPassword == null) {
					mPassword = "";
				}

				// 登录
				loginIMSDK();
			}
				break;
			case OneKeyRegister: {
				if (mCustomUserID == null) {
					mCustomUserID = "";
				}

				if (mPassword == null) {
					mPassword = "";
				}

				// 注册
				// registerIMSDK();
			}
				break;
			case Reconnect: {
				if (mUID == 0) {
					doneWithIMSDKError();
					return;
				}

				autoLoginOrReconnect();
			}
				break;
			case AutoLogin:
				autoLoginOrReconnect();
				break;
			default:
				DTLog.logError();
				break;
			}
		}
			break;
		default:
			DTLog.logError();
			break;
		}

	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {
		if (IMActionRemoteLogin.getInstance() != this) {
			return;
		}

		sSingleton = null;

		if (mCmdJsonObjectResult == null) {
			doneWithIMSDKError();
			return;
		}

		long uid = 0;
		long sid = 0;
		boolean autoCreateCustomUserID = false;

		try {
			uid = mCmdJsonObjectResult.getLong("uid");
			sid = mCmdJsonObjectResult.getLong("sid");

			if (uid == 0) {
				doneWithServerError();
				return;
			}

			if (sid == 0) {
				doneWithServerError();
				return;
			}
			
			IMSocket.getInstance().setUID(uid);
			IMSocket.getInstance().setSessionID(sid);

			IMRemoteMyself.getInstance().setUID(uid);
//			IMRemoteMyself.getInstance().setSessionID(sid);
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			DTLog.log(e.toString());
			this.done("IMSDK Server error");
			return;
		}
		
		// setup id
		if (IMAppSettings.getInstance().mSetupID == 0) {
			IMCmdUserGetSetupID cmd = new IMCmdUserGetSetupID();

			cmd.send();
		} else {
			IMCmdUserSetSetupID cmd = new IMCmdUserSetSetupID();

			cmd.mSetupID = IMAppSettings.getInstance().mSetupID;
			cmd.send();
		}
		
		IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.Logined);

		IMAppSettings.getInstance().mLastLoginCustomUserID = mCustomUserID;
		IMAppSettings.getInstance().mLastLoginPassword = mPassword;

		if (mType == IMActionLoginType.OneKeyLogin) {
			if (IMAppSettings.getInstance().mOneKeyLoginCustomUserID != null
					&& IMAppSettings.getInstance().mOneKeyLoginCustomUserID.length() != 0
					&& IMAppSettings.getInstance().mOneKeyLoginCustomUserID
							.equals(mCustomUserID)) {
				if (IMAppSettings.getInstance().mOneKeyLoginPassword != null
						&& IMAppSettings.getInstance().mOneKeyLoginPassword.length() != 0
						&& !IMAppSettings.getInstance().mOneKeyLoginPassword
								.equals(mPassword)) {
					DTLog.logError();
				}
			}
//			else if (autoCreateCustomUserID) {
//				if (IMAppSettings.getInstance().mOneKeyLoginCustomUserID != null
//						&& IMAppSettings.getInstance().mOneKeyLoginCustomUserID
//								.length() > 0) {
//					DTLog.logError();
//				}
//
//				IMAppSettings.getInstance().mOneKeyLoginCustomUserID = IMPrivateMyself
//						.getInstance().getCustomUserID();
//				IMAppSettings.getInstance().mOneKeyLoginPassword = IMPrivateMyself
//						.getInstance().getPassword();
//			}
		}

		IMAppSettings.getInstance().saveFile();
		
		if (mType == IMActionLoginType.OneKeyLogin || 
				mType == IMActionLoginType.OneKeyRegister) {
			if(MainToSocketProcessStub.getInstance().getLoginRecvListener() != null) {
				try {
					MainToSocketProcessStub.getInstance().getLoginRecvListener().onRecvSuccess(mCmdJsonObjectResult.toString());
					MainToSocketProcessStub.getInstance().setLoginRecvListener();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
		} else if (mType == IMActionLoginType.AutoLogin || 
				mType == IMActionLoginType.Reconnect) {
			
		} else {
			DTLog.logError();
			return;
		}

		if (IMAppSettings.getInstance().mLastLoginCustomUserID == null
				|| IMAppSettings.getInstance().mLastLoginCustomUserID.length() == 0) {
			DTLog.logError();
			return;
		}
		
		DTNotificationCenter.getInstance().postNotification("Remote_Logined");
		
//		IMActionRemoteLogin.newInstance();
	}

	@Override
	public void onActionFailed(String error) {
		if (IMActionRemoteLogin.getInstance() != this) {
			return;
		}

		sSingleton = null;

		Log.e("Debug", "IMActionRemoteLogin failed----type:" + mType + ",time:" + mReTryTime);
		
		
		switch (mType) {
		case OneKeyLogin:
		case OneKeyRegister:
		// 用户行为
		{
			IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
			
			IMAppSettings.getInstance().mLastLoginCustomUserID = "";
			IMAppSettings.getInstance().saveFile();
			
			try {
				if(MainToSocketProcessStub.getInstance().getLoginRecvListener() != null) {
					MainToSocketProcessStub.getInstance().getLoginRecvListener().onRecvFailure(error);
					MainToSocketProcessStub.getInstance().setLoginRecvListener();
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
			break;
		case Reconnect: {
			if (error.equals("Wrong Password") || error.equals("Login Conflict")) {
				IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
				IMAppSettings.getInstance().mLastLoginCustomUserID = "";
				IMAppSettings.getInstance().saveFile();
			} else {
				IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
			}
		}
			break;
		case AutoLogin: {
			DTLog.sign("AutoLogin done");

			if (error.equals("Wrong Password") || error.equals("Login Conflict")) {
				IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
				IMAppSettings.getInstance().mLastLoginCustomUserID = "";
				IMAppSettings.getInstance().saveFile();
			} else {
				IMRemoteMyself.getInstance().setRemoteLoginStatus(LoginStatus.None);
			}
		}
			break;
		default:
			break;
		}
		
		IMActionRemoteLogin.newInstance();
	}

	public void setType(IMActionLoginType type) {
		mType = type;
		DTLog.sign("IMActionLoginType:" + type.toString());
	}
	
	public void setType(int type) {
		mType = IMActionLoginType.fromInt(type);
		DTLog.sign("IMActionLoginType:" + mType.toString());
	}

	public IMActionLoginType getType() {
		return mType;
	}

	public void setPassword(String password) {
		if (password == null || password.length() == 0) {
			DTLog.logError();
			return;
		}

		mPassword = password;
	}

	public enum IMActionLoginType {
		None(0),

		// 用户行为
		OneKeyLogin(3), OneKeyRegister(4),

		// 非用户行为
		Reconnect(10), AutoLogin(11);

		private final int value;

		private IMActionLoginType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		private static final SparseArray<IMActionLoginType> sMapValues = new SparseArray<IMActionLoginType>();

		static {
			for (IMActionLoginType type : IMActionLoginType.values()) {
				sMapValues.put(type.value, type);
			}
		}

		public static IMActionLoginType fromInt(int i) {
			IMActionLoginType type = sMapValues.get(Integer.valueOf(i));

			if (type == null) {
				DTLog.logError();
				return null;
			}

			return type;
		}
	}

	// singleton
	private volatile static IMActionRemoteLogin sSingleton;

	public static IMActionRemoteLogin getInstance() {
		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
//		printCallStatck();
		synchronized (IMActionRemoteLogin.class) {
			if (sSingleton != null && sSingleton.mSocketUpdatedObserver != null) {
				DTNotificationCenter.getInstance().removeObserver("SOCKET_UPDATED",
						sSingleton.mSocketUpdatedObserver);
			}

			sSingleton = new IMActionRemoteLogin();
		}
	}
	
	public static void printCallStatck() {
        Throwable ex = new Throwable();
        StackTraceElement[] stackElements = ex.getStackTrace();
        if (stackElements != null) {
            for (int i = 0; i < stackElements.length; i++) {
                System.out.print(stackElements[i].getClassName()+"--");
                System.out.print(stackElements[i].getFileName()+"--");
                System.out.print(stackElements[i].getLineNumber()+"--");
                System.out.println(stackElements[i].getMethodName());
            }
        }
        System.out.println("**********************************");
    }
}
