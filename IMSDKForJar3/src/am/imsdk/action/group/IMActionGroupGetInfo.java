package am.imsdk.action.group;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.team.IMCmdTeamGetInfo;
import am.imsdk.aacmd.team.IMCmdTeamGetMembers;
import am.imsdk.aacmd.user.IMCmdUserGetInfo;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo.TeamType;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import am.imsdk.model.userinfo.IMUsersMgr;

// a. 获取Group信息
// b.1 获取Group成员列表
// b.2 获取Group成员列表customUserID
public final class IMActionGroupGetInfo extends IMAction {
	public String mGroupID = "";
	private long mTeamID;
	private boolean mStepADone = false;
	private boolean mStepBDone = false;

	public IMActionGroupGetInfo() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (!IMParamJudge.isGroupIDLegal(mGroupID)) {
			doneWithIMSDKError();
			return;
		}

		mTeamID = DTTool.getUnsecretLongValue(mGroupID);

		if (mTeamID <= 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			{
				IMCmdTeamGetInfo cmd = new IMCmdTeamGetInfo();

				cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
					@Override
					public void onCommonFailed(FailedType type, long errorCode,
							JSONObject errorJsonObject) throws JSONException {
						commonFailedDealWithJudge(type);
					}
				};

				cmd.mOnRecvEndListener = new OnRecvEndListener() {
					@Override
					public void onRecvEnd(JSONObject jsonObject) throws JSONException {
						mStepADone = true;

						if (mStepBDone) {
							done();
						}
					}
				};

				cmd.addTeamID(mTeamID);
				cmd.send();
			}

			{
				IMCmdTeamGetMembers cmd = new IMCmdTeamGetMembers();

				cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
					@Override
					public void onCommonFailed(FailedType type, long errorCode,
							JSONObject errorJsonObject) throws JSONException {
						commonFailedDealWithJudge(type);
					}
				};
				
				cmd.mOnRecvEndListener = new OnRecvEndListener() {
					@Override
					public void onRecvEnd(JSONObject jsonObject) throws JSONException {
						nextStep();
					}
				};
				
				cmd.mTeamID = mTeamID;
				cmd.send();
			}
		}
			break;
		case 2:
		{
			IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(mTeamID);
			
			if (mStepADone && teamInfo.mTeamType != TeamType.Group) {
				doneWithIMSDKError();
				return;
			}
			
			if (teamInfo.getUIDList().size() == 0) {
				mStepBDone = true;
				
				if (mStepADone) {
					done();
				}
				
				return;
			}
			
			IMCmdUserGetInfo cmd = new IMCmdUserGetInfo();
			boolean needSend = false;
			
			for (long uid : teamInfo.getUIDList()) {
				String customUserID = IMUsersMgr.getInstance().getCustomUserID(uid);
				
				if (customUserID.length() == 0) {
					cmd.addUID(uid);
					needSend = true;
				}
			}
			
			if (!needSend) {
				mStepBDone = true;
				
				if (mStepADone) {
					done();
				}
				
				return;
			}
			
			cmd.addProperty("phonenum");
			
			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};
			
			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					mStepBDone = true;
					
					if (mStepADone) {
						done();
					}
					
					return;
				}
			};
			
			cmd.send();
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {
	}

	@Override
	public void onActionFailed(String error) {
	}
}
