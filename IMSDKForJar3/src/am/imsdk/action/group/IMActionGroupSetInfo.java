package am.imsdk.action.group;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.im.IMCmdIMSendTeamMsg;
import am.imsdk.aacmd.team.IMCmdTeamSetInfo;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMTeamsMgr;

// 1. 重设GroupInfo
// 2. 发出GroupInfo更新通知
public final class IMActionGroupSetInfo extends IMAction {
	public String mGroupID;
	public String mGroupName;
	public String mExInfo;
	private long mTeamID;

	public IMActionGroupSetInfo() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (!IMParamJudge.isGroupIDLegal(mGroupID)) {
			done(IMParamJudge.getLastError());
			return;
		}

		if (mGroupName == null && mExInfo == null) {
			doneWithIMSDKError();
			return;
		}

		mTeamID = DTTool.getUnsecretLongValue(mGroupID);

		if (mTeamID == 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			boolean needSend = false;
			IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(mTeamID);

			if (mGroupName != null && !mGroupName.equals(teamInfo.mTeamName)) {
				needSend = true;
			} else if (mExInfo != null && !mExInfo.equals(teamInfo.mExInfo)) {
				needSend = true;
			}

			if (!needSend) {
				done("nothing to commit");
				return;
			}

			IMCmdTeamSetInfo cmd = new IMCmdTeamSetInfo();

			cmd.mTeamID = mTeamID;
			cmd.mTeamName = mGroupName;
			cmd.mExInfo = mExInfo;

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					nextStep();
				}
			};

			cmd.send();
		}
			break;
		case 2: {
			IMCmdIMSendTeamMsg cmd = new IMCmdIMSendTeamMsg();

			cmd.mTeamMsgType = TeamMsgType.IMSDKGroupInfoUpdate;
			cmd.mToTeamID = mTeamID;

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					nextStep();
				}
			};

			cmd.send();
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {
	}

	@Override
	public void onActionFailed(String error) {
	}
}
