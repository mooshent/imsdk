package am.imsdk.action.group;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.im.IMCmdIMSendTeamMsg;
import am.imsdk.aacmd.team.IMCmdTeamRemoveMember;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;

// 1. 从群组中移除
// 2. 发送退群通知
public final class IMActionGroupQuit extends IMAction {
	public String mGroupID = "";
	private long mTeamID;

	public IMActionGroupQuit() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (!IMParamJudge.isGroupIDLegal(mGroupID)) {
			doneWithIMSDKError();
			return;
		}

		mTeamID = DTTool.getUnsecretLongValue(mGroupID);

		if (mTeamID == 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			IMCmdTeamRemoveMember cmd = new IMCmdTeamRemoveMember();

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					nextStep();
				}
			};
			
			cmd.mTeamID = mTeamID;
			cmd.mUID = getOwnerUID();
			cmd.send();
		}
			break;
		case 2:
		{
			IMCmdIMSendTeamMsg cmd = new IMCmdIMSendTeamMsg();
			
			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};
			
			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					nextStep();
				}
			};
			
			cmd.mTeamMsgType = TeamMsgType.IMSDKGroupQuit;
			cmd.mToTeamID = mTeamID;
			cmd.send();
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {
	}

	@Override
	public void onActionFailed(String error) {
	}
}
