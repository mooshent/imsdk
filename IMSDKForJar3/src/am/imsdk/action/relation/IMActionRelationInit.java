package am.imsdk.action.relation;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.imsdk.aacmd.team.IMCmdTeamAddMember2WB.WBType;
import am.imsdk.aacmd.team.IMCmdTeamGetMemberFromWB;
import am.imsdk.aacmd.user.IMCmdUserGetInfo;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.userinfo.IMUsersMgr;
import am.imsdk.model.userslist.IMPrivateBlacklistMgr;
import am.imsdk.model.userslist.IMPrivateFriendsMgr;
import android.util.Log;

// 1.a. 获取好友列表
// 1.b. 获取黑名单列表
// 2. 获取好友列表 & 黑名单列表 CustomUserID
public final class IMActionRelationInit extends IMAction {
	private boolean mStepADone = false;
	private boolean mStepBDone = false;
	private ArrayList<Long> mAryFriendsListUIDs = new ArrayList<Long>();
	private ArrayList<Long> mAryBlacklistUIDs = new ArrayList<Long>();

	@Override
	public void onActionBegan() {
		if (!IMPrivateMyself.getInstance().isLogined()) {
			doneWithNetworkError();
			return;
		}
	}

	@Override
	public void onActionDone() {
		if (IMPrivateFriendsMgr.getInstance().setUIDList(mAryFriendsListUIDs)) {
			IMPrivateFriendsMgr.getInstance().saveFile();
		}

		if (IMPrivateBlacklistMgr.getInstance().setUIDList(mAryBlacklistUIDs)) {
			IMPrivateBlacklistMgr.getInstance().saveFile();
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			{
				IMCmdTeamGetMemberFromWB cmd = new IMCmdTeamGetMemberFromWB();

				cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
					@Override
					public void onCommonFailed(FailedType type, long errorCode,
							JSONObject errorJsonObject) throws JSONException {
						commonFailedDealWithJudge(type);
					}
				};

				cmd.mOnRecvEndListener = new OnRecvEndListener() {
					@Override
					public void onRecvEnd(JSONObject jsonObject) throws JSONException {
						Log.e("Debug", "好友列表：" + jsonObject.toString());
						
						if (mAryFriendsListUIDs.size() != 0) {
							doneWithIMSDKError();
							return;
						}

						try {
							JSONArray jsonArray = jsonObject.getJSONArray("uidList");

							for (int i = 0; i < jsonArray.length(); i++) {
								mAryFriendsListUIDs.add(jsonArray.getLong(i));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						if (mStepBDone) {
							nextStep();
							return;
						}

						mStepADone = true;
					}
				};

				cmd.mType = WBType.Friends;
				cmd.send();
			}

			{
				IMCmdTeamGetMemberFromWB cmd = new IMCmdTeamGetMemberFromWB();

				cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
					@Override
					public void onCommonFailed(FailedType type, long errorCode,
							JSONObject errorJsonObject) throws JSONException {
						commonFailedDealWithJudge(type);
					}
				};

				cmd.mOnRecvEndListener = new OnRecvEndListener() {
					@Override
					public void onRecvEnd(JSONObject jsonObject) throws JSONException {
						Log.e("Debug", "黑名单列表：" + jsonObject.toString());
						
						if (mAryBlacklistUIDs.size() != 0) {
							doneWithIMSDKError();
							return;
						}

						try {
							JSONArray jsonArray = jsonObject.getJSONArray("uidList");

							for (int i = 0; i < jsonArray.length(); i++) {
								mAryBlacklistUIDs.add(jsonArray.getLong(i));
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						if (mStepADone) {
							nextStep();
							return;
						}

						mStepBDone = true;
					}
				};

				cmd.mType = WBType.BlackList;
				cmd.send();
			}
		}
			break;
		case 2: {
			IMCmdUserGetInfo cmd = new IMCmdUserGetInfo();
			boolean needSend = false;

			for (long uid : mAryFriendsListUIDs) {
				if (uid == 0) {
					doneWithIMSDKError();
					return;
				}

				String customUserID = IMUsersMgr.getInstance().getCustomUserID(uid);

				if (customUserID.length() == 0) {
					needSend = true;
					cmd.addUID(uid);
				}
			}

			if (!needSend) {
				for (long uid : mAryBlacklistUIDs) {
					if (uid == 0) {
						doneWithIMSDKError();
						return;
					}

					String customUserID = IMUsersMgr.getInstance().getCustomUserID(uid);

					if (customUserID.length() == 0) {
						cmd.addUID(uid);
						needSend = true;
					}
				}
			}

			if (!needSend) {
				done();
				return;
			}

			cmd.addProperty("phonenum");

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) {
					beginStep(2);
				}
			};

			cmd.send();
		}
		default:
			break;
		}
	}
}
