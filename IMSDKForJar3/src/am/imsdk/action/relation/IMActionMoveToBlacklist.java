package am.imsdk.action.relation;

import imsdk.data.IMMyself;
import imsdk.data.relations.IMMyselfRelations;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.imsdk.aacmd.team.IMCmdTeamAddMember2WB;
import am.imsdk.aacmd.team.IMCmdTeamAddMember2WB.WBType;
import am.imsdk.aacmd.team.IMCmdTeamDelMemberFromWb;
import am.imsdk.action.IMAction;
import am.imsdk.action.ammsgs.IMActionSendUserMsg;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.kefu.IMCustomerServiceMgr;
import am.imsdk.model.userinfo.IMUsersMgr;

// 1. 获取uid
// 2. 从好友列表中移除
// 3. 发送IM通知对方（不呈现）
// 4. 加入黑名单列表
public final class IMActionMoveToBlacklist extends IMAction {
	public String mCustomUserID = "";
	private long mUID;

	public IMActionMoveToBlacklist() {
		mStepCount = 4;
	}

	@Override
	public void onActionBegan() {
		if (IMCustomerServiceMgr.getInstance().getUID(mCustomUserID) != 0) {
			done("Can't move customer service to blacklist");
		}

		if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)) {
			doneWithIMSDKError();
			return;
		}

		if (IMMyselfRelations.isMyBlacklistUser(mCustomUserID)) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			checkBeginGetUID(mCustomUserID);
		}
			break;
		case 2: {
			mUID = IMUsersMgr.getInstance().getUID(mCustomUserID);
			if (mUID == 0) {
				doneWithIMSDKError();
				return;
			}

			if (!IMMyselfRelations.isMyFriend(mCustomUserID)) {
				nextStep();
				return;
			}

			IMCmdTeamDelMemberFromWb cmd = new IMCmdTeamDelMemberFromWb();

			cmd.mType = WBType.Friends;
			cmd.mUID = mUID;

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					nextStep();
				}
			};

			cmd.send();
		}
			break;
		case 3: {
			IMActionSendUserMsg action = new IMActionSendUserMsg();

			action.mOnActionFailedListener = new OnActionFailedListener() {
				@Override
				public void onActionFailed(String error) {
					done(error);
				}
			};

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					nextStep();
				}
			};

			IMUserMsg userMsg = new IMUserMsg();

			userMsg.mUserMsgType = UserMsgType.IMSDKBlacklist;
			userMsg.mToCustomUserID = mCustomUserID;
			userMsg.mContent = "";
			userMsg.mFromCustomUserID = IMMyself.getCustomUserID();
			userMsg.mClientSendTime = mBeginTime;

			action.mUserMsg = userMsg;
			action.mTimeoutInterval = mTimeoutInterval;
			action.begin();
		}
			break;
		case 4: {
			IMCmdTeamAddMember2WB cmd = new IMCmdTeamAddMember2WB();

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					nextStep();
				}
			};

			cmd.mType = WBType.BlackList;
			cmd.mUID = mUID;
			cmd.send();
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {
	}

	@Override
	public void onActionFailed(String error) {
	}
}
