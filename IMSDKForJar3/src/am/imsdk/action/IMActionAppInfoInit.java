package am.imsdk.action;

import imsdk.data.IMMyself;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.imsdk.aacmd.user.IMCmdUserGetAppInfo;
import am.imsdk.aacmd.user.IMCmdUserGetCustomerServiceInfo;
import am.imsdk.aacmd.user.IMCmdUserGetCustomerServiceList;
import am.imsdk.model.IMAppSettings;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.kefu.IMCustomerServiceMgr;
import am.imsdk.model.kefu.IMPrivateCSInfo;

// 1、获取appinfo
// 2、拉取客服列表
// 3、拉取客服信息
public final class IMActionAppInfoInit extends IMAction {
	private ArrayList<IMCmdUserGetCustomerServiceInfo> mCmdsList = new ArrayList<IMCmdUserGetCustomerServiceInfo>();
	private ArrayList<Long> mNeedUpdateUIDsList = new ArrayList<Long>();
	private long mNewCustomerServiceVersion;

	public IMActionAppInfoInit() {
		mStepCount = 3;
	}

	@Override
	public void onActionBegan() {
		if (IMMyself.getAppKey().length() == 0) {
			doneWithIMSDKError();
			return;
		}

		if (!IMPrivateMyself.getInstance().isLogined()) {
			doneWithNetworkError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			IMCmdUserGetAppInfo cmd = new IMCmdUserGetAppInfo();

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					done(errorJsonObject != null ? errorJsonObject.toString() : "");
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					if (!jsonObject.has("csver")) {
						done();
						return;
					}

					long version = jsonObject.getLong("csver");

					if (version == 0
							|| IMAppSettings.getInstance().mCustomerServiceVersion == version) {
						done();
						return;
					}

					mNewCustomerServiceVersion = version;
					nextStep();
				}
			};

			cmd.send();
		}
			break;
		case 2: {
			IMCmdUserGetCustomerServiceList cmd = new IMCmdUserGetCustomerServiceList();

			cmd.mCustomerServiceVersion = IMAppSettings.getInstance().mCustomerServiceVersion;
			cmd.mAppKey = IMAppSettings.getInstance().mAppKey;

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					done(errorJsonObject != null ? errorJsonObject.toString() : "");
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					if (!jsonObject.has("csidverlist")) {
						done();
						return;
					}

					JSONArray jsonArray = jsonObject.getJSONArray("csidverlist");

					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject2 = jsonArray.getJSONObject(i);
						long uid = jsonObject2.getLong("csid");
						long version = jsonObject2.getLong("v");

						if (uid == 0) {
							DTLog.logError();
							return;
						}

						IMPrivateCSInfo info = IMCustomerServiceMgr.getInstance()
								.getCSInfo(uid);

						if (info == null || info.getVersion() != version) {
							mNeedUpdateUIDsList.add(uid);
						}
					}

					if (mNeedUpdateUIDsList.size() == 0) {
						done();
						return;
					}

					nextStep();
				}
			};

			cmd.send();
		}
			break;
		case 3: {
			if (mNeedUpdateUIDsList.size() <= 0) {
				doneWithIMSDKError();
				return;
			}

			for (int i = 0; i < (mNeedUpdateUIDsList.size() - 1) / 5 + 1; i++) {
				final IMCmdUserGetCustomerServiceInfo cmd = new IMCmdUserGetCustomerServiceInfo();

				for (int j = i * 5; j < i * 5 + 5 && j < mNeedUpdateUIDsList.size(); j++) {
					long uid = mNeedUpdateUIDsList.get(j);

					if (uid == 0) {
						doneWithIMSDKError();
						return;
					}

					cmd.addUID(uid);
				}

				mCmdsList.add(cmd);
				cmd.mAppKey = IMMyself.getAppKey();

				cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
					@Override
					public void onCommonFailed(FailedType type, long errorCode,
							JSONObject errorJsonObject) throws JSONException {
						done(errorJsonObject != null ? errorJsonObject.toString() : "");
					}
				};

				cmd.mOnRecvEndListener = new OnRecvEndListener() {
					@Override
					public void onRecvEnd(JSONObject jsonObject) throws JSONException {
						if (mCmdsList.contains(cmd)) {
							mCmdsList.remove(cmd);
						}

						if (mCmdsList.size() == 0) {
							done();
						}
					}
				};

				cmd.send();
			}
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActionDone() {
		if (mNewCustomerServiceVersion > 0) {
			IMAppSettings.getInstance().mCustomerServiceVersion = mNewCustomerServiceVersion;
			IMAppSettings.getInstance().saveFile();
		}
	}
}
