package am.imsdk.action;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.LoginStatus;

import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTRemoteMgr;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.kefu.IMCustomerServiceMgr;
import am.imsdk.model.userinfo.IMUsersMgr;
import android.util.Log;
import android.util.SparseArray;

// 注册或登录，用户行为
// 1. 发送登录指令 , 等待接收返回值，并设置属性
// 完成
public final class IMActionLogin extends IMAction {
	public long mUID;
	public String mCustomUserID;
	private String mPassword;
	public String mAppKey;
	private IMActionLoginType mType = IMActionLoginType.None;

	private JSONObject mCmdJsonObjectResult = null;

	private IMPrivateMyself mPrivateMyself;

	private IMActionLogin() {
		mStepCount = 1;
	}

	@Override
	public void onActionBegan() {
		if (mType == null) {
			doneWithIMSDKError();
			return;
		}

		if (mType == IMActionLoginType.None) {
			doneWithIMSDKError();
			return;
		}

		if (mPassword == null || mPassword.length() == 0) {
			doneWithIMSDKError();
			return;
		}

		if (IMCustomerServiceMgr.getInstance().getUID(mCustomUserID) != 0) {
			mPrivateMyself = IMPrivateMyself.getInstance();
			done("Client can't login customer service account");
			return;
		}

		if (mType == IMActionLoginType.OneKeyLogin) {
			if (!(mCustomUserID instanceof String)) {
				doneWithIMSDKError();
				return;
			}

			if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)
					&& mCustomUserID != null && mCustomUserID.length() != 0) {
				doneWithIMSDKError();
				return;
			}
		} else {
			if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)) {
				doneWithIMSDKError();
				return;
			}
		}

		if (mType == IMActionLoginType.OneKeyLogin) {
			if (!(mPassword instanceof String)) {
				doneWithIMSDKError();
				return;
			}

			if (!IMParamJudge.isPasswordLegal(mPassword) && mPassword != null
					&& mPassword.length() != 0) {
				doneWithIMSDKError();
				return;
			}

			if (mCustomUserID == null || mCustomUserID.length() == 0) {
				mUID = 0;
			}
		} else {
			if (!IMParamJudge.isPasswordLegal(mPassword)) {
				doneWithIMSDKError();
				return;
			}
		}

		if (!IMParamJudge.isAppKeyLegal(mAppKey)) {
			doneWithIMSDKError();
			return;
		}

		if (mType == IMActionLoginType.OneKeyLogin
				|| mType == IMActionLoginType.OneKeyRegister) {
			IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.Logining);
			IMPrivateMyself.getInstance().setCustomUserInfoInitedInFact(false);
			IMPrivateMyself.getInstance().setGroupInfoInitedInFact(false);
			IMPrivateMyself.getInstance().setRelationsInitedInFact(false);
//			IMPrivateMyself.getInstance().setAppInfoInitedInFact(false);
			IMPrivateMyself.getInstance().saveFile();
		} else {
			DTLog.logError();
		}

		DTNotificationCenter.getInstance().removeObservers("LoginReceiveSuccess");
		DTNotificationCenter.getInstance().removeObservers("LoginReceiveFailure");
		
		Log.e("Debug", "IMActionLogin--创建--"+ IMActionLogin.this);

		DTNotificationCenter.getInstance().addObserver("LoginReceiveSuccess",
				new Observer() {
					
					@Override
					public void update(Observable observable, Object data) {
						if (IMActionLogin.getInstance() != IMActionLogin.this || isOver()) {
							return;
						}
						
						String strJson = data.toString();
						
						try {
							JSONObject jsonObject = new JSONObject(strJson);
							mCmdJsonObjectResult = jsonObject;
							
							done();
						} catch (JSONException e) {
//							e.printStackTrace();
							done("Login Err: 数据错误");
						}
					}
				});
		
		DTNotificationCenter.getInstance().addObserver("LoginReceiveFailure",
				new Observer() {
					
					@Override
					public void update(Observable observable, Object data) {
						if (IMActionLogin.getInstance() != IMActionLogin.this || isOver()) {
							return;
						}
						
						done("loginErr:" + data.toString());
					}
				});
		
		mPrivateMyself = IMPrivateMyself.getInstance();
	}

	@Override
	public void onActionStepBegan(final int stepNumber) {
		Log.e("Debug", "onActionStepBegan:" + stepNumber + "---"
				+ IMActionLogin.this);

		Log.e("Debug", mType + "");

		switch (mType) {
		case OneKeyLogin: {
			if (mCustomUserID == null) {
				mCustomUserID = "";
			}

			if (mPassword == null) {
				mPassword = "";
			}

			// 登录
			DTRemoteMgr.getInstance().login(mType.getValue(), mCustomUserID,
					mPassword);
		}
			break;
		case OneKeyRegister: {
			if (mCustomUserID == null) {
				mCustomUserID = "";
			}

			if (mPassword == null) {
				mPassword = "";
			}

			// 注册
			// registerIMSDK();
		}
		break;
		default:
			DTLog.logError();
			break;
		}
	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {
		if (mCustomUserID != null && !mCustomUserID.equals(IMMyself.getCustomUserID())) {
			return;
		}

		if (IMActionLogin.getInstance() != this) {
			return;
		}

		if (mPrivateMyself != IMPrivateMyself.getInstance()) {
			return;
		}

		sSingleton = null;

		if (mCmdJsonObjectResult == null) {
			doneWithIMSDKError();
			return;
		}

		long uid = 0;
		long sid = 0;
		boolean autoCreateCustomUserID = false;

		try {
			uid = mCmdJsonObjectResult.getLong("uid");
			sid = mCmdJsonObjectResult.getLong("sid");

			if (uid == 0) {
				doneWithServerError();
				return;
			}

			if (sid == 0) {
				doneWithServerError();
				return;
			}

			IMUsersMgr.getInstance().setCustomUserIDForUID(mCustomUserID, uid);
			IMUsersMgr.getInstance().saveFile();
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			DTLog.log(e.toString());
			this.done("IMSDK Server error");
			return;
		}
		
		if (IMPrivateMyself.getInstance().getPassword().length() == 0) {
			IMPrivateMyself.getInstance().setPassword(mPassword);
		}

		if (mUID == 0) {
			if (uid == 0) {
				DTLog.logError();
				return;
			}

			IMUsersMgr.getInstance().setCustomUserIDForUID(mCustomUserID, uid);
			IMUsersMgr.getInstance().saveFile();

			IMPrivateMyself.getInstance().reinit();

			IMPrivateMyself.getInstance().setPassword(mPassword);
			IMPrivateMyself.getInstance().saveFile();

			if (IMPrivateMyself.getInstance().getUID() == 0) {
				DTLog.logError();
				return;
			}
		}

		// save login history
		// unfinished by lyc
//		IMPrivateMyself.getInstance().setSessionID(sid);
		IMPrivateMyself.getInstance().setLoginStatus(IMMyself.LoginStatus.Logined);

		// init modules
		if (mType == IMActionLoginType.OneKeyLogin) {
			IMPrivateMyself.getInstance().onUserLoginInit();
		} else if (mType == IMActionLoginType.OneKeyRegister) {
			IMPrivateMyself.getInstance().onUserRegisterInit();
		} else {
			IMPrivateMyself.getInstance().onMachineLoginInit();
		}

		if (!mCustomUserID.equals(IMPrivateMyself.getInstance().getCustomUserID())) {
			DTLog.logError();
			return;
		}

		DTNotificationCenter.getInstance().postNotification("IMActionLogin_done");
	}

	@Override
	public void onActionFailed(String error) {
		if (IMActionLogin.getInstance() != this) {
			return;
		}

		sSingleton = null;

		switch (mType) {
		case OneKeyLogin:
		case OneKeyRegister:
		// 用户行为
		{
			DTNotificationCenter.getInstance().postNotification(
					"IMActionUserLogin_failed", error);
		}
			break;
		default:
			break;
		}
	}

	public void setType(IMActionLoginType type) {
		mType = type;
		DTLog.sign("IMActionLoginType:" + type.toString());
	}

	public IMActionLoginType getType() {
		return mType;
	}

	public void setPassword(String password) {
		if (password == null || password.length() == 0) {
			DTLog.logError();
			return;
		}

		mPassword = password;
	}

	public enum IMActionLoginType {
		None(0),

		// 用户行为
		// PureLogin(1), PureRegister(2),

		// 用户行为
		OneKeyLogin(3), OneKeyRegister(4);

		// 非用户行为
//		Reconnect(10), AutoLogin(11);

		private final int value;

		private IMActionLoginType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		private static final SparseArray<IMActionLoginType> sMapValues = new SparseArray<IMActionLoginType>();

		static {
			for (IMActionLoginType type : IMActionLoginType.values()) {
				sMapValues.put(type.value, type);
			}
		}

		public static IMActionLoginType fromInt(int i) {
			IMActionLoginType type = sMapValues.get(Integer.valueOf(i));

			if (type == null) {
				DTLog.logError();
				return null;
			}

			return type;
		}
	}

	// singleton
	private volatile static IMActionLogin sSingleton;

	public static IMActionLogin getInstance() {
		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
//		printCallStatck();
		synchronized (IMActionLogin.class) {
			if (sSingleton != null) {
				DTNotificationCenter.getInstance().removeObservers("LoginReceiveSuccess");
				DTNotificationCenter.getInstance().removeObservers("LoginReceiveFailure");
			}

			sSingleton = new IMActionLogin();
		}
	}
	
	public static void printCallStatck() {
        Throwable ex = new Throwable();
        StackTraceElement[] stackElements = ex.getStackTrace();
        if (stackElements != null) {
            for (int i = 0; i < stackElements.length; i++) {
                System.out.print(stackElements[i].getClassName()+"--");
                System.out.print(stackElements[i].getFileName()+"--");
                System.out.print(stackElements[i].getLineNumber()+"--");
                System.out.println(stackElements[i].getMethodName());
            }
        }
        System.out.println("**********************************");
    }
}
