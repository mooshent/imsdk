package am.imsdk.action.around;

import imsdk.data.around.IMMyselfAround;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.imsdk.aacmd.around.IMCmdAroundGet;
import am.imsdk.aacmd.user.IMCmdUserGetInfo;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMPrivateAroundUsers;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.userinfo.IMPrivateUserInfo;
import am.imsdk.model.userinfo.IMUsersMgr;

// 1. 获取周边用户
// 2. 获取cid
public final class IMActionAroundNext extends IMAction {
	public int mPageIndex;
	public ArrayList<Long> mAryUIDs = new ArrayList<Long>();
	private int mStep3TryTimes;
	
	public IMActionAroundNext() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (mPageIndex == 0) {
			doneWithIMSDKError();
			return;
		}

		IMPrivateAroundUsers.getInstance().mState = IMMyselfAround.State.Paging;
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			IMCmdAroundGet cmd = new IMCmdAroundGet();

			cmd.mLongitude = IMPrivateAroundUsers.getInstance().mLongitude;
			cmd.mLatitude = IMPrivateAroundUsers.getInstance().mLatitude;
			cmd.mPage = 0;

			cmd.mOnCommonFailedListener = new DTCmd.OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new DTCmd.OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					parseCmdResult(jsonObject);
					nextStep();
				}
			};

			cmd.send();
		}
			break;
		case 3: {
			mStep3TryTimes++;

			if (mStep3TryTimes > 3) {
				doneWithIMSDKError();
				return;
			}

			IMCmdUserGetInfo cmd = new IMCmdUserGetInfo();

			cmd.addProperty("phonenum");

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) {
					if (isOver()) {
						return;
					}

					beginStep(3);
				}
			};

			boolean needSend = false;

			for (long uid : mAryUIDs) {
				String customUserID = IMUsersMgr.getInstance().getCustomUserID(uid);

				if (customUserID.length() == 0) {
					cmd.addUID(uid);
					needSend = true;
				}
			}

			if (needSend) {
				cmd.send();
			} else {
				done();
			}
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {
		if (IMPrivateAroundUsers.getInstance().mState == IMMyselfAround.State.Paging) {
			IMPrivateAroundUsers.getInstance().add(mAryUIDs);
			
			if (mAryUIDs.size() < 28) {
				IMPrivateAroundUsers.getInstance().mState = IMMyselfAround.State.Normal;
			} else {
				IMPrivateAroundUsers.getInstance().mState = IMMyselfAround.State.ReachEnd;
			}
			
			IMPrivateAroundUsers.getInstance().saveFile();
		}
	}

	@Override
	public void onActionFailed(String error) {
		if (IMPrivateAroundUsers.getInstance().mState == IMMyselfAround.State.Paging) {
			IMPrivateAroundUsers.getInstance().mState = IMMyselfAround.State.Normal;
		}
	}

	private void parseCmdResult(JSONObject jsonObject) throws JSONException {
		JSONArray jsonArray = jsonObject.getJSONArray("userlocations");

		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject userLocationJsonObject = (JSONObject) jsonArray.get(i);

			long uid = userLocationJsonObject.getLong("uid");

			if (uid == 0) {
				doneWithServerError();
				return;
			}

			IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(uid);

			userInfo.mLongitude = userLocationJsonObject.getDouble("xpos");
			userInfo.mLatitude = userLocationJsonObject.getDouble("ypos");
			userInfo.saveFile();
			
//			userInfo.mUserLocation.mLongitude = userLocationJsonObject
//					.getDouble("xpos");
//			userInfo.mUserLocation.mLatitude = userLocationJsonObject.getDouble("ypos");
//
//			userInfo.saveFile();

			if (uid == IMPrivateMyself.getInstance().getUID()) {
				mAryUIDs.add(0, uid);
			} else {
				mAryUIDs.add(uid);
			}
		}
	}
}
