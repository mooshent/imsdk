package am.imsdk.action.fileserver;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONException;
import org.json.JSONObject;

import remote.service.data.IMRemoteMyself;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;
import am.dtlib.model.c.tool.ProcessTool;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import android.os.AsyncTask;

// 1. 获取url
// 2. 获取文件
public class IMActionDownloadFile extends IMActionFile {
	public String mFileID = "";
	public int mRealFileLength;
	public byte[] mBuffer;
	
	protected String mRealURL = "";
	protected long mUID;
	protected static final int MAX_FILE_LENGTH = 1024 * 1024;

	public IMActionDownloadFile() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (!IMParamJudge.isFileIDLegal(mFileID)) {
			doneWithIMSDKError();
			return;
		}
		
		if(ProcessTool.isRemoteProcess(DTAppEnv.getContext())) {
			mUID = IMRemoteMyself.getInstance().getUID();
		} else {
			mUID = IMPrivateMyself.getInstance().getUID();
		}
		
		if (mUID == 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			FetchURLTask task = new FetchURLTask();
			
			task.mUID = mUID;
			task.execute(mFileID);
		}
			break;
		case 2: {
			DTLog.sign("mRealURL:" + mRealURL);
			
			if (mRealURL == null) {
				doneWithIMSDKError();
				return;
			}
			
			if (mRealURL.length() == 0) {
				doneWithIMSDKError();
				return;
			}
			
			DownFileTask task = new DownFileTask();
			
			task.execute(mRealURL);
		}
			break;
		default:
			break;
		}
	}
	
	private class FetchURLTask extends AsyncTask<String, Integer, String> {
		private long mUID = 0;
		
		@Override
		protected String doInBackground(String... params) {
			try {
				return fetchURL(params[0]);
			} catch (IOException e) {
				e.printStackTrace();
				DTLog.logError();
				return "";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			if (result == null) {
				result = "";
			}

			if (result.length() == 0) {
				doneWithServerError();
				return;
			}

			JSONObject jsonObject;
			
			try {
				jsonObject = new JSONObject(result);
			} catch (JSONException e) {
				e.printStackTrace();
				doneWithIMSDKError();
				return;
			}
			
			long errorCode = 0;
			
			try {
				errorCode = jsonObject.getLong("errcode");
			} catch (JSONException e) {
			}
			
			if (errorCode > 0) {
				done("Illegal FileID");
				return;
			}
			
			try {
				mRealURL = jsonObject.getString("url");
			} catch (JSONException e) {
				e.printStackTrace();
				doneWithIMSDKError();
				return;
			}
			
			if (mRealURL.length() == 0) {
				doneWithIMSDKError();
				return;
			}
			
			nextStep();
		}

		private HttpPost initHttpPost(String fileID, boolean ipAddress) throws UnsupportedEncodingException {
			String md5 = DTTool.getMD5String(fileID + mUID);
			String httpAddress = getHttpAddress1(ipAddress);
			HttpPost httpPost = new HttpPost(httpAddress);
			ArrayList<BasicNameValuePair> formParams = new ArrayList<BasicNameValuePair>();

			formParams.add(new BasicNameValuePair("fid", fileID));
			formParams.add(new BasicNameValuePair("fromuid", mUID + ""));
			formParams.add(new BasicNameValuePair("vercode", md5));

			HttpEntity entity = new UrlEncodedFormEntity(formParams, "UTF-8");

			httpPost.setEntity(entity);

			return httpPost;
		}

		private String fetchURL(String fileID) throws IOException {
			if (mUID == 0) {
				DTLog.logError();
				return "";
			}
			
			if (fileID == null) {
				DTLog.logError();
				return "";
			}
			
			if (fileID.length() == 0) {
				DTLog.logError();
				return "";
			}
			
			HttpClient httpClient = new DefaultHttpClient();

			httpClient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
					HttpVersion.HTTP_1_1);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.CONNECTION_TIMEOUT, 5000);

			HttpPost httpPost = initHttpPost(fileID, false);
			HttpResponse response = null;
			boolean needPost = false;

			try {
				response = httpClient.execute(httpPost);
			} catch (UnknownHostException e) {
				needPost = true;
			}

			if (needPost) {
				httpPost = initHttpPost(fileID, true);

				try {
					response = httpClient.execute(httpPost);
				} catch (Exception e) {
					DTLog.logError();
					return "";
				}
			}
			
			if (response == null) {
				DTLog.logError();
				return "";
			}
			
			mLengthFinished = httpPost.getEntity().getContentLength();
			mLengthOffset += mLengthFinished - 121;
			done(100.0 * mLengthFinished / getTotalLengthExpected());
			
			InputStream inputStream = response.getEntity().getContent();
			byte[] bytes = new byte[1024];
			int recvLength = 0;
			int readResult = 0;
			final long responseContentLength = response.getEntity().getContentLength();

			DTLog.sign("downstep2:" + responseContentLength);
			
			while (readResult != -1 && recvLength < 1024) {
				readResult = inputStream.read(bytes, recvLength, 1024 - recvLength);

				if (readResult != -1) {
					recvLength += readResult;
				}
			}
			
			if (recvLength == 1024) {
				DTLog.logError();
				return "";
			}
			
			if (responseContentLength != recvLength) {
				DTLog.logError();
				return "";
			}

			mLengthFinished += responseContentLength;
			mLengthOffset += responseContentLength - 188;
			done(100.0 * mLengthFinished / getTotalLengthExpected());
			return new String(bytes, 0, recvLength, "UTF8");
		}
	}
	
	private class DownFileTask extends AsyncTask<String, Integer, byte[]> {
		@Override
		protected byte[] doInBackground(String... params) {
			try {
				return downFile(params[0]);
			} catch (IOException e) {
				e.printStackTrace();
				DTLog.logError();
				return null;
			}
		}

		@Override
		protected void onPostExecute(byte[] result) {
			super.onPostExecute(result);
			mBuffer = result;
			nextStep();
		}

		private byte[] downFile(String realURL) throws IOException {
			if (realURL == null) {
				DTLog.logError();
				return null;
			}
			
			if (realURL.length() == 0) {
				DTLog.logError();
				return null;
			}
			
			HttpClient httpClient = new DefaultHttpClient();

			httpClient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION,
					HttpVersion.HTTP_1_1);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.CONNECTION_TIMEOUT, 5000);
			
			HttpGet httpGet = new HttpGet(realURL);
			
			HttpResponse response = httpClient.execute(httpGet);
			
			mLengthFinished += realURL.length();
			mLengthOffset += realURL.length() - 166;
			done(100.0 * mLengthFinished / getTotalLengthExpected());
			
			if (response == null) {
				DTLog.logError();
				return null;
			}

			InputStream inputStream = response.getEntity().getContent();
			int recvLength = 0;
			int readResult = 0;
			
			mRealFileLength = (int)response.getEntity().getContentLength();

			DTLog.sign("mRealFileLength:" + mRealFileLength);
			
			if (mRealFileLength > MAX_FILE_LENGTH) {
				DTLog.logError();
				doneWithIMSDKError();
				return null;
			}

			byte[] bytes = new byte[mRealFileLength];

			while (readResult != -1 && recvLength <= mRealFileLength) {
				readResult = inputStream.read(bytes, recvLength, mRealFileLength - recvLength);

				if (readResult != -1) {
					recvLength += readResult;
					mLengthFinished += readResult;
					done(100.0 * mLengthFinished / getTotalLengthExpected());
				}
			}
			
			if (mRealFileLength != recvLength) {
				DTLog.logError();
				return null;
			}

			return bytes;
		}
	}

	@Override
	public long getTotalLengthExpected() {
		if (mRealFileLength == 0) {
			return MAX_FILE_LENGTH;
		} else {
			return 121 + 188 + 166 + mRealFileLength + mLengthOffset;
		}
	}
	
	protected String getHttpAddress1(boolean ipAddress) {
		ipAddress = true;
		
		String httpAddress = "http://" + (ipAddress ? IM_FILE_URL_REAL_IP
				: IM_FILE_URL_DOMAIN_NAME) + ":8889/file/download";
		
		return httpAddress;
	}
}
