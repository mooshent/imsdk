package am.imsdk.action.ammsgs;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import remote.service.data.IMRemoteMyself;

import am.imsdk.action.IMAction;
import am.imsdk.model.IMPrivateMyself;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

public class IMActionRecvOffLineMsg extends IMAction {
	private String jsonString;
	private long mUid;
	
	public IMActionRecvOffLineMsg() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		mUid = IMRemoteMyself.getInstance().getUID();
		
		if(mUid <= 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			new downloadOfflineMsg().execute();
			break;
		}
		case 2: {
			if(TextUtils.isEmpty(jsonString)) {
				doneWithIMSDKError();
				return;
			}
			break;
			
			
		}
		default:
			break;
		}
	}
	
	private class downloadOfflineMsg extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("Uid", IMPrivateMyself.getInstance().getUID());
				jsonObject.put("Count", 100);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			
			String uriAPI = "http://192.168.20.51:3001/rest/offline_msg/list";// Post方式没有参数在这里
			String result = "";
			HttpPost httpRequst = new HttpPost(uriAPI);// 创建HttpPost对象
			httpRequst.addHeader(HTTP.CONTENT_TYPE, "application/json");

			try {
				StringEntity sEntity = new StringEntity(jsonObject.toString());
				sEntity.setContentType("text/json");
				sEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
				httpRequst.setEntity(sEntity);
				HttpResponse httpResponse = new DefaultHttpClient().execute(httpRequst);
				if (httpResponse.getStatusLine().getStatusCode() == 200) {
					HttpEntity httpEntity = httpResponse.getEntity();
					result = EntityUtils.toString(httpEntity);// 取出应答字符串
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				result = e.getMessage().toString();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
				result = e.getMessage().toString();
			} catch (IOException e) {
				e.printStackTrace();
				result = e.getMessage().toString();
			}

			Log.e("Debug", result);
			
			return result;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			
			jsonString = result;
			IMActionRecvOffLineMsg.this.nextStep();
		}
		
	}
}
