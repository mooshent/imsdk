package am.imsdk.model;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.LoginStatus;
import imsdk.data.mainphoto.IMSDKMainPhoto.OnBitmapRequestProgressListener;

import java.util.ArrayList;

import org.json.JSONException;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedListener;
import am.imsdk.action.IMActionLogin;
import am.imsdk.action.IMActionUserRequestCustomUserInfo;
import am.imsdk.action.IMActionsMgr;
import am.imsdk.action.relation.IMActionRelationInit;
import am.imsdk.action.team.IMActionTeamInit;
import am.imsdk.model.a2.IMSDKMainPhoto2;
import am.imsdk.model.amim.IMUserMsgsMgr;
import am.imsdk.model.amimteam.IMTeamMsgsMgr;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import am.imsdk.model.imgroup.IMPrivateRecentGroups;
import am.imsdk.model.imteam.IMTeamMsgHistoriesMgr;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo.TeamType;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import am.imsdk.model.userinfo.IMPrivateUserInfo;
import am.imsdk.model.userinfo.IMUsersMgr;
import am.imsdk.model.userslist.IMPrivateBlacklistMgr;
import am.imsdk.model.userslist.IMPrivateFriendsMgr;
import android.graphics.Bitmap;
import android.util.Log;

public final class IMPrivateMyself extends IMPrivateUserInfo {
	private IMPrivateMyself() {
		super();

		mLevel = 2;
		this.addIgnoreField("mLoginStatus");
		this.addIgnoreField("mSessionID");
		this.addIgnoreField("mCustomUserInfoInited");
		this.addIgnoreField("mGroupInited");
		this.addIgnoreField("mRelationInited");
		this.addIgnoreField("mAllInited");
		this.addIgnoreField("mCheckInitRunnable");
		this.addIgnoreField("mPassword");

		addRenameField("mGroupIDsList", "mGroupIDsList");

		if (IMMyself.getCustomUserID() != null
				&& IMMyself.getCustomUserID().length() > 0) {
			mUID = IMUsersMgr.getInstance().getUID(IMMyself.getCustomUserID());

			if (mUID > 0) {
				long uid = mUID;

				this.readFromFile();

				if (mUID != uid) {
					DTLog.logError();
				}
			}
		}

//		printCallStatck();
		Log.e("Debug", "IMPrivateMyself construction");
//		heartbeat();
//		checkAllInited();

//		DTNotificationCenter.getInstance().addObserver("heartbeat", new Observer() {
//			@Override
//			public void update(Observable observable, Object data) {
//				Log.e("Debug", "IMPrivateMyself Observer");
//				heartbeat();
//			}
//		});
	}
	
	 public static void printCallStatck() {
        Throwable ex = new Throwable();
        StackTraceElement[] stackElements = ex.getStackTrace();
        if (stackElements != null) {
            for (int i = 0; i < stackElements.length; i++) {
                System.out.print(stackElements[i].getClassName()+"/t");
                System.out.print(stackElements[i].getFileName()+"/t");
                System.out.print(stackElements[i].getLineNumber()+"/t");
                System.out.println(stackElements[i].getMethodName());
            }
            System.out.println("-----------------------------------");
        }
	}
	 
	public String getNickName() {
		if (mNickName == null) {
			mNickName = "";
		}
		
		return mNickName;
	}

	public void setNickName(String mNickName) {
		this.mNickName = mNickName;
	}
	
	public void setPassword(String password) {
		mPassword = password;
	}

	public String getPassword() {
		if (mPassword == null) {
			mPassword = "";
		}

		return mPassword;
	}

	public void setAutoLogin(boolean autoLogin) {
		mAutoLogin = autoLogin;
	}

	public boolean getAutoLogin() {
		return mAutoLogin;
	}
	
//	public long getSessionID() {
//		return mSessionID;
//	}
//
//	public void setSessionID(long mSessionID) {
//		this.mSessionID = mSessionID;
//	}

	private String mNickName;
	private String mPassword = "";
	private ArrayList<Long> mAryTeamIDs = new ArrayList<Long>();
	private ArrayList<Long> mGroupIDsList = new ArrayList<Long>();

//	private long mSessionID;
	private LoginStatus mLoginStatus = LoginStatus.None;
	private boolean mAutoLogin = true;
	private boolean mCustomUserInfoInited;
	private boolean mGroupInfoInited;
	private boolean mRelationsInited;
//	private boolean mOfflineInited;
//	private boolean mAppInfoInited;
//	private boolean mAppInfoInitedInFact;
	private boolean mCustomUserInfoInitedInFact;
	private boolean mGroupInfoInitedInFact = false;
	private boolean mRelationsInitedInFact;
//	private boolean mOfflineInitedInFact;
	private boolean mMainPhotoInitedInFact;
	private boolean mCustomerServiceInitedInFact;
	
	//错误后尝试次数
	private int mCustomUserInfoRetryTime;
	private int mGroupInfoRetryTime;
	private int mRelationsRetryTime;
	private int mAppInfoRetryTime;

	public boolean getCustomUserInfoInitedInFact() {
		return mCustomUserInfoInitedInFact;
	}

	public boolean getCustomUserInfoInited() {
		return mCustomUserInfoInited
				&& IMPrivateMyself.getInstance().getLoginStatus() == LoginStatus.Logined;
	}

//	public boolean getAppInfoInited() {
//		return mAppInfoInited
//				&& IMPrivateMyself.getInstance().getLoginStatus() == LoginStatus.Logined;
//	}
//
//	public boolean getAppInfoInitedInFact() {
//		return mAppInfoInitedInFact
//				&& IMPrivateMyself.getInstance().getLoginStatus() == LoginStatus.Logined;
//	}

//	public void setAppInfoInited(boolean appInfoInited) {
//		mAppInfoInited = appInfoInited;
//	}
//
//	public void setAppInfoInitedInFact(boolean appInfoInitedInFact) {
//		mAppInfoInitedInFact = appInfoInitedInFact;
//	}

	public boolean getRelationsInited() {
		return mRelationsInited
				&& IMPrivateMyself.getInstance().getLoginStatus() == LoginStatus.Logined;
	}

	public void setCustomUserInfoInited(boolean customUserInfoInited) {
		mCustomUserInfoInited = customUserInfoInited;
	}

	public void setCustomUserInfoInitedInFact(boolean customUserInfoInitedInFact) {
		if (mCustomUserInfoInitedInFact && customUserInfoInitedInFact) {
			return;
		}

		if (!mCustomUserInfoInitedInFact && !customUserInfoInitedInFact) {
			return;
		}

		mCustomUserInfoInitedInFact = customUserInfoInitedInFact;

		if (mCustomUserInfoInitedInFact) {
			DTNotificationCenter.getInstance().postNotification(
					"CustomUserInfoInitialized");
		}
	}

	public void setRelationsInited(boolean relationsInited) {
		mRelationsInited = relationsInited;
	}

	public void setRelationsInitedInFact(boolean relationsInitedInFact) {
		mRelationsInitedInFact = relationsInitedInFact;
	}

	public void setCustomerServiceInitedInFact(boolean customerServiceInitedInFact) {
		mCustomerServiceInitedInFact = customerServiceInitedInFact;
	}

	public boolean getCustomerServiceInitedInFact() {
		return mCustomerServiceInitedInFact;
	}

	public boolean couldSendHeartbeat() {
//		return mGroupInfoInited && mRelationsInited;
		return true;
	}

//	public boolean getAllInited() {
//		return mCustomUserInfoInited && mGroupInfoInited && mRelationsInited
//				&& mAppInfoInited;
//	}

	public void reinit() {
		DTLog.sign("reinit");

		if (IMMyself.getCustomUserID().length() == 0) {
			return;
		}

		IMPrivateMyself.newInstance();

		// login action
		IMActionLogin.newInstance();

		// Settings
		// [IMPrivatePersonalSettings newInstance];
		// [IMLocalPersonalSettings newInstance];

		// Around
		IMPrivateAroundUsers.newInstance();

		// Relations
		IMPrivateFriendsMgr.newInstance();
		IMPrivateBlacklistMgr.newInstance();

		// IM
		IMUserMsgsMgr.newInstance();
		IMPrivateRecentContacts.newInstance();
		IMPrivateRecentGroups.newInstance();
		IMTeamMsgsMgr.newInstance();

		// IM History
		// [IMSystemMsgHistory newInstance];
		IMUserMsgHistoriesMgr.newInstance();
		IMTeamMsgHistoriesMgr.newInstance();

		IMActionsMgr.getInstance().setUID(mUID);
	}

	private void initCustomUserInfo() {
		if (mCustomUserInfoInitedInFact) {
			DTLog.logError();
			return;
		}

		IMActionUserRequestCustomUserInfo action = new IMActionUserRequestCustomUserInfo();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if(mCustomUserInfoRetryTime++ >= 3) {
					mCustomUserInfoInitedInFact = true;
					return;
				}
				
				mCustomUserInfoInited = true;
				
				Log.e("Debug", "IMActionUserRequestCustomUserInfo failed:" + mCustomUserInfoRetryTime);
//				checkAllInited();
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				mCustomUserInfoInited = true;

				if (!mCustomUserInfoInitedInFact) {
					mCustomUserInfoInitedInFact = true;
					IMPrivateMyself.this.saveFile();

					DTNotificationCenter.getInstance().postNotification(
							"CustomUserInfoInitialized");
				}

				Log.e("Debug", "IMActionUserRequestCustomUserInfo done");
//				checkAllInited();
			}
		};

		action.mCustomUserID = getCustomUserID();
		action.mTimeoutInterval = 20;
		action.begin();
	}

	private void initGroup() {
		if (mGroupInfoInitedInFact) {
			DTLog.logError();
			return;
		}

		final IMActionTeamInit action = new IMActionTeamInit();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if(mGroupInfoRetryTime++ >= 3) {
					mGroupInfoInitedInFact = true;
					return;
				}
				
				mGroupInfoInited = true;
				
				Log.e("Debug", "IMActionTeamInit failed:" + mGroupInfoRetryTime);
//				checkAllInited();
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				mGroupInfoInited = true;

				if (!mGroupInfoInitedInFact) {
					mGroupInfoInitedInFact = true;
					IMPrivateMyself.this.saveFile();

					DTNotificationCenter.getInstance().postNotification(
							"GroupInitialized");
				}

				Log.e("Debug", "IMActionTeamInit done");
//				checkAllInited();
			}
		};

		action.mTimeoutInterval = 20;
		action.begin();
	}

	private void initRelations() {
		if (mRelationsInitedInFact) {
			DTLog.logError();
			return;
		}

		IMActionRelationInit action = new IMActionRelationInit();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if(mRelationsRetryTime++ >= 3) {
					mRelationsInitedInFact = true;
					return;
				}
				
				mRelationsInited = true;
				
				Log.e("Debug", "IMActionRelationInit failed:" + mRelationsRetryTime);
//				checkAllInited();
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				mRelationsInited = true;

				if (!mRelationsInitedInFact) {
					mRelationsInitedInFact = true;
					IMPrivateMyself.this.saveFile();
					DTNotificationCenter.getInstance().postNotification(
							"RelationsInitialzed");
				}

				Log.e("Debug", "IMActionRelationInit done");
//				checkAllInited();
			}
		};

		action.mTimeoutInterval = 20;
		action.begin();
	}
	
	

	public void onUserLoginInit() {
		mCustomUserInfoInited = false;
		mGroupInfoInited = false;
		mRelationsInited = false;
//		mOfflineInited = false;
//		mAppInfoInited = false;
		mCustomUserInfoInitedInFact = false;
		mGroupInfoInitedInFact = false;
		mRelationsInitedInFact = false;
//		mOfflineInitedInFact = false;
//		mAppInfoInitedInFact = false;
		mMainPhotoInitedInFact = false;
		
		mCustomUserInfoRetryTime = 1;
		mGroupInfoRetryTime = 1;
		mRelationsRetryTime = 1;
		mAppInfoRetryTime = 1;
		
		IMPrivateMyself.this.saveFile();
		
		checkAllInited();
	}

	public void onUserRegisterInit() {
		mCustomUserInfoInited = true;
		mGroupInfoInited = true;
		mRelationsInited = true;
//		mAppInfoInited = true;
		mCustomUserInfoInitedInFact = true;
		mGroupInfoInitedInFact = false;
		mRelationsInitedInFact = true;
//		mAppInfoInitedInFact = false;
		mMainPhotoInitedInFact = true;
		
		mCustomUserInfoRetryTime = 1;
		mGroupInfoRetryTime = 1;
		mRelationsRetryTime = 1;
		mAppInfoRetryTime = 1;
		
		IMPrivateMyself.this.saveFile();
		checkAllInited();
	}

	public void onMachineLoginInit() {
		mCustomUserInfoInited = true;
		mGroupInfoInited = true;
		mRelationsInited = true;
//		mOfflineInited = true;
//		mAppInfoInited = true;
		checkInitAllInFact();
		checkAllInited();
	}

	private void checkInitAllInFact() {
		Log.e("Debug", "checkInitAllInFact -- mCustomUserInfoInitedInFact:" + mCustomUserInfoInitedInFact);
		if (!mCustomUserInfoInitedInFact) {
			initCustomUserInfo();
		}

		for (int i = mGroupIDsList.size() - 1; i >= 0; i--) {
			long teamID = mGroupIDsList.get(i);

			if (!IMDataJudge.isMyGroupLegal(teamID)) {
				mGroupInfoInitedInFact = false;
				break;
			}
		}

		Log.e("Debug", "checkInitAllInFact -- mGroupInfoInitedInFact:" + mGroupInfoInitedInFact);
		if (!mGroupInfoInitedInFact) {
			initGroup();
		}

		Log.e("Debug", "checkInitAllInFact -- mRelationsInitedInFact:" + mRelationsInitedInFact);
		if (!mRelationsInitedInFact) {
			initRelations();
		}
		
//		Log.e("Debug", "checkInitAllInFact -- mOfflineInitedInFact:" + mOfflineInitedInFact);
//		if(!mOfflineInitedInFact) {
//			initOfflineMessage();
//		}

//		Log.e("Debug", "checkInitAllInFact -- mAppInfoInitedInFact:" + mAppInfoInitedInFact);
//		if (!mAppInfoInitedInFact) {
//			initAppInfo();
//		}

		Log.e("Debug", "checkInitAllInFact -- mMainPhotoInitedInFact:" + mMainPhotoInitedInFact);
		if (!mMainPhotoInitedInFact) {
			initMainPhoto();
		}
	}
	
	private Runnable mCheckInitRunnable = new Runnable() {
		
		@Override
		public void run() {
			checkAllInited();
		}
	};

	private void initMainPhoto() {
		if (mMainPhotoInitedInFact) {
			DTLog.logError();
			return;
		}

		if (!mCustomUserInfoInited) {
			return;
		}

		if (!isLogined()) {
			DTLog.logError();
			return;
		}

		if (IMPrivateMyself.getInstance().getMainPhotoFileID().length() == 0) {
			if (mMainPhotoInitedInFact) {
				return;
			}

			mMainPhotoInitedInFact = true;
			IMPrivateMyself.getInstance().saveFile();
			DTNotificationCenter.getInstance().postNotification(
					"MainPhotoModuleInitialized");
		}

		IMSDKMainPhoto2.request(IMPrivateMyself.getInstance().getCustomUserID(), 20,
				new OnBitmapRequestProgressListener() {
					@Override
					public void onSuccess(Bitmap mainPhoto, byte[] buffer) {
						if (mMainPhotoInitedInFact) {
							return;
						}

						mMainPhotoInitedInFact = true;
						IMPrivateMyself.getInstance().saveFile();
						DTNotificationCenter.getInstance().postNotification(
								"MainPhotoModuleInitialized");
					}

					@Override
					public void onProgress(double progress) {
					}

					@Override
					public void onFailure(String error) {
					}
				});
	}

//	private void initAppInfo() {
//		if (mAppInfoInitedInFact) {
//			DTLog.logError();
//			return;
//		}
//
//		if (IMMyself.getAppKey().length() == 0) {
//			DTLog.logError();
//			return;
//		}
//
//		IMActionAppInfoInit action = new IMActionAppInfoInit();
//
//		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
//			@Override
//			public void onActionFailedEnd(String error) {
//				if(mAppInfoRetryTime++ >= 3) {
//					return;
//				}
//				
//				mAppInfoInited = true;
//				
//				Log.e("Debug", "IMActionAppInfoInit failed:" + mAppInfoRetryTime);
//				checkAllInited();
//			}
//		};
//
//		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
//			@Override
//			public void onActionDoneEnd() {
//				mAppInfoInited = true;
//				mAppInfoInitedInFact = true;
//				IMPrivateMyself.getInstance().saveFile();
//				
//				Log.e("Debug", "IMActionAppInfoInit done");
//				checkAllInited();
//
//				DTNotificationCenter.getInstance().postNotification(
//						"CustomerServiceInited");
//			}
//		};
//
//		action.mTimeoutInterval = 20;
//		action.begin();
//
//		// IMCmdUserGetAppInfo cmd = new IMCmdUserGetAppInfo();
//		//
//		// cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
//		// @Override
//		// public void onCommonFailed(FailedType type, long errorCode,
//		// JSONObject errorJsonObject) throws JSONException {
//		// mAppInfoInited = true;
//		// checkAllInited();
//		// }
//		// };
//		//
//		// cmd.mOnRecvEndListener = new OnRecvEndListener() {
//		// @Override
//		// public void onRecvEnd(JSONObject jsonObject) throws JSONException {
//		// mAppInfoInited = true;
//		// mAppInfoInitedInFact = true;
//		// IMPrivateMyself.getInstance().saveFile();
//		// checkAllInited();
//		// }
//		// };
//		//
//		// cmd.send();
//	}

	public void checkAllInited() {
		DTAppEnv.cancelPreviousPerformRequest(mCheckInitRunnable);
		
		if (mCustomUserInfoInitedInFact && mGroupInfoInitedInFact
				&& mRelationsInitedInFact
//				&& mOfflineInitedInFact
//				&& mAppInfoInited
				) {
			saveFile();
			return;
		} else {
			DTAppEnv.performAfterDelayOnUIThread(60, mCheckInitRunnable);
		}
		
		checkInitAllInFact();

//		if (couldSendHeartbeat()) {
//			Log.e("Debug", "IMPrivateMyself checkAllInited");
//			heartbeat();
//		}
	}

	public boolean getGroupInfoInited() {
		return mGroupInfoInited && mLoginStatus == LoginStatus.Logined;
	}

	public boolean getGroupInfoInitedInFact() {
		return mGroupInfoInitedInFact && mLoginStatus == LoginStatus.Logined;
	}

	public void setGroupInfoInited(boolean groupInfoInited) {
		mGroupInfoInited = groupInfoInited;
	}

	public void setGroupInfoInitedInFact(boolean groupInfoInitedInFact) {
		mGroupInfoInitedInFact = groupInfoInitedInFact;
	}

	public void setMainPhotoFileID(String fileID) {
		if (fileID == null) {
			fileID = "";
		}

		if (fileID.equals(getMainPhotoFileID())) {
			return;
		}

		mMainPhotoFileID = fileID;

		try {
			mBaseInfoJsonObject.put("p", mMainPhotoFileID);
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			return;
		}

		mBaseInfo = mBaseInfoJsonObject.toString();
	}

	public void setLoginStatus(LoginStatus loginStatus) {
		if (mLoginStatus == loginStatus) {
			return;
		}

		LoginStatus oldLoginStatus = mLoginStatus;

		mLoginStatus = loginStatus;
		DTNotificationCenter.getInstance().postNotification("IMLoginStatusUpdated",
				oldLoginStatus);
		DTLog.sign("newLoginStatus:" + mLoginStatus);
	}

	public LoginStatus getLoginStatus() {
		return mLoginStatus;
	}

	public boolean isLogined() {
		Log.e("Degub", "curr LoginStatus:" + mLoginStatus);
		return mLoginStatus == LoginStatus.Logined;
	}

//	private Runnable mHeartbeatRunnable = new Runnable() {
//		@Override
//		public void run() {
//			Log.e("Debug", "IMPrivateMyself Runnable:" + IMPrivateMyself.this);
//			heartbeat();
//		}
//	};

//	private void heartbeat() {
//		DTLog.sign("heartbeat");
//
//		if (mLoginStatus == LoginStatus.Logined
//				&& IMPrivateMyself.getInstance().couldSendHeartbeat()) {
//			IMCmdUserHeartbeat cmd = new IMCmdUserHeartbeat();
//
//			cmd.send();
//			
//			checkInitAllInFact();
//		}
//
//		DTAppEnv.cancelPreviousPerformRequest(mHeartbeatRunnable);
//		DTAppEnv.performAfterDelayOnUIThread(80, mHeartbeatRunnable);
//	}

	public boolean removeTeam(long teamID) {
		if (!mAryTeamIDs.contains(teamID)) {
			if (mGroupIDsList.contains(teamID)) {
				mGroupIDsList.remove(Long.valueOf(teamID));
				DTLog.logError();
				return false;
			}

			return false;
		}

		mAryTeamIDs.remove(Long.valueOf(teamID));
		mGroupIDsList.remove(Long.valueOf(teamID));
		return true;
	}

	public ArrayList<Long> getTeamIDList() {
		return mAryTeamIDs;
	}

	public ArrayList<Long> getGroupIDList() {
		return mGroupIDsList;
	}

	public void addTeamID(long teamID) {
		if (mAryTeamIDs.contains(teamID)) {
			return;
		}

		mAryTeamIDs.add(teamID);

		IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);

		if (teamInfo.mTeamType == TeamType.Group) {
			if (mGroupIDsList.contains(teamID)) {
				return;
			}

			mGroupIDsList.add(teamID);
		}
	}

	public void checkIfIPAddressChanged() {
		// 未完成 by lyc
		// DTDevice.getInstance().queryIPAddress();
	}

	public void setTeamIDs(ArrayList<Long> aryTeamIDs) {
		mAryTeamIDs.clear();
		mAryTeamIDs.addAll(aryTeamIDs);
		mGroupIDsList.clear();

		for (long teamID : mAryTeamIDs) {
			IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);

			if (teamInfo.mTeamType == TeamType.Group) {
				if (mGroupIDsList.contains(teamID)) {
					return;
				}

				mGroupIDsList.add(teamID);
			}
		}
	}

	// singleton
	private volatile static IMPrivateMyself sSingleton;

	public static IMPrivateMyself getInstance() {
		if (sSingleton == null) {
			synchronized (IMPrivateMyself.class) {
				if (sSingleton == null) {
					sSingleton = new IMPrivateMyself();
				}
			}
		}

		if (sSingleton == null) {
			DTLog.logError();
		}

		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
		synchronized (IMPrivateMyself.class) {
//			if(sSingleton != null && sSingleton.mHeartbeatRunnable != null) {
//				DTAppEnv.cancelPreviousPerformRequest(sSingleton.mHeartbeatRunnable);
//			}
			sSingleton = new IMPrivateMyself();
		}
	}
	// newInstance end

}
