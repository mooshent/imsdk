package am.imsdk.model.amimteam;

import imsdk.data.localchatmessagehistory.IMGroupChatMessage;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.ambase.IMBaseMsg;
import am.imsdk.model.userinfo.IMUsersMgr;
import android.util.SparseArray;

public class IMTeamMsg extends IMBaseMsg {
	public IMTeamMsg() {
		super();
		addIgnoreField("mStatus");
		addIgnoreField("mState");
		addIgnoreField("mTeamChatMessage");
		addIgnoreField("mExtraData");
	}
	
	public IMTeamMsg(JSONObject serverJsonObject) {
		this();
		
		try {
			mIsRecv = true;
			mServerSendTime = serverJsonObject.getLong("sendtime");
			mContent = serverJsonObject.getString("msgcontent");
			mMsgID = serverJsonObject.getLong("msgid");
			mTeamMsgType = TeamMsgType.fromInt(serverJsonObject.getInt("msgtype"));

			mTeamID = serverJsonObject.getLong("toteamid");
			
			if (mTeamID == 0) {
				DTLog.logError();
				return;
			}
			
			long fromUID = serverJsonObject.getLong("fromuid");
			
			if (fromUID == 0) {
				DTLog.logError();
				return;
			}
			
			mFromCustomUserID = IMUsersMgr.getInstance().getCustomUserID(fromUID);
			
			if (!IMParamJudge.isCustomUserIDLegal(mFromCustomUserID)) {
				DTLog.logError();
				return;
			}
			
			mStatus = IMGroupChatMessage.SUCCESS;
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
		}
	}
	
	public long mTeamID;
	public TeamMsgType mTeamMsgType = TeamMsgType.Normal;
	public int mStatus = IMGroupChatMessage.SUCCESS;
	public static IMTeamMsg sCreatingTeamMsg;
	private IMGroupChatMessage mGroupChatMessage;

	public IMGroupChatMessage getGroupChatMessage() {
		if (mGroupChatMessage != null) {
			if (mTeamMsgType != TeamMsgType.Normal) {
				DTLog.logError();
			}

			return mGroupChatMessage;
		}

		if (mTeamMsgType != TeamMsgType.Normal) {
			DTLog.logError();
			return null;
		}

		sCreatingTeamMsg = this;
		mGroupChatMessage = new IMGroupChatMessage();
		sCreatingTeamMsg = null;

		return mGroupChatMessage;
	}
	
	public enum TeamMsgType {
		Normal(0), 
		Unit(1), // 已废除
		Audio(2), Photo(3), NormalFileText(4), System(9),Video(20),

		IMSDKGroupInfoUpdate(10), IMSDKGroupNewUser(11), IMSDKGroupUserRemoved(12), IMSDKGroupDeleted(
				13), IMSDKGroupQuit(14),

		Custom(100), CustomFileText(104);

		private final int value;

		private TeamMsgType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		private static final SparseArray<TeamMsgType> sValuesArray = new SparseArray<TeamMsgType>();

		static {
			for (TeamMsgType type : TeamMsgType.values()) {
				sValuesArray.put(type.value, type);
			}
		}

		public static TeamMsgType fromInt(int i) {
			TeamMsgType type = sValuesArray.get(Integer.valueOf(i));

			if (type == null) {
				DTLog.logError();
				return null;
			}

			return type;
		}
	}
	
	@Override
	public boolean generateLocalFullPath() {
		if (mTeamID == 0) {
			return false;
		}
		
		if (mIsRecv) {
			if (mMsgID == 0) {
				DTLog.logError();
				return false;
			}
		} else {
			if (mMsgID == 0 && mClientSendTime == 0) {
				DTLog.logError();
				return false;
			}
		}
		
		if (IMPrivateMyself.getInstance().getUID() == 0) {
			DTLog.logError();
			return false;
		}
		
		if (mTeamMsgType == TeamMsgType.Unit) {
			DTLog.logError();
			return false;
		}
		
		setDirectory("ITM", 0);
		setDecryptedDirectory("IMTeamMsg", 0);

		setDirectory(DTTool.getSecretString(IMPrivateMyself.getInstance().getUID()), 1);
		setDecryptedDirectory("" + IMPrivateMyself.getInstance().getUID(), 1);
		
		String groupID = DTTool.getSecretString(mTeamID);
		
		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			DTLog.logError();
			return false;
		}

		setDirectory(DTTool.getMD5String(groupID), 2);
		setDecryptedDirectory(groupID, 2);
		
		if (mIsRecv) {
			if (mMsgID == 0) {
				DTLog.logError();
				return false;
			}

			mLocalFileName = "R_" + DTTool.getSecretString(mMsgID);
			mDecryptedLocalFileName = "R_" + mMsgID;
		} else {
			if (mMsgID != 0) {
				// 已发送成功
				mLocalFileName = "S_" + DTTool.getSecretString(mMsgID);
				mDecryptedLocalFileName = "S_" + mMsgID;
			} else {
				// 未发送成功
				mLocalFileName = DTTool.getSecretString(mClientSendTime);
				mDecryptedLocalFileName = "" + mClientSendTime;
			}
		}
		
		return true;
	}
	
	public long getFromUID() {
		if (!IMParamJudge.isCustomUserIDLegal(mFromCustomUserID)) {
			DTLog.logError();
			return 0;
		}
		
		return IMUsersMgr.getInstance().getUID(mFromCustomUserID);
	}
	
	public String getSendStatusChangedNotificationKey() {
		return mTeamID + "_team_" + mClientSendTime;
	}
}
