package am.imsdk.model.kefu;

import imsdk.data.customerservice.IMSDKCustomerService;
import imsdk.data.customerservice.IMSDKCustomerService.IMCustomerServiceInfo;

import java.lang.ref.WeakReference;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTTool;

public final class IMPrivateCSInfo extends DTLocalModel {
	private long mUID;
	private String mCustomUserID;
	private String mNickName;
	private String mEmail;
	private String mTelephone;
	private String mMainPhotoID;
	private long mVersion;

	private WeakReference<IMCustomerServiceInfo> mCustomerServiceInfoReference;
	private static IMPrivateCSInfo sCreatingCSInfo;
	
	public IMPrivateCSInfo() {
		addDecryptedDirectory("IMPrivateCustomerServiceInfo");
		addDirectory("IPCSI");
	}
	
	@Override
	public boolean generateLocalFullPath() {
		if (mUID == 0) {
			return false;
		}
		
		mDecryptedLocalFileName = mUID + "";
		mLocalFileName = DTTool.getSecretString(mUID);
		return true;
	}

	public static IMPrivateCSInfo getCreatingCSInfo() {
		return sCreatingCSInfo;
	}

	public IMCustomerServiceInfo getCustomerServiceInfo() {
		if (mCustomerServiceInfoReference.get() != null) {
			return mCustomerServiceInfoReference.get();
		}

		sCreatingCSInfo = this;
		IMCustomerServiceInfo customerServiceInfo = new IMCustomerServiceInfo();
		sCreatingCSInfo = null;

		mCustomerServiceInfoReference = new WeakReference<IMSDKCustomerService.IMCustomerServiceInfo>(
				customerServiceInfo);

		return mCustomerServiceInfoReference.get();
	}
	
	public long getUID() {
		if (mUID <= 0) {
			DTLog.logError();
			return 0;
		}
		
		return mUID;
	}
	
	public void setUID(long uid) {
		mUID = uid;
	}
	
	public String getCustomUserID() {
		if (mCustomUserID == null || mCustomUserID.length() == 0) {
			mCustomUserID = "";
		}
		
		return mCustomUserID;
	}
	
	public void setCustomUserID(String customUserID) {
		mCustomUserID = customUserID;

		if (mCustomUserID == null || mCustomUserID.length() == 0) {
			mCustomUserID = "";
		}
	}
	
	public String getNickName() {
		if (mNickName == null || mNickName.length() == 0) {
			mNickName = "";
		}
		
		return mNickName;
	}
	
	public void setNickName(String nickName) {
		mNickName = nickName;

		if (mNickName == null || mNickName.length() == 0) {
			mNickName = "";
		}
	}
	
	public void setTelephone(String telephone) {
		mTelephone = telephone;

		if (mTelephone == null || mTelephone.length() == 0) {
			mTelephone = "";
		}
	}
	
	public String getTelephone() {
		if (mTelephone == null || mTelephone.length() == 0) {
			mTelephone = "";
		}
		
		return mTelephone;
	}
	
	public void setMainPhotoID(String mainPhotoID) {
		mMainPhotoID = mainPhotoID;

		if (mMainPhotoID == null || mMainPhotoID.length() == 0) {
			mMainPhotoID = "";
		}
	}
	
	public String getMainPhotoID() {
		if (mMainPhotoID == null || mMainPhotoID.length() == 0) {
			mMainPhotoID = "";
		}
		
		return mMainPhotoID;
	}

	public String getEmail() {
		if (mEmail == null || mEmail.length() == 0) {
			mEmail = "";
		}
		
		return mEmail;
	}
	
	public void setEmail(String email) {
		mEmail = email;

		if (mEmail == null || mEmail.length() == 0) {
			mEmail = "";
		}
	}
	
	public long getVersion() {
		return mVersion;
	}
	
	public void setVersion(long version) {
		mVersion = version;
	}
}
