package am.imsdk.model.imteam;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;

public final class IMTeamCustomMsgHistory extends IMTeamMsgHistory {
	@Override
	public boolean generateLocalFullPath() {
		if (mUID == 0) {
			return false;
		}

		if (mTeamID == 0) {
			DTLog.logError();
			return false;
		}

		setDirectory(DTTool.getSecretString(mUID), 1);
		setDecryptedDirectory(mUID + "", 1);
		setDirectory(DTTool.getSecretString(mTeamID), 2);
		setDecryptedDirectory(mTeamID + "", 2);
		mLocalFileName = "ITCUMH";
		mDecryptedLocalFileName = "IMTeamCustomMsgHistory";

		return true;
	}
}
