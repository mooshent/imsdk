package am.imsdk.model.imgroup;

import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;

// 只记录本地数据，只记录CustomUserID
public final class IMPrivateRecentGroups extends DTLocalModel {
	public IMPrivateRecentGroups() {
		addRenameField("mAryGroupIDs", "mGroupIDsList");

		if (IMPrivateMyself.getInstance().getUID() > 0) {
			mUID = IMPrivateMyself.getInstance().getUID();
			addDirectory("ITM");
			addDecryptedDirectory("IMTeamMsg");
			addDirectory(DTTool.getSecretString(mUID));
			addDecryptedDirectory("" + mUID);
			mLocalFileName = "IRG";
			mDecryptedLocalFileName = "IMRecentGroups";
			this.readFromFile();
		}
	}

	@Override
	public boolean generateLocalFullPath() {
		if (mUID <= 0) {
			return false;
		}

		return true;
	}

	// singleton
	private volatile static IMPrivateRecentGroups sSingleton;

	public static IMPrivateRecentGroups getInstance() {
		if (sSingleton == null) {
			synchronized (IMPrivateRecentGroups.class) {
				if (sSingleton == null) {
					sSingleton = new IMPrivateRecentGroups();
				}
			}
		}

		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
		synchronized (IMPrivateRecentGroups.class) {
			sSingleton = new IMPrivateRecentGroups();
		}
	}

	// newInstance end

	public boolean add(String groupID) {
		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			DTLog.logError();
			return false;
		}

		if (groupID.length() == 0) {
			DTLog.logError();
			return false;
		}

		if (mGroupIDsList.contains(groupID)) {
			return false;
		}

		mGroupIDsList.add(groupID);
		return true;
	}

	public boolean add(ArrayList<String> aryGroupIDs) {
		if (aryGroupIDs.size() == 0) {
			return false;
		}

		boolean added = false;

		for (String groupID : aryGroupIDs) {
			if (mGroupIDsList.contains(groupID)) {
				continue;
			}

			mGroupIDsList.add(groupID);
			added = true;
		}

		return added;
	}

	public boolean insert(String groupID) {
		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			DTLog.logError();
			return false;
		}

		if (mGroupIDsList.contains(groupID)) {
			return false;
		}

		mGroupIDsList.add(0, groupID);
		return true;
	}

	public String getGroupID(int index) {
		if (index < 0) {
			DTLog.logError();
			return "";
		}

		if (index >= mGroupIDsList.size()) {
			DTLog.logError();
			return "";
		}

		return mGroupIDsList.get(index);
	}

	public boolean remove(String groupID) {
		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			DTLog.logError();
			return false;
		}

		if (!mGroupIDsList.contains(groupID)) {
			return false;
		}

		mGroupIDsList.remove(groupID);
		return true;
	}

	public boolean contains(String groupID) {
		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			DTLog.logError();
			return false;
		}

		return mGroupIDsList.contains(groupID);
	}

	public boolean clear() {
		if (mGroupIDsList.size() == 0) {
			return false;
		}

		mGroupIDsList.clear();
		return true;
	}

	public ArrayList<String> getGroupIDsList() {
		return mGroupIDsList;
	}

	public void setUID(long uid) {
		if (mUID == uid) {
			return;
		}

		mUID = uid;
		mGroupIDsList.clear();
	}

	public boolean setGroupIDsList(ArrayList<String> aryGroupIDs) {
		if (aryGroupIDs.size() == 0 && mGroupIDsList.size() == 0) {
			return false;
		}

		if (mGroupIDsList.size() == aryGroupIDs.size()) {
			boolean needSet = false;

			for (int i = 0; i < aryGroupIDs.size(); i++) {
				if (!IMParamJudge.isGroupIDLegal(aryGroupIDs.get(i))) {
					DTLog.logError();
					return false;
				}

				if (!aryGroupIDs.get(i).equals(mGroupIDsList.get(i))) {
					needSet = true;
					break;
				}
			}

			if (!needSet) {
				return false;
			}
		}

		mGroupIDsList.clear();

		for (String groupID : aryGroupIDs) {
			if (mGroupIDsList.contains(groupID)) {
				DTLog.logError();
				return false;
			}

			mGroupIDsList.add(groupID);
		}

		return true;
	}

	public long getUID() {
		return mUID;
	}

	protected long mUID;
	protected ArrayList<String> mGroupIDsList = new ArrayList<String>();
}
