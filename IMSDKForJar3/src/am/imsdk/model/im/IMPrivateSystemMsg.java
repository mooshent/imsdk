package am.imsdk.model.im;

import imsdk.data.IMSystemMessage;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTTool;

public final class IMPrivateSystemMsg extends DTLocalModel {
	public long mToUID;
	public long mMsgID;
	public IMSystemMessage mSystemMessage = new IMSystemMessage();
	
	public IMPrivateSystemMsg() {
		addForceField("systemMessage");
		addDirectory("ISMH");
		addDecryptedDirectory("IMSystemMsgHistory");
	}
	
	public IMPrivateSystemMsg(JSONObject jsonObject) {
		this();
		
		try {
			mToUID = jsonObject.getLong("touid");
			mSystemMessage.mServerSendTime = jsonObject.getLong("sendtime");
			mSystemMessage.mContent = jsonObject.getString("msgcontent");
			mSystemMessage.mCustonUserId = jsonObject.getString("fromuid");
			mMsgID = jsonObject.getLong("msgid");
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			return;
		}
	}
	
	@Override
	public boolean generateLocalFullPath() {
		setDirectory(DTTool.getSecretString(mToUID), 1);
		setDecryptedDirectory("" + mToUID, 1);
		mLocalFileName = DTTool.getSecretString(mMsgID);
		mDecryptedLocalFileName = "" + mMsgID;
		return true;
	}
}
