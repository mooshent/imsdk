package am.imsdk.model.im;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;
import android.text.TextUtils;

public final class IMUserChatMsgHistoryWithTag extends IMUserMsgHistory {
	public String tag;
	
	@Override
	public boolean generateLocalFullPath() {
		if (mUID == 0) {
			return false;
		}

		if (mOppositeCustomUserID.length() == 0) {
			DTLog.logError();
			return false;
		}
		
		if(TextUtils.isEmpty(tag)) {
			DTLog.logError();
			return false;
		}

		setDirectory(DTTool.getSecretString(mUID), 1);
		setDecryptedDirectory(mUID + "", 1);
		setDirectory(DTTool.getMD5String(mOppositeCustomUserID), 2);
		setDecryptedDirectory(mOppositeCustomUserID, 2);
		mLocalFileName = "IUCMH";
		mDecryptedLocalFileName = "IMUserChatMsgHistory_" + tag;

		return true;
	}

}
