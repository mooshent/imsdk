package am.imsdk.model.userinfo;

import java.util.ArrayList;
import java.util.HashMap;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.imsdk.model.IMPrivateMyself;

public final class IMUsersMgr extends DTLocalModel {
	private HashMap<String, String> mMapCustomUserIDs = new HashMap<String, String>();
	private HashMap<String, Long> mMapUIDs = new HashMap<String, Long>();
	public long mVersionNumber;

	// 仅保存于内存
	private ArrayList<IMPrivateUserInfo> mAryPrivateUserInfos = new ArrayList<IMPrivateUserInfo>();

	private IMUsersMgr() {
		this.addDirectory("IPUI");
		this.addDecryptedDirectory("IMPrivateUserInfo");
		mLocalFileName = "IUM";
		this.addIgnoreField("mAryPrivateUserInfos");
		this.readFromFile();
	}

	public IMPrivateUserInfo getUserInfo(long uid) {
		if (uid == 0) {
			DTLog.logError();
			return null;
		}

		if (IMPrivateMyself.getInstance().getUID() == uid) {
			return IMPrivateMyself.getInstance();
		}

		for (IMPrivateUserInfo userInfo : mAryPrivateUserInfos) {
			if (userInfo.getUID() == uid) {
				return userInfo;
			}
		}

		sCreatingUID = uid;
		
		IMPrivateUserInfo userInfo = new IMPrivateUserInfo();

		sCreatingUID = 0;
		userInfo.readFromFile();
		mAryPrivateUserInfos.add(userInfo);
		return userInfo;
	}
	
	public IMPrivateUserInfo getUserInfo(String customUserID) {
		long uid = getUID(customUserID);
		
		if (uid == 0) {
			DTLog.logError();
			return null;
		}
		
		return getUserInfo(uid);
	}

	public long getUID(String customUserID) {
//		if (customUserID.equals("#CustomerService")) {
//			return IMAppSettings.getInstance().mCustomerServiceUID;
//		}
		
		Long number = mMapUIDs.get(customUserID);

		if (number == null) {
			return 0;
		} else {
			return number.longValue();
		}
	}

	public String getCustomUserID(long uid) {
		if (uid == 0) {
			DTLog.logError();
			return "";
		}
		
//		if (uid == IMAppSettings.getInstance().mCustomerServiceUID) {
//			return "#CustomerService";
//		}

		String customUserID = mMapCustomUserIDs.get("" + uid);

		if (customUserID == null) {
			return "";
		} else {
			return customUserID;
		}
	}

	public void setCustomUserIDForUID(String customUserID, long uid) {
		if (customUserID == null) {
			DTLog.logError();
			return;
		}

		if (customUserID.length() == 0) {
			DTLog.logError();
			return;
		}

		if (uid == 0) {
			DTLog.logError();
			return;
		}

//		if(customUserID.contains("+86")) {
//			customUserID = customUserID.replace("+86", "");
//		}
		
		String value = mMapCustomUserIDs.get("" + uid);

		if (value != null) {
			if (!value.equals(customUserID)) {
				DTLog.logError();
				return;
			}
		}

		mMapCustomUserIDs.put("" + uid, customUserID);
		mMapUIDs.put(customUserID, Long.valueOf(uid));
	}
	
	public static long getCreatingUID() {
		return sCreatingUID;
	}
	
	private static long sCreatingUID;

	// singleton
	private volatile static IMUsersMgr sSingleton;

	/**
	 * @return 单例模式
	 */
	public static IMUsersMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMUsersMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMUsersMgr();
				}
			}
		}
		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
		synchronized (IMUsersMgr.class) {
			sSingleton = new IMUsersMgr();
		}
	}
	// newInstance end
}
