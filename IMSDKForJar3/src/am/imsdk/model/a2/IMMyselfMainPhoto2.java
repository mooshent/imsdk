package am.imsdk.model.a2;

import java.util.Observable;
import java.util.Observer;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.OnInitializedListener;
import imsdk.data.IMSDK.OnActionProgressListener;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedListener;
import am.imsdk.action.IMAction.OnActionPartiallyDoneListener;
import am.imsdk.action.photo.IMActionUserUploadMainPhoto;
import am.imsdk.model.IMPrivateMyself;
import android.graphics.Bitmap;
import android.net.Uri;

public final class IMMyselfMainPhoto2 {
	private static OnInitializedListener sOnInitializedListener;

	static {
		DTNotificationCenter.getInstance().addObserver("MainPhotoModuleInitialized",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!IMPrivateMyself.getInstance().isLogined()) {
							return;
						}

						if (sOnInitializedListener != null) {
							sOnInitializedListener.onInitialized();
						}
					}
				});
	}

	public static boolean isInitialized() {
		return IMPrivateMyself.getInstance().getCustomUserInfoInitedInFact();
	}

	public static void setOnInitializedListener(OnInitializedListener l) {
		sOnInitializedListener = l;
	}

	public static void upload(Bitmap bitmap, final OnActionProgressListener l) {
		// 参数检查
		if (bitmap == null) {
			if (l != null) {
				l.onFailure("bitmap shouldn't be null.");
			}

			return;
		}

		IMActionUserUploadMainPhoto action = new IMActionUserUploadMainPhoto();

		action.mBitmap = bitmap;

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
			@Override
			public void onActionPartiallyDone(double percentage) {
				if (l != null) {
					l.onProgress(percentage);
				}
			}
		};

		action.begin();
	}

	public static Bitmap get() {
		return IMSDKMainPhoto2.get(IMMyself.getCustomUserID());
	}

	public static Uri getLocalUri() {
		return IMSDKMainPhoto2.getLocalUri(IMMyself.getCustomUserID());
	}

	public static byte[] getLocalBuffer() {
		return IMSDKMainPhoto2.getLocalBuffer(IMMyself.getCustomUserID());
	}

	public static Bitmap get(int width, int height) {
		return IMSDKMainPhoto2.get(IMMyself.getCustomUserID(), width, height);
	}

	public static Uri getLocalUri(int width, int height) {
		return IMSDKMainPhoto2.getLocalUri(IMMyself.getCustomUserID(), width, height);
	}

	public static byte[] getLocalBuffer(int width, int height) {
		return IMSDKMainPhoto2
				.getLocalBuffer(IMMyself.getCustomUserID(), width, height);
	}

	public static String getLastError() {
		return IMSDKMainPhoto2.getLastError();
	}
}
