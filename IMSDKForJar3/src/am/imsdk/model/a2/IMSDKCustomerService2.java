package am.imsdk.model.a2;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.OnActionResultListener;
import imsdk.data.IMMyself.OnInitializedListener;
import imsdk.data.IMSDK.OnDataChangedListener;
import imsdk.data.customerservice.IMSDKCustomerService.IMCustomerServiceInfo;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedEndListener;
import am.imsdk.action.IMActionUserRequestCustomerServiceInfo;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.kefu.IMCustomerServiceMgr;
import am.imsdk.model.kefu.IMPrivateCSInfo;

public final class IMSDKCustomerService2 {
	static {
		DTNotificationCenter.getInstance().addObserver("CustomerServiceInited",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!IMSDKCustomerService2.isInitialized()) {
							DTLog.logError();
							return;
						}

						if (sOnInitializedListener != null) {
							sOnInitializedListener.onInitialized();
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("CustomerServiceListUpdated",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!IMSDKCustomerService2.isInitialized()) {
							DTLog.logError();
							return;
						}

						if (sOnDataChangedListener != null) {
							sOnDataChangedListener.onDataChanged();
						}
					}
				});
	}

	private static OnInitializedListener sOnInitializedListener;
	private static OnDataChangedListener sOnDataChangedListener;
	private static String sLastError = "";

	public static boolean isInitialized() {
		//old code : return IMPrivateMyself.getInstance().getAppInfoInitedInFact();
		//motify by ljq 因为注释掉了请求客户的有关逻辑 所以这里先返回true 2015/07/03
		
		return true;
	}

	public static void setOnInitializedListener(final OnInitializedListener l) {
		sOnInitializedListener = l;
	}

	public static ArrayList<String> getList() {
		return IMCustomerServiceMgr.getInstance().getCustomerServiceList();
	}

	public static IMCustomerServiceInfo getCustomerServiceInfo(
			final String customerServiceID) {
		long uid = IMCustomerServiceMgr.getInstance().getUID(customerServiceID);
		IMPrivateCSInfo info = IMCustomerServiceMgr.getInstance().getCSInfo(uid);

		if (uid == 0) {
			DTLog.logError();
			return null;
		}

		if (info == null) {
			DTLog.logError();
			return null;
		}

		return info.getCustomerServiceInfo();
	}

	public static void setOnDataChangedListener(final OnDataChangedListener l) {
		sOnDataChangedListener = l;
	}

	public static long request(final String customerServiceID,
			final long timeoutInterval, final OnActionResultListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!isInitialized()) {
			sLastError = "Customer Service Module Not Initialized Yet";

			if (l != null) {
				DTAppEnv.performAfterDelayOnUIThread(0, new Runnable() {
					@Override
					public void run() {
						l.onFailure("Customer Service Module Not Initialized Yet");
					}
				});
			}

			return actionTime;
		}

		if (!IMParamJudge.isCustomerServiceIDLegal(customerServiceID)) {
			sLastError = IMParamJudge.getLastError();

			final String error = sLastError;

			if (l != null) {
				DTAppEnv.performAfterDelayOnUIThread(0, new Runnable() {
					@Override
					public void run() {
						l.onFailure(error);
					}
				});
			}

			return actionTime;
		}

		IMActionUserRequestCustomerServiceInfo action = new IMActionUserRequestCustomerServiceInfo();

		action.mAppKey = IMMyself.getAppKey();
		action.mCustomerServiceID = customerServiceID;

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {

			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					IMCustomerServiceInfo customerServiceInfo = getCustomerServiceInfo(customerServiceID);

					l.onSuccess(customerServiceInfo);
				}
			}
		};

		action.begin();

		return actionTime;
	}
}
