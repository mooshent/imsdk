package am.imsdk.model.a2;

import imsdk.data.IMSDK;
import imsdk.data.IMSDK.OnDataChangedListener;
import imsdk.data.mainphoto.IMSDKMainPhoto.OnBitmapRequestProgressListener;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedListener;
import am.imsdk.action.IMAction.OnActionPartiallyDoneListener;
import am.imsdk.action.photo.IMActionUserRequestPhoto;
import am.imsdk.action.photo.IMActionUserRequestThumbnail;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.userinfo.IMPrivateUserInfo;
import am.imsdk.model.userinfo.IMUsersMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImageThumbnail;
import am.imsdk.serverfile.image.IMImagesMgr;
import android.graphics.Bitmap;
import android.net.Uri;

public final class IMSDKMainPhoto2 {
	private static String sLastError = "";
	private static HashMap<String, ArrayList<WeakReference<OnDataChangedListener>>> sOnDataChangedListenersHashMap = new HashMap<String, ArrayList<WeakReference<OnDataChangedListener>>>();

	static {
		DTNotificationCenter.getInstance().addObserver("MainPhotoUpdated",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof String)) {
							DTLog.logError();
							return;
						}

						String customUserID = (String) data;

						if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
							DTLog.logError();
							return;
						}

						ArrayList<WeakReference<OnDataChangedListener>> onDataChangedListenersList = sOnDataChangedListenersHashMap
								.get(customUserID);

						if (onDataChangedListenersList == null) {
							return;
						}

						if (onDataChangedListenersList.size() == 0) {
							return;
						}

						for (WeakReference<OnDataChangedListener> weakReference : onDataChangedListenersList) {
							if (weakReference.get() != null) {
								weakReference.get().onDataChanged();
							}
						}
					}
				});
	}

	public static void addOnDataChangedListener(String customUserID,
			OnDataChangedListener l) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return;
		}

		ArrayList<WeakReference<OnDataChangedListener>> onDataChangedListenersList = sOnDataChangedListenersHashMap
				.get(customUserID);

		if (onDataChangedListenersList == null) {
			onDataChangedListenersList = new ArrayList<WeakReference<OnDataChangedListener>>();
			sOnDataChangedListenersHashMap
					.put(customUserID, onDataChangedListenersList);
		}

		for (WeakReference<OnDataChangedListener> weakReference : onDataChangedListenersList) {
			if (weakReference.get() == l) {
				return;
			}
		}

		onDataChangedListenersList
				.add(new WeakReference<IMSDK.OnDataChangedListener>(l));
	}

	public static void removeOnDataChangedListener(final OnDataChangedListener l) {
		for (Map.Entry<String, ArrayList<WeakReference<OnDataChangedListener>>> entry : sOnDataChangedListenersHashMap
				.entrySet()) {
			ArrayList<WeakReference<OnDataChangedListener>> onDataChangedListenersList = entry
					.getValue();

			for (WeakReference<OnDataChangedListener> weakReference : onDataChangedListenersList) {
				if (weakReference.get() == l) {
					onDataChangedListenersList.remove(weakReference);
					break;
				}
			}
		}
	}

	public static long request(final String customUserID, long timeoutInterval,
			final OnBitmapRequestProgressListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			if (l != null) {
				l.onFailure(IMParamJudge.getLastError());
			}

			return actionTime;
		}

		final IMActionUserRequestPhoto action = new IMActionUserRequestPhoto();

		action.mCustomUserID = customUserID;

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
			@Override
			public void onActionPartiallyDone(double percentage) {
				if (l != null) {
					l.onProgress(percentage / 100);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess(action.mBitmap, action.mBuffer);
				}

				DTNotificationCenter.getInstance().postNotification("MainPhotoUpdated",
						customUserID);
			}
		};

		action.mTimeoutInterval = timeoutInterval;
		action.begin();
		return actionTime;
	}

	public static long request(final String customUserID, final int width,
			final int height, final OnBitmapRequestProgressListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			if (l != null) {
				l.onFailure(IMParamJudge.getLastError());
			}

			return actionTime;
		}

		final IMActionUserRequestThumbnail action = new IMActionUserRequestThumbnail();

		action.mCustomUserID = customUserID;
		action.mWidth = width;
		action.mHeight = height;

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
			@Override
			public void onActionPartiallyDone(double percentage) {
				if (l != null) {
					l.onProgress(percentage * 100);
				}

				DTNotificationCenter.getInstance().postNotification("MainPhotoUpdated",
						customUserID);
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess(action.mBitmap, action.mBuffer);
				}
			}
		};

		action.begin();

		return actionTime;
	}

	public static Bitmap get(String customUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return null;
		}

		long uid = IMUsersMgr.getInstance().getUID(customUserID);

		if (uid == 0) {
			sLastError = "uid is 0";
			return null;
		}

		IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(uid);

		if (userInfo.getMainPhotoFileID().length() == 0) {
			sLastError = "userInfo.mMainPhotoFileID is null";
			return null;
		}

		IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
				userInfo.getMainPhotoFileID());

		if (photo == null) {
			DTLog.logError();
			return null;
		}
		
		return photo.getBitmap();
	}

	public static Bitmap get(String customUserID, int width, int height) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return null;
		}

		long uid = IMUsersMgr.getInstance().getUID(customUserID);

		if (uid == 0) {
			sLastError = "uid is 0";
			return null;
		}

		IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(uid);

		if (userInfo.getMainPhotoFileID().length() == 0) {
			sLastError = "userInfo.mMainPhotoFileID is null";
			return null;
		}

		IMImageThumbnail thumbnail = IMImagesMgr.getInstance().getThumbnail(
				userInfo.getMainPhotoFileID(), width, height);

		if (thumbnail == null) {
			DTLog.logError();
			return null;
		}

		return thumbnail.getBitmap();
	}

	public static Uri getLocalUri(String customUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return null;
		}

		long uid = IMUsersMgr.getInstance().getUID(customUserID);

		if (uid == 0) {
			sLastError = "IMSDK Error";
			return null;
		}

		IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(uid);

		if (userInfo.getMainPhotoFileID().length() == 0) {
			return null;
		}

		IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
				userInfo.getMainPhotoFileID());

		return photo.getUri();
	}

	public static byte[] getLocalBuffer(String customUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return null;
		}

		long uid = IMUsersMgr.getInstance().getUID(customUserID);

		if (uid == 0) {
			sLastError = "uid is 0";
			return null;
		}

		IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(uid);

		if (userInfo.getMainPhotoFileID().length() == 0) {
			sLastError = "userInfo.mMainPhotoFileID is null";
			return null;
		}

		IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
				userInfo.getMainPhotoFileID());

		return photo.getBuffer();
	}

	public static Uri getLocalUri(String customUserID, int width, int height) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return null;
		}

		long uid = IMUsersMgr.getInstance().getUID(customUserID);

		if (uid == 0) {
			sLastError = "uid is 0";
			return null;
		}

		IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(uid);

		if (userInfo.getMainPhotoFileID().length() == 0) {
			sLastError = "userInfo.mMainPhotoFileID is null";
			return null;
		}

		IMImageThumbnail thumbnail = IMImagesMgr.getInstance().getThumbnail(
				userInfo.getMainPhotoFileID(), width, height);

		return thumbnail.getUri();
	}

	public static byte[] getLocalBuffer(String customUserID, int width, int height) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return null;
		}

		long uid = IMUsersMgr.getInstance().getUID(customUserID);

		if (uid == 0) {
			sLastError = "uid is 0";
			return null;
		}

		IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(uid);

		if (userInfo.getMainPhotoFileID().length() == 0) {
			sLastError = "userInfo.mMainPhotoFileID is null";
			return null;
		}

		IMImageThumbnail thumbnail = IMImagesMgr.getInstance().getThumbnail(
				userInfo.getMainPhotoFileID(), width, height);

		return thumbnail.getBuffer();
	}

	public static String getLastError() {
		return sLastError;
	}
}
