package am.imsdk.model.a2;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.IMMyselfListener;
import imsdk.data.IMMyself.LoginStatus;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnActionProgressListener;
import imsdk.data.IMMyself.OnCheckServiceLoginStateListener;
import imsdk.data.IMMyself.OnConnectionChangedListener;
import imsdk.data.IMMyself.OnLoginStatusChangedListener;
import imsdk.data.IMMyself.OnReceiveBitmapListener;
import imsdk.data.IMMyself.OnReceiveOrderListener;
import imsdk.data.IMMyself.OnReceiveTextListener;
import imsdk.data.IMSystemMessage;

import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import remote.service.SocketService;
import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTRemoteMgr;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.dtlib.model.c.tool.PollingUtils;
import am.dtlib.model.c.tool.ProcessTool;
import am.dtlib.model.d.DTDevice;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedEndListener;
import am.imsdk.action.IMAction.OnActionPartiallyDoneListener;
import am.imsdk.action.IMActionLogin;
import am.imsdk.action.IMActionLogin.IMActionLoginType;
import am.imsdk.action.ammsgs.IMActionSendUserMsg;
import am.imsdk.demo.util.CPResourceUtil;
import am.imsdk.demo.util.FacesXml;
import am.imsdk.model.IMAudioSender;
import am.imsdk.model.IMAudioSender.OnAudioSenderListener;
import am.imsdk.model.IMChatSetting;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.amim.IMUserMsgsMgr;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import am.imsdk.model.im.IMUserMsgHistory;
import am.imsdk.model.userinfo.IMUsersMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;
import am.imsdk.ui.utils.IMNoticeUtils;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class IMMyself2 {
	public static boolean init(Context applicationContext) {
		if (applicationContext == null) {
			IMParamJudge.setLastError("context couldn't be null");
			return false;
		}

//		if (!IMParamJudge.isAppKeyLegal(appKey)) {
//			return false;
//		}

		if (DTAppEnv.getContext() != applicationContext) {
			// 设置context -- 最高级别环境配置
			DTAppEnv.setContext(applicationContext);

			// 环境配置
			DTAppEnv.setRootDirectory("IMSDK.im");
			DTLog.checkCreateDirectory(DTAppEnv.getIMSDKDirectoryFullPath());

			DTDevice.getInstance();
			DTRemoteMgr.newInstance();
		}

		IMPrivateMyself.getInstance().reinit();
		IMPrivateMyself.getInstance().readFromFile();
		
		//AlarmManager 定时120秒重启一次
		PollingUtils.startPollingService(applicationContext, 60 * 2, SocketService.class, SocketService.ACTION);
		
		DTRemoteMgr.getInstance().checkConnectState(new DTRemoteMgr.OnConnectServiceCallback() {
			
			@Override
			public void onConnected() {
				
			}
		});

		return true;
	}

	public static boolean setCustomUserID(String customUserID) {
		if (!IMParamJudge.isAppKeyLegal(IMMyself.getAppKey())) {
			IMParamJudge.setLastError("You should init first!");
			return false;
		}

		if (!(customUserID instanceof String)) {
			return false;
		}

		if (!IMParamJudge.isCustomUserIDLegal(customUserID) && customUserID != null
				&& customUserID.length() != 0) {
			return false;
		}

		if (sCustomUserID == customUserID) {
			return true;
		}

		sCustomUserID = customUserID;
		
//		if (IMMyself.getLoginStatus() == IMMyself.LoginStatus.Logined) {
//			return true;
//		}

		IMPrivateMyself.getInstance().reinit();
		IMPrivateMyself.getInstance().readFromFile();
		return true;
	}

	public static boolean privateSetCustomUserID(String customUserID) {
		if (!IMParamJudge.isAppKeyLegal(IMMyself.getAppKey())) {
			IMParamJudge.setLastError("You should init first!");
			return false;
		}

		if (!(customUserID instanceof String)) {
			return false;
		}

		if (!IMParamJudge.isCustomUserIDLegal(customUserID) && customUserID != null
				&& customUserID.length() != 0) {
			return false;
		}

		if (sCustomUserID == customUserID) {
			return true;
		}

		sCustomUserID = customUserID;
		IMPrivateMyself.getInstance().reinit();
		IMPrivateMyself.getInstance().readFromFile();
		return true;
	}

	public static boolean setPassword(String password) {
		if (!(password instanceof String)) {
			return false;
		}

		if (!IMParamJudge.isPasswordLegal(password) && password != null
				&& password.length() != 0) {
			return false;
		}

		if (IMPrivateMyself.getInstance().getPassword() == password) {
			return true;
		}

		if (IMMyself.getLoginStatus() != IMMyself.LoginStatus.None) {
			IMMyself.logout();
		}

		IMPrivateMyself.getInstance().setPassword(password);
		return true;
	}

	public static String getPassword() {
		return IMPrivateMyself.getInstance().getPassword();
	}

	public static boolean isLogined() {
		return IMPrivateMyself.getInstance().getLoginStatus()
				.equals(IMMyself.LoginStatus.Logined);
	}

	public static long register(long timeoutInterval, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMParamJudge.isCustomUserIDLegal(getCustomUserID())) {
			commonActionFailure(l, "register", actionTime);
			return actionTime;
		}

		if (!IMParamJudge.isPasswordLegal(getPassword())) {
			commonActionFailure(l, "register", actionTime);
			return actionTime;
		}

//		if (getAppKey().length() == 0) {
//			commonActionFailure(l, "register", actionTime, "appKey不能为空");
//			return actionTime;
//		}

		clearListenersAndPerformRequests();

		DTNotificationCenter.getInstance().addObserver("IMActionLogin_done",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						commonActionSuccess(l, "register", actionTime);
					}
				});

		DTNotificationCenter.getInstance().addObserver("IMActionUserLogin_failed",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (data == null) {
							data = "";
						}

						if (!(data instanceof String)) {
							DTLog.logError();
							return;
						}

						String error = (String) data;

						commonActionFailure(l, "register", actionTime, error);
					}
				});

		IMActionLogin.newInstance();
		IMActionLogin.getInstance().setType(IMActionLoginType.OneKeyRegister);
		IMActionLogin.getInstance().mTimeoutInterval = timeoutInterval;
		IMActionLogin.getInstance().mUID = IMPrivateMyself.getInstance().getUID();
		IMActionLogin.getInstance().mCustomUserID = IMMyself.getCustomUserID();
//		IMActionLogin.getInstance().mAppKey = IMAppSettings.getInstance().mAppKey;
		IMActionLogin.getInstance().setPassword(
				IMPrivateMyself.getInstance().getPassword());
		IMActionLogin.getInstance().begin();

		return actionTime;
	}

	public static void login() {
		login(false, 0, null);
	}
	
	public static void checkServiceLoginStatus(final OnCheckServiceLoginStateListener l) {
		if(IMPrivateMyself.getInstance().getLoginStatus() == LoginStatus.Logined) {
			// SocketService 已登录，如果状态都正常，就不用用户再次登录
			DTRemoteMgr.getInstance().getLoginInfo(new DTRemoteMgr.OnActionListener() {
				
				@Override
				public void onSuccess(Object result) {
					String jsonResult = result.toString();
					
					Log.e("Debug", jsonResult);
					
					if (!TextUtils.isEmpty(jsonResult)) {
						try {
							JSONObject jsonObject = new JSONObject(jsonResult);
							
							int uid = jsonObject.getInt("uid");
//							int sid = jsonObject.getInt("sid");
							
							IMUsersMgr.getInstance().setCustomUserIDForUID(String.valueOf(uid), uid);
							IMUsersMgr.getInstance().saveFile();
							
							IMMyself2.setCustomUserID(String.valueOf(uid));
//							IMPrivateMyself.getInstance().setSessionID(sid);
							IMPrivateMyself.getInstance().setLoginStatus(IMMyself.LoginStatus.Logined);
							
							l.onConnected();
							
							return;
							
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					
					DTLog.sign("Service有登陆，但信息有误，请重新登录");
					l.onNoConnection();
				}
				
				@Override
				public void onFailure() {
					DTLog.sign("Service登录数据获取异常，请重新登录");
					l.onNoConnection();
				}
			});
			
		} else {
			DTLog.sign("可以登录");
			l.onNoConnection();
		}
	}

	public static void login(final boolean autoRegister, final long timeoutInterval,
			final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;
		final String actionType = "login";
		
		if(IMPrivateMyself.getInstance().getLoginStatus() == LoginStatus.Logined) {
			l.onFailure("Service已有登陆，不能重复登录，请先退出");
			return;
		}

		if (autoRegister) {
			if (!IMParamJudge.isCustomUserIDLegal(getCustomUserID())
					&& getCustomUserID() != null && getCustomUserID().length() != 0) {
				commonActionFailure(l, actionType, actionTime);
				return;
			}
		} else {
			if (!IMParamJudge.isCustomUserIDLegal(getCustomUserID())) {
				commonActionFailure(l, actionType, actionTime);
				return;
			}
		}

		if (autoRegister) {
			if (!IMParamJudge.isPasswordLegal(getPassword()) && getPassword() != null
					&& getPassword().length() != 0) {
				commonActionFailure(l, actionType, actionTime);
				return;
			}
		} else {
			if (!IMParamJudge.isCustomUserIDLegal(getCustomUserID())) {
				commonActionFailure(l, actionType, actionTime);
				return;
			}
		}

		clearListenersAndPerformRequests();

		DTNotificationCenter.getInstance().addObserver("IMActionLogin_done",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						FacesXml.downLoadGif(DTAppEnv.getContext());
						commonActionSuccess(l, actionType, actionTime);
					}
				});

		DTNotificationCenter.getInstance().addObserver("IMActionUserLogin_failed",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (data == null) {
							data = "";
						}

						if (!(data instanceof String)) {
							DTLog.logError();
							return;
						}

						String error = (String) data;

						commonActionFailure(l, actionType, actionTime, error);
					}
				});

		if (IMPrivateMyself.getInstance().getPassword().length() == 0) {
			IMPrivateMyself.getInstance().setPassword("www.IMSDK.im");
		}

		IMActionLogin.newInstance();
		IMActionLogin.getInstance().setType(IMActionLoginType.OneKeyLogin);
		IMActionLogin.getInstance().mTimeoutInterval = timeoutInterval;
		IMActionLogin.getInstance().mUID = IMPrivateMyself.getInstance().getUID();
		IMActionLogin.getInstance().mCustomUserID = IMMyself.getCustomUserID();
//		IMActionLogin.getInstance().mAppKey = IMAppSettings.getInstance().mAppKey;
		IMActionLogin.getInstance().setPassword(
				IMPrivateMyself.getInstance().getPassword());
		IMActionLogin.getInstance().begin();

	}

	public static void logout() {
		DTRemoteMgr.getInstance().logout();
	}
	
	public static void stopSocketService() {
		PollingUtils.stopPollingService(DTAppEnv.getContext(), SocketService.class, SocketService.ACTION);
		
		DTAppEnv.getContext().stopService(new Intent(DTAppEnv.getContext(), SocketService.class));
	}

	public static long sendText(String text, String toCustomUserID) {
		return sendText(text, toCustomUserID, 10, null);
	}
	
	public static long sendText(String text, String toCustomUserID,
			long timeoutInterval, final OnActionListener l) {
		return sendText(text, toCustomUserID, null, timeoutInterval, l);
	}

	public static long sendText(String text, String toCustomUserID, HashMap<String, Object> mTags,
			long timeoutInterval, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			commonActionFailure(l, "sendText", actionTime);
			return actionTime;
		}

		if (!IMParamJudge.isIMTextLegal(text)) {
			commonActionFailure(l, "sendText", actionTime);
			return actionTime;
		}

		if (sLastSendTextTimeMillis != 0
				&& System.currentTimeMillis() - sLastSendTextTimeMillis < 1000) {
			if (l != null) {
				l.onFailure("发送频率太快");
			}

			if (sListener != null) {
				sListener.onActionFailure("sendText", "发送频率太快", actionTime);
			}
			return actionTime;
		}

		sLastSendTextTimeMillis = System.currentTimeMillis();

		// 创建IMUserMsg
		final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
				toCustomUserID, actionTime);

		if (userMsg == null) {
			DTLog.logError();
			return actionTime;
		}

		userMsg.mToCustomUserID = toCustomUserID;
		userMsg.mContent = text;
		userMsg.mUserMsgType = UserMsgType.Normal;
		userMsg.mTags = mTags;
		userMsg.saveFile();

		final IMUserMsgHistory history;
		if(mTags != null && mTags.size() > 0 && mTags.containsKey("orderId")) {
			// 维护聊天记录
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(toCustomUserID, mTags.get("orderId").toString());
		} else {
			// 维护聊天记录
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(toCustomUserID);
		}
		
		history.insertUnsentUserMsg(userMsg.mClientSendTime);
		history.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				history.getNewMsgNotificationKey());

		// 维护最近联系人
		IMPrivateRecentContacts.getInstance().insert(toCustomUserID);
		IMPrivateRecentContacts.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification(
				IMPrivateRecentContacts.getInstance().notificationKey());

		// 发送消息
		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				// 维护聊天记录
				history.replaceUnsentUserMsgToSent(userMsg);
				history.saveFile();

				commonActionSuccess(l, "sendText", actionTime);
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				commonActionFailure(l, "sendText", actionTime, error);
			}
		};

		action.mUserMsg = userMsg;
		
		if(timeoutInterval < 10) {
			timeoutInterval = 10;
		}
		action.mTimeoutInterval = timeoutInterval;
		action.mBeginTime = actionTime;
		action.begin();

		return actionTime;
	}
	
	public static long sendNoticeMsg(String text, String toCustomUserID,
			HashMap<String, Object> tags, long timeoutInterval,
			final OnActionListener l) {

		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			commonActionFailure(l, "sendNoticeMsg", actionTime);
			return actionTime;
		}

		if (sLastSendTextTimeMillis != 0
				&& System.currentTimeMillis() - sLastSendTextTimeMillis < 1000) {
			if (l != null) {
				l.onFailure("发送频率太快");
			}

			if (sListener != null) {
				sListener.onActionFailure("sendNoticeMsg", "发送频率太快", actionTime);
			}
			return actionTime;
		}

		sLastSendTextTimeMillis = System.currentTimeMillis();

		// 创建IMUserMsg
		final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
				toCustomUserID, actionTime);

		if (userMsg == null) {
			DTLog.logError();
			return actionTime;
		}

		userMsg.mToCustomUserID = toCustomUserID;
		userMsg.mContent = text;
		userMsg.mUserMsgType = UserMsgType.IMSDKNotice;
		userMsg.mTags = tags;
		userMsg.saveFile();

		// 维护最近联系人
		IMPrivateRecentContacts.getInstance().insert(toCustomUserID);
		IMPrivateRecentContacts.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification(
				IMPrivateRecentContacts.getInstance().notificationKey());

		// 发送消息
		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				commonActionSuccess(l, "sendNoticeMsg", actionTime);
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				commonActionFailure(l, "sendNoticeMsg", actionTime, error);
			}
		};

		action.mUserMsg = userMsg;
		if(timeoutInterval < 10) {
			timeoutInterval = 10;
		}
		action.mTimeoutInterval = timeoutInterval;
		action.mBeginTime = actionTime;
		action.begin();

		return actionTime;
	}
	
	/**
	 * 
	 * @Title: sendOrderState 
	 * @Description: 发送订单
	 * @param orderType 0-结束确认 1-结束确认被拒绝 2-订单成功结束
	 * @return long    返回类型 
	 * @throws 
	 * @author 方子君
	 */
	public static long sendOrderState(int orderType, String toCustomUserID, HashMap<String, Object> mTags,
			long timeoutInterval, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			commonActionFailure(l, "sendOrder", actionTime);
			return actionTime;
		}

		if (sLastSendTextTimeMillis != 0
				&& System.currentTimeMillis() - sLastSendTextTimeMillis < 1000) {
			if (l != null) {
				l.onFailure("发送频率太快");
			}

			if (sListener != null) {
				sListener.onActionFailure("sendOrder", "发送频率太快", actionTime);
			}
			return actionTime;
		}

		sLastSendTextTimeMillis = System.currentTimeMillis();

		// 创建IMUserMsg
		final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
				toCustomUserID, actionTime);

		if (userMsg == null) {
			DTLog.logError();
			return actionTime;
		}

		userMsg.mToCustomUserID = toCustomUserID;
		
		JSONObject content = new JSONObject();
		try {
			content.put("subType", orderType);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		userMsg.mContent = content.toString();
		userMsg.mUserMsgType = UserMsgType.Order;
		userMsg.mTags = mTags;
		userMsg.saveFile();

		// 维护最近联系人
		IMPrivateRecentContacts.getInstance().insert(toCustomUserID);
		IMPrivateRecentContacts.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification(
				IMPrivateRecentContacts.getInstance().notificationKey());

		// 发送消息
		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				// 维护聊天记录
//				history.replaceUnsentUserMsgToSent(userMsg);
//				history.saveFile();

				commonActionSuccess(l, "sendOrder", actionTime);
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				commonActionFailure(l, "sendOrder", actionTime, error);
			}
		};

		action.mUserMsg = userMsg;
		action.mTimeoutInterval = timeoutInterval;
		action.mBeginTime = actionTime;
		action.begin();

		return actionTime;
	}

	public static boolean startRecording(String toCustomUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			return false;
		}

		return IMAudioSender.getInstance().startRecordingToUser(toCustomUserID);
	}

	public static long stopRecording(boolean needSend, HashMap<String, Object> mTags, final long timeoutInterval,
			final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		boolean result = IMAudioSender.getInstance().stopRecording(needSend, mTags,
				timeoutInterval, new OnAudioSenderListener() {
					@Override
					public void onSendSuccess() {
						commonActionSuccess(l, "SendAudio", actionTime);
					}

					@Override
					public void onSendFailure(String error) {
						commonActionFailure(l, "SendAudio", actionTime);
					}

					@Override
					public void onRecordSuccess() {
					}

					@Override
					public void onRecordFailure(String error) {
						commonActionFailure(l, "SendAudio", actionTime);
					}
				}, actionTime);

		if (!result) {
			if (l != null) {
				l.onFailure("IMSDK Error");
			}
		}

		return actionTime;
	}
	
	public static long sendBitmap(final Bitmap bitmap, final String toCustomUserID,
			final long timeoutInterval, final OnActionProgressListener l) {
		return sendBitmap(bitmap, toCustomUserID, null, timeoutInterval, l);
	}

	public static long sendBitmap(final Bitmap bitmap, final String toCustomUserID, HashMap<String, Object> mTags, 
			final long timeoutInterval, final OnActionProgressListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			if (l != null) {
				l.onFailure(IMParamJudge.getLastError());
			}

			if (sListener != null) {
				sListener.onActionFailure("sendBitmap", IMParamJudge.getLastError(),
						actionTime);
			}

			return actionTime;
		}

		if (sLastSendTextTimeMillis != 0
				&& System.currentTimeMillis() - sLastSendTextTimeMillis < 1000) {
			if (l != null) {
				l.onFailure("发送频率太快");
			}

			if (sListener != null) {
				sListener.onActionFailure("sendBitmap", "发送频率太快", actionTime);
			}

			return actionTime;
		}

		sLastSendTextTimeMillis = System.currentTimeMillis();

		// 生成IMImagePhoto
		final IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(bitmap);

		photo.saveFile();

		// 创建content
		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put("fileID", photo.mFileID);
			jsonObject.put("width", photo.getBitmap().getWidth());
			jsonObject.put("height", photo.getBitmap().getHeight());
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();

			if (l != null) {
				l.onFailure("IMSDK Error");
			}

			if (sListener != null) {
				sListener.onActionFailure("sendBitmap", "IMSDK Error", actionTime);
			}

			return actionTime;
		}

		// 创建IMUserMsg
		final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
				toCustomUserID, actionTime);

		userMsg.mToCustomUserID = toCustomUserID;
		userMsg.mContent = jsonObject.toString();
		userMsg.mUserMsgType = UserMsgType.Photo;
		userMsg.mTags = mTags;
		userMsg.saveFile();

		final IMUserMsgHistory history;
		if(mTags != null && mTags.size() > 0 && mTags.containsKey("orderId")) {
			// 维护聊天记录
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(toCustomUserID, mTags.get("orderId").toString());
		} else {
			// 维护聊天记录
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(toCustomUserID);
		}
		
		history.insertUnsentUserMsg(userMsg.mClientSendTime);
		history.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				history.getNewMsgNotificationKey());

		// 维护最近联系人
		IMPrivateRecentContacts.getInstance().insert(toCustomUserID);
		IMPrivateRecentContacts.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification(
				IMPrivateRecentContacts.getInstance().notificationKey());

		// 发送消息
		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				// 维护聊天记录
				history.replaceUnsentUserMsgToSent(userMsg);
				history.saveFile();

				if (l != null) {
					l.onSuccess();
				}

				if (sListener != null) {
					sListener.onActionSuccess("SendBitmap", actionTime);
				}
			}
		};

		action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
			@Override
			public void onActionPartiallyDone(double percentage) {
				if (l != null) {
					l.onProgress(percentage / 100);
				}
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				if (l != null) {
					l.onFailure(error);
				}

				if (sListener != null) {
					sListener.onActionFailure("SendBitmap", error, actionTime);
				}
			}
		};

		action.mUserMsg = userMsg;

		if(timeoutInterval < 10) {
			action.mTimeoutInterval = 10;
		} else {
			action.mTimeoutInterval = timeoutInterval;
		}

		action.mBeginTime = actionTime;
		action.begin();
		return actionTime;
	}

	public static void setListener(IMMyself.IMMyselfListener listener) {
		sListener = listener;
	}

	public static String getCustomUserID() {
		return IMMyself2.sCustomUserID != null ? IMMyself2.sCustomUserID : "";
	}
	
	public static void setNickName(String nickName) {
		IMPrivateMyself.getInstance().setNickName(nickName);
	}
	
	public static String getAppKey() {
//		if (IMAppSettings.getInstance().mAppKey == null) {
//			IMAppSettings.getInstance().mAppKey = "";
//		}
//
//		return IMAppSettings.getInstance().mAppKey;
		return "";
	}

	public static LoginStatus getLoginStatus() {
		return IMPrivateMyself.getInstance().getLoginStatus();
	}

	protected static void commonActionSuccess(final OnActionListener l,
			final String actionType, final long actionTime) {
		if (!DTAppEnv.isUIThread()) {
			DTLog.logError();
			return;
		}

		if (l != null) {
			DTAppEnv.performAfterDelayOnUIThread(0, new Runnable() {
				@Override
				public void run() {
					l.onSuccess();
				}
			});
		}

		if (sListener != null) {
			DTAppEnv.performAfterDelayOnUIThread(0, new Runnable() {
				@Override
				public void run() {
					sListener.onActionSuccess(actionType, actionTime);
				}
			});
		}
	}

	protected static void commonActionFailure(OnActionListener l, String actionType,
			long actionTime) {
		commonActionFailure(l, actionType, actionTime, IMParamJudge.getLastError());
	}

	protected static void commonActionFailure(final OnActionListener l,
			final String actionType, final long actionTime, final String error) {
		if (!DTAppEnv.isUIThread()) {
			DTLog.logError();
			return;
		}

		if (l != null) {
			DTAppEnv.performAfterDelayOnUIThread(0, new Runnable() {
				@Override
				public void run() {
					l.onFailure(error);
				}
			});
		}

		if (sListener != null) {
			DTAppEnv.performAfterDelayOnUIThread(0, new Runnable() {
				@Override
				public void run() {
					sListener.onActionFailure(actionType, error, actionTime);
				}
			});
		}
	}

	public static void setOnReceiveTextListener(OnReceiveTextListener l) {
		sOnReceiveTextListener = l;
	}

	public static void setOnReceiveBitmapListener(OnReceiveBitmapListener l) {
		sOnReceiveBitmapListener = l;
	}
	
	public static void setOnReceiveOrderListener(OnReceiveOrderListener l) {
		sOnReceiveOrderListener = l;
	}

	public static void setOnConnectionChangedListener(OnConnectionChangedListener l) {
		sOnConnectionChangedListener = l;
	}

	private static void clearListenersAndPerformRequests() {
		DTNotificationCenter.getInstance().removeObservers("IMActionLogin_done");
		DTNotificationCenter.getInstance().removeObservers("IMActionUserLogin_failed");
		DTNotificationCenter.getInstance().removeObservers("IMActionLogin_failed");
	}

	private static String sCustomUserID = "";
	private static long sLastSendTextTimeMillis = 0;

	// 通用Listener
	private static IMMyselfListener sListener;

	// IM相关
	private static OnReceiveTextListener sOnReceiveTextListener;
	private static OnReceiveBitmapListener sOnReceiveBitmapListener;
	private static OnReceiveOrderListener sOnReceiveOrderListener;

	// 登录与连接相关
	private static OnConnectionChangedListener sOnConnectionChangedListener;
	private static OnLoginStatusChangedListener sLoginStatusChangedListener;

	public static long getLastSendTextTimeMillis() {
		return sLastSendTextTimeMillis;
	}

//	private static void tryAutoLogin() {
//		DTLog.sign("tryAutoLogin");
//
//		if (!IMAppSettings.getInstance().getAutoLogin()) {
//			return;
//		}
//
//		if (IMPrivateMyself.getInstance().getUID() == 0) {
//			DTLog.logError();
//			return;
//		}
//
//		if (IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.None
//				&& IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.AutoLogining) {
//			DTLog.logError();
//			return;
//		}
//
//		if (IMPrivateMyself.getInstance().getPassword() == null
//				|| IMPrivateMyself.getInstance().getPassword().length() == 0) {
//			DTLog.logError();
//			return;
//		}
//
//		DTLog.sign("AutoLogin begin");
//		IMActionLogin.newInstance();
//		IMActionLogin.getInstance().setType(IMActionLoginType.AutoLogin);
//		IMActionLogin.getInstance().mUID = IMPrivateMyself.getInstance().getUID();
//		IMActionLogin.getInstance().mCustomUserID = IMPrivateMyself.getInstance()
//				.getCustomUserID();
//		IMActionLogin.getInstance().mAppKey = IMAppSettings.getInstance().mAppKey;
//		IMActionLogin.getInstance().setPassword(
//				IMPrivateMyself.getInstance().getPassword());
//		IMActionLogin.getInstance().begin();
//	}

	// 1. 登录状态下调用
	// 2. 断线重连状态下调用
//	private static void tryReconnect() {
//		DTLog.sign("tryReconnect");
//
//		if (IMPrivateMyself.getInstance().getUID() == 0) {
//			DTLog.logError();
//			return;
//		}
//
//		if (IMPrivateMyself.getInstance().getLoginStatus() == LoginStatus.Logined) {
////			if (IMSocket.getInstance().getStatus() == DTSocketStatus.Connected) {
////				DTLog.logError();
////				return;
////			}
//
//			IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.Reconnecting);
//		}
//
//		if (IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.Reconnecting) {
//			DTLog.logError();
//			return;
//		}
//
//		clearListenersAndPerformRequests();
//
//		final long actionTime = System.currentTimeMillis() / 1000;
//
//		DTNotificationCenter.getInstance().addObserver("IMActionLogin_done",
//				new Observer() {
//					@Override
//					public void update(Observable observable, Object data) {
//						if (sListener != null) {
//							sListener.onReconnected();
//						}
//
//						if (sOnConnectionChangedListener != null) {
//							sOnConnectionChangedListener.onReconnected();
//						}
//
//						commonActionSuccess(null, "reconnect", actionTime);
//					}
//				});
//
//		IMActionLogin.newInstance();
//		IMActionLogin.getInstance().setType(IMActionLoginType.Reconnect);
//		IMActionLogin.getInstance().mUID = IMPrivateMyself.getInstance().getUID();
//		IMActionLogin.getInstance().mCustomUserID = IMPrivateMyself.getInstance()
//				.getCustomUserID();
//		IMActionLogin.getInstance().mAppKey = IMAppSettings.getInstance().mAppKey;
//		IMActionLogin.getInstance().setPassword(
//				IMPrivateMyself.getInstance().getPassword());
//		IMActionLogin.getInstance().begin();
//	}

	private static Observer sSocketUpdatedObserver = new Observer() {
		@Override
		public void update(Observable observable, Object data) {
			// TODO 设置Socket本地状态
		}
	};
	
	public static NotificationManager manager=null;
	
	private static void showNotification(String toCustomUserID, String nickName, String content, long serverSendTime) {
		if (manager == null) {
			manager = (NotificationManager) DTAppEnv.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
		}
		
		manager.cancel(Integer.valueOf(toCustomUserID));

		Intent intent = new Intent();
		intent.setAction("imsdk.ui.waitingActivity");
		intent.putExtra("CustomUserID", toCustomUserID);

		// 创建一个PendingIntent，和Intent类似，不同的是由于不是马上调用，需要在下拉状态条出发的activity，所以采用的是PendingIntent,即点击Notification跳转启动到哪个Activity
		PendingIntent pendingIntent = PendingIntent.getActivity(DTAppEnv.getContext(), 0, intent, 0);
		// 下面需兼容Android 2.x版本是的处理方式
		// Notification notify1 = new Notification(R.drawable.message,
		// "TickerText:" + "您有新短消息，请注意查收！", System.currentTimeMillis());
		Notification notify1 = new Notification();
		notify1.icon = CPResourceUtil.getDrawableId(DTAppEnv.getContext(), "ic_launcher");
		
		if(IMChatSetting.getInstance().isMsgSound()) {
			notify1.sound=Uri.parse("android.resource://" + DTAppEnv.getContext().getPackageName() + "/" + CPResourceUtil.getRawId(DTAppEnv.getContext(), "notify")); 
		}
		 
		if(IMChatSetting.getInstance().isMsgVibrate()) {
			notify1.vibrate = new long[] {0,100,200,300};
		}
		
		notify1.tickerText = "您有新消息，请注意查收！";
		notify1.when = serverSendTime;
		notify1.setLatestEventInfo(DTAppEnv.getContext(), nickName, content, pendingIntent);
		notify1.number = 1;
		notify1.flags |= Notification.FLAG_AUTO_CANCEL;
		// FLAG_AUTO_CANCEL表明当通知被用户点击时，通知将被清除。
		// 通过通知管理器来发起通知。如果id不同，则每click，在statu那里增加一个提示
		manager.notify(Integer.valueOf(toCustomUserID), notify1);
	}

	static {
		DTNotificationCenter.getInstance().addObserver("SOCKET_UPDATED",
				sSocketUpdatedObserver);

		DTNotificationCenter.getInstance().addObserver("IMLoginStatusUpdated",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (IMPrivateMyself.getInstance().getLoginStatus() == LoginStatus.Reconnecting) {
							if (sOnConnectionChangedListener != null) {
								sOnConnectionChangedListener.onDisconnected(false);
							}

							if (sListener != null) {
								sListener.onDisconnected(false);
							}
						}

						if (sLoginStatusChangedListener == null) {
							return;
						}

						if (!(data instanceof LoginStatus)) {
							DTLog.logError();
							return;
						}

						LoginStatus oldLoginStatus = (LoginStatus) data;

						sLoginStatusChangedListener.onLoginStatusChanged(
								oldLoginStatus, getLoginStatus());
					}
				});

		DTNotificationCenter.getInstance().addObserver("IMReceiveText", new Observer() {
			@Override
			public void update(Observable observable, Object data) {
				if (!(data instanceof IMUserMsg)) {
					DTLog.logError();
					return;
				}

				IMUserMsg userMsg = (IMUserMsg) data;

				if (!userMsg.mIsRecv) {
					DTLog.logError();
					return;
				}

				if (userMsg.mUserMsgType != UserMsgType.Normal) {
					DTLog.logError();
					return;
				}

				if (getLoginStatus() != LoginStatus.Logined) {
					return;
				}

				if (IMPrivateMyself.getInstance().getUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() != IMPrivateMyself.getInstance().getUID()) {
					DTLog.sign("userMsg.getToUID():" + userMsg.getToUID());
					DTLog.sign("IMPrivateMyself.getInstance().getUID():"
							+ IMPrivateMyself.getInstance().getUID());
					DTLog.logError();
					return;
				}
				

				if (sOnReceiveTextListener != null) {
					sOnReceiveTextListener.onReceiveText(userMsg.mContent,
							userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mServerSendTime, userMsg.mExtraData2.toString());
				}

				if (sListener != null) {
					sListener.onReceiveText(userMsg.mContent,
							userMsg.mFromCustomUserID, userMsg.mServerSendTime);
				}
				
				if(ProcessTool.isBackground(DTAppEnv.getContext())) {
					showNotification(userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mContent, userMsg.mServerSendTime * 1000);
				} else {
					IMNoticeUtils.playMsgNotice(DTAppEnv.getContext());
				}
			}
		});
		
		DTNotificationCenter.getInstance().addObserver("IMReceiveOrder", new Observer() {
			@Override
			public void update(Observable observable, Object data) {
				if (!(data instanceof IMUserMsg)) {
					DTLog.logError();
					return;
				}

				IMUserMsg userMsg = (IMUserMsg) data;

				if (!userMsg.mIsRecv) {
					DTLog.logError();
					return;
				}

				if (userMsg.mUserMsgType != UserMsgType.Order) {
					DTLog.logError();
					return;
				}

				if (getLoginStatus() != LoginStatus.Logined) {
					return;
				}

				if (IMPrivateMyself.getInstance().getUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() != IMPrivateMyself.getInstance().getUID()) {
					DTLog.sign("userMsg.getToUID():" + userMsg.getToUID());
					DTLog.sign("IMPrivateMyself.getInstance().getUID():"
							+ IMPrivateMyself.getInstance().getUID());
					DTLog.logError();
					return;
				}
				
				if(ProcessTool.isBackground(DTAppEnv.getContext())) {
					showNotification(userMsg.mFromCustomUserID, userMsg.getNickName(), "[订单消息]", userMsg.mServerSendTime * 1000);
				} else {
					IMNoticeUtils.playMsgNotice(DTAppEnv.getContext());
				}

				if (sOnReceiveOrderListener != null) {
					sOnReceiveOrderListener.onReceiveOrder(userMsg.mContent, userMsg.mFromCustomUserID, 
							userMsg.getNickName(), userMsg.mServerSendTime, userMsg.mExtraData2.toString());
				}

			}
		});
		
		DTNotificationCenter.getInstance().addObserver("IMReceiveAudio", new Observer() {
			@Override
			public void update(Observable observable, Object data) {
				if (!(data instanceof IMUserMsg)) {
					DTLog.logError();
					return;
				}

				IMUserMsg userMsg = (IMUserMsg) data;

				if (!userMsg.mIsRecv) {
					DTLog.logError();
					return;
				}

				if (userMsg.mUserMsgType != UserMsgType.Audio) {
					DTLog.logError();
					return;
				}

				if (getLoginStatus() != LoginStatus.Logined) {
					return;
				}

				if (IMPrivateMyself.getInstance().getUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() != IMPrivateMyself.getInstance().getUID()) {
					DTLog.sign("userMsg.getToUID():" + userMsg.getToUID());
					DTLog.sign("IMPrivateMyself.getInstance().getUID():"
							+ IMPrivateMyself.getInstance().getUID());
					DTLog.logError();
					return;
				}
				
				if(ProcessTool.isBackground(DTAppEnv.getContext())) {
					showNotification(userMsg.mFromCustomUserID, userMsg.getNickName(), "[语音]", userMsg.mServerSendTime * 1000);
				} else {
					IMNoticeUtils.playMsgNotice(DTAppEnv.getContext());
				}

				if (sOnReceiveTextListener != null) {
					sOnReceiveTextListener.onReceiveAudio(DTTool.getSecretString(userMsg.mMsgID),
							userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mServerSendTime, userMsg.mExtraData2.toString());
				}

				if (sListener != null) {
					sListener.onReceiveAudio(DTTool.getSecretString(userMsg.mMsgID),
							userMsg.mFromCustomUserID, userMsg.mServerSendTime);
				}
			}
		});

		DTNotificationCenter.getInstance().addObserver("IMReceiveSystemText",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMSystemMessage)) {
							DTLog.logError();
							return;
						}

						IMSystemMessage systemMessage = (IMSystemMessage) data;

						if (getLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMPrivateMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}
						
						if(ProcessTool.isBackground(DTAppEnv.getContext())) {
							showNotification(systemMessage.mCustonUserId, "[系统消息]", systemMessage.mContent, systemMessage.mServerSendTime);
						} else {
							IMNoticeUtils.playMsgNotice(DTAppEnv.getContext());
						}

						if (sOnReceiveTextListener != null) {
							DTLog.sign("onReceiveText");
							sOnReceiveTextListener.onReceiveSystemText(
									systemMessage.mCustonUserId, 
									systemMessage.mContent,
									systemMessage.mServerSendTime);
						}

						if (sListener != null) {
							DTLog.sign("sListener onReceiveText");
							sListener.onReceiveSystemText(systemMessage.mCustonUserId, systemMessage.mContent,
									systemMessage.mServerSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("LoginConflict", new Observer() {
			@Override
			public void update(Observable observable, Object data) {
				if (!IMPrivateMyself.getInstance().isLogined()
						&& IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.AutoLogining) {
					return;
				}

//				IMAppSettings.getInstance().mLastLoginCustomUserID = "";
//				IMAppSettings.getInstance().saveFile();
				IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.None);
				DTRemoteMgr.getInstance().reconnect();

				if (sOnConnectionChangedListener != null) {
					sOnConnectionChangedListener.onDisconnected(true);
				}

				if (sListener != null) {
					sListener.onDisconnected(true);
				}
			}
		});

		DTNotificationCenter.getInstance().addObserver("StopRecordingThenSendAudio",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (userMsg.mUserMsgType != UserMsgType.Audio) {
							DTLog.logError();
							return;
						}

						if (!DTAppEnv.isUIThread()) {
							DTLog.logError();
							return;
						}

						if (sListener != null) {
							sListener.onActionSuccess("StopRecordingThenSendAudio",
									userMsg.mClientSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver(
				"StopRecordingThenSendAudioFailed", new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (userMsg.mUserMsgType != UserMsgType.Audio) {
							DTLog.logError();
							return;
						}

						if (!DTAppEnv.isUIThread()) {
							DTLog.logError();
							return;
						}

						if (sListener != null) {
							sListener.onActionFailure("StopRecordingThenSendAudio", "",
									userMsg.mClientSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("IMReceiveBitmapMessage",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (!userMsg.mIsRecv) {
							DTLog.logError();
							return;
						}

						if (userMsg.mUserMsgType != UserMsgType.Photo) {
							DTLog.logError();
							return;
						}

						if (getLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMPrivateMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() != IMPrivateMyself.getInstance()
								.getUID()) {
							DTLog.logError();
							return;
						}
						
						if(ProcessTool.isBackground(DTAppEnv.getContext())) {
							showNotification(userMsg.mFromCustomUserID, userMsg.getNickName(), "[图片]", userMsg.mServerSendTime * 1000);
						} else {
							IMNoticeUtils.playMsgNotice(DTAppEnv.getContext());
						}

						if (sOnReceiveBitmapListener != null) {
							sOnReceiveBitmapListener.onReceiveBitmapMessage(
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mServerSendTime, userMsg.mExtraData2.toString());
						}

						if (sListener != null) {
							sListener.onReceiveBitmapMessage(
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("IMReceiveBitmapProgress",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (!userMsg.mIsRecv) {
							DTLog.logError();
							return;
						}

						if (userMsg.mUserMsgType != UserMsgType.Photo) {
							DTLog.logError();
							return;
						}

						if (getLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMPrivateMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() != IMPrivateMyself.getInstance()
								.getUID()) {
							DTLog.logError();
							return;
						}

						if (userMsg.mExtraData == null) {
							DTLog.logError();
							return;
						}

						double progress = 0;

						try {
							progress = userMsg.mExtraData.getDouble("progress");
						} catch (JSONException e) {
							e.printStackTrace();
						}

						if (sOnReceiveBitmapListener != null) {
							sOnReceiveBitmapListener.onReceiveBitmapProgress(progress,
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}

						if (sListener != null) {
							sListener.onReceiveBitmapProgress(progress,
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("IMReceiveBitmap",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (!userMsg.mIsRecv) {
							DTLog.logError();
							return;
						}

						if (userMsg.mUserMsgType != UserMsgType.Photo) {
							DTLog.logError();
							return;
						}

						if (getLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMPrivateMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() != IMPrivateMyself.getInstance()
								.getUID()) {
							DTLog.logError();
							return;
						}

						String fileID = userMsg.getFileID();

						if (!IMParamJudge.isFileIDLegal(fileID)) {
							DTLog.logError();
							return;
						}

						IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(fileID);

						if (photo.getBitmap() == null) {
							DTLog.logError();
							return;
						}

						if (sOnReceiveBitmapListener != null) {
							sOnReceiveBitmapListener.onReceiveBitmap(photo.getBitmap(),
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}

						if (sListener != null) {
							sListener.onReceiveBitmap(photo.getBitmap(),
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}
					}
				});
	}

	public static void setOnLoginStatusChangedListener(OnLoginStatusChangedListener l) {
		sLoginStatusChangedListener = l;
	}

}
