package am.imsdk.model.a2;

import imsdk.data.localchatmessagehistory.IMChatMessage;
import am.dtlib.model.b.log.DTLog;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import am.imsdk.model.im.IMUserMsgHistory;
import android.text.TextUtils;

public final class IMMyselfLocalChatMessageHistory2 {
	private static String sLastError;

	public static IMChatMessage getChatMessage(String customUserID, int index) {
		return getChatMessage(customUserID, null, index);
	}
	
	public static IMChatMessage getChatMessage(String customUserID, String tag, int index) {
		if (index < 0) {
			sLastError = "Illegal Index";
			return null;
		}

		if (index >= getChatMessageCount(customUserID)) {
			sLastError = "Index Over Range";
			return null;
		}

		IMUserMsgHistory history;
		if(TextUtils.isEmpty(tag)) {
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(customUserID);
		} else {
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(customUserID, tag);
		}

		if (index >= history.mAryUserMsgKeys.size()) {
			DTLog.logError();
			return null;
		}

		IMUserMsg userMsg = history.getUserMsg(index);

		if (userMsg == null) {
			DTLog.logError();
			return null;
		}

		return userMsg.getUserChatMessage();
	}

	public static IMChatMessage getLastChatMessage(String customUserID) {
		if (getChatMessageCount(customUserID) == 0) {
			return null;
		} else {
			return getChatMessage(customUserID, 0);
		}
	}
	
	public static long getChatMessageCount(String customUserID) {
		return getChatMessageCount(customUserID, null);
	}

	public static long getChatMessageCount(String customUserID, String tag) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return 0;
		}

		if (!IMPrivateRecentContacts.getInstance().contains(customUserID)) {
			return 0;
		}

		IMUserMsgHistory history;
		if(TextUtils.isEmpty(tag)) {
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(customUserID);
		} else {
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(customUserID, tag);
		}

		return history.mAryUserMsgKeys.size();
	}
	
	public static void removeAllChatMessage(String customUserID) {
		removeAllChatMessage(customUserID, null);
	}
	
	public static void removeAllChatMessage(String customUserID, String tag) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return;
		}
		
		IMUserMsgHistory history;
		if(TextUtils.isEmpty(tag)) {
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(customUserID);
		} else {
			history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(customUserID, tag);
		}

		history.removeFile();
	}

	public static String getLastError() {
		return sLastError;
	}
}
