package am.imsdk.model;

import am.dtlib.model.c.tool.DTLocalModel;

public class IMChatSetting extends DTLocalModel {
	
	private boolean mMsgVibrate = true;
	private boolean mMsgSound = true;
	
	public boolean isMsgVibrate() {
		return mMsgVibrate;
	}

	public void setMsgVibrate(boolean mMsgVibrate) {
		this.mMsgVibrate = mMsgVibrate;
	}

	public boolean isMsgSound() {
		return mMsgSound;
	}

	public void setMsgSound(boolean mMsgSound) {
		this.mMsgSound = mMsgSound;
	}

	// singleton
	private volatile static IMChatSetting sSingleton;

	private IMChatSetting() {
		mLocalFileName = "ICS";
		this.readFromFile();
	}

	public static IMChatSetting getInstance() {
		if(sSingleton == null) {
			synchronized (IMChatSetting.class) {
				sSingleton = new IMChatSetting();
			}
		}
		
		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
		synchronized (IMChatSetting.class) {
			sSingleton = new IMChatSetting();
		}
	}

}
