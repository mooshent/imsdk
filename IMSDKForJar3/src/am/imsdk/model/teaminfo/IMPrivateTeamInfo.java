package am.imsdk.model.teaminfo;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.model.IMBaseUsersMgr;
import android.util.SparseArray;

public final class IMPrivateTeamInfo extends IMBaseUsersMgr {
	public IMPrivateTeamInfo() {
		addDirectory("IPTI");
		addDecryptedDirectory("IMPrivateTeamInfo");
	}

	public enum TeamType {
		Default(0), Group(1);

		private final int value;

		private TeamType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		private static final SparseArray<TeamType> sValuesArray = new SparseArray<TeamType>();

		static {
			for (TeamType type : TeamType.values()) {
				sValuesArray.put(type.value, type);
			}
		}

		public static TeamType fromInt(int i) {
			TeamType type = sValuesArray.get(Integer.valueOf(i));

			if (type == null) {
				DTLog.logError();
				return null;
			}

			return type;
		}
	}

	public long mTeamID;
	public TeamType mTeamType = TeamType.Default;
	public String mTeamName = "";
	public long mFounderUID;
	public long mOwnerUID;
	public String mCoreInfo = "";
	public String mExInfo = "";
	public long mMaxCount;

	@Override
	public boolean generateLocalFullPath() {
		if (mTeamID == 0) {
			return false;
		}

		mDecryptedLocalFileName = mTeamID + "";
		mLocalFileName = DTTool.getSecretString(mTeamID);
		return true;
	}

	public void parseServerData(JSONObject jsonObjectTeamInfo) throws JSONException {
		if (mTeamID == 0) {
			DTLog.logError();
			return;
		}

		long teamID = jsonObjectTeamInfo.getLong("teamid");

		if (teamID == 0) {
			DTLog.logError();
			return;
		}

		if (teamID != mTeamID) {
			DTLog.logError();
			return;
		}

		mTeamType = TeamType.fromInt(jsonObjectTeamInfo.getInt("teamtype"));
		mTeamName = jsonObjectTeamInfo.getString("teamname");
		mFounderUID = jsonObjectTeamInfo.getLong("uid");

		if (mFounderUID == 0) {
			DTLog.logError();
			return;
		}

		mOwnerUID = mFounderUID;

		if (jsonObjectTeamInfo.has("coreinfo")) {
			String tempCoreInfo = jsonObjectTeamInfo.getString("coreinfo");

			if (tempCoreInfo.length() > 0) {
				mCoreInfo = DTTool.getBase64DecodedString(tempCoreInfo);
			} else {
				mCoreInfo = "";
			}
		}

		if (jsonObjectTeamInfo.has("exinfo")) {
			String tempExInfo = jsonObjectTeamInfo.getString("exinfo");

			if (tempExInfo.length() > 0) {
				mExInfo = DTTool.getBase64DecodedString(tempExInfo);
			}
		}

		mMaxCount = jsonObjectTeamInfo.getLong("maxcount");

		if (mMaxCount == 0) {
			DTLog.logError();
			return;
		}
	}
}
