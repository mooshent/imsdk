package am.imsdk.aacmd;

import am.dtlib.model.b.log.DTLog;
import android.util.SparseArray;

public enum IMCmdType {
	// 推送包
	IM_CMD_PUSH_OFFSET(5000),

	// 四大系统
	IM_CMD_USER_SYSTEM(10000), IM_CMD_IM_SYSTEM(30000), IM_CMD_AROUND_SYSTEM(40000), IM_CMD_TEAM_SYSTEM(
			50000),

	// 用户系统
	// 请求/应答包
	IM_CMD_USER_HELLO(IM_CMD_USER_SYSTEM.getValue() + 100), // 10100
	IM_CMD_USER_REGISTER(IM_CMD_USER_SYSTEM.getValue() + 101), // 10101
	IM_CMD_USER_LOGIN(IM_CMD_USER_SYSTEM.getValue() + 102), // 10102
	IM_CMD_USER_SET_DEVICE_TOKEN(IM_CMD_USER_SYSTEM.getValue() + 103), // 10103
	IM_CMD_USER_SET_INFO(IM_CMD_USER_SYSTEM.getValue() + 104), // 10104
	IM_CMD_USER_GET_INFO(IM_CMD_USER_SYSTEM.getValue() + 105), // 10105
	IM_CMD_USER_GET_UID(IM_CMD_USER_SYSTEM.getValue() + 106), // 10106
	IM_CMD_USER_MODIFY_PASSWORD(IM_CMD_USER_SYSTEM.getValue() + 107), // 10107
	IM_CMD_USER_BIND_PHONENUM(IM_CMD_USER_SYSTEM.getValue() + 108), // 10108
	IM_CMD_USER_RETRIEVE_PASSWORD(IM_CMD_USER_SYSTEM.getValue() + 109), // 10109
//	IM_CMD_USER_PURE(IM_CMD_USER_SYSTEM.getValue() + 111), // 10111
	IM_CMD_USER_GET_APP_INFO(IM_CMD_USER_SYSTEM.getValue() + 1000), // 11000
	IM_CMD_USER_GET_SETUP_ID(IM_CMD_USER_SYSTEM.getValue() + 1001), // 11001
	IM_CMD_USER_SET_SETUP_ID(IM_CMD_USER_SYSTEM.getValue() + 1002), // 11002
	IM_CMD_USER_GET_CUSTOMER_SERVICE_LIST(IM_CMD_USER_SYSTEM.getValue() + 1003), // 11003
	IM_CMD_USER_GET_CUSTOMER_SERVICE_INFO(IM_CMD_USER_SYSTEM.getValue() + 1004), // 11004

	// 推送包
	IM_PUSH_CMD_USER_LOGIN_CONFLICT(IM_CMD_USER_SYSTEM.getValue()
			+ IM_CMD_PUSH_OFFSET.getValue() + 100), // 15100

	// 周边系统
	// 请求/应答包
	IM_CMD_AROUND_GET_AROUND_USER_LOCATION(IM_CMD_AROUND_SYSTEM.getValue() + 1), // 40001

	// 小组系统
	// 请求/应答包
	IM_CMD_TEAM_CREATE(IM_CMD_TEAM_SYSTEM.getValue() + 1), // 50001
	IM_CMD_TEAM_DELETE(IM_CMD_TEAM_SYSTEM.getValue() + 2), // 50002
	IM_CMD_TEAM_GET_INFO(IM_CMD_TEAM_SYSTEM.getValue() + 11), // 50011
	IM_CMD_TEAM_GET_ALL(IM_CMD_TEAM_SYSTEM.getValue() + 12), // 50012
	IM_CMD_TEAM_SET_INFO(IM_CMD_TEAM_SYSTEM.getValue() + 21), // 50021
	IM_CMD_TEAM_ADD_MEMBER(IM_CMD_TEAM_SYSTEM.getValue() + 22), // 50022
	IM_CMD_TEAM_REMOVE_MEMBER(IM_CMD_TEAM_SYSTEM.getValue() + 23), // 50023
	IM_CMD_TEAM_GET_MEMBERS(IM_CMD_TEAM_SYSTEM.getValue() + 24), // 50024

	IM_CMD_TEAM_ADD_MEMBER_2_WB(IM_CMD_TEAM_SYSTEM.getValue() + 25), // 50025
	IM_CMD_TEAM_DEL_MEMBER_FROM_WB(IM_CMD_TEAM_SYSTEM.getValue() + 26), // 50026
	IM_CMD_TEAM_GET_MEMBER_FROM_WB(IM_CMD_TEAM_SYSTEM.getValue() + 27), // 50027

	// IM系统
	// 请求/应答包
	IM_CMD_IM_SEND_USER_MSG(IM_CMD_IM_SYSTEM.getValue() + 101), // 30101
	IM_CMD_IM_SEND_TEAM_MSG(IM_CMD_IM_SYSTEM.getValue() + 102), // 30102
	IM_CMD_IM_SYSTEM_MSG_RECEIVED(IM_CMD_IM_SYSTEM.getValue() + 200), // 30200
	IM_CMD_IM_USER_MSG_RECEIVED(IM_CMD_IM_SYSTEM.getValue() + 201), // 30201
	IM_CMD_IM_TEAM_MSG_RECEIVED(IM_CMD_IM_SYSTEM.getValue() + 202), // 30202
	// 推送包
	IM_PUSH_CMD_IM_SYSTEM_MSG(IM_CMD_IM_SYSTEM.getValue()
			+ IM_CMD_PUSH_OFFSET.getValue() + 100), // 35100
	IM_PUSH_CMD_IM_USER_MSG(IM_CMD_IM_SYSTEM.getValue() + IM_CMD_PUSH_OFFSET.getValue()
			+ 101), // 35101
	IM_PUSH_CMD_IM_TEAM_MSG(IM_CMD_IM_SYSTEM.getValue() + IM_CMD_PUSH_OFFSET.getValue()
			+ 102); // 35102

	private final int value;

	private IMCmdType(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	private static final SparseArray<IMCmdType> sValuesArray = new SparseArray<IMCmdType>();

	static {
		for (IMCmdType type : IMCmdType.values()) {
			sValuesArray.put(type.value, type);
		}
	}

	public static IMCmdType fromInt(int i) {
		IMCmdType type = sValuesArray.get(Integer.valueOf(i));

		if (type == null) {
			DTLog.logError();
			return null;
		}

		return type;
	}
}
