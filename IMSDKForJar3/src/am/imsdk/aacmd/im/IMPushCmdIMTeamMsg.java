package am.imsdk.aacmd.im;

import imsdk.data.IMMyself.LoginStatus;
import imsdk.data.localchatmessagehistory.IMGroupChatMessage;

import java.io.UnsupportedEncodingException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTPushCmd;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.aacmd.team.IMCmdTeamGetInfo;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionDoneListener;
import am.imsdk.action.IMAction.OnActionFailedEndListener;
import am.imsdk.action.IMAction.OnActionFailedListener;
import am.imsdk.action.IMAction.OnActionPartiallyDoneListener;
import am.imsdk.action.ammsgs.IMActionRecvTeamMsg;
import am.imsdk.action.fileserver.IMActionDownloadFile;
import am.imsdk.action.group.IMActionGroupGetInfo;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amimteam.IMTeamMsg;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;
import am.imsdk.model.imgroup.IMPrivateRecentGroups;
import am.imsdk.model.imteam.IMTeamChatMsgHistory;
import am.imsdk.model.imteam.IMTeamMsgHistoriesMgr;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.audio.IMAudiosMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;
import android.util.Log;

public final class IMPushCmdIMTeamMsg extends DTPushCmd {
	private static int sMsgCount = 0;

	public IMPushCmdIMTeamMsg() {
		mCmdTypeValue = IMCmdType.IM_PUSH_CMD_IM_TEAM_MSG.getValue();
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		if (++sMsgCount % 5 == 0) {
			DTNotificationCenter.getInstance().postNotification("heartbeat");
		}

		if (IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.Logined) {
			return;
		}

		final IMActionRecvTeamMsg action = new IMActionRecvTeamMsg();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				IMTeamMsg teamMsg = action.mTeamMsg;

				if (teamMsg == null) {
					return;
				}

				if (teamMsg.mTeamID == 0) {
					DTLog.logError();
					return;
				}

				if (!IMParamJudge.isCustomUserIDLegal(teamMsg.mFromCustomUserID)) {
					DTLog.logError();
					return;
				}

				if (teamMsg.mMsgID == 0) {
					DTLog.logError();
					return;
				}

				if (teamMsg.mServerSendTime == 0) {
					DTLog.logError();
					return;
				}

				onRecvGroupMsg(teamMsg);
			}
		};

		action.mRecvJsonObject = recvJsonObject;
		action.begin();
	}

	private void sendTeamMsgReceived(IMTeamMsg teamMsg) {
		if (teamMsg == null) {
			DTLog.logError();
			return;
		}

		if (teamMsg.mMsgID == 0) {
			DTLog.logError();
			return;
		}

		if (teamMsg.getFromUID() == 0) {
			DTLog.logError();
			return;
		}

		IMCmdIMTeamMsgReceived cmd = new IMCmdIMTeamMsgReceived();

		cmd.mMsgID = teamMsg.mMsgID;
		cmd.mTeamID = teamMsg.mTeamID;
		cmd.send();
	}

	private void insertTeamChatMsgHistory(IMTeamMsg teamMsg) {
		if (!IMParamJudge.isCustomUserIDLegal(teamMsg.mFromCustomUserID)) {
			DTLog.logError();
			return;
		}

		if (!teamMsg.mIsRecv) {
			DTLog.logError();
			return;
		}

		if (teamMsg.mMsgID == 0) {
			DTLog.logError();
			return;
		}

		// 维护聊天记录
		IMTeamChatMsgHistory history = IMTeamMsgHistoriesMgr.getInstance()
				.getTeamChatMsgHistory(teamMsg.mTeamID);

		history.insertRecvTeamMsg(teamMsg.mMsgID);
		history.mUnreadMessageCount = history.mUnreadMessageCount + 1;
		history.saveFile();

		DTNotificationCenter.getInstance().postNotification(
				history.getNewMsgNotificationKey(), teamMsg);

		// 维护最近联系Group
		IMPrivateRecentGroups.getInstance().insert(
				DTTool.getGroupIDFromTeamID(teamMsg.mTeamID));
		IMPrivateRecentGroups.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification("recentGroupsChanged");
	}

	private void onRecvGroupMsg(final IMTeamMsg teamMsg) {
		if (IMPrivateMyself.getInstance().getUID() == teamMsg.getFromUID()) {
			// 服务端bug
			DTLog.logError();
			sendTeamMsgReceived(teamMsg);
			return;
		}

		switch (teamMsg.mTeamMsgType) {
		case Normal: {
			if (teamMsg.isLocalFileExist()) {
				sendTeamMsgReceived(teamMsg);
				return;
			}

			if (!teamMsg.saveFile()) {
				DTLog.logError();
				return;
			}

			IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(
					teamMsg.mTeamID);

			if (teamInfo.getUIDList().size() == 0) {
				return;
			}

			sendTeamMsgReceived(teamMsg);

			// 维护聊天记录
			IMTeamChatMsgHistory history = IMTeamMsgHistoriesMgr.getInstance()
					.getTeamChatMsgHistory(teamMsg.mTeamID);

			history.insertRecvTeamMsg(teamMsg.mMsgID);
			history.mUnreadMessageCount++;
			history.saveFile();
			DTNotificationCenter.getInstance().postNotification(
					history.getNewMsgNotificationKey(), teamMsg);

			// 维护最近联系Group
			IMPrivateRecentGroups.getInstance().insert(teamMsg.getGroupID());
			IMPrivateRecentGroups.getInstance().saveFile();
			DTNotificationCenter.getInstance().postNotification(
					"IMMyRecentGroupsDataChanged");

			// 发出通知
			DTNotificationCenter.getInstance().postNotification(
					"IMReceiveTextFromGroup", teamMsg);
		}
			break;
		case Audio: {
			if (teamMsg.mContent == null || teamMsg.mContent.length() == 0) {
				DTLog.logError();
				return;
			}

			// 维护本地数据
			if (teamMsg.isLocalFileExist()) {
				sendTeamMsgReceived(teamMsg);
				return;
			}

			final IMAudio audio = IMAudiosMgr.getInstance().getAudio(
					teamMsg.getFileID());

			if (audio.isLocalFileExist()) {
				sendTeamMsgReceived(teamMsg);
				teamMsg.saveFile();

				// 维护聊天记录 & 最近联系人
				insertTeamChatMsgHistory(teamMsg);

				// 发出通知
				DTNotificationCenter.getInstance().postNotification(
						"IMReceiveTeamAudio", teamMsg);
				return;
			}

			final IMActionDownloadFile action = new IMActionDownloadFile();

			action.mFileID = teamMsg.getFileID();

			action.mOnActionDoneListener = new OnActionDoneListener() {
				@Override
				public void onActionDone() {
					if (teamMsg.isLocalFileExist()) {
						sendTeamMsgReceived(teamMsg);
						return;
					}

					sendTeamMsgReceived(teamMsg);
					teamMsg.saveFile();

					if (!audio.mFileID.equals(teamMsg.getFileID())) {
						DTLog.logError();
						return;
					}

					IMAudiosMgr.getInstance().replaceClientFileID(audio.mFileID,
							action.mFileID);

					audio.mFileID = action.mFileID;
					audio.mBuffer = action.mBuffer;
					audio.saveFile();

					// 维护聊天记录 & 最近联系人
					insertTeamChatMsgHistory(teamMsg);

					// 发出通知
					DTNotificationCenter.getInstance().postNotification(
							"IMReceiveTeamAudioData");
				}
			};

			action.begin();
		}
			break;
		case Photo: {
			if (teamMsg.mContent == null || teamMsg.mContent.length() == 0) {
				DTLog.logError();
				return;
			}

			if (teamMsg.isLocalFileExist()) {
				sendTeamMsgReceived(teamMsg);
				return;
			}

			final IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
					teamMsg.getFileID());

			if (photo.isLocalFileExist()) {
				sendTeamMsgReceived(teamMsg);
				teamMsg.saveFile();

				// 维护聊天记录 & 最近联系人
				insertTeamChatMsgHistory(teamMsg);

				// 发出通知
				DTNotificationCenter.getInstance().postNotification(
						"IMReceiveBitmapMessageFromGroup", teamMsg);

				return;
			}

			sendTeamMsgReceived(teamMsg);
			teamMsg.saveFile();

			// 维护聊天记录 & 最近联系人
			insertTeamChatMsgHistory(teamMsg);

			teamMsg.mStatus = IMGroupChatMessage.SENDING_OR_RECVING;
			DTNotificationCenter.getInstance().postNotification(
					teamMsg.getSendStatusChangedNotificationKey());

			final IMActionDownloadFile action = new IMActionDownloadFile();

			action.mFileID = teamMsg.getFileID();

			// 发出通知
			DTNotificationCenter.getInstance().postNotification(
					"IMReceiveBitmapMessageFromGroup", teamMsg);

			action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
				@Override
				public void onActionPartiallyDone(double percentage) {
					// 发出通知
					DTNotificationCenter.getInstance().postNotification(
							"IMReceiveBitmapProgressFromGroup", teamMsg);
				}
			};

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					if (!photo.mFileID.equals(teamMsg.getFileID())) {
						DTLog.logError();
						return;
					}

					photo.setBuffer(action.mBuffer);
					photo.saveFile();

					teamMsg.mStatus = IMGroupChatMessage.SUCCESS;
					DTNotificationCenter.getInstance().postNotification(
							teamMsg.getSendStatusChangedNotificationKey());

					// 发出通知
					DTNotificationCenter.getInstance().postNotification(
							"IMReceiveBitmapFromGroup", teamMsg);
				}
			};

			action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
				@Override
				public void onActionFailedEnd(String error) {
					teamMsg.mStatus = IMGroupChatMessage.FAILURE;
					DTNotificationCenter.getInstance().postNotification(
							teamMsg.getSendStatusChangedNotificationKey(), teamMsg);
				}
			};

			action.begin();
		}
			break;
		case NormalFileText:
		case CustomFileText: {
			if (!IMParamJudge.isFileIDLegal(teamMsg.getFileID())) {
				DTLog.logError();
				return;
			}

			// 维护本地数据
			if (teamMsg.isLocalFileExist()) {
				sendTeamMsgReceived(teamMsg);
				return;
			}

			final IMActionDownloadFile action = new IMActionDownloadFile();

			action.mFileID = teamMsg.getFileID();

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					if (teamMsg.isLocalFileExist()) {
						sendTeamMsgReceived(teamMsg);
						return;
					}

					String string = "";

					try {
						string = new String(action.mBuffer, "UTF8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
						DTLog.logError();
						return;
					}

					string = DTTool.getBase64DecodedString(string);
					teamMsg.mContent = string;

					if (teamMsg.mTeamMsgType == TeamMsgType.NormalFileText) {
						teamMsg.mTeamMsgType = TeamMsgType.Normal;
					} else {
						teamMsg.mTeamMsgType = TeamMsgType.Custom;
					}

					teamMsg.saveFile();
					sendTeamMsgReceived(teamMsg);

					// 维护聊天记录 & 最近联系人
					insertTeamChatMsgHistory(teamMsg);

					// 发出通知
					DTNotificationCenter.getInstance().postNotification(
							"IMReceiveTextFromGroup", teamMsg);
				}
			};

			action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
				@Override
				public void onActionFailedEnd(String error) {
					sendTeamMsgReceived(teamMsg);
				}
			};

			action.begin();
		}
			break;
		case IMSDKGroupInfoUpdate: {
//			if (teamMsg.isLocalFileExist()) {
//				sendTeamMsgReceived(teamMsg);
//				return;
//			}

			sendTeamMsgReceived(teamMsg);
			teamMsg.saveFile();

			// 刷新 group info
			IMCmdTeamGetInfo cmd = new IMCmdTeamGetInfo();

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					
					JSONArray teamInfoList = jsonObject.getJSONArray("teaminfolist");
					
					if(teamInfoList.length() <= 0) {
						DTLog.logError();
						return;
					}
					
					Log.e("Debug", "GroupInfoUpdate:" + jsonObject.toString());
					
					for (int i = 0; i < teamInfoList.length(); i++) {
						JSONObject mJsonObjectTeamInfo = (JSONObject) teamInfoList.get(i);
						
						if (!(mJsonObjectTeamInfo instanceof JSONObject)) {
							return;
						}
						
						long teamID = mJsonObjectTeamInfo.getLong("teamid");

						if (teamID == 0 || teamID != teamMsg.mTeamID) {
							DTLog.logError();
							return;
						}
						
						IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);
						
						try {
							teamInfo.parseServerData(mJsonObjectTeamInfo);
						} catch (JSONException e) {
							e.printStackTrace();
							DTLog.logError();
							DTLog.log(mJsonObjectTeamInfo.toString());
							return;
						}
						
						teamInfo.saveFile();
					}

					// 发出通知
					DTNotificationCenter.getInstance().postNotification(
							"CustomGroupInfoUpdated", teamMsg);
				}
			};

			cmd.addTeamID(teamMsg.mTeamID);
			cmd.send();
		 }
			break;
		case IMSDKGroupNewUser: {
//			if (teamMsg.isLocalFileExist()) {
//				sendTeamMsgReceived(teamMsg);
//				return;
//			}

			if (teamMsg.getOperationUID() == IMPrivateMyself.getInstance().getUID()) {
				IMActionGroupGetInfo action = new IMActionGroupGetInfo();

				action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
					@Override
					public void onActionDoneEnd() {
						sendTeamMsgReceived(teamMsg);
						teamMsg.saveFile();

						// 刷新数据
						IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance()
								.getTeamInfo(teamMsg.mTeamID);

						if (teamInfo == null) {
							DTLog.logError();
							return;
						}

						IMPrivateMyself.getInstance().addTeamID(teamMsg.mTeamID);
						IMPrivateMyself.getInstance().saveFile();

						// 通知
						DTNotificationCenter.getInstance().postNotification(
								"AddedToGroup");
					}
				};

				action.mGroupID = teamMsg.getGroupID();
				action.begin();

				return;
			}

			sendTeamMsgReceived(teamMsg);
			teamMsg.saveFile();

			// 刷新 group member list
			IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(
					teamMsg.mTeamID);

			teamInfo.add(teamMsg.getOperationUID());
			teamInfo.saveFile();
			DTNotificationCenter.getInstance().postNotification(
					"GroupMemberAdded:" + teamInfo.mTeamID);

			// 无需发通知
		}
			break;
		case IMSDKGroupUserRemoved: {
//			if (teamMsg.isLocalFileExist()) {
//				sendTeamMsgReceived(teamMsg);
//				return;
//			}

			sendTeamMsgReceived(teamMsg);
			teamMsg.saveFile();

			// 刷新 group member list
			IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(
					teamMsg.mTeamID);

			teamInfo.remove(teamMsg.getOperationUID());
			teamInfo.saveFile();

			DTNotificationCenter.getInstance().postNotification(
					"GroupMemberRemoved:" + teamInfo.mTeamID);

			// 无需发通知
		}
			break;
		case IMSDKGroupDeleted: {
//			if (teamMsg.isLocalFileExist()) {
//				sendTeamMsgReceived(teamMsg);
//				return;
//			}

			sendTeamMsgReceived(teamMsg);
			teamMsg.saveFile();

			// 刷新 team & groups
			IMPrivateMyself.getInstance().removeTeam(teamMsg.mTeamID);

			// 发出通知
			// 未完成 by lyc
		}
			break;
		case IMSDKGroupQuit: {
			if (teamMsg.isLocalFileExist()) {
				sendTeamMsgReceived(teamMsg);
				return;
			}

			sendTeamMsgReceived(teamMsg);
			teamMsg.saveFile();

			// 刷新 group member list
			IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(
					teamMsg.mTeamID);

			teamInfo.remove(teamMsg.getFromUID());
			teamInfo.saveFile();
			DTNotificationCenter.getInstance().postNotification(
					"GroupMemberRemoved:" + teamInfo.mTeamID,
					teamMsg.getOperationCustomUserID());

			// 发出通知
			// 未完成 by lyc
		}
			break;
		default:
			break;
		}
	}
}
