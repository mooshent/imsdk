package am.imsdk.aacmd.im;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;

public final class IMCmdIMTeamMsgReceived extends DTCmd {
	public long mMsgID;
	public long mTeamID;
	
	public IMCmdIMTeamMsgReceived() {
		mCmdTypeValue = IMCmdType.IM_CMD_IM_TEAM_MSG_RECEIVED.getValue();
	}
	
	@Override
	public void initSendJsonObject() throws JSONException {
		if (mMsgID == 0) {
			DTLog.logError();
			return;
		}
		
		if (mTeamID == 0) {
			DTLog.logError();
			return;
		}

		mSendJsonObject.put("msgid", mMsgID);
		mSendJsonObject.put("teamid", mTeamID);
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		// TODO Auto-generated method stub
		
	}
}
