package am.imsdk.aacmd.im;

import imsdk.data.IMMyself;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMTeamsMgr;

public final class IMCmdIMSendTeamMsg extends DTCmd {
	public TeamMsgType mTeamMsgType = TeamMsgType.Normal;
	public long mToTeamID;
	public String mContent = "";
	public boolean mNeedAPNS;
	
	public IMCmdIMSendTeamMsg() {
		mCmdTypeValue = IMCmdType.IM_CMD_IM_SEND_TEAM_MSG.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mToTeamID == 0) {
			DTLog.logError();
			return;
		}

		if (mContent == null) {
			mContent = "";
		}

		mSendJsonObject.put("msgtype", mTeamMsgType.getValue());
		mSendJsonObject.put("toteamid", mToTeamID);
		mSendJsonObject.put("msgcontent", mContent);

		if (mNeedAPNS) {
			IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance()
					.getTeamInfo(mToTeamID);

			mSendJsonObject.put("apnstext", IMMyself.getCustomUserID() + " 在 "
					+ teamInfo.mTeamName + " 群里发了一条消息");
		}
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	@Override
	public void onSendFailed() {
	}

	@Override
	public void onNoRecv() {
	}
}
