package am.imsdk.aacmd.user;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMPrivateMyself;

public final class IMCmdUserSetInfo extends DTCmd {
	public String mBaseInfo;
	public String mCustomUserInfo;
	
	public IMCmdUserSetInfo() {
		mCmdTypeValue = IMCmdType.IM_CMD_USER_SET_INFO.getValue();
	}
	
	@Override
	public void initSendJsonObject() throws JSONException {
		if (mCustomUserInfo == null && mBaseInfo == null) {
			DTLog.logError();
			return;
		}
		
		if (mBaseInfo != null) {
			mSendJsonObject.put("baseinfo", DTTool.getBase64EncodedString(mBaseInfo));
		}
		
		if (mCustomUserInfo != null) {
			mSendJsonObject.put("exinfo", DTTool.getBase64EncodedString(mCustomUserInfo));
		}
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		if (IMPrivateMyself.getInstance().getUID() != getSenderUID()) {
			return;
		}

		if (mBaseInfo != null) {
			IMPrivateMyself.getInstance().setBaseInfo(mBaseInfo);
		}
		
		if (mCustomUserInfo != null) {
			IMPrivateMyself.getInstance().mExInfo = mCustomUserInfo;
		}
		
		IMPrivateMyself.getInstance().saveFile();
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	@Override
	public void onSendFailed() {
	}

	@Override
	public void onNoRecv() {
	}
}
