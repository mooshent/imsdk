package am.imsdk.aacmd.user;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMAppSettings;

public final class IMCmdUserGetSetupID extends DTCmd {
	public IMCmdUserGetSetupID() {
		mCmdTypeValue = IMCmdType.IM_CMD_USER_GET_SETUP_ID.getValue();
	}
	
	@Override
	public void initSendJsonObject() throws JSONException {
	}
	
	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		long setupID = recvJsonObject.getLong("setupid");
		
		if (setupID == 0) {
			DTLog.logError();
			return;
		}
		
		if (IMAppSettings.getInstance().mSetupID != 0) {
			return;
		}
		
		IMAppSettings.getInstance().mSetupID = setupID;
		IMAppSettings.getInstance().saveFile();
		
		if (IMAppSettings.getInstance().mSetupID == 0) {
			DTLog.logError();
			return;
		}
		
		IMCmdUserSetSetupID cmd = new IMCmdUserSetSetupID();
		
		cmd.mSetupID = IMAppSettings.getInstance().mSetupID;
		cmd.send();
	}
}
