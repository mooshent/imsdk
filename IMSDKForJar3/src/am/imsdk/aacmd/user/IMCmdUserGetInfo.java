package am.imsdk.aacmd.user;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.userinfo.IMPrivateUserInfo;
import am.imsdk.model.userinfo.IMUsersMgr;
import android.util.Log;

public final class IMCmdUserGetInfo extends DTCmd {
	public IMCmdUserGetInfo() {
		mCmdTypeValue = IMCmdType.IM_CMD_USER_GET_INFO.getValue();
	}

	public void addUID(long uid) {
		Log.e("Debug", uid + "");
		if (0 == uid) {
			DTLog.logError();
			return;
		}

		for (Long uidObject : mAryUIDs) {
			if (!(uidObject instanceof Long)) {
				DTLog.logError();
				return;
			}

			if (uidObject.longValue() == uid) {
				return;
			}
		}

		mAryUIDs.add(Long.valueOf(uid));
	}

	public void addProperty(String property) {
		for (String string : mAryProperties) {
			if (!(string instanceof String)) {
				DTLog.logError();
				return;
			}

			if (string.equals(property)) {
				return;
			}
		}

		mAryProperties.add(property);
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mAryUIDs.size() == 0) {
			DTLog.logError();
			return;
		}

		if (mAryProperties.size() == 0) {
			DTLog.logError();
			return;
		}

		mSendJsonObject.put("uidlist", new JSONArray(mAryUIDs));
		mSendJsonObject.put("propertylist", new JSONArray(mAryProperties));
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		JSONArray jsonArray = recvJsonObject.getJSONArray("infolist");

		if (!(jsonArray instanceof JSONArray)) {
			DTLog.logError();
			return;
		}

//		if (jsonArray.length() != mAryUIDs.size()) {
//			DTLog.logError();
//			return;
//		}

		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject object;

			try {
				object = jsonArray.getJSONObject(i);
			} catch (JSONException e) {
				e.printStackTrace();
				DTLog.logError();
				DTLog.log(e.toString());
				return;
			}

			if (!(object instanceof JSONObject)) {
				DTLog.logError();
				return;
			}

			long uid = 0;

			try {
				uid = object.getLong("uid");
			} catch (JSONException e) {
				e.printStackTrace();
				DTLog.logError();
				DTLog.log(e.toString());
				return;
			}
			
			IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(uid);
			
			if(userInfo == null) {
				continue;
			}
			
			boolean needSaveUserInfo = false;

			if (mAryProperties.contains("phonenum")) {
//				String customUserID = "";
//
//				try {
//					customUserID = object.getString("phonenum");
//				} catch (JSONException e) {
//					e.printStackTrace();
//					DTLog.logError();
//					DTLog.log(e.toString());
//					return;
//				}
//
//				if (customUserID.length() == 0) {
//					DTLog.logError();
//					return;
//				}
//
//				String localCustomUserID = IMUsersMgr.getInstance()
//						.getCustomUserID(uid);
//
//				if (localCustomUserID.length() > 0) {
//					if (!customUserID.equals(localCustomUserID)) {
//						DTLog.logError();
//						return;
//					}
//				} else {
//					IMUsersMgr.getInstance().setCustomUserIDForUID(customUserID, uid);
//					IMUsersMgr.getInstance().saveFile();
//				}
				IMUsersMgr.getInstance().setCustomUserIDForUID(uid + "", uid);
				IMUsersMgr.getInstance().saveFile();
			}

			if (mAryProperties.contains("baseinfo")) {
				String baseInfo = null;

				try {
					baseInfo = object.getString("baseinfo");
				} catch (JSONException e) {
					e.printStackTrace();
					DTLog.logError();
					DTLog.log(e.toString());
					return;
				}

				if (baseInfo != null) {
					baseInfo = DTTool.getBase64DecodedString(baseInfo);
					userInfo.setBaseInfo(baseInfo);
					needSaveUserInfo = true;
				}
			}

			if (mAryProperties.contains("exinfo")) {
				String exInfo = null;

				try {
					exInfo = object.getString("exinfo");
				} catch (JSONException e) {
				}

				if (exInfo == null) {
					exInfo = "";
				}

				if (exInfo.length() > 0) {
					exInfo = DTTool.getBase64DecodedString(exInfo);
				}

				userInfo.mExInfo = exInfo;
				userInfo.saveFile();

				DTNotificationCenter.getInstance().postNotification(
						"CustomUserInfoUpdated", userInfo.getCustomUserID());
			}

			if (needSaveUserInfo) {
				userInfo.saveFile();
			}
		}
	}

	private ArrayList<Long> mAryUIDs = new ArrayList<Long>();
	private ArrayList<String> mAryProperties = new ArrayList<String>();
}
