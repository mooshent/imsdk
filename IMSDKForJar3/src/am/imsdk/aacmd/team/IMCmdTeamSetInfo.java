package am.imsdk.aacmd.team;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMTeamsMgr;

public final class IMCmdTeamSetInfo extends DTCmd {
	public long mTeamID;
	public String mTeamName = null;
	public String mCoreInfo = null;
	public String mExInfo = null;
	
	public IMCmdTeamSetInfo() {
		mCmdTypeValue = IMCmdType.IM_CMD_TEAM_SET_INFO.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mTeamID == 0) {
			DTLog.logError();
			return;
		}
		
		if (mTeamName == null && mCoreInfo == null && mExInfo == null) {
			DTLog.logError();
			return;
		}
		
		mSendJsonObject.put("teamid", mTeamID);
		
		if (mTeamName != null) {
			mSendJsonObject.put("teamname", mTeamName);
		}

		if (mCoreInfo != null) {
			mSendJsonObject.put("coreinfo", mCoreInfo);
		}

		if (mExInfo != null) {
			mSendJsonObject.put("exinfo", mExInfo);
		}
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(mTeamID);

		if (mTeamName != null) {
			teamInfo.mTeamName = mTeamName;
		}

		if (mCoreInfo != null) {
			teamInfo.mCoreInfo = mCoreInfo;
		}

		if (mExInfo != null) {
			teamInfo.mExInfo = mExInfo;
		}
		
		teamInfo.saveFile();
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	@Override
	public void onSendFailed() {
	}

	@Override
	public void onNoRecv() {
	}
}
