package am.imsdk.aacmd.team;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo.TeamType;

public final class IMCmdTeamCreate extends DTCmd {
	public IMPrivateTeamInfo.TeamType mTeamType = TeamType.Default;
	public String mTeamName = "";
	
	public IMCmdTeamCreate() {
		mCmdTypeValue = IMCmdType.IM_CMD_TEAM_CREATE.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		mSendJsonObject.put("teamtype", mTeamType.getValue());
		mSendJsonObject.put("teamname", mTeamName);
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		if (IMPrivateMyself.getInstance().getUID() != getSenderUID()) {
			return;
		}
		
		long teamID = recvJsonObject.getLong("teamid");
		
		if (teamID == 0) {
			DTLog.logError();
			return;
		}
		
		long maxCount = recvJsonObject.getLong("maxcount");
		IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);
		
		teamInfo.mTeamType = mTeamType;
		teamInfo.mTeamName = mTeamName;
		teamInfo.mFounderUID = getSenderUID();
		teamInfo.mOwnerUID = getSenderUID();
		teamInfo.mMaxCount = maxCount;
		
		teamInfo.setUID(getSenderUID());
		teamInfo.add(getSenderUID());
		teamInfo.saveFile();

		IMPrivateMyself.getInstance().addTeamID(teamID);
		IMPrivateMyself.getInstance().saveFile();
	}
}
