package am.imsdk.aacmd.team;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMPrivateMyself;

public class IMCmdTeamGetMembers extends DTCmd {
	public long mTeamID;

	public IMCmdTeamGetMembers() {
		mCmdTypeValue = IMCmdType.IM_CMD_TEAM_GET_MEMBERS.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mTeamID == 0) {
			DTLog.logError();
			return;
		}

		if (IMPrivateMyself.getInstance().getUID() == 0) {
			DTLog.logError();
			return;
		}
		
		mSendJsonObject.put("teamid", mTeamID);
		mSendJsonObject.put("uid", getSenderUID());
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
	}

	@Override
	public void onSendFailed() {
	}

	@Override
	public void onNoRecv() {
	}
}
