package am.imsdk.aacmd.team;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMPrivateMyself;

public final class IMCmdTeamDelete extends DTCmd {
	public long mTeamID;

	public IMCmdTeamDelete() {
		mCmdTypeValue = IMCmdType.IM_CMD_TEAM_DELETE.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mTeamID == 0) {
			DTLog.logError();
			return;
		}

		mSendJsonObject.put("teamid", mTeamID);
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		IMPrivateMyself.getInstance().removeTeam(mTeamID);
		IMPrivateMyself.getInstance().saveFile();
	}
}
