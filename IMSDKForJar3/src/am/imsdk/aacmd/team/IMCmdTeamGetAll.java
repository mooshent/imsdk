package am.imsdk.aacmd.team;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;

public final class IMCmdTeamGetAll extends DTCmd {
	public long mUID;
	
	public IMCmdTeamGetAll() {
		mCmdTypeValue = IMCmdType.IM_CMD_TEAM_GET_ALL.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mUID == 0) {
			DTLog.logError();
			return;
		}
		
		mSendJsonObject.put("uid", mUID);
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	@Override
	public void onSendFailed() {
	}

	@Override
	public void onNoRecv() {
	}
	
	
}
