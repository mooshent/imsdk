package am.dtlib.model.d;

import java.util.ArrayList;

import am.dtlib.model.c.tool.DTLocalModel;

public final class DTDevice extends DTLocalModel {
	public String mLastSISAddress = "";
	public String mIMAddressFromDomain = "";
	public ArrayList<String> mIMAddressListFromSIS = new ArrayList<String>();
	public String mLastIMAddress = "";

	private DTDevice() {
		mLocalFileName = "DD";
		this.readFromFile();
	}

	// singleton
	private volatile static DTDevice sSingleton;

	public static DTDevice getInstance() {
		if (sSingleton == null) {
			synchronized (DTDevice.class) {
				if (sSingleton == null) {
					sSingleton = new DTDevice();
				}
			}
		}

		return sSingleton;
	}
	// singleton end
}
