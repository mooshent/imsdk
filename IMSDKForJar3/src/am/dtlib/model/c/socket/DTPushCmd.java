package am.dtlib.model.c.socket;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import android.os.Parcel;
import android.os.Parcelable;

public class DTPushCmd extends DTCmd {
	@Override
	public final void initSendJsonObject() {
		DTLog.logError();
	}

	@Override
	public final void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	@Override
	public final void onSendFailed() {
		DTLog.logError();
	}

	@Override
	public final void onNoRecv() {
		DTLog.logError();
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		DTLog.logError();
	}
	
	public DTPushCmd() {
		super();
	}
	
	public DTPushCmd(Parcel in) {
		super(in);
	}

	public static final Parcelable.Creator<DTPushCmd> CREATOR = new Creator<DTPushCmd>() {
		@Override
		public DTPushCmd[] newArray(int size) {
			return new DTPushCmd[size];
		}

		@Override
		public DTPushCmd createFromParcel(Parcel in) {
			return new DTPushCmd(in);
		}
	};
}
