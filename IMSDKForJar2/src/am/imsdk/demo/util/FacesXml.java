package am.imsdk.demo.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedListener;
import am.imsdk.action.IMAction.OnActionPartiallyDoneListener;
import am.imsdk.action.fileserver.IMActionDownloadFile;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Xml;

public class FacesXml {
	public final static String key = "gifLoaded";
	// {/fac:md5.gif}
	public final static int SHOW_IMAGENUM = 63;
	public final static int REAL_IMAGENUM = 61;
	private static LinkedHashMap<String, String> sMapFaceShortNames; // name &
																		// short
																		// name
	private static LinkedHashMap<Integer, String> sMapFaceNames; // position &
																	// name
	public static ConcurrentHashMap<String, Bitmap> face_StaticBitmaps = new ConcurrentHashMap<String, Bitmap>();
	public volatile static int faceH;
	public volatile static int faceW;

	public static LinkedHashMap<String, String> getFacesMap() {
		return sMapFaceShortNames;
	}

	public static LinkedHashMap<Integer, String> getFacesDes() {
	
		return sMapFaceNames;
	}

	public static boolean startParse(InputStream is) {
		if (hasParse()) {
			return true;
		}

		int count = 0;
		int pos = 0;
		int index = 0;

		// 由android.util.Xml创建一个XmlPullParser实例
		XmlPullParser parser = Xml.newPullParser();

		try {
			parser.setInput(is, "UTF-8");
			// 设置输入流 并指明编码方式
			int eventType = parser.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				switch (eventType) {
				case XmlPullParser.START_DOCUMENT:
					// faces = new ArrayList<Face>();
					sMapFaceShortNames= new LinkedHashMap<String, String>();
					sMapFaceNames= new LinkedHashMap<Integer, String>();
					break;
				case XmlPullParser.START_TAG:
					if (parser.getName().equals("Face")) {
						String md5 = parser.getAttributeValue(2);
						String type = parser.getAttributeValue(3);
						String name = "{/fac:" + md5 + "." + type + "}";

						sMapFaceShortNames.put(name, md5 + "." + type);
						sMapFaceNames.put(pos++, name);
					}

					break;
				case XmlPullParser.END_TAG:
					if (parser.getName().equals("Face")) {
						count++;

						// faces.add(face);
						if (count % 21 == 20) {
							sMapFaceShortNames.put("[DEL]" + index,
									"emoticon_delete_button@2x.png");
							sMapFaceNames.put(pos++, "[DEL]" + index++);
							count++;
						}
					}
					break;
				}

				eventType = parser.next();
			}

			return true;
		} catch (XmlPullParserException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean hasParse() {
		if (sMapFaceShortNames != null && sMapFaceShortNames.size() >= SHOW_IMAGENUM
				&& sMapFaceNames != null && sMapFaceNames.size() >= SHOW_IMAGENUM) {
			return true;
		}
		
		return false;
	}

	public static Bitmap getBitmap(String gifName) {
		File file = new File(FileUtils.getGifStorePath(), gifName);
		
		if (!file.exists()) {
			return null;
		}
		
		Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());

		return bitmap;
	}

	public static void downLoadGif(final Context context) {
		SharedPreferences preference = context.getSharedPreferences(FacesXml.key,
				Context.MODE_PRIVATE);
		
		FileUtils.register(context);
		
		File file = new File(FileUtils.getGifStorePath());
		
		FileUtils.unRegister();
		
		if (preference.getBoolean(FacesXml.key, false)) {
			if (file.exists()) {
				int length = file.listFiles().length;
				if (length >= 62) {
					return;
				} else {
					preference.edit().putBoolean(FacesXml.key, false).commit();
				}
			} else {
				preference.edit().putBoolean(FacesXml.key, false).commit();
			}
		} else {
			if (file.exists()) {
				int length = file.listFiles().length;
				if (length >= 62) {
					preference.edit().putBoolean(FacesXml.key, true).commit();
					return;
				}
			}
		}
		
		final IMActionDownloadFile downloadFile = new IMActionDownloadFile();

		downloadFile.mRealFileLength = 582045; // 表情包实际大小
		downloadFile.mTimeoutInterval = 30;

		downloadFile.mFileID = "c00491677735021792154de316703732c531514bce841cfe25872";
		
		downloadFile.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
			@Override
			public void onActionPartiallyDone(double percentage) {
			}
		};

		downloadFile.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				parseGif(downloadFile, context);
			}

		};

		downloadFile.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
			}
		};

		downloadFile.begin();
	}

	private static void parseGif(final IMActionDownloadFile downloadFile,
			Context context) {
		LinkedHashMap<String, Integer> imageUris = null;
		byte[] nuffer = downloadFile.mBuffer;
		String res = null;
		String res1 = null;
		
		try {
			res = new String(nuffer, "UTF-8");
			res1 = res.substring(0, 1402);
			JSONArray jsonArray;
			
			try {
				jsonArray = new JSONArray(res1);
				imageUris = new LinkedHashMap<String, Integer>();
				
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonObject = jsonArray.getJSONObject(i);
					String key = (String) jsonObject.keys().next();
					
					imageUris.put(key, jsonObject.getInt(key));
				}
			} catch (JSONException e) {
				e.printStackTrace();
				DTLog.logError();
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			DTLog.logError();
		}

		if (imageUris == null) {
			return;
		}

		int pos = 1402;
		Iterator<Entry<String, Integer>> iterator = imageUris.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, Integer> entry = iterator.next();
			String key = entry.getKey();
			int value = entry.getValue();
			File file = new File(FileUtils.getGifStorePath(), key);

			if (!file.exists()) {
				file.getParentFile().mkdirs();
			} else {
				pos += value;
				continue;
			}

			FileOutputStream fos = null;

			try {
				fos = new FileOutputStream(file);
				fos.write(nuffer, pos, value);
				pos += value;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (fos != null) {
					try {
						fos.close();
					} catch (IOException e) {
						e.printStackTrace();
						DTLog.logError();
					}
				}
			}
		}

		nuffer = null;
		downloadFile.mBuffer = null;
		SharedPreferences preference = context.getSharedPreferences(FacesXml.key,
				Context.MODE_PRIVATE);
		preference.edit().putBoolean(FacesXml.key, true).commit();
	}
}
