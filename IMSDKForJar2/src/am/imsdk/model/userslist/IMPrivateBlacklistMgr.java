package am.imsdk.model.userslist;

import am.dtlib.model.c.tool.DTTool;
import am.imsdk.model.IMBaseUsersMgr;
import am.imsdk.model.IMPrivateMyself;

public final class IMPrivateBlacklistMgr extends IMBaseUsersMgr {
	public IMPrivateBlacklistMgr() {
		super();
		
		mUID = IMPrivateMyself.getInstance().getUID();
		addDirectory("IB");
		addDecryptedDirectory("IMBlacklist");
		
		if (mUID != 0) {
			readFromFile();
		}
	}
	
	@Override
	public boolean generateLocalFullPath() {
		if (mUID == 0) {
			return false;
		}
		
		mLocalFileName = DTTool.getSecretString(mUID);
		mDecryptedLocalFileName = "" + mUID;
		return true;
	}

	// singleton
	private volatile static IMPrivateBlacklistMgr sSingleton;

	public static IMPrivateBlacklistMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMPrivateBlacklistMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMPrivateBlacklistMgr();
				}
			}
		}

		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
		synchronized (IMPrivateBlacklistMgr.class) {
			sSingleton = new IMPrivateBlacklistMgr();
		}
	}
	// newInstance end
}
