package am.imsdk.model.amim;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.ambase.IMBaseMsgsMgr;
import am.imsdk.model.kefu.IMCustomerServiceMgr;
import am.imsdk.model.userinfo.IMUsersMgr;

public final class IMUserMsgsMgr extends IMBaseMsgsMgr {
	public IMUserMsg getUnsentUserMsg(String toCustomUserID, long clientSendTime) {
		if (mUID == 0) {
			DTLog.logError();
			return null;
		}

		if (IMPrivateMyself.getInstance().getUID() == 0) {
			DTLog.logError();
			return null;
		}

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			DTLog.logError();
			return null;
		}

		String key = clientSendTime + "";
		IMUserMsg userMsg = (IMUserMsg) mMapBaseMsgsUnsent.get(key);

		if (userMsg != null) {
			return userMsg;
		}

		userMsg = new IMUserMsg();

		userMsg.mFromCustomUserID = IMPrivateMyself.getInstance().getCustomUserID();
		userMsg.mToCustomUserID = toCustomUserID;
		userMsg.mClientSendTime = clientSendTime;
		userMsg.readFromFile();
		mMapBaseMsgsUnsent.put(key, userMsg);

		return userMsg;
	}

	public IMUserMsg getSentUserMsg(String toCustomUserID, long msgID) {
		if (mUID == 0) {
			DTLog.logError();
			return null;
		}

		if (mUID != IMPrivateMyself.getInstance().getUID()) {
			DTLog.logError();
			return null;
		}

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			DTLog.logError();
			return null;
		}

		if (msgID == 0) {
			DTLog.logError();
			return null;
		}

		String key = toCustomUserID + "_" + msgID;
		IMUserMsg userMsg = (IMUserMsg) mMapBaseMsgsSent.get(key);

		if (userMsg != null) {
			return userMsg;
		}

		userMsg = new IMUserMsg();

		userMsg.mFromCustomUserID = IMPrivateMyself.getInstance().getCustomUserID();
		userMsg.mToCustomUserID = toCustomUserID;
		userMsg.mMsgID = msgID;
		userMsg.readFromFile();
		mMapBaseMsgsSent.put(key, userMsg);

		return userMsg;
	}

	public IMUserMsg getRecvUserMsg(String fromCustomUserID, long msgID) {
		if (mUID == 0) {
			DTLog.logError();
			return null;
		}

		if (mUID != IMPrivateMyself.getInstance().getUID()) {
			DTLog.logError();
			return null;
		}

		if (!IMParamJudge.isCustomUserIDLegal(fromCustomUserID)) {
			DTLog.logError();
			return null;
		}

		if (msgID == 0) {
			DTLog.logError();
			return null;
		}

		String key = fromCustomUserID + "_" + msgID;
		IMUserMsg userMsg = (IMUserMsg) mMapBaseMsgsRecv.get(key);

		if (userMsg != null) {
			return userMsg;
		}

		userMsg = new IMUserMsg();

		userMsg.mFromCustomUserID = fromCustomUserID;
		userMsg.mMsgID = msgID;
		userMsg.mToCustomUserID = IMPrivateMyself.getInstance().getCustomUserID();
		userMsg.mIsRecv = true;
		userMsg.readFromFile();

		mMapBaseMsgsRecv.put(key, userMsg);

		return userMsg;
	}

	public void moveUnsentUserMsgToBeSent(IMUserMsg userMsg) {
		if (mUID == 0) {
			DTLog.logError();
			return;
		}

		if (mUID != IMPrivateMyself.getInstance().getUID()) {
			DTLog.logError();
			return;
		}

		if (userMsg == null) {
			DTLog.logError();
			return;
		}

		if (userMsg.mIsRecv) {
			DTLog.logError();
			return;
		}

		if (userMsg.mClientSendTime == 0) {
			DTLog.logError();
			return;
		}

		if (userMsg.mMsgID == 0) {
			DTLog.logError();
			return;
		}

		if (!IMParamJudge.isCustomUserIDLegal(userMsg.mToCustomUserID)) {
			DTLog.logError();
			return;
		}

		String oldKey = userMsg.mClientSendTime + "";
		String newKey = userMsg.mToCustomUserID + "_" + userMsg.mMsgID;

		mMapBaseMsgsSent.put(newKey, userMsg);
		mMapBaseMsgsUnsent.remove(oldKey);
	}

	public IMUserMsg getRecvUserMsg(JSONObject jsonObject) {
		if (mUID == 0) {
			DTLog.logError();
			return null;
		}

		if (mUID != IMPrivateMyself.getInstance().getUID()) {
			DTLog.logError();
			return null;
		}

		if (jsonObject == null) {
			DTLog.logError();
			return null;
		}

		if (!(jsonObject instanceof JSONObject)) {
			DTLog.logError();
			return null;
		}

		long fromUID = 0;
		long msgID = 0;

		try {
			fromUID = jsonObject.getLong("fromuid");
			msgID = jsonObject.getLong("msgid");
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			return null;
		}

		if (fromUID == 0) {
			DTLog.logError();
			return null;
		}

		if (msgID == 0) {
			DTLog.logError();
			return null;
		}

		String customUserID = IMCustomerServiceMgr.getInstance().getCustomUserID(
				fromUID);

		if (customUserID.length() == 0) {
			customUserID = IMUsersMgr.getInstance().getCustomUserID(fromUID);
		}

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTLog.logError();
			return null;
		}

		String key = customUserID + "_" + msgID;
		IMUserMsg result = (IMUserMsg) mMapBaseMsgsRecv.get(key);

		if (result != null) {
			return result;
		}

		result = new IMUserMsg(jsonObject);
		mMapBaseMsgsRecv.put(key, result);

		return result;
	}

	// singleton
	private volatile static IMUserMsgsMgr sSingleton;

	public static IMUserMsgsMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMUserMsgsMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMUserMsgsMgr();
				}
			}
		}

		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
		synchronized (IMUserMsgsMgr.class) {
			sSingleton = new IMUserMsgsMgr();
		}
	}
	// newInstance end
}
