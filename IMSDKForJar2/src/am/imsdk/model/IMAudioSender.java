package am.imsdk.model;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedListener;
import am.imsdk.action.ammsgs.IMActionSendTeamMsg;
import am.imsdk.action.ammsgs.IMActionSendUserMsg;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.amim.IMUserMsgsMgr;
import am.imsdk.model.amimteam.IMTeamMsg;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;
import am.imsdk.model.amimteam.IMTeamMsgsMgr;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.im.IMUserChatMsgHistory;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import am.imsdk.model.imgroup.IMPrivateRecentGroups;
import am.imsdk.model.imteam.IMTeamChatMsgHistory;
import am.imsdk.model.imteam.IMTeamMsgHistoriesMgr;
import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.audio.IMAudiosMgr;
import android.media.MediaRecorder;
import android.os.Build;

public final class IMAudioSender {
	public static interface OnAudioSenderListener {
		public void onRecordSuccess();

		public void onRecordFailure(String error);

		public void onSendSuccess();

		public void onSendFailure(String error);
	}

	private static class IMAudioRecorder {
		// begin config
		private String mToCustomUserID;
		private String mToGroupID;
//		private long mBeginActionClientTime;

		// end config
		private boolean mNeedSend;
		private long mStopActionClientTime;
		private OnAudioSenderListener mAudioSenderListener;
		private long mTimeoutInterval = 10;

		// self created
		private MediaRecorder mMediaRecorder;
		private IMAudio mAudio;
		private boolean mIsOver = false;
		private boolean mIsSendingMsg = false;
		private int mAudioReadTryTimes;
		private long mDurationInMilliSeconds;
		private Runnable mOnWriteDataOverRunnable = new Runnable() {
			@Override
			public void run() {
				if (!mAudio.readFromFile()) {
					mAudioReadTryTimes++;

					long maxCount = mTimeoutInterval > 0 ? (mTimeoutInterval * 10 / 3)
							: 3;

					if (mAudioReadTryTimes > maxCount) {
						DTLog.logError();

						if (mAudioSenderListener != null) {
							mAudioSenderListener.onRecordFailure("Timeout");
						}

						return;
					}

					// 0.3秒后重试
					DTAppEnv.getMainHandler()
							.postDelayed(mOnWriteDataOverRunnable, 300);
					return;
				}

				trySendMsg();
			}
		};

		private Runnable mUpdateVolumeRunnable = new Runnable() {
			@Override
			public void run() {
				if (mMediaRecorder == null) {
					return;
				}

				double ratio = (double) mMediaRecorder.getMaxAmplitude() / 1;
				double db = 0;// 分贝

				if (ratio > 1) {
					db = 20 * Math.log10(ratio);
				}

				int result = (int) db;

				if (result > 100) {
					result = 100;
				}

				DTNotificationCenter.getInstance().postNotification("volumeUpdated",
						result);
				DTAppEnv.getMainHandler().postDelayed(mUpdateVolumeRunnable, 200);
				mDurationInMilliSeconds += 200;
			}
		};

		public IMAudioRecorder(long beginActionClientTime) {
//			mBeginActionClientTime = beginActionClientTime;
		}

		private boolean startRecordingToUser(String toCustomUserID) {
			if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
				DTLog.logError();
				return false;
			}

			DTAppEnv.getMainHandler().postDelayed(mUpdateVolumeRunnable, 200);

			mToCustomUserID = toCustomUserID;
			return privateStartRecording();
		}

		private boolean startRecordingToGroup(String toGroupID) {
			if (!IMParamJudge.isGroupIDLegal(toGroupID)) {
				DTLog.logError();
				return false;
			}
			DTAppEnv.getMainHandler().postDelayed(mUpdateVolumeRunnable, 200);
			mToGroupID = toGroupID;
			return privateStartRecording();
		}

		public boolean privateStartRecording() {
			if (!IMParamJudge.isCustomUserIDLegal(mToCustomUserID)
					&& !IMParamJudge.isGroupIDLegal(mToGroupID)) {
				DTLog.logError();
				return false;
			}

			if (mMediaRecorder != null) {
				DTLog.logError();
				return false;
			}

			mMediaRecorder = new MediaRecorder();

			mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);

			if (Build.VERSION.SDK_INT >= 16) {
				mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
			} else {
				DTLog.logError();
				mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
			}

			mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

			byte[] buffer = null;

			mAudio = IMAudiosMgr.getInstance().getAudio(buffer);
			mMediaRecorder.setOutputFile(mAudio.getLocalFullPath());
			mMediaRecorder.setOnErrorListener(null);

			try {
				mMediaRecorder.prepare();
			} catch (IllegalStateException e) {
				e.printStackTrace();
				DTLog.logError();
				DTLog.log(e.toString());
				mMediaRecorder.release();
				mMediaRecorder = null;
				return false;
			} catch (IOException e) {
				e.printStackTrace();
				DTLog.logError();
				DTLog.log(e.toString());
				mMediaRecorder.release();
				mMediaRecorder = null;
				return false;
			}

			mMediaRecorder.start();

			return true;
		}

		public boolean stopRecording(boolean needSend, long timeoutInterval,
				OnAudioSenderListener l, long clientActionTime) {
			mStopActionClientTime = clientActionTime;
			mAudioSenderListener = l;

			if (timeoutInterval > 0) {
				mTimeoutInterval = timeoutInterval;
			}

			if (!mAudio.isLocalFileExist()) {
				DTLog.logError();
				return false;
			}

			mNeedSend = needSend;
			try {
				mMediaRecorder.stop();
				mMediaRecorder.release();
				mMediaRecorder = null;
				
				
				if (!mNeedSend) {
					mIsOver = true;
					
					if (mAudioSenderListener != null) {
						mAudioSenderListener.onRecordSuccess();
					}
					
					return true;
				}
				
				DTAppEnv.getMainHandler().postDelayed(mOnWriteDataOverRunnable, 300);
			} catch (Exception e) {
				// TODO: handle exception
			}
			return true;
		}

		private void trySendMsg() {
			if (mIsSendingMsg || mIsOver) {
				DTLog.logError();
				return;
			}

			if (!mNeedSend) {
				DTLog.logError();
				return;
			}

			if (IMParamJudge.isCustomUserIDLegal(mToCustomUserID)) {
				trySendUserMsg();
			} else {
				if (!IMParamJudge.isGroupIDLegal(mToGroupID)) {
					DTLog.logError();
					return;
				}

				trySendTeamMsg();
			}

			if (mAudioSenderListener != null) {
				mAudioSenderListener.onRecordSuccess();
			}
		}

		private void trySendUserMsg() {
			JSONObject jsonObject = new JSONObject();

			try {
				jsonObject.put("fileID", mAudio.mFileID);
				jsonObject.put("durationInMilliSeconds", mDurationInMilliSeconds);
				jsonObject.put("format", "AMR_NB");
			} catch (JSONException e) {
				e.printStackTrace();
				DTLog.logError();
			}

			// 创建IMUserMsg
			final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
					mToCustomUserID, mStopActionClientTime);

			userMsg.mToCustomUserID = mToCustomUserID;
			userMsg.mContent = jsonObject.toString();
			userMsg.mUserMsgType = UserMsgType.Audio;
			userMsg.saveFile();

			// 维护聊天记录
			final IMUserChatMsgHistory history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(mToCustomUserID);

			history.insertUnsentUserMsg(userMsg.mClientSendTime);
			history.saveFile();
			DTNotificationCenter.getInstance().postNotification(
					history.getNewMsgNotificationKey());

			// 维护最近联系人
			IMPrivateRecentContacts.getInstance().insert(mToCustomUserID);
			IMPrivateRecentContacts.getInstance().saveFile();
			DTNotificationCenter.getInstance().postNotification(
					IMPrivateRecentContacts.getInstance().notificationKey());

			// 发送消息
			IMActionSendUserMsg action = new IMActionSendUserMsg();

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					// 维护聊天记录
					history.replaceUnsentUserMsgToSent(userMsg);
					history.saveFile();

					if (mIsOver) {
						DTLog.logError();
						return;
					}

					if (!mIsSendingMsg) {
						DTLog.logError();
						return;
					}

					mIsSendingMsg = false;
					mIsOver = true;

					if (mAudioSenderListener != null) {
						mAudioSenderListener.onSendSuccess();
					}
				}
			};

			action.mOnActionFailedListener = new OnActionFailedListener() {
				@Override
				public void onActionFailed(String error) {
					if (mIsOver) {
						DTLog.logError();
						return;
					}

					if (!mIsSendingMsg) {
						DTLog.logError();
						return;
					}

					mIsSendingMsg = false;
					mIsOver = true;

					if (mAudioSenderListener != null) {
						mAudioSenderListener.onSendFailure(error);
					}
				}
			};

			mIsSendingMsg = true;
			action.mUserMsg = userMsg;
			action.mTimeoutInterval = mTimeoutInterval;
			action.mBeginTime = mStopActionClientTime;
			action.begin();
		}

		private void trySendTeamMsg() {
			JSONObject jsonObject = new JSONObject();

			try {
				jsonObject.put("fileID", mAudio.mFileID);
				jsonObject.put("durationInMilliSeconds", mDurationInMilliSeconds);
				jsonObject.put("format", "AMR_NB");
			} catch (JSONException e) {
				e.printStackTrace();
				DTLog.logError();
			}

			long teamID = DTTool.getTeamIDFromGroupID(mToGroupID);

			if (teamID == 0) {
				DTLog.logError();
				return;
			}

			// 创建IMTeamMsg
			final IMTeamMsg teamMsg = IMTeamMsgsMgr.getInstance().getUnsentTeamMsg(
					teamID, mStopActionClientTime);

			teamMsg.mTeamID = teamID;
			teamMsg.mContent = jsonObject.toString();
			teamMsg.mTeamMsgType = TeamMsgType.Audio;
			teamMsg.saveFile();

			// 维护聊天记录
			final IMTeamChatMsgHistory history = IMTeamMsgHistoriesMgr.getInstance()
					.getTeamChatMsgHistory(teamID);

			history.insertUnsentTeamMsg(teamMsg.mClientSendTime);
			history.saveFile();
			DTNotificationCenter.getInstance().postNotification(
					history.getNewMsgNotificationKey());

			// 维护最近联系Group
			IMPrivateRecentGroups.getInstance().insert(mToGroupID);
			IMPrivateRecentGroups.getInstance().saveFile();
			DTNotificationCenter.getInstance().postNotification(
					"IMMyRecentGroupsDataChanged");

			// 发送消息
			IMActionSendTeamMsg action = new IMActionSendTeamMsg();

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					// 维护聊天记录
					history.replaceUnsentTeamMsgToSent(teamMsg);
					history.saveFile();

					if (mIsOver) {
						DTLog.logError();
						return;
					}

					if (!mIsSendingMsg) {
						DTLog.logError();
						return;
					}

					mIsSendingMsg = false;
					mIsOver = true;

					if (mAudioSenderListener != null) {
						mAudioSenderListener.onSendSuccess();
					}
				}
			};

			action.mOnActionFailedListener = new OnActionFailedListener() {
				@Override
				public void onActionFailed(String error) {
					if (mIsOver) {
						DTLog.logError();
						return;
					}

					if (!mIsSendingMsg) {
						DTLog.logError();
						return;
					}

					mIsSendingMsg = false;
					mIsOver = true;

					if (mAudioSenderListener != null) {
						mAudioSenderListener.onSendFailure(error);
					}
				}
			};

			mIsSendingMsg = true;
			action.mTeamMsg = teamMsg;
			action.mTimeoutInterval = mTimeoutInterval;
			action.mBeginTime = mStopActionClientTime;
			action.begin();
		}
	}

	private ArrayList<IMAudioRecorder> mAryAudioRecorders = new ArrayList<IMAudioSender.IMAudioRecorder>();

	public boolean startRecordingToUser(String toCustomUserID) {
		IMAudioPlayer.getInstance().stop();

		if (mAryAudioRecorders.size() > 0) {
			DTLog.logError();

			for (IMAudioRecorder audioRecorder : mAryAudioRecorders) {
				audioRecorder.stopRecording(false, 0, null, 0);
			}
		}

		mAryAudioRecorders.clear();

		long actionTime = System.currentTimeMillis() / 1000;

		IMAudioRecorder audioRecorder = new IMAudioRecorder(actionTime);

		if (!audioRecorder.startRecordingToUser(toCustomUserID)) {
			DTLog.logError();
			return false;
		}

		mAryAudioRecorders.add(audioRecorder);

		return true;
	}

	public boolean startRecordingToGroup(String toGroupID) {
		IMAudioPlayer.getInstance().stop();
		if (mAryAudioRecorders.size() > 0) {
			DTLog.logError();

			for (IMAudioRecorder audioRecorder : mAryAudioRecorders) {
				audioRecorder.stopRecording(false, 0, null, 0);
			}
		}

		mAryAudioRecorders.clear();

		long actionTime = System.currentTimeMillis() / 1000;

		IMAudioRecorder audioRecorder = new IMAudioRecorder(actionTime);

		if (!audioRecorder.startRecordingToGroup(toGroupID)) {
			DTLog.logError();
			return false;
		}

		mAryAudioRecorders.add(audioRecorder);

		return true;
	}

	public boolean stopRecording(boolean needSend, long timeoutInterval,
			OnAudioSenderListener l, long clientActionTime) {
		if (mAryAudioRecorders.size() > 1) {
			DTLog.logError();

			for (int i = 0; i < mAryAudioRecorders.size() - 1; i++) {
				IMAudioRecorder audioRecorder = mAryAudioRecorders.get(i);

				if (!(audioRecorder instanceof IMAudioRecorder)) {
					DTLog.logError();
					return false;
				}

				audioRecorder.stopRecording(false, 0, null, 0);
			}

			while (mAryAudioRecorders.size() > 1) {
				mAryAudioRecorders.remove(0);
			}
		}

		if (mAryAudioRecorders.size() != 1) {
			DTLog.logError();
			return false;
		}

		IMAudioRecorder audioRecorder = mAryAudioRecorders.get(0);

		if (!(audioRecorder instanceof IMAudioRecorder)) {
			DTLog.logError();
			return false;
		}

		boolean result = audioRecorder.stopRecording(needSend, timeoutInterval, l,
				clientActionTime);

		mAryAudioRecorders.clear();

		return result;
	}

	// singleton
	private volatile static IMAudioSender sSingleton;

	private IMAudioSender() {
	}

	public static IMAudioSender getInstance() {
		if (sSingleton == null) {
			synchronized (IMAudioSender.class) {
				if (sSingleton == null) {
					sSingleton = new IMAudioSender();
				}
			}
		}

		return sSingleton;
	}

	// singleton end
}
