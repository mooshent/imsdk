package am.imsdk.model;

import imsdk.data.around.IMMyselfAround;

import java.util.ArrayList;

public final class IMPrivateAroundUsers extends IMBaseUsersMgr {
	public IMPrivateAroundUsers() {
		addDirectory("IAUM");
		addDecryptedDirectory("IMAroundUsersMgr");
		addIgnoreField("mState");
		addIgnoreField("mUID");
		mUID = IMPrivateMyself.getInstance().getUID();
		
		if (mUID != 0) {
			readFromFile();
		}
	}

	public IMMyselfAround.State mState = IMMyselfAround.State.Normal;
	public double mLongitude;
	public double mLatitude;
	
	public ArrayList<String> getUsersInLastPage() {
		int currentPageIndex = (mCustomUserIDsList.size() - 1) / 28;
		ArrayList<String> result = new ArrayList<String>();
		
		for (int i = currentPageIndex * 28; i < mCustomUserIDsList.size(); i++) {
			result.add(mCustomUserIDsList.get(i));
		}

		return result;
	}

	// singleton
	private volatile static IMPrivateAroundUsers sSingleton;

	public static IMPrivateAroundUsers getInstance() {
		if (sSingleton == null) {
			synchronized (IMPrivateAroundUsers.class) {
				if (sSingleton == null) {
					sSingleton = new IMPrivateAroundUsers();
				}
			}
		}

		return sSingleton;
	}
	// singleton end

	// newInstance
	public static void newInstance() {
		synchronized (IMPrivateAroundUsers.class) {
			sSingleton = new IMPrivateAroundUsers();
		}
	}
	// newInstance end
}
