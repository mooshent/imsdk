package am.imsdk.model;

import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.model.kefu.IMCustomerServiceMgr;
import am.imsdk.model.userinfo.IMUsersMgr;

public class IMBaseUsersMgr extends DTLocalModel {
	public IMBaseUsersMgr() {
		mLevel = 2;

		addRenameField("mAryUIDs", "mUIDsList");
		addRenameField("mAryCustomUserIDs", "mCustomUserIDsList");
	}

	public boolean add(long uid) {
		if (uid == 0) {
			DTLog.logError();
			return false;
		}

		String customUserID = IMUsersMgr.getInstance().getCustomUserID(uid);

		if (customUserID.length() == 0) {
			DTLog.logError();
			return false;
		}

		if (mUIDsList.size() != mCustomUserIDsList.size()) {
			mUIDsList.clear();
			mCustomUserIDsList.clear();
			DTLog.logError();
			return false;
		}

		if (mUIDsList.contains(uid)) {
			if (!mCustomUserIDsList.contains(customUserID)) {
				DTLog.logError();
				return false;
			}

			return false;
		}

		if (mCustomUserIDsList.contains(customUserID)) {
			DTLog.logError();
			return false;
		}

		mUIDsList.add(uid);
		mCustomUserIDsList.add(customUserID);
		return true;
	}

	public boolean add(String customUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTLog.logError();
			return false;
		}

		long uid = IMUsersMgr.getInstance().getUID(customUserID);

		if (customUserID.length() == 0) {
			DTLog.logError();
			return false;
		}

		if (mUIDsList.size() != mCustomUserIDsList.size()) {
			mUIDsList.clear();
			mCustomUserIDsList.clear();
			DTLog.logError();
			return false;
		}

		if (mUIDsList.contains(uid)) {
			if (!mCustomUserIDsList.contains(customUserID)) {
				DTLog.logError();
				return false;
			}

			return false;
		}

		if (mCustomUserIDsList.contains(customUserID)) {
			DTLog.logError();
			return false;
		}

		mUIDsList.add(uid);
		mCustomUserIDsList.add(customUserID);
		return true;
	}

	public boolean add(ArrayList<Long> aryUIDs) {
		if (aryUIDs.size() == 0) {
			return false;
		}

		if (mUIDsList.size() != mCustomUserIDsList.size()) {
			mUIDsList.clear();
			mCustomUserIDsList.clear();
			DTLog.logError();
			return false;
		}

		boolean added = false;

		for (long uid : aryUIDs) {
			String customUserID = IMUsersMgr.getInstance().getCustomUserID(uid);

			if (customUserID.length() == 0) {
				DTLog.logError();
				return false;
			}

			if (mUIDsList.contains(uid)) {
				if (!mCustomUserIDsList.contains(customUserID)) {
					DTLog.logError();
					return false;
				}

				continue;
			}

			if (mCustomUserIDsList.contains(customUserID)) {
				DTLog.logError();
				return false;
			}

			mUIDsList.add(uid);
			mCustomUserIDsList.add(customUserID);
			added = true;
		}

		return added;
	}

	public boolean insert(String customUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTLog.logError();
			return false;
		}

		if (mCustomUserIDsList.contains(customUserID)) {
			long uid = IMCustomerServiceMgr.getInstance().getUID(customUserID);

			if (uid == 0) {
				uid = IMUsersMgr.getInstance().getUID(customUserID);
			}

			if (!mUIDsList.contains(uid)) {
				DTLog.logError();
				return false;
			}

			mCustomUserIDsList.remove(customUserID);
			mCustomUserIDsList.add(0, customUserID);
			mUIDsList.remove(Long.valueOf(uid));
			mUIDsList.add(0, uid);
			return true;
		}

		long uid = IMCustomerServiceMgr.getInstance().getUID(customUserID);

		if (uid == 0) {
			uid = IMUsersMgr.getInstance().getUID(customUserID);
		}

		if (mUIDsList.contains(uid)) {
			DTLog.logError();
			return false;
		}

		if (uid == 0) {
			DTLog.logError();
			return false;
		}

		mCustomUserIDsList.add(0, customUserID);
		mUIDsList.add(0, uid);
		return true;
	}

	public String getCustomUserID(int index) {
		if (mCustomUserIDsList.size() != mUIDsList.size()) {
			DTLog.logError();
			return "";
		}

		if (index >= mCustomUserIDsList.size()) {
			DTLog.logError();
			return "";
		}

		return mCustomUserIDsList.get(index);
	}

	public long getUID(int index) {
		if (mCustomUserIDsList.size() != mUIDsList.size()) {
			DTLog.logError();
			return 0;
		}

		if (index >= mCustomUserIDsList.size()) {
			DTLog.logError();
			return 0;
		}

		return mUIDsList.get(index).longValue();
	}

	public boolean remove(long uid) {
		if (mUIDsList.size() != mCustomUserIDsList.size()) {
			mUIDsList.clear();
			mCustomUserIDsList.clear();
			DTLog.logError();
			return false;
		}

		if (uid == 0) {
			DTLog.logError();
			return false;
		}

		String customUserID = IMUsersMgr.getInstance().getCustomUserID(uid);

		if (customUserID.length() == 0) {
			DTLog.logError();
			return false;
		}

		if (!mUIDsList.contains(uid)) {
			if (mCustomUserIDsList.contains(customUserID)) {
				DTLog.logError();
				return false;
			}

			return false;
		}

		if (!mCustomUserIDsList.contains(customUserID)) {
			DTLog.logError();
			return false;
		}

		mUIDsList.remove(Long.valueOf(uid));
		mCustomUserIDsList.remove(customUserID);

		if (mUIDsList.size() != mCustomUserIDsList.size()) {
			mUIDsList.clear();
			mCustomUserIDsList.clear();
			DTLog.logError();
			return false;
		}

		return true;
	}

	public boolean remove(String customUserID) {
		if (mUIDsList.size() != mCustomUserIDsList.size()) {
			mUIDsList.clear();
			mCustomUserIDsList.clear();
			DTLog.logError();
			return false;
		}

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTLog.logError();
			return false;
		}

		long uid = IMUsersMgr.getInstance().getUID(customUserID);

		if (uid == 0) {
			DTLog.logError();
			return false;
		}

		if (!mUIDsList.contains(uid)) {
			if (mCustomUserIDsList.contains(customUserID)) {
				DTLog.logError();
				return false;
			}

			return false;
		}

		if (!mCustomUserIDsList.contains(customUserID)) {
			DTLog.logError();
			return false;
		}

		mUIDsList.remove(Long.valueOf(uid));
		mCustomUserIDsList.remove(customUserID);

		if (mUIDsList.size() != mCustomUserIDsList.size()) {
			mUIDsList.clear();
			mCustomUserIDsList.clear();
			DTLog.logError();
			return false;
		}

		return true;
	}

	public boolean contains(long uid) {
		if (uid == 0) {
			DTLog.logError();
			return false;
		}

		return mUIDsList.contains(uid);
	}

	public boolean contains(String customUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTLog.logError();
			return false;
		}

		return mCustomUserIDsList.contains(customUserID);
	}

	public boolean clear() {
		if (mUIDsList.size() != mCustomUserIDsList.size()) {
			mUIDsList.clear();
			mCustomUserIDsList.clear();
			DTLog.logError();
			return false;
		}

		if (mUIDsList.size() == 0) {
			return false;
		}

		mUIDsList.clear();
		mCustomUserIDsList.clear();
		return true;
	}

	public ArrayList<Long> getUIDList() {
		return mUIDsList;
	}

	public ArrayList<String> getCustomUserIDsList() {
		return mCustomUserIDsList;
	}

	public void setUID(long uid) {
		if (mUID == uid) {
			return;
		}

		mUID = uid;

		mUIDsList.clear();
		mCustomUserIDsList.clear();
	}

	public boolean setUIDList(ArrayList<Long> aryUIDs) {
		if (aryUIDs.size() == 0 && mUIDsList.size() == 0) {
			return false;
		}

		if (mUIDsList.size() == aryUIDs.size()) {
			boolean needSet = false;

			for (int i = 0; i < aryUIDs.size(); i++) {
				if (!aryUIDs.get(i).equals(mUIDsList.get(i))) {
					needSet = true;
					break;
				}
			}

			if (!needSet) {
				return false;
			}
		}

		mUIDsList.clear();
		mCustomUserIDsList.clear();

		for (long uid : aryUIDs) {
			if (uid == 0) {
				mUIDsList.clear();
				mCustomUserIDsList.clear();
				DTLog.logError();
				return false;
			}

			String customUserID = IMUsersMgr.getInstance().getCustomUserID(uid);

			if (customUserID.length() == 0) {
				mUIDsList.clear();
				mCustomUserIDsList.clear();
				DTLog.logError();
				return false;
			}

			if (mCustomUserIDsList.contains(customUserID)) {
				mUIDsList.clear();
				mCustomUserIDsList.clear();
				DTLog.logError();
				return false;
			}

			mCustomUserIDsList.add(customUserID);
		}

		mUIDsList.clear();
		mUIDsList.addAll(aryUIDs);

		if (mUIDsList.size() != mCustomUserIDsList.size()) {
			mUIDsList.clear();
			mCustomUserIDsList.clear();
			DTLog.logError();
			return true;
		}

		return true;
	}

	public long getUID() {
		return mUID;
	}

	@Override
	public boolean generateLocalFullPath() {
		if (mUID == 0) {
			return false;
		}

		mLocalFileName = mUID + "";
		mDecryptedLocalFileName = DTTool.getSecretString(mUID);

		return true;
	}

	protected long mUID;
	protected ArrayList<Long> mUIDsList = new ArrayList<Long>();
	protected ArrayList<String> mCustomUserIDsList = new ArrayList<String>();
}
