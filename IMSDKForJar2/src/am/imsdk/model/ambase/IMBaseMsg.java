package am.imsdk.model.ambase;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.amimteam.IMTeamMsg;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;
import am.imsdk.model.userinfo.IMUsersMgr;

public abstract class IMBaseMsg extends DTLocalModel {
	public boolean mIsRecv;
	public String mFromCustomUserID = "";
	public long mClientSendTime;
	public long mServerSendTime;
	public String mContent = "";
	public long mMsgID;
	public JSONObject mExtraData;
	public JSONObject mExtraData2;

	public IMBaseMsg() {
		mLevel = 2;
	}
	
	public String getNickName() {
		if(mExtraData2 == null) {
//			DTLog.logError();
			return "";
		}
		
		String nickName = "";
		
		try {
			nickName = mExtraData2.getString("nickName");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return nickName;
	}

	public long getFromUID() {
		if (!IMParamJudge.isCustomUserIDLegal(mFromCustomUserID)) {
			DTLog.logError();
			return 0;
		}

		return IMUsersMgr.getInstance().getUID(mFromCustomUserID);
	}

	public long getAudioDuration() {
		if ((this instanceof IMUserMsg)) {
			IMUserMsg userMsg = (IMUserMsg) this;

			if (userMsg.mUserMsgType != UserMsgType.Audio) {
				DTLog.logError();
				return 0;
			}
		} else {
			if (!(this instanceof IMTeamMsg)) {
				DTLog.logError();
				return 0;
			}

			IMTeamMsg teamMsg = (IMTeamMsg) this;

			if (teamMsg.mTeamMsgType != TeamMsgType.Audio) {
				DTLog.logError();
				return 0;
			}
		}

		if (mExtraData == null) {
			this.createExtraData();
		}

		if (mExtraData == null) {
			DTLog.logError();
			return 0;
		}

		long result = 0;

		try {
			result = mExtraData.getLong("durationInMilliSeconds");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		result = (result + 500) / 1000;

		if (result == 0) {
			result = 1;
		}

		return result;
	}

	private void createExtraData() {
		if (mExtraData != null) {
			return;
		}

		mExtraData = new JSONObject();

		if (this instanceof IMTeamMsg) {
			IMTeamMsg teamMsg = (IMTeamMsg) this;

			if (!isAudioMsg() && !isPhotoMsg() && !isFileTextMsg()
					&& teamMsg.mTeamMsgType != TeamMsgType.IMSDKGroupNewUser
					&& teamMsg.mTeamMsgType != TeamMsgType.IMSDKGroupUserRemoved) {
				return;
			}
		}

		if (this instanceof IMUserMsg) {
			IMUserMsg userMsg = (IMUserMsg) this;

			if (!isAudioMsg() && !isPhotoMsg() && !isFileTextMsg()
					&& userMsg.mUserMsgType != UserMsgType.IMSDKGroupNoticeBeAdded
					&& userMsg.mUserMsgType != UserMsgType.IMSDKGroupNoticeBeRemoved) {
				return;
			}
		}

		if (mContent == null || mContent.length() == 0) {
			DTLog.logError();
			return;
		}

		try {
			mExtraData = new JSONObject(mContent);
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			return;
		}
	}

	public String getAudioFormat() {
		if (!isAudioMsg()) {
			DTLog.logError();
			return "";
		}

		JSONObject jsonObject = this.getExtraData();

		try {
			String format = jsonObject.getString("format");

			return format;
		} catch (JSONException e) {
			e.printStackTrace();
			return "iLBC";
		}
	}

	public JSONObject getExtraData() {
		if (mExtraData != null) {
			return mExtraData;
		}

		createExtraData();
		return mExtraData;
	}

	public double getPhotoHeight() {
		try {
			return this.getExtraData().getDouble("height");
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
	}

	public double getPhotoWidth() {
		try {
			return this.getExtraData().getDouble("width");
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
	}

	public boolean shouldHasFileID() {
		return isAudioMsg() || isPhotoMsg() || isFileTextMsg();
	}

	public String getFileID() {
		if (!shouldHasFileID()) {
			DTLog.logError();
			return "";
		}

		String fileID;

		try {
			fileID = this.getExtraData().getString("fileID");
		} catch (JSONException e) {
			e.printStackTrace();
			fileID = mContent;
		}

		if (!IMParamJudge.isFileIDLegal(fileID)) {
			DTLog.logError();
			return "";
		}

		return fileID;
	}

	public void setFileID(String fileID) {
		if (!IMParamJudge.isFileIDLegal(fileID)) {
			DTLog.logError();
			return;
		}

		try {
			this.getExtraData().put("fileID", fileID);

			JSONObject jsonObject = new JSONObject(getExtraData().toString());

			jsonObject.put("progress", null);
			mContent = jsonObject.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
		}
	}

	public boolean shouldHasOperationUser() {
		if (!(this instanceof IMTeamMsg)) {
			return false;
		}

		IMTeamMsg teamMsg = (IMTeamMsg) this;

		if (teamMsg.mTeamMsgType == TeamMsgType.IMSDKGroupNewUser
				|| teamMsg.mTeamMsgType == TeamMsgType.IMSDKGroupUserRemoved) {
			return true;
		} else {
			return false;
		}
	}

	public long getOperationUID() {
		if (!shouldHasOperationUser()) {
			DTLog.logError();
			return 0;
		}

		long uid = 0;

		try {
			uid = this.getExtraData().getLong("uid");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (uid != 0) {
			return uid;
		}

		String customUserID = getOperationCustomUserID();

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTLog.logError();
			return 0;
		}

		return IMUsersMgr.getInstance().getUID(customUserID);
	}

	public String getGroupID() {
		if (!(this instanceof IMUserMsg)) {
			if (!(this instanceof IMTeamMsg)) {
				DTLog.logError();
				return "";
			}
			
			IMTeamMsg teamMsg = (IMTeamMsg) this;
			
			return DTTool.getGroupIDFromTeamID(teamMsg.mTeamID);
		}

		IMUserMsg userMsg = (IMUserMsg) this;

		if (userMsg.mUserMsgType != UserMsgType.IMSDKGroupNoticeBeAdded
				&& userMsg.mUserMsgType != UserMsgType.IMSDKGroupNoticeBeRemoved) {
			DTLog.logError();
			return "";
		}

		String groupID = "";

		try {
			groupID = this.getExtraData().getString("groupID");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (IMParamJudge.isGroupIDLegal(groupID)) {
			return groupID;
		}

		if (!IMParamJudge.isGroupIDLegal(mContent)) {
			DTLog.logError();
			return "";
		}

		return mContent;
	}

	public boolean isAudioMsg() {
		if (this instanceof IMUserMsg) {
			IMUserMsg userMsg = (IMUserMsg) this;

			return userMsg.mUserMsgType == UserMsgType.Audio;
		} else {
			if (!(this instanceof IMTeamMsg)) {
				DTLog.logError();
				return false;
			}

			IMTeamMsg teamMsg = (IMTeamMsg) this;

			return teamMsg.mTeamMsgType == TeamMsgType.Audio;
		}
	}

	public boolean isPhotoMsg() {
		if (this instanceof IMUserMsg) {
			IMUserMsg userMsg = (IMUserMsg) this;

			return userMsg.mUserMsgType == UserMsgType.Photo;
		} else {
			if (!(this instanceof IMTeamMsg)) {
				DTLog.logError();
				return false;
			}

			IMTeamMsg teamMsg = (IMTeamMsg) this;

			return teamMsg.mTeamMsgType == TeamMsgType.Photo;
		}
	}

	public boolean isFileTextMsg() {
		if (this instanceof IMUserMsg) {
			IMUserMsg userMsg = (IMUserMsg) this;

			return userMsg.mUserMsgType == UserMsgType.NormalFileText
					|| userMsg.mUserMsgType == UserMsgType.CustomFileText;
		} else {
			if (!(this instanceof IMTeamMsg)) {
				DTLog.logError();
				return false;
			}

			IMTeamMsg teamMsg = (IMTeamMsg) this;

			return teamMsg.mTeamMsgType == TeamMsgType.NormalFileText
					|| teamMsg.mTeamMsgType == TeamMsgType.CustomFileText;
		}
	}

	public String getOperationCustomUserID() {
		if (!shouldHasOperationUser()) {
			DTLog.logError();
			return "";
		}

		String customUserID = "";

		try {
			customUserID = this.getExtraData().getString("customUserID");
		} catch (JSONException e) {
		}

		if (IMParamJudge.isCustomUserIDLegal(customUserID)) {
			return customUserID;
		}

		customUserID = mContent;

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTLog.logError();
			return "";
		}

		return customUserID;
	}
	
	public abstract String getSendStatusChangedNotificationKey();
}
