package am.imsdk.model.im;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;

public final class IMUserChatMsgHistory extends IMUserMsgHistory {
	@Override
	public boolean generateLocalFullPath() {
		if (mUID == 0) {
			return false;
		}

		if (mOppositeCustomUserID.length() == 0) {
			DTLog.logError();
			return false;
		}

		setDirectory(DTTool.getSecretString(mUID), 1);
		setDecryptedDirectory(mUID + "", 1);
		setDirectory(DTTool.getMD5String(mOppositeCustomUserID), 2);
		setDecryptedDirectory(mOppositeCustomUserID, 2);
		mLocalFileName = "IUCMH";
		mDecryptedLocalFileName = "IMUserChatMsgHistory";

		return true;
	}

	public void removeUnsentUserMsgWithClientSendTime(long clientSendTime) {
		if (clientSendTime == 0) {
			DTLog.logError();
			return;
		}

		for (int i = mAryUserMsgKeys.size() - 1; i >= 0; i--) {
			String string = mAryUserMsgKeys.get(i);

			if (!(string instanceof String)) {
				DTLog.logError();
				return;
			}

			if (string.startsWith("R_")) {
				continue;
			}
			
			if (string.startsWith("S_")) {
				continue;
			}

			if (string.equals("" + clientSendTime)) {
				mAryUserMsgKeys.remove(i);
				return;
			}
		}
	}
}
