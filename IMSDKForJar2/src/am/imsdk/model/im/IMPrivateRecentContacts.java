package am.imsdk.model.im;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.model.IMBaseUsersMgr;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.kefu.IMCustomerServiceMgr;
import am.imsdk.model.userinfo.IMUsersMgr;

// 只记录本地数据，只记录CustomUserID
public final class IMPrivateRecentContacts extends IMBaseUsersMgr {
	public IMPrivateRecentContacts() {
		if (IMPrivateMyself.getInstance().getUID() > 0) {
			mUID = IMPrivateMyself.getInstance().getUID();
			addDirectory("IUM");
			addDecryptedDirectory("IMUserMsg");
			addDirectory(DTTool.getSecretString(mUID));
			addDecryptedDirectory("" + mUID);
			mLocalFileName = "irc";
			mDecryptedLocalFileName = "IMRecentContacts";
			this.readFromFile();
		}
	}

	@Override
	public boolean generateLocalFullPath() {
		if (mUID <= 0) {
			return false;
		}

		return true;
	}

	public String notificationKey() {
		return "recentContactsChanged";
	}

	@Override
	public boolean insert(String customUserID) {
		if (mUID == 0) {
			DTLog.logError();
			return false;
		}

		if (mUID != IMPrivateMyself.getInstance().getUID()) {
			DTLog.logError();
			return false;
		}

		if (mUIDsList == null) {
			DTLog.logError();
			return false;
		}

		if (mCustomUserIDsList == null) {
			DTLog.logError();
			return false;
		}

		if (mUIDsList.size() != mCustomUserIDsList.size()) {
			DTLog.logError();
			return false;
		}

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTLog.logError();
			return false;
		}

		// 先判断是否是客服
		long uid = IMCustomerServiceMgr.getInstance().getUID(customUserID);

		if (uid == 0) {
			// 再判断普通用户
			uid = IMUsersMgr.getInstance().getUID(customUserID);
		}

		if (uid == 0) {
			DTLog.logError();
			return false;
		}

		return super.insert(customUserID);
	}

	// singleton
	private volatile static IMPrivateRecentContacts sSingleton;

	public static IMPrivateRecentContacts getInstance() {
		if (sSingleton == null) {
			synchronized (IMPrivateRecentContacts.class) {
				if (sSingleton == null) {
					sSingleton = new IMPrivateRecentContacts();
				}
			}
		}

		return sSingleton;
	}

	// singleton end

	// newInstance
	public static void newInstance() {
		synchronized (IMPrivateRecentContacts.class) {
			sSingleton = new IMPrivateRecentContacts();
		}
	}
	// newInstance end
}
