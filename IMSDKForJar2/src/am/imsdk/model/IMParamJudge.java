package am.imsdk.model;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTTool;

/**
 * 登陆参数是否合法判断工具类
 */
public final class IMParamJudge {
	public static String getLastError() {
		return IMParamJudge.sLastError;
	}

	public static void setLastError(String lastError) {
		IMParamJudge.sLastError = lastError;
	}

	private static String sLastError;

	public static boolean isCustomUserIDLegal(String customUserID) {
		if (customUserID == null) {
			IMParamJudge.sLastError = "phoneNum不能为null";
			return false;
		}

		if (!(customUserID instanceof String)) {
			sLastError = "phoneNum不是String类型";
			return false;
		}

		if (customUserID.length() == 0) {
			IMParamJudge.sLastError = "phoneNum不能为空字符串";
			return false;
		}

		if (customUserID.contains(" ")) {
			IMParamJudge.sLastError = "phoneNum不得包含空格";
			return false;
		}

//		if (customUserID.contains("+86")) {
//			customUserID = customUserID.replace("+86", "");
//		}
//
//		if (customUserID.length() > 11) {
//			IMParamJudge.sLastError = "phoneNum长度不得超过11";
//			return false;
//		}
//
//		if (!customUserID.matches("^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$")) {
//			IMParamJudge.sLastError = "phoneNum不合规范";
//			return false;
//		}

		return true;
	}

	public static boolean isCustomerServiceIDLegal(String customServicePhoneNum) {
		if (customServicePhoneNum == null) {
			IMParamJudge.sLastError = "customServicePhoneNum不能为null";
			return false;
		}

		if (!(customServicePhoneNum instanceof String)) {
			sLastError = "customServicePhoneNum不是String类型";
			return false;
		}

		if (customServicePhoneNum.length() == 0) {
			IMParamJudge.sLastError = "customServicePhoneNum不能为空字符串";
			return false;
		}

		if (customServicePhoneNum.contains(" ")) {
			IMParamJudge.sLastError = "customServicePhoneNum不得包含空格";
			return false;
		}

		if (customServicePhoneNum.length() > 11) {
			IMParamJudge.sLastError = "customServicePhoneNum长度不得超过11";
			return false;
		}

		if (!customServicePhoneNum.matches("^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$")) {
			IMParamJudge.sLastError = "customServicePhoneNum不合规范";
			return false;
		}

		return true;
	}

	public static boolean isCustomUserInfoLegal(String customUserInfo) {
		if (!(customUserInfo instanceof String)) {
			sLastError = "customUserInfo不是String类型";
			return false;
		}

		if (customUserInfo.length() > 40) {
			IMParamJudge.sLastError = "customUserInfo长度不得超过40";
			return false;
		}

		return true;
	}

	public static boolean isPasswordLegal(String password) {
		if (password == null) {
			IMParamJudge.sLastError = "password不能为null";
			return false;
		}

		if (!(password instanceof String)) {
			sLastError = "password不是String类型";
			return false;
		}

		if (password.length() > 32) {
			IMParamJudge.sLastError = "password长度不得超过32";
			return false;
		}

		if (password.length() == 0) {
			IMParamJudge.sLastError = "password不能为空字符串";
			return false;
		}

		return true;
	}

	public static boolean isIMTextLegal(String text) {
		if (text == null) {
			sLastError = "text不能为null";
			return false;
		}

		if (!(text instanceof String)) {
			sLastError = "text不是String类型";
			return false;
		}

		if (text.length() == 0) {
			sLastError = "text不能为空字符串";
			return false;
		}

		if (text.length() > 50000) {
			sLastError = "text内容太长";
			return false;
		}

		return true;
	}

	public static boolean isFriendRequestTextLegal(String text) {
		if (text == null) {
			sLastError = "text不能为null";
			return false;
		}

		if (!(text instanceof String)) {
			sLastError = "text不是String类型";
			return false;
		}

		if (text.length() > 32) {
			sLastError = "text内容长度不能超过32";
			return false;
		}

		return true;
	}

	public static boolean isRejectReasonLegal(String reason) {
		if (reason == null) {
			DTLog.logError();
			return false;
		}

		if (!(reason instanceof String)) {
			sLastError = "reason不是String类型";
			return false;
		}

		if (reason.length() > 32) {
			sLastError = "reason内容长度不能超过32";
			return false;
		}

		return true;
	}

	public static boolean isAppKeyLegal(String appKey) {
//		if (appKey == null) {
//			sLastError = "appKey不能为null";
//			return false;
//		}
//
//		if (!(appKey instanceof String)) {
//			sLastError = "appKey不是String类型";
//			return false;
//		}
//
//		if (appKey.length() == 0) {
//			sLastError = "appKey不能为空字符串";
//			return false;
//		}
//
//		if (appKey.length() != 24) {
//			sLastError = "非法appKey";
//			return false;
//		}
//
//		if (!appKey.matches("^[a-zA-Z0-9]+$")) {
//			sLastError = "非法appKey";
//			return false;
//		}

		return true;
	}

	public static boolean isFileIDLegal(String fileID) {
		if (fileID == null) {
			IMParamJudge.sLastError = "fileID不能为null";
			return false;
		}

		if (!(fileID instanceof String)) {
			sLastError = "fileID不是String类型";
			return false;
		}

		if (fileID.length() > 60) {
			IMParamJudge.sLastError = "fileID长度不得超过60";
			return false;
		}

		if (fileID.length() == 0) {
			IMParamJudge.sLastError = "fileID不能为空字符串";
			return false;
		}

		return true;
	}

	public static boolean isGroupIDLegal(String groupID) {
		if (groupID == null) {
			IMParamJudge.sLastError = "groupID不能为null";
			return false;
		}

		if (!(groupID instanceof String)) {
			sLastError = "groupID不是String类型";
			return false;
		}

		if (groupID.length() == 0) {
			IMParamJudge.sLastError = "groupID不能为空字符串";
			return false;
		}

		if (groupID.length() > 64) {
			IMParamJudge.sLastError = "非法groupID";
			return false;
		}

		if (groupID.length() > 12) {
			IMParamJudge.sLastError = "非法groupID";
			return false;
		}

		long teamID = DTTool.getUnsecretLongValue(groupID);

		if (teamID <= 0) {
			IMParamJudge.sLastError = "非法groupID";
			return false;
		}

		return true;
	}

	public static boolean isGroupNameLegal(String groupName) {
		if (groupName == null) {
			IMParamJudge.sLastError = "groupName不能为null";
			return false;
		}

		if (!(groupName instanceof String)) {
			sLastError = "groupName不是String类型";
			return false;
		}

		if (groupName.length() == 0) {
			IMParamJudge.sLastError = "groupName不能为空字符串";
			return false;
		}

		if (groupName.length() > 32) {
			IMParamJudge.sLastError = "groupName长度不能超过32";
			return false;
		}

		return true;
	}

	public static boolean isTeamIDLegal(long teamID) {
		if (teamID == 0) {
			sLastError = "teamID不能为零";
			return false;
		}

		return true;
	}
}
