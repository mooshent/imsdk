package am.imsdk.model.a2;

import imsdk.data.localchatmessagehistory.IMChatMessage;
import am.dtlib.model.b.log.DTLog;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.im.IMUserChatMsgHistory;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;

public final class IMMyselfLocalChatMessageHistory2 {
	private static String sLastError;

	public static IMChatMessage getChatMessage(String customUserID, int index) {
		if (index < 0) {
			sLastError = "Illegal Index";
			return null;
		}

		if (index >= getChatMessageCount(customUserID)) {
			sLastError = "Index Over Range";
			return null;
		}

		IMUserChatMsgHistory history = IMUserMsgHistoriesMgr.getInstance()
				.getUserChatMsgHistory(customUserID);

		if (index >= history.mAryUserMsgKeys.size()) {
			DTLog.logError();
			return null;
		}

		IMUserMsg userMsg = history.getUserMsg(index);

		if (userMsg == null) {
			DTLog.logError();
			return null;
		}

		return userMsg.getUserChatMessage();
	}

	public static IMChatMessage getLastChatMessage(String customUserID) {
		if (getChatMessageCount(customUserID) == 0) {
			return null;
		} else {
			return getChatMessage(customUserID, 0);
		}
	}

	public static long getChatMessageCount(String customUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return 0;
		}

		if (!IMPrivateRecentContacts.getInstance().contains(customUserID)) {
			return 0;
		}

		IMUserChatMsgHistory history = IMUserMsgHistoriesMgr.getInstance()
				.getUserChatMsgHistory(customUserID);

		return history.mAryUserMsgKeys.size();
	}
	
	public static void removeAllChatMessage(String customUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return;
		}
		
		IMUserChatMsgHistory history = IMUserMsgHistoriesMgr.getInstance()
				.getUserChatMsgHistory(customUserID);

		history.removeFile();
	}

	public static String getLastError() {
		return sLastError;
	}
}
