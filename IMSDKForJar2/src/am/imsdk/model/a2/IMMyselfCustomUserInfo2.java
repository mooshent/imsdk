package am.imsdk.model.a2;

import java.util.Observable;
import java.util.Observer;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnInitializedListener;
import imsdk.data.customuserinfo.IMSDKCustomUserInfo;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.aacmd.user.IMCmdUserSetInfo;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;

public final class IMMyselfCustomUserInfo2 {
	private static String sLastError = "";
	private static OnInitializedListener sOnInitializedListener;

	static {
		DTNotificationCenter.getInstance().addObserver("CustomUserInfoInitialized",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (sOnInitializedListener != null) {
							sOnInitializedListener.onInitialized();
						}
					}
				});
	}

	public static boolean isInitialized() {
		return IMPrivateMyself.getInstance().getCustomUserInfoInitedInFact();
	}

	public static void setOnInitializedListener(OnInitializedListener l) {
		sOnInitializedListener = l;
	}

	public static String get() {
		if (!isInitialized()) {
			sLastError = "CustomUserInfo Module Not Initialized Yet.";
			return "";
		}

		return IMSDKCustomUserInfo.get(IMMyself.getCustomUserID());
	}

	public static long commit(final String customUserInfo, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMParamJudge.isCustomUserInfoLegal(customUserInfo)) {
			if (l != null) {
				l.onFailure(IMParamJudge.getLastError());
			}

			return actionTime;
		}

		IMCmdUserSetInfo cmd = new IMCmdUserSetInfo();

		cmd.mCustomUserInfo = customUserInfo;

		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
			@Override
			public void onCommonFailed(FailedType type, long errorCode,
					JSONObject errorJsonObject) throws JSONException {
				if (l != null) {
					l.onFailure(errorJsonObject == null ? "errorCode:" + errorCode
							: errorJsonObject.toString());
				}
			}
		};

		cmd.mOnRecvEndListener = new OnRecvEndListener() {
			@Override
			public void onRecvEnd(JSONObject jsonObject) throws JSONException {
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		cmd.send();

		return actionTime;
	}

	public static String getLastError() {
		return sLastError;
	}
}
