package am.imsdk.model.a2;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnInitializedListener;
import imsdk.data.IMSDK.OnDataChangedListener;
import imsdk.data.relations.IMMyselfRelations.OnRelationsEventListener;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.aacmd.team.IMCmdTeamAddMember2WB.WBType;
import am.imsdk.aacmd.team.IMCmdTeamDelMemberFromWb;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedEndListener;
import am.imsdk.action.IMAction.OnActionFailedListener;
import am.imsdk.action.ammsgs.IMActionSendUserMsg;
import am.imsdk.action.relation.IMActionAgreeToFriendRequest;
import am.imsdk.action.relation.IMActionDeleteFriendRequest;
import am.imsdk.action.relation.IMActionMoveToBlacklist;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.userinfo.IMUsersMgr;
import am.imsdk.model.userslist.IMPrivateBlacklistMgr;
import am.imsdk.model.userslist.IMPrivateFriendsMgr;

public final class IMMyselfRelations2 {
	private static OnRelationsEventListener sListener;
	private static OnInitializedListener sOnInitializedListener;
	private static OnDataChangedListener sOnDataChangedListener;
	private static OnDataChangedListener sOnFriendsListDataChangedListener;
	private static OnDataChangedListener sOnBlacklistDataChangedListener;

	public static void setOnRelationsEventListener(OnRelationsEventListener l) {
		sListener = l;
	}

	public static void setOnInitializedListener(OnInitializedListener l) {
		sOnInitializedListener = l;
	}

	public static void setOnDataChangedListener(OnDataChangedListener l) {
		sOnDataChangedListener = l;
	}

	public static void setOnFriendsListDataChangedListener(OnDataChangedListener l) {
		sOnFriendsListDataChangedListener = l;
	}

	public static void setOnBlacklistDataChangedListener(OnDataChangedListener l) {
		sOnBlacklistDataChangedListener = l;
	}

	static {
		DTNotificationCenter.getInstance().addObserver("RelationsInitialzed",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (sListener != null) {
							sListener.onInitialized();
						}

						if (sOnInitializedListener != null) {
							sOnInitializedListener.onInitialized();
						}

						if (sOnFriendsListDataChangedListener != null) {
							sOnFriendsListDataChangedListener.onDataChanged();
						}

						if (sOnBlacklistDataChangedListener != null) {
							sOnBlacklistDataChangedListener.onDataChanged();
						}

						if (sOnDataChangedListener != null) {
							sOnDataChangedListener.onDataChanged();
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("ReceiveFriendRequest",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						if (sListener != null) {
							IMUserMsg userMsg = (IMUserMsg) data;

							sListener.onReceiveFriendRequest(userMsg.mContent,
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}
					}
				});
		
		// 接收到对方要解除与你的好友关系请求
		DTNotificationCenter.getInstance().addObserver("ReceiveFriendDeleted",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						if (sListener != null) {
							IMUserMsg userMsg = (IMUserMsg) data;

							sListener.onDeleteFriends(
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}
						
						if (sOnFriendsListDataChangedListener != null) {
							sOnFriendsListDataChangedListener.onDataChanged();
						}

					}
				});
		

		DTNotificationCenter.getInstance().addObserver("ReceiveAgreeToFriendRequest",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						if (sListener != null) {
							IMUserMsg userMsg = (IMUserMsg) data;

							sListener.onReceiveAgreeToFriendRequest(
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("ReceiveRejectToFriendRequest",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						if (sListener != null) {
							IMUserMsg userMsg = (IMUserMsg) data;

							sListener.onReceiveRejectToFriendRequest(userMsg.mContent,
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("BuildFriendshipWithUser",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						if (sListener != null) {
							IMUserMsg userMsg = (IMUserMsg) data;

							IMPrivateFriendsMgr.getInstance().insert(
									userMsg.getOppositeCustomUserID());
							sListener.onBuildFriendshipWithUser(
									userMsg.getOppositeCustomUserID(),
									userMsg.mServerSendTime);
						}

						if (sOnFriendsListDataChangedListener != null) {
							sOnFriendsListDataChangedListener.onDataChanged();
						}

						if (sOnDataChangedListener != null) {
							sOnDataChangedListener.onDataChanged();
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("FriendsListUpdated",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (sOnFriendsListDataChangedListener != null) {
							sOnFriendsListDataChangedListener.onDataChanged();
						}

						if (sOnDataChangedListener != null) {
							sOnDataChangedListener.onDataChanged();
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("BlacklistUpdated",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (sOnBlacklistDataChangedListener != null) {
							sOnBlacklistDataChangedListener.onDataChanged();
						}

						if (sOnDataChangedListener != null) {
							sOnDataChangedListener.onDataChanged();
						}
					}
				});
	}

	public static boolean isInitialized() {
		return IMPrivateMyself.getInstance().getRelationsInited();
	}

	public static ArrayList<String> getFriendsList() {
		return IMPrivateFriendsMgr.getInstance().getCustomUserIDsList();
	}

	public static String getFriend(int index) {
		ArrayList<String> customUserIDsList = IMPrivateFriendsMgr.getInstance()
				.getCustomUserIDsList();

		if (index < 0 || index >= customUserIDsList.size()) {
			return "";
		}

		return customUserIDsList.get(index);
	}

	public static ArrayList<String> getBlacklist() {
		return IMPrivateBlacklistMgr.getInstance().getCustomUserIDsList();
	}

	public static String getBlacklistUser(int index) {
		ArrayList<String> customUserIDsList = IMPrivateBlacklistMgr.getInstance()
				.getCustomUserIDsList();

		if (index < 0 || index >= customUserIDsList.size()) {
			return "";
		}

		return customUserIDsList.get(index);
	}

	public static boolean isMyFriend(String customUserID) {
		if (!IMPrivateMyself.getInstance().getRelationsInited()) {
			sLastError = "IMMyselfRelations Module Not Initialized Yet.";
			return false;
		}

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return false;
		}

		return IMPrivateFriendsMgr.getInstance().contains(customUserID);
	}

	public static boolean isMyBlacklistUser(String customUserID) {
		if (!IMPrivateMyself.getInstance().getRelationsInited()) {
			sLastError = "IMMyselfRelations Module Not Initialized Yet.";
			return false;
		}

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();
			return false;
		}

		return IMPrivateBlacklistMgr.getInstance().contains(customUserID);
	}

	public static long sendFriendRequest(String text, final String toCustomUserID,
			final long timeoutInterval, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMPrivateMyself.getInstance().getRelationsInited()) {
			sLastError = "IMMyselfRelations Module Not Initialized Yet.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (!IMPrivateMyself.getInstance().isLogined()) {
			sLastError = "Counldn't send FriendRequest Before Login.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (text == null) {
			text = "";
		}

		if (!IMParamJudge.isFriendRequestTextLegal(text)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (toCustomUserID.equals(IMMyself.getCustomUserID())) {
			sLastError = "Couldn't Send FriendRequest To Yourself.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (isMyFriend(toCustomUserID)) {
			sLastError = "The User Is Already In Your FriendsList.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		IMUserMsg userMsg = new IMUserMsg();

		userMsg.mUserMsgType = UserMsgType.IMSDKFriendRequest;
		userMsg.mToCustomUserID = toCustomUserID;
		userMsg.mContent = text;
		userMsg.mFromCustomUserID = IMMyself.getCustomUserID();
		userMsg.mClientSendTime = actionTime;

		action.mUserMsg = userMsg;
		action.mTimeoutInterval = timeoutInterval;
		action.begin();

		return actionTime;
	}

	public static long agreeToFriendRequest(final String fromCustomUserID,
			final long timeoutInterval, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMPrivateMyself.getInstance().getRelationsInited()) {
			sLastError = "IMMyselfRelations Module Not Initialized Yet.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (!IMPrivateMyself.getInstance().isLogined()) {
			sLastError = "Counldn't send FriendRequest Before Login.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (!IMParamJudge.isCustomUserIDLegal(fromCustomUserID)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (fromCustomUserID.equals(IMMyself.getCustomUserID())) {
			sLastError = "Couldn't Send AgreeToFriendRequest To Yourself.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		IMActionAgreeToFriendRequest action = new IMActionAgreeToFriendRequest();

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (IMPrivateFriendsMgr.getInstance().add(fromCustomUserID)) {
					IMPrivateFriendsMgr.getInstance().saveFile();
					DTNotificationCenter.getInstance().postNotification("FriendsListUpdated");
				}
				
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		action.mToCustomUserID = fromCustomUserID;
		action.mBeginTime = actionTime;
		action.begin();

		return actionTime;
	}

	public static long rejectToFriendRequest(String reason,
			final String fromCustomUserID, final long timeoutInterval,
			final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMPrivateMyself.getInstance().getRelationsInited()) {
			sLastError = "IMMyselfRelations Module Not Initialized Yet.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (!IMPrivateMyself.getInstance().isLogined()) {
			sLastError = "Counldn't send FriendRequest Before Login.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (reason == null) {
			reason = "";
		}

		if (!IMParamJudge.isRejectReasonLegal(reason)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (!IMParamJudge.isCustomUserIDLegal(fromCustomUserID)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (fromCustomUserID.equals(IMMyself.getCustomUserID())) {
			sLastError = "Couldn't Send RejectToFriendRequest To Yourself.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess();
				}
			}
		};

		IMUserMsg userMsg = new IMUserMsg();

		userMsg.mUserMsgType = UserMsgType.IMSDKRejectToFriendRequest;
		userMsg.mToCustomUserID = fromCustomUserID;
		userMsg.mContent = reason;
		userMsg.mFromCustomUserID = IMMyself.getCustomUserID();
		userMsg.mClientSendTime = actionTime;

		action.mUserMsg = userMsg;
		action.mTimeoutInterval = timeoutInterval;
		action.begin();

		return actionTime;
	}

	public static long removeUserFromFriendsList(final String customUserID,
			long timeoutInterval, final OnActionListener l) {
		long actionTime = System.currentTimeMillis() / 1000;

		if (!IMPrivateMyself.getInstance().getRelationsInited()) {
			sLastError = "IMMyselfRelations Module Not Initialized Yet.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (!IMPrivateMyself.getInstance().isLogined()) {
			sLastError = "Counldn't Remove User From FriendsList Before Login.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = "No In FriendsList.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (!isMyFriend(customUserID)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}
		
		IMActionDeleteFriendRequest action = new IMActionDeleteFriendRequest();
		
		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess();
				}
				
				if (sOnFriendsListDataChangedListener != null) {
					sOnFriendsListDataChangedListener.onDataChanged();
				}
				
				if (sOnDataChangedListener != null) {
					sOnDataChangedListener.onDataChanged();
				}
			}
		};

		action.mToCustomUserID = customUserID;
		action.mBeginTime = actionTime;
		action.begin();

		return actionTime;
	}

	public static long moveUserToBlacklist(final String customUserID,
			long timeoutInterval, final OnActionListener l) {
		long actionTime = System.currentTimeMillis() / 1000;

		if (!IMPrivateMyself.getInstance().getRelationsInited()) {
			sLastError = "IMMyselfRelations Module Not Initialized Yet.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (!IMPrivateMyself.getInstance().isLogined()) {
			sLastError = "Counldn't send FriendRequest Before Login.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		IMActionMoveToBlacklist action = new IMActionMoveToBlacklist();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess();
				}

				if (sOnBlacklistDataChangedListener != null) {
					sOnBlacklistDataChangedListener.onDataChanged();
				}

				if (sOnDataChangedListener != null) {
					sOnDataChangedListener.onDataChanged();
				}
			}
		};

		action.mTimeoutInterval = timeoutInterval;
		action.mCustomUserID = customUserID;
		action.begin();
		return actionTime;
	}

	public static long removeUserFromBlacklist(final String customUserID,
			long timeoutInterval, final OnActionListener l) {
		long actionTime = System.currentTimeMillis() / 1000;

		if (!IMPrivateMyself.getInstance().getRelationsInited()) {
			sLastError = "IMMyselfRelations Module Not Initialized Yet.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (!IMPrivateMyself.getInstance().isLogined()) {
			sLastError = "Counldn't send FriendRequest Before Login.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			sLastError = "No In Blacklist.";

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		if (!isMyBlacklistUser(customUserID)) {
			sLastError = IMParamJudge.getLastError();

			if (l != null) {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						l.onFailure(sLastError);
					}
				});
			}

			return actionTime;
		}

		IMCmdTeamDelMemberFromWb cmd = new IMCmdTeamDelMemberFromWb();

		cmd.mUID = IMUsersMgr.getInstance().getUID(customUserID);
		cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
			@Override
			public void onCommonFailed(FailedType type, long errorCode,
					JSONObject errorJsonObject) throws JSONException {
				if (l != null) {
					if (type == FailedType.RecvError) {
						l.onFailure(errorJsonObject == null ? "Error Code: "
								+ errorCode : errorJsonObject.toString());
					} else {
						l.onFailure("Network Error.");
					}
				}
			}
		};

		cmd.mOnRecvEndListener = new OnRecvEndListener() {
			@Override
			public void onRecvEnd(JSONObject jsonObject) throws JSONException {
				if (l != null) {
					l.onSuccess();
				}

				if (sOnBlacklistDataChangedListener != null) {
					sOnBlacklistDataChangedListener.onDataChanged();
				}

				if (sOnDataChangedListener != null) {
					sOnDataChangedListener.onDataChanged();
				}
			}
		};

		cmd.mType = WBType.BlackList;
		cmd.setTimeoutInterval(timeoutInterval);
		cmd.send();
		return actionTime;
	}

	public static String getLastError() {
		return sLastError;
	}

	private static String sLastError = "";
}
