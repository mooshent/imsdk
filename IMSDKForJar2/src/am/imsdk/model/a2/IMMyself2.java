package am.imsdk.model.a2;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.IMMyselfListener;
import imsdk.data.IMMyself.LoginStatus;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnActionProgressListener;
import imsdk.data.IMMyself.OnAutoLoginListener;
import imsdk.data.IMMyself.OnConnectionChangedListener;
import imsdk.data.IMMyself.OnLoginStatusChangedListener;
import imsdk.data.IMMyself.OnReceiveBitmapListener;
import imsdk.data.IMMyself.OnReceiveTextListener;
import imsdk.data.IMSystemMessage;

import java.util.Observable;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import remote.service.PushService;
import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTFileTool;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.dtlib.model.d.DTDevice;
import am.imsdk.aacmd.IMSocket;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedEndListener;
import am.imsdk.action.IMAction.OnActionPartiallyDoneListener;
import am.imsdk.action.IMActionLogin;
import am.imsdk.action.IMActionLogin.IMActionLoginType;
import am.imsdk.action.ammsgs.IMActionSendUserMsg;
import am.imsdk.demo.util.FacesXml;
import am.imsdk.model.IMAppSettings;
import am.imsdk.model.IMAudioSender;
import am.imsdk.model.IMAudioSender.OnAudioSenderListener;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.amim.IMUserMsgsMgr;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.im.IMUserChatMsgHistory;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import am.imsdk.model.userinfo.IMUsersMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;

public class IMMyself2 {
	// private static IMSDKService sRemoveService;
	//
	// private static ServiceConnection sServiceConnection = new
	// ServiceConnection() {
	// @Override
	// public void onServiceDisconnected(ComponentName name) {
	// sRemoveService = null;
	// }
	//
	// @Override
	// public void onServiceConnected(ComponentName name, IBinder service) {
	// if (!(service instanceof IMSDKBinder)) {
	// DTLog.logError();
	// return;
	// }
	//
	// IMSDKBinder binder = (IMSDKBinder) service;
	//
	// sRemoveService = binder.getService();
	// }
	// };

	public static boolean init(Context applicationContext, String appKey,
			final OnAutoLoginListener l) {
		if (applicationContext == null) {
			IMParamJudge.setLastError("context couldn't be null");
			return false;
		}

		if (!IMParamJudge.isAppKeyLegal(appKey)) {
			return false;
		}

		if (DTAppEnv.getContext() != applicationContext) {
			// 设置context -- 最高级别环境配置
			DTAppEnv.setContext(applicationContext);

			// 环境配置
			DTAppEnv.setRootDirectory("IMSDK.im");
			DTLog.checkCreateDirectory(DTAppEnv.getIMSDKDirectoryFullPath());

			// init DTDevice
			DTDevice.getInstance();
			
			IMSocket.getInstance();

			// // sis
			// IMSocket.getInstance().setSISDomainName("s.imsdk.im");
			// IMSocket.getInstance().setSISDefaultAddress("203.195.162.110:18000");
			//
			// // im
			// IMSocket.getInstance().setDomainName("im.imsdk.im");
			// IMSocket.getInstance().setDefaultIMAddress("14.29.84.61:9100");
			//
			// // run sis
			// IMSocket.getInstance().runSIS();

			// "192.168.20.179:9100"
//			String address = "192.168.20.65:9100";
			String address = "192.168.20.179:9100";
//			String address = "120.25.239.220:9100";
//			IMSocket.getInstance().setSISDefaultAddress(address);
			DTDevice.getInstance().mLastSISAddress = address;
			DTDevice.getInstance().mLastIMAddress = DTDevice.getInstance().mLastSISAddress;
//			IMActionFile.IM_FILE_URL_DOMAIN_NAME = "192.168.20.179";
//			IMActionFile.IM_FILE_URL_REAL_IP = "192.168.20.179";

//			IMSocket.getInstance().setDefaultIMAddress(address);
		}
		
		IMAppSettings.getInstance().readFromFile();

		if (!IMAppSettings.getInstance().mAppKey.equals(appKey)) {
			// 清理所有本地数据
			DTFileTool.checkRemoveDirectory(DTAppEnv.getIMSDKDirectoryFullPath());
			IMAppSettings.newInstance();
			IMAppSettings.getInstance().readFromFile();
			IMAppSettings.getInstance().mAppKey = appKey;
			IMAppSettings.getInstance().saveFile();
		}

		int clientDataVersion = 1;

		if (IMAppSettings.getInstance().mClientDataVersion < clientDataVersion) {
			DTFileTool.checkRemoveDirectory(DTAppEnv.getIMSDKDirectoryFullPath());
			IMUsersMgr.newInstance();
			IMAppSettings.getInstance().mClientDataVersion = clientDataVersion;
			IMAppSettings.getInstance().saveFile();
		}

		sCustomUserID = IMAppSettings.getInstance().mLastLoginCustomUserID;
		IMPrivateMyself.getInstance().reinit();
		IMPrivateMyself.getInstance().readFromFile();

		if (IMAppSettings.getInstance().getAutoLogin() && sCustomUserID != null
				&& sCustomUserID.length() != 0
				&& IMAppSettings.getInstance().mSetupID != 0) {
			if (IMPrivateMyself.getInstance().getUID() == 0) {
				DTLog.logError();
				// 清理所有本地数据
				DTFileTool.checkRemoveDirectory(DTAppEnv.getIMSDKDirectoryFullPath());
				IMAppSettings.newInstance();
				IMAppSettings.getInstance().mAppKey = appKey;
				IMAppSettings.getInstance().saveFile();
				sCustomUserID = "";
				IMPrivateMyself.getInstance().reinit();
				IMPrivateMyself.getInstance().readFromFile();
				return true;
			}

			clearListenersAndPerformRequests();

			// 进入自动登录流程
			final long actionTime = System.currentTimeMillis() / 1000;

			DTNotificationCenter.getInstance().addObserver("IMActionLogin_done",
					new Observer() {
						@Override
						public void update(Observable observable, Object data) {
							if (l != null) {
								l.onAutoLoginSuccess();
							}

							commonActionSuccess(null, "autologin", actionTime);
						}
					});

			DTNotificationCenter.getInstance().addObserver("IMActionLogin_failed",
					new Observer() {
						@Override
						public void update(Observable observable, Object data) {
							if (!(data instanceof Boolean)) {
								DTLog.logError();
								return;
							}

							boolean loginConflict = (Boolean) data;

							if (l != null) {
								l.onAutoLoginFailure(loginConflict);
							}

							commonActionFailure(null, "autologin", actionTime);
						}
					});

			if (l != null) {
				DTAppEnv.performAfterDelayOnUIThread(0, new Runnable() {
					@Override
					public void run() {
						if (!IMParamJudge.isCustomUserIDLegal(IMMyself
								.getCustomUserID())) {
							DTLog.logError();
							return;
						}

						if (!IMParamJudge.isPasswordLegal(IMMyself.getPassword())) {
							DTLog.logError();
							return;
						}

						l.onAutoLoginBegan();
					}
				});
			}

			IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.AutoLogining);
			IMSocket.getInstance().reconnect();

			// Intent intent = new Intent(applicationContext,
			// IMSDKService.class);
			//
			// intent.putExtra("loginType", "AutoLogin");
			// applicationContext.bindService(intent, sServiceConnection,
			// Context.BIND_AUTO_CREATE);
		}

		return true;
	}

	public static boolean setCustomUserID(String customUserID) {
		if (!IMParamJudge.isAppKeyLegal(IMMyself.getAppKey())) {
			IMParamJudge.setLastError("You should init first!");
			return false;
		}

		if (!(customUserID instanceof String)) {
			return false;
		}

		if (!IMParamJudge.isCustomUserIDLegal(customUserID) && customUserID != null
				&& customUserID.length() != 0) {
			return false;
		}

		if (sCustomUserID == customUserID) {
			return true;
		}

		if (IMMyself.getLoginStatus() != IMMyself.LoginStatus.None) {
			IMMyself.logout();
		}

		sCustomUserID = customUserID;
		IMPrivateMyself.getInstance().reinit();
		IMPrivateMyself.getInstance().readFromFile();
		return true;
	}

	public static boolean privateSetCustomUserID(String customUserID) {
		if (!IMParamJudge.isAppKeyLegal(IMMyself.getAppKey())) {
			IMParamJudge.setLastError("You should init first!");
			return false;
		}

		if (!(customUserID instanceof String)) {
			return false;
		}

		if (!IMParamJudge.isCustomUserIDLegal(customUserID) && customUserID != null
				&& customUserID.length() != 0) {
			return false;
		}

		if (sCustomUserID == customUserID) {
			return true;
		}

		sCustomUserID = customUserID;
		IMPrivateMyself.getInstance().reinit();
		IMPrivateMyself.getInstance().readFromFile();
		return true;
	}

	public static boolean setPassword(String password) {
		if (!(password instanceof String)) {
			return false;
		}

		if (!IMParamJudge.isPasswordLegal(password) && password != null
				&& password.length() != 0) {
			return false;
		}

		if (IMPrivateMyself.getInstance().getPassword() == password) {
			return true;
		}

		if (IMMyself.getLoginStatus() != IMMyself.LoginStatus.None) {
			IMMyself.logout();
		}

		IMPrivateMyself.getInstance().setPassword(password);
		return true;
	}

	public static String getPassword() {
		return IMPrivateMyself.getInstance().getPassword();
	}

	public static boolean isLogined() {
		return IMPrivateMyself.getInstance().getLoginStatus()
				.equals(IMMyself.LoginStatus.Logined);
	}

	public static long register(long timeoutInterval, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMParamJudge.isCustomUserIDLegal(getCustomUserID())) {
			commonActionFailure(l, "register", actionTime);
			return actionTime;
		}

		if (!IMParamJudge.isPasswordLegal(getPassword())) {
			commonActionFailure(l, "register", actionTime);
			return actionTime;
		}

//		if (getAppKey().length() == 0) {
//			commonActionFailure(l, "register", actionTime, "appKey不能为空");
//			return actionTime;
//		}

		clearListenersAndPerformRequests();

		DTNotificationCenter.getInstance().addObserver("IMActionLogin_done",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						commonActionSuccess(l, "register", actionTime);
					}
				});

		DTNotificationCenter.getInstance().addObserver("IMActionUserLogin_failed",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (data == null) {
							data = "";
						}

						if (!(data instanceof String)) {
							DTLog.logError();
							return;
						}

						String error = (String) data;

						commonActionFailure(l, "register", actionTime, error);
					}
				});

		IMActionLogin.newInstance();
		IMActionLogin.getInstance().setType(IMActionLoginType.OneKeyRegister);
		IMActionLogin.getInstance().mTimeoutInterval = timeoutInterval;
		IMActionLogin.getInstance().mUID = IMPrivateMyself.getInstance().getUID();
		IMActionLogin.getInstance().mCustomUserID = IMMyself.getCustomUserID();
		IMActionLogin.getInstance().mAppKey = IMAppSettings.getInstance().mAppKey;
		IMActionLogin.getInstance().setPassword(
				IMPrivateMyself.getInstance().getPassword());
		IMActionLogin.getInstance().begin();

		return actionTime;
	}

	public static long login() {
		return login(false, 0, null);
	}

	public static long login(boolean autoRegister, long timeoutInterval,
			final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;
		final String actionType = autoRegister ? "login" : "loginWithAutoRegister";

		if (autoRegister) {
			if (!IMParamJudge.isCustomUserIDLegal(getCustomUserID())
					&& getCustomUserID() != null && getCustomUserID().length() != 0) {
				commonActionFailure(l, actionType, actionTime);
				return actionTime;
			}
		} else {
			if (!IMParamJudge.isCustomUserIDLegal(getCustomUserID())) {
				commonActionFailure(l, actionType, actionTime);
				return actionTime;
			}
		}

		if (autoRegister) {
			if (!IMParamJudge.isPasswordLegal(getPassword()) && getPassword() != null
					&& getPassword().length() != 0) {
				commonActionFailure(l, actionType, actionTime);
				return actionTime;
			}
		} else {
			if (!IMParamJudge.isCustomUserIDLegal(getCustomUserID())) {
				commonActionFailure(l, actionType, actionTime);
				return actionTime;
			}
		}

//		if (getAppKey().length() == 0) {
//			commonActionFailure(l, actionType, actionTime, "appKey不能为空");
//			return actionTime;
//		}

		clearListenersAndPerformRequests();

		DTNotificationCenter.getInstance().addObserver("IMActionLogin_done",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						FacesXml.downLoadGif(DTAppEnv.getContext());
						commonActionSuccess(l, actionType, actionTime);
					}
				});

		DTNotificationCenter.getInstance().addObserver("IMActionUserLogin_failed",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (data == null) {
							data = "";
						}

						if (!(data instanceof String)) {
							DTLog.logError();
							return;
						}

						String error = (String) data;

						commonActionFailure(l, actionType, actionTime, error);
					}
				});

		do {
			if (autoRegister && IMMyself.getCustomUserID().length() == 0) {
				if (IMAppSettings.getInstance().mOneKeyLoginCustomUserID == null
						|| IMAppSettings.getInstance().mOneKeyLoginCustomUserID
								.length() == 0) {
					break;
				}

				if (IMAppSettings.getInstance().mOneKeyLoginPassword == null
						|| IMAppSettings.getInstance().mOneKeyLoginPassword.length() == 0) {
					DTLog.logError();
					break;
				}

				IMMyself.setCustomUserID(IMAppSettings.getInstance().mOneKeyLoginCustomUserID);
				IMMyself.setPassword(IMAppSettings.getInstance().mOneKeyLoginPassword);
			}
		} while (false);

		if (IMPrivateMyself.getInstance().getPassword().length() == 0) {
			IMPrivateMyself.getInstance().setPassword("www.IMSDK.im");
		}

		IMActionLogin.newInstance();
		IMActionLogin.getInstance().setType(
				autoRegister ? IMActionLoginType.OneKeyLogin
						: IMActionLoginType.OneKeyLogin);
		IMActionLogin.getInstance().mTimeoutInterval = timeoutInterval;
		IMActionLogin.getInstance().mUID = IMPrivateMyself.getInstance().getUID();
		IMActionLogin.getInstance().mCustomUserID = IMMyself.getCustomUserID();
		IMActionLogin.getInstance().mAppKey = IMAppSettings.getInstance().mAppKey;
		IMActionLogin.getInstance().setPassword(
				IMPrivateMyself.getInstance().getPassword());
		IMActionLogin.getInstance().begin();

		return actionTime;
	}

	public static void logout() {
		IMAppSettings.getInstance().mLastLoginCustomUserID = "";
		IMAppSettings.getInstance().mLastLoginPassword = "";
		IMAppSettings.getInstance().saveFile();
		IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.None);
		IMSocket.getInstance().checkCloseSocket();
	}

	public static long sendText(String text, String toCustomUserID) {
		return sendText(text, toCustomUserID, 10, null);
	}

	public static long sendText(String text, String toCustomUserID,
			long timeoutInterval, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			commonActionFailure(l, "sendText", actionTime);
			return actionTime;
		}

		if (!IMParamJudge.isIMTextLegal(text)) {
			commonActionFailure(l, "sendText", actionTime);
			return actionTime;
		}

		if (sLastSendTextTimeMillis != 0
				&& System.currentTimeMillis() - sLastSendTextTimeMillis < 1000) {
			commonActionFailure(l, "sendText", actionTime);
			return actionTime;
		}

		sLastSendTextTimeMillis = System.currentTimeMillis();

		// 创建IMUserMsg
		final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
				toCustomUserID, actionTime);

		if (userMsg == null) {
			DTLog.logError();
			return actionTime;
		}

		userMsg.mToCustomUserID = toCustomUserID;
		userMsg.mContent = text;
		userMsg.mUserMsgType = UserMsgType.Normal;
		userMsg.saveFile();

		// 维护聊天记录
		final IMUserChatMsgHistory history = IMUserMsgHistoriesMgr.getInstance()
				.getUserChatMsgHistory(toCustomUserID);

		if (history == null) {
			DTLog.logError();
			return actionTime;
		}

		history.insertUnsentUserMsg(userMsg.mClientSendTime);
		history.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				history.getNewMsgNotificationKey());

		// 维护最近联系人
		IMPrivateRecentContacts.getInstance().insert(toCustomUserID);
		IMPrivateRecentContacts.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification(
				IMPrivateRecentContacts.getInstance().notificationKey());

		// 发送消息
		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				// 维护聊天记录
				history.replaceUnsentUserMsgToSent(userMsg);
				history.saveFile();

				commonActionSuccess(l, "sendText", actionTime);
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				commonActionFailure(l, "sendText", actionTime, error);
			}
		};

		action.mUserMsg = userMsg;
		action.mTimeoutInterval = timeoutInterval;
		action.mBeginTime = actionTime;
		action.begin();

		return actionTime;
	}

	public static boolean startRecording(String toCustomUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			return false;
		}

		return IMAudioSender.getInstance().startRecordingToUser(toCustomUserID);
	}

	public static long stopRecording(boolean needSend, final long timeoutInterval,
			final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		boolean result = IMAudioSender.getInstance().stopRecording(needSend,
				timeoutInterval, new OnAudioSenderListener() {
					@Override
					public void onSendSuccess() {
						commonActionSuccess(l, "SendAudio", actionTime);
					}

					@Override
					public void onSendFailure(String error) {
						commonActionFailure(l, "SendAudio", actionTime);
					}

					@Override
					public void onRecordSuccess() {
					}

					@Override
					public void onRecordFailure(String error) {
						commonActionFailure(l, "SendAudio", actionTime);
					}
				}, actionTime);

		if (!result) {
			if (l != null) {
				l.onFailure("IMSDK Error");
			}
		}

		return actionTime;
	}

	public static long sendBitmap(final Bitmap bitmap, final String toCustomUserID,
			final long timeoutInterval, final OnActionProgressListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			if (l != null) {
				l.onFailure(IMParamJudge.getLastError());
			}

			if (sListener != null) {
				sListener.onActionFailure("sendBitmap", IMParamJudge.getLastError(),
						actionTime);
			}

			return actionTime;
		}

		if (sLastSendTextTimeMillis != 0
				&& System.currentTimeMillis() - sLastSendTextTimeMillis < 1000) {
			if (l != null) {
				l.onFailure("发送频率太快");
			}

			if (sListener != null) {
				sListener.onActionFailure("sendBitmap", "发送频率太快", actionTime);
			}

			return actionTime;
		}

		sLastSendTextTimeMillis = System.currentTimeMillis();

		// 生成IMImagePhoto
		final IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(bitmap);

		photo.saveFile();

		// 创建content
		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put("fileID", photo.mFileID);
			jsonObject.put("width", photo.getBitmap().getWidth());
			jsonObject.put("height", photo.getBitmap().getHeight());
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();

			if (l != null) {
				l.onFailure("IMSDK Error");
			}

			if (sListener != null) {
				sListener.onActionFailure("sendBitmap", "IMSDK Error", actionTime);
			}

			return actionTime;
		}

		// 创建IMUserMsg
		final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
				toCustomUserID, actionTime);

		userMsg.mToCustomUserID = toCustomUserID;
		userMsg.mContent = jsonObject.toString();
		userMsg.mUserMsgType = UserMsgType.Photo;
		userMsg.saveFile();

		// 维护聊天记录
		final IMUserChatMsgHistory history = IMUserMsgHistoriesMgr.getInstance()
				.getUserChatMsgHistory(toCustomUserID);

		history.insertUnsentUserMsg(userMsg.mClientSendTime);
		history.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				history.getNewMsgNotificationKey());

		// 维护最近联系人
		IMPrivateRecentContacts.getInstance().insert(toCustomUserID);
		IMPrivateRecentContacts.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification(
				IMPrivateRecentContacts.getInstance().notificationKey());

		// 发送消息
		IMActionSendUserMsg action = new IMActionSendUserMsg();

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				// 维护聊天记录
				history.replaceUnsentUserMsgToSent(userMsg);
				history.saveFile();

				if (l != null) {
					l.onSuccess();
				}

				if (sListener != null) {
					sListener.onActionSuccess("SendBitmap", actionTime);
				}
			}
		};

		action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
			@Override
			public void onActionPartiallyDone(double percentage) {
				if (l != null) {
					l.onProgress(percentage / 100);
				}
			}
		};

		action.mOnActionFailedEndListener = new OnActionFailedEndListener() {
			@Override
			public void onActionFailedEnd(String error) {
				if (l != null) {
					l.onFailure(error);
				}

				if (sListener != null) {
					sListener.onActionFailure("SendBitmap", error, actionTime);
				}
			}
		};

		action.mUserMsg = userMsg;

		if (timeoutInterval > 0) {
			action.mTimeoutInterval = timeoutInterval;
		}

		action.mBeginTime = actionTime;
		action.begin();
		return actionTime;
	}

	public static void setListener(IMMyself.IMMyselfListener listener) {
		sListener = listener;
	}

	public static String getCustomUserID() {
		return IMMyself2.sCustomUserID != null ? IMMyself2.sCustomUserID : "";
	}
	
	public static void setNickName(String nickName) {
		IMPrivateMyself.getInstance().setNickName(nickName);
	}

	public static String getAppKey() {
		if (IMAppSettings.getInstance().mAppKey == null) {
			IMAppSettings.getInstance().mAppKey = "";
		}

		return IMAppSettings.getInstance().mAppKey;
	}

	public static LoginStatus getLoginStatus() {
		return IMPrivateMyself.getInstance().getLoginStatus();
	}

	protected static void commonActionSuccess(final OnActionListener l,
			final String actionType, final long actionTime) {
		if (!DTAppEnv.isUIThread()) {
			DTLog.logError();
			return;
		}

		if (l != null) {
			DTAppEnv.performAfterDelayOnUIThread(0, new Runnable() {
				@Override
				public void run() {
					l.onSuccess();
				}
			});
		}

		if (sListener != null) {
			DTAppEnv.performAfterDelayOnUIThread(0, new Runnable() {
				@Override
				public void run() {
					sListener.onActionSuccess(actionType, actionTime);
				}
			});
		}
	}

	protected static void commonActionFailure(OnActionListener l, String actionType,
			long actionTime) {
		commonActionFailure(l, actionType, actionTime, IMParamJudge.getLastError());
	}

	protected static void commonActionFailure(final OnActionListener l,
			final String actionType, final long actionTime, final String error) {
		if (!DTAppEnv.isUIThread()) {
			DTLog.logError();
			return;
		}

		if (l != null) {
			DTAppEnv.performAfterDelayOnUIThread(0, new Runnable() {
				@Override
				public void run() {
					l.onFailure(error);
				}
			});
		}

		if (sListener != null) {
			DTAppEnv.performAfterDelayOnUIThread(0, new Runnable() {
				@Override
				public void run() {
					sListener.onActionFailure(actionType, error, actionTime);
				}
			});
		}
	}

	public static void setOnReceiveTextListener(OnReceiveTextListener l) {
		sOnReceiveTextListener = l;
	}

	public static void setOnReceiveBitmapListener(OnReceiveBitmapListener l) {
		sOnReceiveBitmapListener = l;
	}

	public static void setOnConnectionChangedListener(OnConnectionChangedListener l) {
		sOnConnectionChangedListener = l;
	}

	private static void clearListenersAndPerformRequests() {
		DTNotificationCenter.getInstance().removeObservers("IMActionLogin_done");
		DTNotificationCenter.getInstance().removeObservers("IMActionUserLogin_failed");
		DTNotificationCenter.getInstance().removeObservers("IMActionLogin_failed");
	}

	private static String sCustomUserID = "";
	private static long sLastSendTextTimeMillis = 0;

	// 通用Listener
	private static IMMyselfListener sListener;

	// IM相关
	private static OnReceiveTextListener sOnReceiveTextListener;
	private static OnReceiveBitmapListener sOnReceiveBitmapListener;

	// 登录与连接相关
	private static OnConnectionChangedListener sOnConnectionChangedListener;
	private static OnLoginStatusChangedListener sLoginStatusChangedListener;

	public static long getLastSendTextTimeMillis() {
		return sLastSendTextTimeMillis;
	}

	private static void tryAutoLogin() {
		DTLog.sign("tryAutoLogin");

		if (!IMAppSettings.getInstance().getAutoLogin()) {
			return;
		}

		if (IMPrivateMyself.getInstance().getUID() == 0) {
			DTLog.logError();
			return;
		}

		if (IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.None
				&& IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.AutoLogining) {
			DTLog.logError();
			return;
		}

		if (IMPrivateMyself.getInstance().getPassword() == null
				|| IMPrivateMyself.getInstance().getPassword().length() == 0) {
			DTLog.logError();
			return;
		}

		DTLog.sign("AutoLogin begin");
		IMActionLogin.newInstance();
		IMActionLogin.getInstance().setType(IMActionLoginType.AutoLogin);
		IMActionLogin.getInstance().mUID = IMPrivateMyself.getInstance().getUID();
		IMActionLogin.getInstance().mCustomUserID = IMPrivateMyself.getInstance()
				.getCustomUserID();
		IMActionLogin.getInstance().mAppKey = IMAppSettings.getInstance().mAppKey;
		IMActionLogin.getInstance().setPassword(
				IMPrivateMyself.getInstance().getPassword());
		IMActionLogin.getInstance().begin();
	}

	// 1. 登录状态下调用
	// 2. 断线重连状态下调用
	private static void tryReconnect() {
		DTLog.sign("tryReconnect");

		if (IMPrivateMyself.getInstance().getUID() == 0) {
			DTLog.logError();
			return;
		}

		if (IMPrivateMyself.getInstance().getLoginStatus() == LoginStatus.Logined) {
			IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.Reconnecting);
		}

		if (IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.Reconnecting) {
			DTLog.logError();
			return;
		}

		clearListenersAndPerformRequests();

		final long actionTime = System.currentTimeMillis() / 1000;

		DTNotificationCenter.getInstance().addObserver("IMActionLogin_done",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (sListener != null) {
							sListener.onReconnected();
						}

						if (sOnConnectionChangedListener != null) {
							sOnConnectionChangedListener.onReconnected();
						}

						commonActionSuccess(null, "reconnect", actionTime);
					}
				});

		IMActionLogin.newInstance();
		IMActionLogin.getInstance().setType(IMActionLoginType.Reconnect);
		IMActionLogin.getInstance().mUID = IMPrivateMyself.getInstance().getUID();
		IMActionLogin.getInstance().mCustomUserID = IMPrivateMyself.getInstance()
				.getCustomUserID();
		IMActionLogin.getInstance().mAppKey = IMAppSettings.getInstance().mAppKey;
		IMActionLogin.getInstance().setPassword(
				IMPrivateMyself.getInstance().getPassword());
		IMActionLogin.getInstance().begin();
	}

	private static Observer sSocketUpdatedObserver = new Observer() {
		@Override
		public void update(Observable observable, Object data) {
			Log.e("Debug", "IMMySelf2---sSocketUpdatedObserver");
			DTLog.sign("sSocketUpdatedObserver");
			DTLog.sign("IMPrivateMyself.getInstance().getLoginStatus():"
					+ IMPrivateMyself.getInstance().getLoginStatus());

			if (IMActionLogin.getInstance() != null) {
				DTLog.sign("IMActionLogin.getInstance().getType():"
						+ IMActionLogin.getInstance().getType());
			}

			switch (IMPrivateMyself.getInstance().getLoginStatus()) {
			case None:
				break;
			case Logining: {
				if (IMActionLogin.getInstance() == null) {
					DTLog.logError();
					return;
				}

				if (IMActionLogin.getInstance().getType() != IMActionLoginType.OneKeyLogin
						&& IMActionLogin.getInstance().getType() != IMActionLoginType.OneKeyRegister) {
					DTLog.logError();
					return;
				}

				if (IMActionLogin.getInstance().isOver()) {
					DTLog.logError();
					return;
				}
			}
				break;
			case Reconnecting:
				if (IMActionLogin.getInstance() == null) {
					tryReconnect();
				}

				if (IMActionLogin.getInstance() == null) {
					DTLog.logError();
					return;
				}

				if (IMActionLogin.getInstance().getType() != IMActionLoginType.Reconnect) {
					DTLog.logError();
					return;
				}

				if (IMActionLogin.getInstance().isOver()) {
					DTLog.logError();
					return;
				}
				break;
			case AutoLogining: {
				if (IMActionLogin.getInstance() != null) {
					DTLog.sign("LoginType:" + IMActionLogin.getInstance().getType());
				}

				// reinit的时候被清理掉
				if (IMActionLogin.getInstance() == null
						|| IMActionLogin.getInstance().getType() == IMActionLoginType.None) {
					tryAutoLogin();
				}

				if (IMActionLogin.getInstance() == null) {
					DTLog.logError();
					return;
				}

				if (IMActionLogin.getInstance().getType() != IMActionLoginType.AutoLogin) {
					DTLog.logError();
					DTLog.log(IMActionLogin.getInstance().getType().toString());
					return;
				}

				if (IMActionLogin.getInstance().isOver()) {
					DTLog.logError();
					return;
				}
			}
				break;
			case Logined:
				tryReconnect();
				break;
			default:
				break;
			}
		}
	};

	static {
		DTNotificationCenter.getInstance().addObserver("SOCKET_UPDATED",
				sSocketUpdatedObserver);

		DTNotificationCenter.getInstance().addObserver("IMLoginStatusUpdated",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (IMPrivateMyself.getInstance().getLoginStatus() == LoginStatus.Reconnecting) {
							if (sOnConnectionChangedListener != null) {
								sOnConnectionChangedListener.onDisconnected(false);
							}

							if (sListener != null) {
								sListener.onDisconnected(false);
							}
						}

						if (sLoginStatusChangedListener == null) {
							return;
						}

						if (!(data instanceof LoginStatus)) {
							DTLog.logError();
							return;
						}

						LoginStatus oldLoginStatus = (LoginStatus) data;

						sLoginStatusChangedListener.onLoginStatusChanged(
								oldLoginStatus, getLoginStatus());
					}
				});

		DTNotificationCenter.getInstance().addObserver("IMReceiveText", new Observer() {
			@Override
			public void update(Observable observable, Object data) {
				if (!(data instanceof IMUserMsg)) {
					DTLog.logError();
					return;
				}

				IMUserMsg userMsg = (IMUserMsg) data;

				if (!userMsg.mIsRecv) {
					DTLog.logError();
					return;
				}

				if (userMsg.mUserMsgType != UserMsgType.Normal) {
					DTLog.logError();
					return;
				}

				if (getLoginStatus() != LoginStatus.Logined) {
					return;
				}

				if (IMPrivateMyself.getInstance().getUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() != IMPrivateMyself.getInstance().getUID()) {
					DTLog.sign("userMsg.getToUID():" + userMsg.getToUID());
					DTLog.sign("IMPrivateMyself.getInstance().getUID():"
							+ IMPrivateMyself.getInstance().getUID());
					DTLog.logError();
					return;
				}

				if (sOnReceiveTextListener != null) {
					sOnReceiveTextListener.onReceiveText(userMsg.mContent,
							userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mServerSendTime);
				}

				if (sListener != null) {
					sListener.onReceiveText(userMsg.mContent,
							userMsg.mFromCustomUserID, userMsg.mServerSendTime);
				}
			}
		});
		
		DTNotificationCenter.getInstance().addObserver("IMReceiveAudio", new Observer() {
			@Override
			public void update(Observable observable, Object data) {
				if (!(data instanceof IMUserMsg)) {
					DTLog.logError();
					return;
				}

				IMUserMsg userMsg = (IMUserMsg) data;

				if (!userMsg.mIsRecv) {
					DTLog.logError();
					return;
				}

				if (userMsg.mUserMsgType != UserMsgType.Audio) {
					DTLog.logError();
					return;
				}

				if (getLoginStatus() != LoginStatus.Logined) {
					return;
				}

				if (IMPrivateMyself.getInstance().getUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() != IMPrivateMyself.getInstance().getUID()) {
					DTLog.sign("userMsg.getToUID():" + userMsg.getToUID());
					DTLog.sign("IMPrivateMyself.getInstance().getUID():"
							+ IMPrivateMyself.getInstance().getUID());
					DTLog.logError();
					return;
				}

				if (sOnReceiveTextListener != null) {
					sOnReceiveTextListener.onReceiveAudio(DTTool.getSecretString(userMsg.mMsgID),
							userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mServerSendTime);
				}

				if (sListener != null) {
					sListener.onReceiveAudio(DTTool.getSecretString(userMsg.mMsgID),
							userMsg.mFromCustomUserID, userMsg.mServerSendTime);
				}
			}
		});

		DTNotificationCenter.getInstance().addObserver("IMReceiveSystemText",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMSystemMessage)) {
							DTLog.logError();
							return;
						}

						IMSystemMessage systemMessage = (IMSystemMessage) data;

						if (getLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMPrivateMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if (sOnReceiveTextListener != null) {
							DTLog.sign("onReceiveText");
							sOnReceiveTextListener.onReceiveSystemText(
									systemMessage.mContent,
									systemMessage.mServerSendTime);
						}

						if (sListener != null) {
							DTLog.sign("sListener onReceiveText");
							sListener.onReceiveSystemText(systemMessage.mContent,
									systemMessage.mServerSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("LoginConflict", new Observer() {
			@Override
			public void update(Observable observable, Object data) {
				if (!IMPrivateMyself.getInstance().isLogined()
						&& IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.AutoLogining) {
					return;
				}

				IMAppSettings.getInstance().mLastLoginCustomUserID = "";
				IMAppSettings.getInstance().saveFile();
				IMPrivateMyself.getInstance().setLoginStatus(LoginStatus.None);
				IMSocket.getInstance().reconnect();

				if (sOnConnectionChangedListener != null) {
					sOnConnectionChangedListener.onDisconnected(true);
				}

				if (sListener != null) {
					sListener.onDisconnected(true);
				}
			}
		});

		DTNotificationCenter.getInstance().addObserver("StopRecordingThenSendAudio",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (userMsg.mUserMsgType != UserMsgType.Audio) {
							DTLog.logError();
							return;
						}

						if (!DTAppEnv.isUIThread()) {
							DTLog.logError();
							return;
						}

						if (sListener != null) {
							sListener.onActionSuccess("StopRecordingThenSendAudio",
									userMsg.mClientSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver(
				"StopRecordingThenSendAudioFailed", new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (userMsg.mUserMsgType != UserMsgType.Audio) {
							DTLog.logError();
							return;
						}

						if (!DTAppEnv.isUIThread()) {
							DTLog.logError();
							return;
						}

						if (sListener != null) {
							sListener.onActionFailure("StopRecordingThenSendAudio", "",
									userMsg.mClientSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("IMReceiveBitmapMessage",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (!userMsg.mIsRecv) {
							DTLog.logError();
							return;
						}

						if (userMsg.mUserMsgType != UserMsgType.Photo) {
							DTLog.logError();
							return;
						}

						if (getLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMPrivateMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() != IMPrivateMyself.getInstance()
								.getUID()) {
							DTLog.logError();
							return;
						}

						if (sOnReceiveBitmapListener != null) {
							sOnReceiveBitmapListener.onReceiveBitmapMessage(
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.getNickName(), userMsg.mServerSendTime);
						}

						if (sListener != null) {
							sListener.onReceiveBitmapMessage(
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("IMReceiveBitmapProgress",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (!userMsg.mIsRecv) {
							DTLog.logError();
							return;
						}

						if (userMsg.mUserMsgType != UserMsgType.Photo) {
							DTLog.logError();
							return;
						}

						if (getLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMPrivateMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() != IMPrivateMyself.getInstance()
								.getUID()) {
							DTLog.logError();
							return;
						}

						if (userMsg.mExtraData == null) {
							DTLog.logError();
							return;
						}

						double progress = 0;

						try {
							progress = userMsg.mExtraData.getDouble("progress");
						} catch (JSONException e) {
							e.printStackTrace();
						}

						if (sOnReceiveBitmapListener != null) {
							sOnReceiveBitmapListener.onReceiveBitmapProgress(progress,
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}

						if (sListener != null) {
							sListener.onReceiveBitmapProgress(progress,
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}
					}
				});

		DTNotificationCenter.getInstance().addObserver("IMReceiveBitmap",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (!userMsg.mIsRecv) {
							DTLog.logError();
							return;
						}

						if (userMsg.mUserMsgType != UserMsgType.Photo) {
							DTLog.logError();
							return;
						}

						if (getLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMPrivateMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() != IMPrivateMyself.getInstance()
								.getUID()) {
							DTLog.logError();
							return;
						}

						String fileID = userMsg.getFileID();

						if (!IMParamJudge.isFileIDLegal(fileID)) {
							DTLog.logError();
							return;
						}

						IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(fileID);

						if (photo.getBitmap() == null) {
							DTLog.logError();
							return;
						}

						if (sOnReceiveBitmapListener != null) {
							sOnReceiveBitmapListener.onReceiveBitmap(photo.getBitmap(),
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}

						if (sListener != null) {
							sListener.onReceiveBitmap(photo.getBitmap(),
									DTTool.getSecretString(userMsg.mMsgID),
									userMsg.mFromCustomUserID, userMsg.mServerSendTime);
						}
					}
				});
	}

	public static void setOnLoginStatusChangedListener(OnLoginStatusChangedListener l) {
		sLoginStatusChangedListener = l;
	}
}
