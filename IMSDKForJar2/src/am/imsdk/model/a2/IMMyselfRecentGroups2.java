package am.imsdk.model.a2;

import imsdk.data.IMSDK.OnDataChangedListener;
import imsdk.data.localchatmessagehistory.IMGroupChatMessage;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.amimteam.IMTeamMsg;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.imgroup.IMPrivateRecentGroups;
import am.imsdk.model.imteam.IMTeamChatMsgHistory;
import am.imsdk.model.imteam.IMTeamMsgHistoriesMgr;

public final class IMMyselfRecentGroups2 extends DTLocalModel {
	public static void setOnDataChangedListener(OnDataChangedListener l) {
		sListener = l;
	}

	static {
		DTNotificationCenter.getInstance().addObserver(
				IMPrivateRecentContacts.getInstance().notificationKey(),
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (sListener != null) {
							sListener.onDataChanged();
						}
					}
				});
	}

	private static OnDataChangedListener sListener;

	public static ArrayList<String> getGroupsList() {
		return (ArrayList<String>) IMPrivateRecentGroups.getInstance()
				.getGroupIDsList();
	}

	public static boolean removeGroup(String groupID) {
		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			sLastError = IMParamJudge.getLastError();
			return false;
		}

		return IMPrivateRecentGroups.getInstance().remove(groupID);
	}

	public static int getChatMessageCount(String groupID) {
		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			sLastError = IMParamJudge.getLastError();
			return 0;
		}

		if (!IMPrivateRecentGroups.getInstance().contains(groupID)) {
			return 0;
		}

		IMTeamChatMsgHistory history = IMTeamMsgHistoriesMgr.getInstance()
				.getTeamChatMsgHistory(DTTool.getTeamIDFromGroupID(groupID));

		return history.mAryTeamMsgKeys.size();
	}
	
	public static IMGroupChatMessage getLastGroupChatMessage(String groupID) {
		if (getChatMessageCount(groupID) == 0) {
			return null;
		} else {
			return getGroupChatMessage(groupID, 0);
		}
	}

	public static IMGroupChatMessage getGroupChatMessage(String groupID, int index) {
		if (index < 0) {
			sLastError = "Illegal Index";
			return null;
		}

		if (index >= getChatMessageCount(groupID)) {
			sLastError = "Index Over Range";
			return null;
		}

		IMTeamChatMsgHistory history = IMTeamMsgHistoriesMgr.getInstance()
				.getTeamChatMsgHistory(DTTool.getTeamIDFromGroupID(groupID));

		if (index >= history.mAryTeamMsgKeys.size()) {
			DTLog.logError();
			return null;
		}

		IMTeamMsg teamMsg = history.getTeamMsg(index);

		if (teamMsg == null) {
			DTLog.logError();
			return null;
		}

		return teamMsg.getGroupChatMessage();
	}

	public static long getUnreadChatMessageCount() {
		long result = 0;
		ArrayList<String> aryGroupIDs = getGroupsList();

		for (String groupID : aryGroupIDs) {
			if (!IMParamJudge.isGroupIDLegal(groupID)) {
				DTLog.logError();
				sLastError = "IMSDK Error.";
				continue;
			}

			IMTeamChatMsgHistory history = IMTeamMsgHistoriesMgr.getInstance()
					.getTeamChatMsgHistory(DTTool.getTeamIDFromGroupID(groupID));

			result += history.mUnreadMessageCount;
		}

		return result;
	}
	
	public static long getUnreadChatMessageCount(String groupID) {
		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			DTLog.logError();
			sLastError = "IMSDK Error.";
			return 0;
		}

		IMTeamChatMsgHistory history = IMTeamMsgHistoriesMgr.getInstance()
				.getTeamChatMsgHistory(DTTool.getTeamIDFromGroupID(groupID));

		return history.mUnreadMessageCount;
	}

	public static boolean clearUnreadChatMessage() {
		if (getUnreadChatMessageCount() == 0) {
			return false;
		}

		boolean result = true;
		ArrayList<String> aryGroupIDs = getGroupsList();

		for (String groupID : aryGroupIDs) {
			if (!IMParamJudge.isGroupIDLegal(groupID)) {
				DTLog.logError();
				sLastError = "IMSDK Error.";
				result = false;
				continue;
			}

			IMTeamChatMsgHistory history = IMTeamMsgHistoriesMgr.getInstance()
					.getTeamChatMsgHistory(DTTool.getTeamIDFromGroupID(groupID));

			history.mUnreadMessageCount = 0;
			history.saveFile();
		}

		return result;
	}

	public static boolean clearUnreadChatMessage(String groupID) {
		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			sLastError = IMParamJudge.getLastError();
			return false;
		}

		IMTeamChatMsgHistory history = IMTeamMsgHistoriesMgr.getInstance()
				.getTeamChatMsgHistory(DTTool.getTeamIDFromGroupID(groupID));

		history.mUnreadMessageCount = 0;
		history.saveFile();
		return true;
	}

	public static String getLastError() {
		return sLastError;
	}

	private static String sLastError = "";
}
