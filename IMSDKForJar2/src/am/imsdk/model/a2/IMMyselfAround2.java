package am.imsdk.model.a2;

import imsdk.data.IMUserLocation;
import imsdk.data.around.IMMyselfAround.OnAroundActionListener;
import imsdk.data.around.IMMyselfAround.State;

import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.action.IMAction;
import am.imsdk.action.around.IMActionAroundNext;
import am.imsdk.action.around.IMActionAroundUpdate;
import am.imsdk.model.IMPrivateAroundUsers;
import am.imsdk.model.userinfo.IMPrivateUserInfo;
import am.imsdk.model.userinfo.IMUsersMgr;

public final class IMMyselfAround2 {
	public static State getState() {
		return IMPrivateAroundUsers.getInstance().mState;
	}

	public static void update(final OnAroundActionListener l) {
		IMPrivateAroundUsers.getInstance().mState = State.Normal;

		IMActionAroundUpdate action = new IMActionAroundUpdate();

		action.mOnActionFailedListener = new IMAction.OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new IMAction.OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess(IMPrivateAroundUsers.getInstance().getUsersInLastPage());
				}
			}
		};

		action.mTimeoutInterval = 200;
		action.begin();
	}

	public static void nextPage(final OnAroundActionListener l) {
		if (IMPrivateAroundUsers.getInstance().mState == State.ReachEnd) {
			if (l != null) {
				l.onFailure("Already Reach The End.");
			}

			return;
		}

		IMPrivateAroundUsers.getInstance().mState = State.Normal;

		IMActionAroundNext action = new IMActionAroundNext();

		action.mOnActionFailedListener = new IMAction.OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
				if (l != null) {
					l.onFailure(error);
				}
			}
		};

		action.mOnActionDoneEndListener = new IMAction.OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				if (l != null) {
					l.onSuccess(IMPrivateAroundUsers.getInstance().getUsersInLastPage());
				}
			}
		};

		action.begin();
	}

	public static ArrayList<String> getAllUsers() {
		return IMPrivateAroundUsers.getInstance().getCustomUserIDsList();
	}

	public static String getUser(int index) {
		ArrayList<String> customUserIDsList = IMPrivateAroundUsers.getInstance()
				.getCustomUserIDsList();

		if (index < 0 || index >= customUserIDsList.size()) {
			return "";
		}

		return customUserIDsList.get(index);
	}

	public static ArrayList<IMUserLocation> getAllUserLocations() {
		ArrayList<String> aryCustomUserIDs = IMPrivateAroundUsers.getInstance()
				.getCustomUserIDsList();
		ArrayList<IMUserLocation> result = new ArrayList<IMUserLocation>();

		for (String customUserID : aryCustomUserIDs) {
			IMPrivateUserInfo userInfo = IMUsersMgr.getInstance().getUserInfo(
					customUserID);

			if (userInfo == null) {
				DTLog.logError();
				continue;
			}

			result.add(userInfo.getUserLocation());
		}

		return result;
	}
}
