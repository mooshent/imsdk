package am.imsdk.aacmd.user;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.kefu.IMCustomerServiceMgr;
import am.imsdk.model.kefu.IMPrivateCSInfo;

public final class IMCmdUserGetCustomerServiceInfo extends DTCmd {
	public String mAppKey;
	private ArrayList<Long> mUIDsList = new ArrayList<Long>();

	public IMCmdUserGetCustomerServiceInfo() {
		mCmdTypeValue = IMCmdType.IM_CMD_USER_GET_CUSTOMER_SERVICE_INFO.getValue();
	}

	public void addUID(long uid) {
		for (long tempUID : mUIDsList) {
			if (tempUID == uid) {
				return;
			}
		}

		mUIDsList.add(uid);
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mAppKey == null || mAppKey.length() == 0) {
			DTLog.logError();
			return;
		}

		if (mUIDsList == null || mUIDsList.size() == 0) {
			DTLog.logError();
			return;
		}

		mSendJsonObject.put("appKey", mAppKey);
		mSendJsonObject.put("uidlist", new JSONArray(mUIDsList));
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		JSONArray jsonArray = recvJsonObject.getJSONArray("cslist");

		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);

			if (jsonObject.getLong("enable") == 0) {
				// 禁用或者删除
				long uid = jsonObject.getLong("uid");

				if (uid == 0) {
					DTLog.logError();
					return;
				}

				IMPrivateCSInfo info = IMCustomerServiceMgr.getInstance()
						.getCSInfo(uid);

				if (info != null) {
					IMCustomerServiceMgr.getInstance().removeCSInfo(uid);
					IMCustomerServiceMgr.getInstance().saveFile();

					info.saveFile();
				}

				continue;
			}

			IMPrivateCSInfo info = IMCustomerServiceMgr.getInstance().getCSInfo(
					jsonObject);

			if (info == null) {
				DTLog.logError();
				return;
			}
		}
	}
}
