package am.imsdk.aacmd.user;

import org.json.JSONException;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMParamJudge;
import android.text.TextUtils;

public final class IMCmdUserRegister extends DTCmd {
	public String mCustomUserID;
	public String mPassword;
	public String mNickName;
	public String mDid;

	public IMCmdUserRegister() {
		mCmdTypeValue = IMCmdType.IM_CMD_USER_REGISTER.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID) && mCustomUserID != null
				&& mCustomUserID.length() != 0) {
			DTLog.logError();
			return;
		}

		if (!IMParamJudge.isPasswordLegal(mPassword)) {
			DTLog.logError();
			return;
		}

		mSendJsonObject.put("platform", "a");

		if (!TextUtils.isEmpty(mNickName)) {
			mSendJsonObject.put("nickname", mNickName);
		}
		
		if (!TextUtils.isEmpty(mCustomUserID)) {
			if(!mCustomUserID.contains("+86")) {
				mCustomUserID = "+86" + mCustomUserID;
			}
			mSendJsonObject.put("phonenum", mCustomUserID);
		}
		
		if (!TextUtils.isEmpty(mDid)) {
			mSendJsonObject.put("did", mDid);
		}
		
		if (!TextUtils.isEmpty(mPassword)) {
//			mSendJsonObject.put("password", DTTool.getMD5String(mPassword));
			mSendJsonObject.put("password", mPassword);
		}
	}
}
