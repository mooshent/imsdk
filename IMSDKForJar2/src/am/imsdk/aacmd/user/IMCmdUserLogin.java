package am.imsdk.aacmd.user;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.userinfo.IMUsersMgr;
import android.text.TextUtils;

public final class IMCmdUserLogin extends DTCmd {
	public String mPhoneNum;
	public String mPassword;
	public boolean mAutoLogin;
	public long mSetupID;
	
	public IMCmdUserLogin() {
		mCmdTypeValue = IMCmdType.IM_CMD_USER_LOGIN.getValue();
	}
	
	@Override
	public void initSendJsonObject() throws JSONException {
		if(!IMParamJudge.isCustomUserIDLegal(mPhoneNum)) {
			DTLog.logError();
			return;
		}
		
		if (!IMParamJudge.isPasswordLegal(mPassword)) {
			DTLog.logError();
			return;
		}
		
		mSendJsonObject.put("platform", "a");
		
		if (!TextUtils.isEmpty(mPhoneNum)) {
//			if(!mPhoneNum.contains("+86")) {
//				mPhoneNum = "+86" + mPhoneNum;
//			}
			mSendJsonObject.put("uid", Long.parseLong(mPhoneNum));
		}
		
		if (!TextUtils.isEmpty(mPassword)) {
//			mSendJsonObject.put("password", DTTool.getMD5String(mPassword));
			mSendJsonObject.put("password", mPassword);
		}
		
		if (mSetupID != 0) {
			mSendJsonObject.put("setupid", mSetupID);
		}
		if (mAutoLogin) {
			mSendJsonObject.put("auto", mAutoLogin ? 1 : 0);
		}
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
	}

	@Override
	public void onSendFailed() {
	}

	@Override
	public void onNoRecv() {
	}
}
