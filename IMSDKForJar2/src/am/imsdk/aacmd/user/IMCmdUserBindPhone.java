package am.imsdk.aacmd.user;

import org.json.JSONException;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMParamJudge;
import android.text.TextUtils;

public class IMCmdUserBindPhone extends DTCmd{

	public String mPhoneNum;
	public String mPassword;
	
	public IMCmdUserBindPhone() {
		mCmdTypeValue = IMCmdType.IM_CMD_USER_BIND_PHONENUM.getValue();
	}
	
	@Override
	public void initSendJsonObject() throws JSONException {

		if(!IMParamJudge.isCustomUserIDLegal(mPhoneNum)) {
			DTLog.logError();
			return;
		}
		
		if (!IMParamJudge.isPasswordLegal(mPassword)) {
			DTLog.logError();
			return;
		}
		
		if (!TextUtils.isEmpty(mPhoneNum)) {
			if(!mPhoneNum.contains("+86")) {
				mPhoneNum = "+86" + mPhoneNum;
			}
			mSendJsonObject.put("phonenum", mPhoneNum);
		}
//		mSendJsonObject.put("password", DTTool.getMD5String(mPassword));
		mSendJsonObject.put("password", mPassword);
	}
	
}