package am.imsdk.aacmd.user;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.c.socket.DTPushCmd;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMPrivateMyself;

public final class IMPushCmdUserLoginConflict extends DTPushCmd {
	public IMPushCmdUserLoginConflict() {
		mCmdTypeValue = IMCmdType.IM_PUSH_CMD_USER_LOGIN_CONFLICT.getValue();
	}
	
	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		if (!IMPrivateMyself.getInstance().isLogined()) {
			return;
		}
		
		DTNotificationCenter.getInstance().postNotification("LoginConflict");
	}
}
