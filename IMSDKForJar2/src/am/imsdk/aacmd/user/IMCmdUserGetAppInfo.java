package am.imsdk.aacmd.user;

import imsdk.data.IMMyself;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMAppSettings;
import am.imsdk.model.IMParamJudge;

public final class IMCmdUserGetAppInfo extends DTCmd {
	public IMCmdUserGetAppInfo() {
		mCmdTypeValue = IMCmdType.IM_CMD_USER_GET_APP_INFO.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (!IMParamJudge.isAppKeyLegal(IMMyself.getAppKey())) {
			DTLog.logError();
			return;
		}

		mSendJsonObject.put("appkey", IMMyself.getAppKey());
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		if (!recvJsonObject.has("kefu")) {
			IMAppSettings.getInstance().mCustomerServiceUID = 0;
			IMAppSettings.getInstance().saveFile();
			return;
		}
		
		long uid = recvJsonObject.getLong("kefu");

		if (uid == 0) {
			DTLog.logError();
			return;
		}

		IMAppSettings.getInstance().mCustomerServiceUID = uid;
		IMAppSettings.getInstance().saveFile();
	}
}
