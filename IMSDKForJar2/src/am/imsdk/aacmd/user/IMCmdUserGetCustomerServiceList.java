package am.imsdk.aacmd.user;

import org.json.JSONException;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;

public final class IMCmdUserGetCustomerServiceList extends DTCmd {
	public String mAppKey = "";
	public long mCustomerServiceVersion;
	
	public IMCmdUserGetCustomerServiceList() {
		mCmdTypeValue = IMCmdType.IM_CMD_USER_GET_CUSTOMER_SERVICE_LIST.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mAppKey == null || mAppKey.length() == 0) {
			DTLog.logError();
			return;
		}
		
		mSendJsonObject.put("appkey", mAppKey);
		mSendJsonObject.put("v", mCustomerServiceVersion);
	}
}
