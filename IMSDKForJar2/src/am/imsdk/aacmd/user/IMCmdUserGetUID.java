package am.imsdk.aacmd.user;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.kefu.IMCustomerServiceMgr;
import am.imsdk.model.userinfo.IMUsersMgr;
import android.text.TextUtils;

public final class IMCmdUserGetUID extends DTCmd {
	public String mPhoneNum;
	
	public IMCmdUserGetUID() {
		mCmdTypeValue = IMCmdType.IM_CMD_USER_GET_UID.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (!IMParamJudge.isCustomUserIDLegal(mPhoneNum)) {
			DTLog.logError();
			return;
		}

		if (!TextUtils.isEmpty(mPhoneNum)) {
			if(!mPhoneNum.contains("+86")) {
				mPhoneNum = "+86" + mPhoneNum;
			}
			mSendJsonObject.put("phonenum", mPhoneNum);
		}
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		long uid = 0;
		try {
			uid = recvJsonObject.getLong("uid");
		} catch (Exception e) {
			e.printStackTrace();
			DTLog.logError();
			DTLog.log(e.toString());
			return;
		}
		
		if (uid == 0) {
			DTLog.logError();
			return;
		}
		
		DTLog.log("uid:" + uid + "");

		if ((uid & 0x000f) == 6) {
			DTLog.logError();
			DTLog.log(uid + "");

			// 客服账号
			IMCustomerServiceMgr.getInstance().set(mPhoneNum, uid);
			IMCustomerServiceMgr.getInstance().saveFile();
		} else {
			IMUsersMgr.getInstance().setCustomUserIDForUID(mPhoneNum, uid);
			IMUsersMgr.getInstance().saveFile();
		}

	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	@Override
	public void onSendFailed() {
		DTLog.logError();
	}

	@Override
	public void onNoRecv() {
		DTLog.logError();
	}
}
