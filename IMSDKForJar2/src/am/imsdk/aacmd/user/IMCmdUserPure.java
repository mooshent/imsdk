package am.imsdk.aacmd.user;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMParamJudge;
import android.text.TextUtils;

public final class IMCmdUserPure extends DTCmd {
	public boolean mLogin;
	public String mCustomUserID;
	public String mPassword;
	
	public IMCmdUserPure() {
//		mCmdTypeValue = IMCmdType.IM_CMD_USER_PURE.getValue();
	}
	
	@Override
	public void initSendJsonObject() throws JSONException {
		if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)) {
			DTLog.logError();
			return;
		}
		
		if (!IMParamJudge.isPasswordLegal(mPassword)) {
			DTLog.logError();
			return;
		}

		mSendJsonObject.put("login", mLogin ? 1 : 0);
		mSendJsonObject.put("platform", "a");
		if (!TextUtils.isEmpty(mCustomUserID)) {
			if(!mCustomUserID.contains("+86")) {
				mCustomUserID = "+86" + mCustomUserID;
			}
			mSendJsonObject.put("phonenum", mCustomUserID);
		}
//		mSendJsonObject.put("password", DTTool.getMD5String(mPassword));
		mSendJsonObject.put("password", mPassword);
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
	}

	@Override
	public void onSendFailed() {
	}

	@Override
	public void onNoRecv() {
	}
}
