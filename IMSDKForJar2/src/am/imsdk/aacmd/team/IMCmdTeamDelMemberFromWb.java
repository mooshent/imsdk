package am.imsdk.aacmd.team;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.aacmd.team.IMCmdTeamAddMember2WB.WBType;
import am.imsdk.model.userslist.IMPrivateBlacklistMgr;
import am.imsdk.model.userslist.IMPrivateFriendsMgr;

public class IMCmdTeamDelMemberFromWb extends DTCmd {
	public WBType mType = WBType.Friends;
	public long mUID;
	
	public IMCmdTeamDelMemberFromWb() {
		mCmdTypeValue = IMCmdType.IM_CMD_TEAM_DEL_MEMBER_FROM_WB.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mUID == 0) {
			DTLog.logError();
		}
		
		mSendJsonObject.put("type", mType.getValue());
		mSendJsonObject.put("uid", mUID);
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		switch (mType) {
		case Friends:
		{
			if (IMPrivateFriendsMgr.getInstance().remove(mUID)) {
				IMPrivateFriendsMgr.getInstance().saveFile();
			}
		}
			break;
		case BlackList:
		{
			if (IMPrivateBlacklistMgr.getInstance().remove(mUID)) {
				IMPrivateBlacklistMgr.getInstance().saveFile();
			}
		}
			break;
		default:
			break;
		}
	}
}
