package am.imsdk.aacmd.team;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.userslist.IMPrivateBlacklistMgr;
import am.imsdk.model.userslist.IMPrivateFriendsMgr;

// w = white
// b = black
public class IMCmdTeamAddMember2WB extends DTCmd {
	public WBType mType = WBType.Friends;
	public long mUID;
	
	public static enum WBType {
		Friends(1), BlackList(2);
		
		private final int value;

		private WBType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}
	
	public IMCmdTeamAddMember2WB() {
		mCmdTypeValue = IMCmdType.IM_CMD_TEAM_ADD_MEMBER_2_WB.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mUID == 0) {
			DTLog.logError();
			return;
		}
		
		if (mUID == IMPrivateMyself.getInstance().getUID()) {
			DTLog.logError();
			return;
		}

		mSendJsonObject.put("uid", mUID);
		mSendJsonObject.put("type", mType.getValue());
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		switch (mType) {
		case Friends:
		{
			if (IMPrivateFriendsMgr.getInstance().add(mUID)) {
				IMPrivateFriendsMgr.getInstance().saveFile();
			}
			
			if (IMPrivateBlacklistMgr.getInstance().remove(mUID)) {
				IMPrivateBlacklistMgr.getInstance().saveFile();
			}
		}
			break;
		case BlackList:
		{
			if (IMPrivateBlacklistMgr.getInstance().add(mUID)) {
				IMPrivateBlacklistMgr.getInstance().saveFile();
			}
			
			if (IMPrivateFriendsMgr.getInstance().remove(mUID)) {
				IMPrivateFriendsMgr.getInstance().saveFile();
			}
		}
			break;
		default:
			break;
		}
	}
}
