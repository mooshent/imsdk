package am.imsdk.aacmd.team;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo.TeamType;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import am.imsdk.model.userinfo.IMUsersMgr;

public final class IMCmdTeamAddMember extends DTCmd {
	public long mTeamID;
	public ArrayList<Long> mUids = new ArrayList<Long>();

	public IMCmdTeamAddMember() {
		mCmdTypeValue = IMCmdType.IM_CMD_TEAM_ADD_MEMBER.getValue();
	}
	
	public void addUID(long uid) {
		if(uid <= 0) {
			DTLog.logError();
			return;
		}
		
		for (Long uidObject : mUids) {
			if (!(uidObject instanceof Long)) {
				DTLog.logError();
				return;
			}

			if (uidObject.longValue() == uid) {
				return;
			}
		}

		mUids.add(Long.valueOf(uid));
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mTeamID == 0) {
			DTLog.logError();
			return;
		}

		mSendJsonObject.put("teamid", mTeamID);
		mSendJsonObject.put("uids", new JSONArray(mUids));
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		
		IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(
				mTeamID);

		teamInfo.add(mUids);
		teamInfo.saveFile();

		if (teamInfo.mTeamType == TeamType.Group) {
			for (int i = 0; i < mUids.size(); i++) {
				String customUserID = IMUsersMgr.getInstance().getCustomUserID(mUids.get(i));
				
				if (customUserID.length() == 0) {
					DTLog.logError();
					return;
				}
				
				DTNotificationCenter.getInstance().postNotification(
						"GroupMemberAdded:" + teamInfo.mTeamID, customUserID);
			}
		}
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	@Override
	public void onSendFailed() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onNoRecv() {
		// TODO Auto-generated method stub

	}
}
