package am.imsdk.aacmd.around;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;

public final class IMCmdAroundGet extends DTCmd {
	public double mLongitude;
	public double mLatitude;
	public int mLevel = 1;
	public long mHour;
	public int mPage;
	
	public IMCmdAroundGet() {
		mCmdTypeValue = IMCmdType.IM_CMD_AROUND_GET_AROUND_USER_LOCATION.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		mSendJsonObject.put("xpos", mLongitude);
		mSendJsonObject.put("ypos", mLatitude);
		mSendJsonObject.put("level", mLevel);
		mSendJsonObject.put("page", mPage);
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	@Override
	public void onSendFailed() {
	}

	@Override
	public void onNoRecv() {
	}
}
