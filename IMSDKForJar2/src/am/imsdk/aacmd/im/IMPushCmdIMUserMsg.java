package am.imsdk.aacmd.im;

import imsdk.data.IMMyself.LoginStatus;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTPushCmd;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.aacmd.team.IMCmdTeamAddMember2WB;
import am.imsdk.aacmd.team.IMCmdTeamAddMember2WB.WBType;
import am.imsdk.action.IMAction.OnActionDoneEndListener;
import am.imsdk.action.IMAction.OnActionFailedListener;
import am.imsdk.action.IMAction.OnActionPartiallyDoneListener;
import am.imsdk.action.ammsgs.IMActionRecvUserMsg;
import am.imsdk.action.ammsgs.IMActionSendUserMsg;
import am.imsdk.action.fileserver.IMActionDownloadFile;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.im.IMPrivateRecentContacts;
import am.imsdk.model.im.IMUserChatMsgHistory;
import am.imsdk.model.im.IMUserCustomMsgHistory;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo.TeamType;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import am.imsdk.model.userslist.IMPrivateFriendsMgr;
import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.audio.IMAudiosMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;
import android.util.Log;

public final class IMPushCmdIMUserMsg extends DTPushCmd {
	private static int sMsgCount = 0;

	public IMPushCmdIMUserMsg() {
		mCmdTypeValue = IMCmdType.IM_PUSH_CMD_IM_USER_MSG.getValue();
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		if (++sMsgCount % 5 == 0) {
			DTNotificationCenter.getInstance().postNotification("heartbeat");
		}

		if (IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.Logined) {
			return;
		}

		final IMActionRecvUserMsg action = new IMActionRecvUserMsg();

		action.mOnActionFailedListener = new OnActionFailedListener() {
			@Override
			public void onActionFailed(String error) {
			}
		};

		action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
			@Override
			public void onActionDoneEnd() {
				IMUserMsg userMsg = action.mUserMsg;

				if (userMsg == null) {
					return;
				}

				if (userMsg.mUserMsgType == UserMsgType.Unit) {
					DTLog.logError();
					return;
				}

				if (!IMParamJudge.isCustomUserIDLegal(userMsg.mFromCustomUserID)) {
					DTLog.logError();
					return;
				}

				if (!IMParamJudge.isCustomUserIDLegal(userMsg.mToCustomUserID)) {
					DTLog.logError();
					return;
				}

				if (userMsg.getToUID() != IMPrivateMyself.getInstance().getUID()) {
					DTLog.logError();
					return;
				}

				if (userMsg.mMsgID == 0) {
					DTLog.logError();
					return;
				}

				if (userMsg.mServerSendTime == 0) {
					DTLog.logError();
					return;
				}

				onRecvUserMsg(userMsg);
			}
		};

		action.mRecvJsonObject = recvJsonObject;
		action.begin();
	}

	private void sendUserMsgReceived(IMUserMsg userMsg) {
		if (userMsg == null) {
			DTLog.logError();
			return;
		}

		if (userMsg.mMsgID == 0) {
			DTLog.logError();
			return;
		}

		if (userMsg.getFromUID() == 0) {
			DTLog.logError();
			return;
		}

		if (userMsg.mContent.length() > IMActionSendUserMsg.IMTextMaxLength) {
			return;
		}

		IMCmdIMUserMsgReceived cmd = new IMCmdIMUserMsgReceived();

		cmd.mMsgID = userMsg.mMsgID;
		cmd.mFromUID = userMsg.getFromUID();
		cmd.send();
	}

	private void insertUserChatMsgHistory(IMUserMsg userMsg) {
		if (!IMParamJudge.isCustomUserIDLegal(userMsg.mFromCustomUserID)) {
			DTLog.logError();
			return;
		}

		if (!userMsg.mIsRecv) {
			DTLog.logError();
			return;
		}

		if (userMsg.mMsgID == 0) {
			DTLog.logError();
			return;
		}

		// 维护聊天记录
		IMUserChatMsgHistory history = IMUserMsgHistoriesMgr.getInstance()
				.getUserChatMsgHistory(userMsg.mFromCustomUserID);

		history.insertRecvUserMsg(userMsg.mMsgID);
		history.mUnreadMessageCount = history.mUnreadMessageCount + 1;
		history.saveFile();

		DTNotificationCenter.getInstance().postNotification(
				history.getNewMsgNotificationKey());

		// 维护最近联系人
		IMPrivateRecentContacts.getInstance().insert(userMsg.mFromCustomUserID);
		IMPrivateRecentContacts.getInstance().saveFile();
		DTNotificationCenter.getInstance().postNotification(
				IMPrivateRecentContacts.getInstance().notificationKey());
	}

	private void onRecvUserMsg(final IMUserMsg userMsg) {
		switch (userMsg.mUserMsgType) {
		case Normal: {
			if (userMsg.isLocalFileExist()) {
				sendUserMsgReceived(userMsg);
				return;
			}

			sendUserMsgReceived(userMsg);
			userMsg.saveFile();

			// 维护聊天记录 & 最近联系人
			insertUserChatMsgHistory(userMsg);

			// 发出通知
			DTNotificationCenter.getInstance().postNotification("IMReceiveText",
					userMsg);
		}
			break;
		case Audio: {
			if (!IMParamJudge.isFileIDLegal(userMsg.getFileID())) {
				DTLog.logError();
				sendUserMsgReceived(userMsg);
				return;
			}

			if (userMsg.isLocalFileExist()) {
				sendUserMsgReceived(userMsg);
				return;
			}

			final IMAudio audio = IMAudiosMgr.getInstance().getAudio(
					userMsg.getFileID());

			if (audio.isLocalFileExist()) {
				sendUserMsgReceived(userMsg);
				userMsg.saveFile();

				// 维护聊天记录 & 最近联系人
				insertUserChatMsgHistory(userMsg);

				// 发出通知
				DTNotificationCenter.getInstance().postNotification("IMReceiveAudio",
						userMsg);

				return;
			}

			final IMActionDownloadFile action = new IMActionDownloadFile();

			action.mOnActionFailedListener = new OnActionFailedListener() {
				@Override
				public void onActionFailed(String error) {
				}
			};

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					if (userMsg.isLocalFileExist()) {
						sendUserMsgReceived(userMsg);
						return;
					}

					sendUserMsgReceived(userMsg);

					if (!userMsg.saveFile()) {
						sendUserMsgReceived(userMsg);
						DTLog.logError();
						return;
					}

					if (!audio.mFileID.equals(userMsg.getFileID())) {
						DTLog.logError();
						return;
					}

					IMAudiosMgr.getInstance().replaceClientFileID(audio.mFileID,
							action.mFileID);
					audio.mFileID = action.mFileID;
					audio.mBuffer = action.mBuffer;
					audio.saveFile();

					// 维护聊天记录 & 最近联系人
					insertUserChatMsgHistory(userMsg);

					// 发出通知
					DTNotificationCenter.getInstance().postNotification(
							"IMReceiveAudio", userMsg);
				}
			};

			action.mFileID = userMsg.getFileID();
			action.begin();
		}
			break;
		case Photo: {
			if (userMsg.mContent.length() == 0) {
				DTLog.logError();
				return;
			}

			// 维护本地数据
			if (userMsg.isLocalFileExist()) {
				this.sendUserMsgReceived(userMsg);
				return;
			}

			if (!IMParamJudge.isFileIDLegal(userMsg.getFileID())) {
				DTLog.logError();
				return;
			}

			final IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
					userMsg.getFileID());

			if (photo.isLocalFileExist()) {
				sendUserMsgReceived(userMsg);
				userMsg.saveFile();

				// 维护聊天记录 & 最近联系人
				insertUserChatMsgHistory(userMsg);

				// 发出通知
				DTNotificationCenter.getInstance().postNotification(
						"IMReceiveBitmapMessage", userMsg);
				return;
			}

			sendUserMsgReceived(userMsg);
			userMsg.saveFile();

			// 维护聊天记录 & 最近联系人
			insertUserChatMsgHistory(userMsg);

			final IMActionDownloadFile action = new IMActionDownloadFile();

			action.mFileID = userMsg.getFileID();

			// 发出通知
			DTNotificationCenter.getInstance().postNotification(
					"IMReceiveBitmapMessage", userMsg);

			action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
				@Override
				public void onActionPartiallyDone(double percentage) {
					if (userMsg.getExtraData() == null) {
						DTLog.logError();
						return;
					}

					try {
						userMsg.getExtraData().put("progress",
								Double.valueOf(percentage / 100));
					} catch (JSONException e) {
						e.printStackTrace();
						DTLog.logError();
						return;
					}

					// 发出通知
					DTNotificationCenter.getInstance().postNotification(
							"IMReceiveBitmapProgress", userMsg);
				}
			};

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					if (!photo.mFileID.equals(userMsg.getFileID())) {
						DTLog.logError();
						return;
					}

					photo.setBuffer(action.mBuffer);
					photo.saveFile();

					if (photo.getBitmap() == null) {
						DTLog.logError();
						return;
					}

					// 发出通知
					DTNotificationCenter.getInstance().postNotification(
							"IMReceiveBitmap", userMsg);
				}
			};

			action.begin();
		}
			break;
		case Custom: {
			if (userMsg.isLocalFileExist()) {
				sendUserMsgReceived(userMsg);
				return;
			}

			if (!userMsg.saveFile()) {
				sendUserMsgReceived(userMsg);
				DTLog.logError();
				return;
			}

			sendUserMsgReceived(userMsg);

			// 维护自定义消息历史记录
			IMUserCustomMsgHistory history = IMUserMsgHistoriesMgr.getInstance()
					.getUserCustomMsgHistory(userMsg.mFromCustomUserID);

			history.insertUnsentUserMsg(userMsg.mClientSendTime);
			history.mUnreadMessageCount++;
			history.saveFile();
			DTNotificationCenter.getInstance().postNotification(
					history.getNewMsgNotificationKey());

			// 发出通知
			DTNotificationCenter.getInstance().postNotification(
					"IMReceiveCustomMessage", userMsg);
		}
			break;
		case IMSDKFriendRequest: {
			if (userMsg.isLocalFileExist()) {
				sendUserMsgReceived(userMsg);
				return;
			}

			if (!userMsg.saveFile()) {
				sendUserMsgReceived(userMsg);
				DTLog.logError();
				return;
			}

			sendUserMsgReceived(userMsg);

			DTNotificationCenter.getInstance().postNotification("ReceiveFriendRequest",
					userMsg);
		}
			break;
		case IMSDKDeleteFriend: {
			Log.e("Debug", "收到 IMSDKDeleteFriend 命令");
			if (userMsg.isLocalFileExist()) {
				sendUserMsgReceived(userMsg);
				return;
			}

			if (!userMsg.saveFile()) {
				sendUserMsgReceived(userMsg);
				DTLog.logError();
				return;
			}

			sendUserMsgReceived(userMsg);
			
			if (IMPrivateRecentContacts.getInstance().remove(userMsg.mFromCustomUserID)) {
				IMPrivateRecentContacts.getInstance().saveFile();
				DTNotificationCenter.getInstance().postNotification(
						IMPrivateRecentContacts.getInstance().notificationKey());
			}

			if (IMPrivateFriendsMgr.getInstance().remove(userMsg.mFromCustomUserID)) {
				IMPrivateFriendsMgr.getInstance().saveFile();
				DTNotificationCenter.getInstance().postNotification("FriendsListUpdated");
			}
			
			DTNotificationCenter.getInstance().postNotification("ReceiveFriendDeleted",
					userMsg);
		}
			break;
		case IMSDKAgreeToFriendRequest: {
			if (userMsg.isLocalFileExist()) {
				sendUserMsgReceived(userMsg);
				return;
			}

			if (!userMsg.saveFile()) {
				sendUserMsgReceived(userMsg);
				DTLog.logError();
				return;
			}

			sendUserMsgReceived(userMsg);

			DTNotificationCenter.getInstance().postNotification(
					"ReceiveAgreeToFriendRequest", userMsg);

			IMCmdTeamAddMember2WB cmd = new IMCmdTeamAddMember2WB();

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					DTNotificationCenter.getInstance().postNotification(
							"BuildFriendshipWithUser", userMsg);
				}
			};

			cmd.mType = WBType.Friends;
			cmd.mUID = userMsg.getFromUID();
			cmd.send();
		}
			break;
		case IMSDKRejectToFriendRequest: {
			if (userMsg.isLocalFileExist()) {
				sendUserMsgReceived(userMsg);
				return;
			}

			if (!userMsg.saveFile()) {
				sendUserMsgReceived(userMsg);
				DTLog.logError();
				return;
			}

			sendUserMsgReceived(userMsg);

			DTNotificationCenter.getInstance().postNotification(
					"ReceiveRejectToFriendRequest", userMsg);
		}
			break;
		case IMSDKBlacklist: {
			if (userMsg.isLocalFileExist()) {
				sendUserMsgReceived(userMsg);
				return;
			}

			if (!userMsg.saveFile()) {
				sendUserMsgReceived(userMsg);
				DTLog.logError();
				return;
			}

			sendUserMsgReceived(userMsg);

			if (IMPrivateRecentContacts.getInstance().remove(userMsg.mFromCustomUserID)) {
				IMPrivateRecentContacts.getInstance().saveFile();
				DTNotificationCenter.getInstance().postNotification(
						IMPrivateRecentContacts.getInstance().notificationKey());
			}

			if (IMPrivateFriendsMgr.getInstance().remove(userMsg.mFromCustomUserID)) {
				IMPrivateFriendsMgr.getInstance().saveFile();
				DTNotificationCenter.getInstance().postNotification("FriendsListUpdated");
			}
		}
			break;
		case IMSDKGroupNoticeBeAdded: {
			if (userMsg.isLocalFileExist()) {
				sendUserMsgReceived(userMsg);
				return;
			}

			sendUserMsgReceived(userMsg);

			if (!userMsg.saveFile()) {
				DTLog.logError();
				return;
			}

			// 刷新数据
			// long teamID = DTTool.getTeamIDFromGroupID(userMsg.getGroupID());
			// IMPrivateTeamInfo teamInfo =
			// IMTeamsMgr.getInstance().getTeamInfo(teamID);
			//
			// if (teamInfo == null) {
			// DTLog.logError();
			// return;
			// }
			//
			// teamInfo.remove(IMPrivateMyself.getInstance().getUID());
			// teamInfo.saveFile();
			//
			// if (teamInfo.mTeamType == TeamType.Group) {
			// DTNotificationCenter.getInstance().postNotification(
			// "TeamUpdated:" + teamInfo.mTeamID,
			// IMPrivateMyself.getInstance().getCustomUserID());
			// }
			//
			// IMPrivateMyself.getInstance().removeTeam(teamID);
			// IMPrivateMyself.getInstance().saveFile();
			//
			// // 通知
			// DTNotificationCenter.getInstance().postNotification("RemovedFromGroup",
			// userMsg);
			//
			// DTNotificationCenter.getInstance()
			// .postNotification("AddedToGroup", userMsg);
		}
			break;
		case IMSDKGroupNoticeBeRemoved: {
			if (userMsg.isLocalFileExist()) {
				sendUserMsgReceived(userMsg);
				return;
			}

			sendUserMsgReceived(userMsg);

			if (!userMsg.saveFile()) {
				DTLog.logError();
				return;
			}

			// 刷新数据
			long teamID = DTTool.getTeamIDFromGroupID(userMsg.getGroupID());
			IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(teamID);

			if (teamInfo == null) {
				DTLog.logError();
				return;
			}

			teamInfo.remove(IMPrivateMyself.getInstance().getUID());
			teamInfo.saveFile();

			if (teamInfo.mTeamType == TeamType.Group) {
				DTNotificationCenter.getInstance().postNotification(
						"TeamUpdated:" + teamInfo.mTeamID,
						IMPrivateMyself.getInstance().getCustomUserID());
			}

			IMPrivateMyself.getInstance().removeTeam(teamID);
			IMPrivateMyself.getInstance().saveFile();

			// 通知
			DTNotificationCenter.getInstance().postNotification("RemovedFromGroup",
					userMsg);
		}
			break;
		default: {
			DTLog.logError();

			if (userMsg.isLocalFileExist()) {
				sendUserMsgReceived(userMsg);
				return;
			}

			if (!userMsg.saveFile()) {
				sendUserMsgReceived(userMsg);
				DTLog.logError();
				return;
			}

			sendUserMsgReceived(userMsg);
		}
			break;
		}
	}
}
