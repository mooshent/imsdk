package am.imsdk.aacmd.im;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;

public final class IMCmdIMSendUserMsg extends DTCmd {
	public UserMsgType mUserMsgType = UserMsgType.Normal;
	public long mToUID;
	public String mContent = "";
	public boolean mNeedApns;

	public IMCmdIMSendUserMsg() {
		mCmdTypeValue = IMCmdType.IM_CMD_IM_SEND_USER_MSG.getValue();
	}

	@Override
	public void initSendJsonObject() throws JSONException {
		if (mToUID == 0) {
			DTLog.logError();
			return;
		}

		if (mContent == null) {
			mContent = "";
		}

		mSendJsonObject.put("msgtype", mUserMsgType.getValue());
		mSendJsonObject.put("touid", mToUID);
		mSendJsonObject.put("msgcontent", mContent);
		
		JSONObject extraData = new JSONObject();
		extraData.put("nickName", IMPrivateMyself.getInstance().getNickName());
		
		mSendJsonObject.put("extraData", extraData.toString());

		if (mNeedApns) {
			if (mUserMsgType == UserMsgType.Normal) {
				mSendJsonObject.put("apnstext", IMPrivateMyself.getInstance().getCustomUserID()
						+ " 给您发送了一条消息");
			} else if (mUserMsgType == UserMsgType.IMSDKAgreeToFriendRequest) {
				mSendJsonObject.put("apnstext", IMPrivateMyself.getInstance().getCustomUserID()
						+ " 同意加您为好友");
			} else if (mUserMsgType == UserMsgType.IMSDKDeleteFriend) {
				mSendJsonObject.put("apnstext", IMPrivateMyself.getInstance().getCustomUserID()
						+ " 将您从好友列表中删除");
			} else if (mUserMsgType == UserMsgType.IMSDKRejectToFriendRequest) {
				mSendJsonObject.put("apnstext", IMPrivateMyself.getInstance().getCustomUserID()
						+ " 拒绝加您为好友");
			} else if (mUserMsgType == UserMsgType.IMSDKFriendRequest) {
				mSendJsonObject.put("apnstext", IMPrivateMyself.getInstance().getCustomUserID()
						+ " 请求加您为好友");
			} else if (mUserMsgType == UserMsgType.Photo) {
				mSendJsonObject.put("apnstext", IMPrivateMyself.getInstance().getCustomUserID()
						+ " 发来一条图片消息");
			} else if (mUserMsgType == UserMsgType.Audio) {
				mSendJsonObject.put("apnstext", IMPrivateMyself.getInstance().getCustomUserID()
						+ " 发来一条语音消息");
			} else {
				DTLog.logError();
			}
		}
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
	}
}
