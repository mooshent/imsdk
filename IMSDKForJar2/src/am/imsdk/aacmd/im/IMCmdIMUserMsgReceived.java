package am.imsdk.aacmd.im;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;

public final class IMCmdIMUserMsgReceived extends DTCmd {
	public long mMsgID;
	public long mFromUID;
	
	public IMCmdIMUserMsgReceived() {
		mCmdTypeValue = IMCmdType.IM_CMD_IM_USER_MSG_RECEIVED.getValue();
	}
	
	@Override
	public void initSendJsonObject() throws JSONException {
		if (mMsgID == 0) {
			DTLog.logError();
			return;
		}
		
		if (mFromUID == 0) {
			DTLog.logError();
			return;
		}

		mSendJsonObject.put("msgid", mMsgID);
		mSendJsonObject.put("uid", mFromUID);
	}
	
	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
	}
	
	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
		DTLog.sign("onRecvError - mMsgID:" + mMsgID);
	}
	
	@Override
	public void onSendFailed() {
	}
	
	@Override
	public void onNoRecv() {
	}
}
