package am.imsdk.aacmd.im;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd;
import am.imsdk.aacmd.IMCmdType;

public final class IMCmdIMSystemMsgReceived extends DTCmd {
	public long mSystemMsgID;
	
	public IMCmdIMSystemMsgReceived() {
		mCmdTypeValue = IMCmdType.IM_CMD_IM_SYSTEM_MSG_RECEIVED.getValue();
	}
	
	@Override
	public void initSendJsonObject() throws JSONException {
		mSendJsonObject.put("systemmsgid", mSystemMsgID);
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
	}

	@Override
	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	@Override
	public void onSendFailed() {
	}

	@Override
	public void onNoRecv() {
	}
}
