package am.imsdk.aacmd.im;

import imsdk.data.IMMyself.LoginStatus;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTPushCmd;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.aacmd.IMCmdType;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.im.IMPrivateSystemMsg;

public final class IMPushCmdIMSystemMsg extends DTPushCmd {
	public IMPushCmdIMSystemMsg() {
		mCmdTypeValue = IMCmdType.IM_PUSH_CMD_IM_SYSTEM_MSG.getValue();
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		if (IMPrivateMyself.getInstance().getLoginStatus() != LoginStatus.Logined) {
			return;
		}
		
		IMPrivateSystemMsg privateSystemMsg = new IMPrivateSystemMsg(recvJsonObject);
		
		if (privateSystemMsg.mToUID != IMPrivateMyself.getInstance().getUID()) {
			DTLog.logError();
			return;
		}
		
		DTNotificationCenter.getInstance().postNotification("IMReceiveSystemText", privateSystemMsg.mSystemMessage);
		
		IMCmdIMSystemMsgReceived cmd = new IMCmdIMSystemMsgReceived();
		
		cmd.mSystemMsgID = privateSystemMsg.mMsgID;
		cmd.send();
	}
}
