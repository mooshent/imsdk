package am.imsdk.action;

import java.util.ArrayList;

import am.dtlib.model.b.log.DTLog;
import am.imsdk.model.IMPrivateMyself;

public final class IMActionsMgr {
	public long mUID;

	private IMActionsMgr() {
		mUID = IMPrivateMyself.getInstance().getUID();
	}

	public void beginAction(IMAction action) {
		if (mActionsList.contains(action)) {
			DTLog.logError();
			return;
		}

		mActionsList.add(action);
	}

	public void endAction(IMAction action) {
		if (!mActionsList.contains(action)) {
			DTLog.logError();
			return;
		}

		mActionsList.remove(action);
	}

	public void setUID(long uid) {
		if (uid == 0 && uid != mUID) {
			for (int i = mActionsList.size() - 1; i >= 0; i--) {
				IMAction action = mActionsList.get(i);

				if (action.isOver()) {
					continue;
				}

				action.done("User Logout");
			}
		}

		mUID = uid;
	}

	private ArrayList<IMAction> mActionsList = new ArrayList<IMAction>();

	// singleton
	private volatile static IMActionsMgr sSingleton;

	public static IMActionsMgr getInstance() {
		if (sSingleton == null) {
			synchronized (IMActionsMgr.class) {
				if (sSingleton == null) {
					sSingleton = new IMActionsMgr();
				}
			}
		}
		return sSingleton;
	}

	// singleton end

//	// newInstance 
//	public static void newInstance() {
//		synchronized (IMActionsMgr.class) {
//			sSingleton = new IMActionsMgr();
//		}
//	}
//	// newInstance end
}
