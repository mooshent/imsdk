package am.imsdk.action.photo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.imsdk.aacmd.user.IMCmdUserSetInfo;
import am.imsdk.action.IMAction;
import am.imsdk.action.fileserver.IMActionUploadFile;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;
import android.graphics.Bitmap;
import android.os.AsyncTask;

// 1. 压缩为byte[]
// 2. 上传文件
// 3. 设置baseInfo
public final class IMActionUserUploadMainPhoto extends IMAction {
	public Bitmap mBitmap;
	public byte[] mBuffer;
	public String mFileID;

	public IMActionUserUploadMainPhoto() {
		mStepCount = 3;
		mTimeoutInterval = 30;
	}

	@Override
	public void onActionBegan() {
		if (mBitmap == null && mBuffer == null) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			if (mBuffer != null && mBuffer.length != 0) {
				nextStep();
				return;
			}

			AsyncTask<Bitmap, Integer, byte[]> asyncTask = new AsyncTask<Bitmap, Integer, byte[]>() {
				@Override
				protected byte[] doInBackground(Bitmap... params) {
					Bitmap bitmap = params[0];
					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

					bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);

					byte[] buffer = outputStream.toByteArray();

					try {
						outputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
						DTLog.logError();
					}

					return buffer;
				}

				@Override
				protected void onPostExecute(byte[] result) {
					super.onPostExecute(result);

					mBuffer = result;

					if (mBuffer != null && mBuffer.length != 0) {
						nextStep();
					} else {
						doneWithIMSDKError();
					}
				}
			};

			asyncTask.execute(mBitmap);
		}
			break;
		case 2: {
			final IMActionUploadFile action = new IMActionUploadFile();

			action.mBuffer = mBuffer;

			action.mOnActionFailedListener = new OnActionFailedListener() {
				@Override
				public void onActionFailed(String error) {
					done(error);
				}
			};

			action.mOnActionPartiallyDoneListener = new OnActionPartiallyDoneListener() {
				@Override
				public void onActionPartiallyDone(double percentage) {
					IMActionUserUploadMainPhoto.this.done(0.01 * percentage
							* action.getTotalLengthExpected()
							/ (action.getTotalLengthExpected() + 150));
					DTLog.sign("percentage:" + percentage);
				}
			};

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					mFileID = action.mFileID;
					nextStep();
				}
			};

			action.begin();
		}
			break;
		case 3: {
			if (IMPrivateMyself.getInstance().getUID() != getOwnerUID()) {
				done();
				return;
			}

			if (mFileID == null) {
				doneWithIMSDKError();
				return;
			}

			if (mFileID.length() == 0) {
				doneWithIMSDKError();
				return;
			}

			IMCmdUserSetInfo cmd = new IMCmdUserSetInfo();
			String baseInfo = IMPrivateMyself.getInstance().getBaseInfo();
			JSONObject jsonObject;

			try {
				jsonObject = new JSONObject(baseInfo);
			} catch (JSONException e) {
				jsonObject = new JSONObject();
			}

			try {
				jsonObject.put("p", mFileID);
			} catch (JSONException e) {
				e.printStackTrace();
				doneWithIMSDKError();
				return;
			}

			DTLog.sign("upload mFileID: " + mFileID);

			baseInfo = jsonObject.toString();
			cmd.mBaseInfo = jsonObject.toString();

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					doneWithIMSDKError();
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					done(100);
					done();
				}
			};

			cmd.send();
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActionDone() {
		if (mBitmap == null) {
			DTLog.logError();
			return;
		}
		
		if (mBuffer == null) {
			DTLog.logError();
			return;
		}
		
		if (mBuffer.length == 0) {
			DTLog.logError();
			return;
		}
		
		IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(mFileID);
		
		photo.setBitmap(mBitmap);
		photo.setBuffer(mBuffer);
		photo.saveFile();
	}
}
