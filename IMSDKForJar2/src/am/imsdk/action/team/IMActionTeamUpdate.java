package am.imsdk.action.team;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.aacmd.team.IMCmdTeamGetInfo;
import am.imsdk.aacmd.team.IMCmdTeamGetMembers;
import am.imsdk.aacmd.user.IMCmdUserGetInfo;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.teaminfo.IMPrivateTeamInfo;
import am.imsdk.model.teaminfo.IMTeamsMgr;
import am.imsdk.model.userinfo.IMUsersMgr;

// a. 获取Team信息
// b.1 获取Team成员列表
// b.2 获取Team成员列表customUserID
public final class IMActionTeamUpdate extends IMAction {
	public long mTeamID;
	private boolean mStepADone = false;
	private boolean mStepBDone = false;
	private JSONObject mJsonObjectTeamInfo;
	private ArrayList<Long> mAryUIDs = new ArrayList<Long>();

	public IMActionTeamUpdate() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (mTeamID <= 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			{
				IMCmdTeamGetInfo cmd = new IMCmdTeamGetInfo();

				cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
					@Override
					public void onCommonFailed(FailedType type, long errorCode,
							JSONObject errorJsonObject) throws JSONException {
						commonFailedDealWithJudge(type);
					}
				};

				cmd.mOnRecvEndListener = new OnRecvEndListener() {
					@Override
					public void onRecvEnd(JSONObject jsonObject) throws JSONException {
						JSONArray jsonArray = jsonObject.getJSONArray("teamInfolist");
						
						if (jsonArray.length() == 0) {
							if (IMPrivateMyself.getInstance().removeTeam(mTeamID)) {
								IMPrivateMyself.getInstance().saveFile();
							}

							return;
						}

						if (jsonArray.length() != 1) {
							doneWithIMSDKError();
							return;
						}

						mJsonObjectTeamInfo = (JSONObject) jsonArray.get(0);

						if (!(mJsonObjectTeamInfo instanceof JSONObject)) {
							doneWithIMSDKError();
							return;
						}
						
						long teamID = mJsonObjectTeamInfo.getLong("teamid");

						if (teamID == 0) {
							doneWithServerError();
							return;
						}
						
						if (teamID != mTeamID) {
							doneWithIMSDKError();
							return;
						}

						mStepADone = true;

						if (mStepBDone) {
							done();
						}
					}
				};

				cmd.addTeamID(mTeamID);
				cmd.send();
			}

			{
				IMCmdTeamGetMembers cmd = new IMCmdTeamGetMembers();

				cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
					@Override
					public void onCommonFailed(FailedType type, long errorCode,
							JSONObject errorJsonObject) throws JSONException {
						commonFailedDealWithJudge(type);
					}
				};

				cmd.mOnRecvEndListener = new OnRecvEndListener() {
					@Override
					public void onRecvEnd(JSONObject jsonObject) throws JSONException {
						mAryUIDs.clear();
						
						JSONArray jsonArray = jsonObject.getJSONArray("members");

						for (int i = 0; i < jsonArray.length(); i++) {
							long uid = jsonArray.getLong(i);

							if (uid == 0) {
								DTLog.logError();
								return;
							}

							mAryUIDs.add(uid);
						}
						
						nextStep();
					}
				};

				cmd.mTeamID = mTeamID;
				cmd.send();
			}
		}
			break;
		case 2: {
			if (mAryUIDs.size() == 0) {
				mStepBDone = true;

				if (mStepADone) {
					done();
				}

				return;
			}

			IMCmdUserGetInfo cmd = new IMCmdUserGetInfo();
			boolean needSend = false;

			for (long uid : mAryUIDs) {
				if (uid == 0) {
					DTLog.logError();
					continue;
				}
				
				String customUserID = IMUsersMgr.getInstance().getCustomUserID(uid);

				if (customUserID.length() == 0) {
					cmd.addUID(uid);
					needSend = true;
				}
			}

			if (!needSend) {
				mStepBDone = true;

				if (mStepADone) {
					done();
				}

				return;
			}

			cmd.addProperty("phonenum");

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					mStepBDone = true;

					if (mStepADone) {
						done();
					}

					return;
				}
			};

			cmd.send();
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActionDone() {
		IMPrivateMyself.getInstance().addTeamID(mTeamID);
		IMPrivateTeamInfo teamInfo = IMTeamsMgr.getInstance().getTeamInfo(mTeamID);
		
		try {
			teamInfo.parseServerData(mJsonObjectTeamInfo);
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			DTLog.log(mJsonObjectTeamInfo.toString());
			return;
		}
		
		if (mAryUIDs.size() == 0) {
			DTLog.logError();
			return;
		}
		
		teamInfo.setUIDList(mAryUIDs);
		teamInfo.saveFile();
		
		DTNotificationCenter.getInstance().postNotification(
				"TeamUpdated:" + mTeamID);
	}
}
