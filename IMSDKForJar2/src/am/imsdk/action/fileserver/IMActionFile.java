package am.imsdk.action.fileserver;

import am.imsdk.action.IMAction;

public abstract class IMActionFile extends IMAction {
	public static String IM_FILE_URL_DOMAIN_NAME = "file.imsdk.im";
	public static String IM_FILE_URL_REAL_IP = "203.195.162.110";

	abstract public long getTotalLengthExpected();
	
	protected long mLengthFinished;
	protected long mLengthOffset;
}
