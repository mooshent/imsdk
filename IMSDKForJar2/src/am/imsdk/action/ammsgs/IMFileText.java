package am.imsdk.action.ammsgs;

import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTTool;

public final class IMFileText extends DTLocalModel {
	String mFileID;
	String mText;
	
	public IMFileText() {
		setDirectory("IFT", 0);
		setDecryptedDirectory("IMFileText", 0);
	}
	
	@Override
	public boolean generateLocalFullPath() {
		if (mFileID.length() == 0) {
			return false;
		}
		
		mLocalFileName = DTTool.getMD5String(mFileID);
		mDecryptedLocalFileName = mFileID;
		return true;
	}
}
