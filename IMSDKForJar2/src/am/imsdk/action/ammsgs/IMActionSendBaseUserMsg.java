package am.imsdk.action.ammsgs;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.imsdk.aacmd.im.IMCmdIMSendUserMsg;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.kefu.IMCustomerServiceMgr;
import am.imsdk.model.userinfo.IMUsersMgr;

// 发送各种类型的UserMsg
// 不负责本地存储
// 不负责大容量消息拆分
// 1. 获取uid
// 2. 发送文本消息
public final class IMActionSendBaseUserMsg extends IMAction {
	public IMUserMsg mUserMsg;

	public IMActionSendBaseUserMsg() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (mUserMsg == null) {
			doneWithIMSDKError();
			return;
		}

		if (!IMParamJudge.isCustomUserIDLegal(mUserMsg.mToCustomUserID)) {
			doneWithIMSDKError();
			return;
		}

		if (!IMParamJudge.isCustomUserIDLegal(mUserMsg.mFromCustomUserID)) {
			doneWithIMSDKError();
			return;
		}

		if (!mUserMsg.mFromCustomUserID.equals(IMPrivateMyself.getInstance()
				.getCustomUserID())) {
			doneWithIMSDKError();
			return;
		}

		if (mUserMsg.mContent == null) {
			mUserMsg.mContent = "";
		}

		if (mUserMsg.mContent.length() > IMActionSendUserMsg.IMTextMaxLength) {
			doneWithIMSDKError();
			return;
		}

		if (mUserMsg.mClientSendTime == 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			checkBeginGetUID(mUserMsg.mToCustomUserID);
		}
			break;
		case 2: {
			// 先判断是否发给客服
			long uid = IMCustomerServiceMgr.getInstance().getUID(
					mUserMsg.mToCustomUserID);

			if (uid == 0) {
				// 非客服则判断普通用户
				uid = IMUsersMgr.getInstance().getUID(mUserMsg.mToCustomUserID);
			}

			if (uid == 0) {
				doneWithIMSDKError();
				return;
			}

			IMCmdIMSendUserMsg cmd = new IMCmdIMSendUserMsg();

			cmd.mToUID = uid;
			cmd.mContent = mUserMsg.mContent;
			cmd.mUserMsgType = mUserMsg.mUserMsgType;

			if (mUserMsg.mUserMsgType == UserMsgType.IMSDKBlacklist) {
				cmd.mNeedApns = false;
			} else {
				cmd.mNeedApns = true;
			}

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					mUserMsg.mMsgID = jsonObject.getLong("msgid");
					mUserMsg.mServerSendTime = jsonObject.getLong("sendtime");
					done();
				}
			};

			cmd.send();
		}
			break;
		default:
			break;
		}
	}
}
