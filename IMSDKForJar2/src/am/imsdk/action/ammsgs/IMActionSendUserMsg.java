package am.imsdk.action.ammsgs;

import imsdk.data.localchatmessagehistory.IMChatMessage;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.action.IMAction;
import am.imsdk.action.fileserver.IMActionUploadFile;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.amim.IMUserMsgsMgr;
import am.imsdk.serverfile.audio.IMAudio;
import am.imsdk.serverfile.audio.IMAudiosMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;

// 发送各种类型的UserMsg
// 只负责大容量消息转文件
// 维护内存IMUserMsgsMgr
// 1. 上传文件
// 2. 发送文本消息
public final class IMActionSendUserMsg extends IMAction {
	public static final int IMTextMaxLength = 400;
	public IMUserMsg mUserMsg;
	private IMUserMsg mRealUserMsg;
	private byte[] mBuffer;

	public IMActionSendUserMsg() {
		mStepCount = 2;
	}

	@Override
	public void begin() {
		if (mUserMsg == null) {
			doneWithIMSDKError();
			return;
		}

		super.begin();

		mUserMsg.mStatus = IMChatMessage.SENDING_OR_RECVING;

		if (!mUserMsg.saveFile()) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionBegan() {
		if (mUserMsg == null) {
			doneWithIMSDKError();
			return;
		}

		if (mUserMsg.mUserMsgType == null) {
			doneWithIMSDKError();
			return;
		}

		if (mUserMsg.mUserMsgType == UserMsgType.Unit) {
			doneWithIMSDKError();
			return;
		}

		if (mUserMsg.mIsRecv) {
			doneWithIMSDKError();
			return;
		}

		if (!IMParamJudge.isCustomUserIDLegal(mUserMsg.mToCustomUserID)) {
			doneWithIMSDKError();
			return;
		}

		if (mUserMsg.mContent == null) {
			mUserMsg.mContent = "";
		}

		if (!IMParamJudge.isCustomUserIDLegal(mUserMsg.mFromCustomUserID)) {
			mUserMsg.mFromCustomUserID = IMPrivateMyself.getInstance()
					.getCustomUserID();
		}

		if (!mUserMsg.mFromCustomUserID.equals(IMPrivateMyself.getInstance()
				.getCustomUserID())) {
			doneWithIMSDKError();
			return;
		}

		if (mUserMsg.mClientSendTime == 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			switch (mUserMsg.mUserMsgType) {
			case Normal:
			case Custom: {
				if (mUserMsg.mContent.length() <= IMTextMaxLength) {
					nextStep();
					return;
				}

				// 构造FileText
				IMFileText fileText = IMFileTextsMgr.getInstance().getFileTextWithText(
						mUserMsg.getFileID());

				fileText.saveFile();

				try {
					mBuffer = DTTool.getBase64EncodedString(fileText.mText).getBytes(
							"UTF8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					doneWithIMSDKError();
					return;
				}

				mRealUserMsg = new IMUserMsg();

				mRealUserMsg.mFromCustomUserID = IMPrivateMyself.getInstance()
						.getCustomUserID();
				mRealUserMsg.mToCustomUserID = mUserMsg.mToCustomUserID;
				mRealUserMsg.mClientSendTime = mUserMsg.mClientSendTime;

				if (mUserMsg.mUserMsgType == UserMsgType.Normal) {
					mRealUserMsg.mUserMsgType = UserMsgType.NormalFileText;
				} else {
					mRealUserMsg.mUserMsgType = UserMsgType.CustomFileText;
				}

				mRealUserMsg.mContent = fileText.mFileID;
			}
				break;
			case Audio: {
				IMAudio audio = IMAudiosMgr.getInstance()
						.getAudio(mUserMsg.getFileID());

				if (audio.mBuffer == null || audio.mBuffer.length == 0) {
					doneWithIMSDKError();
					return;
				}

				if (!audio.isLocalFileExist()) {
					audio.saveFile();
				}

				mBuffer = audio.mBuffer;
			}
				break;
			case Photo: {
				IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
						mUserMsg.getFileID());

				if (photo.getBuffer() == null || photo.getBufferLength() == 0) {
					if (!photo.readFromFile()) {
						doneWithIMSDKError();
						return;
					}
				}

				if (!photo.isLocalFileExist()) {
					photo.saveFile();
				}

				mBuffer = photo.getBuffer();
			}
				break;
			default:
				nextStep();
				return;
			}

			final IMActionUploadFile action = new IMActionUploadFile();

			action.mOnActionFailedListener = new OnActionFailedListener() {
				@Override
				public void onActionFailed(String error) {
					done(error);
				}
			};

			action.mOnActionDoneListener = new OnActionDoneListener() {
				@Override
				public void onActionDone() {
					switch (mUserMsg.mUserMsgType) {
					case Normal:
					case Custom: {
						if (mRealUserMsg == null) {
							doneWithIMSDKError();
							return;
						}

						IMFileText fileText = IMFileTextsMgr.getInstance()
								.getFileTextWithFileID(mUserMsg.getFileID());

						if (fileText == null) {
							doneWithIMSDKError();
							return;
						}

						if (!fileText.isLocalFileExist()) {
							doneWithIMSDKError();
							return;
						}

						fileText.removeFile();
						IMFileTextsMgr.getInstance().replaceClientFileID(
								fileText.mFileID, action.mFileID);
						fileText.mFileID = action.mFileID;
						fileText.saveFile();
					}
						break;
					case Audio: {
						mRealUserMsg = mUserMsg;

						IMAudio audio = IMAudiosMgr.getInstance().getAudio(
								mUserMsg.getFileID());

						audio.removeFile();
						IMAudiosMgr.getInstance().replaceClientFileID(audio.mFileID,
								action.mFileID);
						audio.mFileID = action.mFileID;
						audio.saveFile();
					}
						break;
					case Photo: {
						mRealUserMsg = mUserMsg;

						IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
								mUserMsg.getFileID());

						photo.removeFile();
						IMImagesMgr.getInstance().replacePhotoFileID(photo.mFileID,
								action.mFileID);
						photo.mFileID = action.mFileID;
						photo.saveFile();
					}
						break;
					default:
						doneWithIMSDKError();
						return;
					}

					if (mRealUserMsg == null) {
						doneWithIMSDKError();
						return;
					}

					mRealUserMsg.setFileID(action.mFileID);
					nextStep();
				}
			};

			action.mBuffer = mBuffer;
			action.begin();
		}
			break;
		case 2: {
			IMActionSendBaseUserMsg action = new IMActionSendBaseUserMsg();

			if (mRealUserMsg != null) {
				action.mUserMsg = mRealUserMsg;
			} else {
				action.mUserMsg = mUserMsg;
			}

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					done();
				}
			};

			action.mOnActionFailedListener = new OnActionFailedListener() {
				@Override
				public void onActionFailed(String error) {
					done(error);
				}
			};

			action.begin();
		}
		default:
			break;
		}
	}

	@Override
	public void onActionDone() {
		if (mUserMsg.mContent.length() > IMTextMaxLength) {
			mRealUserMsg.mStatus = IMChatMessage.SUCCESS;
			mRealUserMsg.saveFile();
			mUserMsg.mMsgID = mRealUserMsg.mMsgID;
			mUserMsg.mServerSendTime = mRealUserMsg.mServerSendTime;
		}

		IMUserMsgsMgr.getInstance().moveUnsentUserMsgToBeSent(mUserMsg);
		mUserMsg.mStatus = IMChatMessage.SUCCESS;
		mUserMsg.saveFile();

		DTNotificationCenter.getInstance().postNotification(
				mUserMsg.getSendStatusChangedNotificationKey());
	}

	@Override
	public void onActionFailed(String error) {
		mUserMsg.mStatus = IMChatMessage.FAILURE;
		mUserMsg.saveFile();
		DTNotificationCenter.getInstance().postNotification(
				mUserMsg.getSendStatusChangedNotificationKey());
	}

	private static class IMFileText extends DTLocalModel {
		public String mFileID;
		public String mText;

		public IMFileText() {
			setDirectory("IFT", 0);
			setDecryptedDirectory("IMFileText", 0);
		}

		@Override
		public boolean generateLocalFullPath() {
			if (mFileID.length() == 0) {
				return false;
			}

			mLocalFileName = DTTool.getMD5String(mFileID);
			mDecryptedLocalFileName = mFileID;
			return true;
		}
	}

	private static class IMFileTextsMgr {
		private HashMap<String, IMFileText> mMapFileTexts = new HashMap<String, IMActionSendUserMsg.IMFileText>();

		public IMFileText getFileTextWithFileID(String fileID) {
			IMFileText fileText = mMapFileTexts.get(fileID);

			if (fileText != null) {
				if (!(fileText instanceof IMFileText)) {
					DTLog.logError();
					return null;
				}

				return fileText;
			}

			fileText = new IMFileText();

			fileText.mFileID = fileID;
			fileText.readFromFile();
			mMapFileTexts.put(fileID, fileText);

			return fileText;
		}

		public IMFileText getFileTextWithText(String text) {
			IMFileText fileText = new IMFileText();

			for (int i = 0; i < 100000000; i++) {
				fileText.mFileID = i + "";

				if (!fileText.isLocalFileExist()) {
					break;
				}
			}

			if (fileText.isLocalFileExist()) {
				DTLog.logError();
				return null;
			}

			fileText.mText = text;
			mMapFileTexts.put(fileText.mFileID, fileText);

			return fileText;
		}

		public void replaceClientFileID(String clientFileID, String fileID) {
			IMFileText fileText = mMapFileTexts.get(clientFileID);

			if (fileText != null) {
				mMapFileTexts.put(clientFileID, null);
				mMapFileTexts.put(fileID, fileText);
			}
		}

		// singleton
		private volatile static IMFileTextsMgr sSingleton;

		private IMFileTextsMgr() {
		}

		public static IMFileTextsMgr getInstance() {
			if (sSingleton == null) {
				synchronized (IMFileTextsMgr.class) {
					if (sSingleton == null) {
						sSingleton = new IMFileTextsMgr();
					}
				}
			}

			return sSingleton;
		}
		// singleton end
	}
}
