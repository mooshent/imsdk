package am.imsdk.action.ammsgs;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.imsdk.aacmd.im.IMCmdIMSendTeamMsg;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amimteam.IMTeamMsg;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;

// 发送各种类型的TeamMsg
// 不负责本地存储
// 不负责大容量消息拆分
// 1. 发送文本消息
public final class IMActionSendBaseTeamMsg extends IMAction {
	public IMTeamMsg mTeamMsg;

	public IMActionSendBaseTeamMsg() {
		mStepCount = 1;
	}

	@Override
	public void onActionBegan() {
		if (mTeamMsg == null) {
			doneWithIMSDKError();
			return;
		}

		if (mTeamMsg.mTeamMsgType == null) {
			doneWithIMSDKError();
			return;
		}

		if (!IMParamJudge.isTeamIDLegal(mTeamMsg.mTeamID)) {
			doneWithIMSDKError();
			return;
		}
		
		if (!IMParamJudge.isCustomUserIDLegal(mTeamMsg.mFromCustomUserID)) {
			doneWithIMSDKError();
			return;
		}
		
		if (!mTeamMsg.mFromCustomUserID.equals(IMPrivateMyself.getInstance().getCustomUserID())) {
			doneWithIMSDKError();
			return;
		}

		if (mTeamMsg.mContent == null) {
			mTeamMsg.mContent = "";
		}

		if (mTeamMsg.mContent.length() > IMActionSendTeamMsg.IMTextMaxLength) {
			doneWithIMSDKError();
			return;
		}

		if (mTeamMsg.mClientSendTime == 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			if (mTeamMsg.mTeamID == 0) {
				doneWithIMSDKError();
				return;
			}
			
			IMCmdIMSendTeamMsg cmd = new IMCmdIMSendTeamMsg();

			cmd.mToTeamID = mTeamMsg.mTeamID;
			cmd.mContent = mTeamMsg.mContent;
			cmd.mTeamMsgType = mTeamMsg.mTeamMsgType;

            if (mTeamMsg.mTeamMsgType == TeamMsgType.IMSDKGroupInfoUpdate || 
            		mTeamMsg.mTeamMsgType == TeamMsgType.IMSDKGroupNewUser || 
            		mTeamMsg.mTeamMsgType == TeamMsgType.IMSDKGroupUserRemoved || 
            		mTeamMsg.mTeamMsgType == TeamMsgType.IMSDKGroupDeleted || 
            		mTeamMsg.mTeamMsgType == TeamMsgType.IMSDKGroupQuit || 
            		mTeamMsg.mTeamMsgType == TeamMsgType.IMSDKGroupDeleted || 
            		mTeamMsg.mTeamMsgType == TeamMsgType.Custom) {
            		cmd.mNeedAPNS = false;
            } else {
            		cmd.mNeedAPNS = true;
            }
            
			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					long msgID = jsonObject.getLong("msgid");
					long serverSendTime = jsonObject.getLong("sendtime");

					mTeamMsg.mMsgID = msgID;
					mTeamMsg.mServerSendTime = serverSendTime;
					done();
				}
			};

			cmd.send();
		}
			break;
		default:
			break;
		}
	}
}
