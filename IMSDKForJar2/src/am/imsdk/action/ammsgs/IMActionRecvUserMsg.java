package am.imsdk.action.ammsgs;

import org.json.JSONException;
import org.json.JSONObject;

import am.imsdk.action.IMAction;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsgsMgr;

// 1. 获取CustomUserID
// 2. 判断是否Unit
public final class IMActionRecvUserMsg extends IMAction {
	public JSONObject mRecvJsonObject;
	private long mFromUID;
	public IMUserMsg mUserMsg;

	public IMActionRecvUserMsg() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (mRecvJsonObject == null) {
			doneWithIMSDKError();
			return;
		}

		if (mUserMsg != null) {
			doneWithIMSDKError();
			return;
		}

		try {
			mFromUID = mRecvJsonObject.getLong("fromuid");
		} catch (JSONException e) {
			e.printStackTrace();
			doneWithIMSDKError();
			return;
		}

		if (mFromUID == 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			checkBeginGetCustomUserID(mFromUID);
		}
			break;
		case 2: {
			mUserMsg = IMUserMsgsMgr.getInstance().getRecvUserMsg(mRecvJsonObject);

			if (mUserMsg == null) {
				doneWithIMSDKError();
				return;
			}
			
			if (!IMParamJudge.isCustomUserIDLegal(mUserMsg.mFromCustomUserID)) {
				doneWithIMSDKError();
				return;
			}
			
			done();
		}
			break;
		default:
			break;
		}
	}
}
