package am.imsdk.action.group;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.aacmd.im.IMCmdIMSendTeamMsg;
import am.imsdk.aacmd.im.IMCmdIMSendUserMsg;
import am.imsdk.aacmd.team.IMCmdTeamAddMember;
import am.imsdk.aacmd.team.IMCmdTeamRemoveMember;
import am.imsdk.action.IMAction;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;
import am.imsdk.model.userinfo.IMUsersMgr;

// 1. 获取uid
// 2. 添加或删除成员
// 3.a. 发送通知给对方（如果是自己就不用发）
// 3.b. 发送通知给群组其他成员
public final class IMActionOperateGroupMember extends IMAction {
	public String mCustomUserID = "";
	public long mTeamID;
	public boolean mRemoveAction = false;
	private long mUID;
	private boolean mStep3ADone = false;
	private boolean mStep3BDone = false;

	public IMActionOperateGroupMember() {
		mStepCount = 3;
	}

	@Override
	public void onActionBegan() {
		if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)) {
			doneWithIMSDKError();
			return;
		}

		if (mTeamID == 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			checkBeginGetUID(mCustomUserID);
		}
			break;
		case 2: {
			mUID = IMUsersMgr.getInstance().getUID(mCustomUserID);

			if (mUID == 0) {
				doneWithIMSDKError();
				return;
			}

			if (mRemoveAction) {
				IMCmdTeamRemoveMember cmd = new IMCmdTeamRemoveMember();

				cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
					@Override
					public void onCommonFailed(FailedType type, long errorCode,
							JSONObject errorJsonObject) throws JSONException {
						commonFailedDealWithJudge(type);
					}
				};

				cmd.mOnRecvEndListener = new OnRecvEndListener() {
					@Override
					public void onRecvEnd(JSONObject jsonObject) throws JSONException {
						nextStep();
					}
				};

				cmd.mTeamID = mTeamID;
				cmd.mUID = mUID;
				cmd.send();
			} else {
				IMCmdTeamAddMember cmd = new IMCmdTeamAddMember();

				cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
					@Override
					public void onCommonFailed(FailedType type, long errorCode,
							JSONObject errorJsonObject) throws JSONException {
						commonFailedDealWithJudge(type);
					}
				};

				cmd.mOnRecvEndListener = new OnRecvEndListener() {
					@Override
					public void onRecvEnd(JSONObject jsonObject) throws JSONException {
						nextStep();
					}
				};

				cmd.mTeamID = mTeamID;
				cmd.addUID(mUID);
				cmd.send();
			}
		}
			break;
		case 3: {
			if (mUID == IMPrivateMyself.getInstance().getUID()) {

				IMPrivateMyself.getInstance().removeTeam(mTeamID);
			} else {
				IMCmdIMSendUserMsg cmd = new IMCmdIMSendUserMsg();

				cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
					@Override
					public void onCommonFailed(FailedType type, long errorCode,
							JSONObject errorJsonObject) throws JSONException {
						commonFailedDealWithJudge(type);
					}
				};

				cmd.mOnRecvEndListener = new OnRecvEndListener() {
					@Override
					public void onRecvEnd(JSONObject jsonObject) throws JSONException {
						mStep3ADone = true;
						if (mStep3BDone) {
							nextStep();
						}
					}
				};

				cmd.mToUID = mUID;
				cmd.mUserMsgType = mRemoveAction ? UserMsgType.IMSDKGroupNoticeBeRemoved
						: UserMsgType.IMSDKGroupNoticeBeAdded;

				String groupID = DTTool.getSecretString(mTeamID);

				cmd.mContent = groupID;
				cmd.send();
			}

			{
				// 3.b. 发送通知给群组其他成员
				IMCmdIMSendTeamMsg cmd = new IMCmdIMSendTeamMsg();

				cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
					@Override
					public void onCommonFailed(FailedType type, long errorCode,
							JSONObject errorJsonObject) throws JSONException {
						commonFailedDealWithJudge(type);
					}
				};

				cmd.mOnRecvEndListener = new OnRecvEndListener() {
					@Override
					public void onRecvEnd(JSONObject jsonObject) throws JSONException {
						if (mUID == IMPrivateMyself.getInstance().getUID()) {
							nextStep();
						} else {
							mStep3BDone = true;

							if (mStep3ADone) {
								nextStep();
							}
						}
					}
				};

				// 创建content
				JSONObject jsonObject = new JSONObject();

				try {
					jsonObject.put("uid", IMUsersMgr.getInstance()
							.getUID(mCustomUserID));
					jsonObject.put("customUserID", mCustomUserID);
				} catch (JSONException e) {
					e.printStackTrace();
					DTLog.logError();
					return;
				}

				cmd.mToTeamID = mTeamID;
				cmd.mContent = jsonObject.toString();
				cmd.mTeamMsgType = mRemoveAction ? TeamMsgType.IMSDKGroupUserRemoved
						: TeamMsgType.IMSDKGroupNewUser;
				cmd.send();
			}
		}
			break;
		default:
			break;
		}
	}

	@Override
	public void onActionPartiallyDone(double percentage) {
	}

	@Override
	public void onActionDone() {
	}

	@Override
	public void onActionFailed(String error) {
	}
}
