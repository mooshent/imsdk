package am.imsdk.action;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.imsdk.aacmd.user.IMCmdUserGetInfo;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.userinfo.IMUsersMgr;

// 1. 获取uid
// 2. 获取CustomUserInfo
public final class IMActionUserRequestCustomUserInfo extends IMAction {
	public String mCustomUserID;
	private long mUID;
	
	public IMActionUserRequestCustomUserInfo() {
		mStepCount = 2;
	}

	@Override
	public void onActionBegan() {
		if (!IMParamJudge.isCustomUserIDLegal(mCustomUserID)) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1:
		{
			checkBeginGetUID(mCustomUserID);
		}
			break;
		case 2:
		{
			mUID = IMUsersMgr.getInstance().getUID(mCustomUserID);
			
			if (mUID == 0) {
				doneWithIMSDKError();
				return;
			}
			
			IMCmdUserGetInfo cmd = new IMCmdUserGetInfo();
			
			cmd.addUID(mUID);
			cmd.addProperty("exinfo");
			
			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
					commonFailedDealWithJudge(type);
				}
			};
			
			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					done();
				}
			};
			
			cmd.send();
		}
			break;
		default:
			break;
		}
	}
}
