package am.imsdk.action.relation;

import imsdk.data.IMMyself;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.c.socket.DTCmd.FailedType;
import am.dtlib.model.c.socket.DTCmd.OnCommonFailedListener;
import am.dtlib.model.c.socket.DTCmd.OnRecvEndListener;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.aacmd.team.IMCmdTeamAddMember2WB;
import am.imsdk.aacmd.team.IMCmdTeamAddMember2WB.WBType;
import am.imsdk.action.IMAction;
import am.imsdk.action.ammsgs.IMActionSendUserMsg;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.kefu.IMCustomerServiceMgr;
import am.imsdk.model.userinfo.IMUsersMgr;

public final class IMActionAgreeToFriendRequest extends IMAction {
	public String mToCustomUserID;
	private boolean mCmdDone = false;
	private boolean mActionDone = false;

	public IMActionAgreeToFriendRequest() {
		mStepCount = 1;
	}

	@Override
	public void onActionBegan() {
		if (IMCustomerServiceMgr.getInstance().getUID(mToCustomUserID) != 0) {
			done("You can't build friendship with customer service");
		}

		if (!IMParamJudge.isCustomUserIDLegal(mToCustomUserID)) {
			doneWithIMSDKError();
			return;
		}

		if (IMUsersMgr.getInstance().getUID(mToCustomUserID) == 0) {
			doneWithIMSDKError();
			return;
		}
	}

	@Override
	public void onActionStepBegan(int stepNumber) {
		switch (stepNumber) {
		case 1: {
			IMActionSendUserMsg action = new IMActionSendUserMsg();

			action.mOnActionFailedListener = new OnActionFailedListener() {
				@Override
				public void onActionFailed(String error) {
					done(error);
				}
			};

			action.mOnActionDoneEndListener = new OnActionDoneEndListener() {
				@Override
				public void onActionDoneEnd() {
					if (mCmdDone) {
						done();
						return;
					}

					mActionDone = true;
				}
			};

			final IMUserMsg userMsg = new IMUserMsg();

			userMsg.mUserMsgType = UserMsgType.IMSDKAgreeToFriendRequest;
			userMsg.mToCustomUserID = mToCustomUserID;
			userMsg.mContent = "";
			userMsg.mFromCustomUserID = IMMyself.getCustomUserID();
			userMsg.mClientSendTime = mBeginTime;

			action.mUserMsg = userMsg;
			action.mTimeoutInterval = mTimeoutInterval;
			action.begin();

			IMCmdTeamAddMember2WB cmd = new IMCmdTeamAddMember2WB();

			cmd.mOnCommonFailedListener = new OnCommonFailedListener() {
				@Override
				public void onCommonFailed(FailedType type, long errorCode,
						JSONObject errorJsonObject) throws JSONException {
				}
			};

			cmd.mOnRecvEndListener = new OnRecvEndListener() {
				@Override
				public void onRecvEnd(JSONObject jsonObject) throws JSONException {
					DTNotificationCenter.getInstance().postNotification(
							"BuildFriendshipWithUser", userMsg);

					if (mActionDone) {
						done();
						return;
					}

					mCmdDone = true;
				}
			};

			cmd.mType = WBType.Friends;
			cmd.mUID = userMsg.getToUID();
			cmd.send();
		}
			break;
		default:
			break;
		}
	}
}
