package am.imsdk.ui.views;

import imsdk.data.IMMyself;
import imsdk.views.IMResizeLayout;
import imsdk.views.IMResizeLayout.OnResizeListener;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import am.imsdk.demo.gif.EmotionManager;
import am.imsdk.demo.gif.GifEmotionUtils;
import am.imsdk.demo.util.CPResourceUtil;
import am.imsdk.demo.util.DensityUtils;
import am.imsdk.demo.util.FacesXml;
import am.imsdk.demo.util.FileUtils;
import am.imsdk.model.IMAudioSender;
import am.imsdk.model.IMAudioSender.OnAudioSenderListener;
import am.imsdk.model.a2.IMMyselfRecentContacts2;
import am.imsdk.ui.adapter.IMBaseChatViewAdapter;
import am.imsdk.ui.views.AudioRecorderDialog.OnErrorListener;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public abstract class IMBaseChatView extends FrameLayout implements
		View.OnClickListener, OnErrorListener, View.OnLongClickListener {

	protected WeakReference<Activity> mActivityReference;

	public static final int MSG_SEND_CLASSICAL_EMOTION = 0x05;
	protected static final int CHOOSE_PHOTO = 0; // 用于标识从用户相册选择照片
	protected static final int TAKE_PHOTO = 1; // 用于标识拍照
	public static final int MSG_SEND_EMOTION = 0x04;

	protected final int INPUT_STATE_NOTHING = 0;
	protected final int INPUT_STATE_TEXT = 1;
	protected final int INPUT_STATE_FACE = 2;
	protected final int INPUT_STATE_AUDIO = 3;
	protected final int INPUT_STATE_PLUS = 10;

	protected int mInputState = INPUT_STATE_TEXT;

	// config
	// titlebar
	protected boolean mTitleBarVisible = false;

	// content
	protected boolean mUserMainPhotoVisible = false;
	protected int mUserMainPhotoCornerRadius = 0;
	protected boolean mUserNameVisible = false;
	protected int mMaxGifCountInMessage = DEFAULTDISPLAYMAXGIFNUMINMESSAGE;
	protected final static int DEFAULTDISPLAYMAXGIFNUMINMESSAGE = 9;

	// model
	protected InputMethodManager mInputMethodManager;
	protected IMBaseChatViewAdapter mAdapter;

	// temp data
	protected int mKeyboardHeight;
	protected int mLastHeightMeasureSpec;
	protected Uri mImageUri;

	protected int mScrollState;
	protected int mFirstVisibleItem;
	protected int mLastVisibleItem;

	protected int mResultCode;
	protected Intent mData;

	// ui
	// root
	protected IMResizeLayout mResizeLayout;

	// titlebar
	protected ImageButton mBackBtn;
	protected TextView mTitleTextView;
	protected Button mTitleRightBtn;

	// content
	protected ListView mListView;
	
	protected View mSendView;
	protected View mEditLayout;

	// footer
	protected ImageButton mFaceBtn;
	protected ImageButton mPlusBtn;
	protected EditText mContentEditText;
	protected ImageButton mSendOrAudioSwitchBtn;
	protected Button mRecordBtn;
	protected Button mSendBtn;

	// 表情 或 +按钮内容
	protected View mFooterView;

	// 表情内容
	protected ViewPager mFacesViewPager;
	protected View mFacesViewPagerIndexPanel;

	// +内容
	protected View mPlusContentView;
	protected ImageButton mChoosePhotoBtn;
	protected ImageButton mTakePhotoBtn;

	// 录音提示框
	protected AudioRecorderDialog mVoiceRecordDialog;

	// 表情管理
	protected EmotionManager mEmotionManager;
	protected GifEmotionUtils mGifEmotionUtils;

	protected ArrayList<Integer> mIDs;
	
	public static interface OnChatViewTouchListener {
		public void onTouch(View v);
	}

	public IMBaseChatView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public IMBaseChatView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		if (!(context instanceof Activity)) {
			throw new RuntimeException(
					"context 上下文对象必须为 Activity 类型， 比如： MainActivity.this");
		}

		mActivityReference = new WeakReference<Activity>((Activity) context);

		TypedArray typedArray = null;

		try {
			typedArray = context.obtainStyledAttributes(attrs, CPResourceUtil
					.getstyleableIds(mActivityReference.get(), "IMChatView"));

			int idGif = CPResourceUtil.getstyleableId(mActivityReference.get(),
					"IMChatView_max_gif_count_in_message");

			if (typedArray.hasValue(idGif)) {
				mMaxGifCountInMessage = typedArray.getInteger(idGif,
						DEFAULTDISPLAYMAXGIFNUMINMESSAGE);

				if (mMaxGifCountInMessage <= 0) {
					mMaxGifCountInMessage = DEFAULTDISPLAYMAXGIFNUMINMESSAGE;
				}
			} else {
				mMaxGifCountInMessage = DEFAULTDISPLAYMAXGIFNUMINMESSAGE;
			}

			int idTitleBarVisible = CPResourceUtil.getstyleableId(
					mActivityReference.get(), "IMChatView_title_bar_visible");

			if (typedArray.hasValue(idTitleBarVisible)) {
				mTitleBarVisible = typedArray.getBoolean(idTitleBarVisible,
						mTitleBarVisible);
			}

			int idUserMainPhotoVisible = CPResourceUtil.getstyleableId(
					mActivityReference.get(), "IMChatView_main_photo_visible");

			if (typedArray.hasValue(idUserMainPhotoVisible)) {
				mUserMainPhotoVisible = typedArray.getBoolean(idUserMainPhotoVisible,
						mUserMainPhotoVisible);
			}

			int idMainPhotoCornerRadius = CPResourceUtil.getstyleableId(
					mActivityReference.get(), "IMChatView_main_photo_corner_radius");

			if (typedArray.hasValue(idMainPhotoCornerRadius)) {
				mUserMainPhotoCornerRadius = typedArray.getDimensionPixelSize(
						idMainPhotoCornerRadius, mUserMainPhotoCornerRadius);
			}
		} finally {
			if (typedArray != null) {
				typedArray.recycle();
			}
		}

		initViews(context);
		addListeners();
	}

	// open Interface
	// init
	public IMBaseChatView(Activity activity) {
		super(activity);

		if (activity == null) {
			DTLog.logError();
			throw new RuntimeException("context不能为null");
		}

		if (!(activity instanceof Activity)) {
			DTLog.logError();
			throw new RuntimeException("context必须为Activity 如:MainActivity.this");
		}

		initViews(activity);
		addListeners();
	}

	// config
	// titlebar
	public void setTitleBarVisible(boolean visible) {
		if (mActivityReference.get() == null) {
			mTitleBarVisible = visible;
			return;
		}

		if (visible) {
			findViewById(
					CPResourceUtil.getId(mActivityReference.get(),
							"imchatview_titlebar")).setVisibility(View.VISIBLE);
		} else {
			findViewById(
					CPResourceUtil.getId(mActivityReference.get(),
							"imchatview_titlebar")).setVisibility(View.GONE);
		}

		mTitleBarVisible = visible;
	}
	
	/**
	 * 设置操作栏是否可见
	 * @param visible
	 */
	public void setOperationViewVisible(boolean visible) {
		if (mActivityReference.get() == null) {
			return;
		}

		if (visible) {
			findViewById(
					CPResourceUtil.getId(mActivityReference.get(),
							"imchatview_sendview")).setVisibility(View.VISIBLE);
		} else {
			findViewById(
					CPResourceUtil.getId(mActivityReference.get(),
							"imchatview_sendview")).setVisibility(View.GONE);
		}
	}

	public boolean isTitleBarVisible() {
		return mTitleBarVisible;
	}

	// config
	// content listview
	public void setUserMainPhotoVisible(boolean visible) {
		mUserMainPhotoVisible = visible;

		if (mAdapter != null) {
			mAdapter.setUserMainPhotoVisible(visible);
		}
	}

	public boolean isUserMainPhotoVisible() {
		return mUserMainPhotoVisible;
	}

	// config
	// content listView
	public void setUserMainPhotoCornerRadius(int cornerRadius) {
		mUserMainPhotoCornerRadius = cornerRadius;

		if (mAdapter != null) {
			mAdapter.setUserMainPhotoCornerRadius(cornerRadius);
		}
	}

	public int getUserMainPhotoCornerRadius() {
		return mUserMainPhotoCornerRadius;
	}

	// config
	// content listView
	public void setUserNameVisible(boolean visible) {
		mUserNameVisible = visible;

		if (mAdapter != null) {
			mAdapter.setUserNameVisible(visible);
		}
	}

	public boolean isUserNameVisible() {
		return mUserNameVisible;
	}

	// config
	// content listView
	public void setMaxGifCountInMessage(int maxCount) {
		if (maxCount <= 0) {
			throw new RuntimeException("setMaxGifCountInMessage()方法，参数必须大于 0 !");
		}

		if (mGifEmotionUtils != null) {
			mGifEmotionUtils.setShowGifNum(maxCount);
		}

		mMaxGifCountInMessage = maxCount;
	}

	public int getMaxGifCountInMessage() {
		return mMaxGifCountInMessage;
	}

	protected void initIDs() {
		if (mActivityReference.get() == null) {
			return;
		}

		mIDs = new ArrayList<Integer>();

		mIDs.add(CPResourceUtil.getLayoutId(mActivityReference.get(),
				"im_chatview_item_chatting"));// 0
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(), "chatting_item_system"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"chatting_item_system_content"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"chatting_item_left_userhead"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"chatting_item_left_addon"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"chatting_item_left_username"));// 5
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"chatting_item_left_content"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"content_left_image_root"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"content_left_image_progress"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"content_left_image_mask"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(), "content_left_image")); // 10

		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"chatting_item_right_userhead"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"chatting_item_right_addon"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"chatting_item_right_username"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"chatting_item_right_content")); // 14
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"content_right_image_root")); // 15
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"content_right_image_progress"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"content_right_image_mask"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(), "content_right_image"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"chatting_item_right_progress"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(), "chatting_item_left")); // 20

		mIDs.add(CPResourceUtil.getId(mActivityReference.get(), "chatting_item_right"));
		mIDs.add(0);
		mIDs.add(0);

		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"imchatview_sendoraudioswtich"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(), "imchatview_back")); // 25
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(), "imchatview_plus"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(), "imchatview_face"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"imchatview_choosephoto"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(), "imchatview_takephoto"));
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"imchatview_titlebarright"));// 30
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(), "imchatview_root"));
		
		mIDs.add(CPResourceUtil.getLayoutId(mActivityReference.get(),
				"im_chatview_image"));//32 显示大图layout
		
		mIDs.add(CPResourceUtil.getId(mActivityReference.get(),
				"imchatview_send")); //33
	}

	protected void initCtrls() {
		// titlebar
		if (mTitleBarVisible) {
			this.findViewById(
					CPResourceUtil.getId(mActivityReference.get(),
							"imchatview_titlebar")).setVisibility(View.VISIBLE);
		} else {
			this.findViewById(
					CPResourceUtil.getId(mActivityReference.get(),
							"imchatview_titlebar")).setVisibility(View.GONE);
		}

		// title
		mBackBtn = (ImageButton) this.findViewById(mIDs.get(25));
		mTitleTextView = (TextView) this.findViewById(CPResourceUtil.getId(
				mActivityReference.get(), "imchatview_title"));

		mTitleRightBtn = (Button) this.findViewById(mIDs.get(30));

		// content
		mListView = (ListView) this.findViewById(CPResourceUtil.getId(
				mActivityReference.get(), "imchatview_listview"));

		// footer
		mFaceBtn = (ImageButton) this.findViewById(mIDs.get(27));
		mPlusBtn = (ImageButton) this.findViewById(mIDs.get(26));
		mContentEditText = (EditText) this.findViewById(CPResourceUtil.getId(
				mActivityReference.get(), "imchatview_edittext"));
		mContentEditText.requestFocus();
		mSendOrAudioSwitchBtn = (ImageButton) this.findViewById(mIDs.get(24));
		mSendBtn = (Button) this.findViewById(mIDs.get(33));
		mRecordBtn = (Button) this.findViewById(CPResourceUtil.getId(
				mActivityReference.get(), "imchatview_record"));
		
		mEditLayout = this.findViewById(CPResourceUtil.getId(mActivityReference.get(),
				"imchatview_editlayout"));

		mFooterView = this.findViewById(CPResourceUtil.getId(mActivityReference.get(),
				"imchatview_footer"));
		mFacesViewPager = (ViewPager) this.findViewById(CPResourceUtil.getId(
				mActivityReference.get(), "imchatview_facesviewpager"));
		mFacesViewPagerIndexPanel = this.findViewById(CPResourceUtil.getId(
				mActivityReference.get(), "imchatview_facesindexpanel"));
		mPlusContentView = this.findViewById(CPResourceUtil.getId(
				mActivityReference.get(), "imchatview_pluscontent"));
		mChoosePhotoBtn = (ImageButton) this.findViewById(mIDs.get(28));
		mTakePhotoBtn = (ImageButton) this.findViewById(mIDs.get(29));

		mResizeLayout = (IMResizeLayout) this.findViewById(mIDs.get(31));

		mResizeLayout.setOnResizeListener(new OnResizeListener() {
			@Override
			public void OnResize(int w, int h, int oldw, int oldh) {
				if (mKeyboardHeight > DensityUtils.dp2px(mActivityReference.get(), 208)) {
					return;
				}

				if (oldw != 0 && oldh != 0) {
					if (h < oldh) {
						mKeyboardHeight = oldh - h;
						resetFooterViewHeight();
					}
				}
			}
		});
	}

	protected void resetFooterViewHeight() {
		if (mKeyboardHeight <= 0) {
			//默认208dp
			mKeyboardHeight = DensityUtils.dp2px(mActivityReference.get(), 208);
		}
		ViewGroup.LayoutParams footerParams = mFooterView.getLayoutParams();

		footerParams.height = mKeyboardHeight;
		mFooterView.setLayoutParams(footerParams);
		
		//剩余空隙
		int remainingSpace = mKeyboardHeight - 3*DensityUtils.dp2px(mActivityReference.get(), 32) 
				- mFacesViewPagerIndexPanel.getLayoutParams().height;
		int x = (int) (remainingSpace * 0.382 / 2.764);
		int y1 = (int) (remainingSpace * 0.618 / 2.764);
		//int y2 = (int) (remainingSpace / 2.764);
		
		ViewGroup.LayoutParams mFacesParams = mFacesViewPager.getLayoutParams();
		mFacesParams.height = 3*x + 3*DensityUtils.dp2px(mActivityReference.get(), 32);
		mFacesViewPager.setLayoutParams(mFacesParams);
		
		mFooterView.setPadding(0, y1, 0, 0);
	}

	protected void initListeners() {
		mSendOrAudioSwitchBtn.setOnClickListener(this);
		mBackBtn.setOnClickListener(this);
		mPlusBtn.setOnClickListener(this);
		mFaceBtn.setOnClickListener(this);
		mChoosePhotoBtn.setOnClickListener(this);
		mTakePhotoBtn.setOnClickListener(this);
		mTitleRightBtn.setOnClickListener(this);
		mSendBtn.setOnClickListener(this);
	}

	protected void initViews(Context context) {
		mActivityReference = new WeakReference<Activity>((Activity) context);
		FileUtils.register(mActivityReference.get());

		LayoutInflater.from(context).inflate(
				CPResourceUtil.getLayoutId(context, "im_chatview"), this);

		initIDs();

		mInputMethodManager = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);

		initCtrls();
		initListeners();

		mEmotionManager = new EmotionManager(mActivityReference.get(), mFacesViewPager,
				mFacesViewPagerIndexPanel, getHandler());
		mGifEmotionUtils = new GifEmotionUtils(mMaxGifCountInMessage);
	}

	protected OnChatViewTouchListener mOnChatViewTouchListener = new OnChatViewTouchListener() {
		@Override
		public void onTouch(View v) {
			switch (mInputState) {
			case INPUT_STATE_NOTHING:
				break;
			case INPUT_STATE_TEXT:
				mInputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
				break;
			case INPUT_STATE_FACE:
				mEmotionManager.destory();
				mFooterView.setVisibility(View.GONE);
				mInputState = INPUT_STATE_NOTHING;
				break;
			case INPUT_STATE_AUDIO: {
			}
				break;
			case INPUT_STATE_PLUS: {
				mFooterView.setVisibility(View.GONE);
				mInputState = INPUT_STATE_NOTHING;
			}
				break;
			default:
				break;
			}
		}
	};

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if (mLastHeightMeasureSpec > heightMeasureSpec) {
			mEmotionManager.destory();
			mFooterView.setVisibility(GONE);
		}

		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		mLastHeightMeasureSpec = heightMeasureSpec;
	};

	public void setTitle(String title) {
		mTitleTextView.setText(title);
	}

	protected String getChatRoomTitle() {
		return mTitleTextView.getText().toString();
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();

		if (id == mIDs.get(24)) {
			// 发送按钮点击事件
//			onSendOrSwithAudioBtnClick(v);
			onSwitchAudioBtnClick(v);
		} else if (id == mIDs.get(25)) {
			onBackBtnClick(v);// 返回按钮点击事件
		} else if (id == mIDs.get(26)) {
			onSendOrSwithPlusBtnClick(v);// 点击 + 触发的事件
		} else if (id == mIDs.get(27)) {
			onFaceBtnClick(v);// 点击 表情 触发的事件
		} else if (id == mIDs.get(28)) {
			onChoicePhotoBtnClick(v);// 点击 图片 触发的事件
		} else if (id == mIDs.get(29)) {
			onTakePhotoBtnClick(v);// 点击 拍照 触发的事件
		} else if (id == mIDs.get(30)) {
			// onChoiceVideoBtnClick(v);// 点击 视频 触发的事件
		} else if (id == mIDs.get(31)) {
			// onVideoCallClick(v);// 点击 视频通话 触发的事件
		} else if (id == mIDs.get(3)) {
			// onHeadPhotoClick(v,0); // 点击 左侧头像 触发的事件
		} else if (id == mIDs.get(11)) {
			// onHeadPhotoClick(v,1);// 点击 右侧头像 触发的事件
		} else if (id == mIDs.get(12)) {
			// resendMessage(v); // 点击 重发图片 触发的事件
		} else if (id == mIDs.get(33)) {
			onSendBtnClick(v);
		}

		// else if (id == mIDs.get(32)) {
		// // right_TitleBarBtnClick(v);// 点击 右上角的button 触发的事件
		// }
	}

	protected abstract boolean onSendText(String text);

	protected void onSendOrSwithPlusBtnClick(View v) {
//		String text = mContentEditText.getText().toString();
//
//		if ((mInputState == INPUT_STATE_TEXT || mInputState == INPUT_STATE_FACE) && text.length() > 0) {
//			if (onSendText(text)) {
//				mContentEditText.setText("");
//				mBuilder = null;
//			}
//		} else {
//			onPlusBtnClick(v);
//		}
		
		onPlusBtnClick(v);
	}
	
	protected void onSendBtnClick(View v) {
		String text = mContentEditText.getText().toString();
		
		if ((mInputState == INPUT_STATE_TEXT || mInputState == INPUT_STATE_FACE) && text.length() > 0) {
			if (onSendText(text)) {
				mContentEditText.setText("");
				mBuilder = null;
			}
		}
	}

	protected void onBackBtnClick(View v) {
		mInputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
		mActivityReference.get().setResult(mResultCode, mData);
		mActivityReference.get().finish(); // 结束,实际开发中，可以返回主界面
	}

	protected void onPlusBtnClick(View v) {
		if (INPUT_STATE_PLUS == mInputState) {
			mFooterView.setVisibility(GONE);
			mInputState = INPUT_STATE_NOTHING;
			return;
		}

		switch (mInputState) {
		case INPUT_STATE_NOTHING: {
			mFooterView.setVisibility(VISIBLE);
			mFacesViewPager.setVisibility(View.GONE);
			mPlusContentView.setVisibility(View.VISIBLE);
		}
			break;
		case INPUT_STATE_TEXT: {
			mFooterView.setVisibility(VISIBLE);
			mFacesViewPager.setVisibility(View.GONE);
			mPlusContentView.setVisibility(View.VISIBLE);
			mActivityReference.get().getWindow()
					.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
			resetFooterViewHeight();
			mInputMethodManager
					.hideSoftInputFromWindow(mFooterView.getWindowToken(), 0);
		}
			break;
		case INPUT_STATE_AUDIO: {
//			onSwitchAudioBtnClick(mSendOrAudioSwitchBtn);
			
			mSendOrAudioSwitchBtn.setImageDrawable(getResources().getDrawable(
					CPResourceUtil.getDrawableId(mActivityReference.get(),
							"im_chatting_setmode_voice_btn_normal")));
			
			mEditLayout.setVisibility(View.VISIBLE);
			mRecordBtn.setVisibility(View.GONE);
			
			mFooterView.setVisibility(VISIBLE);
			mFacesViewPager.setVisibility(View.GONE);
			mPlusContentView.setVisibility(View.VISIBLE);
		}
			break;
		case INPUT_STATE_FACE: {
			mEmotionManager.destory();
			mFacesViewPager.setVisibility(View.GONE);
			mPlusContentView.setVisibility(View.VISIBLE);
		}
			break;
		case INPUT_STATE_PLUS:
			DTLog.logError();
			break;
		default:
			DTLog.logError();
			break;
		}

		mInputState = INPUT_STATE_PLUS;
	}

	protected void onFaceBtnClick(View v) {
		if (mInputState == INPUT_STATE_FACE) {
			mEmotionManager.destory();
			mFooterView.setVisibility(View.GONE);
			mInputState = INPUT_STATE_NOTHING;
			return;
		}

		switch (mInputState) {
		case INPUT_STATE_NOTHING: {
			mPlusContentView.setVisibility(View.GONE);
			mFacesViewPager.setVisibility(View.VISIBLE);
			mFooterView.setVisibility(View.VISIBLE);
		}
			break;
		case INPUT_STATE_TEXT: {
			mPlusContentView.setVisibility(View.GONE);
			mFacesViewPager.setVisibility(View.VISIBLE);
			mFooterView.setVisibility(View.VISIBLE);
			mActivityReference.get().getWindow()
					.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
			resetFooterViewHeight();
			mInputMethodManager
					.hideSoftInputFromWindow(mFooterView.getWindowToken(), 0);
		}
			break;
		case INPUT_STATE_FACE:
			DTLog.logError();
			break;
		case INPUT_STATE_AUDIO: {
			onSwitchAudioBtnClick(mSendOrAudioSwitchBtn);
			mPlusContentView.setVisibility(View.GONE);
			mFacesViewPager.setVisibility(View.VISIBLE);
			mFooterView.setVisibility(View.VISIBLE);
		}
			break;
		case INPUT_STATE_PLUS: {
			mPlusContentView.setVisibility(View.GONE);
			mFacesViewPager.setVisibility(View.VISIBLE);
			mFooterView.setVisibility(View.VISIBLE);
		}
			break;
		default:
			DTLog.logError();
			break;
		}

		mEmotionManager.initialize();
		mInputState = INPUT_STATE_FACE;
		mBuilder = null;
	}

	protected void onChoicePhotoBtnClick(View view) {
		FileUtils.choosePhoto(mActivityReference.get(), CHOOSE_PHOTO);
	}

	protected void onTakePhotoBtnClick(View v) {
		mImageUri = FileUtils.generateImageUri();
		FileUtils.takePhoto(mActivityReference.get(), mImageUri, TAKE_PHOTO);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case CHOOSE_PHOTO:
			if (data != null) {
				// 获取路径
				try {
					Uri originalUri = data.getData();

					sendPhotoMessage(originalUri);
				} catch (Exception e) {
				}
			}
			break;
		case TAKE_PHOTO:
			if (resultCode == Activity.RESULT_OK) {
				try {
					sendPhotoMessage(mImageUri);
				} catch (Exception e) {
				}
			}
			break;
		default:
			break;
		}
	}

	protected abstract void onSendBitmap(Bitmap bitmap);

	protected void sendPhotoMessage(Uri originalUri) {
		final String path = FileUtils.getPath(mActivityReference.get(), originalUri);
		final BitmapFactory.Options options = new BitmapFactory.Options();

		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, options);

		int inSampleSize;

		HashMap<String, Object> key = new HashMap<String, Object>();

		inSampleSize = options.outWidth * options.outHeight * 4 / 262144;
		inSampleSize = (int) Math.sqrt(inSampleSize);

		if (inSampleSize == 0) {
			inSampleSize = 1;
		}

		key.put("width", options.outWidth / inSampleSize);
		key.put("height", options.outHeight / inSampleSize);
		key.put("small_height", 300);
		key.put("small_width", 300);
		key.put("percent", 0);
		key.put("uri", path);

		options.inSampleSize = inSampleSize == 0 ? 1 : inSampleSize;
		options.inJustDecodeBounds = false;

		final Bitmap bitmap = BitmapFactory.decodeFile(path, options);

		onSendBitmap(bitmap);
	}

	/**
	 * 设置返回时的回传至
	 * 
	 * @param resultCode
	 * @param data
	 */
	public void setResult(int resultCode, Intent data) {
		this.mResultCode = resultCode;
		this.mData = data;
	}

	// 底部工具栏右侧按钮（语音或者发送）按下时
	public void onSwitchAudioBtnClick(View view) {
		if (!(view instanceof ImageButton)) {
			DTLog.logError();
			return;
		}

		ImageButton imageView = (ImageButton) view;

		if (mInputState == INPUT_STATE_AUDIO) {
			mRecordBtn.setVisibility(View.GONE);
//			mFaceBtn.setVisibility(View.VISIBLE);
//			mContentEditText.setVisibility(View.VISIBLE);
			mEditLayout.setVisibility(View.VISIBLE);
			
			String text = mContentEditText.getText().toString();

			if (text.length() >= 1) {
				mSendBtn.setVisibility(View.VISIBLE);
				mPlusBtn.setVisibility(View.GONE);
			}
			
			imageView.setImageDrawable(getResources().getDrawable(
					CPResourceUtil.getDrawableId(mActivityReference.get(),
							"im_setmode_voice_btn_selector")));
			mInputMethodManager.showSoftInput(mContentEditText, 0);
			mContentEditText.requestFocus();
			mInputState = INPUT_STATE_TEXT;
			return;
		}

		switch (mInputState) {
		case INPUT_STATE_NOTHING:
			break;
		case INPUT_STATE_TEXT:
			mInputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
			break;
		case INPUT_STATE_FACE:
			mEmotionManager.destory();
			mFooterView.setVisibility(GONE);
			break;
		case INPUT_STATE_AUDIO:
			DTLog.logError();
			break;
		case INPUT_STATE_PLUS:
			mFooterView.setVisibility(GONE);
			break;
		default:
			DTLog.logError();
			break;
		}

//		mFaceBtn.setVisibility(View.GONE);
//		mContentEditText.setVisibility(View.GONE);
		mEditLayout.setVisibility(View.GONE);
		mPlusBtn.setImageDrawable(getResources().getDrawable(
				CPResourceUtil.getDrawableId(mActivityReference.get(),
						"im_type_select_btn")));
		
		mRecordBtn.setVisibility(View.VISIBLE);
		imageView.setImageDrawable(getResources().getDrawable(
				CPResourceUtil.getDrawableId(mActivityReference.get(),
						"im_setmode_keyboard_btn_selector")));

		mInputState = INPUT_STATE_AUDIO;
	}

	protected static class IMBaseChatViewHandler extends Handler {
		protected final WeakReference<IMBaseChatView> mBaseChatViewReference;
		protected final WeakReference<Activity> mActivityReference;

		public IMBaseChatViewHandler(IMBaseChatView chatView, Activity activity,
				Looper looper) {
			super(looper);
			mBaseChatViewReference = new WeakReference<IMBaseChatView>(chatView);
			mActivityReference = new WeakReference<Activity>(activity);
		}

		@Override
		public void handleMessage(Message msg) {
			if (mActivityReference.get() == null) {
				return;
			}

			if (mBaseChatViewReference.get() == null) {
				return;
			}

			mBaseChatViewReference.get().processHandlerMessage(msg);
		}
	}

	protected Handler mHandler;

	@Override
	public Handler getHandler() {
		if (mHandler == null) {
			synchronized (this) {
				if (mHandler == null) {
					mHandler = new IMBaseChatViewHandler(IMBaseChatView.this,
							mActivityReference.get(), mActivityReference.get()
									.getMainLooper());
				}
			}
		}

		return mHandler;
	}

	public void processHandlerMessage(Message msg) {
		switch (msg.what) {
		case MSG_SEND_CLASSICAL_EMOTION:
			showNormalMsg((String) msg.obj);
			break;
		default:
			break;
		}
	}

	protected SpannableStringBuilder mBuilder;

	protected void showNormalMsg(String command) {
		int index = mContentEditText.getSelectionStart();

		if (!mContentEditText.isFocused()) {
			mContentEditText.requestFocus();
		}

		mContentEditText.setCursorVisible(true);

		if (command.startsWith("[DEL]")) {// 如果点击的是删除键
			int keyCode = KeyEvent.KEYCODE_DEL;
			KeyEvent keyEventDown = new KeyEvent(KeyEvent.ACTION_DOWN, keyCode);
			KeyEvent keyEventUp = new KeyEvent(KeyEvent.ACTION_UP, keyCode);

			mContentEditText.onKeyDown(keyCode, keyEventDown);
			mContentEditText.onKeyUp(keyCode, keyEventUp);
			mBuilder = null;
		} else { // 否则将表情插入当前光标所有位置
			Editable editable = mContentEditText.getEditableText();

			if (index < 0 || index >= editable.length()) {
				Bitmap bitmap = FacesXml.face_StaticBitmaps.get(FacesXml.getFacesMap()
						.get(command));

				if (bitmap != null) {
					ImageSpan span = new ImageSpan(mActivityReference.get(), bitmap);

					// 创建一个SpannableString对象，以便插入用ImageSpan对象封装的图像
					SpannableString spannableString = new SpannableString(command);

					// 用ImageSpan对象替换face
					spannableString.setSpan(span, 0, command.length(),
							Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

					// 将随机获得的图像追加到EditText控件的最后
					mContentEditText.append(spannableString);
					mBuilder = null;
					return;
				}
			} else {
				if (mBuilder == null) {
					editable.insert(index, command);
					mContentEditText.setText(replace(mContentEditText.getText()));
					mContentEditText.setSelection(index + command.length());
				} else {
					Bitmap bitmap = FacesXml.face_StaticBitmaps.get(FacesXml
							.getFacesMap().get(command));

					if (bitmap != null) {
						ImageSpan span = new ImageSpan(mActivityReference.get(), bitmap);

						mBuilder.insert(index, command);
						mBuilder.setSpan(span, index, index + command.length(),
								Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
						mContentEditText.setText(mBuilder);
						mContentEditText.setSelection(index + command.length());
					}
				}
			}

		}
	}

	protected Pattern buildPattern() {
		int size = FacesXml.getFacesMap().size();
		StringBuilder patternString = new StringBuilder(size * 3);

		patternString.append('(');

		Iterator<String> iterator = FacesXml.getFacesMap().keySet().iterator();

		while (iterator.hasNext()) {
			String key = iterator.next();

			if (key.startsWith("[DEL]")) {
				continue;
			}

			patternString.append(Pattern.quote(key));
			patternString.append('|');
		}

		patternString.replace(patternString.length() - 1, patternString.length(), ")");
		return Pattern.compile(patternString.toString());
	}

	protected Pattern pattern;

	protected CharSequence replace(CharSequence text) {
		try {
			mBuilder = new SpannableStringBuilder(text);

			if (pattern == null) {
				pattern = buildPattern();
			}

			Matcher matcher = pattern.matcher(text);

			while (matcher.find()) {
				String mIDs = matcher.group();
				String gifFileName = FacesXml.getFacesMap().get(mIDs);
				Bitmap bitmap = FacesXml.face_StaticBitmaps.get(gifFileName);

				if (bitmap != null) {
					ImageSpan span = new ImageSpan(mActivityReference.get(), bitmap);

					mBuilder.setSpan(span, matcher.start(), matcher.end(),
							Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				}
			}

			return mBuilder;
		} catch (Exception e) {
			return text;
		}
	}

	protected View getViewByPosition(int pos, ListView listView) {
		final int firstListItemPosition = listView.getFirstVisiblePosition();
		final int lastListItemPosition = firstListItemPosition
				+ listView.getChildCount() - 1;

		if (pos < firstListItemPosition || pos > lastListItemPosition) {
			return listView.getAdapter().getView(pos, null, listView);
		} else {
			final int childIndex = pos - firstListItemPosition;

			return listView.getChildAt(childIndex);
		}
	}

	protected abstract void onUpdateScreenGif(int startPos, int endPos);

	protected void updataScreenGif(int startPos, int endPos) {
		onUpdateScreenGif(startPos, endPos);
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		FileUtils.unRegister();
		mGifEmotionUtils.destory();

		IMMyselfRecentContacts2.clearUnreadChatMessage();

		switch (mInputState) {
		case INPUT_STATE_NOTHING:
			break;
		case INPUT_STATE_TEXT:
			mInputMethodManager.hideSoftInputFromWindow(
					mContentEditText.getWindowToken(), 0);
			break;
		case INPUT_STATE_FACE: {
			mEmotionManager.destory();
			mFooterView.setVisibility(GONE);
		}
			break;
		case INPUT_STATE_AUDIO: {
			mFooterView.setVisibility(View.GONE);
		}
			break;
		case INPUT_STATE_PLUS: {
			mFooterView.setVisibility(View.GONE);
		}
			break;
		default:
			break;
		}
	}

	protected abstract boolean onStartRecording();

	protected void addListeners() {
		mListView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					mOnChatViewTouchListener.onTouch(v);
					break;
				default:
					break;
				}

				return false;
			}
		});

		mListView.setOnScrollListener(new OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				if (mScrollState != scrollState) {
					mScrollState = scrollState;
					mGifEmotionUtils.setScrollState(scrollState);

					if (mScrollState == OnScrollListener.SCROLL_STATE_IDLE) {
						updataScreenGif(mFirstVisibleItem, mLastVisibleItem);
					} else if (mScrollState == OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
						mGifEmotionUtils.stopAllGif();
					}
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				mFirstVisibleItem = firstVisibleItem;
				mLastVisibleItem = firstVisibleItem + visibleItemCount - 1;
			}
		});

		mRecordBtn.setOnTouchListener(new OnTouchListener() {
			protected float mLastX;
			protected float mLastY;
			protected Rect mRect;
			protected long mGapTime;

			@SuppressLint("NewApi")
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (System.currentTimeMillis() - mGapTime < 500) {
					return true;
				}
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					mRect = new Rect();

					mContentEditText.getLocalVisibleRect(mRect);
					mLastX = event.getX();
					mLastY = event.getY();
					mRecordBtn.setText(mActivityReference.get().getString(
							CPResourceUtil.getStringId(mActivityReference.get(),
									"str_voice_up")));
					mRecordBtn.setBackground(getResources().getDrawable(
							CPResourceUtil.getDrawableId(mActivityReference.get(),
									"im_chatting_send_btn_bg_pressed")));
					if (mVoiceRecordDialog == null) {
						mVoiceRecordDialog = new AudioRecorderDialog(mActivityReference
								.get(), CPResourceUtil.getStyleId(
								mActivityReference.get(), "custom_dialog_transparent"));
						mVoiceRecordDialog.setOnErrorListener(IMBaseChatView.this);
					}

					if (!onStartRecording()) {
						DTLog.logError();
						return false;
					}

					mVoiceRecordDialog.show();

					if (!mVoiceRecordDialog.isShowing()) {
						mVoiceRecordDialog.show();
					}
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					mGapTime = System.currentTimeMillis();
					DTAppEnv.getMainHandler().removeCallbacksAndMessages(null);
					mRecordBtn.setText(mActivityReference.get().getString(
							CPResourceUtil.getStringId(mActivityReference.get(),
									"str_voice_press")));
					mRecordBtn.setBackground(getResources().getDrawable(
							CPResourceUtil.getDrawableId(mActivityReference.get(),
									"im_chatting_send_btn_bg")));

					mVoiceRecordDialog.requestDismiss();

					// 如果松开后，仍在按钮区域
					if (mRect != null && mRect.contains((int) mLastX, (int) mLastY)
							&& !mVoiceRecordDialog.isTooShort()) {
						final long actionTime = System.currentTimeMillis() / 1000;

						boolean result = IMAudioSender.getInstance().stopRecording(
								true, 10, new OnAudioSenderListener() {
									@Override
									public void onSendSuccess() {
									}

									@Override
									public void onSendFailure(String error) {
									}

									@Override
									public void onRecordSuccess() {
										mAdapter.notifyDataSetChanged();
										mListView.setSelection(mAdapter.getCount() - 1);
									}

									@Override
									public void onRecordFailure(String error) {
									}
								}, actionTime);

						if (!result) {
							DTLog.logError();
							return false;
						}
					} else {
						IMMyself.stopRecording(false, 0, null);
					}

					mRect = null;
				} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
					mLastX = event.getX();
					mLastY = event.getY();

					if (mRect != null) {
						if (mRect.contains((int) mLastX, (int) mLastY)) {
							if (mVoiceRecordDialog != null) {
								mVoiceRecordDialog.showRecordingView();
							}
						} else {
							if (mVoiceRecordDialog != null) {
								mVoiceRecordDialog.showCancelRecordView();
							}
						}
					}
				}

				return false;
			}
		});

		mContentEditText.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (mInputState == INPUT_STATE_TEXT) {
					return false;
				}

				switch (mInputState) {
				case INPUT_STATE_NOTHING:
					break;
				case INPUT_STATE_TEXT:
					DTLog.logError();
					break;
				case INPUT_STATE_FACE: {
				}
					break;
				case INPUT_STATE_AUDIO: {
					DTLog.logError();
				}
					break;
				case INPUT_STATE_PLUS: {
				}
					break;
				default:
					break;
				}

				mActivityReference
						.get()
						.getWindow()
						.setSoftInputMode(
								WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
				mInputMethodManager.showSoftInput(mContentEditText,
						InputMethodManager.HIDE_NOT_ALWAYS);
				
				if (mContentEditText.getText().length() >= 1) {
					mSendBtn.setVisibility(View.VISIBLE);
					mPlusBtn.setVisibility(View.GONE);
				} else {
					mSendBtn.setVisibility(View.GONE);
					mPlusBtn.setVisibility(View.VISIBLE);
				}
				
				mInputState = INPUT_STATE_TEXT;
				return true;
			}
		});

		mContentEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (mContentEditText.getText().length() >= 1) {
					mSendBtn.setVisibility(View.VISIBLE);
					mPlusBtn.setVisibility(View.GONE);
				} else {
					mSendBtn.setVisibility(View.GONE);
					mPlusBtn.setVisibility(View.VISIBLE);
				}
				

				if (mInputState != INPUT_STATE_TEXT && mInputState != INPUT_STATE_FACE) {
					DTLog.sign(mInputState);
					DTLog.logError();
					return;
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
	}

	@Override
	public void onError(String msg) {
		mVoiceRecordDialog.requestDismiss();
	}

	protected Activity getActivity() {
		return mActivityReference.get();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// 按下返回键
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			switch (mInputState) {
			case INPUT_STATE_FACE:
				mEmotionManager.destory();
				mFooterView.setVisibility(View.GONE);
				mInputState = INPUT_STATE_NOTHING;
				return true;
			case INPUT_STATE_PLUS:
				mInputState = INPUT_STATE_NOTHING;
				mFooterView.setVisibility(View.GONE);
				return true;
			}
		}

		return false;
	}
}
