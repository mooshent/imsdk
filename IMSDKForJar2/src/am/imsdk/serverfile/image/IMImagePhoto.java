package am.imsdk.serverfile.image;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.SoftReference;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTLocalModel;
import am.dtlib.model.c.tool.DTTool;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;

public final class IMImagePhoto extends DTLocalModel {
	public String mFileID = "";
	private SoftReference<Bitmap> mBitmapReference = new SoftReference<Bitmap>(null);
	private SoftReference<byte[]> mBufferReference = new SoftReference<byte[]>(null);
	private int mWidth = 0;
	private int mHeight = 0;
	
	public IMImagePhoto() {
		addDirectory("II");
		addDecryptedDirectory("IMImage");
		mIsStreamData = true;
	}

	@Override
	public boolean generateLocalFullPath() {
		if (mFileID.length() == 0) {
			return false;
		}

		mLocalFileName = DTTool.getMD5String(mFileID);
		mDecryptedLocalFileName = mFileID + ".jpg";
		return true;
	}

	@Override
	public boolean saveFile() {
		if (!generateLocalFullPath()) {
			DTLog.logError();
			return false;
		}

		byte[] buffer = mBufferReference.get();

		if (buffer != null) {
			protectedWriteToFile(buffer, getLocalFullPath());
			return false;
		}

		Bitmap bitmap = mBitmapReference.get();

		if (bitmap == null) {
			DTLog.logError();
			return false;
		}

		FileOutputStream outputSteam;

		try {
			outputSteam = new FileOutputStream(getLocalFullPath());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			DTLog.logError();
			return false;
		}

		bitmap.compress(CompressFormat.JPEG, 50, outputSteam);

		try {
			outputSteam.flush();
			outputSteam.close();
		} catch (IOException e) {
			e.printStackTrace();
			DTLog.logError();
			return false;
		}

		return true;
	}

	@Override
	public boolean readFromFile() {
		if (!generateLocalFullPath()) {
			DTLog.logError();
			return false;
		}

		if (!isLocalFileExist()) {
			return false;
		}

		File file = new File(getLocalFullPath());
		FileInputStream fileInputStream;

		try {
			fileInputStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			DTLog.logError();
			return false;
		}

		byte[] buffer = null;

		try {
			buffer = new byte[fileInputStream.available()];
			fileInputStream.read(buffer);
			fileInputStream.close();
		} catch (IOException e) {
			return false;
		}

		mBufferReference = new SoftReference<byte[]>(buffer);

		Bitmap bitmap = BitmapFactory.decodeByteArray(buffer, 0, buffer.length);

		if (bitmap != null) {
			mBitmapReference = new SoftReference<Bitmap>(bitmap);
		} else {
			mBitmapReference.clear();
		}

		mWidth = bitmap.getWidth();
		mHeight = bitmap.getWidth();

		return bitmap != null;
	}

	public void setBitmap(Bitmap bitmap) {
		if (mBitmapReference.get() == bitmap) {
			return;
		}

		mBitmapReference = new SoftReference<Bitmap>(bitmap);
		mWidth = bitmap.getWidth();
		mHeight = bitmap.getWidth();
	}

	public Bitmap getBitmap() {
		if (mBitmapReference.get() == null && mBufferReference.get() == null) {
			readFromFile();
		}
		
		if (mBitmapReference.get() == null && mBufferReference.get() != null
				&& mBufferReference.get().length != 0) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(mBufferReference.get(), 0,
					mBufferReference.get().length);
			
			mBitmapReference = new SoftReference<Bitmap>(bitmap);
			mWidth = bitmap.getWidth();
			mHeight = bitmap.getWidth();
		}

		return mBitmapReference.get();
	}

	public void setBuffer(byte[] buffer) {
		if (mBufferReference.get() == buffer) {
			return;
		}

		mBufferReference = new SoftReference<byte[]>(buffer);
	}

	public byte[] getBuffer() {
		byte[] buffer = mBufferReference.get();

		if (buffer == null) {
			buffer = new byte[0];
		}

		return buffer;
	}

	public long getBufferLength() {
		if (mBufferReference.get() == null) {
			return 0;
		} else {
			return mBufferReference.get().length;
		}
	}

	public Uri getUri() {
		return Uri.fromFile(new File(getLocalFullPath()));
	}
	
	public int getWidth() {
		if (mWidth > 0) {
			return mWidth;
		}
		
		if (mBitmapReference != null && mWidth == 0) {
			DTLog.logError();
		}
		
		readFromFile();
		return mWidth;
	}
	
	public int getHeight() {
		if (mHeight > 0) {
			return mHeight;
		}

		if (mBitmapReference != null && mHeight == 0) {
			DTLog.logError();
		}
		
		readFromFile();
		return mHeight;
	}
}
