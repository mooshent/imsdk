package am.core.java;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;

import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.text.TextUtils;
import android.util.Log;

/**
 * 
 * 客户端Socket
 * 用于发送cmd到remote service
 * @author mooshent
 */
public class LocalClient {
	
	private static final String TAG = "BufferTest";
	private static final String ConnectName = "imsdk.core.localsocket";
	
	private LocalSocket client = null;
	
	private BufferedInputStream is;
	private BufferedOutputStream os;
	
	private int timeout = 30000;

	public int connect(String name) {
		if(TextUtils.isEmpty(name)) return -1;
		
//		if (client != null && client.isConnected())
//			return 1;
		try {
			client = new LocalSocket();
			client.connect(new LocalSocketAddress(name));
			
			is = new BufferedInputStream(client.getInputStream());
			os = new BufferedOutputStream(client.getOutputStream());
			
			return 1;
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}
	}

	public int close() {
		try {
			if(is != null) {
				is.close();
			}
			
			if(os != null) {
				os.close();
			}
		} catch (IOException e) {
//			e.printStackTrace();
		} finally {
			is = null;
			os = null;
		}
		
		try {
			if(client != null) {
				client.close();
			}
		} catch (IOException e) {
//			e.printStackTrace();
		} finally {
			client = null;
		}

		return 1;
	}

	public int recv(byte[] buffer, int offset, int size) {
		if(client == null || !client.isConnected()) {
			Log.e("Debug", "LocalaClient err 1");
			return -1;
		}
		
		try {
			if(is == null) {
				Log.e("Debug", "LocalaClient err 2");
				return -1;
			}
			
			return is.read(buffer, offset, size);
		} catch (IOException e) {
//			e.printStackTrace();
			Log.e("Debug", "LocalaClient err 3");
			close();
			return -1;
		}
	}

	public int send(byte[] buffer, int offset, int size) {
		if(client == null || !client.isConnected()) {
			return -1;
		}
		
		try {
			Log.d(TAG,"send count = " + size);
			
			if(os == null) {
				return -1;
			}
			os.write(buffer, offset, size);
			os.flush();
			
			return 0;
		} catch (IOException e) {
//			e.printStackTrace();
			
			close();
			return -1;
		}
	}
	
	private LocalClient() { }

	private static LocalClient instance = null;

	public synchronized static LocalClient getInstance() {
		if (instance == null) {
			instance = new LocalClient();
		}

		return instance;
	}

}
