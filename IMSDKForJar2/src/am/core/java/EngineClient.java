package am.core.java;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import remote.service.data.RemoteDataManager;
import remote.service.data.base.DTRemoteEnv;
import am.core.jni.IMProtocol;
import am.dtlib.model.b.log.DTRemoteLog;
import am.dtlib.model.c.socket.DTSendThread;
import am.dtlib.model.c.socket.DTThread;
import am.dtlib.model.d.DTDevice;
import android.content.Context;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;

public class EngineClient {
	
	private EngineClient() {}
	
	private static EngineClient instance = null;

	public synchronized static EngineClient getInstance() {
		if (instance == null) {
			instance = new EngineClient();
		}

		return instance;
	}
	
	public enum ECSocketStatus {
		None(0), Connecting(1), Connected(2);

		private final int value;

		private ECSocketStatus(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	public ECSocketStatus getStatus() {
		synchronized (mSyncObject) {
			return mStatus;
		}
	}

	// 多线程调用
	private void setStatus(ECSocketStatus newStatus) {
		if (getStatus() == newStatus) {
			return;
		}

		synchronized (mSyncObject) {
			mStatus = newStatus;
		}

		if (newStatus == ECSocketStatus.Connected) {
			DTDevice.getInstance().mLastIMAddress = mCurrentIMAddress;
			DTDevice.getInstance().saveFile();

			checkReconnectWithNextIMAddress();
		}
	}

	/**
	 * 
	 * @Description: 是否调用负载均衡
	 * @return 
	 * @throws 
	 * @author 方子君
	 */
	public synchronized boolean isRunningSIS() {
		return mIsRunningSIS;
	}

	public void setSISDefaultAddress(String sisDefaultAddress) {
		mSISDefaultAddress = sisDefaultAddress;
	}

	public String getSISDefaultAddress() {
		return mSISDefaultAddress;
	}

	public void setSISDomainName(String sisDomainName) {
		mSISDomainName = sisDomainName;
	}

	public String getSISDomainName() {
		return mSISDomainName;
	}

	public void setDomainName(String domainName) {
		mDomainName = domainName;
	}

	public String getDomainName() {
		return mDomainName;
	}

	public void setDefaultIMAddress(String defaultIMAddress) {
		mDefaultIMAddress = defaultIMAddress;
	}

	public String getDefaultIMAddress() {
		return mDefaultIMAddress;
	}

	public void setForceIMAddress(String forceIMAddress) {
		mForceIMAddress = forceIMAddress;
	}

	public String getForceIMAddress() {
		return mForceIMAddress;
	}

	public String getCurrentIMAddress() {
		return mCurrentIMAddress;
	}

	/**
	 * 
	 * @Description: 调用负载均衡获取连接IP
	 * @throws 
	 * @author 方子君
	 */
	public void runSIS() {
		if (mSISDomainName.length() == 0) {
			DTRemoteLog.logError();
			return;
		}

		if (mSISDefaultAddress.length() == 0) {
			DTRemoteLog.logError();
			return;
		}

		Thread parseDomainThread = new Thread(new Runnable() {
			@Override
			public void run() {

				InetAddress netAddress = null;

				try {
					netAddress = InetAddress.getByName(mSISDomainName);
				} catch (UnknownHostException e) {
				}

				final String sisAddress = netAddress != null ? netAddress
						.getHostAddress() : "";

				if (sisAddress.length() > 0) {
					new Thread(new Runnable() {
						@Override
						public void run() {
							DTDevice.getInstance().mLastSISAddress = sisAddress
									+ ":18000";
							DTDevice.getInstance().saveFile();
							DTRemoteLog.log("sis domain name " + mSISDomainName + " to ip:"
									+ DTDevice.getInstance().mLastSISAddress);
						}
					}).start();
				} else {
					DTRemoteLog.log("Failed transfer sis domain name " + mSISDomainName
							+ " to IP Address");
				}

				if (mDomainName.length() == 0) {
					DTRemoteLog.logError();
					return;
				}

				netAddress = null;

				try {
					netAddress = InetAddress.getByName(mDomainName);
				} catch (UnknownHostException e) {
				}

				final String imAddress = netAddress != null ? netAddress
						.getHostAddress() : "";

				DTRemoteLog.sign(true, "imAddress: " + imAddress);

				if (imAddress.length() > 0) {
					new Thread(new Runnable() {
						@Override
						public void run() {
							DTDevice.getInstance().mIMAddressFromDomain = sisAddress
									+ ":9100";
							DTDevice.getInstance().saveFile();
							DTRemoteLog.log("domain name " + mDomainName + " to ip:"
									+ DTDevice.getInstance().mIMAddressFromDomain);
						}
					}).start();
				} else {
					DTRemoteLog.log("Failed transfer sis domain name " + mDomainName
							+ " to IP Address");
				}
			
			}
		});

		parseDomainThread.start();

		String currentSISAddress = "";

		if (DTDevice.getInstance().mLastSISAddress.length() > 0) {
			currentSISAddress = DTDevice.getInstance().mLastSISAddress;
		} else {
			if (mSISDefaultAddress.length() == 0) {
				DTRemoteLog.logError();
				return;
			}

			currentSISAddress = mSISDefaultAddress;
		}

		if (currentSISAddress.length() == 0) {
			DTRemoteLog.logError();
			return;
		}

		String[] address = currentSISAddress.split(":");

		if (address.length != 2) {
			DTRemoteLog.logError();
			return;
		}

		final String ipAddress = address[0];
		final int port = Integer.parseInt(address[1]);

		if (ipAddress.length() == 0) {
			DTRemoteLog.logError();
			return;
		}

		if (port == 0) {
			DTRemoteLog.logError();
			return;
		}

		Thread udpThread = new Thread(new Runnable() {
			@Override
			public void run() {
				DTRemoteLog.sign(true, "udpThread");

				short length = 128;
				DatagramSocket udpSocket = null;

				try {
					udpSocket = new DatagramSocket();
				} catch (SocketException e) {
					e.printStackTrace();
					DTRemoteLog.logError();
					DTRemoteLog.log(e.toString());
					return;
				}

				byte[] buffer = new byte[128];

				buffer[0] = (byte) (length >> 8);
				buffer[1] = (byte) length;

				String string = "UE";
				byte[] jsonBuf = null;

				try {
					jsonBuf = string.getBytes("UTF8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					DTRemoteLog.logError();
					DTRemoteLog.log(e.toString());
					return;
				}

				buffer[2] = jsonBuf[0];
				buffer[3] = jsonBuf[1];

				string = "NH-test";

				try {
					jsonBuf = string.getBytes("UTF8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					DTRemoteLog.logError();
					DTRemoteLog.log(e.toString());
					return;
				}

				for (int i = 0; i < 30 && i < jsonBuf.length; i++) {
					buffer[4 + i] = jsonBuf[i];
				}

				TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);

				if (telephonyManager == null) {
					DTRemoteLog.logError();
					return;
				}

				String networkOperator = telephonyManager.getNetworkOperator();

				if (networkOperator == null) {
					DTRemoteLog.logError();
					return;
				}

				if (networkOperator.length() != 0) {
					if (networkOperator.length() < 4) {
						DTRemoteLog.logError();
						return;
					}

					int tel_opera = 0;

					try {
						int mcc = Integer.parseInt(networkOperator.substring(0, 3));
						int mnc = Integer.parseInt(networkOperator.substring(3));

						tel_opera = mcc * 100 + mnc;
					} catch (Exception e) {
						e.printStackTrace();
						tel_opera = 0;
					}

					buffer[34] = (byte) (tel_opera >> 24);
					buffer[35] = (byte) (tel_opera >> 16);
					buffer[36] = (byte) (tel_opera >> 8);
					buffer[37] = (byte) tel_opera;
				}

				string = "qswddddddsddddsdwwssdsd";

				try {
					jsonBuf = string.getBytes("UTF8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					DTRemoteLog.logError();
					DTRemoteLog.log(e.toString());
					return;
				}

				for (int i = 0; i < 50 && i < jsonBuf.length; i++) {
					buffer[42 + i] = jsonBuf[i];
				}

				string = "0.1.0";

				try {
					jsonBuf = string.getBytes("UTF8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
					DTRemoteLog.logError();
					DTRemoteLog.log(e.toString());
					return;
				}

				for (int i = 0; i < 10 && i < jsonBuf.length; i++) {
					buffer[92 + i] = jsonBuf[i];
				}

				InetAddress inetAddress = null;

				try {
					inetAddress = InetAddress.getByName(ipAddress);
				} catch (UnknownHostException e) {
					e.printStackTrace();
					DTRemoteLog.logError();
					DTRemoteLog.log(e.toString());
					return;
				}

				DatagramPacket sendPacket = new DatagramPacket(buffer, buffer.length,
						inetAddress, port);

				try {
					udpSocket.send(sendPacket);
				} catch (IOException e) {
					return;
				}

				DTRemoteLog.sign(true, "udpSocket sent");

				byte[] recvBuffer = new byte[1024];
				byte[] resultBuffer = new byte[1024];
				int resultBufferLength = 0;
				int retryTimes = 0;
				int parseStringRetryTimes = 0;
				int parseJsonRetryTimes = 0;
				JSONObject jsonResult = null;

				while (true) {
					DatagramPacket recvPacket = new DatagramPacket(recvBuffer,
							recvBuffer.length);

					try {
						udpSocket.receive(recvPacket);
					} catch (IOException e) {
						e.printStackTrace();
						DTRemoteLog.logError();
						DTRemoteLog.log(e.toString());
						return;
					}

					DTRemoteLog.sign(true,
							"udpSocket recv Length:" + recvPacket.getLength());

					if (recvPacket.getLength() == 0) {
						retryTimes++;

						if (retryTimes > 3) {
							break;
						}

						continue;
					}

					retryTimes = 0;

					for (int i = 0; i < recvPacket.getLength(); i++) {
						resultBuffer[resultBufferLength + i] = recvBuffer[i];
					}

					resultBufferLength += recvPacket.getLength();

					String recvStr = new String(resultBuffer, 0, resultBufferLength);

					DTRemoteLog.sign(true, "udpSocket recv String:" + recvStr);

					if (recvStr.length() == 0) {
						parseStringRetryTimes++;

						if (parseStringRetryTimes > 3) {
							break;
						}

						continue;
					}

					parseStringRetryTimes = 0;

					try {
						jsonResult = new JSONObject(recvStr);
					} catch (JSONException e) {
					}

					if (jsonResult == null) {
						parseJsonRetryTimes++;

						if (parseJsonRetryTimes > 3) {
							break;
						}

						continue;
					}

					parseJsonRetryTimes = 0;

					DTRemoteLog.sign(true, "UDP Result:" + recvStr);
					udpSocket.close();
					break;
				}

				if (jsonResult == null) {
					DTRemoteLog.log("UDP Failed");
				} else {
					DTRemoteLog.log("UDP Succeeded");

					JSONArray array = null;

					try {
						array = jsonResult.getJSONArray("ips");
					} catch (JSONException e) {
						e.printStackTrace();
						DTRemoteLog.logError();
						DTRemoteLog.log(e.toString());
						return;
					}

					if (array == null) {
						DTRemoteLog.logError();
						return;
					}

					if (!(array instanceof JSONArray)) {
						DTRemoteLog.logError();
						return;
					}

					final JSONArray finalArray = array;

					new Thread(new Runnable() {
						@Override
						public void run() {
							DTDevice.getInstance().mIMAddressListFromSIS.clear();

							for (int i = 0; i < finalArray.length(); i++) {
								try {
									DTDevice.getInstance().mIMAddressListFromSIS
											.add(finalArray.getString(i));
								} catch (JSONException e) {
									e.printStackTrace();
									DTRemoteLog.logError();
									DTRemoteLog.log(e.toString());
									return;
								}
							}

							DTDevice.getInstance().saveFile();
						}
					}).start();
				}
			}
		});

		udpThread.start();
	}

	public void setSessionID(long sessionID) {
		mSessionID = sessionID;
	}

	public long getSessionID() {
		return mSessionID;
	}

	public void setUID(long uid) {
		mUID = uid;
	}

	public long getUID() {
		return mUID;
	}

	// socket内部接口
	// 可以用于延时执行
	private void checkReconnectWithNextIMAddress() {
		if (getStatus() != ECSocketStatus.Connecting) {
			return;
		}

		this.reconnectWithNextIMAddress();
	}

	private void nextIMAddress() {
		ArrayList<String> array = new ArrayList<String>();

		// 1. aryAddressesFromSIS
		if (DTDevice.getInstance().mIMAddressListFromSIS.size() > 0) {
			for (String string : DTDevice.getInstance().mIMAddressListFromSIS) {
				array.add(string);
			}
		}

		// 2. lastIMAddress
		if (DTDevice.getInstance().mLastIMAddress.length() > 0) {
			array.remove(DTDevice.getInstance().mLastIMAddress);
			array.add(DTDevice.getInstance().mLastIMAddress);
		}

		// 3. imAddressFromDomain
		if (DTDevice.getInstance().mIMAddressFromDomain.length() > 0) {
			array.remove(DTDevice.getInstance().mIMAddressFromDomain);
			array.add(DTDevice.getInstance().mIMAddressFromDomain);
		}

		// 4. defaultIMAddress
		if (mDefaultIMAddress.length() > 0) {
			array.remove(mDefaultIMAddress);
			array.add(mDefaultIMAddress);
		}

		if (array.size() <= 0) {
			DTRemoteLog.logError();
			return;
		}

		int index = array.indexOf(mCurrentIMAddress);

		if (index == -1) {
			mCurrentIMAddress = array.get(0);
		} else {
			index++;

			if (index == array.size()) {
				mCurrentIMAddress = null;
			} else {
				if (index >= array.size()) {
					DTRemoteLog.logError();
					return;
				}

				mCurrentIMAddress = array.get(index);

				if (mCurrentIMAddress.length() <= 0) {
					DTRemoteLog.logError();
					return;
				}

				if (!mCurrentIMAddress.contains(":")) {
					DTRemoteLog.logError();
					return;
				}
			}
		}
	}

	// 主线程调用
	private void reconnectWithNextIMAddress() {
		DTRemoteLog.sign(true, "reconnectWithNextIMAddress");
		
		DTRemoteEnv.getMainHandler().removeCallbacks(mReconnectRunnable);
		DTRemoteEnv.getMainHandler().removeCallbacks(mCheckReconnectWithNextIMAddressRunnable);

		this.checkCloseSubThreads();
		this.nextIMAddress();

		if (mCurrentIMAddress != null) {
			this.startSendThread();
			return;
		}

		if (mLastTryConnectTime == 0) {
			DTRemoteLog.logError();
			return;
		}

		long currentTime = System.currentTimeMillis() / 1000;
		long timeInterval = currentTime - mLastTryConnectTime;

		if (timeInterval > 10) {
			this.firstIMAddress();

			if (mCurrentIMAddress == null) {
				DTRemoteLog.logError();
				return;
			}

			this.startSendThread();
		} else {
			DTRemoteEnv.getMainHandler().postDelayed(mReconnectRunnable, (10 - timeInterval) * 1000);
		}
	}
	
	public void send2Server(byte[] buffer) {
		Log.d("BufferTest", "EngineClient recv count = " + buffer.length);
		
		if(buffer != null && buffer.length > 0) {
			synchronized (mSynAryCmdNeedSend) {
				mSynAryCmdNeedSend.add(buffer);
			}
		}
		
		synchronized (mSynAryCmdNeedSend) {
			mSynAryCmdNeedSend.notifyAll();
		}
		
		if(getStatus() == ECSocketStatus.None) {
			reconnect();
		}
	}
	
	private void removeAllCommand() {
		mRecvBufferContentSize = 0;
		
		mSynAryCmdNeedSend.clear();
	}

	public void reconnect() {
		DTRemoteLog.sign(true, "reconnect");

		if (getStatus() != ECSocketStatus.None) {
			checkCloseSocket();
		}
		
		DTRemoteEnv.getMainHandler().removeCallbacks(mReconnectRunnable);
		DTRemoteEnv.getMainHandler().removeCallbacks(mCheckReconnectWithNextIMAddressRunnable);
		
		setStatus(ECSocketStatus.Connecting);
		this.connect();
	}

	private void connect() {
		this.firstIMAddress();
		this.startSendThread();
	}

	// 主线程调用
	private void startSendThread() {
		DTRemoteLog.sign(true, "startSendThread");

		synchronized (mSyncObject) {
			if (EngineClient.this.mSendThread != null) {
				return;
			}
		}

		synchronized (mSyncObject) {
			mSendThread = new DTSendThread(new Runnable() {
				public void run() {
					threadFuncSend();
				}
			});

			mSendThread.start();
		}

		DTRemoteLog.sign(true, "Try connect " + mCurrentIMAddress);

		DTRemoteEnv.getMainHandler().removeCallbacks(mCheckReconnectWithNextIMAddressRunnable);
		DTRemoteEnv.getMainHandler().postDelayed(mCheckReconnectWithNextIMAddressRunnable, 3 * 1000);
	}
	
	private void threadFuncSend() {
		if (mCurrentIMAddress == null) {
			DTRemoteLog.logError();
			return;
		}

		if (!mCurrentIMAddress.contains(":")) {
			DTRemoteLog.logError();
			return;
		}

		String[] arrayStrings = mCurrentIMAddress.split(":");

		if (arrayStrings.length != 2) {
			DTRemoteLog.logError();
			return;
		}

		int port = Integer.parseInt(arrayStrings[1]);

		if (port <= 0) {
			DTRemoteLog.logError();
			return;
		}

		synchronized (mSyncObject) {
			if (mSendThread == null || mSendThread.isCanceled()
					|| DTSendThread.currentThread() != mSendThread) {
				return;
			}
		}

		int result = IMProtocol.InitIMSDK(arrayStrings[0], port);

		synchronized (mSyncObject) {
			if (mSendThread == null || mSendThread.isCanceled()
					|| DTSendThread.currentThread() != mSendThread) {
				return;
			}
		}

		if (result < 0) {
			// 连接失败
			DTRemoteEnv.getMainHandler().post(new Runnable() {
				@Override
				public void run() {
					// 立即重连
					DTRemoteEnv.getMainHandler().removeCallbacks(mCheckReconnectWithNextIMAddressRunnable);
					DTRemoteEnv.getMainHandler().postDelayed(mCheckReconnectWithNextIMAddressRunnable, 3 * 1000);
				}
			});

			return;
		}

		synchronized (mSyncObject) {
			if (mSendThread == null || mSendThread.isCanceled()
					|| DTSendThread.currentThread() != mSendThread) {
				// 当前线程已被取消
				return;
			}
		}

		// 连接成功
		DTRemoteLog.sign(true, "connected");
		setStatus(ECSocketStatus.Connected);

		DTThread recvThread = new DTThread(new Runnable() {
			@Override
			public void run() {
				threadFuncRecv();
			}
		});

		synchronized (mSyncObject) {
			if (mSendThread == null || mSendThread.isCanceled()
					|| DTSendThread.currentThread() != mSendThread) {
				// 当前线程已被取消
				return;
			}

			mSendThread.setRecvThread(recvThread);
		}

		recvThread.start();

		byte[] cmd = null;

		do {
			synchronized (mSynAryCmdNeedSend) {
				if (mSynAryCmdNeedSend.size() > 0) {
					cmd = mSynAryCmdNeedSend.get(0);
					mSynAryCmdNeedSend.remove(0);
				} else {
					cmd = null;
				}
			}

			if (cmd == null) {
				synchronized (mSyncObject) {
					if (mSendThread == null || mSendThread.isCanceled()
							|| mSendThread != DTSendThread.currentThread()) {
						// 当前线程已被取消
						return;
					}
				}

				synchronized (mSynAryCmdNeedSend) {
					try {
						mSynAryCmdNeedSend.wait();
					} catch (InterruptedException e) {
					}
				}

				continue;
			}

			if (cmd == null) {
				return;
			}

			if (cmd.length == 0) {
				DTRemoteLog.logError();
				return;
			}

//			DTLog.sign(
//					true,
//					"TCP Send CmdType:" + cmd.getClass().getSimpleName() + " "
//							+ cmd.getCmdTypeValue() + "\n TCP Send Json:"
//							+ cmd.getSendJson());

			int length = cmd.length;

			synchronized (mSyncObject) {
				if (mSendThread == null || mSendThread.isCanceled()
						|| mSendThread != DTSendThread.currentThread()) {
					// 当前线程已被取消
					return;
				}
			}

			DTRemoteLog.sign(true, "TCP length:" + length);

			// printBuffer(buffer, length);
			result = IMProtocol.SendIMSDK(cmd, 0, length);

			DTRemoteLog.sign(true, "TCP length:" + length);
			DTRemoteLog.sign(true, "TCP Send Result:" + result);

			synchronized (mSyncObject) {
				if (mSendThread == null || mSendThread.isCanceled()
						|| mSendThread != Thread.currentThread()) {
					return;
				}
			}

			if (result != 0) {
				// 发送失败
				DTRemoteEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
//						if (finalCmd.getSenderUID() != mUID) {
//							return;
//						}
//
//						if (finalCmd.mOnSendFailedListener != null) {
//							finalCmd.mOnSendFailedListener.onSendFailed();
//						} else if (finalCmd.mOnCommonFailedListener != null) {
//							try {
//								finalCmd.mOnCommonFailedListener.onCommonFailed(
//										FailedType.SendFailed, 0, new JSONObject());
//							} catch (JSONException e) {
//								e.printStackTrace();
//								DTRemoteLog.logError();
//								return;
//							}
//						} else {
//							finalCmd.onSendFailed();
//						}

						synchronized (mSyncObject) {
							if (mSendThread == null || mSendThread.isCanceled()
									|| mSendThread != Thread.currentThread()) {
								// 客户端主动断开
							} else if (mSendThread == Thread.currentThread()
									&& !mSendThread.isCanceled()) {
								// 网络原因发送失败需要客户端重新连接
								DTRemoteEnv.getMainHandler().post(new Runnable() {
									@Override
									public void run() {
										reconnect();
									}
								});
							} else {
								// 客户端已经做了重连，并且在极短时间内就连上了
							}
						}
					}
				});

				return;
			}

			// 发送成功
		} while (true);
	}
	
	private void threadFuncRecv() {
		DTRemoteLog.sign(true, "recvThread");

		int recvSize = 0;

		while (true) {
			DTRemoteLog.sign(true, "begin RecvIMSDK");

			synchronized (mSyncObject) {
				if (mSendThread == null
						|| mSendThread.getRecvThread() != Thread.currentThread()
						|| mSendThread.isCanceled()) {
					return;
				}
			}

			recvSize = IMProtocol.RecvIMSDK(mRecvBuffer, mRecvBufferContentSize,
					2048 - mRecvBufferContentSize);
			
			DTRemoteLog.sign(true, "end RecvIMSDK");
			DTRemoteLog.sign(true, "TCP recvSize:" + recvSize);

			synchronized (mSyncObject) {
				if (mSendThread == null
						|| mSendThread.getRecvThread() != Thread.currentThread()
						|| mSendThread.isCanceled()) {
					return;
				}
			}

			if (recvSize <= 0) {
				synchronized (mSyncObject) {
					if (mSendThread != null
							&& mSendThread.getRecvThread() == Thread.currentThread()) {
						// 真正被动断开连接
						DTRemoteEnv.getMainHandler().post(new Runnable() {
							@Override
							public void run() {
								if (getStatus() != ECSocketStatus.None) {
									checkCloseSubThreads();
									setStatus(ECSocketStatus.None);
								}
							}
						});
					} else {
						// 主动断开连接之后recv返回
						// 不处理
					}
				}

				return;
			}

			synchronized (mSyncObject) {
				if (mSendThread == null
						|| mSendThread.getRecvThread() != Thread.currentThread()
						|| mSendThread.isCanceled()) {
					return;
				}
			}
			
			mRecvBufferContentSize += recvSize;
			EngineClient.this.tryDecode();

			synchronized (mSyncObject) {
				if (mSendThread == null
						|| mSendThread.getRecvThread() != Thread.currentThread()
						|| mSendThread.isCanceled()) {
					return;
				}
			}
		}
	}

	private void firstIMAddress() {
		mCurrentIMAddress = null;
		mLastTryConnectTime = System.currentTimeMillis() / 1000;

		// 1. aryAddressesFromSIS
		if (DTDevice.getInstance().mIMAddressListFromSIS.size() > 0) {
			mCurrentIMAddress = DTDevice.getInstance().mIMAddressListFromSIS.get(0);

			if (mCurrentIMAddress.length() == 0) {
				DTRemoteLog.logError();
				return;
			}

			if (!mCurrentIMAddress.contains(":")) {
				DTRemoteLog.logError();
				return;
			}

			return;
		}

		// 2. lastIMAddress
		if (DTDevice.getInstance().mLastIMAddress.length() > 0) {
			mCurrentIMAddress = DTDevice.getInstance().mLastIMAddress;

			if (mCurrentIMAddress.length() == 0) {
				DTRemoteLog.logError();
				return;
			}

			if (!mCurrentIMAddress.contains(":")) {
				DTRemoteLog.logError();
				return;
			}

			return;
		}

		// 3. imAddressFromDomain
		if (DTDevice.getInstance().mIMAddressFromDomain.length() > 0) {
			mCurrentIMAddress = DTDevice.getInstance().mIMAddressFromDomain;

			if (mCurrentIMAddress.length() == 0) {
				DTRemoteLog.logError();
				return;
			}

			if (!mCurrentIMAddress.contains(":")) {
				DTRemoteLog.logError();
				return;
			}

			return;
		}

		// 4. defaultIMAddress
		mCurrentIMAddress = this.getDefaultIMAddress();

		if (mCurrentIMAddress.length() == 0) {
			DTRemoteLog.logError();
			return;
		}

		if (!mCurrentIMAddress.contains(":")) {
			DTRemoteLog.logError();
			return;
		}
	}

	public void checkCloseSocket() {
		DTRemoteLog.sign(true, "checkCloseSocket");

		if (getStatus() == ECSocketStatus.None) {
			return;
		}

		this.checkCloseSubThreads();
		setStatus(ECSocketStatus.None);
	}

	// 主线程调用
	// Socket内部接口，不改变status，不发送通知
	private void checkCloseSubThreads() {
		DTRemoteLog.sign(true, "checkCloseSubThreads");

		if (getStatus() == ECSocketStatus.None) {
			DTRemoteLog.logError();
			return;
		}

		synchronized (mSyncObject) {
			if (mSendThread == null) {
				return;
			}

			mSendThread.cancel();
			mSendThread = null;
		}

		this.removeAllCommand();
	}
	
	// 子线程调用
	private void tryDecode() {
		while (mRecvBufferContentSize >= 24) {
			int length = 0;
			int cmdTypeValue = 0;
			int version = 0;
			int sequenceID = 0;
			long errorCode = 0;
			long uid = 0;
			long compress = 0;

			int start = 0;

			for (int i = 0; i < 2; i++) {
				length = (length << 8) + (mRecvBuffer[i] & 0xff);
			}

			start += 2;

			for (int i = 0; i < 2; i++) {
				cmdTypeValue = (cmdTypeValue << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 2;

			for (int i = 0; i < 2; i++) {
				version = (version << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 2;

			for (int i = 0; i < 2; i++) {
				sequenceID = (sequenceID << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 2;

			for (int i = 0; i < 4; i++) {
				errorCode = (errorCode << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 4;

			for (int i = 0; i < 8; i++) {
				uid = (uid << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 8;

			for (int i = 0; i < 4; i++) {
				compress = (compress << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 4;

			if (length > 2048) {
				Log.e("Debug", "send error: length>2048");
//					DTLog.logError("DTSocket recv error: length>2048");
//					DTAppEnv.cancelPreviousPerformRequest(mReconnectRunnable);
//					DTAppEnv.performAfterDelayOnUIThread(0, mReconnectRunnable);
				return;
			}

			if (version > 10) {
				Log.e("Debug", "send error: version>10");
//					DTLog.logError("DTSocket recv error: version>10");
//					DTAppEnv.cancelPreviousPerformRequest(mReconnectRunnable);
//					DTAppEnv.performAfterDelayOnUIThread(0, mReconnectRunnable);
				return;
			}

			if (0 == length) {
				Log.e("Debug", "send error: length=0");
//					DTLog.logError("DTSocket recv error: length=0");
//					DTAppEnv.cancelPreviousPerformRequest(mReconnectRunnable);
//					DTAppEnv.performAfterDelayOnUIThread(0, mReconnectRunnable);
				return;
			}

			if (0 == cmdTypeValue) {
				Log.e("Debug", "send error: cmdTypeValue=0");
//					DTLog.logError("DTSocket recv error: cmdTypeValue=0");
//					DTAppEnv.cancelPreviousPerformRequest(mReconnectRunnable);
//					DTAppEnv.performAfterDelayOnUIThread(0, mReconnectRunnable);
				return;
			}

			if (length > mRecvBufferContentSize) {
				Log.e("Debug", "send error: length > mRecvBufferContentSize");
//					DTLog.logError("DTSocket recv error: length > mRecvBufferContentSize");
				break;
			}

			String json = "";

			try {
				json = new String(mRecvBuffer, 24, length - 24, "UTF8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				Log.e("Debug", e.toString());
//					DTLog.logError();
//					DTLog.log(e.toString());
				return;
			}

			byte[] buffer = new byte[length];
			for (int i = 0; i < buffer.length; i++) {
				buffer[i] = mRecvBuffer[i];
			}
			dataTransfer(buffer, 0, length);

			if (mRecvBufferContentSize < length) {
				Log.e("Debug", "send error mRecvBufferContentSize < length");
//					DTLog.logError();
				return;
			}

			int leftLength = mRecvBufferContentSize - length;

			if (leftLength > 0) {
				for (int i = 0; i < leftLength; i++) {
					mRecvBuffer[i] = mRecvBuffer[length + i];
				}

				mRecvBufferContentSize = leftLength;
			} else {
				mRecvBufferContentSize = 0;
			}
		}
	}
	
	private void dataTransfer(byte[] buffer, int i, int length) {
		if(isClientExist()) {
			LocalServer.getInstance().send2Client(buffer, 0, length);
		} else {
			
		}
	}
	
	private boolean isClientExist() {
		return RemoteDataManager.getInstance().getClientState();
	}
	
	private boolean mIsRunningSIS = false;
	private String mSISDomainName = "";
	private String mSISDefaultAddress = "";
	private String mDomainName = "";
	private String mDefaultIMAddress = "";
	private String mForceIMAddress = "";
	private String mCurrentIMAddress = "";

	private long mSessionID = 0;
	private long mUID = 0;

	private long mLastTryConnectTime = 0;

	private Object mSyncObject = new Object(); // 为mSendThread和mStatus加锁
	private DTSendThread mSendThread = null;
	private ECSocketStatus mStatus = ECSocketStatus.None;
	private ECSocketStatus mStatusNotified = ECSocketStatus.None;

	private byte[] mRecvBuffer = new byte[2048];
	private int mRecvBufferContentSize = 0;
	
	private Context mContext;
	private Handler mHandler;

	private ArrayList<byte[]> mSynAryCmdNeedSend = new ArrayList<byte[]>();
//	private ArrayList<DTCmd> mSynAryCmdSent = new ArrayList<DTCmd>();

	private Runnable mCheckReconnectWithNextIMAddressRunnable = new Runnable() {
		@Override
		public void run() {
			EngineClient.this.checkReconnectWithNextIMAddress();
		}
	};

	private Runnable mReconnectRunnable = new Runnable() {
		@Override
		public void run() {
			EngineClient.this.reconnect();
		}
	};

	private void printBuffer(byte[] buff, int offset, int count) {
		System.out.println("====================");
		for (int i = offset; i < count; i++) {
			String hex = Integer.toHexString(buff[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			System.out.print(hex.toUpperCase() + " ");
		}
		System.out.println("");
		System.out.println("====================");
	}


}
