package am.core.java;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;

import remote.service.ServerInfoStub;
import remote.service.data.RemoteDataManager;
import remote.service.data.base.DTRemoteEnv;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.os.RemoteException;
import android.util.Log;

public class LocalServer {

	private ServerThread mServerThread;

	private LocalServerSocket server = null;
	private ClientRecvThread clientRecvThread = null;
	
	private Object send2ClientObject = new Object(); //Server接收client阻塞，只允许处理一次client
	
	private LocalServerStatus mStatus = LocalServerStatus.close;
	
	private void setLocalServerStatus(LocalServerStatus status) {
		if(status.equals(LocalServerStatus.listenning)) {
			DTRemoteEnv.getMainHandler().postDelayed(new Runnable() {
				
				@Override
				public void run() {
					if(ServerInfoStub.getInstance().getOnLocalSocketListener() != null) {
						try {
							ServerInfoStub.getInstance().getOnLocalSocketListener().onServerListening();
						} catch (RemoteException e) {
							e.printStackTrace();
						}
						ServerInfoStub.getInstance().setOnLocalSocketListener(null);
					}
				}
			}, 1000);
			
		}
		
		mStatus = status;
	}
	
	public LocalServerStatus getLocalServerStatus() {
		return mStatus;
	}
	
	public enum LocalServerStatus {
		listenning(1), close(0);
		
		int mType;
		LocalServerStatus(int type) {
			mType = type;
		}
		
		public int getValue() {
			return mType;
		}
	}

	public boolean isInit() {
		if (server != null && mServerThread != null) {
			return true;
		}

		return false;
	}
	
	public void init() {
		try {
			server = new LocalServerSocket("imsdk.core.localsocket");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		startServerThread();
	}

//	public boolean isClientConnected() {
//		if (getLocalServerStatus().equals(LocalServerStatus.connected)) {
//			return true;
//		}
//
//		return false;
//	}
	
	public void closeClient() {
		if(clientRecvThread != null) {
			clientRecvThread.closeClient();
			clientRecvThread = null;
		}
	}
	
	public void closeServer() {
		closeClient();
		
		if (mServerThread != null) {
			mServerThread.interrupt();
			mServerThread = null;
		}

		if(server != null) {
			try {
				server.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			server = null;
		}
	}

	private void startServerThread() {
		mServerThread = new ServerThread();
		mServerThread.start();
	}

	class ServerThread extends Thread {

		@Override
		public void run() {
			try {
				while(true) {
					if(server == null) break;
					
					setLocalServerStatus(LocalServerStatus.listenning);
					
					LocalSocket clientSocket = server.accept();
					clientSocket.setSoTimeout(5 * 1000);
					
					Log.e("Debug", "---客户端连接成功---" + clientSocket.toString());
					
					if(clientRecvThread != null) {
						Log.e("Debug", "监听新Client前先删除原有");
						clientRecvThread.closeClient();
						clientRecvThread = null;
					}
					
					clientRecvThread = new ClientRecvThread(clientSocket);
					clientRecvThread.start();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			Log.e("Debug", "---server 关闭---");
			
			setLocalServerStatus(LocalServerStatus.close);
		}

	}
	
	class ClientRecvThread extends Thread {
		private LocalSocket clientSocket = null;

		private BufferedInputStream is = null;
		private BufferedOutputStream os = null;
		
		public ClientRecvThread(LocalSocket client) {
			clientSocket = client;
		}
		
		@Override
		public void run() {
			Log.e("Debug", "---开始服务线程---" + ClientRecvThread.this);
			
			try {
				is = new BufferedInputStream(clientSocket.getInputStream());
				os = new BufferedOutputStream(clientSocket.getOutputStream());
				
				int recvSize = 0;

				while (true) {
					try {
						if(clientSocket == null || is == null) {
							Log.e("Debug", "clientSocket == null || is == null" + ClientRecvThread.this);
							break;
						}
						
						recvSize = is.read(mRecvBuffer, mRecvBufferContentSize, 
								2048 - mRecvBufferContentSize);

						Log.d("BufferTest", "localServer receiver count = " + recvSize);
						
						if(recvSize > 0) {
							mRecvBufferContentSize += recvSize;
							tryDecode();
						} else {
							break;
						}
					} catch (SocketTimeoutException e) {
						e.printStackTrace();
					} catch (IOException e2) {
					}

				}
				Log.e("Debug", "监听Client结束，回收");
				closeClient();
				
				Log.e("Debug", "---客户端断开连接---" + ClientRecvThread.this);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		public void write(byte[] buffer, int offset, int size) {
			if(clientSocket != null && os != null) {
				try {
					Log.d("BufferTest", "localServer send count = " + size);
					
					os.write(buffer, offset, size);
					os.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		public void closeClient() {
			if(clientSocket == null) return;
			
			System.out.println("------------server closeClient----:" + clientSocket.toString());
			
			mRecvBufferContentSize = 0;
			
			System.out.println("------------server closeClient----:1");
			try {
				if(is != null) {
					is.close();
				}
			} catch (Exception e) {
			} finally {
				is = null;
			}
			System.out.println("------------server closeClient----:2");
			try {
				if(os != null) {
					os.close();
				}
			} catch (Exception e) {
			} finally {
				os = null;
			}
			System.out.println("------------server closeClient----:3");
			try {
				if(clientSocket != null) {
					clientSocket.close();
				}
			} catch (Exception e) {
			} finally {
				clientSocket = null;
			}
			System.out.println("------------server closeClient----:4");
			clientRecvThread = null;
			
			
		}
	}
	
	public void send2Client(byte[] buffer, int offset, int size) {
		if(clientRecvThread != null) {
			clientRecvThread.write(buffer, offset, size);
		}
	}
	
	// 子线程调用
	private void tryDecode() {
		while (mRecvBufferContentSize >= 24) {
			if(!RemoteDataManager.getInstance().getClientState()) {
				mRecvBufferContentSize = 0;
				return;
			}
			
			int length = 0;
			int cmdTypeValue = 0;
			int version = 0;
			int sequenceID = 0;
			long errorCode = 0;
			long uid = 0;
			long compress = 0;

			int start = 0;

			for (int i = 0; i < 2; i++) {
				length = (length << 8) + (mRecvBuffer[i] & 0xff);
			}

			start += 2;

			for (int i = 0; i < 2; i++) {
				cmdTypeValue = (cmdTypeValue << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 2;

			for (int i = 0; i < 2; i++) {
				version = (version << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 2;

			for (int i = 0; i < 2; i++) {
				sequenceID = (sequenceID << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 2;

			for (int i = 0; i < 4; i++) {
				errorCode = (errorCode << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 4;

			for (int i = 0; i < 8; i++) {
				uid = (uid << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 8;

			for (int i = 0; i < 4; i++) {
				compress = (compress << 8) + (mRecvBuffer[start + i] & 0xff);
			}

			start += 4;

			if (length > 2048) {
				Log.e("Debug", "send error: length>2048");
//				DTLog.logError("DTSocket recv error: length>2048");
//				DTAppEnv.cancelPreviousPerformRequest(mReconnectRunnable);
//				DTAppEnv.performAfterDelayOnUIThread(0, mReconnectRunnable);
				return;
			}

			if (version > 10) {
				Log.e("Debug", "send error: version>10");
//				DTLog.logError("DTSocket recv error: version>10");
//				DTAppEnv.cancelPreviousPerformRequest(mReconnectRunnable);
//				DTAppEnv.performAfterDelayOnUIThread(0, mReconnectRunnable);
				return;
			}

			if (0 == length) {
				Log.e("Debug", "send error: length=0");
//				DTLog.logError("DTSocket recv error: length=0");
//				DTAppEnv.cancelPreviousPerformRequest(mReconnectRunnable);
//				DTAppEnv.performAfterDelayOnUIThread(0, mReconnectRunnable);
				return;
			}

			if (0 == cmdTypeValue) {
				Log.e("Debug", "send error: cmdTypeValue=0");
//				DTLog.logError("DTSocket recv error: cmdTypeValue=0");
//				DTAppEnv.cancelPreviousPerformRequest(mReconnectRunnable);
//				DTAppEnv.performAfterDelayOnUIThread(0, mReconnectRunnable);
				return;
			}

			if (length > mRecvBufferContentSize) {
				Log.e("Debug", "send error: length > mRecvBufferContentSize");
//				DTLog.logError("DTSocket recv error: length > mRecvBufferContentSize");
				break;
			}

			String json = "";

			try {
				json = new String(mRecvBuffer, 24, length - 24, "UTF8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				Log.e("Debug", e.toString());
//				DTLog.logError();
//				DTLog.log(e.toString());
				return;
			}

			byte[] buffer = new byte[length];
			for (int i = 0; i < buffer.length; i++) {
				buffer[i] = mRecvBuffer[i];
			}
			
			if(RemoteDataManager.getInstance().getClientState()) {
				EngineClient.getInstance().send2Server(buffer);
			}

			if (mRecvBufferContentSize < length) {
				Log.e("Debug", "send error mRecvBufferContentSize < length");
//				DTLog.logError();
				return;
			}

			int leftLength = mRecvBufferContentSize - length;

			if (leftLength > 0) {
				for (int i = 0; i < leftLength; i++) {
					mRecvBuffer[i] = mRecvBuffer[length + i];
				}

				mRecvBufferContentSize = leftLength;
			} else {
				mRecvBufferContentSize = 0;
			}
		}
	}
	
	private byte[] mRecvBuffer = new byte[2048];
	private int mRecvBufferContentSize = 0;

	private LocalServer() { }

	private static LocalServer instance = null;

	public synchronized static LocalServer getInstance() {
		if (instance == null) {
			instance = new LocalServer();
		}

		return instance;
	}
	
	private void printBuffer(byte[] buff, int count) {
		Log.i("BufferTest", "==========localServer==========");
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < count; i++) {
			String hex = Integer.toHexString(buff[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}
			sb.append(hex.toUpperCase() + " ");
		}
		Log.i("BufferTest", sb.toString());
		Log.i("BufferTest", "==========end localServer==========");
	}

}
