package am.core.jni;

public class IMProtocol {
	public native static int InitIMSDK(String ip, int port);

	public native static int Close();

	// 返回接收数据长度
	public native static int RecvIMSDK(byte[] buf, int offset, int size);

	// 返回已发送数据长度
	public native static int SendIMSDK(byte[] buf, int offset, int size);

	public native static String GetErrorMessage();

	public native static int GetSdkVersion();

	private static final String soName = "imsdk";

	static {
		System.loadLibrary(soName);
	}
}
