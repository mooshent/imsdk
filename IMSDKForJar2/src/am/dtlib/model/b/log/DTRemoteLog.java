package am.dtlib.model.b.log;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.util.Log;

public final class DTRemoteLog {
//	public static String getErrorLogFileFullPath() {
//		String fileName = "DEL";
//
//		if (DTAppEnv.isSDKDebugable()) {
//			fileName = "DTErrorLog.txt";
//		}
//		
//		File errorLog = new File(DTAppEnv.getIMSDKDirectoryFullPath(), fileName);
//		
//		if(!errorLog.exists()) {
//			try {
//				errorLog.createNewFile();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//
//		return errorLog.toString();
//	}
//
//	public static String getCommonLogFileFullPath() {
//		String fileName = "DL";
//
//		if (DTAppEnv.isSDKDebugable()) {
//			fileName = "DTLog.txt";
//		}
//		
//		File commonLog = new File(DTAppEnv.getIMSDKDirectoryFullPath(), fileName);
//		
//		if(!commonLog.exists()) {
//			try {
//				commonLog.createNewFile();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//
//		return commonLog.toString();
//	}
//
//	public static boolean isFolderExists(String folderFullPath) {
//		File file = new File(folderFullPath);
//
//		if (file.exists() && file.isDirectory()) {
//			return true;
//		} else {
//			return false;
//		}
//	}
//
//	public static void checkCreateDirectory(String fullPath) {
//		File file = new File(fullPath);
//
//		if (file.exists() && file.isDirectory()) {
//			return;
//		}
//
//		if (!file.mkdirs()) {
//			DTRemoteLog.logError();
//		}
//	}

	public static void sign() {
		signPrivate(4);
	}

	public static void sign(boolean ifSign, String strSignContent) {
		if (ifSign) {
			addLog(strSignContent, false);
			sign(strSignContent);
		}
	}
	
	
	public static void d(String logmsg){
		System.out.println(logmsg);
		Log.d("IMSDK", logmsg);
	}

	public static void sign(String strSignContent) {
		signPrivate(5);

		if (strSignContent == null) {
			System.out.println("null");
			Log.d("IMSDK", "null");
		} else if (strSignContent.length() == 0) {
			System.out.println("Empty String");
			Log.d("IMSDK", "Empty String");
		} else {
			System.out.println(strSignContent);
			Log.d("IMSDK", strSignContent);
		}
	}

	public static void sign(int nSignContent) {
		signPrivate(4);
		System.out.println("" + nSignContent);
	}

	public static void sign(float fSignContent) {
		signPrivate(4);
		Log.d("IMSDK", "" + fSignContent);
		System.out.println("" + fSignContent);
	}

	public static void sign(long lSignContent) {
		signPrivate(4);
		Log.d("IMSDK", "" + lSignContent);
		System.out.println("" + lSignContent);
	}

	public static void sign(byte byteSignContent) {
		signPrivate(4);
		Log.d("IMSDK", "" + byteSignContent);
		System.out.println("" + byteSignContent);
	}

	public static void sign(boolean bSignContent) {
		signPrivate(4);
		Log.d("IMSDK", "" + bSignContent);
		System.out.println("" + bSignContent);
	}

	public static void signGrade(int nGradeCount) {
		for (int i = 0; i < nGradeCount; i++) {
			signPrivate(4 + i);
		}
	}

	private static void signPrivate(int nGrade) {
		System.out.println("File:"
				+ Thread.currentThread().getStackTrace()[nGrade].getFileName()
				+ " Line:"
				+ Thread.currentThread().getStackTrace()[nGrade].getLineNumber()
				+ " Class:"
				+ Thread.currentThread().getStackTrace()[nGrade].getClassName()
				+ " Method:"
				+ Thread.currentThread().getStackTrace()[nGrade].getMethodName());
		
		Log.d("IMSDK", "File:"
				+ Thread.currentThread().getStackTrace()[nGrade].getFileName()
				+ " Line:"
				+ Thread.currentThread().getStackTrace()[nGrade].getLineNumber()
				+ " Class:"
				+ Thread.currentThread().getStackTrace()[nGrade].getClassName()
				+ " Method:"
				+ Thread.currentThread().getStackTrace()[nGrade].getMethodName());
	}

	public static void log() {
		addLog(null, false);
		sign();
	}

	public static void log(String strContent) {
		addLog(strContent, false);
		sign(strContent);
	}

	public static void logError() {
		addLog("Error Occured!", true);
		addLog("Error Occured!", false);
	}
	
	public static void logError(String errerMsg) {
		addLog("Error Occured! "+errerMsg, true);
		addLog("Error Occured! "+errerMsg, false);
	}

	// 中文字符只能输出到文件
	public static void log(byte[] btLogContent) {
		String strLogContent = "";

		try {
			strLogContent = new String(btLogContent, "UTF8");
		} catch (UnsupportedEncodingException e) {
		}

		addLog(strLogContent, false);
	}

	// 参数为null时输出title和空行
	private static void addLog(String strLogContent, boolean error) {
		int nGrade = 4;
		final String strTab = "-";
		final String strEndline = "  \r\n";

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",
				Locale.getDefault());
		String strTitle = simpleDateFormat.format(new Date()) + strEndline + strEndline
				+ "File:"
				+ Thread.currentThread().getStackTrace()[nGrade].getFileName()
				+ strEndline + strTab + strTab + strTab + "Line:"
				+ Thread.currentThread().getStackTrace()[nGrade].getLineNumber()
				+ strEndline + strTab + strTab + strTab + "Class:"
				+ Thread.currentThread().getStackTrace()[nGrade].getClassName()
				+ strEndline + strTab + strTab + strTab + "Method:"
				+ Thread.currentThread().getStackTrace()[nGrade].getMethodName()
				+ strEndline + strEndline;

		if (Thread.currentThread().getStackTrace().length > ++nGrade) {
			strTitle += "File:"
					+ Thread.currentThread().getStackTrace()[nGrade].getFileName()
					+ strEndline + strTab + strTab + strTab + "Line:"
					+ Thread.currentThread().getStackTrace()[nGrade].getLineNumber()
					+ strEndline + strTab + strTab + strTab + "Class:"
					+ Thread.currentThread().getStackTrace()[nGrade].getClassName()
					+ strEndline + strTab + strTab + strTab + "Method:"
					+ Thread.currentThread().getStackTrace()[nGrade].getMethodName()
					+ strEndline + strEndline;
		}

		if (Thread.currentThread().getStackTrace().length > ++nGrade) {
			strTitle += "File:"
					+ Thread.currentThread().getStackTrace()[nGrade].getFileName()
					+ strEndline + strTab + strTab + strTab + "Line:"
					+ Thread.currentThread().getStackTrace()[nGrade].getLineNumber()
					+ strEndline + strTab + strTab + strTab + "Class:"
					+ Thread.currentThread().getStackTrace()[nGrade].getClassName()
					+ strEndline + strTab + strTab + strTab + "Method:"
					+ Thread.currentThread().getStackTrace()[nGrade].getMethodName()
					+ strEndline + strEndline;
		}

		if (Thread.currentThread().getStackTrace().length > ++nGrade) {
			strTitle += "File:"
					+ Thread.currentThread().getStackTrace()[nGrade].getFileName()
					+ strEndline + strTab + strTab + strTab + "Line:"
					+ Thread.currentThread().getStackTrace()[nGrade].getLineNumber()
					+ strEndline + strTab + strTab + strTab + "Class:"
					+ Thread.currentThread().getStackTrace()[nGrade].getClassName()
					+ strEndline + strTab + strTab + strTab + "Method:"
					+ Thread.currentThread().getStackTrace()[nGrade].getMethodName()
					+ strEndline + strEndline;
		}

		if (Thread.currentThread().getStackTrace().length > ++nGrade) {
			strTitle += "File:"
					+ Thread.currentThread().getStackTrace()[nGrade].getFileName()
					+ strEndline + strTab + strTab + strTab + "Line:"
					+ Thread.currentThread().getStackTrace()[nGrade].getLineNumber()
					+ strEndline + strTab + strTab + strTab + "Class:"
					+ Thread.currentThread().getStackTrace()[nGrade].getClassName()
					+ strEndline + strTab + strTab + strTab + "Method:"
					+ Thread.currentThread().getStackTrace()[nGrade].getMethodName()
					+ strEndline + strEndline;
		}

//		try {
//			FileWriter file = new FileWriter(error ? getErrorLogFileFullPath()
//					: getCommonLogFileFullPath(), true);
//
//			file.append(strEndline);
//			file.append(strEndline);
//			file.append(strEndline);
//
//			if (strLogContent == null) {
//				file.append(strEndline);
//			} else {
//				file.append(strTab + strTab + strTab + strLogContent + strEndline);
//			}
//
//			file.append(strTitle);
//			file.append(strEndline);
//			file.append(strEndline);
//			file.append(strEndline);
//
//			file.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}

		if (error) {
			System.err.println(strTitle + " " + strLogContent);
			Log.d("IMSDK", strTitle + " " + strLogContent);
		}
	}

	// 获取日志 并清空
	public static String getErrorLogThenClear() {
		String strResult = "";

//		try {
//			FileReader fileReader = new FileReader(getErrorLogFileFullPath());
//			BufferedReader bufferReader = new BufferedReader(fileReader, 500);
//			String strLine;
//
//			while ((strLine = bufferReader.readLine()) != null) {
//				strResult += strLine;
//				strResult += "\r\n";
//			}
//
//			bufferReader.close();
//			fileReader.close();
//		} catch (FileNotFoundException e) {
//			DTRemoteLog.logError();
//			DTRemoteLog.log(e.toString());
//			return "";
//		} catch (IOException e) {
//			DTRemoteLog.logError();
//			DTRemoteLog.log(e.toString());
//			return "";
//		}

		return strResult;
	}

	public static void clearAll() {
//		File file = new File(getErrorLogFileFullPath());
//
//		if (!file.delete()) {
//			DTRemoteLog.logError();
//		}
//
//		file = new File(getCommonLogFileFullPath());
//
//		if (!file.delete()) {
//			DTRemoteLog.logError();
//		}
	}
}
