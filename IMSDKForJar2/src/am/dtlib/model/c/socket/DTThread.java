package am.dtlib.model.c.socket;

public class DTThread extends Thread {
	public DTThread(Runnable runnable) {
		super(runnable);
	}
	
	public boolean isCanceled() {
		return mCancelValue;
	}
	
	public void cancel() {
		synchronized (mSynCancelObject) {
			mCancelValue = true;
		}
		
		this.interrupt();
	}

	private Object mSynCancelObject = new Object();
	private boolean mCancelValue = false;
}
