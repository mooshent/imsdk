package am.dtlib.model.c.socket;

import remote.service.PushService;
import remote.service.aidl.OnLocalSocketListener;
import remote.service.aidl.ServerInfoListener;
import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.c.socket.DTSocket.DTSocketStatus;
import am.dtlib.model.c.tool.ProcessTool;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

public final class DTConnect {
	
	private static OnServerSocketConnectListener mConnectListener;
	
	public static boolean getLocalSocketState(OnServerSocketConnectListener l) {
		if(DTSocket.getInstance().getStatus().equals(DTSocketStatus.Connected)) {
			return true;
		}
		
		mConnectListener = l;
		
//		if(DTSocket.getInstance().getStatus() != DTSocketStatus.None) {
//			DTSocket.getInstance().checkCloseSocket();
//		}
		
		if(connectListener == null) {
			if(ProcessTool.isRemoteProcessExist(DTAppEnv.getContext())) {
				DTAppEnv.getContext().startService(new Intent(DTAppEnv.getContext(), PushService.class));
			}
			
			DTAppEnv.getContext().bindService(
					new Intent(DTAppEnv.getContext(), PushService.class), 
					serviceConnection, Context.BIND_AUTO_CREATE);
		} else {
			checkServerSocketStatus();
		}
		
		return false;
	}
	
	public static void closeClient() {
		if(connectListener != null) {
			try {
				connectListener.closeSocket();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void disconnectService() {
		if(connectListener != null) {
			try {
				connectListener.closeClient();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			
			DTAppEnv.getContext().unbindService(serviceConnection);
		}
	}
	
	private static void checkServerSocketStatus() {
		try {
			switch (connectListener.getLocalServerStatus()) {
			case 0:
				Log.e("Debug", "LocalServer 绑定成功, 服务未打开或已被连接, 正在尝试重启远程服务");
				connectListener.reconnect(localSocketListener);
				break;
			case 1:
				Log.d("Debug", "LocalServer 绑定成功, 服务等待连接");
				if(mConnectListener != null) {
					mConnectListener.onServerListening();
					mConnectListener = null;
				}
				break;
			default:
				break;
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	private static ServerInfoListener connectListener;
	
	private static ServiceConnection serviceConnection = new ServiceConnection() {
		
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			connectListener = ServerInfoListener.Stub.asInterface(service);
			
			checkServerSocketStatus();
		}
		
		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.e("Debug", "LocalServer 断开连接");
			connectListener = null;
			
			DTAppEnv.getContext().unbindService(serviceConnection);
			
			serviceConnection = null;
		}
		
	};
	
	static OnLocalSocketListener.Stub localSocketListener = new OnLocalSocketListener.Stub() {
		
		@Override
		public void onServerListening() throws RemoteException {
			Log.d("Debug", "服务重启完成, 正在等待连接");
			if(mConnectListener != null) {
				mConnectListener.onServerListening();
				mConnectListener = null;
			}
		}
		
		@Override
		public void onServerClosed() throws RemoteException {
			
		}
	};
	
	interface OnServerSocketConnectListener {
		void onServerListening();
	}

}
