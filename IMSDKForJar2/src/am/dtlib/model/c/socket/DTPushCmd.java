package am.dtlib.model.c.socket;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.b.log.DTLog;

public abstract class DTPushCmd extends DTCmd {
	@Override
	public final void initSendJsonObject() {
		DTLog.logError();
	}

	@Override
	public final void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	@Override
	public final void onSendFailed() {
		DTLog.logError();
	}

	@Override
	public final void onNoRecv() {
		DTLog.logError();
	}

	@Override
	public void onRecv(JSONObject recvJsonObject) throws JSONException {
		DTLog.logError();
	}
}
