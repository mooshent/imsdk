package am.dtlib.model.c.socket;

import java.io.UnsupportedEncodingException;
import java.util.Observer;

import org.json.JSONException;
import org.json.JSONObject;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;

public abstract class DTCmd {
	public static int CMD_ERROR_NONE = 0; // 无错误
	public static int CMD_ERROR_UNKNOWN = 1; // 未知错误
	public static int CMD_ERROR_SERVER_BUSY = 2; // 服务器繁忙
	public static int CMD_ERROR_CLIENT_BUG = 3; // 客户端请求数据包异常
	public static int CMD_ERROR_OUT_OF_REACH = 4; // 未能到达，权限不够（如金币不足，或无法查看私照）
	public static int CMD_ERROR_TOUCH_TOP = 5; // 已到达最大值（如周边用户已全部拉取完成）
	public static int CMD_ERROR_NO_DATA = 6; // 无可用数据返回
	public static int CMD_ERROR_TIMEOUT = 7; // 已过期
	public static int CMD_ERROR_OUT = 8; // 要求离开
	public static int CMD_ERROR_ACCOUNT = 9; // 注册帐号已存在
	public static int CMD_ERROR_BLACKLIST = 10; // 黑名单不允许发送消息
	public static int CMD_ERROR_WHITELIST = 11; // 不在白名单不允许发送消息
	public static int CMD_ERROR_CID = 12; // CID与UID配对错误
	public static int CMD_ERROR_APPKEY = 13; // AppKey错误
	public static int CMD_ERROR_PASSWORD = 100; // 密码错误
	public static int CMD_ERROR_NET = 101; // 网络错误
	public static int CMD_ERROR_SYSTEM = 102; // 服务端系统错误
	public static int CMD_ERROR_TOUID_NOT_EXIST = 104; // 目标UID不存在
	public static int CMD_ERROR_TEAM_MAXNUM = 105; // 已到达小组数目上限
	public static int CMD_ERROR_TEAM_PRI = 106; // 密码错误
	public static int CMD_ERROR_CID_NOT_EXIST = 108; // CID已存在

	private Observer mSocketUpdatedObserver = null;

	public int getCmdTypeValue() {
		return mCmdTypeValue;
	}

	public int getVersion() {
		return mVersion;
	}

	public int getSequenceID() {
		return mSequenceID;
	}

	public long getSession() {
		return mSession;
	}

	public long getSenderUID() {
		return mSenderUID;
	}

	public long getCompress() {
		return mCompress;
	}

	public int getSendLength() {
		return mSendLength;
	}

	public String getSendJson() {
		return mSendJson;
	}

	public long getRecvLength() {
		return mRecvLength;
	}

	public String getRecvJson() {
		return mRecvJson;
	}

	public void setTimeoutInterval(long timeoutInterval) {
		mTimeoutInterval = timeoutInterval;
	}

	public long getTimeoutInterval() {
		return mTimeoutInterval;
	}

	public long getSendTime() {
		return mSendTime;
	}

	public abstract void initSendJsonObject() throws JSONException;

	public int encode(byte[] buffer) {
		if (buffer == null) {
			DTLog.logError();
			return 0;
		}

		if (mCmdTypeValue == 0) {
			DTLog.logError();
			DTLog.log(this.getClass().getSimpleName());
			return 0;
		}

		try {
			this.initSendJsonObject();
			mSendJson = mSendJsonObject.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			DTLog.logError();
			mSendJson = "";
		}

		if (mCmdTypeValue == 0) {
			DTLog.logError();
			return 0;
		}

		buffer[0] = (byte) (mSendLength >> 8);
		buffer[1] = (byte) mSendLength;
		buffer[2] = (byte) (mCmdTypeValue >> 8);
		buffer[3] = (byte) mCmdTypeValue;
		buffer[4] = (byte) (mVersion >> 8);
		buffer[5] = (byte) mVersion;
		buffer[6] = (byte) (mSequenceID >> 8);
		buffer[7] = (byte) mSequenceID;
		mSession = DTSocket.getInstance().getSessionID();
		buffer[8] = (byte) (mSession >> 24);
		buffer[9] = (byte) (mSession >> 16);
		buffer[10] = (byte) (mSession >> 8);
		buffer[11] = (byte) mSession;
		mSenderUID = DTSocket.getInstance().getUID();
		buffer[12] = (byte) (mSenderUID >> 56);
		buffer[13] = (byte) (mSenderUID >> 48);
		buffer[14] = (byte) (mSenderUID >> 40);
		buffer[15] = (byte) (mSenderUID >> 32);
		buffer[16] = (byte) (mSenderUID >> 24);
		buffer[17] = (byte) (mSenderUID >> 16);
		buffer[18] = (byte) (mSenderUID >> 8);
		buffer[19] = (byte) mSenderUID;
		buffer[20] = (byte) (mCompress >> 24);
		buffer[21] = (byte) (mCompress >> 16);
		buffer[22] = (byte) (mCompress >> 8);
		buffer[23] = (byte) mCompress;

		mSendLength = 24;

		byte[] jsonBuf = null;

		if (mSendJson != null) {
			try {
				jsonBuf = mSendJson.getBytes("UTF8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				DTLog.logError();
				DTLog.log(e.toString());
				return 0;
			}

			if (jsonBuf.length > 2048 - 24) {
				DTLog.logError();
				return 0;
			}

			for (int i = 0; i < jsonBuf.length; i++) {
				buffer[i + 24] = jsonBuf[i];
			}

			mSendLength += jsonBuf.length;
		}

		buffer[0] = (byte) (mSendLength >> 8);
		buffer[1] = (byte) mSendLength;

		return mSendLength;
	}

	public byte[] getSendData() {
		if (mBufferSend != null) {
			return mBufferSend;
		}

		mBufferSend = new byte[2050];
		this.encode(mBufferSend);

		return mBufferSend;
	}

	public void send() {
		if (mTimeoutInterval == 0) {
			DTLog.logError();
			return;
		}

		if (mSendTime != 0) {
			// 超时退出
			if (System.currentTimeMillis() - mSendTime * 1000 > mTimeoutInterval * 1000) {
				if (mOnSendFailedListener != null) {
					DTAppEnv.performAfterDelayOnUIThread(0, new Runnable() {
						@Override
						public void run() {
							mOnSendFailedListener.onSendFailed();
						}
					});
				} else if (mOnCommonFailedListener != null) {
					DTAppEnv.performAfterDelayOnUIThread(0, new Runnable() {
						@Override
						public void run() {
							try {
								mOnCommonFailedListener.onCommonFailed(
										FailedType.SendFailed, 0, null);
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					});
				} else {
					DTAppEnv.performAfterDelayOnUIThread(0, new Runnable() {
						@Override
						public void run() {
							onSendFailed();
						}
					});
				}

				return;
			}
		}

		if (mSendTime == 0) {
			mSendTime = System.currentTimeMillis() / 1000;
		}
		
		DTSocket.getInstance().sendCmd(this);
	}

	public void sendWithTimeInterval(long timeoutInterval) {
		mTimeoutInterval = timeoutInterval;
		send();
	}

	public void onRecv(JSONObject recvJsonObject) throws JSONException {
	}

	public void onRecvError(long errorCode, JSONObject errorJsonObject)
			throws JSONException {
		DTLog.logError();
	}

	public void onSendFailed() {
	}

	public void onNoRecv() {
	}

	public interface OnRecvListener {
		public void onRecv(JSONObject recvJsonObject) throws JSONException;
	}

	public OnRecvListener mOnRecvListener;

	public interface OnRecvErrorListener {
		public void onRecvError(long errorCode, JSONObject errorJsonObject)
				throws JSONException;
	}

	public OnRecvErrorListener mOnRecvErrorListener;

	public interface OnSendFailedListener {
		public void onSendFailed();
	}

	public OnSendFailedListener mOnSendFailedListener;

	public interface OnNoRecvListener {
		public void onNoRecv();
	}

	public OnNoRecvListener mOnNoRecvListener;

	public enum FailedType {
		SendFailed(1), NoRecv(2), RecvError(3);

		private final int value;

		private FailedType(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	public interface OnCommonFailedListener {
		public void onCommonFailed(FailedType type, long errorCode,
				JSONObject errorJsonObject) throws JSONException;
	}

	public OnCommonFailedListener mOnCommonFailedListener;

	public interface OnRecvEndListener {
		public void onRecvEnd(JSONObject jsonObject) throws JSONException;
	}

	public OnRecvEndListener mOnRecvEndListener;

	public boolean compare(DTCmd cmd) {
		return false;
	}

	private static int s_sequenceID = 0;

	public DTCmd() {
		mVersion = 1;
		mSession = DTSocket.getInstance().getSessionID();
		mSequenceID = ++s_sequenceID;
		mTimeoutInterval = 5;
	}

	protected int mCmdTypeValue;

	private int mVersion;
	private int mSequenceID;
	private long mSession;
	private long mSenderUID;
	private long mCompress;

	private int mSendLength;
	private String mSendJson;
	protected JSONObject mSendJsonObject = new JSONObject();

	private int mRecvLength;
	private String mRecvJson;

	private long mTimeoutInterval;
	private long mSendTime;

	private byte[] mBufferSend = null;
	
	private int reTrySend = 0;
}
