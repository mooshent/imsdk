package am.dtlib.model.c.tool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.b.log.DTLog;
import android.util.Log;

public final class DTNotificationCenter {
	private class DTObservable extends Observable {
		public void post(Object data) {
			this.setChanged();
			this.notifyObservers(data);
		}

		@Override
		public void addObserver(Observer observer) {
			mAryObservers.add(observer);
			super.addObserver(observer);
		}
		
		@Override
		public synchronized void deleteObserver(Observer observer) {
			mAryObservers.remove(observer);
			super.deleteObserver(observer);
		}

		public ArrayList<Observer> getObservers() {
			return mAryObservers;
		}

		private ArrayList<Observer> mAryObservers = new ArrayList<Observer>();
	}

	public void addObserver(String notification, Observer observer) {
		if (observer == null) {
			DTLog.logError();
			return;
		}

		DTObservable observable = mMapObservables.get(notification);

		if (observable == null) {
			observable = new DTObservable();
			mMapObservables.put(notification, observable);
		}

		observable.addObserver(observer);
	}
	
	public boolean containObserver(String notification) {
		return mMapObservables.containsKey(notification);
	}

	public void removeObserver(String notification, Observer observer) {
		Log.e("DebugDelete", notification + "---" + observer);
		DTObservable observable = mMapObservables.get(notification);

		if (observable != null) {
			observable.deleteObserver(observer);

			if (observable.countObservers() == 0) {
				mMapObservables.remove(notification);
			}
		}
	}

	public void removeObservers(String notification) {
		DTObservable observable = mMapObservables.get(notification);

		if (observable != null) {
			observable.deleteObservers();
			
			mMapObservables.remove(notification);
		}
	}

//	public void removeObserver(Observer observer) {
//		Iterator<HashMap.Entry<String, DTObservable>> iterator = mMapObservables
//				.entrySet().iterator();
//
//		while (iterator.hasNext()) {
//			HashMap.Entry<String, DTObservable> entry = iterator.next();
//			DTObservable observable = entry.getValue();
//
//			observable.deleteObserver(observer);
//
//			if (observable.countObservers() == 0) {
//				iterator.remove();
//			}
//		}
//		
//		
//	}

	public void postNotification(String notification) {
		try {
			this.postNotification(notification, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void postSyncNotification(String notification) {
		try {
			this.postSyncNotification(notification, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void postNotification(String notification, final Object object) {
		DTLog.sign("post Notification:" + notification);

		final DTObservable observable = mMapObservables.get(notification);

		if (observable != null) {
			if (DTAppEnv.isUIThread()) {
				observable.post(object);
			} else {
				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						observable.post(object);
					}
				});
			}
		}
	}

	public void postSyncNotification(String notification, final Object object) {
		DTLog.sign("post Sync Notification:" + notification);

		final DTObservable observable = mMapObservables.get(notification);

		if (observable != null) {
			if (DTAppEnv.isUIThread()) {
				for (Observer observer : observable.getObservers()) {
					observer.update(observable, object);
				}
			} else {
				final Object syncObject = new Object();

				DTAppEnv.getMainHandler().post(new Runnable() {
					@Override
					public void run() {
						for (Observer observer : observable.getObservers()) {
							observer.update(observable, object);
						}

						synchronized (syncObject) {
							syncObject.notify();
						}
					}
				});

				try {
					synchronized (syncObject) {
						syncObject.wait();
					}
				} catch (InterruptedException e) {
				}
			}
		}
	}

	// singleton
	private volatile static DTNotificationCenter sSingleton;

	private DTNotificationCenter() {
	}

	public static DTNotificationCenter getInstance() {
		if (sSingleton == null) {
			synchronized (DTNotificationCenter.class) {
				if (sSingleton == null) {
					sSingleton = new DTNotificationCenter();
				}
			}
		}

		return sSingleton;
	}

	// singleton end

	private HashMap<String, DTObservable> mMapObservables = new HashMap<String, DTObservable>();
}
