package am.dtlib.model.c.tool;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import am.dtlib.model.b.log.DTLog;

public final class DTFileTool {
	public static void checkRemoveDirectory(String directoryFullPath) {
		if (directoryFullPath.length() == 0) {
			DTLog.logError();
			return;
		}

		File file = new File(directoryFullPath);

		checkRemoveFile(file);
	}
	
	private static void checkRemoveFile(File file) {
		if (file.isDirectory()) {
			for (File child : file.listFiles()) {
				checkRemoveFile(child);
		    }
		}
		
		if (file.exists() && !file.delete()) {
			DTLog.logError();
			return;
		}
	}

	public static void checkRemoveDirectoryOfFilePath(String fileFullPath) {
		DTLog.sign("fileFullPath:" + fileFullPath);
		
		if (fileFullPath.length() == 0) {
			DTLog.logError();
			return;
		}

		File file = new File(fileFullPath);
		String directoryPath = file.getParent();

		file = new File(directoryPath);

		checkRemoveFile(file);
	}
	
	public static boolean writeToFile(byte[] buffer, String localFullPath) {
		FileOutputStream fileOutputStream = null;

		try {
			fileOutputStream = new FileOutputStream(localFullPath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			DTLog.logError();
			DTLog.log(e.toString());
			return false;
		}

		try {
			fileOutputStream.write(buffer);
			fileOutputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
			DTLog.logError();
			DTLog.log(e.toString());
			return false;
		} finally {
			try {
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				DTLog.logError();
				DTLog.log(e.toString());
				return false;
			}
		}
		
		return true;
	}
	
	public static byte[] getContentOfFile(String localFullPath) {
		File file = new File(localFullPath);

		if (!file.exists()) {
			return null;
		}

		FileInputStream fileInputStream;

		try {
			fileInputStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			return null;
		}

		byte[] buffer;

		try {
			buffer = new byte[fileInputStream.available()];
			fileInputStream.read(buffer);
			fileInputStream.close();
		} catch (IOException e) {
			return null;
		}
		
		return buffer;
	}

	public static byte[] getContentOfFile(String localFullPath, int offset) {
		if (offset < 0) {
			DTLog.logError();
			return null;
		}
		
		File file = new File(localFullPath);

		if (!file.exists()) {
			return null;
		}

		FileInputStream fileInputStream;

		try {
			fileInputStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			return null;
		}

		byte[] buffer;

		try {
			if (fileInputStream.available() <= offset) {
				fileInputStream.close();
				return null;
			}
			
			buffer = new byte[fileInputStream.available() - offset];
			fileInputStream.read(buffer);
			fileInputStream.close();
		} catch (IOException e) {
			return null;
		}
		
		return buffer;
	}
	
	public static boolean fileExistsAtPath(String localFullPath) {
		File file = new File(localFullPath);
		
		return file.exists() && !file.isDirectory();
	}
}
