package am.dtlib.model.c.tool;

import android.app.ActivityManager;
import android.content.Context;

public class ProcessTool {

	public static String getCurProcessName(Context context) {
		int pid = android.os.Process.myPid();
		ActivityManager mActivityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningAppProcessInfo appProcess : mActivityManager
				.getRunningAppProcesses()) {
			if (appProcess.pid == pid) {
				return appProcess.processName;
			}
		}
		return null;
	}
	
	public static boolean isRemoteProcessExist(Context context) {
		String processName = context.getPackageName() + ":im";
		
		ActivityManager mActivityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningAppProcessInfo appProcess : mActivityManager
				.getRunningAppProcesses()) {
			if(appProcess.processName.contains(context.getPackageName())) {
				System.out.println("------------------------" + appProcess.processName);
			}
			
			if (appProcess.processName.equals(processName)) {
				return true;
			}
		}
		
		return false;
	}
	
	public static String getProcessName(Context context) {
		int pid = android.os.Process.myPid();
		ActivityManager mActivityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningAppProcessInfo appProcess : mActivityManager
				.getRunningAppProcesses()) {
			if (appProcess.pid == pid) {
				return appProcess.processName;
			}
		}
		
		return "";
	}
	
	public static boolean isRemoteProcess(Context context) {
		if(getProcessName(context).equals(":im")) {
			return true;
		}
		
		return false;
	}

}
