package imsdk.views;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import am.imsdk.demo.gif.GifEmotionUtils;
import am.imsdk.demo.util.FacesXml;
import am.imsdk.demo.util.FileUtils;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.TextView;

public class IMEmotionTextView extends TextView {
	private GifEmotionUtils mGifEmotionUtils;
	private Handler mHandler;
	private Context mContext;

	public IMEmotionTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mHandler = new Handler();
		this.mContext = context;
	}

	public IMEmotionTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mHandler = new Handler();
		this.mContext = context;
	}

	public IMEmotionTextView(Context context) {
		super(context);

		mHandler = new Handler();
		this.mContext = context;
	}

	public void setStaticEmotionText(String content) {
		if (content == null || "".equals(content)) {
			this.setText("");
			return;
		}

		if (init(mContext)) {
			if (mGifEmotionUtils == null) {
				mGifEmotionUtils = new GifEmotionUtils(10);
			}

			mGifEmotionUtils.setStaticEmotionText(this, content);
		} else {
			this.setText(content);
		}

	}

	public void setGifEmotionText(String content) {
		if (content == null || "".equals(content)) {
			this.setText("");
			return;
		}

		if (init(mContext)) {
			if (mGifEmotionUtils == null) {
				mGifEmotionUtils = new GifEmotionUtils(10);
			}

			mGifEmotionUtils.setSpannableText(this, content, mHandler);
		} else {
			this.setText(content);
		}
	}

	private boolean init(Context mContext) {
		if (FacesXml.hasParse()) {
			return true;
		}

		File file = new File(FileUtils.getGifStorePath(), "face.xml");
		
		if (!file.exists()) {
			mContext.getSharedPreferences(FacesXml.key, Context.MODE_PRIVATE).edit()
					.putBoolean(FacesXml.key, false).commit();
			return false;
		}

		FileInputStream fis = null;

		try {
			fis = new FileInputStream(file);

			if (FacesXml.startParse(new FileInputStream(file))) {
			}

			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
