package imsdk.views;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnActionProgressListener;
import imsdk.data.IMMyself.OnReceiveBitmapListener;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.demo.util.CPResourceUtil;
import am.imsdk.demo.util.FileUtils;
import am.imsdk.model.IMAppSettings;
import am.imsdk.model.IMAudioSender;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.a2.IMMyself2;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;
import am.imsdk.model.amim.IMUserMsgsMgr;
import am.imsdk.model.im.IMUserChatMsgHistory;
import am.imsdk.model.im.IMUserMsgHistoriesMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;
import am.imsdk.ui.adapter.IMChatViewAdapter;
import am.imsdk.ui.views.IMBaseChatView;
import am.imsdk.ui.views.dialog.OperationChatDialog;
import am.imsdk.ui.views.dialog.ShowTextMsgDialog;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.ClipboardManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class IMChatView extends IMBaseChatView {
	// init data
	private String mCustomUserID = "";
	private IMUserChatMsgHistory mUserChatMsgHistory;
	
	private ClipboardManager clipboard;
	private AudioManager audioManager;

	public IMChatView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public IMChatView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	//listener
	public interface OnHeadPhotoClickListener{
		public void onClick(View v, String customUserID);
	}
	
	public void setOnHeadPhotoClickListener(OnHeadPhotoClickListener listener){
		if(mAdapter != null){
			mAdapter.setOnHeadPhotoClickListener(listener);
		}
	}
	
	public void setOrderContinueListener(View.OnClickListener l) {
		if(mAdapter != null){
			mAdapter.setOrderContinueListener(l);
		}
	}
	
	public void setOrderConfirmListener(View.OnClickListener l) {
		if(mAdapter != null){
			mAdapter.setOrderConfirmListener(l);
		}
	}
	
	public void setOrderItemListener(View.OnClickListener l) {
		if(mAdapter != null){
			mAdapter.setOrderItemListener(l);
		}
	}
	
//	public void setOnContentLongClickListener(View.OnLongClickListener l) {
//		if(mAdapter != null) {
//			mAdapter.setOnContentLongClickListener(l);
//		}
//	}
	
	// open Interface
	// init
	public IMChatView(Activity activity, String customUserID) {
		super(activity);
		setCustomUserID(customUserID);
	}

	// init
	public void setCustomUserID(String customUserID) {
		if (!IMParamJudge.isCustomUserIDLegal(customUserID)) {
			DTLog.log(customUserID);
			DTLog.logError();
			throw new RuntimeException(IMParamJudge.getLastError());
		}

		mCustomUserID = customUserID;
		mUserChatMsgHistory = IMUserMsgHistoriesMgr.getInstance()
				.getUserChatMsgHistory(customUserID);

		if (mUserChatMsgHistory == null) {
			DTLog.logError();
			return;
		}

		clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
		audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
		
		if (mAdapter == null) {
			mAdapter = new IMChatViewAdapter(this,mActivityReference.get(),
					mGifEmotionUtils, mIDs, mUserNameVisible, mUserMainPhotoVisible,
					mUserMainPhotoCornerRadius, mOnChatViewTouchListener,mCustomUserID);
			mListView.setAdapter(mAdapter);
			mListView.setSelection(mAdapter.getCount() - 1);
			mAdapter.setOnImageClickListener(this);
			mAdapter.setOnContentLongClickListener(this);
		} else {
			((IMChatViewAdapter) mAdapter).setCustomUserID(mCustomUserID);
		}

		if (mTitleBarVisible && mTitleTextView != null && mCustomUserID != null) {
			String title = mCustomUserID;

			if (title.length() > 19) {
				title = title.substring(0, 19);
			}

			mTitleTextView.setText(title);
		}

		DTNotificationCenter.getInstance().addObserver(
				mUserChatMsgHistory.getNewMsgNotificationKey(), new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						DTLog.sign("getNewMsgNotificationKey");

						if (mAdapter == null) {
							DTLog.logError();
							return;
						}

						mAdapter.notifyDataSetChanged();
						mListView.setSelection(mAdapter.getCount() - 1);
					}
				});

		IMMyself.setOnReceiveBitmapListener(new OnReceiveBitmapListener() {
			@Override
			public void onReceiveBitmapProgress(double progress, String messageID,
					String fromCustomUserID, long serverActionTime) {
				if (mCustomUserID == null || !mCustomUserID.equals(fromCustomUserID)) {
					return;
				}
				if (mAdapter != null) {
					DTNotificationCenter.getInstance().postNotification("IMChatImageMessageID:" + DTTool.getUnsecretIntValue(messageID), progress);
					
					if(progress == 1) {
						mAdapter.notifyDataSetChanged();
						mListView.setSelection(mAdapter.getCount() - 1);
					}
					
//					Integer position = mAdapter.mPhotoMsgIDPositionMap.get(DTTool
//							.getUnsecretIntValue(messageID));
//
//					if (position != null) {
//						final IMUserMsg userMsg = mUserChatMsgHistory
//								.getUserMsg(mUserChatMsgHistory.getCount() - 1
//										- position);
//						View view = getViewByPosition(position, mListView);
//
//						if (userMsg.mUserMsgType != null
//								&& userMsg.mUserMsgType.equals(UserMsgType.Photo)) {
//							TextView rightImageProgressTextView = (TextView) view
//									.findViewById(CPResourceUtil.getId(
//											mActivityReference.get(),
//											"content_right_image_progress"));
//							View rightImageMaskView = view.findViewById(CPResourceUtil
//									.getId(mActivityReference.get(),
//											"content_right_image_mask"));
//
//							rightImageMaskView.setVisibility(View.VISIBLE);
//							rightImageProgressTextView.setVisibility(View.VISIBLE);
//							rightImageProgressTextView.setText((int) progress + "%");
//						}
//					}
				}
			}

			@Override
			public void onReceiveBitmapMessage(String messageID,
					String fromCustomUserID, String nickName, long serverActionTime) {
			}

			@Override
			public void onReceiveBitmap(Bitmap bitmap, String messageID,
					String fromCustomUserID, long serverActionTime) {
				if (mAdapter != null) {
					DTNotificationCenter.getInstance().postNotification("IMChatImageMessageID:" + messageID, 100);
//					Integer position = mAdapter.mPhotoMsgIDPositionMap.get(DTTool
//							.getUnsecretIntValue(messageID));
//
//					if (position != null) {
//						final IMUserMsg userMsg = mUserChatMsgHistory
//								.getUserMsg(mUserChatMsgHistory.getCount() - 1
//										- position);
//						View view = getViewByPosition(position, mListView);
//
//						if (userMsg.mUserMsgType != null
//								&& userMsg.mUserMsgType.equals(UserMsgType.Photo)) {
//							TextView leftImageProgressTextView = (TextView) view
//									.findViewById(mIDs.get(8));
//							View leftImageMaskView = view.findViewById(mIDs.get(9));
//
//							leftImageMaskView.setVisibility(View.GONE);
//							leftImageProgressTextView.setVisibility(View.GONE);
//
//							ImageView leftImage = (ImageView) view.findViewById(mIDs
//									.get(10));
//							final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) leftImage
//									.getLayoutParams();
//
//							params.height = bitmap.getHeight();
//							params.width = bitmap.getWidth();
//
//							leftImage.setLayoutParams(params);
//							leftImage.setImageBitmap(bitmap);
//						}
//					}
				}
			}
		});
	}

	@Override
	protected boolean onSendText(String text) {
		if (mCustomUserID.equals("#CustomerService")) {
			if(true){
				//old code : if (!IMPrivateMyself.getInstance().getAppInfoInitedInFact()) {
				//motify by ljq 因为注释掉了请求客服的有关逻辑 所以这里先返回true 2015/07/03
				
				Toast.makeText(mActivityReference.get(), "正在初始化", Toast.LENGTH_SHORT)
						.show();
				DTNotificationCenter.getInstance().postNotification("heartbeat");
				return false;
			}

			if (IMAppSettings.getInstance().mCustomerServiceUID == 0) {
				Toast.makeText(mActivityReference.get(), "当前app未开通客服功能",
						Toast.LENGTH_SHORT).show();
				return false;
			}
		}

		if (IMMyself2.getLastSendTextTimeMillis() != 0
				&& System.currentTimeMillis() - IMMyself2.getLastSendTextTimeMillis() < 1000) {
			return false;
		}
		
		if(text.equals("1") || text.equals("2") || text.equals("3")) {
			long actionTime = System.currentTimeMillis() / 1000;
			
			// 创建IMUserMsg
			final IMUserMsg userMsg = IMUserMsgsMgr.getInstance().getUnsentUserMsg(
					mCustomUserID, actionTime);

			if (userMsg == null) {
				DTLog.logError();
				return false;
			}

			userMsg.mToCustomUserID = mCustomUserID;
			userMsg.mContent = text;
			userMsg.mUserMsgType = UserMsgType.Order;
			userMsg.saveFile();

			// 维护聊天记录
			final IMUserChatMsgHistory history = IMUserMsgHistoriesMgr.getInstance()
					.getUserChatMsgHistory(mCustomUserID);

			if (history == null) {
				DTLog.logError();
				return false;
			}

			history.insertUnsentUserMsg(userMsg.mClientSendTime);
			history.saveFile();
			DTNotificationCenter.getInstance().postNotification(
					history.getNewMsgNotificationKey());
			return true;
		}

		IMMyself.sendText(text, mCustomUserID, 10, new OnActionListener() {
			@Override
			public void onSuccess() {
			}

			@Override
			public void onFailure(String error) {
				if (error != null && error.equals("102")) {
					Toast.makeText(mActivityReference.get(), "对方还没有安装最近版本，暂不能收到消息",
							Toast.LENGTH_LONG).show();
				} else {
					DTLog.logError();
					DTLog.log(error);
					DTLog.sign(error);
				}
			}
		});

		return true;
	}

	@Override
	protected void onSendBitmap(Bitmap bitmap) {
		IMMyself.sendBitmap(bitmap, mCustomUserID, 30, new OnActionProgressListener() {
			@Override
			public void onSuccess() {
			}

			@Override
			public void onProgress(double progress) {
			}

			@Override
			public void onFailure(String error) {
				if (error != null && error.equals("102")) {
					Toast.makeText(mActivityReference.get(), "对方还没有安装最近版本，暂不能收到消息",
							Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	private static class IMChatViewHandler extends Handler {
		private final WeakReference<IMChatView> mChatViewReference;
		private final WeakReference<Activity> mActivityReference;

		public IMChatViewHandler(IMChatView chatView, Activity activity, Looper looper) {
			super(looper);
			mChatViewReference = new WeakReference<IMChatView>(chatView);
			mActivityReference = new WeakReference<Activity>(activity);
		}

		@Override
		public void handleMessage(Message msg) {
			if (mActivityReference.get() == null) {
				return;
			}

			if (mChatViewReference.get() == null) {
				return;
			}

			mChatViewReference.get().processHandlerMessage(msg);
		}
	}

	private Handler mHandler;

	@Override
	public Handler getHandler() {
		if (mHandler == null) {
			synchronized (this) {
				if (mHandler == null) {
					mHandler = new IMChatViewHandler(IMChatView.this,
							mActivityReference.get(), mActivityReference.get()
									.getMainLooper());
				}
			}
		}

		return mHandler;
	}

	public void processHandlerMessage(Message msg) {
		switch (msg.what) {
		case MSG_SEND_CLASSICAL_EMOTION:
			showNormalMsg((String) msg.obj);
			break;
		default:
			break;
		}
	}

	@Override
	protected void onUpdateScreenGif(int startPos, int endPos) {
		for (int i = startPos; i <= endPos; i++) {
			IMUserMsg userMsg = mUserChatMsgHistory.getUserMsg(mUserChatMsgHistory
					.getCount() - 1 - i);

			if (userMsg == null) {
				DTLog.logError();
				return;
			}

			if (!(userMsg instanceof IMUserMsg)) {
				DTLog.logError();
				return;
			}

			if (userMsg.mUserMsgType != UserMsgType.Normal
					|| userMsg.mContent.indexOf("}") == -1
					|| userMsg.mContent.indexOf("{") == -1) {
				continue;
			}

			View itemView = getViewByPosition(i, mListView);

			if (itemView != null) {
				TextView contentTextView = (TextView) itemView.findViewById(CPResourceUtil.getId(mActivityReference.get(), "tv_chatcontent"));

				mGifEmotionUtils.setSpannableText(contentTextView, userMsg.mContent,
						mHandler);
			}
		}
	}

	@Override
	protected boolean onStartRecording() {
		return IMAudioSender.getInstance().startRecordingToUser(mCustomUserID);
	}

	@Override
	public void setTitleBarVisible(boolean visible) {
		super.setTitleBarVisible(visible);

		if (mTitleBarVisible && mTitleTextView != null && mCustomUserID != null) {
			String title = mCustomUserID;

			if (title.length() > 19) {
				title = title.substring(0, 19);
			}

			mTitleTextView.setText(title);
		}
	}

	@Override
	public boolean onLongClick(View v) {
		int position = (Integer) v.getTag();
		
		final int actualPosition = mUserChatMsgHistory.getCount() - 1 - position;
		
		if(actualPosition >= 0 && actualPosition < mUserChatMsgHistory.getCount()) {
			final IMUserMsg userMsg = mUserChatMsgHistory.getUserMsg(actualPosition);
			
			if(userMsg != null) {
				switch (userMsg.mUserMsgType) {
				case Audio:
					final OperationChatDialog dialog = new OperationChatDialog(getContext());
					dialog.show();
					
					dialog.setDeleteClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View view) {
							userMsg.removeFile();
							
							mUserChatMsgHistory.removeUserMsg(actualPosition);
							mUserChatMsgHistory.saveFile();
							
							mAdapter.notifyDataSetChanged();
							
							if (dialog != null && dialog.isShowing()) {
								dialog.dismiss();
							}
						}
					});
					dialog.setPlayModeClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View view) {
							if (dialog != null && dialog.isShowing()) {
								dialog.dismiss();
							}

							TextView temp = (TextView) view;
							if(temp.getText().equals(getContext().getString(CPResourceUtil.getStringId(getContext(), "use_receiver_mode")))) {
								temp.setText(getContext().getString(CPResourceUtil.getStringId(getContext(), "use_speaker_mode")));
								
								if(audioManager != null) {
									audioManager.setMode(AudioManager.MODE_NORMAL);
								}
							} else {
								temp.setText(getContext().getString(CPResourceUtil.getStringId(getContext(), "use_receiver_mode")));
								
								if(audioManager != null) {
									audioManager.setMode(AudioManager.MODE_IN_CALL);
								}
							}
						}
					});
					
					break;
				case Normal:
				case Photo:
					final ShowTextMsgDialog showTextMsgDialog = new ShowTextMsgDialog(getContext(), (userMsg.mUserMsgType == UserMsgType.Photo));
					showTextMsgDialog.show();
					
					showTextMsgDialog.setCopyClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							if (showTextMsgDialog != null && showTextMsgDialog.isShowing()) {
								showTextMsgDialog.dismiss();
							}
							
							TextView temp = (TextView) v;
							if (temp.getText().toString().equals("保存到手机")) {
								IMImagePhoto imagePhoto = IMImagesMgr.getInstance().getPhoto(userMsg.getFileID());
								
								if(imagePhoto.isLocalFileExist()) {
									String localPath = imagePhoto.getLocalFullPath();
									
									SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");//设置日期格式
									String fileName = df.format(new Date());
									String newPath =  FileUtils.getStorePath() + "youwo/images/" + fileName + ".jpg";
									if(FileUtils.copyFile(localPath, newPath)) {
										Toast.makeText(getContext(), "图片保存路径：" + newPath, Toast.LENGTH_SHORT).show();
									} else {
										Toast.makeText(getContext(), "图片保存失败", Toast.LENGTH_SHORT).show();
									}
								} else {
									Toast.makeText(getContext(), "图片保存失败， 本地缓存不存在", Toast.LENGTH_SHORT).show();
								}
								
							}else{
								clipboard.setText(userMsg.mContent);
								Toast.makeText(getContext(), "复制成功", Toast.LENGTH_SHORT).show();
							}
						}
					});
					
					showTextMsgDialog.setForwardClickListener(new View.OnClickListener() {
											
						@Override
						public void onClick(View v) {
							//功能尚未实现
						}
					});
					
					showTextMsgDialog.setDeleteClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							if(userMsg.mUserMsgType == UserMsgType.Photo) {
								String fileID = userMsg.getFileID();
								
								IMImagesMgr.getInstance().getPhoto(fileID).removeFile();
							}
							
							userMsg.removeFile();
							
							mUserChatMsgHistory.removeUserMsg(actualPosition);
							mUserChatMsgHistory.saveFile();
							
							mAdapter.notifyDataSetChanged();
							
							if (showTextMsgDialog != null && showTextMsgDialog.isShowing()) {
								showTextMsgDialog.dismiss();
							}
						}
					});
					
					showTextMsgDialog.setAtHeClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							mContentEditText.setText("@" + userMsg.mFromCustomUserID);
							
							if (showTextMsgDialog != null && showTextMsgDialog.isShowing()) {
								showTextMsgDialog.dismiss();
							}
						}
					});
					
					break;
				default:
					break;
				}
			}
		}
		return false;
	}
}