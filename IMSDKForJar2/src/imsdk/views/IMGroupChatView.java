package imsdk.views;

import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnActionProgressListener;
import imsdk.data.group.IMGroupInfo;
import imsdk.data.group.IMMyselfGroup;
import imsdk.data.group.IMSDKGroup;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.dtlib.model.c.tool.DTTool;
import am.imsdk.demo.R;
import am.imsdk.demo.util.CPResourceUtil;
import am.imsdk.demo.util.FileUtils;
import am.imsdk.model.IMAudioSender;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.a2.IMMyselfGroup2;
import am.imsdk.model.amimteam.IMTeamMsg;
import am.imsdk.model.amimteam.IMTeamMsg.TeamMsgType;
import am.imsdk.model.imteam.IMTeamChatMsgHistory;
import am.imsdk.model.imteam.IMTeamMsgHistoriesMgr;
import am.imsdk.serverfile.image.IMImagePhoto;
import am.imsdk.serverfile.image.IMImagesMgr;
import am.imsdk.ui.adapter.IMGroupChatViewAdapter;
import am.imsdk.ui.views.IMBaseChatView;
import am.imsdk.ui.views.dialog.OperationChatDialog;
import am.imsdk.ui.views.dialog.ShowTextMsgDialog;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.text.ClipboardManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class IMGroupChatView extends IMBaseChatView {
	// init data
	private String mGroupID = "";
	private long mTeamID;
	private IMTeamChatMsgHistory mTeamChatMsgHistory;
	
	private ClipboardManager clipboard;
	private AudioManager audioManager;

	//listener
	public void setOnHeadPhotoClickListener(IMChatView.OnHeadPhotoClickListener listener){
		if(mAdapter != null){
			mAdapter.setOnHeadPhotoClickListener(listener);
		}
	}
	
	// open Interface
	// init
	public IMGroupChatView(Activity activity, String groupID) {
		super(activity);
		setGroupID(groupID);
	}

	// init
	public void setGroupID(String groupID) {
		if (!IMParamJudge.isGroupIDLegal(groupID)) {
			DTLog.logError();
			throw new RuntimeException(IMParamJudge.getLastError());
		}

		mGroupID = groupID;
		mTeamID = DTTool.getTeamIDFromGroupID(mGroupID);
		mTeamChatMsgHistory = IMTeamMsgHistoriesMgr.getInstance()
				.getTeamChatMsgHistory(mTeamID);

		if (mTeamChatMsgHistory == null) {
			DTLog.logError();
			return;
		}
		
		clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
		audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);

		if (mTitleBarVisible && mTitleTextView != null && mGroupID != null) {
			IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(mGroupID);

			mTitleTextView.setText(groupInfo.getGroupName());
		}

		DTNotificationCenter.getInstance().addObserver(
				mTeamChatMsgHistory.getNewMsgNotificationKey(), new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						DTLog.sign("getNewMsgNotificationKey");

						if (mAdapter == null) {
							DTLog.logError();
							return;
						}

						mAdapter.notifyDataSetChanged();
						mListView.setSelection(mAdapter.getCount() - 1);
					}
				});

		if (mAdapter == null) {
			mAdapter = new IMGroupChatViewAdapter(mActivityReference.get(),
					mGifEmotionUtils, mIDs, mUserNameVisible, mUserMainPhotoVisible,
					mUserMainPhotoCornerRadius, mOnChatViewTouchListener, mGroupID);
			mListView.setAdapter(mAdapter);
			mListView.setSelection(mAdapter.getCount() - 1);
			mAdapter.setOnImageClickListener(this);
			mAdapter.setOnContentLongClickListener(this);
		} else {
			((IMGroupChatViewAdapter) mAdapter).setGroupID(mGroupID);
		}

		if (!IMParamJudge.isTeamIDLegal(DTTool.getTeamIDFromGroupID(groupID))) {
			throw new RuntimeException(IMParamJudge.getLastError());
		}

		mGroupID = groupID;
		mTeamID = DTTool.getTeamIDFromGroupID(groupID);
		mTeamChatMsgHistory = IMTeamMsgHistoriesMgr.getInstance()
				.getTeamChatMsgHistory(mTeamID);

		if (mTeamChatMsgHistory == null) {
			DTLog.logError();
			return;
		}

		DTNotificationCenter.getInstance().addObserver(
				mTeamChatMsgHistory.getNewMsgNotificationKey(), new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						DTLog.sign("getNewMsgNotificationKey");

						if (mAdapter == null) {
							return;
						}

						mAdapter.notifyDataSetChanged();
						mListView.setSelection(mAdapter.getCount() - 1);
					}
				});

		// if (mAdapter == null) {
		// mAdapter = new IMGroupChatViewAdapter(mActivityReference.get(),
		// mGifEmotionUtils, mIDs, mUserNameVisible, mUserMainPhotoVisible,
		// mUserMainPhotoCornerRadius, mOnChatViewTouchListener, mGroupID);
		// mListView.setAdapter(mAdapter);
		// mListView.setSelection(mAdapter.getCount() - 1);
		// // mAdapter.setOnResendClickListener(this);
		// mAdapter.setOnHeadPhotoClickListener(this);
		// mAdapter.setOnImageClickListener(this);
		// } else {
		// ()mAdapter.setCustomUserID(mGroupID);
		// mListView.setSelection(mAdapter.getCount() - 1);
		// }

		DTNotificationCenter.getInstance().addObserver("IMReceiveBitmapFromGroup",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMTeamMsg)) {
							DTLog.logError();
							return;
						}

						IMTeamMsg teamMsg = (IMTeamMsg) data;

						if (teamMsg.mTeamMsgType != TeamMsgType.Photo) {
							DTLog.logError();
							return;
						}

						if (!IMParamJudge
								.isCustomUserIDLegal(teamMsg.mFromCustomUserID)) {
							DTLog.logError();
							return;
						}

						if (teamMsg.mTeamID == 0) {
							DTLog.logError();
							return;
						}

						if (teamMsg.mTeamID != mTeamID) {
							return;
						}

						String groupID = DTTool.getSecretString(teamMsg.mTeamID);

						if (!IMParamJudge.isGroupIDLegal(groupID)) {
							DTLog.logError();
							return;
						}

						if (teamMsg.mServerSendTime == 0) {
							DTLog.logError();
							return;
						}

						if (teamMsg.mTeamMsgType != TeamMsgType.Photo) {
							DTLog.logError();
							return;
						}

						if (!IMParamJudge.isFileIDLegal(teamMsg.getFileID())) {
							DTLog.logError();
							return;
						}

						IMImagePhoto photo = IMImagesMgr.getInstance().getPhoto(
								teamMsg.getFileID());

						int position = mTeamChatMsgHistory.getIndex(teamMsg);
						if (position == -1) {
							DTLog.logError();
						} else {
							if (mAdapter != null) {

								View view = getViewByPosition(mAdapter.getCount()
										- position - 1, mListView);

								if (teamMsg.mTeamMsgType != null
										&& teamMsg.mTeamMsgType
												.equals(TeamMsgType.Photo)) {
									TextView leftImageProgressTextView = (TextView) view
											.findViewById(mIDs.get(8));
									View leftImageMaskView = view.findViewById(mIDs
											.get(9));

									leftImageMaskView.setVisibility(View.GONE);
									leftImageProgressTextView.setVisibility(View.GONE);

									ImageView leftImage = (ImageView) view
											.findViewById(mIDs.get(10));
									final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) leftImage
											.getLayoutParams();

									params.height = photo.getBitmap().getHeight();
									params.width = photo.getBitmap().getWidth();
									leftImage.setLayoutParams(params);
									leftImage.setImageBitmap(photo.getBitmap());
								}
							}
						}
					}
				});
	}

	public IMGroupChatView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public IMGroupChatView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	protected boolean onSendText(String text) {
		if (IMMyselfGroup2.getLastSendTextTimeMillis() != 0
				&& System.currentTimeMillis()
						- IMMyselfGroup2.getLastSendTextTimeMillis() < 1000) {
			return false;
		}

		IMMyselfGroup.sendText(text, mGroupID, new OnActionListener() {
			@Override
			public void onSuccess() {
			}

			@Override
			public void onFailure(String error) {
				if (error != null && error.equals("102")) {
					Toast.makeText(mActivityReference.get(), "对方还没有安装最近版本，赞不能收到消息",
							Toast.LENGTH_LONG).show();
				}
			}
		});

		return true;
	}

	@Override
	protected void onSendBitmap(Bitmap bitmap) {
		IMMyselfGroup.sendBitmap(bitmap, mGroupID, 30, new OnActionProgressListener() {
			@Override
			public void onSuccess() {
			}

			@Override
			public void onProgress(double progress) {
			}

			@Override
			public void onFailure(String error) {
				if (error != null && error.equals("102")) {
					Toast.makeText(mActivityReference.get(), "对方还没有安装最近版本，赞不能收到消息",
							Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	protected void onUpdateScreenGif(int startPos, int endPos) {
		for (int i = startPos; i <= endPos; i++) {
			IMTeamMsg userMsg = mTeamChatMsgHistory.getTeamMsg(mTeamChatMsgHistory
					.getCount() - 1 - i);

			if (userMsg == null) {
				DTLog.logError();
				return;
			}

			if (!(userMsg instanceof IMTeamMsg)) {
				DTLog.logError();
				return;
			}

			if (userMsg.mTeamMsgType != TeamMsgType.Normal
					|| userMsg.mContent.indexOf("}") == -1
					|| userMsg.mContent.indexOf("{") == -1) {
				continue;
			}

			View itemView = getViewByPosition(i, mListView);

			if (itemView != null) {
				int id;

				if (userMsg.mIsRecv) {
					id = mIDs.get(6);
				} else {
					id = mIDs.get(14);
				}

				TextView contentTextView = (TextView) itemView.findViewById(id);

				mGifEmotionUtils.setSpannableText(contentTextView, userMsg.mContent,
						mHandler);
			}
		}
	}

	@Override
	protected boolean onStartRecording() {
		return IMAudioSender.getInstance().startRecordingToGroup(mGroupID);
	}

	@Override
	public void setTitleBarVisible(boolean visible) {
		super.setTitleBarVisible(visible);

		if (mTitleBarVisible && mTitleTextView != null && mGroupID != null) {
			IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(mGroupID);

			mTitleTextView.setText(groupInfo.getGroupName());
		}
	}

	@Override
	public boolean onLongClick(View v) {
		int position = (Integer) v.getTag();
		
		final int actualPosition = mTeamChatMsgHistory.getCount() - 1 - position;
		
		if(actualPosition >= 0 && actualPosition < mTeamChatMsgHistory.getCount()) {
			final IMTeamMsg teamMsg = mTeamChatMsgHistory.getTeamMsg(actualPosition);
			
			if(teamMsg != null) {
				switch (teamMsg.mTeamMsgType) {
				case Audio:
					final OperationChatDialog dialog = new OperationChatDialog(getContext());
					dialog.show();
					
					dialog.setDeleteClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View view) {
							teamMsg.removeFile();
							
							mTeamChatMsgHistory.removeTeamMsg(actualPosition);
							mTeamChatMsgHistory.saveFile();
							
							mAdapter.notifyDataSetChanged();
							
							if (dialog != null && dialog.isShowing()) {
								dialog.dismiss();
							}
						}
					});
					dialog.setPlayModeClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View view) {
							if (dialog != null && dialog.isShowing()) {
								dialog.dismiss();
							}
							
							TextView temp = (TextView) view;
							if(temp.getText().equals(getContext().getString(CPResourceUtil.getStringId(getContext(), "use_receiver_mode")))) {
								temp.setText(getContext().getString(CPResourceUtil.getStringId(getContext(), "use_speaker_mode")));
								
								if(audioManager != null) {
									audioManager.setMode(AudioManager.MODE_NORMAL);
								}
							} else {
								temp.setText(getContext().getString(CPResourceUtil.getStringId(getContext(), "use_receiver_mode")));
								
								if(audioManager != null) {
									audioManager.setMode(AudioManager.MODE_IN_CALL);
								}
							}
						}
					});
					
					break;
				case Normal:
				case Photo:
					final ShowTextMsgDialog showTextMsgDialog = new ShowTextMsgDialog(getContext(), (teamMsg.mTeamMsgType == TeamMsgType.Photo));
					showTextMsgDialog.show();
					
					showTextMsgDialog.setCopyClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							if (showTextMsgDialog != null && showTextMsgDialog.isShowing()) {
								showTextMsgDialog.dismiss();
							}
							
							TextView temp = (TextView) v;
							if (temp.getText().toString().equals("保存到手机")) {
								IMImagePhoto imagePhoto = IMImagesMgr.getInstance().getPhoto(teamMsg.getFileID());
								
								if(imagePhoto.isLocalFileExist()) {
									String localPath = imagePhoto.getLocalFullPath();
									
									SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");//设置日期格式
									String fileName = df.format(new Date());
									String newPath =  FileUtils.getStorePath() + "youwo/images/" + fileName + ".jpg";
									if(FileUtils.copyFile(localPath, newPath)) {
										Toast.makeText(getContext(), "图片保存路径：" + newPath, Toast.LENGTH_SHORT).show();
									} else {
										Toast.makeText(getContext(), "图片保存失败", Toast.LENGTH_SHORT).show();
									}
								} else {
									Toast.makeText(getContext(), "图片保存失败， 本地缓存不存在", Toast.LENGTH_SHORT).show();
								}
								
							}else{
								clipboard.setText(teamMsg.mContent);
								Toast.makeText(getContext(), "复制成功", Toast.LENGTH_SHORT).show();
							}
						}
					});
					
					showTextMsgDialog.setForwardClickListener(new View.OnClickListener() {
											
						@Override
						public void onClick(View v) {
							//功能尚未实现
						}
					});
					
					showTextMsgDialog.setDeleteClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							if(teamMsg.mTeamMsgType == TeamMsgType.Photo) {
								String fileID = teamMsg.getFileID();
								
								IMImagesMgr.getInstance().getPhoto(fileID).removeFile();
							}
							
							teamMsg.removeFile();
							
							mTeamChatMsgHistory.removeTeamMsg(actualPosition);
							mTeamChatMsgHistory.saveFile();
							
							mAdapter.notifyDataSetChanged();
							
							if (showTextMsgDialog != null && showTextMsgDialog.isShowing()) {
								showTextMsgDialog.dismiss();
							}
						}
					});
					
					showTextMsgDialog.setAtHeClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							mContentEditText.setText("@" + teamMsg.mFromCustomUserID);
							
							if (showTextMsgDialog != null && showTextMsgDialog.isShowing()) {
								showTextMsgDialog.dismiss();
							}
						}
					});
					
					break;
				default:
					break;
				}
			}
		}
		return false;
	}

}