package imsdk.data.relations;

import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnInitializedListener;
import imsdk.data.IMSDK.OnDataChangedListener;

import java.util.ArrayList;

import am.imsdk.model.a2.IMMyselfRelations2;

public final class IMMyselfRelations {
	public static interface OnRelationsEventListener {
		public void onInitialized();

		public void onReceiveFriendRequest(String text, String fromCustomUserID,
				long serverActionTime);

		public void onReceiveAgreeToFriendRequest(String fromCustomUserID,
				long serverActionTime);

		public void onReceiveRejectToFriendRequest(String reason,
				String fromCustomUserID, long serverActionTime);
		
		public void onDeleteFriends(String fromCustomUserID, long serverActionTime);

		public void onBuildFriendshipWithUser(String customUserID, long serverActionTime);
	}

	public static boolean isInitialized() {
		return IMMyselfRelations2.isInitialized();
	}

	public static void setOnInitializedListener(OnInitializedListener l) {
		IMMyselfRelations2.setOnInitializedListener(l);
	}

	public static void setOnFriendsListDataChangedListener(OnDataChangedListener l) {
		IMMyselfRelations2.setOnFriendsListDataChangedListener(l);
	}

	public static void setOnBlacklistDataChangedListener(OnDataChangedListener l) {
		IMMyselfRelations2.setOnBlacklistDataChangedListener(l);
	}

	public static void setOnDataChangedListener(OnDataChangedListener l) {
		IMMyselfRelations2.setOnDataChangedListener(l);
	}

	public static void setOnRelationsEventListener(OnRelationsEventListener l) {
		IMMyselfRelations2.setOnRelationsEventListener(l);
	}

	public static ArrayList<String> getFriendsList() {
		return IMMyselfRelations2.getFriendsList();
	}

	public static ArrayList<String> getBlacklist() {
		return IMMyselfRelations2.getBlacklist();
	}

	public static boolean isMyFriend(String customUserID) {
		return IMMyselfRelations2.isMyFriend(customUserID);
	}

	public static boolean isMyBlacklistUser(String customUserID) {
		return IMMyselfRelations2.isMyBlacklistUser(customUserID);
	}

	public static long sendFriendRequest(String text, final String toCustomUserID,
			final long timeoutInterval, final OnActionListener l) {
		return IMMyselfRelations2.sendFriendRequest(text, toCustomUserID,
				timeoutInterval, l);
	}

	public static long agreeToFriendRequest(final String fromCustomUserID,
			final long timeoutInterval, final OnActionListener l) {
		return IMMyselfRelations2.agreeToFriendRequest(fromCustomUserID,
				timeoutInterval, l);
	}

	public static long rejectToFriendRequest(String reason,
			final String fromCustomUserID, final long timeoutInterval,
			final OnActionListener l) {
		return IMMyselfRelations2.rejectToFriendRequest(reason, fromCustomUserID,
				timeoutInterval, l);
	}

	public static long removeUserFromFriendsList(final String customUserID,
			long timeoutInterval, final OnActionListener l) {
		return IMMyselfRelations2.removeUserFromFriendsList(customUserID,
				timeoutInterval, l);

	}

	public static long moveUserToBlacklist(final String customUserID,
			long timeoutInterval, final OnActionListener l) {
		return IMMyselfRelations2.moveUserToBlacklist(customUserID, timeoutInterval, l);
	}

	public static long removeUserFromBlacklist(final String customUserID,
			long timeoutInterval, final OnActionListener l) {
		return IMMyselfRelations2.removeUserFromBlacklist(customUserID,
				timeoutInterval, l);

	}

	public static String getLastError() {

		return IMMyselfRelations2.getLastError();
	}

}
