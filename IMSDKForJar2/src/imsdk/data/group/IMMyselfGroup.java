package imsdk.data.group;

import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnActionProgressListener;
import imsdk.data.IMMyself.OnActionResultListener;
import imsdk.data.IMMyself.OnInitializedListener;

import java.util.ArrayList;

import am.imsdk.model.a2.IMMyselfGroup2;
import android.graphics.Bitmap;

public final class IMMyselfGroup {
	public static interface OnGroupEventsListener {
		// init
		public void onInitialized();

		// event notifications
		public void onGroupDeletedByUser(String groupID, String customUserID,
				long actionServerTime);

		public void onGroupNameUpdated(String newGroupName, String groupID,
				long actionServerTime);

		public void onCustomGroupInfoUpdated(String newGroupInfo, String groupID,
				long actionSeverTime);

		public void onGroupMemberUpdated(ArrayList<String> memberList, String groupID,
				long actionServerTime);

		public void onAddedToGroup(String groupID, long actionServerTime);

		public void onRemovedFromGroup(String groupID, String customUserID,
				long actionServerTime);
	}

	public static interface OnGroupMessageListener {
		// im text
		public void onReceiveText(String text, String groupID, String fromCustomUserID,
				long actionServerTime);

		public void onReceiveCustomMessage(String customMessage, String groupID,
				String fromCustomUserID, long actionServerTime);

		// im bitmap
		public void onReceiveBitmapMessage(String messageID, String groupID,
				String fromCustomUserID, long serverActionTime);

		public void onReceiveBitmap(Bitmap bitmap, String groupID,
				String fromCustomUserID, long serverActionTime);

		public void onReceiveBitmapProgress(double progress, String groupID,
				String fromCustomUserID, long serverActionTime);
	}

	public static interface OnGroupActionsListener {
		public void onActionSuccess(String actionType, long clientActionTime);

		public void onActionFailure(String actionType, String error,
				long clientActiontime);
	}

	public static boolean isInitialized() {
		return IMMyselfGroup2.isInitialized();
	}

	public static void setOnInitializedListener(final OnInitializedListener l) {
		IMMyselfGroup2.setOnInitializedListener(l);
	}

	public static void setOnGroupEventsListener(final OnGroupEventsListener l) {
		IMMyselfGroup2.setOnGroupEventsListener(l);
	}

	public static void setOnGroupActionsListener(final OnGroupActionsListener l) {
		IMMyselfGroup2.setOnGroupActionsListener(l);
	}

	public static void setOnGroupMessageListener(final OnGroupMessageListener l) {
		IMMyselfGroup2.setOnGroupMessageListener(l);
	}

	public static ArrayList<String> getMyGroupsList() {
		return IMMyselfGroup2.getMyGroupsList();
	}

	public static ArrayList<String> getMyOwnGroupIDList() {
		return IMMyselfGroup2.getMyOwnGroupIDList();
	}

	public static boolean isGroupInMyList(String groupID) {
		return IMMyselfGroup2.isGroupInMyList(groupID);
	}

	public static boolean isMyOwnGroup(String groupID) {
		return IMMyselfGroup2.isMyOwnGroup(groupID);
	}

	public static long createGroup(final String groupName,
			final OnActionResultListener l) {
		return IMMyselfGroup2.createGroup(groupName, l);
	}

	public static long deleteGroup(final String groupID, final OnActionListener l) {
		return IMMyselfGroup2.deleteGroup(groupID, l);
	}

	public static long addMemeber(String customUserID, String toGroupID,
			final OnActionListener l) {
		return IMMyselfGroup2.addMemeber(customUserID, toGroupID, l);

	}

	public static long removeMember(String customUserID, String fromGroupID,
			final OnActionListener l) {
		return IMMyselfGroup2.removeMember(customUserID, fromGroupID, l);
	}

	public static long sendText(String text, String toGroupID, final OnActionListener l) {
		return IMMyselfGroup2.sendText(text, toGroupID, l);
	}

	public static boolean startRecording(String toGroupID) {
		return IMMyselfGroup2.startRecording(toGroupID);
	}

	public static long stopRecording(boolean needSend, final long timeoutInterval,
			final OnActionListener l) {
		return IMMyselfGroup2.stopRecording(needSend, timeoutInterval, l);
	}

	public static long sendBitmap(final Bitmap bitmap, final String toGroupID,
			final long timeoutInterval, final OnActionProgressListener l) {
		return IMMyselfGroup2.sendBitmap(bitmap, toGroupID, timeoutInterval, l);
	}

	public static long quitGroup(String fromGroupID, final OnActionListener l) {
		return IMMyselfGroup2.quitGroup(fromGroupID, l);
	}

	public static String getLastError() {
		return IMMyselfGroup2.getLastError();
	}
}
