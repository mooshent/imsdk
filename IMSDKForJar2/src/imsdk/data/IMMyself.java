package imsdk.data;

import am.imsdk.model.a2.IMMyself2;
import android.content.Context;
import android.graphics.Bitmap;

public class IMMyself {
	public static interface OnReceiveTextListener {
		public void onReceiveText(String text, String fromCustomUserID, String nickName,
				long serverActionTime);

		public void onReceiveSystemText(String text, long serverActionTime);
		
		public void onReceiveAudio(String messageID, String fromCustomUserID, String nickName,
				long serverActionTime);
	}

	public static interface OnReceiveBitmapListener {
		public void onReceiveBitmapMessage(String messageID, String fromCustomUserID, String nickName,
				long serverActionTime);

		public void onReceiveBitmap(Bitmap bitmap, String messageID,
				String fromCustomUserID, long serverActionTime);

		public void onReceiveBitmapProgress(double progress, String messageID,
				String fromCustomUserID, long serverActionTime);
	}

	public static interface OnConnectionChangedListener {
		public void onDisconnected(boolean loginConflict);

		public void onReconnected();
	}

	public static interface OnAutoLoginListener {
		public void onAutoLoginBegan();

		public void onAutoLoginSuccess();

		public void onAutoLoginFailure(boolean loginConflict);
	}

	public static interface OnActionListener {
		public void onSuccess();

		public void onFailure(String error);
	}

	public static interface OnActionResultListener {
		public void onSuccess(Object result);

		public void onFailure(String error);
	}

	public static interface OnActionProgressListener {
		public void onSuccess();

		public void onProgress(double progress);

		public void onFailure(String error);
	}

	public static interface OnLoginStatusChangedListener {
		public void onLoginStatusChanged(LoginStatus oldLoginStatus,
				LoginStatus newLoginStatus);
	}

	public static interface OnInitializedListener {
		public void onInitialized();
	}

	/**
	 * @method
	 * @brief 初始化登录信息，可能触发自动登录
	 * @param applicationContext
	 *            Android应用上下文环境
	 * @param appKey
	 *            应用标识，不能为空，开发者需要填写从www.IMSDK.im官网注册时获取的appkey
	 */
	public static boolean init(Context applicationContext, String appKey,
			OnAutoLoginListener l) {
		return IMMyself2.init(applicationContext, appKey, l);
	}

	// 可能触发退出登录
	public static boolean setCustomUserID(String customUserID) {
		return IMMyself2.setCustomUserID(customUserID);
	}

	// 可能触发退出登录
	public static boolean setPassword(String password) {
		return IMMyself2.setPassword(password);
	}

	public static String getPassword() {
		return IMMyself2.getPassword();
	}

	public static boolean isLogined() {
		return IMMyself2.isLogined();
	}

	public static long register(long timeoutInterval, final OnActionListener l) {
		return IMMyself2.register(timeoutInterval, l);
	}

	public static long login() {
		return IMMyself2.login();
	}

	public static long login(boolean autoRegister, long timeoutInterval,
			final OnActionListener l) {
		return IMMyself2.login(autoRegister, timeoutInterval, l);
	}

	public static void logout() {
		IMMyself2.logout();
	}

	public static long sendText(String text, String toCustomUserID) {
		return IMMyself2.sendText(text, toCustomUserID);
	}

	public static long sendText(String text, String toCustomUserID,
			long timeoutInterval, final OnActionListener l) {
		return IMMyself2.sendText(text, toCustomUserID, timeoutInterval, l);
	}

	public static boolean startRecording(String toCustomUserID) {
		return IMMyself2.startRecording(toCustomUserID);
	}

	public static long stopRecording(boolean needSend, final long timeoutInterval,
			final OnActionListener l) {
		return IMMyself2.stopRecording(needSend, timeoutInterval, l);
	}

	public static long sendBitmap(final Bitmap bitmap, final String toCustomUserID,
			final long timeoutInterval, final OnActionProgressListener l) {
		return IMMyself2.sendBitmap(bitmap, toCustomUserID, timeoutInterval, l);
	}

	public static void setListener(IMMyself.IMMyselfListener listener) {
		IMMyself2.setListener(listener);
	}

	public static String getCustomUserID() {
		return IMMyself2.getCustomUserID();
	}

	public static String getAppKey() {
		return IMMyself2.getAppKey();
	}
	
	public static void setNickName(String nickName) {
		IMMyself2.setNickName(nickName);
	}

	public enum LoginStatus {
		// 未登录
		None(0), Logining(1), Reconnecting(2), AutoLogining(4),

		// 已登录
		Logined(11);

		private final int value;

		private LoginStatus(int value) {
			this.value = value;
		}

		public int getValue() {
			return value;
		}
	}

	public static LoginStatus getLoginStatus() {
		return IMMyself2.getLoginStatus();
	}

	public interface IMMyselfListener {
		// actions
		public void onActionSuccess(String actionType, long clientActionTime);

		public void onActionFailure(String actionType, String error,
				long clientActiontime);

		// login
		public void onDisconnected(boolean loginConflict);

		public void onReconnected();

		// text
		public void onReceiveText(String text, String fromCustomUserID,
				long serverActionTime);
		
		// audio
		public void onReceiveAudio(String messageID, String fromCustomUserID,
				long serverActionTime);

		// system text
		public void onReceiveSystemText(String text, long serverActionTime);

		// bitmap
		public void onReceiveBitmapMessage(String messageID, String fromCustomUserID,
				long serverActionTime);

		public void onReceiveBitmap(Bitmap bitmap, String messageID,
				String fromCustomUserID, long serverActionTime);

		public void onReceiveBitmapProgress(double progress, String messageID,
				String fromCustomUserID, long serverActionTime);
	}

	public static void setOnReceiveTextListener(OnReceiveTextListener l) {
		IMMyself2.setOnReceiveTextListener(l);
	}

	public static void setOnReceiveBitmapListener(OnReceiveBitmapListener l) {
		IMMyself2.setOnReceiveBitmapListener(l);
	}

	public static void setOnConnectionChangedListener(OnConnectionChangedListener l) {
		IMMyself2.setOnConnectionChangedListener(l);
	}

	public static void setOnLoginStatusChangedListener(OnLoginStatusChangedListener l) {
		IMMyself2.setOnLoginStatusChangedListener(l);
	}
}
