package imsdk.data.custommessage;

import imsdk.data.IMMyself.LoginStatus;
import imsdk.data.IMMyself.OnActionListener;

import java.util.Observable;
import java.util.Observer;

import am.dtlib.model.b.log.DTLog;
import am.dtlib.model.c.tool.DTNotificationCenter;
import am.imsdk.model.IMParamJudge;
import am.imsdk.model.IMPrivateMyself;
import am.imsdk.model.a2.IMMyself2;
import am.imsdk.model.amim.IMUserMsg;
import am.imsdk.model.amim.IMUserMsg.UserMsgType;

public final class IMMyselfCustomMessage extends IMMyself2 {
	public interface OnReceiveCustomMessageListener {
		public void onReceiveCustomMessage(String customMessage,
				String fromCustomUserID, long serverActionTime);
	}

	public static long send(String customMessage, String toCustomUserID) {
		return send(customMessage, toCustomUserID, 0, null);
	}

	public static long send(String customMessage, String toCustomUserID,
			long timeoutInterval, final OnActionListener l) {
		final long actionTime = System.currentTimeMillis() / 1000;

		if (!IMParamJudge.isCustomUserIDLegal(toCustomUserID)) {
			commonActionFailure(l, "sendCustomMessage", actionTime);
			return actionTime;
		}

		if (!IMParamJudge.isIMTextLegal(customMessage)) {
			commonActionFailure(l, "sendCustomMessage", actionTime);
			return actionTime;
		}

		if (sLastSendCustomMessageTime != 0
				&& System.currentTimeMillis() - sLastSendCustomMessageTime < 1000) {
			commonActionFailure(l, "sendCustomMessage", actionTime);
			return actionTime;
		}

		return actionTime;
	}

	static {
		DTNotificationCenter.getInstance().addObserver("IMReceiveCustomMessage",
				new Observer() {
					@Override
					public void update(Observable observable, Object data) {
						if (!(data instanceof IMUserMsg)) {
							DTLog.logError();
							return;
						}

						IMUserMsg userMsg = (IMUserMsg) data;

						if (!userMsg.mIsRecv) {
							DTLog.logError();
							return;
						}

						if (userMsg.mUserMsgType != UserMsgType.Custom) {
							DTLog.logError();
							return;
						}

						if (getLoginStatus() != LoginStatus.Logined) {
							return;
						}

						if (IMPrivateMyself.getInstance().getUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() == 0) {
							DTLog.logError();
							return;
						}

						if (userMsg.getToUID() != IMPrivateMyself.getInstance()
								.getUID()) {
							DTLog.logError();
							return;
						}

						if (sReceiveCustomMessageListener != null) {
							sReceiveCustomMessageListener.onReceiveCustomMessage(
									userMsg.mContent, userMsg.mFromCustomUserID,
									userMsg.mServerSendTime);
						}
					}
				});
	}

	public static void setOnReceiveCustomMessageListener(
			OnReceiveCustomMessageListener l) {
		sReceiveCustomMessageListener = l;
	}

	private static OnReceiveCustomMessageListener sReceiveCustomMessageListener;
	private static long sLastSendCustomMessageTime;
}
