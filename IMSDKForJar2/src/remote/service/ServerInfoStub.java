package remote.service;

import remote.service.aidl.OnLocalSocketListener;
import remote.service.aidl.ServerInfoListener;
import remote.service.data.RemoteDataManager;
import am.core.java.LocalServer;
import android.os.RemoteException;

public class ServerInfoStub extends ServerInfoListener.Stub {

	OnLocalSocketListener localSocketListener;

	@Override
	public int getLoginState() throws RemoteException {
		return 0;
	}

	@Override
	public void reconnect(OnLocalSocketListener l) throws RemoteException {
		setOnLocalSocketListener(l);
		
		// 清除LocalServer的所有对象
		LocalServer.getInstance().closeClient();
//		LocalServer.getInstance().closeServer();
		
		if(!LocalServer.getInstance().isInit()) {
			// 重新打开LocalServer监听
			LocalServer.getInstance().init();
		}
	}

	@Override
	public int getLocalServerStatus() throws RemoteException {
		return LocalServer.getInstance().getLocalServerStatus().getValue();
	}
	
	@Override
	public void closeSocket() throws RemoteException {
		LocalServer.getInstance().closeClient();
	}

	@Override
	public void closeClient() throws RemoteException {
//		RemoteDataManager.getInstance().setClientClose();
	}
	
	public OnLocalSocketListener getOnLocalSocketListener() {
		return localSocketListener;
	}
	
	public void setOnLocalSocketListener(OnLocalSocketListener l) {
		localSocketListener = l;
	}
	
	public void onDestory() {
		localSocketListener = null;
	}

	private ServerInfoStub() {

	}

	public static ServerInfoStub instance;

	public static synchronized ServerInfoStub getInstance() {
		if (instance == null) {
			instance = new ServerInfoStub();
		}

		return instance;
	}

}
