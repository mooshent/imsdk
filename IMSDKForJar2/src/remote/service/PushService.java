package remote.service;

import remote.service.data.RemoteDataManager;
import remote.service.data.base.DTRemoteEnv;
import am.dtlib.model.d.DTDevice;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class PushService extends Service {
	
	@Override
	public IBinder onBind(Intent arg0) {
		RemoteDataManager.getInstance().setClientExist();
		return ServerInfoStub.getInstance();
	}
	
	@Override
	public boolean onUnbind(Intent intent) {
		ServerInfoStub.getInstance().onDestory();
		RemoteDataManager.getInstance().setClientClose();
		return super.onUnbind(intent);
	}
	
	public void onCreate() {
		super.onCreate();
		
		DTRemoteEnv.setContext(getApplicationContext());
		RemoteDataManager.getInstance().setContext(getApplicationContext());
	}
	
}
