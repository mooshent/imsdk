package remote.service.aidl;

import remote.service.aidl.OnLocalSocketListener;
import remote.service.aidl.OnLoginStatusChangeListener;

interface ServerInfoListener {
    /** 获取登录状态 */
	int getLoginState();
	
	/** 清除状态并重连 */
	void reconnect(OnLocalSocketListener listener);
	
	/** 通知socket断开连接 */
	void closeSocket();
	
	/** 获取Socket状态 -- 0: close, 1: listening, 2: connected */
	int getLocalServerStatus();
	
	/** 客户端退出通知 */
	void closeClient();
}