package remote.service.aidl;

interface LocalServerConnectListener {
    /** 状态正常，可以连接 */
	void onServerListening();
	
	/** 服务已关闭 */
	void onServerClosed();
}