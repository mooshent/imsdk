package remote.service.data;

import imsdk.data.IMMyself;
import am.core.java.EngineClient;
import am.dtlib.model.a.base.DTAppEnv;
import am.dtlib.model.c.socket.DTSocket;
import am.dtlib.model.c.socket.DTSocket.DTSocketStatus;
import am.imsdk.model.IMAppSettings;
import android.content.Context;
import android.util.Log;


public class RemoteDataManager {
	private boolean isClientExist = false; //标示客户端进程是否存在
	
	public boolean getClientState() {
		return isClientExist;
	}
	
	public void setClientExist() {
		if(mContext == null) {
			return;
		}
		
		isClientExist = true;
		
		Log.e("Debug", "remote 设置客户端存在");
		
		DTAppEnv.cancelAllPreviousPerformRequests();
		
		if(DTSocket.getInstance() != null && DTSocket.getInstance().getStatus() != DTSocketStatus.None) {
			IMMyself.logout();
		}
	}
	
	private Context mContext;
	
	public void setContext(Context context) {
		mContext = context;
	}
	
	public void setClientClose() {
		if(mContext == null) {
			return;
		}
		
		Log.e("Debug", "remote 设置客户端不存在");
		
		isClientExist = false;
		
		EngineClient.getInstance().checkCloseSocket();
		
		DTAppEnv.performAfterDelayOnUIThread(10, offLineLoginRunnable);
	}
	
	Runnable offLineLoginRunnable = new Runnable() {
		
		@Override
		public void run() {
			Log.e("Debug", "remote 10秒等待结束，开始登录......");
			
			IMMyself.init(mContext, "", new IMMyself.OnAutoLoginListener() {
				
				@Override
				public void onAutoLoginSuccess() {
					Log.e("Debug", "remote 自动登录成功");
				}
				
				@Override
				public void onAutoLoginFailure(boolean loginConflict) {
					Log.e("Debug", "remote 自动登录失败");
				}
				
				@Override
				public void onAutoLoginBegan() {
					
				}
			});
			
			String mCustomUid = IMAppSettings.getInstance().mLastLoginCustomUserID;
			String mPwd = IMAppSettings.getInstance().mLastLoginPassword;
			
			IMMyself.setCustomUserID(mCustomUid);
			IMMyself.setPassword(mPwd);
			IMMyself.login(false, 5, new IMMyself.OnActionListener() {
				
				@Override
				public void onSuccess() {
					Log.e("Debug", "remote 登录成功");
				}
				
				@Override
				public void onFailure(String error) {
					Log.e("Debug", "remote 登录成功：" + error);
				}
			});
		}
	};

	private RemoteDataManager() {}
	
	public static RemoteDataManager instance;
	
	public static synchronized RemoteDataManager getInstance() {
		if(instance == null) {
			instance = new RemoteDataManager();
		}
		
		return instance;
	}
}
