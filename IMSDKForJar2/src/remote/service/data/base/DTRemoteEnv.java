package remote.service.data.base;

import am.dtlib.model.a.base.DTAppEnv;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

public final class DTRemoteEnv {

	private static Context mContext;
	private static Handler mHandler;
	
	public static void setContext(Context context) {
		if (null == context) {
			return;
		}
		
		mContext = context;
		mHandler = new Handler(context.getMainLooper());
	}
	
	public static Context getContext() {
		return mContext;
	}
	
	public static Handler getMainHandler() {
		if (mHandler == null) {
			return null;
		}

		return mHandler;
	}
	
	public static void cancelPreviousPerformRequest(Runnable runnable) {
		if (runnable == null) {
			return;
		}

		if (mHandler == null) {
			return;
		}

		mHandler.removeCallbacks(runnable);
	}

	public static void cancelAllPreviousPerformRequests() {
		if (mHandler == null) {
			return;
		}

		mHandler.removeCallbacksAndMessages(null);
	}

	public static void performAfterDelayOnUIThread(long timeInterval, Runnable runnable) {
		if (null == mHandler) {
			return;
		}

		mHandler.postDelayed(runnable, timeInterval * 1000);
	}
	
	public static boolean isRemoteUIThread() {
		if (DTAppEnv.getContext() == null) {
			return false;
		}

		return Looper.myLooper() == DTRemoteEnv.getContext().getMainLooper();
	}

}
