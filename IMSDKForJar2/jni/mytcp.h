#ifndef MYTCP_H
#define MYTCP_H

#include <android/log.h>
#define LOG_TAG "IMProtocol"

#ifndef LOGD
//#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
//#define LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
//#define LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
//#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define LOGF(...)  __android_log_print(ANDROID_LOG_FATAL,LOG_TAG,__VA_ARGS__)
#endif

#define  MAX_BODY_LEN           10960
#pragma pack(1)

class CMyTcp {
public:
	CMyTcp();
	~CMyTcp();

public:
	int Init(char* ip, int port);

	char* GetErrMsg() {
		return (char*)m_errMsg;
	}

	int Send(char* pkg, int len);
	int Recv(char* pkg, int len);

private:
	int CreateClientTCPSocket(char* szIP, unsigned short ushPort);

private:
	int m_nSocket;
private:
	char m_errMsg[1024];
};

#endif // MYTCP_H
