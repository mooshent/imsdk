package opendemo.activity.base;

import java.util.ArrayList;

import opendemo.customview.PullToRefreshListView;
import opendemo.customview.PullToRefreshListView.OnRefreshListener;
import opendemo.activity.R;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

public class BaseListActivity extends BaseActivity implements OnItemClickListener,
		OnRefreshListener {
	// ui
	protected ArrayAdapter<String> mAdapter;
	protected TextView mEmptyTextView;
	protected PullToRefreshListView mListView;
	protected ProgressBar mInitProgressBar;
	
	@Override
	protected final void initBase(int layoutResID) {
		super.initBase(layoutResID);
	}

	protected void initBaseList(int layoutResID, ArrayList<String> dataList) {
		initBase(layoutResID);

		mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
				dataList);
		mEmptyTextView = (TextView) findViewById(R.id.empty_textview);
		mListView = (PullToRefreshListView) findViewById(R.id.listview);
		mListView.setAdapter(mAdapter);
		mListView.setEmptyView(mEmptyTextView);
		mListView.setOnItemClickListener(this);

		mListView.setOnRefreshListener(this);
		mListView.showHeadFirstTime();
		mListView.setLoadEnable(false);
		mListView.onRefreshComplete();

		mInitProgressBar = (ProgressBar) findViewById(R.id.init_progressbar);
		mInitProgressBar.setVisibility(View.GONE);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int positon, long id) {
	}

	@Override
	protected void onResume() {
		super.onResume();
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public void onRefresh() {
	}
}
