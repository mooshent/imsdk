package opendemo.activity.photo;

import imsdk.data.mainphoto.IMSDKMainPhoto;
import imsdk.data.mainphoto.IMSDKMainPhoto.OnBitmapRequestProgressListener;
import opendemo.activity.base.BaseActivity;
import opendemo.activity.R;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

// 用户头像展示界面
// 主要演示功能：
// 1. 获取服务端用户头像
// 2. 读取本地用户头像数据
public final class PhotoActivity extends BaseActivity {
	// data
	private String mCustomUserID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBase(R.layout.activity_photo);

		mCustomUserID = getIntent().getStringExtra("CustomUserID");

		// 设置标题栏显示内容
		setTitleWithCustomUserID(mCustomUserID);

		final TextView tipTextView = (TextView) findViewById(R.id.photo_tip_textview);
		final ImageView imageView = (ImageView) findViewById(R.id.photo_imageview);

		// 读取本地数据
		Bitmap bitmap = IMSDKMainPhoto.get(mCustomUserID);

		if (bitmap != null) {
			imageView.setImageBitmap(bitmap);
		} else {
			tipTextView.setText("无本地数据");
		}

		// 向服务端请求最新头像数据
		// 已实现网络流量优化
		IMSDKMainPhoto.request(mCustomUserID, 30,
				new OnBitmapRequestProgressListener() {
					@Override
					public void onSuccess(Bitmap mainPhoto, byte[] buffer) {
						if (mainPhoto == null) {
							tipTextView.setText("用户还没有设置头像");
						} else {
							tipTextView.setText("下载成功！");
							imageView.setImageBitmap(mainPhoto);
						}
					}

					@Override
					public void onProgress(double progress) {
						int percent = (int) (progress * 100);

						tipTextView.setText("下载进度 : " + percent + "%");
					}

					@Override
					public void onFailure(String error) {
						tipTextView.setText("下载失败 : " + error);
					}
				});
	}
}