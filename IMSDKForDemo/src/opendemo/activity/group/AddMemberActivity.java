package opendemo.activity.group;

import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.group.IMGroupInfo;
import imsdk.data.group.IMMyselfGroup;
import imsdk.data.group.IMSDKGroup;

import java.util.ArrayList;

import opendemo.activity.base.BaseActivity;
import opendemo.activity.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class AddMemberActivity extends BaseActivity implements View.OnClickListener {
	// data
	private String mGroupID;
	private final static int SUCCESS = 0;
	private final static int FAIL = -1;
	private ArrayList<String> mAddedMembersList;

	// ui
	private EditText mCustomUserIDEditText;
	private Button mAddMemberBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBase(R.layout.activity_group_addmember);

		mGroupID = getIntent().getStringExtra("groupID");

		((ImageButton) findViewById(R.id.left)).setOnClickListener(this);

		IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(mGroupID);

		// 设置标题栏显示内容
		setTitle(groupInfo.getGroupName());

		mAddMemberBtn = (Button) findViewById(R.id.addmember_btn);
		mAddMemberBtn.setOnClickListener(this);

		mCustomUserIDEditText = (EditText) findViewById(R.id.addmember_edittext);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.left:
			if (mAddedMembersList != null && mAddedMembersList.size() > 0) {
				Intent intent = new Intent();

				intent.putStringArrayListExtra("addedMembersList", mAddedMembersList);
				setResult(Activity.RESULT_OK, intent);
			}

			this.finish();
			break;
		case R.id.addmember_btn:
			final String customUserID = mCustomUserIDEditText.getText().toString();

			mAddMemberBtn.setEnabled(false);

			if (customUserID != null && customUserID.trim().length() > 0) {
				IMMyselfGroup.addMemeber(customUserID, mGroupID,
						new OnActionListener() {
							@Override
							public void onSuccess() {
								if (mAddedMembersList == null) {
									mAddedMembersList = new ArrayList<String>();
								}

								mAddedMembersList.add(customUserID);
								showDialog(customUserID, SUCCESS, null);
								mCustomUserIDEditText.setText("");
							}

							@Override
							public void onFailure(String error) {
								showDialog(customUserID, FAIL, error);
							}
						});
			}
			break;
		default:
			break;
		}
	}

	private void showDialog(String customUserID, int state, String error) {
		mAddMemberBtn.setEnabled(true);

		String result = null;

		IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(mGroupID);

		if (state == SUCCESS) {
			result = customUserID + "已成功加入群 " + groupInfo.getGroupName() + " !";
		} else {
			result = customUserID + "加入群 " + groupInfo.getGroupName() + " 失败:" + error;
		}

		Toast.makeText(AddMemberActivity.this, result, Toast.LENGTH_SHORT).show();
	}
}
