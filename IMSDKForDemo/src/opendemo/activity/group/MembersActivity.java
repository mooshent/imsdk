package opendemo.activity.group;

import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.group.IMGroupInfo;
import imsdk.data.group.IMMyselfGroup;
import imsdk.data.group.IMSDKGroup;

import java.util.ArrayList;

import opendemo.activity.base.BaseListActivity;
import opendemo.activity.R;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

public class MembersActivity extends BaseListActivity implements View.OnClickListener {
	// data
	private String mGroupID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mGroupID = getIntent().getStringExtra("groupID");

		// 获取群用户列表前，获取IMGroupInfo对象
		IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(mGroupID);

		// 获取群用户列表，初始化界面
		initBaseList(R.layout.activity_group_members, groupInfo.getMemberList());

		// 设置标题栏显示内容
		setTitle("成员列表");

		TextView rightTitleBar = (TextView) findViewById(R.id.right);

		rightTitleBar.setText("添加群成员");
		rightTitleBar.setOnClickListener(this);

		// 只有自己创建的群，才有权利添加或删除群成员
		if (IMMyselfGroup.isMyOwnGroup(mGroupID)) {
			rightTitleBar.setVisibility(View.VISIBLE);
		} else {
			rightTitleBar.setVisibility(View.INVISIBLE);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// 弹窗询问是否确定移除某个成员
		showAlertDialog(position);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.right: {
			Intent intent = new Intent(MembersActivity.this, AddMemberActivity.class);

			intent.putExtra("groupID", mGroupID);
			startActivityForResult(intent, 1000);
		}
			break;
		default:
			break;
		}
	}

	private void showAlertDialog(final int position) {
		AlertDialog.Builder builder = new AlertDialog.Builder(MembersActivity.this);

		// 获取群名前，先获取IMGroupInfo对象
		IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(mGroupID);

		// 通过IMGroupInfo对象，获取群名
		String groupName = groupInfo.getGroupName();

		// 获取群用户列表
		final ArrayList<String> membersList = groupInfo.getMemberList();

		builder.setTitle("www.IMSDK.im");
		builder.setMessage("确定将" + membersList.get(position - 1) + "从" + groupName
				+ "移除？");
		builder.setCancelable(false);

		builder.setPositiveButton("确定", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();

				final String userToBeRemoved = membersList.get(position - 1);

				IMMyselfGroup.removeMember(userToBeRemoved, mGroupID,
						new OnActionListener() {
							@Override
							public void onSuccess() {
								Toast.makeText(MembersActivity.this,
										"移除成员" + userToBeRemoved + "成功！",
										Toast.LENGTH_SHORT).show();
								mAdapter.notifyDataSetChanged();
							}

							@Override
							public void onFailure(String error) {
								Toast.makeText(MembersActivity.this,
										"移除成员" + userToBeRemoved + "失败！" + error,
										Toast.LENGTH_SHORT).show();
							}
						});
			}
		});

		builder.setNegativeButton("取消", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		builder.create().show();
	}
}
