package opendemo.activity.group;

import imsdk.data.IMMyself.OnActionResultListener;
import imsdk.data.IMMyself.OnInitializedListener;
import imsdk.data.group.IMGroupInfo;
import imsdk.data.group.IMMyselfGroup;
import imsdk.data.group.IMMyselfGroup.OnGroupEventsListener;
import imsdk.data.group.IMSDKGroup;

import java.util.ArrayList;

import opendemo.activity.base.BaseListActivity;
import opendemo.activity.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

public class MyGroupsActivity extends BaseListActivity implements View.OnClickListener {
	// data
	private ArrayList<String> mGroupNamesList;

	// ui
	private TextView mRightTitleBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mGroupNamesList = new ArrayList<String>();

		if (IMMyselfGroup.isInitialized()) {
			ArrayList<String> groupIDsList = IMMyselfGroup.getMyGroupsList();

			for (String groupID : groupIDsList) {
				IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(groupID);

				mGroupNamesList.add(groupInfo.getGroupName());
			}

			Toast.makeText(MyGroupsActivity.this, "群组模块已初始化", Toast.LENGTH_SHORT)
					.show();
		}

		initBaseList(R.layout.activity_mygroups, mGroupNamesList);

		mListView.setPullEnable(false);
		mListView.setLoadEnable(false);

		setTitle(R.string.my_groups);

		mRightTitleBar = (TextView) findViewById(R.id.right);
		mRightTitleBar.setText(getString(R.string.found_a_group));
		mRightTitleBar.setVisibility(View.VISIBLE);

		// 判断群组模块是否已经初始化
		if (IMMyselfGroup.isInitialized()) {
			mInitProgressBar.setVisibility(View.GONE);
			updateListView();
		} else {
			mInitProgressBar.setVisibility(View.VISIBLE);

			IMMyselfGroup.setOnInitializedListener(new OnInitializedListener() {
				@Override
				public void onInitialized() {
					mInitProgressBar.setVisibility(View.GONE);
				}
			});

			// 设置群组事件监听器
			IMMyselfGroup.setOnGroupEventsListener(new OnGroupEventsListener() {
				@Override
				public void onInitialized() {
					mInitProgressBar.setVisibility(View.GONE);
					updateListView();
				}

				@Override
				public void onRemovedFromGroup(String groupID, String customUserID,
						long actionServerTime) {
					updateListView();
				}

				@Override
				public void onGroupNameUpdated(String newGroupName, String groupID,
						long actionServerTime) {
					updateListView();
				}

				@Override
				public void onGroupMemberUpdated(ArrayList<String> memberList,
						String groupID, long actionServerTime) {
					updateListView();
				}

				@Override
				public void onGroupDeletedByUser(String groupID, String customUserID,
						long actionServerTime) {
					updateListView();
				}

				@Override
				public void onCustomGroupInfoUpdated(String newGroupInfo,
						String groupID, long actionSeverTime) {
					updateListView();
				}

				@Override
				public void onAddedToGroup(String groupID, long actionServerTime) {
					updateListView();
				}
			});
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Intent intent = new Intent(MyGroupsActivity.this, GroupActivity.class);

		intent.putExtra("groupID", IMMyselfGroup.getMyGroupsList().get(position - 1));
		startActivityForResult(intent, 100);
	}

	private void updateListView() {
		mGroupNamesList.clear();

		ArrayList<String> groupIDsList = IMMyselfGroup.getMyGroupsList();

		if (groupIDsList == null || groupIDsList.size() == 0) {
			mAdapter.notifyDataSetChanged();
			return;
		}

		for (String groupID : groupIDsList) {
			IMGroupInfo groupInfo = IMSDKGroup.getGroupInfo(groupID);

			mGroupNamesList.add(groupInfo.getGroupName());
		}

		mAdapter.notifyDataSetChanged();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.right:
			mRightTitleBar.setEnabled(false);

			IMMyselfGroup.createGroup("我的IMSDK群", new OnActionResultListener() {
				@Override
				public void onSuccess(Object result) {
					mRightTitleBar.setEnabled(true);
					updateListView();
				}

				@Override
				public void onFailure(String error) {
					mRightTitleBar.setEnabled(true);
					showDialog("创建失败:" + error);
				}
			});
			break;
		default:
			break;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		updateListView();
	}
}
