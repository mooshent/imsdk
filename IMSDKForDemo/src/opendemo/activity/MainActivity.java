package opendemo.activity;

import imsdk.data.IMMyself;
import imsdk.data.IMMyself.OnActionListener;
import imsdk.data.IMMyself.OnReceiveBitmapListener;
import imsdk.data.IMMyself.OnReceiveTextListener;
import imsdk.data.custommessage.IMMyselfCustomMessage;
import imsdk.data.group.IMMyselfGroup;
import imsdk.data.group.IMMyselfGroup.OnGroupMessageListener;
import imsdk.data.relations.IMMyselfRelations;
import imsdk.data.relations.IMMyselfRelations.OnRelationsEventListener;
import imsdk.views.IMEmotionTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import opendemo.activity.base.BaseActivity;
import opendemo.activity.chatview.ChatActivity;
import opendemo.activity.chatview.ChatXMLActivity;
import opendemo.activity.group.MyGroupsActivity;
import opendemo.activity.social.AroundActivity;
import opendemo.activity.social.RelationActivity;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

// 登录后的第一个界面
// 只要用户处于登录状态下，就不会销毁MainActivity
// 主要演示功能：
// 1. 监听消息接收
// 	 a) 用户消息
//   b) 群消息
// 2. 监听社交关系事件
public final class MainActivity extends BaseActivity implements View.OnClickListener {
	// ui
	private EditText mTargetUserIDEditText;
	private EditText mContentEditText;
	private EditText mCustomMessageEditText;
	private Button mSendBtn;
	private TextView mAppKeyTextView;

	private TextView mCountDownTextView;
	private View mCountDownBackgroundView;

	private Animation mShowAnimation;
	private Animation mHideAnimation;
	private boolean mCountDownAnimating;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBase(R.layout.activity_main);

		setTitleWithCustomUserID(IMMyself.getCustomUserID());

		TextView rightTextView = ((TextView) findViewById(R.id.right));

		rightTextView.setText("扩展");
		rightTextView.setVisibility(View.VISIBLE);

		((Button) findViewById(R.id.main_around_btn)).setOnClickListener(this);
		((Button) findViewById(R.id.main_mygroups_btn)).setOnClickListener(this);
		((Button) findViewById(R.id.main_chatctrldemo_btn)).setOnClickListener(this);
		((Button) findViewById(R.id.main_customerservice_btn)).setOnClickListener(this);

		mTargetUserIDEditText = (EditText) findViewById(R.id.main_custom_userid_edittext);
		mContentEditText = (EditText) findViewById(R.id.main_content_edittext);
		mCustomMessageEditText = (EditText) findViewById(R.id.main_custommessage_edittext);
		mSendBtn = (Button) findViewById(R.id.main_send_btn);
		mAppKeyTextView = (TextView) findViewById(R.id.main_appkey_textview);
		mCountDownTextView = (TextView) findViewById(R.id.main_count_down_textview);
		mCountDownBackgroundView = findViewById(R.id.main_count_down_bg_view);
		mShowAnimation = AnimationUtils.loadAnimation(this, R.anim.show);
		mHideAnimation = AnimationUtils.loadAnimation(this, R.anim.hide);

		mHideAnimation.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				mCountDownBackgroundView.setVisibility(View.INVISIBLE);
			}
		});

		mAppKeyTextView.setText("AppKey:" + IMMyself.getAppKey());

		mSendBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String text = mContentEditText.getText().toString();

				if (text.length() > 0) {
					// 根据消息的实际长度，计算出超时时长
					int timeoutInterval = (text.length() / 400 + 1) * 3;

					// 开始执行倒计时动画
					MainActivity.this.startCountDownAnimation(timeoutInterval);

					// 发送IM消息
					IMMyself.sendText(text, mTargetUserIDEditText.getText().toString(),
							timeoutInterval, new OnActionListener() {
								@Override
								public void onSuccess() {
									MainActivity.this.stopCountDownAnimation();
									Toast.makeText(MainActivity.this, "发送成功",
											Toast.LENGTH_SHORT).show();
								}

								@Override
								public void onFailure(String error) {
									MainActivity.this.stopCountDownAnimation();
									Toast.makeText(MainActivity.this,
											"发送失败 -- " + error, Toast.LENGTH_SHORT)
											.show();
								}
							});
				} else {
					String customMessage = mCustomMessageEditText.getText().toString();

					if (customMessage.length() > 0) {
						// 根据自定义消息的实际长度，计算出超时时长
						int timeoutInterval = (customMessage.length() / 400 + 1) * 3;

						// 开始执行倒计时动画
						MainActivity.this.startCountDownAnimation(timeoutInterval);

						// 发送自定义消息
						IMMyselfCustomMessage.send(customMessage, mTargetUserIDEditText
								.getText().toString(), timeoutInterval,
								new OnActionListener() {
									@Override
									public void onSuccess() {
										// 停止执行倒计时动画
										MainActivity.this.stopCountDownAnimation();

										// 提示自定义消息发送成功
										Toast.makeText(MainActivity.this,
												"CustomMessage发送成功", Toast.LENGTH_SHORT)
												.show();
									}

									@Override
									public void onFailure(String error) {
										// 停止执行倒计时动画
										MainActivity.this.stopCountDownAnimation();

										// 提示自定义消息发送失败
										Toast.makeText(MainActivity.this,
												"CustomMessage发送失败 -- " + error,
												Toast.LENGTH_SHORT).show();
									}
								});
					}
				}
			}
		});

		// 设置接收消息回调
		IMMyself.setOnReceiveTextListener(new OnReceiveTextListener() {
			@Override
			public void onReceiveText(String text, String fromCustomUserID, String nickName,
					long serverActionTime, String extraData) {
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_layout,
						(ViewGroup) findViewById(R.id.toast_root));
				View view = layout.findViewById(R.id.toast_textview);

				if (view instanceof ImageView) {
					return;
				}

				if (!(view instanceof IMEmotionTextView)) {
					return;
				}

				IMEmotionTextView textView = (IMEmotionTextView) layout
						.findViewById(R.id.toast_textview);

				textView.setGifEmotionText(fromCustomUserID + "于"
						+ getTimeBylong(serverActionTime * 1000) + " 发来消息:" + "\n"
						+ text + "\n扩展信息：" + extraData);

				Toast toast = new Toast(MainActivity.this);

				toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 300);
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}

			@Override
			public void onReceiveSystemText(String fromCustomUserID, String text, long serverActionTime) {
				Toast.makeText(MainActivity.this,
						getTimeBylong(serverActionTime * 1000) + " 收到系统消息:" + text,
						Toast.LENGTH_LONG).show();
			}
			
			@Override
			public void onReceiveAudio(String messageID, String fromCustomUserID, String nickName,
					long serverActionTime, String extraData) {
				Toast.makeText(MainActivity.this,
						getTimeBylong(serverActionTime * 1000) + " 收到语音消息" + "\n扩展信息：" + extraData,
						Toast.LENGTH_LONG).show();
			}
		});
		
		IMMyself.setOnReceiveBitmapListener(new OnReceiveBitmapListener() {
			
			@Override
			public void onReceiveBitmapProgress(double progress, String messageID,
					String fromCustomUserID, long serverActionTime) {
				
			}
			
			@Override
			public void onReceiveBitmapMessage(String messageID,
					String fromCustomUserID, String nickName, long serverActionTime, String extraData) {
				Toast.makeText(MainActivity.this,
						getTimeBylong(serverActionTime * 1000) + " 收到图片消息" + "\n扩展信息：" + extraData,
						Toast.LENGTH_LONG).show();
			}
			
			@Override
			public void onReceiveBitmap(Bitmap bitmap, String messageID,
					String fromCustomUserID, long serverActionTime) {
				
			}
		});

		// 设置接收自定义消息回调
		IMMyselfCustomMessage
				.setOnReceiveCustomMessageListener(new IMMyselfCustomMessage.OnReceiveCustomMessageListener() {
					@Override
					public void onReceiveCustomMessage(String customMessage,
							String fromCustomUserID, long serverActionTime) {
						LayoutInflater inflater = getLayoutInflater();
						View layout = inflater.inflate(R.layout.toast_layout,
								(ViewGroup) findViewById(R.id.toast_root));
						IMEmotionTextView textView = (IMEmotionTextView) layout
								.findViewById(R.id.toast_textview);

						textView.setStaticEmotionText(fromCustomUserID + "于"
								+ getTimeBylong(serverActionTime * 1000) + " 发来自定义消息:"
								+ customMessage);

						Toast toast = new Toast(MainActivity.this);

						toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 300);
						toast.setDuration(Toast.LENGTH_SHORT);
						toast.setView(layout);
						toast.show();
					}
				});

		// 设置接收群消息回调
		IMMyselfGroup.setOnGroupMessageListener(new OnGroupMessageListener() {
			@Override
			public void onReceiveText(String text, String groupID,
					String fromCustomUserID, long actionServerTime) {
				LayoutInflater inflater = getLayoutInflater();
				View view = inflater.inflate(R.layout.toast_layout,
						(ViewGroup) findViewById(R.id.toast_root));
				IMEmotionTextView textView = (IMEmotionTextView) view
						.findViewById(R.id.toast_textview);

				textView.setGifEmotionText("群消息: " + fromCustomUserID + "于"
						+ getTimeBylong(actionServerTime * 1000) + " 发来消息:" + "\n"
						+ text);

				Toast toast = new Toast(MainActivity.this);

				toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 300);
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(view);
				toast.show();
			}

			@Override
			public void onReceiveCustomMessage(String customMessage, String groupID,
					String fromCustomUserID, long actionServerTime) {
				LayoutInflater inflater = getLayoutInflater();
				View view = inflater.inflate(R.layout.toast_layout,
						(ViewGroup) findViewById(R.id.toast_root));
				IMEmotionTextView textView = (IMEmotionTextView) view
						.findViewById(R.id.toast_textview);

				textView.setGifEmotionText("群消息: " + fromCustomUserID + "于"
						+ getTimeBylong(actionServerTime * 1000) + " 发来消息:" + "\n"
						+ customMessage);

				Toast toast = new Toast(MainActivity.this);

				toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 300);
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(view);
				toast.show();
			}

			@Override
			public void onReceiveBitmapMessage(String messageID, String groupID,
					String fromCustomUserID, long serverActionTime) {
			}

			@Override
			public void onReceiveBitmap(Bitmap bitmap, String groupID,
					String fromCustomUserID, long serverActionTime) {
			}

			@Override
			public void onReceiveBitmapProgress(double progress, String groupID,
					String fromCustomUserID, long serverActionTime) {
			}
		});

		// 设置用户社交关系事件监听器
		IMMyselfRelations.setOnRelationsEventListener(new OnRelationsEventListener() {
			@Override
			public void onReceiveRejectToFriendRequest(String reason,
					String fromCustomUserID, long serverActionTime) {
				if (isBackground(MainActivity.this)) {
					showNotify(notifyId++, "很抱歉，" + fromCustomUserID + "拒绝了你得好友请求！",
							"IMSDK", MainActivity.this);
				} else {
					Toast.makeText(MainActivity.this,
							"很抱歉，" + fromCustomUserID + "拒绝了你得好友请求！", Toast.LENGTH_LONG)
							.show();
				}
			}

			@Override
			public void onReceiveFriendRequest(String text, String fromCustomUserID,
					long serverActionTime) {
				if (isBackground(MainActivity.this)) {
					showIntentActivityNotify(notifyId++, "附加消息：" + text, "IMSDK提示：收到 "
							+ fromCustomUserID + " 一条好友请求消息！", fromCustomUserID,
							RelationActivity.class, MainActivity.this);
				} else {
					Intent intent = new Intent(MainActivity.this,
							RelationActivity.class);

					intent.putExtra("CustomUserID", fromCustomUserID);
					intent.putExtra("notice", true);
					startActivity(intent);
				}
			}

			@Override
			public void onReceiveAgreeToFriendRequest(String fromCustomUserID,
					long serverActionTime) {
				String content = fromCustomUserID + "同意了你的好友请求，点击聊天！";

				if (isBackground(MainActivity.this)) {
					showIntentActivityNotify(notifyId++, content, "来自 IMSDK 消息！",
							fromCustomUserID, ChatActivity.class, MainActivity.this);
				} else {
					Intent intent = new Intent(MainActivity.this, ChatActivity.class);

					intent.putExtra("CustomUserID", fromCustomUserID);
					startActivity(intent);
				}
			}

			@Override
			public void onInitialized() {
			}

			@Override
			public void onBuildFriendshipWithUser(String customUserID,
					long serverActionTime) {
				Toast.makeText(MainActivity.this,
						"onBuildFriendshipWithUser  " + customUserID, Toast.LENGTH_LONG)
						.show();
			}

			@Override
			public void onDeleteFriends(String fromCustomUserID, long serverActionTime) {
				
			}
		});
		
		IMMyself.setOnReceiveOrderListener(new IMMyself.OnReceiveOrderListener() {
			
			@Override
			public void onReceiveOrder(String content, String fromCustomUserID, String nickName,
					long serverActionTime, String extraData) {
				LayoutInflater inflater = getLayoutInflater();
				View view = inflater.inflate(R.layout.toast_layout,
						(ViewGroup) findViewById(R.id.toast_root));
				IMEmotionTextView textView = (IMEmotionTextView) view
						.findViewById(R.id.toast_textview);

				textView.setGifEmotionText("订单消息: " + fromCustomUserID + "于"
						+ getTimeBylong(serverActionTime * 1000) + " 发来消息:" + "\n"
						+ content + "\n"
						+ extraData);

				Toast toast = new Toast(MainActivity.this);

				toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 300);
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(view);
				toast.show();
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			IMMyself.logout();
			Toast.makeText(this, "成功退出登录", Toast.LENGTH_SHORT).show();
		}

		return super.onKeyDown(keyCode, event);
	}

	private void startCountDownAnimation(final int timeInterval) {
		InputMethodManager inputMethodMgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

		inputMethodMgr.hideSoftInputFromWindow(mTargetUserIDEditText.getWindowToken(),
				0);
		inputMethodMgr.hideSoftInputFromWindow(mContentEditText.getWindowToken(), 0);
		inputMethodMgr.hideSoftInputFromWindow(mCustomMessageEditText.getWindowToken(),
				0);

		mTargetUserIDEditText.clearFocus();
		mContentEditText.clearFocus();
		mCustomMessageEditText.clearFocus();

		mSendBtn.setClickable(false);
		mTargetUserIDEditText.setFocusable(false);
		mContentEditText.setFocusable(false);
		mCustomMessageEditText.setFocusable(false);
		mCountDownBackgroundView.setVisibility(View.VISIBLE);
		mCountDownTextView.setVisibility(View.VISIBLE);
		mCountDownAnimating = true;
		mCountDownBackgroundView.startAnimation(mShowAnimation);

		this.countDown(timeInterval);
	}

	private void stopCountDownAnimation() {
		mSendBtn.setClickable(true);
		mTargetUserIDEditText.setFocusable(true);
		mTargetUserIDEditText.setFocusableInTouchMode(true);
		mContentEditText.setFocusable(true);
		mContentEditText.setFocusableInTouchMode(true);
		mCustomMessageEditText.setFocusable(true);
		mCustomMessageEditText.setFocusableInTouchMode(true);
		mCountDownAnimating = false;
		mCountDownTextView.clearAnimation();
		mCountDownBackgroundView.startAnimation(mHideAnimation);
		mCountDownTextView.setVisibility(View.INVISIBLE);
	}

	private void countDown(final int timeInterval) {
		if (!mCountDownAnimating || timeInterval < 0) {
			this.stopCountDownAnimation();
			return;
		}

		if (timeInterval == 0) {
			mCountDownTextView.setText("IMSDK");
		} else {
			mCountDownTextView.setText("" + timeInterval);
		}

		mCountDownTextView.clearAnimation();

		AnimationSet countDownAnimationSet = (AnimationSet) AnimationUtils
				.loadAnimation(this, R.anim.count_down);

		countDownAnimationSet.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				if (!mCountDownAnimating) {
					return;
				}

				MainActivity.this.countDown(timeInterval - 1);
			}
		});

		mCountDownTextView.startAnimation(countDownAnimationSet);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.right:
			startActivity(new Intent(MainActivity.this, ExtendActivity.class));
			break;
		case R.id.main_around_btn:
			startActivity(new Intent(MainActivity.this, AroundActivity.class));
			break;
		case R.id.main_mygroups_btn:
			startActivity(new Intent(MainActivity.this, MyGroupsActivity.class));
			break;
		case R.id.main_chatctrldemo_btn:
//			startActivity(new Intent(MainActivity.this, ChatActivity.class).putExtra(
//					"CustomUserID", IMMyself.getCustomUserID()));
			
			showUserList();
			break;
		case R.id.main_customerservice_btn:
			startActivity(new Intent(MainActivity.this, ServiceActivity.class));
			break;
		default:
			break;
		}
	}

	private int notifyId;

	/** 显示通知栏点击跳转到指定Activity */
	public void showIntentActivityNotify(int notifyId, String content, String title,
			String user, Class<? extends Activity> clazz, Context context) {
		// Notification.FLAG_ONGOING_EVENT --设置常驻
		// Flag;Notification.FLAG_AUTO_CANCEL 通知栏上点击此通知后自动清除此通知
		// notification.flags = Notification.FLAG_AUTO_CANCEL;
		// //在通知栏上点击此通知后自动清除此通知
		NotificationManager notificationManager = (NotificationManager) this
				.getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

		builder.setContentTitle(title).setContentText(content)
		// .setNumber(number)//显示数量
				.setTicker("您有一条新消息！")// 通知首次出现在通知栏，带上升动画效果的
				.setWhen(System.currentTimeMillis())// 通知产生的时间，会在通知信息里显示
				.setPriority(Notification.PRIORITY_DEFAULT)// 设置该通知优先级
				.setAutoCancel(false)// 设置这个标志当用户单击面板就可以让通知将自动取消
				// .setOngoing(false)//ture，设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)
				.setDefaults(Notification.DEFAULT_ALL)// 向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合：
				// Notification.DEFAULT_ALL Notification.DEFAULT_SOUND 添加声音 //
				// requires VIBRATE permission
				.setAutoCancel(true)// 点击后让通知将消失
				.setSmallIcon(R.drawable.ic_launcher);

		// 点击的意图ACTION是跳转到Intent
		Intent intent = new Intent(this, clazz);

		intent.putExtra("CustomUserID", user);
		intent.putExtra("notify", true);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		builder.setContentIntent(pendingIntent);
		notificationManager.notify(notifyId, builder.build());
	}

	public void showNotify(int notifyId, String content, String user, Context context) {
		NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

		builder.setContentTitle("来自" + user + "的信息").setContentText(content)
		// .setNumber(number)//显示数量
				.setTicker("您有一条新消息！")// 通知首次出现在通知栏，带上升动画效果的
				.setWhen(System.currentTimeMillis())// 通知产生的时间，会在通知信息里显示
				.setPriority(Notification.PRIORITY_DEFAULT)// 设置该通知优先级
				.setAutoCancel(false)// 设置这个标志当用户单击面板就可以让通知将自动取消
				// .setOngoing(false)//ture，设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)
				.setDefaults(Notification.DEFAULT_ALL)// 向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合：
				// Notification.DEFAULT_ALL Notification.DEFAULT_SOUND 添加声音 //
				// requires VIBRATE permission
				.setSmallIcon(R.drawable.ic_launcher);

		notificationManager.notify(notifyId, builder.build());
	}

	/**
	 * 
	 * @param context
	 * @return 判断当前应用是否在前台或后台
	 */
	public boolean isBackground(Context context) {
		ActivityManager activityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> appProcesses = activityManager
				.getRunningAppProcesses();

		for (RunningAppProcessInfo appProcess : appProcesses) {
			if (appProcess.processName.equals(context.getPackageName())) {
				if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_BACKGROUND) {
					return true; // "后台"
				} else {
					return false; // "前台"
				}
			}
		}

		return false;
	}

	public String getTimeBylong(long longtime) {
		Date date = new Date(longtime);
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
				Locale.getDefault());
		String currentTime = format.format(date);

		return currentTime;
	}
	
	private void showUserList() {
		Dialog userDialog = new Dialog(this);
		
		View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_userlist, null);
		
		ListView userList = (ListView) dialogView.findViewById(R.id.lv_userlist);
		
		final List<Map<String, Object>> listems = new ArrayList<Map<String, Object>>(); 
		
		Map<String, Object> listem1 = new HashMap<String, Object>();  
        listem1.put("phone", "11180"); 
        
        Map<String, Object> listem2 = new HashMap<String, Object>();  
        listem2.put("phone", "11181"); 
        
        Map<String, Object> listem3 = new HashMap<String, Object>();  
        listem3.put("phone", "3000027"); 
        
        Map<String, Object> listem4 = new HashMap<String, Object>();  
        listem4.put("phone", "3000023");
        
        listems.add(listem1);
        listems.add(listem2);
        listems.add(listem3);
        listems.add(listem4);
        
        SimpleAdapter simplead = new SimpleAdapter(this, listems,  
                R.layout.item_user, new String[] { "phone" },  
                new int[] { R.id.tvName });  
          
        userList.setAdapter(simplead);
        userList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				String phone = listems.get(arg2).get("phone").toString();
				
				startActivity(new Intent(MainActivity.this, ChatXMLActivity.class).putExtra(
						"CustomUserID", phone));
			}
		});
        
        final EditText mCustomUidEditView = (EditText) dialogView.findViewById(R.id.mCustomUidEditView);
        
        Button mSaveBtn = (Button) dialogView.findViewById(R.id.mSaveBtn);
        mSaveBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mCustomUidEditView != null && !TextUtils.isEmpty(mCustomUidEditView.getText().toString())) {
					String customUid = mCustomUidEditView.getText().toString();
					
					startActivity(new Intent(MainActivity.this, ChatXMLActivity.class).putExtra(
							"CustomUserID", customUid));
				}
			}
		});
        
        userDialog.setTitle("用户列表");
        userDialog.setContentView(dialogView);
        userDialog.show();
	}
}
