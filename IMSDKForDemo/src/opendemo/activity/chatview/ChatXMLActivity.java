package opendemo.activity.chatview;

import java.util.HashMap;

import imsdk.data.IMMyself;
import imsdk.views.IMChatView;
import opendemo.activity.base.BaseActivity;
import opendemo.activity.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

public class ChatXMLActivity extends BaseActivity {
	// data
	private String mCustomUserID;

	// ui
	private IMChatView mChatView;
	
	private View commentLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 通过配置xml的方式创建一个IMChatView
		initBase(R.layout.activity_chat_xml);

		mCustomUserID = getIntent().getExtras().getString("CustomUserID");

		// 获取已创建的IMChatView实例
		mChatView = (IMChatView) findViewById(R.id.chat_xml_chatview);

		// 配置IMChatView实例
		mChatView.addTag("orderId", 157525); //一定要在setCustomUserID之前
		mChatView.setCustomUserID(mCustomUserID);
		mChatView.setMaxGifCountInMessage(10);
		mChatView.setUserMainPhotoVisible(true);
		mChatView.setUserMainPhotoCornerRadius(10);
		mChatView.setTitleBarVisible(true);
		mChatView.setOperationViewVisible(true);

		// 添加头像点击事件监听
		mChatView.setOnHeadPhotoClickListener(new IMChatView.OnHeadPhotoClickListener() {

			@Override
			public void onClick(View v, String customUserID) {
				Toast.makeText(ChatXMLActivity.this, "您点击了" + customUserID,
						Toast.LENGTH_SHORT).show();
			}

		});

		mChatView.setOrderContinueListener(new Runnable() {
			
			@Override
			public void run() {
				Toast.makeText(ChatXMLActivity.this, "继续订单", Toast.LENGTH_SHORT).show();
				mChatView.setOperationViewVisible(true);
				commentLayout.setVisibility(View.GONE);
				
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("orderId", 157525);
				
				IMMyself.sendNoticeMsg("对方不同意结束服务", mCustomUserID, map, 5, new IMMyself.OnActionListener() {
					
					@Override
					public void onSuccess() {
						Toast.makeText(ChatXMLActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
					}
					
					@Override
					public void onFailure(String error) {
						Toast.makeText(ChatXMLActivity.this, "发送失败", Toast.LENGTH_SHORT).show();
					}
				});
			}
		});

		mChatView.setOrderConfirmListener(new Runnable() {
			
			@Override
			public void run() {
				Toast.makeText(ChatXMLActivity.this, "确定订单", Toast.LENGTH_SHORT).show();
				mChatView.setOperationViewVisible(false);
				commentLayout.setVisibility(View.VISIBLE);
			}
		});
		
		mChatView.setExtraBtnVisible(true);
		mChatView.setExtraBtnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mChatView.setOperationViewVisible(false);
			}
		});
		
		commentLayout = findViewById(R.id.commentLayout);
	}
	
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.commentBtn:
			mChatView.setOperationViewVisible(true);
			commentLayout.setVisibility(View.GONE);
			break;
		case R.id.btn:
			IMMyself.sendRequestForOrderFinish(mCustomUserID, 157525, 5, new IMMyself.OnActionListener() {
				
				@Override
				public void onSuccess() {
					Toast.makeText(ChatXMLActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
				}
				
				@Override
				public void onFailure(String error) {
					Toast.makeText(ChatXMLActivity.this, "发送失败", Toast.LENGTH_SHORT).show();
				}
			});
			break;
		default:
			break;
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// 为了实现捕获用户选择的图片
		mChatView.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// 为了实现点击返回键隐藏表情栏
		mChatView.onKeyDown(keyCode, event);
		return super.onKeyDown(keyCode, event);
	}
}
