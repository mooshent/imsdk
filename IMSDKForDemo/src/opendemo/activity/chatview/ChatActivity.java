package opendemo.activity.chatview;

import imsdk.views.IMChatView;
import opendemo.activity.base.BaseActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

public class ChatActivity extends BaseActivity {
	// data
	private String mCustomUserID;

	// ui
	private IMChatView mChatView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initBase(0);

		mCustomUserID = getIntent().getStringExtra("CustomUserID");

		// 创建一个IMChatView实例
		mChatView = new IMChatView(this, mCustomUserID);

		// 为IMChatView实例配置参数
		mChatView.setMaxGifCountInMessage(10);
		mChatView.setUserMainPhotoVisible(true);
		mChatView.setUserMainPhotoCornerRadius(10);
		mChatView.setTitleBarVisible(true);

		// 添加到当前activity
		setContentView(mChatView);
		
		//添加头像点击事件监听
		mChatView.setOnHeadPhotoClickListener(new IMChatView.OnHeadPhotoClickListener() {
			
			@Override
			public void onClick(View v, String customUserID) {
				Toast.makeText(ChatActivity.this, "您点击了"+customUserID, Toast.LENGTH_SHORT).show();
			}
			
		});
		
		mChatView.setOrderContinueListener(new Runnable() {
			
			@Override
			public void run() {
				Toast.makeText(ChatActivity.this, "继续订单", Toast.LENGTH_SHORT).show();
				
			}
		});
		
		mChatView.setOrderConfirmListener(new Runnable() {
			
			@Override
			public void run() {
				Toast.makeText(ChatActivity.this, "确定订单", Toast.LENGTH_SHORT).show();
			}
		});
		
//		mChatView.setOnContentLongClickListener(new View.OnLongClickListener() {
//			
//			@Override
//			public boolean onLongClick(View v) {
//				
//				
//				return false;
//			}
//		});
		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// 为了实现捕获用户选择的图片
		mChatView.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// 为了实现点击返回键隐藏表情栏
		mChatView.onKeyDown(keyCode, event);
		return super.onKeyDown(keyCode, event);
	}
}
