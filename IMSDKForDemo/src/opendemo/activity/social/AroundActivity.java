package opendemo.activity.social;

import imsdk.data.around.IMMyselfAround;
import imsdk.data.around.IMMyselfAround.OnAroundActionListener;

import java.util.ArrayList;

import opendemo.activity.R;
import opendemo.activity.base.BaseListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

// 周边用户列表界面
// 主要演示功能：
// 1. 获取周边用户列表
// 2. 刷新周边用户列表
public class AroundActivity extends BaseListActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		initBaseList(R.layout.activity_around, IMMyselfAround.getAllUsers());

		// 设置标题栏显示内容
		setTitle("周围用户");

		mListView.showHeadFirstTime();
		mListView.setPullEnable(true);
		mListView.setLoadEnable(true);
		mListView.showHeadFirstTime();
		update();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if (position == 0) {
			return;
		}

		Intent intent = new Intent(AroundActivity.this, UserActivity.class);

		// 注意传入的参数position需要减一
		intent.putExtra("CustomUserID", IMMyselfAround.getUser(position - 1));
		startActivity(intent);
	}

	private void update() {
		// 刷新周边用户列表
		IMMyselfAround.update(new OnAroundActionListener() {
			@Override
			public void onSuccess(ArrayList<String> customUserIDsListInCurrentPage) {
				// 刷新列表
				mAdapter.notifyDataSetChanged();

				// 停止下拉刷新动画
				mListView.onRefreshComplete();
			}

			@Override
			public void onFailure(String error) {
				// 停止下拉刷新动画
				mListView.onRefreshComplete();

				// 提示加载失败
				Toast.makeText(AroundActivity.this, "加载失败:" + error, Toast.LENGTH_SHORT)
						.show();
			}
		});
	}

	@Override
	public void onRefresh() {
		update();
	}
}